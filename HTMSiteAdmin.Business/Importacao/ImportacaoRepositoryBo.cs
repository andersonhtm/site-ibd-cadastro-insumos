﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTMSiteAdmin.Data.Daos.Importacao;

namespace HTMSiteAdmin.Business.Importacao
{
    public class ImportacaoCertificadoBo : ImportacaoCertificadoDao { }

    public class ImportacaoClienteBo : ImportacaoClienteDao { }

    public class ImportacaoCategoriaProdutoBo : ImportacaoCategoriaProdutoDao { }

    public class ImportacaoFinalidadeUsoBo : ImportacaoFinalidadeUsoDao { }

    public class ImportacaoClienteProdutoBo : ImportacaoClienteProdutoDao { }

    public class ImportacaoClienteProdutoCertificadoBo : ImportacaoClienteProdutoCertificadoDao { }

    public class ImportacaoContatoBo : ImportacaoContatoDao { }

    public class ImportacaoProdutoBo : ImportacaoProdutoDao { }

    public class ImportacaoProdutoCertificadoBo : ImportacaoProdutoCertificadoDao { }

    public class ImportacaoProdutoCategoriaBo : ImportacaoProdutoCategoriaDao { }

    public class ImportacaoProdutoFinalidadeBo : ImportacaoProdutoFinalidadeDao { }

}
