﻿using System;
using System.Linq;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Data.Daos.Sistema.ControleAcesso;
using System.Text;
using HTMSiteAdmin.Library.Exceptions;

namespace HTMSiteAdmin.Business.Sistema.ControleAcesso
{
    public sealed class UsuarioBo : UsuarioDao
    {
        public static USUARIO CheckLogin(string UserID, string Pass)
        {
            try
            {
                StringBuilder sbValidacoes = new StringBuilder();

                if (string.IsNullOrWhiteSpace(UserID))
                    sbValidacoes.AppendLine("Usuário não informado.");

                if (string.IsNullOrWhiteSpace(Pass))
                    sbValidacoes.AppendLine("Senha não informada.");

                if (sbValidacoes.Length > 0)
                    throw new HTMSiteAdminException(sbValidacoes.ToString());

                return new UsuarioDao().Find(lbda => lbda.UID.Equals(UserID) && lbda.SENHA.Equals(Pass)).First();
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else if (ex.Message.Contains("Sequence contains no elements")) return null;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao validar o login. Entre em contato com o administrador do sistema.");
            }
        }
    }
}
