﻿using System;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Data.Daos.Sistema.ControleAcesso;
using HTMSiteAdmin.Library.Exceptions;

namespace HTMSiteAdmin.Business.Sistema.ControleAcesso
{
    public class SessaoBo : SessaoDao
    {
        public static SESSAO AbrirSessao(Guid SessionToken, string IPv4, string IPv6, string MachineName, string SystemInfo)
        {
            try
            {
                SESSAO _newSession = new SESSAO()
                {
                    ID_SESSAO = SessionToken,
                    DATA_INICIO = DateTime.Now,
                    IP = IPv4,
                    SISTEMA = SystemInfo
                };

                var _dao = new SessaoDao();
                _dao.Add(_newSession);
                _dao.SaveChanges();

                return _newSession;
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Não foi possível abrir uma nova sessão.");
            }
        }

    }
}
