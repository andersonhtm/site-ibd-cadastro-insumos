﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTMSiteAdmin.Data.Daos.Pessoas;

namespace HTMSiteAdmin.Business.Pessoas
{
    /// <summary>
    /// Tipos de Pessoa
    /// </summary>
    public enum enmTipoPessoa
    {
        PessoaFisica = 1,
        PessoaJuridica = 2
    }

    public class PessoaBo : PessoaDao { }

    public class PessoaClassificacaoBo : PessoaClassificacaoDao { }

    public class PessoaPessoaClassificacaoBo : PessoaPessoaClassificacaoDao { }

    public class PessoaTipoBo : PessoaTipoDao { }

    public class PessoaSituacaoBo : PessoaSituacaoDao { }

    public class PessoaContatoBo : PessoaContatoDao { }
}
