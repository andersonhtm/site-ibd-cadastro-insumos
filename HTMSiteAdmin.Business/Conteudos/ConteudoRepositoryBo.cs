﻿using HTMSiteAdmin.Data.Daos.Conteudos;

namespace HTMSiteAdmin.Business.Conteudos
{
    public class ConteudoBo : ConteudoDao { }

    public class ConteudoCategoriaBo : ConteudoCategoriaDao { }

    public class ConteudoCategoriaSituacaoBo : ConteudoCategoriaSituacaoDao { }

    public class ConteudoSituacaoBo : ConteudoSituacaoDao { }

    public class ConteudoTipoBo : ConteudoTipoDao { }

    public class ConteudoTipoSituacaoBo : ConteudoTipoSituacaoDao { }
}
