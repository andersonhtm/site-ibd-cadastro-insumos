﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTMSiteAdmin.Data.Daos.Galerias;

namespace HTMSiteAdmin.Business.Galerias
{
    public class GaleriaBo : GaleriaDao { }

    public class GaleriaArquivoBo : GaleriaArquivoDao { }

    public class GaleriaArquivoTipoBo : GaleriaArquivoTipoDao { }

    public class GaleriaSituacaoBo : GaleriaSituacaoDao { }

    public class GaleriaTipoBo : GaleriaTipoDao { }

}
