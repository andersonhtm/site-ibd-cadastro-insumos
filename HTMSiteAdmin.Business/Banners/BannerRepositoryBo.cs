﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTMSiteAdmin.Data.Daos.Banners;

namespace HTMSiteAdmin.Business.Banners
{
    public class BannerBo : BannerDao { }

    public class BannerPosicaoBo : BannerPosicaoDao { }

    public class BannerEstatisticaBo : BannerEstatisticaDao { }

    public class BannerPosicaoSituacaoBo : BannerPosicaoSituacaoDao { }

    public class BannerSituacaoBo : BannerSituacaoDao { }
}
