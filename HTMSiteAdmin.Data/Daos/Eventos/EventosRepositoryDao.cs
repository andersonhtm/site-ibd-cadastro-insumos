﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTMSiteAdmin.Data.Interfaces.Eventos;

namespace HTMSiteAdmin.Data.Daos.Eventos
{
    public class EventoCategoriaDao : BaseDao<EVENTO_CATEGORIA>, IEventoCategoriaDao { }

    public class EventoDao : BaseDao<EVENTO>, IEventoDao { }
}
