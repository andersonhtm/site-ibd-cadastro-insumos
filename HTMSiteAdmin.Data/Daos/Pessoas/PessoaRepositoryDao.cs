﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTMSiteAdmin.Data.Interfaces.Pessoas;

namespace HTMSiteAdmin.Data.Daos.Pessoas
{
    public class PessoaDao : BaseDao<PESSOA>, IPessoaDao { }

    public class PessoaClassificacaoDao : BaseDao<PESSOA_CLASSIFICACAO>, IPessoaClassificacaoDao { }

    public class PessoaPessoaClassificacaoDao : BaseDao<PESSOA_PESSOA_CLASSIFICACAO>, IPessoaPessoaClassificacaoDao { }

    public class PessoaTipoDao : BaseDao<PESSOA_TIPO>, IPessoaTipoDao { }

    public class PessoaSituacaoDao : BaseDao<PESSOA_SITUACAO>, IPessoaSituacaoDao { }

    public class PessoaContatoDao : BaseDao<PESSOA_CONTATO>, IPessoaContatoDao { }
}
