﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTMSiteAdmin.Data.Interfaces.Links;

namespace HTMSiteAdmin.Data.Daos.Links
{
    public class LinkSituacaoDao : BaseDao<LINK_SITUACAO>, ILinkSituacaoDao { }

    public class LinkCategoriaDao : BaseDao<LINK_CATEGORIA>, ILinkCategoriaDao { }

    public class LinkDao : BaseDao<LINK>, ILinkDao { }
}
