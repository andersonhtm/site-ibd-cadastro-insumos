﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTMSiteAdmin.Data.Interfaces.Importacao;

namespace HTMSiteAdmin.Data.Daos.Importacao
{
    public class ImportacaoCertificadoDao : BaseDao<IMPORTACAO_CERTIFICADO>, IImportacaoCertificadoDao { }

    public class ImportacaoClienteDao : BaseDao<IMPORTACAO_CLIENTE>, IImportacaoClienteDao { }

    public class ImportacaoCategoriaProdutoDao : BaseDao<IMPORTACAO_CATEGORIAPRODUTO>, IImportacaoCategoriaProdutoDao { }

    public class ImportacaoFinalidadeUsoDao : BaseDao<IMPORTACAO_FINALIDADEUSO>, IImportacaoFinalidadeUsoDao { }

    public class ImportacaoClienteProdutoDao : BaseDao<IMPORTACAO_CLIENTE_PRODUTO>, IImportacaoClienteProdutoDao { }

    public class ImportacaoClienteProdutoCertificadoDao : BaseDao<IMPORTACAO_CLIENTE_PRODUTO_CERTIFICADO>, IImportacaoClienteProdutoCertificadoDao { }

    public class ImportacaoContatoDao : BaseDao<IMPORTACAO_CONTATO>, IImportacaoContatoDao { }

    public class ImportacaoProdutoDao : BaseDao<IMPORTACAO_PRODUTO>, IImportacaoProdutoDao { }

    public class ImportacaoProdutoCertificadoDao : BaseDao<IMPORTACAO_PRODUTO_CERTIFICADO>, IImportacaoProdutoCertificadoDao { }

    public class ImportacaoProdutoCategoriaDao : BaseDao<IMPORTACAO_PRODUTO_CATEGORIAPRODUTO>, IImportacaoProdutoCategoriaDao { }
    
    public class ImportacaoProdutoFinalidadeDao : BaseDao<IMPORTACAO_PRODUTO_FINALIDADEUSO>, IImportacaoProdutoFinalidadeUsoDao { }
}
