﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTMSiteAdmin.Data.Interfaces.Downloads;

namespace HTMSiteAdmin.Data.Daos.Downloads
{
    public class DownloadDao : BaseDao<DOWNLOAD>, IDownloadDao { }

    public class DownloadCategoriaDao : BaseDao<DOWNLOAD_CATEGORIA>, IDownloadCategoriaDao { }
}
