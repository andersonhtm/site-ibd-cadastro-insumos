﻿using HTMSiteAdmin.Data.Interfaces.Conteudos;

namespace HTMSiteAdmin.Data.Daos.Conteudos
{
    public class ConteudoDao : BaseDao<CONTEUDO>, IConteudoDao { }

    public class ConteudoCategoriaDao : BaseDao<CONTEUDO_CATEGORIA>, IConteudoCategoriaDao { }

    public class ConteudoCategoriaSituacaoDao : BaseDao<CONTEUDO_CATEGORIA_SITUCAO>, IConteudoCategoriaSituacaoDao { }

    public class ConteudoSituacaoDao : BaseDao<CONTEUDO_SITUACAO>, IConteudoSituacaoDao { }

    public class ConteudoTipoDao : BaseDao<CONTEUDO_TIPO>, IConteudoTipoDao { }

    public class ConteudoTipoSituacaoDao : BaseDao<CONTEUDO_TIPO_SITUACAO>, IConteudoTipoSituacaoDao { }
}
