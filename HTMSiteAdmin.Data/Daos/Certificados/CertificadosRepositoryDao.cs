﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTMSiteAdmin.Data.Interfaces.Certificados;

namespace HTMSiteAdmin.Data.Daos.Certificados
{
    public class CertificadoCategoriaDao : BaseDao<CERTIFICADO_CATEGORIA>, ICertificadoCategoriaDao { }

    public class CertificadoDao : BaseDao<CERTIFICADO>, ICertificadoDao { }
}
