﻿using HTMSiteAdmin.Data.Interfaces.Sistema.ControleAcesso;

namespace HTMSiteAdmin.Data.Daos.Sistema.ControleAcesso
{
    public class UsuarioDao : BaseDao<USUARIO>, IUsuarioDao { }
}
