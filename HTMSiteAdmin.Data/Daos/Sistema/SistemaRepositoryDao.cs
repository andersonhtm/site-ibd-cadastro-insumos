﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTMSiteAdmin.Data.Interfaces.Sistema;

namespace HTMSiteAdmin.Data.Daos.Sistema
{
    public class MenuDao : BaseDao<MENU>, IMenuDao { }

    public class ArquivoDigitalDao : BaseDao<ARQUIVO_DIGITAL>, IArquivoDigitalDao { }
}
