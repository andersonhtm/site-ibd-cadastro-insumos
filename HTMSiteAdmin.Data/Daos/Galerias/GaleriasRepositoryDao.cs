﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTMSiteAdmin.Data.Interfaces.Galerias;

namespace HTMSiteAdmin.Data.Daos.Galerias
{
    public class GaleriaDao : BaseDao<GALERIA>, IGaleriaDao { }

    public class GaleriaArquivoDao : BaseDao<GALERIA_ARQUIVO>, IGaleriaArquivoDao { }

    public class GaleriaArquivoTipoDao : BaseDao<GALERIA_ARQUIVO_TIPO>, IGaleriaArquivoTipoDao { }

    public class GaleriaSituacaoDao : BaseDao<GALERIA_SITUACAO>, IGaleriaSituacaoDao { }

    public class GaleriaTipoDao : BaseDao<GALERIA_TIPO>, IGaleriaTipoDao { }

}
