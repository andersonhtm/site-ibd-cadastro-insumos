﻿using System;
using System.ComponentModel;
using System.Linq;
using HTMSiteAdmin.Data.Interfaces;

namespace HTMSiteAdmin.Data.Daos
{
    public abstract class BaseDao<T> : IBaseDao<T> where T : class
    {
        HTMSiteAdminEntities entidades = new HTMSiteAdminEntities();

        public void Add(T pEntity) { entidades.AddObject(pEntity.GetType().Name, pEntity); }

        public void Update(T pEntityCurrent) { entidades.ApplyCurrentValues<T>(pEntityCurrent.GetType().Name, pEntityCurrent); }

        public void Delete(T pEntity) { entidades.DeleteObject(pEntity); }

        public void Attach(T pEntity) { entidades.AttachTo(pEntity.GetType().Name, pEntity); }

        public void Detach(T pEntity) { entidades.Detach(pEntity); }

        public IQueryable<T> Find(System.Linq.Expressions.Expression<Func<T, bool>> where) { return entidades.CreateObjectSet<T>().Where(where); }

        public IQueryable<T> GetAll() { return entidades.CreateObjectSet<T>(); }

        public void SaveChanges() { entidades.SaveChanges(); }
    }
}
