﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTMSiteAdmin.Data.Interfaces.Localizacao;

namespace HTMSiteAdmin.Data.Daos.Localizacao
{
    public class UfDao : BaseDao<UF>, IUfDao { }
}
