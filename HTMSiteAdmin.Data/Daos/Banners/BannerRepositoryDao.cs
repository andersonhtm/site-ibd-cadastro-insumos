﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTMSiteAdmin.Data.Interfaces.Banners;

namespace HTMSiteAdmin.Data.Daos.Banners
{
    public class BannerDao : BaseDao<BANNER>, IBannerDao { }

    public class BannerPosicaoDao : BaseDao<BANNER_POSICAO>, IBannerPosicaoDao { }

    public class BannerEstatisticaDao : BaseDao<BANNER_ESTATISTICA>, IBannerEstatisticaDao { }

    public class BannerPosicaoSituacaoDao : BaseDao<BANNER_POSICAO_SITUACAO>, IBannerPosicaoSituacaoDao { }

    public class BannerSituacaoDao : BaseDao<BANNER_SITUACAO>, IBannerSituacaoDao { }
}
