﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace HTMSiteAdmin.Data.Interfaces
{
    public interface IBaseDao<T>
    {
        void Add(T pEntity);
        void Update(T pEntityCurrent);
        void Delete(T pEntity);
        void Attach(T pEntity);
        void Detach(T pEntity);

        IQueryable<T> Find(Expression<Func<T, bool>> where);
        IQueryable<T> GetAll();

        void SaveChanges();
    }
}
