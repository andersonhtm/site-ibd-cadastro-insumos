﻿
namespace HTMSiteAdmin.Data.Interfaces.Conteudos
{
    public interface IConteudoDao : IBaseDao<CONTEUDO> { }

    public interface IConteudoCategoriaDao : IBaseDao<CONTEUDO_CATEGORIA> { }

    public interface IConteudoCategoriaSituacaoDao : IBaseDao<CONTEUDO_CATEGORIA_SITUCAO> { }

    public interface IConteudoSituacaoDao : IBaseDao<CONTEUDO_SITUACAO> { }

    public interface IConteudoTipoDao : IBaseDao<CONTEUDO_TIPO> { }

    public interface IConteudoTipoSituacaoDao : IBaseDao<CONTEUDO_TIPO_SITUACAO> { }
}
