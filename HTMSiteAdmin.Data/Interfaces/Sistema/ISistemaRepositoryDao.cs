﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HTMSiteAdmin.Data.Interfaces.Sistema
{
    public interface IMenuDao : IBaseDao<MENU> { }

    public interface IArquivoDigitalDao : IBaseDao<ARQUIVO_DIGITAL> { }
}
