﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HTMSiteAdmin.Data.Interfaces.Banners
{
    public interface IBannerDao : IBaseDao<BANNER> { }

    public interface IBannerPosicaoDao : IBaseDao<BANNER_POSICAO> { }

    public interface IBannerEstatisticaDao : IBaseDao<BANNER_ESTATISTICA> { }

    public interface IBannerPosicaoSituacaoDao : IBaseDao<BANNER_POSICAO_SITUACAO> { }

    public interface IBannerSituacaoDao : IBaseDao<BANNER_SITUACAO> { }
}
