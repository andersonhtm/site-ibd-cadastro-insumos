﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HTMSiteAdmin.Data.Interfaces.Downloads
{
    public interface IDownloadDao : IBaseDao<DOWNLOAD> { }

    public interface IDownloadCategoriaDao : IBaseDao<DOWNLOAD_CATEGORIA> { }
}
