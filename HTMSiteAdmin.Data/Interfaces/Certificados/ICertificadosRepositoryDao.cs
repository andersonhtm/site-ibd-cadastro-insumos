﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HTMSiteAdmin.Data.Interfaces.Certificados
{
    public interface ICertificadoCategoriaDao : IBaseDao<CERTIFICADO_CATEGORIA> { }

    public interface ICertificadoDao : IBaseDao<CERTIFICADO> { }
}
