﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HTMSiteAdmin.Data.Interfaces.Importacao
{
    public interface IImportacaoCertificadoDao : IBaseDao<IMPORTACAO_CERTIFICADO> { }

    public interface IImportacaoClienteDao : IBaseDao<IMPORTACAO_CLIENTE> { }

    public interface IImportacaoCategoriaProdutoDao : IBaseDao<IMPORTACAO_CATEGORIAPRODUTO> { }
    
    public interface IImportacaoFinalidadeUsoDao : IBaseDao<IMPORTACAO_FINALIDADEUSO> { }

    public interface IImportacaoClienteProdutoDao : IBaseDao<IMPORTACAO_CLIENTE_PRODUTO> { }

    public interface IImportacaoClienteProdutoCertificadoDao : IBaseDao<IMPORTACAO_CLIENTE_PRODUTO_CERTIFICADO> { }

    public interface IImportacaoContatoDao : IBaseDao<IMPORTACAO_CONTATO> { }

    public interface IImportacaoProdutoDao : IBaseDao<IMPORTACAO_PRODUTO> { }

    public interface IImportacaoProdutoCertificadoDao : IBaseDao<IMPORTACAO_PRODUTO_CERTIFICADO> { }

    public interface IImportacaoProdutoCategoriaDao : IBaseDao<IMPORTACAO_PRODUTO_CATEGORIAPRODUTO> { }

    public interface IImportacaoProdutoFinalidadeUsoDao : IBaseDao<IMPORTACAO_PRODUTO_FINALIDADEUSO> { }
}
