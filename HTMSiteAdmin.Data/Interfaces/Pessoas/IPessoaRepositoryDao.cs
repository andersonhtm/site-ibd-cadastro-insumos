﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HTMSiteAdmin.Data.Interfaces.Pessoas
{
    public interface IPessoaDao : IBaseDao<PESSOA> { }

    public interface IPessoaClassificacaoDao : IBaseDao<PESSOA_CLASSIFICACAO> { }

    public interface IPessoaPessoaClassificacaoDao : IBaseDao<PESSOA_PESSOA_CLASSIFICACAO> { }

    public interface IPessoaTipoDao : IBaseDao<PESSOA_TIPO> { }

    public interface IPessoaSituacaoDao : IBaseDao<PESSOA_SITUACAO> { }

    public interface IPessoaContatoDao : IBaseDao<PESSOA_CONTATO> { }
}
