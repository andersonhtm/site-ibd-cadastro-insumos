﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HTMSiteAdmin.Data.Interfaces.Links
{
    public interface ILinkSituacaoDao : IBaseDao<LINK_SITUACAO> { }

    public interface ILinkCategoriaDao : IBaseDao<LINK_CATEGORIA> { }

    public interface ILinkDao : IBaseDao<LINK> { }
}
