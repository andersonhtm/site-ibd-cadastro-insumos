﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HTMSiteAdmin.Data.Interfaces.Galerias
{
    public interface IGaleriaDao : IBaseDao<GALERIA> { }

    public interface IGaleriaArquivoDao : IBaseDao<GALERIA_ARQUIVO> { }

    public interface IGaleriaArquivoTipoDao : IBaseDao<GALERIA_ARQUIVO_TIPO> { }

    public interface IGaleriaSituacaoDao : IBaseDao<GALERIA_SITUACAO> { }

    public interface IGaleriaTipoDao : IBaseDao<GALERIA_TIPO> { }
}
