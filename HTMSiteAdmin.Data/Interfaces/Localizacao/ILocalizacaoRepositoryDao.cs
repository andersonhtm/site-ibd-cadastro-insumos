﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HTMSiteAdmin.Data.Interfaces.Localizacao
{
    public interface IUfDao : IBaseDao<UF> { }
}
