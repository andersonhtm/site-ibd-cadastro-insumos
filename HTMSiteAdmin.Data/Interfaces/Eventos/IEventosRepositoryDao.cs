﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HTMSiteAdmin.Data.Interfaces.Eventos
{
    public interface IEventoCategoriaDao : IBaseDao<EVENTO_CATEGORIA> { }

    public interface IEventoDao : IBaseDao<EVENTO> { }
}
