﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace HTMSiteAdmin.Web.Classes
{
    public class Configuracao
    {
        private static string _SYSTEM_SMTP;
        public static string SYSTEM_SMTP
        {
            get
            {
                if (string.IsNullOrEmpty(_SYSTEM_SMTP))
                {
                    _SYSTEM_SMTP = ConfigurationManager.AppSettings["SYSTEM_SMTP"].ToString();
                }
                return Configuracao._SYSTEM_SMTP;
            }
        }

        private static string _SYSTEM_MAIL;
        public static string SYSTEM_MAIL
        {
            get
            {
                if (string.IsNullOrEmpty(_SYSTEM_MAIL))
                {
                    _SYSTEM_MAIL = ConfigurationManager.AppSettings["SYSTEM_MAIL"].ToString();
                }
                return Configuracao._SYSTEM_MAIL;
            }
        }

        private static string _SYSTEM_PWD;
        public static string SYSTEM_PWD
        {
            get
            {
                if (string.IsNullOrEmpty(_SYSTEM_PWD))
                {
                    _SYSTEM_PWD = ConfigurationManager.AppSettings["SYSTEM_PWD"].ToString();
                }
                return Configuracao._SYSTEM_PWD;
            }
        }

        private static int? _SYSTEM_MAILPORT;
        public static int SYSTEM_MAILPORT
        {
            get
            {
                if (!_SYSTEM_MAILPORT.HasValue)
                {
                    _SYSTEM_MAILPORT = (int.Parse(ConfigurationManager.AppSettings["SYSTEM_MAILPORT"]));
                }
                return Configuracao._SYSTEM_MAILPORT.Value;
            }
        }

        private static bool? _SYSTEM_MAILENABLESSL;
        public static bool SYSTEM_MAILENABLESSL
        {
            get
            {
                if (!_SYSTEM_MAILENABLESSL.HasValue)
                {
                    _SYSTEM_MAILENABLESSL = ConfigurationManager.AppSettings["SYSTEM_MAILENABLESSL"].ToString().ToLower() == "true";
                }
                return Configuracao._SYSTEM_MAILENABLESSL.Value;
            }
        }

        private static string _REGISTER_EMAILTO;
        public static string REGISTER_EMAILTO
        {
            get
            {
                if (string.IsNullOrEmpty(_REGISTER_EMAILTO))
                {
                    _REGISTER_EMAILTO = ConfigurationManager.AppSettings["REGISTER_EMAILTO"].ToString();
                }
                return Configuracao._REGISTER_EMAILTO;
            }
        }

        private static string _REGISTER_EMAILREPLY;
        public static string REGISTER_EMAILREPLY
        {
            get
            {
                if (string.IsNullOrEmpty(_REGISTER_EMAILREPLY))
                {
                    _REGISTER_EMAILREPLY = ConfigurationManager.AppSettings["REGISTER_EMAILREPLY"].ToString();
                }
                return Configuracao._REGISTER_EMAILREPLY;
            }
        }

        private static string _CV_EMAILTO;
        public static string CV_EMAILTO
        {
            get
            {
                if (string.IsNullOrEmpty(_CV_EMAILTO))
                {
                    _CV_EMAILTO = ConfigurationManager.AppSettings["CV_EMAILTO"].ToString();
                }
                return Configuracao._CV_EMAILTO;
            }
        }

        private static string _CV_EMAILREPLY;
        public static string CV_EMAILREPLY
        {
            get
            {
                if (string.IsNullOrEmpty(_CV_EMAILREPLY))
                {
                    _CV_EMAILREPLY = ConfigurationManager.AppSettings["CV_EMAILREPLY"].ToString();
                }
                return Configuracao._CV_EMAILREPLY;
            }
        }

        private static string _CONTACT_EMAILTO;
        public static string CONTACT_EMAILTO
        {
            get
            {
                if (string.IsNullOrEmpty(_CONTACT_EMAILTO))
                {
                    _CONTACT_EMAILTO = ConfigurationManager.AppSettings["CONTACT_EMAILTO"].ToString();
                }
                return Configuracao._CONTACT_EMAILTO;
            }
        }

        private static string _CONTACT_EMAILREPLY;
        public static string CONTACT_EMAILREPLY
        {
            get
            {
                if (string.IsNullOrEmpty(_CONTACT_EMAILREPLY))
                {
                    _CONTACT_EMAILREPLY = ConfigurationManager.AppSettings["CONTACT_EMAILREPLY"].ToString();
                }
                return Configuracao._CONTACT_EMAILREPLY;
            }
        }

        private static string _MAIL_BLINDCOPYTO;
        public static string MAIL_BLINDCOPYTO
        {
            get
            {
                if (string.IsNullOrEmpty(_MAIL_BLINDCOPYTO))
                {
                    _MAIL_BLINDCOPYTO = ConfigurationManager.AppSettings["MAIL_BLINDCOPYTO"].ToString();
                }
                return Configuracao._MAIL_BLINDCOPYTO;
            }
        }

        private static string _SYSTEM_MAILFROMNAME;
        public static string SYSTEM_MAILFROMNAME
        {
            get
            {
                if (string.IsNullOrEmpty(_SYSTEM_MAILFROMNAME))
                {
                    _SYSTEM_MAILFROMNAME = ConfigurationManager.AppSettings["SYSTEM_MAILFROMNAME"].ToString();
                }
                return Configuracao._SYSTEM_MAILFROMNAME;
            }
        }
    }
}