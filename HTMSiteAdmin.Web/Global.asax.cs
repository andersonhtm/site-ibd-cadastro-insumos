﻿using System;
using System.Web.UI;
using LifeTimeObjects;

namespace HTMSiteAdmin.Web
{
    public class Global : System.Web.HttpApplication
    {
        #region [ Session Objects ]

        public static void SaveObject(Page _Page, string ViewToken, string ObjectName, object ObjectToSave) { SessionLifeTimeObjects.Save(_Page, ViewToken, ObjectName, ObjectToSave); }
        public static object RestoreObject(Page _Page, string ViewToken, string ObjectName) { return SessionLifeTimeObjects.Restore(_Page, ViewToken, ObjectName); }
        public static Guid GetSessionToken(Page _Page) { return new Guid(LifeTimeObjectsUtil.GetSessionToken(_Page)); }

        #endregion

        #region [ User's Acess Control ]

        public static void Login(Page _Page, string OBJECT_USER_KEY, object OBJECT_USER) { LoggedUsers.Login(_Page, OBJECT_USER_KEY, OBJECT_USER); }
        public static object GetLoggedUser(Page _Page) { return LoggedUsers.GetLoggedUser(_Page); }
        public static void Logout(Page _Page) { LoggedUsers.Logout(_Page); }

        #endregion

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
