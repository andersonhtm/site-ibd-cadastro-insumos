﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Library.Exceptions;
using System.Text;
using System.IO;
using HTMSiteAdmin.Business.Importacao;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class ImportacaoClienteUpload : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this, ViewToken, "ViewAspect", value); }
        }
        private UploadedFile ArquivoEnviado
        {
            get { return (UploadedFile)Global.RestoreObject(this, ViewToken, "ArquivoEnviado"); }
            set { Global.SaveObject(this, ViewToken, "ArquivoEnviado", value); }
        }
        private List<IMPORTACAO_CLIENTE> RegistrosLidos
        {
            get { return (List<IMPORTACAO_CLIENTE>)Global.RestoreObject(this, ViewToken, "RegistrosLidos"); }
            set { Global.SaveObject(this, ViewToken, "RegistrosLidos", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
        }

        public void ValidateAcess() { throw new NotImplementedException(); }

        public void MapButtons() { throw new NotImplementedException(); }

        public void UserInterfaceAspectChanged()
        {
            //StartPageControls();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    break;
                case enmUserInterfaceAspect.New:
                    break;
                case enmUserInterfaceAspect.Update:
                    break;
                default:
                    break;
            }
        }

        public void StartPageControls() { throw new NotImplementedException(); }

        public void RestartForm() { throw new NotImplementedException(); }

        public void ClearMemory() { throw new NotImplementedException(); }

        public void ClearFields() { throw new NotImplementedException(); }

        public void EnableFields() { throw new NotImplementedException(); }

        public void DisableFields() { throw new NotImplementedException(); }

        public void ButtonNew_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonSave_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonSaveNew_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonRefresh_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonCancel_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonDelete_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void GetForm() { throw new NotImplementedException(); }

        public void SaveView() { throw new NotImplementedException(); }

        public void SetForm() { throw new NotImplementedException(); }

        protected void afuArquivo_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e) { ArquivoEnviado = HTMSiteAdmin.Library.UserInterface.WebControlsUtility.AsyncFileUpload_UploadedComplete(sender, e); }

        protected void btnLerArquivo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (ArquivoEnviado == null)
                    throw new HTMSiteAdminException("Nenhum arquivo foi enviado.");

                if (!ArquivoEnviado.FileExtension.ToUpper().Contains("CSV"))
                    throw new HTMSiteAdminException("Arquivo enviado não é do tipo CSV.");

                StringBuilder sbErrosLeitura = new StringBuilder();

                using (MemoryStream _objMemoryStream = new MemoryStream(ArquivoEnviado.FileBytes))
                {
                    using (StreamReader _objReader = new StreamReader(_objMemoryStream, true))
                    {
                        bool isPrimeiraLinha = true;
                        string strLinha;
                        int nrLinha = 1;
                        while ((strLinha = _objReader.ReadLine()) != null)
                        {
                            if (isPrimeiraLinha)
                            {
                                if (!strLinha.Equals("ID_CLIENTE;PF_PJ;MATRICULA;APELIDO_FANTASIA;NOME_RAZAO;CPF_CNPJ;RG_IE;CEP;LOGRADOURO;ENDERECO;NUMERO;CXPOSTAL;COMPLEMENTO;MUNICIPIO;ESTADO;UF;PAIS;PAIS_EN;EMAIL;DDI;TELEFONE;FAX;CELULAR;SITE"))
                                    throw new HTMSiteAdminException("O cabeçalho do arquivo é inválido. Por favor faça o download do arquivo modelo e siga o layout do mesmo.");

                                isPrimeiraLinha = false;
                            }
                            else
                            {
                                try
                                {
                                    if (RegistrosLidos == null)
                                        RegistrosLidos = new List<IMPORTACAO_CLIENTE>();

                                    string[] arrLinha = strLinha.Split(';');

                                    StringBuilder sbValidacoes = new StringBuilder();

                                    if (string.IsNullOrWhiteSpace(arrLinha[0]))
                                        sbValidacoes.AppendLine(string.Format("Erro ao ler a linha {0}. Motivo: o ID do cliente é obrigatório.", nrLinha));

                                    if (string.IsNullOrWhiteSpace(arrLinha[1]))
                                        sbValidacoes.AppendLine(string.Format("Erro ao ler a linha {0}. Motivo: o Tipo de Pessoa é obrigatório.", nrLinha));

                                    if (sbValidacoes.Length > 0)
                                        throw new HTMSiteAdminException(sbValidacoes.ToString());

                                    RegistrosLidos.Add(new IMPORTACAO_CLIENTE()
                                    {
                                        ID_CLIENTE = arrLinha[0],
                                        PF_PJ = arrLinha[1].Equals("F") ? 1 : 2,
                                        MATRICULA = !string.IsNullOrWhiteSpace(arrLinha[2]) ? arrLinha[2] : string.Empty,
                                        APELIDO_FANTASIA = !string.IsNullOrWhiteSpace(arrLinha[3]) ? arrLinha[3] : string.Empty,
                                        NOME_RAZAO = !string.IsNullOrWhiteSpace(arrLinha[4]) ? arrLinha[4] : string.Empty,
                                        CPF_CNPJ = !string.IsNullOrWhiteSpace(arrLinha[5]) ? arrLinha[5] : string.Empty,
                                        RG_IE = !string.IsNullOrWhiteSpace(arrLinha[6]) ? arrLinha[6] : string.Empty,
                                        CEP = !string.IsNullOrWhiteSpace(arrLinha[7]) ? arrLinha[7] : string.Empty,
                                        LOGRADOURO = !string.IsNullOrWhiteSpace(arrLinha[8]) ? arrLinha[8] : string.Empty,
                                        ENDERECO = !string.IsNullOrWhiteSpace(arrLinha[9]) ? arrLinha[9] : string.Empty,
                                        NUMERO = !string.IsNullOrWhiteSpace(arrLinha[10]) ? arrLinha[10] : string.Empty,
                                        CXPOSTAL = !string.IsNullOrWhiteSpace(arrLinha[11]) ? arrLinha[11] : string.Empty,
                                        COMPLEMENTO = !string.IsNullOrWhiteSpace(arrLinha[12]) ? arrLinha[12] : string.Empty,
                                        MUNICIPIO = !string.IsNullOrWhiteSpace(arrLinha[13]) ? arrLinha[13] : string.Empty,
                                        ESTADO = !string.IsNullOrWhiteSpace(arrLinha[14]) ? arrLinha[14] : string.Empty,
                                        ESTADO_SIGLA = !string.IsNullOrWhiteSpace(arrLinha[15]) ? arrLinha[15] : string.Empty,
                                        PAIS = !string.IsNullOrWhiteSpace(arrLinha[16]) ? arrLinha[16] : string.Empty,
                                        PAIS_EN = !string.IsNullOrWhiteSpace(arrLinha[17]) ? arrLinha[17] : string.Empty,
                                        EMAIL = !string.IsNullOrWhiteSpace(arrLinha[18]) ? arrLinha[18] : string.Empty,
                                        DDI = !string.IsNullOrWhiteSpace(arrLinha[19]) ? arrLinha[19] : string.Empty,
                                        TELEFONE = !string.IsNullOrWhiteSpace(arrLinha[20]) ? arrLinha[20] : string.Empty,
                                        FAX = !string.IsNullOrWhiteSpace(arrLinha[21]) ? arrLinha[21] : string.Empty,
                                        CELULAR = !string.IsNullOrWhiteSpace(arrLinha[22]) ? arrLinha[22] : string.Empty,
                                        SITE = !string.IsNullOrWhiteSpace(arrLinha[23]) ? arrLinha[23] : string.Empty,
                                        REGISTRO_ORIGEM = 2,
                                        ID_SESSAO = Global.GetSessionToken(this.Page)
                                    });
                                }
                                catch (Exception ex)
                                {
                                    if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) sbErrosLeitura.AppendLine(ex.Message);
                                    else sbErrosLeitura.AppendLine(string.Format("Erro desconhecido ao ler a linha {0}. Motivo: {1}", nrLinha, ex.Message));
                                }
                            }

                            nrLinha++;
                        }
                    }
                }

                if (sbErrosLeitura.Length > 0)
                    ucShowException1.ShowExceptions(new HTMSiteAdminException(sbErrosLeitura.ToString()));

                gvView.DataSource = RegistrosLidos;
                gvView.DataBind();
            }
            catch (Exception ex)
            {
                RegistrosLidos = null;
                ucShowException1.ShowExceptions(ex);
            }
            finally { ArquivoEnviado = null; }
        }

        protected void btnEnviarDados_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                var _objBo = new ImportacaoClienteBo();

                foreach (var item in RegistrosLidos)
                {
                    if (_objBo.Find(lbda => lbda.ID_CLIENTE == item.ID_CLIENTE && lbda.REGISTRO_ORIGEM == 2).Count() > 0)
                    {
                        var _objAtual = _objBo.Find(lbda => lbda.ID_CLIENTE == item.ID_CLIENTE && lbda.REGISTRO_ORIGEM == 2).First();

                        _objAtual.PF_PJ = item.PF_PJ;
                        _objAtual.MATRICULA = item.MATRICULA;
                        _objAtual.APELIDO_FANTASIA = item.APELIDO_FANTASIA;
                        _objAtual.NOME_RAZAO = item.NOME_RAZAO;
                        _objAtual.CPF_CNPJ = item.CPF_CNPJ;
                        _objAtual.RG_IE = item.RG_IE;
                        _objAtual.CEP = item.CEP;
                        _objAtual.LOGRADOURO = item.LOGRADOURO;
                        _objAtual.ENDERECO = item.ENDERECO;
                        _objAtual.NUMERO = item.NUMERO;
                        _objAtual.CXPOSTAL = item.CXPOSTAL;
                        _objAtual.COMPLEMENTO = item.COMPLEMENTO;
                        _objAtual.MUNICIPIO = item.MUNICIPIO;
                        _objAtual.ESTADO = item.ESTADO;
                        _objAtual.ESTADO_SIGLA = item.ESTADO_SIGLA;
                        _objAtual.PAIS = item.PAIS;
                        _objAtual.PAIS_EN = item.PAIS_EN;
                        _objAtual.PAIS_ES = item.PAIS_ES;
                        _objAtual.EMAIL = item.EMAIL;
                        _objAtual.DDI = item.DDI;
                        _objAtual.TELEFONE = item.TELEFONE;
                        _objAtual.FAX = item.FAX;
                        _objAtual.CELULAR = item.CELULAR;
                        _objAtual.SITE = item.SITE;
                        _objAtual.ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO;

                        _objBo.Update(_objAtual);
                    }
                    else
                        _objBo.Add(item);
                }

                _objBo.SaveChanges();
                ucShowException1.Sucess();
                RegistrosLidos = null;
                gvView.DataSource = null;
                gvView.DataBind();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
            finally { ArquivoEnviado = null; }
        }
    }
}