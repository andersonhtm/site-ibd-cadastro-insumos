﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Business.Sistema;
using HTMSiteAdmin.Business.Sistema.ControleAcesso;
using HTMSiteAdmin.Data;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        private string ViewToken
        {
            get { return "MASTER_PAGE_TOKEN"; }
        }
        /*
        private SESSAO SessaoAberta
        {
            get { return (SESSAO)Global.RestoreObject(this.Page, ViewToken, "SessaoAberta"); }
            set { Global.SaveObject(this.Page, ViewToken, "SessaoAberta", value); }
        }
        */

        private SESSAO SessaoAberta
        {
            get { return (Session["SessaoAberta"] as SESSAO); }
            set { Session["SessaoAberta"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            /*
            if (SessaoAberta == null && this.Page.Session["SessionToken"] == null)
                SessaoAberta = SessaoBo.AbrirSessao(Global.GetSessionToken(this.Page), Request.UserHostAddress, null, Request.UserHostName, Request.UserAgent);

            if (Global.GetLoggedUser(this.Page) == null)
                Response.Redirect("~/Admin/Login.aspx");
            */
            if (SessaoAberta == null)
                Response.Redirect("~/Admin/Login.aspx");

            IniciarMenu();

            ltrMemory.Text = HTMSiteAdmin.Library.Util.GetTotalMemoryString();
            
            if (SessaoAberta.ID_USUARIO_LOGADO.HasValue)
                ltrUsuario.Text = SessaoAberta.USUARIO.PESSOA.ID_PESSOA_TIPO == 1 ? SessaoAberta.USUARIO.PESSOA.PF_NOME_COMPLETO : SessaoAberta.USUARIO.PESSOA.PJ_RAZAO_SOCIAL;

        }

        private void IniciarMenu()
        {
            try
            {
                NavigationMenu.Items.Clear();

                IQueryable<MENU> _objRootMenus = new MenuBo().Find(lbda => !lbda.ID_MENU_SUPERIOR.HasValue).OrderByDescending(lbda => lbda.PRIORIDADE);

                if (_objRootMenus != null && _objRootMenus.Count() > 0)
                {
                    foreach (MENU _objRootMenu in _objRootMenus)
                    {
                        if (_objRootMenu.ID_MENU_SITUACAO == 1)
                        {
                            MenuItem _objRootMenuItem = new MenuItem();
                            _objRootMenuItem.Text = _objRootMenu.NOME;
                            _objRootMenuItem.NavigateUrl = !string.IsNullOrWhiteSpace(_objRootMenu.URL) ? _objRootMenu.URL : string.Empty;
                            _objRootMenuItem.ToolTip = !string.IsNullOrWhiteSpace(_objRootMenu.DESCRICAO) ? _objRootMenu.DESCRICAO : string.Empty;

                            GetSubMenus(_objRootMenu, ref _objRootMenuItem);

                            NavigationMenu.Items.Add(_objRootMenuItem);
                            NavigationMenu.DataBind();
                        }
                    }
                }

                _objRootMenus = null;
            }
            catch (Exception ex) { throw; }
        }

        private void GetSubMenus(MENU _objRootMenu, ref MenuItem _objRootMenuItem)
        {
            try
            {
                IQueryable<MENU> _objSubMenus = new MenuBo().Find(lbda => lbda.ID_MENU_SUPERIOR == _objRootMenu.ID_MENU).OrderByDescending(lbda => lbda.PRIORIDADE);

                if (_objSubMenus != null && _objSubMenus.Count() > 0)
                {
                    foreach (MENU _objSubMenu in _objSubMenus)
                    {
                        if (_objSubMenu.ID_MENU_SITUACAO == 1)
                        {
                            MenuItem _objSubMenuItem = new MenuItem();
                            _objSubMenuItem.Text = _objSubMenu.NOME;
                            _objSubMenuItem.NavigateUrl = !string.IsNullOrWhiteSpace(_objSubMenu.URL) ? _objSubMenu.URL : string.Empty;
                            _objSubMenuItem.ToolTip = !string.IsNullOrWhiteSpace(_objSubMenu.DESCRICAO) ? _objSubMenu.DESCRICAO : string.Empty;

                            GetSubMenus(_objSubMenu, ref _objSubMenuItem);

                            _objRootMenuItem.ChildItems.Add(_objSubMenuItem);
                        }
                    }
                }

                _objSubMenus = null;
            }
            catch (Exception ex) { throw; }
        }

        protected void btnLogout_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                SessaoAberta = null;
                Response.Redirect("~/Admin/Login.aspx");
            }
            catch (Exception) { throw; }
        }
    }
}