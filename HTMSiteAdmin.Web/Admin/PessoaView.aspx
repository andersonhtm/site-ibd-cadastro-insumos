﻿<%@ Page Title=":: MasterPainel - HTM Gestão e Tecnologia ::" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true"
    CodeBehind="PessoaView.aspx.cs" Inherits="HTMSiteAdmin.Web.Admin.PessoaView" %>

<%@ Register Src="UserControls/ucShowException.ascx" TagName="ucShowException" TagPrefix="uc1" %>
<%@ Register Src="UserControls/Bars/ucButtons.ascx" TagName="ucButtons" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" language="javascript">
        function SetarApelido() {
            if ($get('<%=txtApelido.ClientID%>').value == '') $get('<%=txtApelido.ClientID%>').value = $get('<%=txtNome_completo.ClientID%>').value;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upView" runat="server">
        <ContentTemplate>
            <h1>
                ..:: Cadastro de Pessoas
            </h1>
            <asp:Panel ID="pnlView" runat="server">
                <fieldset class="Fieldset">
                    <legend>:: Dados Básicos</legend>
                    <div class="Fields">
                        <p>
                            <asp:Label ID="Label2" runat="server" Text="Código/Matrícula:" AssociatedControlID="txtCodigo"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtCodigo" MaxLength="50" Width="300px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="Label4" runat="server" Text="Classificação:" CssClass="Label" AssociatedControlID="chkPessoaClassificacao" />
                            <asp:CheckBoxList ID="chkPessoaClassificacao" runat="server" DataSourceID="edsPessoaClassificacao"
                                DataTextField="NOME" DataValueField="ID_PESSOA_CLASSIFICACAO" RepeatDirection="Horizontal"
                                RepeatLayout="Flow" />
                            <asp:EntityDataSource ID="edsPessoaClassificacao" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                                DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="PESSOA_CLASSIFICACAO"
                                EntityTypeFilter="PESSOA_CLASSIFICACAO" Select="it.[ID_PESSOA_CLASSIFICACAO], it.[NOME]" />
                        </p>
                        <p>
                            <asp:Label ID="Label3" runat="server" Text="Tipo de Pessoa:" AssociatedControlID="ddlTipoPessoa"
                                CssClass="Label" />
                            <asp:DropDownList ID="ddlTipoPessoa" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTipoPessoa_SelectedIndexChanged"
                                CssClass="Field">
                                <asp:ListItem Text="Pessoa Física" Selected="True" Value="1" />
                                <asp:ListItem Text="Pessoa Jurídica" Value="2" />
                            </asp:DropDownList>
                        </p>
                        <asp:Panel ID="pnlPessoaJuridica" runat="server" Visible="false">
                            <p>
                                <asp:Label ID="lblRazao_social" runat="server" Text="Razão social:" AssociatedControlID="txtRazao_social"
                                    CssClass="Label" />
                                <asp:TextBox runat="server" ID="txtRazao_social" MaxLength="255" Width="400px" CssClass="Field" />
                                <span class="Validators">*</span>
                                <asp:RequiredFieldValidator ID="rfvRazao_social" runat="server" ControlToValidate="txtRazao_social"
                                    CssClass="Validators" Display="Dynamic" ErrorMessage="Campo Requerido" SetFocusOnError="True"
                                    ValidationGroup="vgView" />
                            </p>
                            <p>
                                <asp:Label ID="lblNome_fantasia" runat="server" Text="Nome fantasia:" AssociatedControlID="txtNome_fantasia"
                                    CssClass="Label" />
                                <asp:TextBox runat="server" ID="txtNome_fantasia" MaxLength="255" Width="400px" CssClass="Field" />
                                <span class="Validators">*</span>
                                <asp:RequiredFieldValidator ID="rfvNome_fantasia" runat="server" ControlToValidate="txtNome_fantasia"
                                    CssClass="Validators" Display="Dynamic" ErrorMessage="Campo Requerido" SetFocusOnError="True"
                                    ValidationGroup="vgView" />
                            </p>
                            <p style="vertical-align: middle;">
                                <asp:Label ID="Label6" runat="server" Text="CNPJ:" AssociatedControlID="txtCnpj"
                                    CssClass="Label" />
                                <asp:TextBox runat="server" ID="txtCnpj" MaxLength="15" CssClass="Field" />
                                <ajaxToolkit:FilteredTextBoxExtender ID="txtCnpj_FilteredTextBoxExtender" runat="server"
                                    TargetControlID="txtCnpj" FilterType="Numbers" />
                            </p>
                            <p>
                                <asp:Label ID="lblData_fundacao" runat="server" Text="Data de fundação:" AssociatedControlID="txtData_fundacao"
                                    CssClass="Label" />
                                <asp:TextBox runat="server" ID="txtData_fundacao" MaxLength="8" Width="70px" CssClass="Field" />
                                <ajaxToolkit:MaskedEditExtender ID="txtData_fundacao_MaskedEditExtender" runat="server"
                                    TargetControlID="txtData_fundacao" ClearMaskOnLostFocus="true" MaskType="Date"
                                    Mask="99/99/9999" UserDateFormat="DayMonthYear" />
                            </p>
                        </asp:Panel>
                        <asp:Panel ID="pnlPessoaFisica" runat="server" Visible="true">
                            <p>
                                <asp:Label ID="lblNome_completo" runat="server" Text="Nome completo:" AssociatedControlID="txtNome_completo"
                                    CssClass="Label" />
                                <asp:TextBox runat="server" ID="txtNome_completo" MaxLength="255" Width="300px" CssClass="Field" />
                                <span class="Validators">*</span>
                                <asp:RequiredFieldValidator ID="rfvNome_completo" runat="server" ControlToValidate="txtNome_completo"
                                    CssClass="Validators" Display="Dynamic" ErrorMessage="Campo Requerido" SetFocusOnError="True"
                                    ValidationGroup="vgView" />
                            </p>
                            <p>
                                <asp:Label ID="Label10" runat="server" Text="Apelido:" AssociatedControlID="txtApelido"
                                    CssClass="Label" />
                                <asp:TextBox runat="server" ID="txtApelido" MaxLength="255" Width="300px" CssClass="Field"
                                    onfocus="javascript:SetarApelido();" onblur="javascript:SetarApelido();" />
                                <span class="Validators">*</span>
                                <asp:RequiredFieldValidator ID="rfvApelido" runat="server" ControlToValidate="txtApelido"
                                    CssClass="Validators" Display="Dynamic" ErrorMessage="Campo Requerido" SetFocusOnError="True"
                                    ValidationGroup="vgView" />
                            </p>
                            <p style="vertical-align: middle;">
                                <asp:Label ID="Label7" runat="server" Text="CPF:" AssociatedControlID="txtCpf" CssClass="Label" />
                                <asp:TextBox runat="server" ID="txtCpf" MaxLength="15" CssClass="Field" />
                                <ajaxToolkit:FilteredTextBoxExtender ID="txtCpf_FilteredTextBoxExtender" runat="server"
                                    TargetControlID="txtCpf" FilterType="Numbers" />
                            </p>
                            <p>
                                <asp:Label ID="lblData_nascimento" runat="server" Text="Data de nascimento:" AssociatedControlID="txtData_nascimento"
                                    CssClass="Label" />
                                <asp:TextBox runat="server" ID="txtData_nascimento" MaxLength="8" Width="70px" CssClass="Field" />
                                <ajaxToolkit:MaskedEditExtender ID="txtData_nascimento_MaskedEditExtender" runat="server"
                                    TargetControlID="txtData_nascimento" ClearMaskOnLostFocus="true" MaskType="Date"
                                    Mask="99/99/9999" UserDateFormat="DayMonthYear" />
                            </p>
                        </asp:Panel>
                        <p style="display: block; width: 100%;">
                            <asp:Label ID="Label1" runat="server" Text="Situação:" AssociatedControlID="ddlSituacao"
                                CssClass="Label" />
                            <asp:DropDownList ID="ddlSituacao" runat="server" CssClass="Field">
                                <asp:ListItem Selected="True" Value="1">Ativo</asp:ListItem>
                                <asp:ListItem Value="0">Inativo</asp:ListItem>
                            </asp:DropDownList>
                        </p>
                        <div style="display: block; width: 100%;">
                            <asp:Label ID="Label11" runat="server" Text="Foto/Logo:" AssociatedControlID="afuArquivoLogoFoto"
                                CssClass="Label" Style="float: left;" />
                            <ajaxToolkit:AsyncFileUpload ID="afuArquivoLogoFoto" runat="server" OnUploadedComplete="afuArquivoLogoFoto_UploadedComplete"
                                Style="display: inline-block; float: left; margin-left: 2px;" />
                        </div>
                    </div>
                </fieldset>
                <fieldset class="Fieldset">
                    <legend>..:: Contatos</legend>
                    <div>
                        <asp:GridView ID="gvPessoaContato" runat="server" EmptyDataText="Não há informações cadastradas."
                            CssClass="GridView" AutoGenerateColumns="false" Width="100%" DataKeyNames="ID_PESSOA_CONTATO"
                            OnSelectedIndexChanged="gvPessoaContato_SelectedIndexChanged" OnRowCommand="gvPessoaContato_RowCommand">
                            <Columns>
                                <asp:BoundField DataField="NOME_CONTATO" HeaderText="Nome" />
                                <asp:BoundField DataField="TELEFONE" HeaderText="Telefone" />
                                <asp:BoundField DataField="E_MAIL" HeaderText="E-Mail" />
                                <asp:BoundField DataField="SITE" HeaderText="Site" />
                                <asp:CommandField ButtonType="Image" SelectImageUrl="~/Admin/Media/Images/GridViews/edit.png"
                                    ShowSelectButton="True" ItemStyle-CssClass="gvCommandFieldItem" />
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnExcluir" runat="server" CausesValidation="False" CommandName="Excluir"
                                            OnClientClick="javascript:return confirm('Deseja excluir este registro?');" ImageUrl="~/Admin/Media/Images/GridViews/delete.png"
                                            CommandArgument='<%# Bind("ID_PESSOA_CONTATO") %>' ToolTip="Excluir" />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="gvCommandFieldItem" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="Fields" style="margin-top: 10px;">
                        <p>
                            <asp:Label ID="lblNome_contato" runat="server" Text="Contato:" AssociatedControlID="txtNome_contato"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtNome_contato" MaxLength="255" Width="200px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblTelefone" runat="server" Text="Telefone:" AssociatedControlID="txtTelefone"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtTelefone" MaxLength="30" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblE_mail" runat="server" Text="E-Mail:" AssociatedControlID="txtE_mail"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtE_mail" MaxLength="255" Width="200px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblSite" runat="server" Text="Site:" AssociatedControlID="txtSite"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtSite" MaxLength="255" Width="200px" CssClass="Field" />
                        </p>
                    </div>
                    <uc2:ucButtons ID="ucButtonsPessoaContato" runat="server" />
                    <uc1:ucShowException ID="ucShowExceptionPessoaContato" runat="server" />
                </fieldset>
                <asp:Panel ID="pnlArquivoAtual" runat="server" Visible="false">
                    <fieldset class="Fieldset">
                        <legend>..:: Foto/Logo</legend>
                        <div>
                            <asp:Image ID="imgFotoLogo" runat="server" ImageUrl="~/ShowFile.aspx?action=1" />
                        </div>
                    </fieldset>
                </asp:Panel>
            </asp:Panel>
            <uc2:ucButtons ID="ucButtons1" runat="server" />
            <uc1:ucShowException ID="ucShowException1" runat="server" />
            <asp:Panel ID="pnlViewCollection" runat="server">
                <div class="Fields">
                    <p>
                        <asp:Label ID="Label5" runat="server" Text="Situação:" AssociatedControlID="ddlFiltroSituacao"
                            CssClass="LabelFilters" Width="150px" />
                        <asp:DropDownList ID="ddlFiltroSituacao" runat="server" CssClass="Field">
                            <asp:ListItem Selected="True" Value="1">Ativo</asp:ListItem>
                            <asp:ListItem Value="2">Inativo</asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    <p>
                        <asp:Label ID="Label8" runat="server" Text="Código:" AssociatedControlID="txtFiltroMatricula"
                            CssClass="LabelFilters" Width="150px" />
                        <asp:TextBox runat="server" ID="txtFiltroMatricula" MaxLength="255" CssClass="Field"
                            Width="100px" />
                    </p>
                    <p>
                        <asp:Label ID="Label9" runat="server" Text="Nome/Razão Social:" AssociatedControlID="txtFiltroNomeRazao"
                            CssClass="LabelFilters" Width="150px" />
                        <asp:TextBox runat="server" ID="txtFiltroNomeRazao" MaxLength="255" CssClass="Field"
                            Width="300px" />
                        <asp:ImageButton ID="btnFiltrar" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/botao_pesquisar_1.png"
                            OnClick="btnFiltrar_Click" onmouseover="javascript:ChangeButtonsImage('pesquisar', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('pesquisar', 'out', this);" Style="margin-left: 50px;" />
                    </p>
                </div>
                <asp:GridView runat="server" ID="gvView" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="ID_PESSOA" DataSourceID="edsView" Width="100%" CssClass="GridView"
                    OnRowCommand="gvView_RowCommand" CellPadding="4" GridLines="None" OnRowDataBound="gvView_RowDataBound"
                    OnSelectedIndexChanged="gvView_SelectedIndexChanged">
                    <AlternatingRowStyle CssClass="gvAlternatingRowStyle" />
                    <Columns>
                        <asp:BoundField DataField="ID_PESSOA" HeaderText="Identificador" ReadOnly="True"
                            SortExpression="ID_PESSOA" ItemStyle-CssClass="gvCenter" />
                        <asp:BoundField DataField="MATRICULA" HeaderText="Código" ReadOnly="True" SortExpression="MATRICULA"
                            ItemStyle-CssClass="gvCenter" />
                        <asp:TemplateField HeaderText="Nome / Razão Social" SortExpression="NOME">
                            <ItemTemplate>
                                <asp:Literal ID="ltrNomeRazao" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Situação" SortExpression="ID_PESSOA_SITUACAO">
                            <ItemTemplate>
                                <asp:Literal ID="ltrSituacao" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEditar" runat="server" CausesValidation="False" CommandName="Select"
                                    ImageUrl="~/Admin/Media/Images/GridViews/edit.png" Text="Select" ToolTip="Exibir/Editar" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnExcluir" runat="server" CausesValidation="False" CommandName="Excluir"
                                    OnClientClick="javascript:return confirm('Deseja excluir este registro?');" ImageUrl="~/Admin/Media/Images/GridViews/delete.png"
                                    CommandArgument='<%# Bind("ID_PESSOA") %>' ToolTip="Excluir" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle CssClass="EditRowStyle" />
                    <HeaderStyle CssClass="gvHeaderStyle" />
                    <PagerSettings Position="TopAndBottom" />
                    <PagerStyle CssClass="gvPagerStyle" />
                    <PagerTemplate>
                        <asp:ImageButton ID="btnGvFirst" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/first.png"
                            CommandName="FirstPage" ToolTip="Primeira Página" />
                        <asp:ImageButton ID="btnGvBack" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/back.png"
                            CommandName="BackPage" ToolTip="Página Anterior" />
                        <asp:TextBox ID="txtGvPage" runat="server" Text="<%# gvView.PageIndex + 1 %>" MaxLength="4"
                            Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:Label ID="ltrPager" Text="de" runat="server" CssClass="gvPagerLiteral" EnableTheming="false" />
                        <asp:TextBox ID="txtGvPages" runat="server" Text="<%# gvView.PageCount %>" Enabled="false"
                            ReadOnly="true" Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:ImageButton ID="btnGvGo" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/botao_ir_1.png"
                            CommandName="GoToPage" ToolTip="Ir para a página" onmouseover="javascript:ChangeButtonsImage('gotopage', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('gotopage', 'out', this);" />
                        <asp:ImageButton ID="btnGvNext" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/next.png"
                            CommandName="NextPage" ToolTip="Próxima Página" />
                        <asp:ImageButton ID="btnGvLast" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/last.png"
                            CommandName="LastPage" ToolTip="Última Página" />
                    </PagerTemplate>
                    <RowStyle CssClass="gvRowStyle" />
                    <SelectedRowStyle CssClass="gvSelectedRowStyle" />
                    <SortedAscendingCellStyle CssClass="gvSortedAscendingCellStyle" />
                    <SortedAscendingHeaderStyle CssClass="gvSortedAscendingHeaderStyle" />
                    <SortedDescendingCellStyle CssClass="gvSortedDescendingCellStyle" />
                    <SortedDescendingHeaderStyle CssClass="gvSortedDescendingHeaderStyle" />
                </asp:GridView>
                <asp:EntityDataSource ID="edsView" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                    DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="PESSOA"
                    EntityTypeFilter="PESSOA" OnQueryCreated="edsView_QueryCreated" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
