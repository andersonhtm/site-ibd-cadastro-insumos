﻿<%@ Page Title=":: MasterPainel - HTM Gestão e Tecnologia ::" Language="C#" MasterPageFile="~/Admin/Admin.Master"
    AutoEventWireup="true" CodeBehind="DownloadView.aspx.cs" Inherits="HTMSiteAdmin.Web.Admin.DownloadView" %>

<%@ Register Src="UserControls/ucShowException.ascx" TagName="ucShowException" TagPrefix="uc1" %>
<%@ Register Src="UserControls/Bars/ucButtons.ascx" TagName="ucButtons" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upView" runat="server">
        <ContentTemplate>
            <h1>
                ..:: Arquivo
            </h1>
            <asp:Panel ID="pnlView" runat="server">
                <fieldset class="Fieldset">
                    <legend>:: Dados Básicos</legend>
                    <div class="Fields">
                        <p>
                            <asp:Label ID="lblNome" runat="server" Text="Descrição:" AssociatedControlID="txtDescricao"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtDescricao" MaxLength="500" Width="300px" CssClass="Field" />
                            <span class="Validators">*</span>
                            <asp:RequiredFieldValidator ID="rfvDescricao" runat="server" ControlToValidate="txtDescricao"
                                CssClass="Validators" Display="Dynamic" ErrorMessage="Campo Requerido" ValidationGroup="vgView"></asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <asp:Label ID="Label1" runat="server" Text="Descrição (Inglês):" AssociatedControlID="txtDescricao_en"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtDescricao_en" MaxLength="500" Width="300px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="Label11" runat="server" Text="Ordem:" AssociatedControlID="txtOrdem"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtOrdem" MaxLength="4" Width="75px" CssClass="Field" />
                            <ajaxToolkit:FilteredTextBoxExtender ID="txtOrdem_FilteredTextBoxExtender" runat="server"
                                FilterType="Numbers" TargetControlID="txtOrdem">
                            </ajaxToolkit:FilteredTextBoxExtender>
                        </p>
                        <p>
                            <asp:Label ID="Label8" runat="server" Text="Tipo:" AssociatedControlID="ddlId_download_tipo"
                                CssClass="Label" />
                            <asp:DropDownList ID="ddlId_download_tipo" runat="server" DataSourceID="edsDownloadTipo"
                                DataTextField="DESCRICAO" DataValueField="ID_DOWNLOAD_TIPO" CssClass="Field"
                                OnDataBound="ddlId_download_tipo_DataBound" />
                            <span class="Validators">*</span>
                            <asp:CompareValidator ID="cvId_download_tipo" ErrorMessage="Campo Requerido" ControlToValidate="ddlId_download_tipo"
                                runat="server" CssClass="Validators" Display="Dynamic" Operator="GreaterThan"
                                Type="Integer" ValidationGroup="vgView" ValueToCompare="0" />
                            <asp:EntityDataSource ID="edsDownloadTipo" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                                DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="DOWNLOAD_TIPO"
                                EntityTypeFilter="DOWNLOAD_TIPO" Select="it.[ID_DOWNLOAD_TIPO], it.[DESCRICAO]">
                            </asp:EntityDataSource>
                        </p>
                        <p>
                            <asp:Label ID="Label3" runat="server" Text="Categoria:" AssociatedControlID="ddlId_download_categoria"
                                CssClass="Label" />
                            <asp:DropDownList ID="ddlId_download_categoria" runat="server" DataSourceID="edsDownloadCategoria"
                                DataTextField="DESCRICAO" DataValueField="ID_DOWNLOAD_CATEGORIA" OnDataBound="ddlId_download_categoria_DataBound"
                                CssClass="Field" />
                            <asp:EntityDataSource ID="edsDownloadCategoria" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                                DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="DOWNLOAD_CATEGORIA"
                                EntityTypeFilter="DOWNLOAD_CATEGORIA">
                            </asp:EntityDataSource>
                        </p>
                        <p>
                            <asp:Label ID="Label4" runat="server" Text="Detalhes:" AssociatedControlID="txtSubDescricao"
                                CssClass="LabelTop" />
                            <asp:TextBox runat="server" ID="txtSubDescricao" MaxLength="1000" Width="300px" CssClass="Field"
                                Rows="3" TextMode="MultiLine" />
                        </p>
                        <p>
                            <asp:Label ID="Label5" runat="server" Text="Detalhes (Inglês):" AssociatedControlID="txtSubDescricao_en"
                                CssClass="LabelTop" />
                            <asp:TextBox runat="server" ID="txtSubDescricao_en" MaxLength="1000" Width="300px"
                                CssClass="Field" Rows="3" TextMode="MultiLine" />
                        </p>
                        <p>
                            <asp:Label ID="Label7" runat="server" Text="Situação:" AssociatedControlID="chkAtivo"
                                CssClass="Label" />
                            <asp:CheckBox Text="Ativo" runat="server" ID="chkAtivo" Checked="true" />
                        </p>
                    </div>
                    <div class="Fields">
                        <asp:Label ID="Label9" runat="server" Text="Arquivo:" AssociatedControlID="afuArquivoEvento"
                            CssClass="Label" Style="float: left;" />
                        <ajaxToolkit:AsyncFileUpload ID="afuArquivoEvento" runat="server" Style="display: inline-block;
                            float: left; margin-left: 2px;" OnUploadedComplete="afuArquivoEvento_UploadedComplete" />
                    </div>
                </fieldset>
                <asp:Panel ID="pnlArquivoAtual" runat="server" Visible="false">
                    <fieldset class="Fieldset">
                        <legend>..:: Arquivo Atual</legend>
                        <div>
                            <asp:Image ID="imgMiniaturaAtual" runat="server" ImageUrl="~/ShowFile.aspx?action=1" />
                            <asp:ImageButton ID="btnDownloadAtual" ImageUrl="~/Admin/Media/Images/arquivo_geral.png"
                                runat="server" />
                        </div>
                    </fieldset>
                </asp:Panel>
            </asp:Panel>
            <uc2:ucButtons ID="ucButtons1" runat="server" />
            <asp:Panel ID="pnlViewCollection" runat="server">
                <div class="Fields">
                    <p>
                        <asp:Label ID="Label10" runat="server" Text="Tipo:" AssociatedControlID="ddlFiltroTipo"
                            CssClass="LabelFilters" />
                        <asp:DropDownList ID="ddlFiltroTipo" runat="server" DataSourceID="edsDownloadTipo"
                            DataTextField="DESCRICAO" DataValueField="ID_DOWNLOAD_TIPO" CssClass="Field"
                            OnDataBound="ddlFiltroTipo_DataBound" />
                        <asp:EntityDataSource ID="edsFiltroTipo" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                            DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="DOWNLOAD_TIPO"
                            EntityTypeFilter="DOWNLOAD_TIPO" Select="it.[ID_DOWNLOAD_TIPO], it.[DESCRICAO]">
                        </asp:EntityDataSource>
                    </p>
                    <p>
                        <asp:Label ID="Label6" runat="server" Text="Categoria:" AssociatedControlID="ddlFiltroCategoria"
                            CssClass="LabelFilters" />
                        <asp:DropDownList ID="ddlFiltroCategoria" runat="server" DataSourceID="edsFiltroCategoria"
                            DataTextField="DESCRICAO" DataValueField="ID_DOWNLOAD_CATEGORIA" OnDataBound="ddlFiltroCategoria_DataBound"
                            CssClass="Field" />
                        <asp:EntityDataSource ID="edsFiltroCategoria" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                            DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="DOWNLOAD_CATEGORIA"
                            EntityTypeFilter="DOWNLOAD_CATEGORIA">
                        </asp:EntityDataSource>
                    </p>
                    <p>
                        <asp:Label ID="Label2" runat="server" Text="Descrição:" AssociatedControlID="txtFiltroDescricao"
                            CssClass="LabelFilters" />
                        <asp:TextBox runat="server" ID="txtFiltroDescricao" MaxLength="255" Width="250px"
                            CssClass="Field" />
                        <asp:ImageButton ID="btnFiltrar" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/botao_pesquisar_1.png"
                            OnClick="btnFiltrar_Click" onmouseover="javascript:ChangeButtonsImage('pesquisar', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('pesquisar', 'out', this);" Style="margin-left: 50px;" />
                    </p>
                </div>
                <asp:GridView runat="server" ID="gvView" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="ID_DOWNLOAD" DataSourceID="edsView" Width="100%" CssClass="GridView"
                    OnRowCommand="gvView_RowCommand" CellPadding="4" GridLines="None" OnRowDataBound="gvView_RowDataBound"
                    OnSelectedIndexChanged="gvView_SelectedIndexChanged">
                    <AlternatingRowStyle CssClass="gvAlternatingRowStyle" />
                    <Columns>
                        <asp:BoundField DataField="ID_DOWNLOAD" HeaderText="Código" ReadOnly="True" SortExpression="ID_DOWNLOAD"
                            ItemStyle-CssClass="gvCenter" ItemStyle-Width="40px" />
                        <asp:TemplateField HeaderText="Arquivo" SortExpression="DESCRICAO">
                            <ItemTemplate>
                                <asp:Image ID="imgMiniatura" runat="server" ImageUrl="~/ShowFile.aspx?action=1" />
                                <asp:ImageButton ID="btnDownload" ImageUrl="~/Admin/Media/Images/arquivo_geral.png"
                                    runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCRICAO" HeaderText="Descrição" SortExpression="DESCRICAO"
                            ItemStyle-CssClass="gvCenter" ItemStyle-Width="250px" />
                        <asp:TemplateField HeaderText="Link">
                            <ItemTemplate>
                                <asp:Literal ID="ltrLink" runat="server" />
                            </ItemTemplate>
                            <HeaderStyle CssClass="gvHeaderStyle" />
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="ORDEM" HeaderText="Ordem" SortExpression="ORDEM" ItemStyle-CssClass="gvCenter"
                            ItemStyle-Width="75px" />
                        <asp:TemplateField HeaderText="Usuário">
                            <ItemTemplate>
                                <asp:Literal ID="ltrUsuario" runat="server" />
                            </ItemTemplate>
                            <HeaderStyle CssClass="gvHeaderStyle" />
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Data (Última Alteração)">
                            <ItemTemplate>
                                <asp:Literal ID="ltrData" runat="server" />
                            </ItemTemplate>
                            <HeaderStyle CssClass="gvHeaderStyle" />
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEditar" runat="server" CausesValidation="False" CommandName="Select"
                                    ImageUrl="~/Admin/Media/Images/GridViews/edit.png" Text="Select" ToolTip="Exibir/Editar" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnExcluir" runat="server" CausesValidation="False" CommandName="Excluir"
                                    OnClientClick="javascript:return confirm('Deseja excluir este registro?');" ImageUrl="~/Admin/Media/Images/GridViews/delete.png"
                                    CommandArgument='<%# Bind("ID_DOWNLOAD") %>' ToolTip="Excluir" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle CssClass="EditRowStyle" />
                    <HeaderStyle CssClass="gvHeaderStyle" />
                    <PagerSettings Position="TopAndBottom" />
                    <PagerStyle CssClass="gvPagerStyle" />
                    <PagerTemplate>
                        <asp:ImageButton ID="btnGvFirst" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/first.png"
                            CommandName="FirstPage" ToolTip="Primeira Página" />
                        <asp:ImageButton ID="btnGvBack" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/back.png"
                            CommandName="BackPage" ToolTip="Página Anterior" />
                        <asp:TextBox ID="txtGvPage" runat="server" Text="<%# gvView.PageIndex + 1 %>" MaxLength="4"
                            Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:Label ID="ltrPager" Text="de" runat="server" CssClass="gvPagerLiteral" EnableTheming="false" />
                        <asp:TextBox ID="txtGvPages" runat="server" Text="<%# gvView.PageCount %>" Enabled="false"
                            ReadOnly="true" Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:ImageButton ID="btnGvGo" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/botao_ir_1.png"
                            CommandName="GoToPage" ToolTip="Ir para a página" onmouseover="javascript:ChangeButtonsImage('gotopage', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('gotopage', 'out', this);" />
                        <asp:ImageButton ID="btnGvNext" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/next.png"
                            CommandName="NextPage" ToolTip="Próxima Página" />
                        <asp:ImageButton ID="btnGvLast" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/last.png"
                            CommandName="LastPage" ToolTip="Última Página" />
                    </PagerTemplate>
                    <RowStyle CssClass="gvRowStyle" />
                    <SelectedRowStyle CssClass="gvSelectedRowStyle" />
                    <SortedAscendingCellStyle CssClass="gvSortedAscendingCellStyle" />
                    <SortedAscendingHeaderStyle CssClass="gvSortedAscendingHeaderStyle" />
                    <SortedDescendingCellStyle CssClass="gvSortedDescendingCellStyle" />
                    <SortedDescendingHeaderStyle CssClass="gvSortedDescendingHeaderStyle" />
                </asp:GridView>
                <asp:EntityDataSource ID="edsView" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                    DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="DOWNLOAD"
                    EntityTypeFilter="DOWNLOAD" OnQueryCreated="edsView_QueryCreated" />
            </asp:Panel>
            <uc1:ucShowException ID="ucShowException1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
