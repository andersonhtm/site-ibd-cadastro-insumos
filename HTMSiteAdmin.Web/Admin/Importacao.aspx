﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Importacao.aspx.cs" Inherits="HTMSiteAdmin.Web.Admin.Importacao" %>
<%@ Register src="UserControls/ucShowException.ascx" tagname="ucShowException" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        ..:: Importação 
    </h1>
    <asp:Panel ID="pnlView" runat="server">
        <fieldset class="Fieldset">
            <legend>:: Arquivo</legend>
            <div style="display: block; width: 100%">
                <p>
                    <asp:Label ID="Label9" runat="server" Text="Produtos:" AssociatedControlID="fupProdutos"
                        CssClass="Label" Style="float: left;" />
                    <asp:FileUpload runat="server" ID="fupProdutos" Width="400px" />
                    <asp:RequiredFieldValidator ID="rfvfupProdutos" ControlToValidate="fupProdutos" 
                        SetFocusOnError="true" runat="server" ErrorMessage="*" />
                 </p>
                <p>
                    <asp:Label ID="Label1" runat="server" Text="Clientes:" AssociatedControlID="fupClientes"
                        CssClass="Label" Style="float: left;" />
                    <asp:FileUpload runat="server" ID="fupClientes" Width="400px" />
                    <asp:RequiredFieldValidator ID="rfvfupClientes" ControlToValidate="fupClientes" 
                        SetFocusOnError="true" runat="server" ErrorMessage="*" />
                 </p>
                 <p>
                    <asp:Label ID="Label2" runat="server" Text="Certificados:" AssociatedControlID="fupCertificados"
                        CssClass="Label" Style="float: left;" />
                    <asp:FileUpload runat="server" ID="fupCertificados" Width="400px" />
                    <asp:RequiredFieldValidator ID="rfvfupCertificados" ControlToValidate="fupCertificados" 
                        SetFocusOnError="true" runat="server" ErrorMessage="*" />
                 </p>
                 <p>
                    <asp:Label ID="Label3" runat="server" Text="Contatos de Clientes:" AssociatedControlID="fupCertificados"
                        CssClass="Label" Style="float: left;" />
                    <asp:FileUpload runat="server" ID="fupContatoCliente" Width="400px" />
                    <asp:RequiredFieldValidator ID="rfvfupContatoCliente" ControlToValidate="fupContatoCliente" 
                        SetFocusOnError="true" runat="server" ErrorMessage="*" />
                 </p>
                 <p>
                    <asp:Label ID="Label4" runat="server" Text="Cliente x Produto x Certificado:" AssociatedControlID="fupClienteProdutoCertificado"
                        CssClass="Label" Style="float: left;" />
                    <asp:FileUpload runat="server" ID="fupClienteProdutoCertificado" Width="400px" />
                    <asp:RequiredFieldValidator ID="rfvfupClienteProdutoCertificado" ControlToValidate="fupCertificados" 
                        SetFocusOnError="true" runat="server" ErrorMessage="*" />
                 </p>
            </div>
            <div class="Fields">
                <p>
                    <asp:ImageButton ImageUrl="~/Admin/Media/Images/botao_importar_1_off.png" runat="server"
                        ID="btnProcessar" onmouseover="javascript:ChangeButtonsImage('botao_importar_1', 'over', this);"
                        onmouseout="javascript:ChangeButtonsImage('botao_importar_1', 'out', this);" 
                        onclick="btnProcessar_Click" />
                </p>
                <uc1:ucShowException ID="mensagem" runat="server" />
            </div>
        </fieldset>
    </asp:Panel>
</asp:Content>
