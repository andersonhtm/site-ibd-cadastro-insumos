﻿<%@ Page Title=":: MasterPainel - HTM Gestão e Tecnologia ::" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true"
    CodeBehind="BannerPosicaoView.aspx.cs" Inherits="HTMSiteAdmin.Web.Admin.BannerPosicaoView" %>

<%@ Register Src="UserControls/ucShowException.ascx" TagName="ucShowException" TagPrefix="uc1" %>
<%@ Register Src="UserControls/Bars/ucButtons.ascx" TagName="ucButtons" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upView" runat="server">
        <ContentTemplate>
            <h1>
                ..:: Posições para Banners
            </h1>
            <asp:Panel ID="pnlView" runat="server">
                <fieldset class="Fieldset">
                    <legend>:: Dados Básicos</legend>
                    <div class="Fields">
                        <p>
                            <asp:Label ID="lblNome" runat="server" Text="Descriçao:" AssociatedControlID="txtNome"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtNome" MaxLength="100" Width="300px" CssClass="Field" />
                            <span class="Validators">*</span>
                            <asp:RequiredFieldValidator ID="rfvNome" runat="server" ControlToValidate="txtNome"
                                ErrorMessage="Campo Requerido" Display="Dynamic" SetFocusOnError="True" CssClass="Validators"
                                ValidationGroup="vgView" />
                        </p>
                        <p>
                            <asp:Label ID="lblLargura_maxima" runat="server" Text="Largura:" AssociatedControlID="txtLargura_maxima"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtLargura_maxima" MaxLength="4" Width="50px" CssClass="Field" />
                            <ajaxToolkit:FilteredTextBoxExtender ID="txtLargura_maxima_FilteredTextBoxExtender"
                                runat="server" FilterType="Numbers" TargetControlID="txtLargura_maxima">
                            </ajaxToolkit:FilteredTextBoxExtender>
                        </p>
                        <p>
                            <asp:Label ID="lblAltura_maxima" runat="server" Text="Altura:" AssociatedControlID="txtAltura_maxima"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtAltura_maxima" MaxLength="4" Width="50px" CssClass="Field" />
                            <ajaxToolkit:FilteredTextBoxExtender ID="txtAltura_maxima_FilteredTextBoxExtender"
                                runat="server" FilterType="Numbers" TargetControlID="txtAltura_maxima">
                            </ajaxToolkit:FilteredTextBoxExtender>
                        </p>
                        <p>
                            <asp:Label ID="lblResource_string_name" runat="server" Text="Chave de Tradução:"
                                AssociatedControlID="txtResource_string_name" CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtResource_string_name" MaxLength="255" Width="300px"
                                CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="Label3" runat="server" Text="Primeiro Ativo:" AssociatedControlID="chkApenas_um_ativo"
                                CssClass="Label" />
                            <asp:CheckBox ID="chkApenas_um_ativo" Text="Carregar somente o primeiro ativo" runat="server" />
                        </p>
                        <p>
                            <asp:Label ID="Label1" runat="server" Text="Situação:" AssociatedControlID="ddlSituacao"
                                CssClass="Label" />
                            <asp:DropDownList ID="ddlSituacao" runat="server" CssClass="Field">
                                <asp:ListItem Selected="True" Value="1">Ativo</asp:ListItem>
                                <asp:ListItem Value="2">Inativo</asp:ListItem>
                            </asp:DropDownList>
                        </p>
                        <p>
                            <asp:Label ID="lblDescricao" runat="server" Text="Observação:" AssociatedControlID="txtDescricao"
                                CssClass="Label" Style="vertical-align: top;" />
                            <asp:TextBox runat="server" ID="txtDescricao" MaxLength="255" Width="300px" Rows="3"
                                TextMode="MultiLine" CssClass="Field" />
                        </p>
                    </div>
                </fieldset>
            </asp:Panel>
            <uc2:ucButtons ID="ucButtons1" runat="server" />
            <uc1:ucShowException ID="ucShowException1" runat="server" />
            <asp:Panel ID="pnlViewCollection" runat="server">
                <div class="Fields">
                    <p>
                        <asp:Label ID="Label2" runat="server" Text="Descrição:" AssociatedControlID="txtFiltroDescricao"
                            CssClass="LabelFilters" />
                        <asp:TextBox runat="server" ID="txtFiltroDescricao" MaxLength="255" Width="250px"
                            CssClass="Field" />
                        <asp:ImageButton ID="btnFiltrar" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/botao_pesquisar_1.png"
                            OnClick="btnFiltrar_Click" onmouseover="javascript:ChangeButtonsImage('pesquisar', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('pesquisar', 'out', this);" Style="margin-left: 50px;" />
                    </p>
                </div>
                <asp:GridView runat="server" ID="gvView" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="ID_BANNER_POSICAO" DataSourceID="edsView" Width="100%" CssClass="GridView"
                    OnRowCommand="gvView_RowCommand" CellPadding="4" GridLines="None" OnRowDataBound="gvView_RowDataBound"
                    OnSelectedIndexChanged="gvView_SelectedIndexChanged">
                    <AlternatingRowStyle CssClass="gvAlternatingRowStyle" />
                    <Columns>
                        <asp:BoundField DataField="ID_BANNER_POSICAO" HeaderText="Código" ReadOnly="True"
                            SortExpression="ID_BANNER_POSICAO" ItemStyle-CssClass="gvCenter" />
                        <asp:BoundField DataField="NOME" HeaderText="Descrição" SortExpression="NOME" ItemStyle-CssClass="gvCenter" />
                        <asp:BoundField DataField="LARGURA_MAXIMA" HeaderText="Largura" SortExpression="LARGURA_MAXIMA"
                            ItemStyle-CssClass="gvCenter" />
                        <asp:BoundField DataField="ALTURA_MAXIMA" HeaderText="Altura" SortExpression="ALTURA_MAXIMA"
                            ItemStyle-CssClass="gvCenter" />
                        <asp:TemplateField HeaderText="Situação" SortExpression="ID_BANNER_POSICAO_SITUACAO">
                            <ItemTemplate>
                                <asp:Literal ID="ltrSituacao" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEditar" runat="server" CausesValidation="False" CommandName="Select"
                                    ImageUrl="~/Admin/Media/Images/GridViews/edit.png" Text="Select" ToolTip="Exibir/Editar" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnExcluir" runat="server" CausesValidation="False" CommandName="Excluir"
                                    OnClientClick="javascript:return confirm('Deseja excluir este registro?');" ImageUrl="~/Admin/Media/Images/GridViews/delete.png"
                                    CommandArgument='<%# Bind("ID_BANNER_POSICAO") %>' ToolTip="Excluir" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle CssClass="EditRowStyle" />
                    <HeaderStyle CssClass="gvHeaderStyle" />
                    <PagerSettings Position="TopAndBottom" />
                    <PagerStyle CssClass="gvPagerStyle" />
                    <PagerTemplate>
                        <asp:ImageButton ID="btnGvFirst" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/first.png"
                            CommandName="FirstPage" ToolTip="Primeira Página" />
                        <asp:ImageButton ID="btnGvBack" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/back.png"
                            CommandName="BackPage" ToolTip="Página Anterior" />
                        <asp:TextBox ID="txtGvPage" runat="server" Text="<%# gvView.PageIndex + 1 %>" MaxLength="4"
                            Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:Label ID="ltrPager" Text="de" runat="server" CssClass="gvPagerLiteral" EnableTheming="false" />
                        <asp:TextBox ID="txtGvPages" runat="server" Text="<%# gvView.PageCount %>" Enabled="false"
                            ReadOnly="true" Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:ImageButton ID="btnGvGo" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/botao_ir_1.png"
                            CommandName="GoToPage" ToolTip="Ir para a página" onmouseover="javascript:ChangeButtonsImage('gotopage', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('gotopage', 'out', this);" />
                        <asp:ImageButton ID="btnGvNext" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/next.png"
                            CommandName="NextPage" ToolTip="Próxima Página" />
                        <asp:ImageButton ID="btnGvLast" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/last.png"
                            CommandName="LastPage" ToolTip="Última Página" />
                    </PagerTemplate>
                    <RowStyle CssClass="gvRowStyle" />
                    <SelectedRowStyle CssClass="gvSelectedRowStyle" />
                    <SortedAscendingCellStyle CssClass="gvSortedAscendingCellStyle" />
                    <SortedAscendingHeaderStyle CssClass="gvSortedAscendingHeaderStyle" />
                    <SortedDescendingCellStyle CssClass="gvSortedDescendingCellStyle" />
                    <SortedDescendingHeaderStyle CssClass="gvSortedDescendingHeaderStyle" />
                </asp:GridView>
                <asp:EntityDataSource ID="edsView" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                    DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="BANNER_POSICAO"
                    EntityTypeFilter="BANNER_POSICAO" OnQueryCreated="edsView_QueryCreated" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
