﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Business.Conteudos;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Library.Exceptions;
using HTMSiteAdmin.Business.Sistema;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class ConteudoView : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this, ViewToken, "ViewAspect", value); }
        }
        private CONTEUDO ObjCarregado
        {
            get { return (CONTEUDO)Global.RestoreObject(this, ViewToken, "ObjCarregado"); }
            set { Global.SaveObject(this, ViewToken, "ObjCarregado", value); }
        }
        public UploadedFile ArquivoEnviado
        {
            get { return (UploadedFile)Global.RestoreObject(this, ViewToken, "ArquivoEnviado"); }
            set { Global.SaveObject(this, ViewToken, "ArquivoEnviado", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MapButtons();

            if (!IsPostBack && ObjCarregado == null)
                ViewAspect = enmUserInterfaceAspect.Start;

            if (!IsPostBack) UserInterfaceAspectChanged();
        }

        public void ValidateAcess() { throw new NotImplementedException(); }

        public void MapButtons()
        {
            ucButtons1.ButtonNew.CausesValidation = false;
            ucButtons1.ButtonNew.Click += new ImageClickEventHandler(ButtonNew_Click);

            ucButtons1.ButtonSave.ValidationGroup = "vgView";
            ucButtons1.ButtonSave.CausesValidation = true;
            ucButtons1.ButtonSave.Click += new ImageClickEventHandler(ButtonSave_Click);

            ucButtons1.ButtonSaveNew.ValidationGroup = "vgView";
            ucButtons1.ButtonSaveNew.CausesValidation = true;
            ucButtons1.ButtonSaveNew.Click += new ImageClickEventHandler(ButtonSaveNew_Click);

            ucButtons1.ButtonCancel.CausesValidation = false;
            ucButtons1.ButtonCancel.Click += new ImageClickEventHandler(ButtonCancel_Click);

            ucButtons1.ButtonRefresh.CausesValidation = false;
            ucButtons1.ButtonRefresh.Click += new ImageClickEventHandler(ButtonRefresh_Click);

            ucButtons1.ButtonDelete.CausesValidation = false;
            ucButtons1.ButtonDelete.Click += new ImageClickEventHandler(ButtonDelete_Click);
        }

        public void UserInterfaceAspectChanged()
        {
            StartPageControls();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    ucButtons1.ButtonNew.Visible = true;
                    ucButtons1.ButtonSave.Visible = false;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = false;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = false;

                    int currentPageIndex = gvView.PageIndex;
                    gvView.DataBind();
                    gvView.PageIndex = currentPageIndex;

                    pnlViewCollection.Visible = gvView.Rows.Count > 0;
                    break;
                case enmUserInterfaceAspect.New:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = true;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    RestartForm();
                    break;
                case enmUserInterfaceAspect.Update:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    ClearFields();

                    SetForm();
                    break;
                default:
                    break;
            }
        }

        public void RestartForm()
        {
            ClearMemory();
            ClearFields();
        }

        public void StartPageControls()
        {
            try
            {
                if (!IsPostBack)
                {
                    ddlId_conteudo_tipo.DataBind();
                    ddlFiltroTipoConteudo.DataBind();
                    ddlCategoriaConteudo.DataBind();
                    ddlFiltroCategoria.DataBind();
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSaveNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void SaveView()
        {
            try
            {
                ConteudoBo _objBo = new ConteudoBo();

                switch (ViewAspect)
                {
                    case enmUserInterfaceAspect.Start:
                        break;
                    case enmUserInterfaceAspect.New:
                        ObjCarregado = new CONTEUDO()
                        {
                            ID_CONTEUDO_TIPO = int.Parse(ddlId_conteudo_tipo.SelectedValue),
                            ID_CONTEUDO_SITUACAO = int.Parse(ddlSituacao.SelectedValue),
                            TITULO = txtTitulo.Text,
                            TITULO_EN = !string.IsNullOrWhiteSpace(txtTitulo_en.Text) ? txtTitulo_en.Text : string.Empty,
                            SUB_TITULO = !string.IsNullOrWhiteSpace(txtSub_titulo.Text) ? txtSub_titulo.Text : string.Empty,
                            SUB_TITULO_EN = !string.IsNullOrWhiteSpace(txtSub_titulo_en.Text) ? txtSub_titulo_en.Text : string.Empty,
                            CONTENT_DATA = txtConteudo.Text,
                            CONTENT_DATA_EN = !string.IsNullOrWhiteSpace(txtConteudo_en.Text) ? txtConteudo_en.Text : string.Empty,
                            FONTE = !string.IsNullOrWhiteSpace(txtFonte.Text) ? txtFonte.Text : string.Empty,
                            OBSERVACAO = !string.IsNullOrWhiteSpace(txtObservacao.Text) ? txtFonte.Text : string.Empty,
                            ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO
                        };

                        FillNullabeValues();

                        _objBo.Add(ObjCarregado);
                        break;
                    case enmUserInterfaceAspect.Update:
                        ObjCarregado = _objBo.Find(lbda => lbda.ID_CONTEUDO == ObjCarregado.ID_CONTEUDO).First();

                        ObjCarregado.ID_CONTEUDO_TIPO = int.Parse(ddlId_conteudo_tipo.SelectedValue);
                        ObjCarregado.ID_CONTEUDO_SITUACAO = int.Parse(ddlSituacao.SelectedValue);
                        ObjCarregado.TITULO = txtTitulo.Text;
                        ObjCarregado.TITULO_EN = !string.IsNullOrWhiteSpace(txtTitulo_en.Text) ? txtTitulo_en.Text : string.Empty;
                        ObjCarregado.SUB_TITULO = !string.IsNullOrWhiteSpace(txtSub_titulo.Text) ? txtSub_titulo.Text : string.Empty;
                        ObjCarregado.SUB_TITULO_EN = !string.IsNullOrWhiteSpace(txtSub_titulo_en.Text) ? txtSub_titulo_en.Text : string.Empty;
                        ObjCarregado.CONTENT_DATA = txtConteudo.Text;
                        ObjCarregado.CONTENT_DATA_EN = !string.IsNullOrWhiteSpace(txtConteudo_en.Text) ? txtConteudo_en.Text : string.Empty;
                        ObjCarregado.FONTE = !string.IsNullOrWhiteSpace(txtFonte.Text) ? txtFonte.Text : string.Empty;
                        ObjCarregado.OBSERVACAO = !string.IsNullOrWhiteSpace(txtObservacao.Text) ? txtFonte.Text : string.Empty;
                        ObjCarregado.ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO;

                        FillNullabeValues();

                        _objBo.Update(ObjCarregado);
                        break;
                    default:
                        break;
                }

                //FillArquivoDigital();

                _objBo.SaveChanges();

                _objBo = null; ObjCarregado = null;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("FK_CONTEUDO_CONTEUDO_CATEGORIA")) throw new HTMSiteAdminException("Uma categoria deve ser selecionada.");
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao salvar conteúdo, entre em contato com o administrador do sistema.");
            }
        }

        private void FillNullabeValues()
        {
            if (!ddlCategoriaConteudo.SelectedValue.Equals("0"))
                ObjCarregado.ID_CONTEUDO_CATEGORIA = int.Parse(ddlCategoriaConteudo.SelectedValue);

            //if (!string.IsNullOrWhiteSpace(txtData_ocorrencia.Text))
            //    ObjCarregado.DATA_OCORRENCIA = DateTime.Parse(txtData_ocorrencia.Text);

            if (!string.IsNullOrWhiteSpace(txtData_publicacao.Text))
                ObjCarregado.DATA_PUBLICACAO = DateTime.Parse(txtData_publicacao.Text);
        }

        private void FillArquivoDigital()
        {
            try
            {
                if (ArquivoEnviado != null)
                {
                    if (ObjCarregado.ID_ARQUIVO_DIGITAL.HasValue)
                    {
                        ObjCarregado.ARQUIVO_DIGITAL.ARQUIVO = ArquivoEnviado.FileBytes;
                        ObjCarregado.ARQUIVO_DIGITAL.NOME = ArquivoEnviado.FileName;
                        ObjCarregado.ARQUIVO_DIGITAL.EXTENSAO = ArquivoEnviado.FileExtension;
                        ObjCarregado.ARQUIVO_DIGITAL.DATA = DateTime.Now;
                    }
                    else
                    {
                        ObjCarregado.ARQUIVO_DIGITAL = new ARQUIVO_DIGITAL()
                        {
                            ID_ARQUIVO = Guid.NewGuid(),
                            NOME = ArquivoEnviado.FileName,
                            EXTENSAO = ArquivoEnviado.FileExtension,
                            ARQUIVO = ArquivoEnviado.FileBytes,
                            DATA = DateTime.Now
                        };
                    }
                }
            }
            catch (Exception ex) { throw; }
        }

        public void ButtonRefresh_Click(object sender, ImageClickEventArgs e)
        {
            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    //Nada fazer
                    break;
                case enmUserInterfaceAspect.New:
                    ClearFields();
                    break;
                case enmUserInterfaceAspect.Update:
                    ClearFields();
                    //TODO: Carregar dados do usuário
                    break;
                default:
                    break;
            }
        }

        public void ButtonCancel_Click(object sender, ImageClickEventArgs e)
        {
            ClearMemory();
            ViewAspect = enmUserInterfaceAspect.Start;
            UserInterfaceAspectChanged();
        }

        public void ButtonDelete_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void GetForm() { throw new NotImplementedException(); }

        public void SetForm()
        {
            try
            {
                txtTitulo.Text = ObjCarregado.TITULO;
                txtTitulo_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.TITULO_EN) ? ObjCarregado.TITULO_EN : string.Empty;
                txtSub_titulo.Text = !string.IsNullOrWhiteSpace(ObjCarregado.SUB_TITULO) ? ObjCarregado.SUB_TITULO : string.Empty;
                txtSub_titulo_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.SUB_TITULO_EN) ? ObjCarregado.SUB_TITULO_EN : string.Empty;

                ddlId_conteudo_tipo.DataBind();
                ddlId_conteudo_tipo.SelectedValue = ObjCarregado.ID_CONTEUDO_TIPO.ToString();

                ddlCategoriaConteudo.DataBind();
                ddlCategoriaConteudo.SelectedValue = (ObjCarregado.ID_CONTEUDO_CATEGORIA.HasValue) ? ObjCarregado.ID_CONTEUDO_CATEGORIA.Value.ToString() : "0";

                txtConteudo.Text = ObjCarregado.CONTENT_DATA;
                txtConteudo_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.CONTENT_DATA_EN) ? ObjCarregado.CONTENT_DATA_EN : string.Empty;
                txtFonte.Text = !string.IsNullOrWhiteSpace(ObjCarregado.FONTE) ? ObjCarregado.FONTE : string.Empty;
                txtObservacao.Text = !string.IsNullOrWhiteSpace(ObjCarregado.OBSERVACAO) ? ObjCarregado.OBSERVACAO : string.Empty;
                //txtData_ocorrencia.Text = ObjCarregado.DATA_OCORRENCIA.HasValue ? ObjCarregado.DATA_OCORRENCIA.Value.ToShortDateString() : string.Empty;
                txtData_publicacao.Text = ObjCarregado.DATA_PUBLICACAO.HasValue ? ObjCarregado.DATA_PUBLICACAO.Value.ToShortDateString() : string.Empty;
                ddlSituacao.SelectedValue = ObjCarregado.ID_CONTEUDO_SITUACAO.ToString();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void EnableFields() { throw new NotImplementedException(); }

        public void DisableFields() { throw new NotImplementedException(); }

        public void ClearFields()
        {
            try
            {
                //Dados Básicos
                txtTitulo.Text = txtTitulo_en.Text = txtSub_titulo.Text = txtSub_titulo_en.Text = txtConteudo.Text = string.Empty;
                txtConteudo_en.Text = txtFonte.Text = txtObservacao.Text = string.Empty;
                //txtData_ocorrencia.Text = 
                    txtData_publicacao.Text = string.Empty;
                ddlId_conteudo_tipo.DataBind();
                ddlId_conteudo_tipo.SelectedIndex = 0;
                ddlCategoriaConteudo.DataBind();
                ddlCategoriaConteudo.SelectedIndex = 0;
                ddlSituacao.SelectedValue = "1";

            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ClearMemory() { ObjCarregado = null; }

        protected void ddlId_conteudo_tipo_DataBound(object sender, EventArgs e)
        {
            try
            {
                HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender);

                IQueryable<CONTEUDO_TIPO> ConteudoTipos = new ConteudoTipoBo().GetAll();

                if (ConteudoTipos != null)
                {
                    foreach (CONTEUDO_TIPO _ConteudoTipo in ConteudoTipos)
                        HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_AddItem(sender, _ConteudoTipo.NOME, _ConteudoTipo.ID_CONTEUDO_TIPO.ToString());
                }

                ConteudoTipos = null;
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void ddlCategoriaConteudo_DataBound(object sender, EventArgs e)
        {
            try
            {
                HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender);

                IQueryable<CONTEUDO_CATEGORIA> ConteudoCategorias = new ConteudoCategoriaBo().GetAll();

                if (ConteudoCategorias != null)
                {
                    foreach (CONTEUDO_CATEGORIA _Item in ConteudoCategorias)
                        HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_AddItem(sender, _Item.NOME, _Item.ID_CONTEUDO_CATEGORIA.ToString());
                }

                ConteudoCategorias = null;
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void ddlCategoriaConteudo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender);
                IQueryable<CONTEUDO_CATEGORIA> ConteudoCategorias = new ConteudoCategoriaBo().GetAll();

                if (ConteudoCategorias != null)
                {
                    foreach (CONTEUDO_CATEGORIA _Item in ConteudoCategorias)
                        HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_AddItem(sender, _Item.NOME, _Item.ID_CONTEUDO_CATEGORIA.ToString());
                }

                ConteudoCategorias = null;
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void btnFiltrar_Click(object sender, ImageClickEventArgs e)
        {
            try { gvView.DataBind(); }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void gvView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((Literal)e.Row.FindControl("ltrTitulo")).Text = (((CONTEUDO)e.Row.DataItem).TITULO.Length > 150) ? ((CONTEUDO)e.Row.DataItem).TITULO.Substring(0, 147) + "..." : ((Literal)e.Row.FindControl("ltrTitulo")).Text = ((CONTEUDO)e.Row.DataItem).TITULO;

                    ((Literal)e.Row.FindControl("ltrTipo")).Text = new ConteudoTipoBo().Find(lbda => lbda.ID_CONTEUDO_TIPO == ((CONTEUDO)e.Row.DataItem).ID_CONTEUDO_TIPO).First().NOME;
                    ((Literal)e.Row.FindControl("ltrCategoria")).Text = new ConteudoCategoriaBo().Find(lbda => lbda.ID_CONTEUDO_CATEGORIA == ((CONTEUDO)e.Row.DataItem).ID_CONTEUDO_CATEGORIA).First().NOME;
                    ((Literal)e.Row.FindControl("ltrSituacao")).Text = new ConteudoSituacaoBo().Find(lbda => lbda.ID_CONTEUDO_SITUACAO == ((CONTEUDO)e.Row.DataItem).ID_CONTEUDO_SITUACAO).First().NOME;
                    ((Literal)e.Row.FindControl("ltrDataPublicacao")).Text = ((CONTEUDO)e.Row.DataItem).DATA_PUBLICACAO.HasValue ? ((CONTEUDO)e.Row.DataItem).DATA_PUBLICACAO.Value.ToShortDateString() : "";
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _SelectedKey = Convert.ToInt32(gvView.SelectedDataKey.Values["ID_CONTEUDO"]);

                ObjCarregado = new ConteudoBo().Find(lbda => lbda.ID_CONTEUDO == _SelectedKey).First();

                ViewAspect = enmUserInterfaceAspect.Update;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int currentPageIndex, _SelectedKey;

                switch (e.CommandName)
                {
                    case "Excluir":
                        _SelectedKey = Convert.ToInt32(e.CommandArgument);

                        ConteudoBo _objBo = new ConteudoBo();
                        _objBo.Delete(_objBo.Find(lbda => lbda.ID_CONTEUDO == _SelectedKey).First());
                        _objBo.SaveChanges();
                        _objBo = null;

                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex >= 0 && currentPageIndex <= gvView.PageCount - 1)
                            gvView.PageIndex = currentPageIndex;
                        break;
                    case "FirstPage":
                        gvView.DataBind();
                        gvView.PageIndex = 0;
                        break;
                    case "BackPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex > 0)
                            gvView.PageIndex = currentPageIndex - 1;
                        break;
                    case "GoToPage":
                        currentPageIndex = gvView.PageIndex;

                        int goToTop = 1, goToBottom = 1, goTo = 1;
                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text))
                            goToTop = int.Parse(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text);

                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text))
                            goToBottom = int.Parse(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text);

                        gvView.DataBind();

                        goTo = (goToTop != currentPageIndex + 1) ? goToTop : goToBottom;

                        if (goTo >= 1 && goTo <= gvView.PageCount)
                            gvView.PageIndex = goTo - 1;
                        break;
                    case "NextPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex < gvView.PageCount)
                            gvView.PageIndex = currentPageIndex + 1;
                        break;
                    case "LastPage":
                        gvView.DataBind();
                        gvView.PageIndex = gvView.PageCount - 1;
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void edsView_QueryCreated(object sender, QueryCreatedEventArgs e)
        {
            try
            {
                var result = e.Query.Cast<CONTEUDO>();

                int _ID_SITUACAO = int.Parse(ddlFiltroSituacao.SelectedValue);
                int _ID_CONTEUDO_TIPO = int.Parse(ddlFiltroTipoConteudo.SelectedValue);
                int _ID_CONTEUDO_CATEGORIA = int.Parse(ddlFiltroCategoria.SelectedValue);

                if (!string.IsNullOrWhiteSpace(txtFiltroTitulo.Text))
                {
                    if (_ID_CONTEUDO_TIPO > 0)
                    {
                        if (_ID_CONTEUDO_CATEGORIA > 0)
                        {
                            e.Query = from item in result
                                      where
                                        item.TITULO.Contains(txtFiltroTitulo.Text)
                                        && item.ID_CONTEUDO_TIPO == _ID_CONTEUDO_TIPO
                                        && item.ID_CONTEUDO_CATEGORIA == _ID_CONTEUDO_CATEGORIA
                                        && item.ID_CONTEUDO_SITUACAO == _ID_SITUACAO
                                      select item;
                        }
                        else
                        {
                            e.Query = from item in result
                                      where
                                        item.TITULO.Contains(txtFiltroTitulo.Text)
                                        && item.ID_CONTEUDO_TIPO == _ID_CONTEUDO_TIPO
                                        && item.ID_CONTEUDO_SITUACAO == _ID_SITUACAO
                                      select item;
                        }
                    }
                    else
                    {
                        if (_ID_CONTEUDO_CATEGORIA > 0)
                        {
                            e.Query = from item in result
                                      where
                                        item.TITULO.Contains(txtFiltroTitulo.Text)
                                        && item.ID_CONTEUDO_CATEGORIA == _ID_CONTEUDO_CATEGORIA
                                        && item.ID_CONTEUDO_SITUACAO == _ID_SITUACAO
                                      select item;
                        }
                        else
                        {
                            e.Query = from item in result
                                      where
                                        item.TITULO.Contains(txtFiltroTitulo.Text)
                                        && item.ID_CONTEUDO_SITUACAO == _ID_SITUACAO
                                      select item;
                        }
                    }
                }
                else
                {
                    if (_ID_CONTEUDO_TIPO > 0)
                    {
                        if (_ID_CONTEUDO_CATEGORIA > 0)
                        {
                            e.Query = from item in result
                                      where
                                        item.ID_CONTEUDO_TIPO == _ID_CONTEUDO_TIPO
                                        && item.ID_CONTEUDO_CATEGORIA == _ID_CONTEUDO_CATEGORIA
                                        && item.ID_CONTEUDO_SITUACAO == _ID_SITUACAO
                                      select item;
                        }
                        else
                        {
                            e.Query = from item in result
                                      where
                                        item.ID_CONTEUDO_TIPO == _ID_CONTEUDO_TIPO
                                        && item.ID_CONTEUDO_SITUACAO == _ID_SITUACAO
                                      select item;
                        }
                    }
                    else
                    {
                        if (_ID_CONTEUDO_CATEGORIA > 0)
                        {
                            e.Query = from item in result
                                      where
                                        item.ID_CONTEUDO_CATEGORIA == _ID_CONTEUDO_CATEGORIA
                                        && item.ID_CONTEUDO_SITUACAO == _ID_SITUACAO
                                      select item;
                        }
                        else
                        {
                            e.Query = from item in result
                                      where
                                        item.ID_CONTEUDO_SITUACAO == _ID_SITUACAO
                                      select item;
                        }
                    }
                }
            }
            catch (Exception ex) { throw; }
        }

        private void CarregarArquivo()
        {
            if (ObjCarregado.ID_ARQUIVO_DIGITAL.HasValue)
            {
                imgEvento.ImageUrl = string.Format("~/ShowFile.aspx?action=1&fileid={0}&width={1}&height={2}", ObjCarregado.ID_ARQUIVO_DIGITAL.ToString(), 400, 300);
                pnlArquivoAtual.Visible = true;
            }
            else
            {
                pnlArquivoAtual.Visible = false;
                imgEvento.ImageUrl = "~/ShowFile.aspx?action=1";
            }
        }

        protected void afuArquivoEvento_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e) { ArquivoEnviado = HTMSiteAdmin.Library.UserInterface.WebControlsUtility.AsyncFileUpload_UploadedComplete(sender, e); }

        protected void btnExcluirImagem_Click(object sender, ImageClickEventArgs e)
        {
            ConteudoBo _objBo = new ConteudoBo();

            ObjCarregado = _objBo.Find(s => s.ID_CONTEUDO == ObjCarregado.ID_CONTEUDO).First();

            ARQUIVO_DIGITAL _objArquivoDigital = ObjCarregado.ARQUIVO_DIGITAL;

            ObjCarregado.ID_ARQUIVO_DIGITAL = null;

            _objBo.Update(ObjCarregado);
            _objBo.SaveChanges();

            ArquivoDigitalBo _objArquivoBo = new ArquivoDigitalBo();
            _objArquivoDigital = _objArquivoBo.Find(s => s.ID_ARQUIVO == _objArquivoDigital.ID_ARQUIVO).First();
            _objArquivoBo.Delete(_objArquivoDigital);
            _objArquivoBo.SaveChanges();

            ObjCarregado = _objBo.Find(s => s.ID_CONTEUDO == ObjCarregado.ID_CONTEUDO).First();

            _objArquivoDigital = null; _objBo = null; _objArquivoBo = null;

            CarregarArquivo();
        }
    }
}