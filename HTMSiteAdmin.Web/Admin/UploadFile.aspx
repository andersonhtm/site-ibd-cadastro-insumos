﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadFile.aspx.cs" Inherits="HTMSiteAdmin.Web.Admin.UploadFile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data" style="margim: 0px;
    padding: 0px;">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" />
    <ajaxToolkit:AsyncFileUpload ID="afuArquivoEnviado" runat="server" OnUploadedComplete="afuArquivoEnviado_UploadedComplete"
        Style="margim: 0px; padding: 0px;" />
    </form>
</body>
</html>
