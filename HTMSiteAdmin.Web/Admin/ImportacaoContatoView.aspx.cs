﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Importacao;
using HTMSiteAdmin.Library.Exceptions;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class ImportacaoContatoView : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this, ViewToken, "ViewAspect", value); }
        }
        private IMPORTACAO_CONTATO ObjCarregado
        {
            get { return (IMPORTACAO_CONTATO)Global.RestoreObject(this, ViewToken, "ObjCarregado"); }
            set { Global.SaveObject(this, ViewToken, "ObjCarregado", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MapButtons();

            if (!IsPostBack && ObjCarregado == null)
                ViewAspect = enmUserInterfaceAspect.Start;

            if (!IsPostBack) UserInterfaceAspectChanged();
        }

        public void ValidateAcess() { throw new NotImplementedException(); }

        public void MapButtons()
        {
            ucButtons1.ButtonNew.CausesValidation = false;
            ucButtons1.ButtonNew.Click += new ImageClickEventHandler(ButtonNew_Click);

            ucButtons1.ButtonSave.ValidationGroup = "vgView";
            ucButtons1.ButtonSave.CausesValidation = true;
            ucButtons1.ButtonSave.Click += new ImageClickEventHandler(ButtonSave_Click);

            ucButtons1.ButtonSaveNew.ValidationGroup = "vgView";
            ucButtons1.ButtonSaveNew.CausesValidation = true;
            ucButtons1.ButtonSaveNew.Click += new ImageClickEventHandler(ButtonSaveNew_Click);

            ucButtons1.ButtonCancel.CausesValidation = false;
            ucButtons1.ButtonCancel.Click += new ImageClickEventHandler(ButtonCancel_Click);

            ucButtons1.ButtonRefresh.CausesValidation = false;
            ucButtons1.ButtonRefresh.Click += new ImageClickEventHandler(ButtonRefresh_Click);

            ucButtons1.ButtonDelete.CausesValidation = false;
            ucButtons1.ButtonDelete.Click += new ImageClickEventHandler(ButtonDelete_Click);
        }

        public void UserInterfaceAspectChanged()
        {
            StartPageControls();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    ucButtons1.Visible = false;

                    pnlView.Visible = false;

                    pnlView.Visible = false;
                    gvView.DataSource = null;
                    gvView.DataBind();
                    break;
                case enmUserInterfaceAspect.New:
                    ucButtons1.Visible = true;

                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = true;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    RestartForm();
                    break;
                case enmUserInterfaceAspect.Update:
                    ucButtons1.Visible = true;

                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    ClearFields();

                    SetForm();
                    break;
                default:
                    break;
            }
        }

        public void StartPageControls()
        {
            try
            {
                if (!IsPostBack) { ddlId_cliente.DataBind(); }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void RestartForm()
        {
            ClearMemory();
            ClearFields();
        }

        public void ButtonNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                CarregarContatosCliente();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSaveNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonRefresh_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                switch (ViewAspect)
                {
                    case enmUserInterfaceAspect.Start:
                        //Nada fazer
                        break;
                    case enmUserInterfaceAspect.New:
                        ClearFields();
                        break;
                    case enmUserInterfaceAspect.Update:
                        ClearFields();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonCancel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ClearMemory();
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonDelete_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void GetForm() { throw new NotImplementedException(); }

        public void SaveView()
        {
            try
            {
                ImportacaoContatoBo _objBo = new ImportacaoContatoBo();

                switch (ViewAspect)
                {
                    case enmUserInterfaceAspect.Start:
                        break;
                    case enmUserInterfaceAspect.New:
                        ObjCarregado = new IMPORTACAO_CONTATO()
                        {
                            ID_CONTATO = Guid.NewGuid().ToString(),
                            ID_CLIENTE = ddlId_cliente.SelectedValue,
                            NOME = !string.IsNullOrWhiteSpace(txtNome.Text) ? txtNome.Text : string.Empty,
                            TELEFONE = !string.IsNullOrWhiteSpace(txtTelefone.Text) ? txtTelefone.Text : string.Empty,
                            EMAIL = !string.IsNullOrWhiteSpace(txtEmail.Text) ? txtEmail.Text : string.Empty,
                            REGISTRO_ORIGEM = 1,
                            ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO
                        };

                        _objBo.Add(ObjCarregado);
                        break;
                    case enmUserInterfaceAspect.Update:
                        ObjCarregado = _objBo.Find(lbda => lbda.ID_CONTATO == ObjCarregado.ID_CONTATO).First();

                        ObjCarregado.NOME = !string.IsNullOrWhiteSpace(txtNome.Text) ? txtNome.Text : string.Empty;
                        ObjCarregado.TELEFONE = !string.IsNullOrWhiteSpace(txtTelefone.Text) ? txtTelefone.Text : string.Empty;
                        ObjCarregado.EMAIL = !string.IsNullOrWhiteSpace(txtEmail.Text) ? txtEmail.Text : string.Empty;
                        ObjCarregado.ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO;

                        _objBo.Update(ObjCarregado);
                        break;
                    default:
                        break;
                }

                _objBo.SaveChanges();

                _objBo = null; ObjCarregado = null;
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao salvar os dados. Mensagem: " + ex.Message);
            }
        }

        public void SetForm()
        {
            try
            {
                txtNome.Text = !string.IsNullOrWhiteSpace(ObjCarregado.NOME) ? ObjCarregado.NOME : string.Empty;
                txtTelefone.Text = !string.IsNullOrWhiteSpace(ObjCarregado.TELEFONE) ? ObjCarregado.TELEFONE : string.Empty;
                txtEmail.Text = !string.IsNullOrWhiteSpace(ObjCarregado.EMAIL) ? ObjCarregado.EMAIL : string.Empty;
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao carregar o formulário. Mensagem: " + ex.Message);
            }
        }

        public void EnableFields() { throw new NotImplementedException(); }

        public void DisableFields() { throw new NotImplementedException(); }

        public void ClearFields()
        {
            try
            {
                txtEmail.Text = txtNome.Text = txtTelefone.Text = string.Empty;
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao limpar os campos do formulário. Mensagem: " + ex.Message);
            }
        }

        public void ClearMemory() { ObjCarregado = null; }

        protected void gvView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //((Literal)e.Row.FindControl("ltrRegistroOrigem")).Text = ((IMPORTACAO_CLIENTE)e.Row.DataItem).REGISTRO_ORIGEM == 2 ? "Importação" : "Manual";
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string _SelectedKey = gvView.SelectedDataKey.Values["ID_CONTATO"].ToString();

                ObjCarregado = new ImportacaoContatoBo().Find(lbda => lbda.ID_CONTATO == _SelectedKey).First();

                ViewAspect = enmUserInterfaceAspect.Update;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int currentPageIndex;
                string _CommandKey;

                switch (e.CommandName)
                {
                    case "Excluir":
                        _CommandKey = e.CommandArgument.ToString();

                        ImportacaoContatoBo _objBo = new ImportacaoContatoBo();
                        _objBo.Delete(_objBo.Find(lbda => lbda.ID_CONTATO == _CommandKey).First());
                        _objBo.SaveChanges();
                        _objBo = null;

                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex >= 0 && currentPageIndex <= gvView.PageCount - 1)
                            gvView.PageIndex = currentPageIndex;
                        break;
                    case "FirstPage":
                        gvView.DataBind();
                        gvView.PageIndex = 0;
                        break;
                    case "BackPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex > 0)
                            gvView.PageIndex = currentPageIndex - 1;
                        break;
                    case "GoToPage":
                        currentPageIndex = gvView.PageIndex;

                        int goToTop = 1, goToBottom = 1, goTo = 1;
                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text))
                            goToTop = int.Parse(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text);

                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text))
                            goToBottom = int.Parse(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text);

                        gvView.DataBind();

                        goTo = (goToTop != currentPageIndex + 1) ? goToTop : goToBottom;

                        if (goTo >= 1 && goTo <= gvView.PageCount)
                            gvView.PageIndex = goTo - 1;
                        break;
                    case "NextPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex < gvView.PageCount)
                            gvView.PageIndex = currentPageIndex + 1;
                        break;
                    case "LastPage":
                        gvView.DataBind();
                        gvView.PageIndex = gvView.PageCount - 1;
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void edsView_QueryCreated(object sender, QueryCreatedEventArgs e)
        {
            //try
            //{
            //    var result = e.Query.Cast<IMPORTACAO_CLIENTE>();

            //    int idOrigem = int.Parse(ddlFiltroOrigem.SelectedValue);

            //    if (idOrigem == 0)
            //    {
            //        if (!string.IsNullOrWhiteSpace(txtFiltroDescricao.Text))
            //        {
            //            if (!string.IsNullOrWhiteSpace(txtFiltroMatricula.Text))
            //            {
            //                e.Query = from item in result
            //                          where
            //                            item.NOME_RAZAO.Contains(txtFiltroDescricao.Text) || item.APELIDO_FANTASIA.Contains(txtFiltroDescricao.Text)
            //                            && item.MATRICULA.Contains(txtFiltroMatricula.Text)
            //                          select item;
            //            }
            //            else
            //            {
            //                e.Query = from item in result
            //                          where item.NOME_RAZAO.Contains(txtFiltroDescricao.Text) || item.APELIDO_FANTASIA.Contains(txtFiltroDescricao.Text)
            //                          select item;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        if (!string.IsNullOrWhiteSpace(txtFiltroDescricao.Text))
            //        {
            //            if (!string.IsNullOrWhiteSpace(txtFiltroMatricula.Text))
            //            {
            //                e.Query = from item in result
            //                          where
            //                            item.NOME_RAZAO.Contains(txtFiltroDescricao.Text) || item.APELIDO_FANTASIA.Contains(txtFiltroDescricao.Text)
            //                            && item.REGISTRO_ORIGEM == idOrigem
            //                            && item.MATRICULA.Contains(txtFiltroMatricula.Text)
            //                          select item;
            //            }
            //            else
            //            {
            //                e.Query = from item in result
            //                          where
            //                            item.NOME_RAZAO.Contains(txtFiltroDescricao.Text) || item.APELIDO_FANTASIA.Contains(txtFiltroDescricao.Text)
            //                            && item.REGISTRO_ORIGEM == idOrigem
            //                          select item;
            //            }
            //        }
            //        else
            //        {
            //            if (!string.IsNullOrWhiteSpace(txtFiltroMatricula.Text))
            //            {
            //                e.Query = from item in result
            //                          where
            //                            item.REGISTRO_ORIGEM == idOrigem
            //                            && item.MATRICULA.Contains(txtFiltroMatricula.Text)
            //                          select item;
            //            }
            //            else
            //            {
            //                e.Query = from item in result
            //                          where
            //                            item.REGISTRO_ORIGEM == idOrigem
            //                          select item;
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex) { throw; }
        }

        protected void ddlId_cliente_DataBound(object sender, EventArgs e) { HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender, "pt"); }

        protected void ddlId_cliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            try { CarregarContatosCliente(); }
            catch (Exception ex) { throw; }
        }

        private void CarregarContatosCliente()
        {
            if (ddlId_cliente.SelectedValue.Equals("0"))
            {
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
            else
            {
                ucButtons1.Visible = true;

                ucButtons1.ButtonNew.Visible = true;
                ucButtons1.ButtonSave.Visible = false;
                ucButtons1.ButtonRefresh.Visible = false;
                ucButtons1.ButtonCancel.Visible = false;
                ucButtons1.ButtonDelete.Visible = false;

                pnlView.Visible = false;

                gvView.DataBind();

                pnlViewCollection.Visible = gvView.Rows.Count > 0;
            }
        }
    }
}