﻿using System;
using HTMSiteAdmin.Library.Exceptions;
using LifeTimeObjects.Exceptions;

namespace HTMSiteAdmin.Web.Admin.UserControls
{
    public partial class ucShowException : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            pnlShowExceptions.Visible = ExceptionLogo.Visible = ErrorExceptionLogo.Visible = CorrectLogo.Visible = false;
        }

        public void ShowExceptions(object _Exceptions)
        {
            if (_Exceptions.GetType() == typeof(HTMSiteAdminException))
            {
                pnlShowExceptions.CssClass = "divShowException";
                pnlShowExceptions.Visible = true;
                ExceptionLogo.Visible = true;
                ErrorExceptionLogo.Visible = false;
                CorrectLogo.Visible = false;

                ltrExceptionTitle.Text = "<strong>Os seguintes itens devem ser observados antes de concluir a sua operação:</strong>";
                ltrExceptions.Text = "<p>- " + ((HTMSiteAdminException)_Exceptions).Message.Replace("\r\n", "<br />") + "</p>";
            }
            else if (_Exceptions.GetType() == typeof(MultiLoginException))
            {
                pnlShowExceptions.CssClass = "divShowException";
                pnlShowExceptions.Visible = true;
                ExceptionLogo.Visible = true;
                ErrorExceptionLogo.Visible = false;
                CorrectLogo.Visible = false;

                ltrExceptionTitle.Text = "<strong>Atenção:</strong>";
                ltrExceptions.Text = "<p>- " + ((MultiLoginException)_Exceptions).Message.Replace("\r\n", "<br />") + "</p>";
            }
            else if (_Exceptions.GetType() == typeof(HTMSiteAdminErrorException))
            {
                pnlShowExceptions.CssClass = "divShowException";
                pnlShowExceptions.Visible = true;
                ExceptionLogo.Visible = false;
                ErrorExceptionLogo.Visible = true;
                CorrectLogo.Visible = false;

                ltrExceptionTitle.Text = "<strong>Os seguintes erros ocorreram ao executar a sua operação:</strong>";
                ltrExceptions.Text = "<p>- " + ((HTMSiteAdminErrorException)_Exceptions).Message.Replace("\r\n", "<br />") + "</p>";
            }
            else
                pnlShowExceptions.Visible = ExceptionLogo.Visible = ErrorExceptionLogo.Visible = false;
        }

        public void Sucess()
        {
            pnlShowExceptions.CssClass = "divShowExceptionSucesso";
            pnlShowExceptions.Visible = true;
            ExceptionLogo.Visible = false;
            ErrorExceptionLogo.Visible = false;
            CorrectLogo.Visible = true;

            ltrExceptionTitle.Text = "<strong>Operação realizada com sucesso!</strong>";
            ltrExceptions.Text = string.Empty;
        }
    }
}