﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucShowException.ascx.cs"
    Inherits="HTMSiteAdmin.Web.Admin.UserControls.ucShowException" %>
<asp:Panel ID="pnlShowExceptions" runat="server" CssClass="divShowException">
    <div class="divShowExceptionLogo">
        <asp:Image ID="ExceptionLogo" runat="server" ImageUrl="~/Admin/Media/Images/Exception/alert.png"
            Width="30px" Style="vertical-align: middle;" />
        <asp:Image ID="ErrorExceptionLogo" runat="server" ImageUrl="~/Admin/Media/Images/Exception/error.png"
            Width="30px" Style="vertical-align: middle;" />
        <asp:Image ID="CorrectLogo" runat="server" ImageUrl="~/Admin/Media/Images/Exception/correct.png"
            Style="vertical-align: middle;" />
    </div>
    <asp:Panel runat="server" ID="pnlExceptions" CssClass="divExceptions">
        <asp:Literal ID="ltrExceptionTitle" runat="server" />
        <asp:Literal ID="ltrExceptions" runat="server" />
    </asp:Panel>
</asp:Panel>
