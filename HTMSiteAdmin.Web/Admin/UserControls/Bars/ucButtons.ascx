﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucButtons.ascx.cs" Inherits="HTMSiteAdmin.Web.Admin.UserControls.Bars.ucButtons" %>
<div class="ButtonsBarContainer" style="position: relative; float: left; display: block;
    width: 100%;">
    <p>
        <asp:ImageButton ID="btnNew" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/botao_novo_1.png"
            ToolTip="Novo" onmouseover="javascript:ChangeButtonsImage('New', 'over', this);"
            onmouseout="javascript:ChangeButtonsImage('New', 'out', this);" />
        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/botao_salvar_1.png"
            ToolTip="Salvar" onmouseover="javascript:ChangeButtonsImage('Save', 'over', this);"
            onmouseout="javascript:ChangeButtonsImage('Save', 'out', this);" />
        <asp:ImageButton ID="btnSaveNew" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/issue.png"
            ToolTip="Salvar e Novo" Visible="false" onmouseover="javascript:ChangeButtonsImage('SaveNew', 'over', this);"
            onmouseout="javascript:ChangeButtonsImage('SaveNew', 'out', this);" />
        <asp:ImageButton ID="btnRefresh" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/botao_limpar_1.png"
            ToolTip="Limpar Campos" onmouseover="javascript:ChangeButtonsImage('Refresh', 'over', this);"
            onmouseout="javascript:ChangeButtonsImage('Refresh', 'out', this);" />
        <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/botao_voltar_1.png"
            ToolTip="Voltar" onmouseover="javascript:ChangeButtonsImage('Cancel', 'over', this);"
            onmouseout="javascript:ChangeButtonsImage('Cancel', 'out', this);" />
        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/delete.png"
            ToolTip="Excluir" onmouseover="javascript:ChangeButtonsImage('Delete', 'over', this);"
            onmouseout="javascript:ChangeButtonsImage('Delete', 'out', this);" />
    </p>
</div>
