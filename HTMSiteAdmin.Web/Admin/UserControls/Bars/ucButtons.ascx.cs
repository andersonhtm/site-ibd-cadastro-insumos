﻿using System;
using System.Web.UI.WebControls;

namespace HTMSiteAdmin.Web.Admin.UserControls.Bars
{
    public partial class ucButtons : System.Web.UI.UserControl
    {
        public ImageButton ButtonNew { get { return this.btnNew; } }
        public ImageButton ButtonSave { get { return this.btnSave; } }
        public ImageButton ButtonSaveNew { get { return this.btnSaveNew; } }
        public ImageButton ButtonRefresh { get { return this.btnRefresh; } }
        public ImageButton ButtonCancel { get { return this.btnCancel; } }
        public ImageButton ButtonDelete { get { return this.btnDelete; } }

        protected void Page_Load(object sender, EventArgs e) { }
    }
}