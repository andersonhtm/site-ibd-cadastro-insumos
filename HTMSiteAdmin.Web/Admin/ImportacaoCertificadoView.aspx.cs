﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Importacao;
using HTMSiteAdmin.Library.Exceptions;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class ImportacaoCertificadoView : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this, ViewToken, "ViewAspect", value); }
        }
        private IMPORTACAO_CERTIFICADO ObjCarregado
        {
            get { return (IMPORTACAO_CERTIFICADO)Global.RestoreObject(this, ViewToken, "ObjCarregado"); }
            set { Global.SaveObject(this, ViewToken, "ObjCarregado", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MapButtons();

            if (!IsPostBack && ObjCarregado == null)
                ViewAspect = enmUserInterfaceAspect.Start;

            if (!IsPostBack) UserInterfaceAspectChanged();
        }

        public void ValidateAcess() { throw new NotImplementedException(); }

        public void MapButtons()
        {
            ucButtons1.ButtonNew.CausesValidation = false;
            ucButtons1.ButtonNew.Click += new ImageClickEventHandler(ButtonNew_Click);

            ucButtons1.ButtonSave.ValidationGroup = "vgView";
            ucButtons1.ButtonSave.CausesValidation = true;
            ucButtons1.ButtonSave.Click += new ImageClickEventHandler(ButtonSave_Click);

            ucButtons1.ButtonSaveNew.ValidationGroup = "vgView";
            ucButtons1.ButtonSaveNew.CausesValidation = true;
            ucButtons1.ButtonSaveNew.Click += new ImageClickEventHandler(ButtonSaveNew_Click);

            ucButtons1.ButtonCancel.CausesValidation = false;
            ucButtons1.ButtonCancel.Click += new ImageClickEventHandler(ButtonCancel_Click);

            ucButtons1.ButtonRefresh.CausesValidation = false;
            ucButtons1.ButtonRefresh.Click += new ImageClickEventHandler(ButtonRefresh_Click);

            ucButtons1.ButtonDelete.CausesValidation = false;
            ucButtons1.ButtonDelete.Click += new ImageClickEventHandler(ButtonDelete_Click);
        }

        public void UserInterfaceAspectChanged()
        {
            StartPageControls();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    ucButtons1.ButtonNew.Visible = true;
                    ucButtons1.ButtonSave.Visible = false;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = false;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = false;

                    int currentPageIndex = gvView.PageIndex;
                    gvView.DataBind();
                    gvView.PageIndex = currentPageIndex;

                    pnlViewCollection.Visible = gvView.Rows.Count > 0;
                    break;
                case enmUserInterfaceAspect.New:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = true;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    RestartForm();
                    break;
                case enmUserInterfaceAspect.Update:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    ClearFields();

                    SetForm();
                    break;
                default:
                    break;
            }
        }

        public void StartPageControls()
        {
            try
            {
                if (!IsPostBack) { }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void RestartForm()
        {
            ClearMemory();
            ClearFields();
        }

        public void ButtonNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSaveNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonRefresh_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                switch (ViewAspect)
                {
                    case enmUserInterfaceAspect.Start:
                        //Nada fazer
                        break;
                    case enmUserInterfaceAspect.New:
                        ClearFields();
                        break;
                    case enmUserInterfaceAspect.Update:
                        ClearFields();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonCancel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ClearMemory();
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonDelete_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void GetForm() { throw new NotImplementedException(); }

        public void SaveView()
        {
            try
            {
                ImportacaoCertificadoBo _objBo = new ImportacaoCertificadoBo();

                switch (ViewAspect)
                {
                    case enmUserInterfaceAspect.Start:
                        break;
                    case enmUserInterfaceAspect.New:
                        ObjCarregado = new IMPORTACAO_CERTIFICADO()
                        {
                            ID_CERTIFICADO = -1 + int.Parse(_objBo.GetAll().Min(lbda => lbda.ID_CERTIFICADO).ToString()),
                            NOME = !string.IsNullOrWhiteSpace(txtNome.Text) ? txtNome.Text : string.Empty,
                            NOME_EN = !string.IsNullOrWhiteSpace(txtNome_en.Text) ? txtNome_en.Text : string.Empty,
                            //NOME_ES = !string.IsNullOrWhiteSpace(txtNome_es.Text) ? txtNome_es.Text : string.Empty,
                            SIGLA = !string.IsNullOrWhiteSpace(txtSigla.Text) ? txtSigla.Text : string.Empty,
                            SIGLA_EN = !string.IsNullOrWhiteSpace(txtSigla_en.Text) ? txtSigla_en.Text : string.Empty,
                            //SIGLA_ES = !string.IsNullOrWhiteSpace(txtSigla_es.Text) ? txtSigla_es.Text : string.Empty,
                            REGISTRO_ORIGEM = 1,
                            ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO
                        };

                        _objBo.Add(ObjCarregado);
                        break;
                    case enmUserInterfaceAspect.Update:
                        ObjCarregado = _objBo.Find(lbda => lbda.ID_CERTIFICADO == ObjCarregado.ID_CERTIFICADO).First();

                        ObjCarregado.NOME = !string.IsNullOrWhiteSpace(txtNome.Text) ? txtNome.Text : string.Empty;
                        ObjCarregado.NOME_EN = !string.IsNullOrWhiteSpace(txtNome_en.Text) ? txtNome_en.Text : string.Empty;
                        //ObjCarregado.NOME_ES = !string.IsNullOrWhiteSpace(txtNome_es.Text) ? txtNome_es.Text : string.Empty;
                        ObjCarregado.SIGLA = !string.IsNullOrWhiteSpace(txtSigla.Text) ? txtSigla.Text : string.Empty;
                        ObjCarregado.SIGLA_EN = !string.IsNullOrWhiteSpace(txtSigla_en.Text) ? txtSigla_en.Text : string.Empty;
                        //ObjCarregado.SIGLA_ES = !string.IsNullOrWhiteSpace(txtSigla_es.Text) ? txtSigla_es.Text : string.Empty;
                        ObjCarregado.ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO;

                        _objBo.Update(ObjCarregado);
                        break;
                    default:
                        break;
                }

                _objBo.SaveChanges();

                _objBo = null; ObjCarregado = null;
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao salvar os dados. Mensagem: " + ex.Message);
            }
        }

        public void SetForm()
        {
            try
            {
                txtNome.Text = !string.IsNullOrWhiteSpace(ObjCarregado.NOME) ? ObjCarregado.NOME : string.Empty;
                txtNome_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.NOME_EN) ? ObjCarregado.NOME_EN : string.Empty;
                //txtNome_es.Text = !string.IsNullOrWhiteSpace(ObjCarregado.NOME_ES) ? ObjCarregado.NOME_ES : string.Empty;
                txtSigla.Text = !string.IsNullOrWhiteSpace(ObjCarregado.SIGLA) ? ObjCarregado.SIGLA : string.Empty;
                txtSigla_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.SIGLA_EN) ? ObjCarregado.SIGLA_EN : string.Empty;
                //txtSigla_es.Text = !string.IsNullOrWhiteSpace(ObjCarregado.SIGLA_ES) ? ObjCarregado.SIGLA_ES : string.Empty;
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao carregar o formulário. Mensagem: " + ex.Message);
            }
        }

        public void EnableFields() { throw new NotImplementedException(); }

        public void DisableFields() { throw new NotImplementedException(); }

        public void ClearFields()
        {
            try
            {
                txtNome.Text = txtNome_en.Text = /*txtNome_es.Text = */ string.Empty;
                txtSigla.Text = txtSigla_en.Text = /*txtSigla_es.Text = */ string.Empty;
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao limpar os campos do formulário. Mensagem: " + ex.Message);
            }
        }

        public void ClearMemory() { ObjCarregado = null; }

        protected void btnFiltrar_Click(object sender, ImageClickEventArgs e)
        {
            try { gvView.DataBind(); }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void gvView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((Literal)e.Row.FindControl("ltrRegistroOrigem")).Text = ((IMPORTACAO_CERTIFICADO)e.Row.DataItem).REGISTRO_ORIGEM == 2 ? "Importação" : "Manual";
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _SelectedKey = int.Parse(gvView.SelectedDataKey.Values["ID_CERTIFICADO"].ToString());

                ObjCarregado = new ImportacaoCertificadoBo().Find(lbda => lbda.ID_CERTIFICADO == _SelectedKey).First();

                ViewAspect = enmUserInterfaceAspect.Update;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int currentPageIndex, _CommandKey;

                switch (e.CommandName)
                {
                    case "Excluir":
                        _CommandKey = int.Parse(e.CommandArgument.ToString());

                        ImportacaoCertificadoBo _objBo = new ImportacaoCertificadoBo();
                        _objBo.Delete(_objBo.Find(lbda => lbda.ID_CERTIFICADO == _CommandKey).First());
                        _objBo.SaveChanges();
                        _objBo = null;

                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex >= 0 && currentPageIndex <= gvView.PageCount - 1)
                            gvView.PageIndex = currentPageIndex;
                        break;
                    case "FirstPage":
                        gvView.DataBind();
                        gvView.PageIndex = 0;
                        break;
                    case "BackPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex > 0)
                            gvView.PageIndex = currentPageIndex - 1;
                        break;
                    case "GoToPage":
                        currentPageIndex = gvView.PageIndex;

                        int goToTop = 1, goToBottom = 1, goTo = 1;
                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text))
                            goToTop = int.Parse(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text);

                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text))
                            goToBottom = int.Parse(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text);

                        gvView.DataBind();

                        goTo = (goToTop != currentPageIndex + 1) ? goToTop : goToBottom;

                        if (goTo >= 1 && goTo <= gvView.PageCount)
                            gvView.PageIndex = goTo - 1;
                        break;
                    case "NextPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex < gvView.PageCount)
                            gvView.PageIndex = currentPageIndex + 1;
                        break;
                    case "LastPage":
                        gvView.DataBind();
                        gvView.PageIndex = gvView.PageCount - 1;
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void edsView_QueryCreated(object sender, QueryCreatedEventArgs e)
        {
            try
            {
                var result = e.Query.Cast<IMPORTACAO_CERTIFICADO>();

                int idOrigem = int.Parse(ddlFiltroOrigem.SelectedValue);

                if (idOrigem == 0)
                {
                    if (!string.IsNullOrWhiteSpace(txtFiltroDescricao.Text))
                    {
                        e.Query = from item in result
                                  where item.NOME.Contains(txtFiltroDescricao.Text) || item.NOME_EN.Contains(txtFiltroDescricao.Text)
                                  select item;
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(txtFiltroDescricao.Text))
                    {
                        e.Query = from item in result
                                  where
                                    item.NOME.Contains(txtFiltroDescricao.Text) || item.NOME_EN.Contains(txtFiltroDescricao.Text)
                                    && item.REGISTRO_ORIGEM == idOrigem
                                  select item;
                    }
                    else
                    {
                        e.Query = from item in result
                                  where
                                    item.REGISTRO_ORIGEM == idOrigem
                                  select item;

                    }
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void btnLimparCamposFiltro_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                txtFiltroDescricao.Text = string.Empty;

                gvView.DataBind();
            }
            catch (Exception ex) { throw; }
        }
    }
}