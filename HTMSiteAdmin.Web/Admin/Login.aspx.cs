﻿using System;
using System.Linq;
using System.Web.UI;
using HTMSiteAdmin.Business.Sistema.ControleAcesso;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Library.Exceptions;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class Login : System.Web.UI.Page
    {
        private string ViewToken
        {
            get { return "MASTER_PAGE_TOKEN"; }
        }
        /*
        private SESSAO SessaoAberta
        {
            get { return (SESSAO)Global.RestoreObject(this.Page, ViewToken, "SessaoAberta"); }
            set { Global.SaveObject(this.Page, ViewToken, "SessaoAberta", value); }
        }
*/

        private SESSAO SessaoAberta
        {
            get { return (Session["SessaoAberta"] as SESSAO); }
            set { Session["SessaoAberta"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e) { }

        protected void btnLogin_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                USUARIO _objLoggedUser = UsuarioBo.CheckLogin(txtUsuario.Text, txtSenha.Text);

                if (_objLoggedUser == null)
                    throw new HTMSiteAdminException("Usuário não encontrado e/ou senha inválida.");

                SessaoBo _SessaoBo = new SessaoBo();

                if (SessaoAberta == null)
                    SessaoAberta = SessaoBo.AbrirSessao(Guid.NewGuid(), Request.UserHostAddress, null, Request.UserHostName, Request.UserAgent);

                SessaoAberta = _SessaoBo.Find(lbda => lbda.ID_SESSAO == SessaoAberta.ID_SESSAO).First();
                SessaoAberta.ID_USUARIO_LOGADO = _objLoggedUser.ID_USUARIO;
                _SessaoBo.Update(SessaoAberta);
                _SessaoBo.SaveChanges();

                _objLoggedUser.ID_SESSAO = SessaoAberta.ID_SESSAO;

                //Global.Login(this, _objLoggedUser.ID_USUARIO.ToString(), _objLoggedUser);
                Response.Redirect("~/Admin/Default.aspx");
            }
            catch (Exception ex)
            {
                pnlMensagemErro.Visible = true;
                ltrMensagemErro.Text = ex.Message;
            }
        }

        protected void btnLimpar_Click(object sender, ImageClickEventArgs e)
        {
            txtUsuario.Text = txtSenha.Text = string.Empty;
        }
    }
}