﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Configuration;
using HTMSiteAdmin.Library;
using System.Text;
using System.Data.SqlClient;
using HTMSiteAdmin.Data;
using GenericParsing;
using HTMSiteAdmin.Library.Exceptions;
using HTMSiteAdmin.Business.Importacao;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class Importacao : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected DataTable CsvToDataTable(string fullPath, string dataTableName)
        {
            DataTable dataTable = new DataTable(dataTableName);

            using (GenericParserAdapter parser = new GenericParserAdapter(fullPath, Encoding.GetEncoding("Windows-1252")))
            {
                parser.ColumnDelimiter = ';';
                parser.FirstRowHasHeader = true;
                parser.MaxBufferSize = 4096;
                parser.MaxRows = 50000;
                parser.TextQualifier = '\"';
                dataTable = parser.GetDataTable();
                dataTable.TableName = dataTableName;
            }

            return dataTable;
        }

        protected void btnProcessar_Click(object sender, ImageClickEventArgs e)
        {
            if (!Validar() || !Page.IsValid)
                return;

            ProcessarArquivos();
        }

        private bool Validar()
        {
            if (!fupProdutos.HasFile || !fupClientes.HasFile || !fupCertificados.HasFile || !fupClienteProdutoCertificado.HasFile)
            {
                mensagem.ShowExceptions(new HTMSiteAdminException("Todos os arquivos devem ser informados."));
                return false;
            }

            if (Path.GetExtension(fupProdutos.FileName).ToLower() != ".csv" ||
                Path.GetExtension(fupClientes.FileName).ToLower() != ".csv" ||
                Path.GetExtension(fupContatoCliente.FileName).ToLower() != ".csv" ||
                Path.GetExtension(fupClienteProdutoCertificado.FileName).ToLower() != ".csv" || 
                Path.GetExtension(fupCertificados.FileName).ToLower() != ".csv")
            {
                mensagem.ShowExceptions(new HTMSiteAdminException("Arquivos informados devem possuir extensão CSV."));
                return false;
            }

            return true;
        }

        private bool ValidarCabecalho(DataTable dt, string[] colunas, out string colunasFaltantes) {
            StringBuilder str = new StringBuilder();
            colunasFaltantes = "";
            bool sucesso = true;

            foreach (string col in colunas)
            {
                if (dt.Columns[col] == null)
                {
                    sucesso = false;
                    str.AppendFormat("{0}{1}", str.Length > 0 ? ", " : "", col);
                }
            }
            colunasFaltantes = str.ToString();
            return sucesso;
        }

        private bool ProcessarArquivos()
        {
            DataTable dt;
            DataSet ds = new DataSet();
            string colunasFaltantes = "";

            #region Produtos

            dt = ConverterArquivo(fupProdutos.FileContent, "produto");
            if (!ValidarCabecalho(dt, new string[] { "ID_PRODUTO", "NOME", "NOME_EN" }, out colunasFaltantes))
            {
                mensagem.ShowExceptions(new HTMSiteAdminException(string.Format("Arquivo de produtos inválido. Coluna(s) '{0}' não existe(m) no arquivo.", colunasFaltantes)));
                return false;
            }

            dt.Columns.Add(new DataColumn("REGISTRO_ORIGEM", typeof(int)));
            dt.Columns.Add(new DataColumn("PRODUTO", typeof(bool)));
            dt.Columns.Add(new DataColumn("INSUMO", typeof(bool)));
            dt.Columns.Add(new DataColumn("ID_SESSAO", typeof(Guid)));

            foreach (DataRow row in dt.Rows)
            {
                row["REGISTRO_ORIGEM"] = 2;
                row["PRODUTO"] = true;
                row["INSUMO"] = false;
                row["ID_SESSAO"] = ((SESSAO)Session["SessaoAberta"]).ID_SESSAO;
            }
            ds.Tables.Add(dt);
            #endregion

            #region Clientes
            dt = ConverterArquivo(fupClientes.FileContent, "cliente");
            if (!ValidarCabecalho(dt, new string[] { "ID_CLIENTE", "PF_PJ", "MATRICULA", "APELIDO_FANTASIA", "NOME_RAZAO", 
                                                    "CPF_CNPJ", "RG_IE", "CEP", "LOGRADOURO", "ENDERECO", "NUMERO", "CXPOSTAL", 
                                                    "COMPLEMENTO", "MUNICIPIO", "ESTADO", "UF", "PAIS", "PAIS_EN", "EMAIL", "DDI", 
                                                    "TELEFONE", "FAX", "CELULAR", "SITE" }, out colunasFaltantes))
            {
                mensagem.ShowExceptions(new HTMSiteAdminException(string.Format("Arquivo de clientes inválido. Coluna(s) '{0}' não existe(m) no arquivo.", colunasFaltantes)));
                return false;
            }

            dt.Columns.Add(new DataColumn("REGISTRO_ORIGEM", typeof(int)));
            dt.Columns.Add(new DataColumn("PF_PJ_INT", typeof(int)));
            dt.Columns.Add(new DataColumn("ID_SESSAO", typeof(Guid)));

            foreach (DataRow row in dt.Rows)
            {
                try
                {
                    row["ID_CLIENTE"] = Guid.Parse(row["ID_CLIENTE"].ToString());
                }
                catch (Exception)
                {
                    mensagem.ShowExceptions(new HTMSiteAdminException(string.Format("Arquivo de clientes inválido. O Registro ID_CLIENTE: '{0}' Não é um identificador válido. (UNIQUEIDENTIFIER)", row["ID_CLIENTE"].ToString())));
                }

                row["REGISTRO_ORIGEM"] = 2;
                row["PF_PJ_INT"] = (row["PF_PJ"].ToString() == "F") ? 1 : 2;
                row["ID_SESSAO"] = ((SESSAO)Session["SessaoAberta"]).ID_SESSAO;
            }
            ds.Tables.Add(dt);
            #endregion

            #region Certificados
            dt = ConverterArquivo(fupCertificados.FileContent, "certificado");
            if (!ValidarCabecalho(dt, new string[] { "ID_CERTIFICADO", "NOME", "NOME_EN", "SIGLA", "SIGLA_EN", "ORIGEM" }, out colunasFaltantes))
            {
                mensagem.ShowExceptions(new HTMSiteAdminException(string.Format("Arquivo de certificados inválido. Coluna(s) '{0}' não existe(m) no arquivo.", colunasFaltantes)));
                return false;
            }
            dt.Columns.Add(new DataColumn("REGISTRO_ORIGEM", typeof(int)));
            dt.Columns.Add(new DataColumn("ID_SESSAO", typeof(Guid)));

            foreach (DataRow row in dt.Rows)
            {
                row["REGISTRO_ORIGEM"] = 2;
                row["ID_SESSAO"] = ((SESSAO)Session["SessaoAberta"]).ID_SESSAO;
            }
            ds.Tables.Add(dt);
            #endregion

            #region Contatos Cliente
            dt = ConverterArquivo(fupContatoCliente.FileContent, "contatocliente");
            if (!ValidarCabecalho(dt, new string[] { "ID_CONTATO", "ID_CLIENTE", "NOME", "TELEFONE", "CELULAR", "EMAIL" }, out colunasFaltantes))
            {
                mensagem.ShowExceptions(new HTMSiteAdminException(string.Format("Arquivo de contatos de clientes inválido. Coluna(s) '{0}' não existe(m) no arquivo.", colunasFaltantes)));
                return false;
            }
            dt.Columns.Add(new DataColumn("REGISTRO_ORIGEM", typeof(int)));
            dt.Columns.Add(new DataColumn("ID_SESSAO", typeof(Guid)));

            foreach (DataRow row in dt.Rows)
            {
                try
                {
                    row["ID_CLIENTE"] = Guid.Parse(row["ID_CLIENTE"].ToString());
                }
                catch (Exception)
                {
                    mensagem.ShowExceptions(new HTMSiteAdminException(string.Format("Arquivo de contatos de clientes inválido. O Registro ID_CLIENTE: '{0}' Não é um identificador válido. (UNIQUEIDENTIFIER)", row["ID_CLIENTE"].ToString())));
                }

                row["REGISTRO_ORIGEM"] = 2;
                row["ID_SESSAO"] = ((SESSAO)Session["SessaoAberta"]).ID_SESSAO;
            }
            ds.Tables.Add(dt);
            #endregion

            #region Clientes Produtos Certificados
            dt = ConverterArquivo(fupClienteProdutoCertificado.FileContent, "clienteprodutocertificado");
            if (!ValidarCabecalho(dt, new string[] { "ID_CLIENTE","ID_PRODUTO","ID_CERTIFICADO" }, out colunasFaltantes))
            {
                mensagem.ShowExceptions(new HTMSiteAdminException(string.Format("Arquivo de clientes x produtos x certificados inválido. Coluna(s) '{0}' não existe(m) no arquivo.", colunasFaltantes)));
                return false;
            }
            dt.Columns.Add(new DataColumn("REGISTRO_ORIGEM", typeof(int)));
            dt.Columns.Add(new DataColumn("ID_SESSAO", typeof(Guid)));

            foreach (DataRow row in dt.Rows)
            {
                try
                {
                    row["ID_CLIENTE"] = Guid.Parse(row["ID_CLIENTE"].ToString());
                }
                catch (Exception)
                {
                    mensagem.ShowExceptions(new HTMSiteAdminException(string.Format("Arquivo de clientes x produtos x certificados. O Registro ID_CLIENTE: '{0}' Não é um identificador válido. (UNIQUEIDENTIFIER)", row["ID_CLIENTE"].ToString())));
                }

                row["REGISTRO_ORIGEM"] = 2;
                row["ID_SESSAO"] = ((SESSAO)Session["SessaoAberta"]).ID_SESSAO;
            }
            ds.Tables.Add(dt.DefaultView.ToTable(true, new string[]{ "ID_CLIENTE","ID_PRODUTO","ID_CERTIFICADO","REGISTRO_ORIGEM","ID_SESSAO"}));
            #endregion

            Processar(ds);

            return true;
        }

        DataTable ConverterArquivo(Stream fileContent, string dataTableName) {
            string path = string.Format(@"{0}{1}\", Server.MapPath(ConfigurationManager.AppSettings["RELATIVEPATH_ARQUIVODIGITAL"]), "Importacao");
            string fileName = string.Format("{0}_{1}_{2}.csv", ((SESSAO)Session["SessaoAberta"]).ID_SESSAO.ToString(), dataTableName, Guid.NewGuid().ToString());
            string fullPath = string.Format("{0}{1}", path, fileName);

            using (StreamReader reader = new StreamReader(fileContent, Encoding.GetEncoding("Windows-1252"), true))
            {
                string str = reader.ReadToEnd();
                using (FileStream file = new FileStream(fullPath, FileMode.CreateNew))
                {
                    using (StreamWriter writer = new StreamWriter(file, Encoding.GetEncoding("Windows-1252")))
                    {
                        writer.Write(str);
                    }
                }
            }

            return CsvToDataTable(fullPath, dataTableName);
        }

        void Processar(DataSet ds) 
        { 

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                SqlTransaction transaction = null;

                DataTable dtProdutoCertificado = new DataTable();

                conn.Open();
                try
                {
                    transaction = conn.BeginTransaction();

                    #region Apagar relações
                    using (SqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Transaction = transaction;
                        cmd.Parameters.AddWithValue("@REGISTRO_ORIGEM", 2);

                        // apaga e insere dados de insumos (id_produto < 0) no datatable
                        cmd.CommandText = "dbo.PRODUTO_APAGAR_RELACOES";
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd)) {
                            da.Fill(dtProdutoCertificado);
                        }

                        cmd.CommandText = "dbo.CLIENTE_APAGAR_RELACOES";
                        cmd.ExecuteNonQuery();

                        cmd.CommandText = "dbo.CERTIFICADO_APAGAR_RELACOES";
                        cmd.ExecuteNonQuery();
                    }
                    #endregion

                    #region Inclusão de todos os registros
                    using (SqlBulkCopy sqlbulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, transaction))
                    {
                        // produto
                        sqlbulkCopy.DestinationTableName = "dbo.IMPORTACAO_PRODUTO";
                        sqlbulkCopy.ColumnMappings.Add("ID_PRODUTO", "ID_PRODUTO");
                        sqlbulkCopy.ColumnMappings.Add("NOME", "NOME");
                        sqlbulkCopy.ColumnMappings.Add("NOME_EN", "NOME_EN");
                        sqlbulkCopy.ColumnMappings.Add("PRODUTO", "PRODUTO");
                        sqlbulkCopy.ColumnMappings.Add("INSUMO", "INSUMO");
                        sqlbulkCopy.ColumnMappings.Add("ID_SESSAO", "ID_SESSAO");
                        sqlbulkCopy.ColumnMappings.Add("REGISTRO_ORIGEM", "REGISTRO_ORIGEM");
                        sqlbulkCopy.WriteToServer(ds.Tables["produto"]);

                        // cliente
                        sqlbulkCopy.DestinationTableName = "dbo.IMPORTACAO_CLIENTE";
                        sqlbulkCopy.ColumnMappings.Clear();
                        sqlbulkCopy.ColumnMappings.Add("ID_CLIENTE", "ID_CLIENTE");
                        sqlbulkCopy.ColumnMappings.Add("PF_PJ_INT", "PF_PJ");
                        sqlbulkCopy.ColumnMappings.Add("MATRICULA", "MATRICULA");
                        sqlbulkCopy.ColumnMappings.Add("NOME_RAZAO", "NOME_RAZAO");
                        sqlbulkCopy.ColumnMappings.Add("APELIDO_FANTASIA", "APELIDO_FANTASIA");
                        sqlbulkCopy.ColumnMappings.Add("CPF_CNPJ", "CPF_CNPJ");
                        sqlbulkCopy.ColumnMappings.Add("RG_IE", "RG_IE");
                        sqlbulkCopy.ColumnMappings.Add("CEP", "CEP");
                        sqlbulkCopy.ColumnMappings.Add("LOGRADOURO", "LOGRADOURO");
                        sqlbulkCopy.ColumnMappings.Add("ENDERECO", "ENDERECO");
                        sqlbulkCopy.ColumnMappings.Add("NUMERO", "NUMERO");
                        sqlbulkCopy.ColumnMappings.Add("CXPOSTAL", "CXPOSTAL");
                        sqlbulkCopy.ColumnMappings.Add("COMPLEMENTO", "COMPLEMENTO");
                        sqlbulkCopy.ColumnMappings.Add("MUNICIPIO", "MUNICIPIO");
                        sqlbulkCopy.ColumnMappings.Add("ESTADO", "ESTADO");
                        sqlbulkCopy.ColumnMappings.Add("UF", "ESTADO_SIGLA");
                        sqlbulkCopy.ColumnMappings.Add("PAIS", "PAIS");
                        sqlbulkCopy.ColumnMappings.Add("PAIS_EN", "PAIS_EN");
                        sqlbulkCopy.ColumnMappings.Add("EMAIL", "EMAIL");
                        sqlbulkCopy.ColumnMappings.Add("DDI", "DDI");
                        sqlbulkCopy.ColumnMappings.Add("TELEFONE", "TELEFONE");
                        sqlbulkCopy.ColumnMappings.Add("FAX", "FAX");
                        sqlbulkCopy.ColumnMappings.Add("CELULAR", "CELULAR");
                        sqlbulkCopy.ColumnMappings.Add("SITE", "SITE");
                        sqlbulkCopy.ColumnMappings.Add("ID_SESSAO", "ID_SESSAO");
                        sqlbulkCopy.ColumnMappings.Add("REGISTRO_ORIGEM", "REGISTRO_ORIGEM");
                        sqlbulkCopy.WriteToServer(ds.Tables["cliente"]);

                        // certificado
                        sqlbulkCopy.DestinationTableName = "dbo.IMPORTACAO_CERTIFICADO";
                        sqlbulkCopy.ColumnMappings.Clear();
                        sqlbulkCopy.ColumnMappings.Add("ID_CERTIFICADO", "ID_CERTIFICADO");
                        sqlbulkCopy.ColumnMappings.Add("NOME", "NOME");
                        sqlbulkCopy.ColumnMappings.Add("NOME_EN", "NOME_EN");
                        sqlbulkCopy.ColumnMappings.Add("SIGLA", "SIGLA");
                        sqlbulkCopy.ColumnMappings.Add("SIGLA_EN", "SIGLA_EN");
                        sqlbulkCopy.ColumnMappings.Add("ID_SESSAO", "ID_SESSAO");
                        sqlbulkCopy.ColumnMappings.Add("REGISTRO_ORIGEM", "REGISTRO_ORIGEM");
                        sqlbulkCopy.WriteToServer(ds.Tables["certificado"]);

                        // contato cliente
                        sqlbulkCopy.DestinationTableName = "dbo.IMPORTACAO_CONTATO";
                        sqlbulkCopy.ColumnMappings.Clear();
                        sqlbulkCopy.ColumnMappings.Add("ID_CONTATO", "ID_CONTATO");
                        sqlbulkCopy.ColumnMappings.Add("ID_CLIENTE", "ID_CLIENTE");
                        sqlbulkCopy.ColumnMappings.Add("NOME", "NOME");
                        sqlbulkCopy.ColumnMappings.Add("TELEFONE", "TELEFONE");
                        sqlbulkCopy.ColumnMappings.Add("CELULAR", "CELULAR");
                        sqlbulkCopy.ColumnMappings.Add("EMAIL", "EMAIL");
                        sqlbulkCopy.ColumnMappings.Add("ID_SESSAO", "ID_SESSAO");
                        sqlbulkCopy.ColumnMappings.Add("REGISTRO_ORIGEM", "REGISTRO_ORIGEM");
                        sqlbulkCopy.WriteToServer(ds.Tables["contatocliente"]);

                        // cliente x produto x certificado
                        sqlbulkCopy.DestinationTableName = "dbo.IMPORTACAO_PRODUTO_CERTIFICADO";
                        sqlbulkCopy.ColumnMappings.Clear();
                        sqlbulkCopy.ColumnMappings.Add("ID_PRODUTO", "ID_PRODUTO");
                        sqlbulkCopy.ColumnMappings.Add("ID_CERTIFICADO", "ID_CERTIFICADO");
                        sqlbulkCopy.ColumnMappings.Add("ID_CLIENTE", "ID_CLIENTE");
                        sqlbulkCopy.ColumnMappings.Add("ID_SESSAO", "ID_SESSAO");
                        sqlbulkCopy.ColumnMappings.Add("REGISTRO_ORIGEM", "REGISTRO_ORIGEM");
                        sqlbulkCopy.WriteToServer(ds.Tables["clienteprodutocertificado"]);

                        // cliente x produto x certificado (insumos)
                        sqlbulkCopy.DestinationTableName = "dbo.IMPORTACAO_PRODUTO_CERTIFICADO";
                        sqlbulkCopy.ColumnMappings.Clear();
                        sqlbulkCopy.ColumnMappings.Add("ID_PRODUTO", "ID_PRODUTO");
                        sqlbulkCopy.ColumnMappings.Add("ID_CERTIFICADO", "ID_CERTIFICADO");
                        sqlbulkCopy.ColumnMappings.Add("ID_CLIENTE", "ID_CLIENTE");
                        sqlbulkCopy.ColumnMappings.Add("ID_SESSAO", "ID_SESSAO");
                        sqlbulkCopy.ColumnMappings.Add("REGISTRO_ORIGEM", "REGISTRO_ORIGEM");
                        sqlbulkCopy.WriteToServer(dtProdutoCertificado);

                        sqlbulkCopy.Close();
                    }
                    #endregion

                    transaction.Commit();
                    transaction = null;
                }
                catch (Exception ex)
                {
                    if (transaction != null)
                        transaction.Rollback();

                    mensagem.ShowExceptions(new HTMSiteAdminErrorException("Erro ao processar arquivos.", ex));
                    return;
                }
                finally {
                    if (conn.State != ConnectionState.Closed)
                        conn.Close();
                }
            }

            mensagem.Sucess();
        }

    }


}