﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Pessoas;
using HTMSiteAdmin.Library.Exceptions;
using System.Text;
using System.Data.Objects.DataClasses;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class PessoaView : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this, ViewToken, "ViewAspect", value); }
        }
        private PESSOA ObjCarregado
        {
            get { return (PESSOA)Global.RestoreObject(this, ViewToken, "ObjCarregado"); }
            set { Global.SaveObject(this, ViewToken, "ObjCarregado", value); }
        }
        private EntityCollection<PESSOA_CONTATO> ContatosCadastrados
        {
            get { return (EntityCollection<PESSOA_CONTATO>)Global.RestoreObject(this, ViewToken, "PessoaContatosCadastrados"); }
            set { Global.SaveObject(this, ViewToken, "PessoaContatosCadastrados", value); }
        }
        public UploadedFile ArquivoEnviado
        {
            get { return (UploadedFile)Global.RestoreObject(this, ViewToken, "ArquivoEnviado"); }
            set { Global.SaveObject(this, ViewToken, "ArquivoEnviado", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MapButtons();

            if (!IsPostBack && ObjCarregado == null)
                ViewAspect = enmUserInterfaceAspect.Start;

            if (!IsPostBack) UserInterfaceAspectChanged();
        }

        #region [ IPagina Members ]

        public void MapButtons()
        {
            ucButtons1.ButtonNew.CausesValidation = false;
            ucButtons1.ButtonNew.Click += new ImageClickEventHandler(ButtonNew_Click);

            ucButtons1.ButtonSave.ValidationGroup = "vgView";
            ucButtons1.ButtonSave.CausesValidation = true;
            ucButtons1.ButtonSave.Click += new ImageClickEventHandler(ButtonSave_Click);

            ucButtons1.ButtonSaveNew.ValidationGroup = "vgView";
            ucButtons1.ButtonSaveNew.CausesValidation = true;
            ucButtons1.ButtonSaveNew.Click += new ImageClickEventHandler(ButtonSaveNew_Click);

            ucButtons1.ButtonCancel.CausesValidation = false;
            ucButtons1.ButtonCancel.Click += new ImageClickEventHandler(ButtonCancel_Click);

            ucButtons1.ButtonRefresh.CausesValidation = false;
            ucButtons1.ButtonRefresh.Click += new ImageClickEventHandler(ButtonRefresh_Click);

            ucButtons1.ButtonDelete.CausesValidation = false;
            ucButtons1.ButtonDelete.Click += new ImageClickEventHandler(ButtonDelete_Click);

            MapButtonsPessoaContato();
        }

        public void UserInterfaceAspectChanged()
        {
            StartPageControls();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    ucButtons1.ButtonNew.Visible = true;
                    ucButtons1.ButtonSave.Visible = false;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = false;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = false;

                    int currentPageIndex = gvView.PageIndex;
                    gvView.DataBind();
                    gvView.PageIndex = currentPageIndex;

                    pnlViewCollection.Visible = gvView.Rows.Count > 0;
                    break;
                case enmUserInterfaceAspect.New:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = true;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    RestartForm();
                    break;
                case enmUserInterfaceAspect.Update:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    ClearFields();

                    SetForm();
                    break;
                default:
                    break;
            }
        }

        public void RestartForm()
        {
            ClearMemory();

            ddlTipoPessoa.SelectedValue = "1";
            ddlTipoPessoa_SelectedIndexChanged(ddlTipoPessoa, null);
            //ClearFields(); Já está dentro de "ddlTipoPessoa_SelectedIndexChanged"
        }

        public void StartPageControls()
        {
            try
            {
                if (!IsPostBack) { chkPessoaClassificacao.DataBind(); }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSaveNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void SaveView()
        {
            try
            {
                bool isClassificado = false;
                foreach (ListItem _Item in chkPessoaClassificacao.Items)
                {
                    if (!isClassificado)
                        isClassificado = _Item.Selected;
                }

                if (!isClassificado)
                    throw new HTMSiteAdminException("Selecione, pelo menos, uma classificação.");

                PessoaBo _objBo = new PessoaBo();

                switch (ViewAspect)
                {
                    case enmUserInterfaceAspect.Start:
                        //Nada fazer
                        break;
                    case enmUserInterfaceAspect.New:
                        ObjCarregado = new PESSOA()
                        {
                            ID_PESSOA_TIPO = int.Parse(ddlTipoPessoa.SelectedValue),
                            MATRICULA = !string.IsNullOrWhiteSpace(txtCodigo.Text) ? txtCodigo.Text : string.Empty,
                            ID_PESSOA_SITUACAO = int.Parse(ddlSituacao.SelectedValue),
                            PF_NOME_COMPLETO = !string.IsNullOrWhiteSpace(txtNome_completo.Text) ? txtNome_completo.Text : string.Empty,
                            PF_APELIDO = !string.IsNullOrWhiteSpace(txtApelido.Text) ? txtApelido.Text : txtNome_completo.Text,
                            PF_CPF = ddlTipoPessoa.SelectedValue.Equals("1") ? txtCpf.Text : string.Empty,
                            PF_RG = string.Empty,
                            //Datas são feitas em FillObjectDates(); PF_DATA_NASCIMENTO = Convert.ToDateTime(txtData_nascimento.Text),
                            PJ_RAZAO_SOCIAL = !string.IsNullOrWhiteSpace(txtRazao_social.Text) ? txtRazao_social.Text : string.Empty,
                            PJ_NOME_FANTASIA = !string.IsNullOrWhiteSpace(txtNome_fantasia.Text) ? txtNome_fantasia.Text : string.Empty,
                            PJ_CNPJ = ddlTipoPessoa.SelectedValue.Equals("2") ? txtCnpj.Text : string.Empty,
                            PJ_IE = string.Empty,
                            //Datas são feitas em FillObjectDates(); PJ_DATA_FUNDACAO = Convert.ToDateTime(txtData_fundacao.Text),
                            ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO
                        };

                        FillObjectDates();
                        FillArquivoDigital();
                        UpdatePessoaPessoaClassificacoes();
                        UpdatePessoaContatos();

                        _objBo.Add(ObjCarregado);
                        break;
                    case enmUserInterfaceAspect.Update:
                        ObjCarregado = _objBo.Find(lbda => lbda.ID_PESSOA == ObjCarregado.ID_PESSOA).First();
                        //DADOS DA PESSOA
                        ObjCarregado.ID_PESSOA_TIPO = int.Parse(ddlTipoPessoa.SelectedValue);
                        ObjCarregado.MATRICULA = !string.IsNullOrWhiteSpace(txtCodigo.Text) ? txtCodigo.Text : string.Empty;
                        ObjCarregado.ID_PESSOA_SITUACAO = int.Parse(ddlSituacao.SelectedValue);
                        ObjCarregado.PF_NOME_COMPLETO = !string.IsNullOrWhiteSpace(txtNome_completo.Text) ? txtNome_completo.Text : string.Empty;
                        ObjCarregado.PF_CPF = ddlTipoPessoa.SelectedValue.Equals("1") ? txtCpf.Text : string.Empty;
                        ObjCarregado.PF_RG = string.Empty;
                        //Datas são feitas em FillObjectDates(); PF_DATA_NASCIMENTO = Convert.ToDateTime(txtData_nascimento.Text),
                        ObjCarregado.PJ_RAZAO_SOCIAL = !string.IsNullOrWhiteSpace(txtRazao_social.Text) ? txtRazao_social.Text : string.Empty;
                        ObjCarregado.PJ_NOME_FANTASIA = !string.IsNullOrWhiteSpace(txtNome_fantasia.Text) ? txtNome_fantasia.Text : string.Empty;
                        ObjCarregado.PJ_CNPJ = ddlTipoPessoa.SelectedValue.Equals("2") ? txtCnpj.Text : string.Empty;
                        ObjCarregado.PJ_IE = string.Empty;
                        //Datas são feitas em FillObjectDates(); PJ_DATA_FUNDACAO = Convert.ToDateTime(txtData_fundacao.Text),
                        ObjCarregado.ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO;

                        FillObjectDates();
                        FillArquivoDigital();
                        UpdatePessoaPessoaClassificacoes();
                        UpdatePessoaContatos();

                        _objBo.Update(ObjCarregado);
                        break;
                    default:
                        break;
                }

                _objBo.SaveChanges();

                _objBo = null; ObjCarregado = null; ContatosCadastrados = null;
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao salvar view. Mensagem: " + ex.Message);
            }
        }

        private void UpdatePessoaContatos()
        {
            try
            {
                if (ContatosCadastrados != null)
                {
                    foreach (PESSOA_CONTATO _objPessoaContato in ContatosCadastrados)
                    {
                        if (_objPessoaContato.ID_PESSOA_CONTATO == 0)
                        {
                            ObjCarregado.PESSOA_CONTATO.Add(new PESSOA_CONTATO()
                            {
                                NOME_CONTATO = _objPessoaContato.NOME_CONTATO,
                                TELEFONE = !string.IsNullOrWhiteSpace(_objPessoaContato.TELEFONE) ? _objPessoaContato.TELEFONE : string.Empty,
                                E_MAIL = !string.IsNullOrWhiteSpace(_objPessoaContato.E_MAIL) ? _objPessoaContato.E_MAIL : string.Empty,
                                SITE = !string.IsNullOrWhiteSpace(_objPessoaContato.SITE) ? _objPessoaContato.SITE : string.Empty,
                                ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO
                            });
                        }
                        else
                        {
                            ObjCarregado.PESSOA_CONTATO.FirstOrDefault(lbda => lbda.ID_PESSOA_CONTATO == _objPessoaContato.ID_PESSOA_CONTATO).NOME_CONTATO = _objPessoaContato.NOME_CONTATO;
                            ObjCarregado.PESSOA_CONTATO.FirstOrDefault(lbda => lbda.ID_PESSOA_CONTATO == _objPessoaContato.ID_PESSOA_CONTATO).TELEFONE = !string.IsNullOrWhiteSpace(_objPessoaContato.TELEFONE) ? _objPessoaContato.TELEFONE : string.Empty;
                            ObjCarregado.PESSOA_CONTATO.FirstOrDefault(lbda => lbda.ID_PESSOA_CONTATO == _objPessoaContato.ID_PESSOA_CONTATO).E_MAIL = !string.IsNullOrWhiteSpace(_objPessoaContato.E_MAIL) ? _objPessoaContato.E_MAIL : string.Empty;
                            ObjCarregado.PESSOA_CONTATO.FirstOrDefault(lbda => lbda.ID_PESSOA_CONTATO == _objPessoaContato.ID_PESSOA_CONTATO).SITE = !string.IsNullOrWhiteSpace(_objPessoaContato.SITE) ? _objPessoaContato.SITE : string.Empty;
                            ObjCarregado.PESSOA_CONTATO.FirstOrDefault(lbda => lbda.ID_PESSOA_CONTATO == _objPessoaContato.ID_PESSOA_CONTATO).ID_SESSAO = _objPessoaContato.ID_SESSAO;
                        }
                    }
                }
            }
            catch (Exception ex) { throw; }
        }

        private void UpdatePessoaPessoaClassificacoes()
        {
            foreach (ListItem _Item in chkPessoaClassificacao.Items)
            {
                if (_Item.Selected && ObjCarregado.PESSOA_PESSOA_CLASSIFICACAO.Count(lbda => lbda.ID_PESSOA_CLASSIFICACAO == int.Parse(_Item.Value)) == 0)
                {
                    ObjCarregado.PESSOA_PESSOA_CLASSIFICACAO.Add(new PESSOA_PESSOA_CLASSIFICACAO()
                    {
                        ID_PESSOA_CLASSIFICACAO = int.Parse(_Item.Value),
                        ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO
                    });
                }
                else if (!_Item.Selected && ObjCarregado.PESSOA_PESSOA_CLASSIFICACAO.Count(lbda => lbda.ID_PESSOA_CLASSIFICACAO == int.Parse(_Item.Value)) > 0)
                    ObjCarregado.PESSOA_PESSOA_CLASSIFICACAO.Remove(ObjCarregado.PESSOA_PESSOA_CLASSIFICACAO.FirstOrDefault(lbda => lbda.ID_PESSOA_CLASSIFICACAO == int.Parse(_Item.Value)));
            }
        }

        private void FillObjectDates()
        {
            if (!string.IsNullOrWhiteSpace(txtData_nascimento.Text))
                ObjCarregado.PF_DATA_NASCIMENTO = Convert.ToDateTime(txtData_nascimento.Text);

            if (!string.IsNullOrWhiteSpace(txtData_fundacao.Text))
                ObjCarregado.PJ_DATA_FUNDACAO = Convert.ToDateTime(txtData_fundacao.Text);
        }

        private void FillArquivoDigital()
        {
            try
            {
                if (ArquivoEnviado != null)
                {
                    if (ObjCarregado.ID_ARQUIVO.HasValue)
                    {
                        ObjCarregado.ARQUIVO_DIGITAL.ARQUIVO = ArquivoEnviado.FileBytes;
                        ObjCarregado.ARQUIVO_DIGITAL.NOME = ArquivoEnviado.FileName;
                        ObjCarregado.ARQUIVO_DIGITAL.EXTENSAO = ArquivoEnviado.FileExtension;
                        ObjCarregado.ARQUIVO_DIGITAL.DATA = DateTime.Now;
                    }
                    else
                    {
                        ObjCarregado.ARQUIVO_DIGITAL = new ARQUIVO_DIGITAL()
                        {
                            ID_ARQUIVO = Guid.NewGuid(),
                            NOME = ArquivoEnviado.FileName,
                            EXTENSAO = ArquivoEnviado.FileExtension,
                            ARQUIVO = ArquivoEnviado.FileBytes,
                            DATA = DateTime.Now
                        };
                    }
                }
            }
            catch (Exception ex) { throw; }
        }

        public void ButtonRefresh_Click(object sender, ImageClickEventArgs e)
        {
            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    //Nada fazer
                    break;
                case enmUserInterfaceAspect.New:
                    ClearFields();
                    break;
                case enmUserInterfaceAspect.Update:
                    ClearFields();
                    break;
                default:
                    break;
            }
        }

        public void ButtonCancel_Click(object sender, ImageClickEventArgs e)
        {
            ClearMemory();
            ViewAspect = enmUserInterfaceAspect.Start;
            UserInterfaceAspectChanged();
        }

        public void ButtonDelete_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void GetForm() { throw new NotImplementedException(); }

        public void SetForm()
        {
            try
            {
                ddlTipoPessoa.SelectedValue = ObjCarregado.ID_PESSOA_TIPO.ToString();
                ddlTipoPessoa_SelectedIndexChanged(ddlTipoPessoa, null);

                switch ((enmTipoPessoa)ObjCarregado.ID_PESSOA_TIPO)
                {
                    case enmTipoPessoa.PessoaFisica:
                        txtCpf.Text = !string.IsNullOrWhiteSpace(ObjCarregado.PF_CPF) ? ObjCarregado.PF_CPF : string.Empty;
                        txtNome_completo.Text = !string.IsNullOrWhiteSpace(ObjCarregado.PF_NOME_COMPLETO) ? ObjCarregado.PF_NOME_COMPLETO : string.Empty;
                        txtApelido.Text = !string.IsNullOrWhiteSpace(ObjCarregado.PF_APELIDO) ? ObjCarregado.PF_APELIDO : ObjCarregado.PF_NOME_COMPLETO;
                        txtData_nascimento.Text = ObjCarregado.PF_DATA_NASCIMENTO.HasValue ? ObjCarregado.PF_DATA_NASCIMENTO.Value.ToShortDateString() : string.Empty;
                        break;
                    case enmTipoPessoa.PessoaJuridica:
                        txtCpf.Text = !string.IsNullOrWhiteSpace(ObjCarregado.PJ_CNPJ) ? ObjCarregado.PJ_CNPJ : string.Empty;
                        txtRazao_social.Text = !string.IsNullOrWhiteSpace(ObjCarregado.PJ_RAZAO_SOCIAL) ? ObjCarregado.PJ_RAZAO_SOCIAL : string.Empty;
                        txtNome_fantasia.Text = !string.IsNullOrWhiteSpace(ObjCarregado.PJ_NOME_FANTASIA) ? ObjCarregado.PJ_NOME_FANTASIA : string.Empty;
                        txtData_fundacao.Text = ObjCarregado.PJ_DATA_FUNDACAO.HasValue ? ObjCarregado.PJ_DATA_FUNDACAO.Value.ToShortDateString() : string.Empty;
                        break;
                }

                ddlSituacao.SelectedValue = ObjCarregado.ID_PESSOA_SITUACAO.ToString();
                txtCodigo.Text = !string.IsNullOrWhiteSpace(ObjCarregado.MATRICULA) ? ObjCarregado.MATRICULA : string.Empty;

                chkPessoaClassificacao.DataBind();
                foreach (PESSOA_PESSOA_CLASSIFICACAO _objPESSOA_PESSOA_CLASSIFICACAO in ObjCarregado.PESSOA_PESSOA_CLASSIFICACAO)
                    chkPessoaClassificacao.Items.FindByValue(_objPESSOA_PESSOA_CLASSIFICACAO.ID_PESSOA_CLASSIFICACAO.ToString()).Selected = true;

                ContatosCadastrados = ObjCarregado.PESSOA_CONTATO;

                CarregarPessoaContatos();

                if (ObjCarregado.ID_ARQUIVO.HasValue)
                {
                    imgFotoLogo.ImageUrl = string.Format("~/ShowFile.aspx?action=1&fileid={0}&width={1}&height={2}", ObjCarregado.ID_ARQUIVO.ToString(), 800, 600);
                    pnlArquivoAtual.Visible = true;
                }
                else
                {
                    pnlArquivoAtual.Visible = false;
                    imgFotoLogo.ImageUrl = "~/ShowFile.aspx?action=1";
                }
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao salvar view. Mensagem: " + ex.Message);
            }
        }

        public void EnableFields() { throw new NotImplementedException(); }

        public void DisableFields() { throw new NotImplementedException(); }

        public void ClearFields()
        {
            try
            {
                txtRazao_social.Text = txtNome_fantasia.Text = txtData_fundacao.Text = string.Empty; //PJ
                txtNome_completo.Text = txtApelido.Text = txtData_nascimento.Text = string.Empty; //PF

                txtCpf.Text = txtCnpj.Text = txtCodigo.Text = string.Empty;
                ddlSituacao.SelectedValue = "1";
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ClearMemory() { ObjCarregado = null; ContatosCadastrados = null; }

        #endregion

        protected void ddlTipoPessoa_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ClearFields();

                switch ((enmTipoPessoa)int.Parse(((DropDownList)sender).SelectedValue))
                {
                    case enmTipoPessoa.PessoaFisica:
                        FormToPF();
                        break;
                    case enmTipoPessoa.PessoaJuridica:
                        FormToPJ();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        private void FormToPF()
        {
            pnlPessoaFisica.Visible = true;
            pnlPessoaJuridica.Visible = false;

            //Validadores PF
            rfvNome_completo.Enabled = rfvApelido.Enabled = true;

            //Validadores PJ
            rfvRazao_social.Enabled = rfvNome_fantasia.Enabled = false;
        }

        private void FormToPJ()
        {
            pnlPessoaFisica.Visible = false;
            pnlPessoaJuridica.Visible = true;

            //Validadores PF
            rfvNome_completo.Enabled = rfvApelido.Enabled = false;

            //Validadores PJ
            rfvRazao_social.Enabled = rfvNome_fantasia.Enabled = true;
        }

        public void ValidateAcess() { throw new NotImplementedException(); }

        protected void gvPessoaClassificacoes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((Literal)e.Row.FindControl("ltrClassificacao")).Text = new PessoaClassificacaoBo().Find(lbda => lbda.ID_PESSOA_CLASSIFICACAO == (int)e.Row.DataItem).First().NOME;
                }
            }
            catch (Exception ex) { throw; }
        }

        #region [ Pessoa Contatos ]

        private void CarregarPessoaContatos()
        {
            gvPessoaContato.DataSource = ContatosCadastrados;
            gvPessoaContato.DataBind();
        }

        private void MapButtonsPessoaContato()
        {
            ucButtonsPessoaContato.ButtonNew.Visible = false;

            ucButtonsPessoaContato.ButtonSave.CausesValidation = false;
            ucButtonsPessoaContato.ButtonSave.Click += new ImageClickEventHandler(ButtonSavePessoaContato_Click);
            ucButtonsPessoaContato.ButtonSave.ImageUrl = "~/Admin/Media/Images/Bars/Buttons/botao_adicionar_1.png";
            ucButtonsPessoaContato.ButtonSave.ToolTip = "Adicionar";
            ucButtonsPessoaContato.ButtonSave.Attributes.Add("onmouseover", "javascript:ChangeButtonsImage('Add', 'over', this);");
            ucButtonsPessoaContato.ButtonSave.Attributes.Add("onmouseout", "javascript:ChangeButtonsImage('Add', 'out', this);");

            ucButtonsPessoaContato.ButtonSaveNew.Visible = false;

            ucButtonsPessoaContato.ButtonCancel.CausesValidation = false;
            ucButtonsPessoaContato.ButtonCancel.Click += new ImageClickEventHandler(ButtonCancelPessoaContato_Click);

            ucButtonsPessoaContato.ButtonRefresh.CausesValidation = false;
            ucButtonsPessoaContato.ButtonRefresh.Click += new ImageClickEventHandler(ButtonRefreshPessoaContato_Click);

            ucButtonsPessoaContato.ButtonDelete.Visible = false;
        }

        public void ButtonSavePessoaContato_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                StringBuilder sbValidacoes = new StringBuilder();

                if (string.IsNullOrWhiteSpace(txtNome_contato.Text))
                    sbValidacoes.AppendLine("Necessário informar o nome do contato.");

                if (sbValidacoes.Length > 0)
                    throw new HTMSiteAdminException(sbValidacoes.ToString());

                if (ContatosCadastrados == null)
                    ContatosCadastrados = new EntityCollection<PESSOA_CONTATO>();

                if (gvPessoaContato.SelectedIndex < 0)
                {
                    ContatosCadastrados.Add(new PESSOA_CONTATO()
                    {
                        NOME_CONTATO = txtNome_contato.Text,
                        TELEFONE = !string.IsNullOrWhiteSpace(txtTelefone.Text) ? txtTelefone.Text : string.Empty,
                        E_MAIL = !string.IsNullOrWhiteSpace(txtE_mail.Text) ? txtE_mail.Text : string.Empty,
                        SITE = !string.IsNullOrWhiteSpace(txtSite.Text) ? txtSite.Text : string.Empty,
                        ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO
                    });
                }
                else
                {
                    ContatosCadastrados.First(lbda => lbda.ID_PESSOA_CONTATO == int.Parse(gvPessoaContato.SelectedDataKey.Values["ID_PESSOA_CONTATO"].ToString())).NOME_CONTATO = txtNome_contato.Text;
                    ContatosCadastrados.First(lbda => lbda.ID_PESSOA_CONTATO == int.Parse(gvPessoaContato.SelectedDataKey.Values["ID_PESSOA_CONTATO"].ToString())).TELEFONE = !string.IsNullOrWhiteSpace(txtTelefone.Text) ? txtTelefone.Text : string.Empty;
                    ContatosCadastrados.First(lbda => lbda.ID_PESSOA_CONTATO == int.Parse(gvPessoaContato.SelectedDataKey.Values["ID_PESSOA_CONTATO"].ToString())).E_MAIL = !string.IsNullOrWhiteSpace(txtE_mail.Text) ? txtE_mail.Text : string.Empty;
                    ContatosCadastrados.First(lbda => lbda.ID_PESSOA_CONTATO == int.Parse(gvPessoaContato.SelectedDataKey.Values["ID_PESSOA_CONTATO"].ToString())).SITE = !string.IsNullOrWhiteSpace(txtSite.Text) ? txtSite.Text : string.Empty;
                    ContatosCadastrados.First(lbda => lbda.ID_PESSOA_CONTATO == int.Parse(gvPessoaContato.SelectedDataKey.Values["ID_PESSOA_CONTATO"].ToString())).ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO;
                }

                gvPessoaContato.DataSource = ContatosCadastrados;
                gvPessoaContato.DataBind();

                ClearFieldsPessoaContato();
            }
            catch (Exception ex) { ucShowExceptionPessoaContato.ShowExceptions(ex); }
        }

        public void ButtonCancelPessoaContato_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ClearFieldsPessoaContato();
                gvPessoaContato.SelectedIndex = -1;
            }
            catch (Exception ex) { throw; }
        }

        public void ButtonRefreshPessoaContato_Click(object sender, ImageClickEventArgs e) { ClearFieldsPessoaContato(); }

        private void ClearFieldsPessoaContato() { txtNome_contato.Text = txtTelefone.Text = txtE_mail.Text = txtSite.Text = string.Empty; }

        protected void gvPessoaContato_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ClearFieldsPessoaContato();

                int _ID_PESSOA_CONTATO = int.Parse(gvPessoaContato.SelectedDataKey.Values["ID_PESSOA_CONTATO"].ToString());
                PESSOA_CONTATO _objPESSOA_CONTATO = ContatosCadastrados.FirstOrDefault(lbda => lbda.ID_PESSOA_CONTATO == _ID_PESSOA_CONTATO);

                txtNome_contato.Text = _objPESSOA_CONTATO.NOME_CONTATO;
                txtTelefone.Text = !string.IsNullOrWhiteSpace(_objPESSOA_CONTATO.TELEFONE) ? _objPESSOA_CONTATO.TELEFONE : string.Empty;
                txtE_mail.Text = !string.IsNullOrWhiteSpace(_objPESSOA_CONTATO.E_MAIL) ? _objPESSOA_CONTATO.E_MAIL : string.Empty;
                txtSite.Text = !string.IsNullOrWhiteSpace(_objPESSOA_CONTATO.SITE) ? _objPESSOA_CONTATO.SITE : string.Empty;

                _objPESSOA_CONTATO = null;
            }
            catch (Exception ex) { ucShowExceptionPessoaContato.ShowExceptions(ex); }
        }

        protected void gvPessoaContato_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int currentPageIndex, _SelectedKey;

                switch (e.CommandName)
                {
                    case "Excluir":
                        _SelectedKey = Convert.ToInt32(e.CommandArgument);

                        PessoaContatoBo _objBo = new PessoaContatoBo();
                        _objBo.Delete(_objBo.Find(lbda => lbda.ID_PESSOA_CONTATO == _SelectedKey).First());
                        _objBo.SaveChanges();
                        _objBo = null;

                        ContatosCadastrados.Remove(ContatosCadastrados.FirstOrDefault(lbda => lbda.ID_PESSOA_CONTATO == _SelectedKey));

                        CarregarPessoaContatos();

                        currentPageIndex = gvView.PageIndex;
                        gvView.DataBind();

                        if (currentPageIndex >= 0 && currentPageIndex <= gvView.PageCount - 1)
                            gvView.PageIndex = currentPageIndex;
                        break;
                }


            }
            catch (Exception ex) { ucShowExceptionPessoaContato.ShowExceptions(ex); }
        }

        #endregion

        #region [ ViewCollection ]

        protected void btnFiltrar_Click(object sender, ImageClickEventArgs e)
        {
            try { gvView.DataBind(); }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void gvView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((Literal)e.Row.FindControl("ltrNomeRazao")).Text = ((PESSOA)e.Row.DataItem).ID_PESSOA_TIPO == (int)enmTipoPessoa.PessoaFisica ? ((PESSOA)e.Row.DataItem).PF_NOME_COMPLETO : ((PESSOA)e.Row.DataItem).PJ_RAZAO_SOCIAL;
                    ((Literal)e.Row.FindControl("ltrSituacao")).Text = new PessoaSituacaoBo().Find(lbda => lbda.ID_PESSOA_SITUACAO == ((PESSOA)e.Row.DataItem).ID_PESSOA_SITUACAO).First().NOME;
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _SelectedKey = Convert.ToInt32(gvView.SelectedDataKey.Values["ID_PESSOA"]);

                ObjCarregado = new PessoaBo().Find(lbda => lbda.ID_PESSOA == _SelectedKey).First();

                ViewAspect = enmUserInterfaceAspect.Update;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int currentPageIndex, _SelectedKey;

                switch (e.CommandName)
                {
                    case "Excluir":
                        _SelectedKey = Convert.ToInt32(e.CommandArgument);

                        PessoaBo _objBo = new PessoaBo();
                        _objBo.Delete(_objBo.Find(lbda => lbda.ID_PESSOA == _SelectedKey).First());
                        _objBo.SaveChanges();
                        _objBo = null;

                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex >= 0 && currentPageIndex <= gvView.PageCount - 1)
                            gvView.PageIndex = currentPageIndex;
                        break;
                    case "FirstPage":
                        gvView.DataBind();
                        gvView.PageIndex = 0;
                        break;
                    case "BackPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex > 0)
                            gvView.PageIndex = currentPageIndex - 1;
                        break;
                    case "GoToPage":
                        currentPageIndex = gvView.PageIndex;

                        int goToTop = 1, goToBottom = 1, goTo = 1;
                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text))
                            goToTop = int.Parse(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text);

                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text))
                            goToBottom = int.Parse(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text);

                        gvView.DataBind();

                        goTo = (goToTop != currentPageIndex + 1) ? goToTop : goToBottom;

                        if (goTo >= 1 && goTo <= gvView.PageCount)
                            gvView.PageIndex = goTo - 1;
                        break;
                    case "NextPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex < gvView.PageCount)
                            gvView.PageIndex = currentPageIndex + 1;
                        break;
                    case "LastPage":
                        gvView.DataBind();
                        gvView.PageIndex = gvView.PageCount - 1;
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void edsView_QueryCreated(object sender, QueryCreatedEventArgs e)
        {
            try
            {
                var result = e.Query.Cast<PESSOA>();

                int _ID_SITUACAO = int.Parse(ddlFiltroSituacao.SelectedValue);

                if (!string.IsNullOrWhiteSpace(txtFiltroNomeRazao.Text))
                {
                    if (!string.IsNullOrWhiteSpace(txtFiltroMatricula.Text))
                    {
                        e.Query = from item in result
                                  where
                                    item.PF_NOME_COMPLETO.Contains(txtFiltroNomeRazao.Text) || item.PJ_RAZAO_SOCIAL.Contains(txtFiltroNomeRazao.Text) || item.PJ_NOME_FANTASIA.Contains(txtFiltroNomeRazao.Text)
                                    && item.MATRICULA.Contains(txtFiltroMatricula.Text)
                                    && item.ID_PESSOA_SITUACAO == _ID_SITUACAO
                                  select item;
                    }
                    else
                    {
                        e.Query = from item in result
                                  where
                                    item.PF_NOME_COMPLETO.Contains(txtFiltroNomeRazao.Text) || item.PJ_RAZAO_SOCIAL.Contains(txtFiltroNomeRazao.Text) || item.PJ_NOME_FANTASIA.Contains(txtFiltroNomeRazao.Text)
                                    && item.ID_PESSOA_SITUACAO == _ID_SITUACAO
                                  select item;
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(txtFiltroMatricula.Text))
                    {
                        e.Query = from item in result
                                  where
                                    item.ID_PESSOA_SITUACAO == _ID_SITUACAO
                                    && item.MATRICULA.Contains(txtFiltroMatricula.Text)
                                  select item;
                    }
                    else
                    {
                        e.Query = from item in result
                                  where
                                    item.ID_PESSOA_SITUACAO == _ID_SITUACAO
                                  select item;
                    }
                }
            }
            catch (Exception ex) { throw; }
        }

        #endregion

        protected void afuArquivoLogoFoto_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e) { ArquivoEnviado = HTMSiteAdmin.Library.UserInterface.WebControlsUtility.AsyncFileUpload_UploadedComplete(sender, e); }
    }
}