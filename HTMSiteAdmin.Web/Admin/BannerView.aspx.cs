﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Business.Banners;
using HTMSiteAdmin.Library.Exceptions;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Data;
using System.Text;
using HTMSiteAdmin.Business.Sistema;
using System.IO;
using HTMSiteAdmin.Library;
using System.Configuration;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class BannerView : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this, ViewToken, "ViewAspect", value); }
        }
        private BANNER ObjCarregado
        {
            get { return (BANNER)Global.RestoreObject(this, ViewToken, "ObjCarregado"); }
            set { Global.SaveObject(this, ViewToken, "ObjCarregado", value); }
        }
        public UploadedFile ArquivoEnviado
        {
            get { return (UploadedFile)Global.RestoreObject(this, ViewToken, "ArquivoEnviado"); }
            set { Global.SaveObject(this, ViewToken, "ArquivoEnviado", value); }
        }
        public UploadedFile ArquivoEnviado_en
        {
            get { return (UploadedFile)Global.RestoreObject(this, ViewToken, "ArquivoEnviado_en"); }
            set { Global.SaveObject(this, ViewToken, "ArquivoEnviado_en", value); }
        }
        public UploadedFile Miniatura
        {
            get { return (UploadedFile)Global.RestoreObject(this, ViewToken, "Miniatura"); }
            set { Global.SaveObject(this, ViewToken, "Miniatura", value); }
        }
        public UploadedFile Miniatura_en
        {
            get { return (UploadedFile)Global.RestoreObject(this, ViewToken, "Miniatura_en"); }
            set { Global.SaveObject(this, ViewToken, "Miniatura_en", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MapButtons();

            if (!IsPostBack && ObjCarregado == null)
                ViewAspect = enmUserInterfaceAspect.Start;

            if (!IsPostBack)
            {
                UserInterfaceAspectChanged();
                gvView.Sort("ID_BANNER_POSICAO", SortDirection.Descending);
            }
        }

        public void ValidateAcess() { throw new NotImplementedException(); }

        public void MapButtons()
        {
            ucButtons1.ButtonNew.CausesValidation = false;
            ucButtons1.ButtonNew.Click += new ImageClickEventHandler(ButtonNew_Click);

            ucButtons1.ButtonSave.ValidationGroup = "vgView";
            ucButtons1.ButtonSave.CausesValidation = true;
            ucButtons1.ButtonSave.Click += new ImageClickEventHandler(ButtonSave_Click);

            ucButtons1.ButtonSaveNew.ValidationGroup = "vgView";
            ucButtons1.ButtonSaveNew.CausesValidation = true;
            ucButtons1.ButtonSaveNew.Click += new ImageClickEventHandler(ButtonSaveNew_Click);

            ucButtons1.ButtonCancel.CausesValidation = false;
            ucButtons1.ButtonCancel.Click += new ImageClickEventHandler(ButtonCancel_Click);

            ucButtons1.ButtonRefresh.CausesValidation = false;
            ucButtons1.ButtonRefresh.Click += new ImageClickEventHandler(ButtonRefresh_Click);

            ucButtons1.ButtonDelete.CausesValidation = false;
            ucButtons1.ButtonDelete.Click += new ImageClickEventHandler(ButtonDelete_Click);
        }

        public void UserInterfaceAspectChanged()
        {
            StartPageControls();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    ucButtons1.ButtonNew.Visible = true;
                    ucButtons1.ButtonSave.Visible = false;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = false;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = false;

                    int currentPageIndex = gvView.PageIndex;
                    gvView.DataBind();
                    gvView.PageIndex = currentPageIndex;

                    pnlViewCollection.Visible = gvView.Rows.Count > 0;
                    break;
                case enmUserInterfaceAspect.New:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = true;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    RestartForm();
                    break;
                case enmUserInterfaceAspect.Update:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    ClearFields();

                    SetForm();
                    break;
                default:
                    break;
            }
        }

        public void RestartForm()
        {
            ClearMemory();
            ClearFields();
        }

        public void StartPageControls()
        {
            try
            {
                if (!IsPostBack)
                {

                    ddlId_banner_posicao.DataBind();
                    ddlFiltroBannerPosicao.DataBind();
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void SaveView()
        {
            StringBuilder sbValidacoes = new StringBuilder();

            if (ViewAspect == enmUserInterfaceAspect.New && ArquivoEnviado == null)
                sbValidacoes.AppendLine("Necessário informar um arquivo para criar um novo banner.");

            if (!string.IsNullOrWhiteSpace(txtData_publicacao_inicio.Text)
                && !string.IsNullOrWhiteSpace(txtData_publicacao_fim.Text)
                && DateTime.Compare(Convert.ToDateTime(txtData_publicacao_inicio.Text).Date, Convert.ToDateTime(txtData_publicacao_fim.Text).Date) >= 0)
                sbValidacoes.AppendLine("A data de fim da publicação deve ser maior que a data de início.");

            if (sbValidacoes.Length > 0)
                throw new HTMSiteAdminException(sbValidacoes.ToString());

            BannerBo _objBo = new BannerBo();

            try
            {
                switch (ViewAspect)
                {
                    case enmUserInterfaceAspect.Start:
                        break;
                    case enmUserInterfaceAspect.New:
                        ObjCarregado = new BANNER()
                        {
                            ID_BANNER_POSICAO = int.Parse(ddlId_banner_posicao.SelectedValue),
                            ID_BANNER_SITUACAO = int.Parse(ddlSituacao.SelectedValue),
                            TITULO = !string.IsNullOrWhiteSpace(txtTitulo.Text) ? txtTitulo.Text : string.Empty,
                            TITULO_EN = !string.IsNullOrWhiteSpace(txtTitulo_en.Text) ? txtTitulo_en.Text : string.Empty,
                            SUB_TITULO = !string.IsNullOrWhiteSpace(txtSubTitulo.Text) ? txtSubTitulo.Text : string.Empty,
                            SUB_TITULO_EN = !string.IsNullOrWhiteSpace(txtSubTitulo_en.Text) ? txtSubTitulo_en.Text : string.Empty,
                            DESCRICAO = !string.IsNullOrWhiteSpace(txtDescricao.Text) ? txtDescricao.Text : string.Empty,
                            CAMINHO = !string.IsNullOrWhiteSpace(txtCaminho.Text) ? txtCaminho.Text : string.Empty,
                            CAMINHO_EN = !string.IsNullOrWhiteSpace(txtCaminho_en.Text) ? txtCaminho_en.Text : string.Empty,
                            ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO
                        };

                        FillObjectDates();

                        FillArquivoDigital();

                        _objBo.Add(ObjCarregado);
                        break;
                    case enmUserInterfaceAspect.Update:
                        ObjCarregado = _objBo.Find(lbda => lbda.ID_BANNER == ObjCarregado.ID_BANNER).First();

                        ObjCarregado.ID_BANNER_POSICAO = int.Parse(ddlId_banner_posicao.SelectedValue);
                        ObjCarregado.ID_BANNER_SITUACAO = int.Parse(ddlSituacao.SelectedValue);
                        ObjCarregado.TITULO = !string.IsNullOrWhiteSpace(txtTitulo.Text) ? txtTitulo.Text : string.Empty;
                        ObjCarregado.TITULO_EN = !string.IsNullOrWhiteSpace(txtTitulo_en.Text) ? txtTitulo_en.Text : string.Empty;
                        ObjCarregado.SUB_TITULO = !string.IsNullOrWhiteSpace(txtSubTitulo.Text) ? txtSubTitulo.Text : string.Empty;
                        ObjCarregado.SUB_TITULO_EN = !string.IsNullOrWhiteSpace(txtSubTitulo_en.Text) ? txtSubTitulo_en.Text : string.Empty;
                        ObjCarregado.DESCRICAO = !string.IsNullOrWhiteSpace(txtDescricao.Text) ? txtDescricao.Text : string.Empty;
                        ObjCarregado.CAMINHO = !string.IsNullOrWhiteSpace(txtCaminho.Text) ? txtCaminho.Text : string.Empty;
                        ObjCarregado.CAMINHO_EN = !string.IsNullOrWhiteSpace(txtCaminho_en.Text) ? txtCaminho_en.Text : string.Empty;
                        ObjCarregado.ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO;

                        FillObjectDates();

                        FillArquivoDigital();

                        _objBo.Update(ObjCarregado);
                        break;
                    default:
                        break;
                }

                _objBo.SaveChanges();
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao salvar os dados.");
            }
            finally { _objBo = null; ObjCarregado = null; ArquivoEnviado = null; ArquivoEnviado_en = null; Miniatura = null; Miniatura_en = null; }
        }

        private void FillObjectDates()
        {
            if (!string.IsNullOrWhiteSpace(txtData_publicacao_inicio.Text))
                ObjCarregado.DATA_PUBLICACAO_INICIO = Convert.ToDateTime(txtData_publicacao_inicio.Text);
            else
                ObjCarregado.DATA_PUBLICACAO_INICIO = null;

            if (!string.IsNullOrWhiteSpace(txtData_publicacao_fim.Text))
                ObjCarregado.DATA_PUBLICACAO_FIM = Convert.ToDateTime(txtData_publicacao_fim.Text);
            else
                ObjCarregado.DATA_PUBLICACAO_FIM = null;
        }



        private void FillArquivoDigital()
        {
            string path = Server.MapPath(ConfigurationManager.AppSettings["RELATIVEPATH_ARQUIVODIGITAL"]);

            try
            {
                if (ArquivoEnviado != null)
                {
                    if (ObjCarregado.ID_ARQUIVO.HasValue)
                    {
                        Util.SaveFile(path, ObjCarregado.ID_ARQUIVO.ToString(), ArquivoEnviado.FileExtension, ArquivoEnviado.FileBytes);
                        //ObjCarregado.ARQUIVO_DIGITAL.ARQUIVO = ArquivoEnviado.FileBytes;
                        ObjCarregado.ARQUIVO_DIGITAL.NOME = ArquivoEnviado.FileName;
                        ObjCarregado.ARQUIVO_DIGITAL.EXTENSAO = ArquivoEnviado.FileExtension;
                        ObjCarregado.ARQUIVO_DIGITAL.DATA = DateTime.Now;
                    }
                    else
                    {
                        ObjCarregado.ARQUIVO_DIGITAL = new ARQUIVO_DIGITAL()
                        {
                            ID_ARQUIVO = Guid.NewGuid(),
                            NOME = ArquivoEnviado.FileName,
                            EXTENSAO = ArquivoEnviado.FileExtension,
                            //ARQUIVO = ArquivoEnviado.FileBytes,
                            DATA = DateTime.Now
                        };

                        Util.SaveFile(path, ObjCarregado.ARQUIVO_DIGITAL.ID_ARQUIVO.ToString(), ArquivoEnviado.FileExtension, ArquivoEnviado.FileBytes);
                    }
                }

                if (ArquivoEnviado_en != null)
                {
                    if (ObjCarregado.ID_ARQUIVO_EN.HasValue)
                    {
                        Util.SaveFile(path, ObjCarregado.ID_ARQUIVO_EN.ToString(), ArquivoEnviado_en.FileExtension, ArquivoEnviado_en.FileBytes);
                        //ObjCarregado.ARQUIVO_DIGITAL1.ARQUIVO = ArquivoEnviado_en.FileBytes;
                        ObjCarregado.ARQUIVO_DIGITAL1.NOME = ArquivoEnviado_en.FileName;
                        ObjCarregado.ARQUIVO_DIGITAL1.EXTENSAO = ArquivoEnviado_en.FileExtension;
                        ObjCarregado.ARQUIVO_DIGITAL1.DATA = DateTime.Now;
                    }
                    else
                    {
                        ObjCarregado.ARQUIVO_DIGITAL1 = new ARQUIVO_DIGITAL()
                        {
                            ID_ARQUIVO = Guid.NewGuid(),
                            NOME = ArquivoEnviado_en.FileName,
                            EXTENSAO = ArquivoEnviado_en.FileExtension,
                            //ARQUIVO = ArquivoEnviado_en.FileBytes,
                            DATA = DateTime.Now
                        };

                        Util.SaveFile(path, ObjCarregado.ARQUIVO_DIGITAL1.ID_ARQUIVO.ToString(), ArquivoEnviado_en.FileExtension, ArquivoEnviado_en.FileBytes);
                    }
                }

                if (Miniatura != null)
                {
                    if (ObjCarregado.ID_MINIATURA.HasValue)
                    {
                        Util.SaveFile(path, ObjCarregado.ID_MINIATURA.ToString(), Miniatura.FileExtension, Miniatura.FileBytes);
                        //ObjCarregado.ARQUIVO_DIGITAL2.ARQUIVO = Miniatura.FileBytes;
                        ObjCarregado.ARQUIVO_DIGITAL2.NOME = Miniatura.FileName;
                        ObjCarregado.ARQUIVO_DIGITAL2.EXTENSAO = Miniatura.FileExtension;
                        ObjCarregado.ARQUIVO_DIGITAL2.DATA = DateTime.Now;
                    }
                    else
                    {
                        ObjCarregado.ARQUIVO_DIGITAL2 = new ARQUIVO_DIGITAL()
                        {
                            ID_ARQUIVO = Guid.NewGuid(),
                            NOME = Miniatura.FileName,
                            EXTENSAO = Miniatura.FileExtension,
                            //ARQUIVO = Miniatura.FileBytes,
                            DATA = DateTime.Now
                        };

                        Util.SaveFile(path, ObjCarregado.ARQUIVO_DIGITAL2.ID_ARQUIVO.ToString(), Miniatura.FileExtension, Miniatura.FileBytes);
                    }
                }

                if (Miniatura_en != null)
                {
                    if (ObjCarregado.ID_MINIATURA_EN.HasValue)
                    {
                        Util.SaveFile(path, ObjCarregado.ID_MINIATURA_EN.ToString(), Miniatura_en.FileExtension, Miniatura_en.FileBytes);

                        //ObjCarregado.ARQUIVO_DIGITAL3.ARQUIVO = Miniatura_en.FileBytes;
                        ObjCarregado.ARQUIVO_DIGITAL3.NOME = Miniatura_en.FileName;
                        ObjCarregado.ARQUIVO_DIGITAL3.EXTENSAO = Miniatura_en.FileExtension;
                        ObjCarregado.ARQUIVO_DIGITAL3.DATA = DateTime.Now;
                    }
                    else
                    {
                        ObjCarregado.ARQUIVO_DIGITAL3 = new ARQUIVO_DIGITAL()
                        {
                            ID_ARQUIVO = Guid.NewGuid(),
                            NOME = Miniatura_en.FileName,
                            EXTENSAO = Miniatura_en.FileExtension,
                            //ARQUIVO = Miniatura_en.FileBytes,
                            DATA = DateTime.Now
                        };

                        Util.SaveFile(path, ObjCarregado.ARQUIVO_DIGITAL3.ID_ARQUIVO.ToString(), Miniatura_en.FileExtension, Miniatura_en.FileBytes);
                    }
                }
            }
            catch (Exception ex) { throw; }
        }

        public void ButtonSaveNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonRefresh_Click(object sender, ImageClickEventArgs e)
        {
            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    //Nada fazer
                    break;
                case enmUserInterfaceAspect.New:
                    ClearFields();
                    break;
                case enmUserInterfaceAspect.Update:
                    ClearFields();
                    break;
                default:
                    break;
            }
        }

        public void ButtonCancel_Click(object sender, ImageClickEventArgs e)
        {
            ClearMemory();
            ViewAspect = enmUserInterfaceAspect.Start;
            UserInterfaceAspectChanged();
        }

        public void ButtonDelete_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void GetForm() { throw new NotImplementedException(); }

        public void SetForm()
        {
            try
            {
                ddlId_banner_posicao.DataBind();
                ddlId_banner_posicao.SelectedValue = ObjCarregado.ID_BANNER_POSICAO.ToString();
                ddlSituacao.SelectedValue = ObjCarregado.ID_BANNER_SITUACAO.ToString();

                txtTitulo.Text = !string.IsNullOrWhiteSpace(ObjCarregado.TITULO) ? ObjCarregado.TITULO : string.Empty;
                txtTitulo_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.TITULO_EN) ? ObjCarregado.TITULO_EN : string.Empty;
                txtSubTitulo.Text = !string.IsNullOrWhiteSpace(ObjCarregado.SUB_TITULO) ? ObjCarregado.SUB_TITULO : string.Empty;
                txtSubTitulo_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.SUB_TITULO_EN) ? ObjCarregado.SUB_TITULO_EN : string.Empty;
                txtDescricao.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO) ? ObjCarregado.DESCRICAO : string.Empty;
                txtCaminho.Text = !string.IsNullOrWhiteSpace(ObjCarregado.CAMINHO) ? ObjCarregado.CAMINHO : string.Empty;
                txtCaminho_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.CAMINHO_EN) ? ObjCarregado.CAMINHO_EN : string.Empty;
                txtData_publicacao_inicio.Text = ObjCarregado.DATA_PUBLICACAO_INICIO.Value.ToShortDateString();
                txtData_publicacao_fim.Text = ObjCarregado.DATA_PUBLICACAO_FIM.Value.ToShortDateString();

                CarregarArquivosDigitais();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        private void CarregarArquivosDigitais()
        {
            if (ObjCarregado.ID_ARQUIVO.HasValue)
            {
                imgBanner.ImageUrl = string.Format("~/ShowFile.aspx?action=1&fileid={0}&width={1}&height={2}", ObjCarregado.ID_ARQUIVO.ToString(), ObjCarregado.BANNER_POSICAO.LARGURA_MAXIMA.Value, ObjCarregado.BANNER_POSICAO.ALTURA_MAXIMA.Value);
                pnlArquivoAtual.Visible = true;
            }
            else
            {
                pnlArquivoAtual.Visible = false;
                imgBanner.ImageUrl = "~/ShowFile.aspx?action=1";
            }

            if (ObjCarregado.ID_ARQUIVO_EN.HasValue)
            {
                imgBanner_en.ImageUrl = string.Format("~/ShowFile.aspx?action=1&fileid={0}&width={1}&height={2}", ObjCarregado.ID_ARQUIVO_EN.ToString(), ObjCarregado.BANNER_POSICAO.LARGURA_MAXIMA.Value, ObjCarregado.BANNER_POSICAO.ALTURA_MAXIMA.Value);
                pnlArquivoAtual_en.Visible = true;
            }
            else
            {
                pnlArquivoAtual_en.Visible = false;
                imgBanner_en.ImageUrl = "~/ShowFile.aspx?action=1";
            }

            if (ObjCarregado.ID_MINIATURA.HasValue)
            {
                imgBannerMiniatura.ImageUrl = string.Format("~/ShowFile.aspx?action=1&fileid={0}&width={1}&height={2}", ObjCarregado.ID_MINIATURA.ToString(), 100, 50);
                pnlMiniatura.Visible = true;
            }
            else
            {
                pnlMiniatura.Visible = false;
                imgBannerMiniatura.ImageUrl = "~/ShowFile.aspx?action=1";
            }

            if (ObjCarregado.ID_MINIATURA_EN.HasValue)
            {
                imgBannerMiniatura_en.ImageUrl = string.Format("~/ShowFile.aspx?action=1&fileid={0}&width={1}&height={2}", ObjCarregado.ID_MINIATURA_EN.ToString(), 100, 50);
                pnlMiniatura_en.Visible = true;
            }
            else
            {
                pnlMiniatura_en.Visible = false;
                imgBannerMiniatura_en.ImageUrl = "~/ShowFile.aspx?action=1";
            }
        }

        public void EnableFields() { throw new NotImplementedException(); }

        public void DisableFields() { throw new NotImplementedException(); }

        public void ClearFields()
        {
            try
            {
                //Dados Básicos
                txtTitulo.Text = txtDescricao.Text = txtData_publicacao_inicio.Text = txtData_publicacao_fim.Text = txtCaminho.Text = txtCaminho_en.Text = string.Empty;
                ddlId_banner_posicao.DataBind();
                ddlId_banner_posicao.SelectedValue = "0";

                pnlArquivoAtual.Visible = false;
                imgBanner.ImageUrl = string.Empty;
                imgBanner_en.ImageUrl = string.Empty;

                ddlSituacao.SelectedValue = "1";
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ClearMemory() { ObjCarregado = null; ArquivoEnviado = null; ArquivoEnviado_en = null; Miniatura = null; Miniatura_en = null; }

        protected void ddlId_banner_posicao_DataBound(object sender, EventArgs e)
        {
            try
            {
                HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender);

                IQueryable<BANNER_POSICAO> BannerPosicoes = new BannerPosicaoBo().GetAll();

                if (BannerPosicoes != null)
                {
                    foreach (BANNER_POSICAO _BannerPosicao in BannerPosicoes)
                        HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_AddItem(sender, _BannerPosicao.NOME, _BannerPosicao.ID_BANNER_POSICAO.ToString());
                }

                BannerPosicoes = null;
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void edsView_QueryCreated(object sender, QueryCreatedEventArgs e)
        {
            try
            {
                var result = e.Query.Cast<BANNER>();

                int _ID_BANNER_POSICAO = int.Parse(ddlFiltroBannerPosicao.SelectedValue);
                int _ID_BANNER_SITUACAO = int.Parse(ddlFiltroSituacao.SelectedValue);

                if (!string.IsNullOrWhiteSpace(txtFiltroDescricao.Text))
                {
                    if (!ddlFiltroBannerPosicao.SelectedValue.Equals("0"))
                    {
                        e.Query = from item in result
                                  where
                                    item.TITULO.Contains(txtFiltroDescricao.Text) || item.DESCRICAO.Contains(txtFiltroDescricao.Text)
                                    && item.ID_BANNER_POSICAO == _ID_BANNER_POSICAO
                                    && item.ID_BANNER_SITUACAO == _ID_BANNER_SITUACAO
                                  select item;
                    }
                    else
                    {
                        e.Query = from item in result
                                  where
                                    item.TITULO.Contains(txtFiltroDescricao.Text) || item.DESCRICAO.Contains(txtFiltroDescricao.Text)
                                    && item.ID_BANNER_SITUACAO == _ID_BANNER_SITUACAO
                                  select item;
                    }
                }
                else
                {
                    if (!ddlFiltroBannerPosicao.SelectedValue.Equals("0"))
                    {
                        e.Query = from item in result
                                  where
                                    item.ID_BANNER_POSICAO == _ID_BANNER_POSICAO
                                    && item.ID_BANNER_SITUACAO == _ID_BANNER_SITUACAO
                                  select item;
                    }
                    else
                    {
                        e.Query = from item in result
                                  where
                                    item.ID_BANNER_SITUACAO == _ID_BANNER_SITUACAO
                                  select item;
                    }
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((Literal)e.Row.FindControl("ltrPosicao")).Text = new BannerPosicaoBo().Find(lbda => lbda.ID_BANNER_POSICAO == ((BANNER)e.Row.DataItem).ID_BANNER_POSICAO).First().NOME;
                    ((Literal)e.Row.FindControl("ltrSituacao")).Text = new BannerSituacaoBo().Find(lbda => lbda.ID_BANNER_SITUACAO == ((BANNER)e.Row.DataItem).ID_BANNER_SITUACAO).First().NOME;
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void btnFiltrar_Click(object sender, ImageClickEventArgs e)
        {
            try { gvView.DataBind(); }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void gvView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _SelectedKey = Convert.ToInt32(gvView.SelectedDataKey.Values["ID_BANNER"]);

                ObjCarregado = new BannerBo().Find(lbda => lbda.ID_BANNER == _SelectedKey).First();

                ViewAspect = enmUserInterfaceAspect.Update;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int currentPageIndex, _SelectedKey;

                switch (e.CommandName)
                {
                    case "Excluir":
                        _SelectedKey = _SelectedKey = Convert.ToInt32(e.CommandArgument);

                        if (new BannerEstatisticaBo().Find(lbda => lbda.ID_BANNER == _SelectedKey).Count() > 0)
                            throw new HTMSiteAdminException("Registro está sendo utilizado e não poderá ser excluído, remova todas as referências a este registro, depois tente novamente.");

                        BannerBo _objBo = new BannerBo();
                        _objBo.Delete(_objBo.Find(lbda => lbda.ID_BANNER == _SelectedKey).First());
                        _objBo.SaveChanges();
                        _objBo = null;

                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex >= 0 && currentPageIndex <= gvView.PageCount - 1)
                            gvView.PageIndex = currentPageIndex;
                        break;
                    case "FirstPage":
                        gvView.DataBind();
                        gvView.PageIndex = 0;
                        break;
                    case "BackPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex > 0)
                            gvView.PageIndex = currentPageIndex - 1;
                        break;
                    case "GoToPage":
                        currentPageIndex = gvView.PageIndex;

                        int goToTop = 1, goToBottom = 1, goTo = 1;
                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text))
                            goToTop = int.Parse(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text);

                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text))
                            goToBottom = int.Parse(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text);

                        gvView.DataBind();

                        goTo = (goToTop != currentPageIndex + 1) ? goToTop : goToBottom;

                        if (goTo >= 1 && goTo <= gvView.PageCount)
                            gvView.PageIndex = goTo - 1;
                        break;
                    case "NextPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex < gvView.PageCount)
                            gvView.PageIndex = currentPageIndex + 1;
                        break;
                    case "LastPage":
                        gvView.DataBind();
                        gvView.PageIndex = gvView.PageCount - 1;
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void afuArquivoBanner_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e) { ArquivoEnviado = HTMSiteAdmin.Library.UserInterface.WebControlsUtility.AsyncFileUpload_UploadedComplete(sender, e); }

        protected void afuArquivoBanner_en_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e) { ArquivoEnviado_en = HTMSiteAdmin.Library.UserInterface.WebControlsUtility.AsyncFileUpload_UploadedComplete(sender, e); }

        protected void afuMiniatura_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e) { Miniatura = HTMSiteAdmin.Library.UserInterface.WebControlsUtility.AsyncFileUpload_UploadedComplete(sender, e); }

        protected void afuMiniatura_en_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e) { Miniatura_en = HTMSiteAdmin.Library.UserInterface.WebControlsUtility.AsyncFileUpload_UploadedComplete(sender, e); }

        protected void btnExcluirImagem_Click(object sender, ImageClickEventArgs e)
        {
            BannerBo _objBo = new BannerBo();

            ObjCarregado = _objBo.Find(s => s.ID_BANNER == ObjCarregado.ID_BANNER).First();

            ARQUIVO_DIGITAL _objArquivoDigital = ObjCarregado.ARQUIVO_DIGITAL;

            ObjCarregado.ID_ARQUIVO = null;

            _objBo.Update(ObjCarregado);
            _objBo.SaveChanges();

            ArquivoDigitalBo _objArquivoBo = new ArquivoDigitalBo();
            _objArquivoDigital = _objArquivoBo.Find(s => s.ID_ARQUIVO == _objArquivoDigital.ID_ARQUIVO).First();
            _objArquivoBo.Delete(_objArquivoDigital);
            _objArquivoBo.SaveChanges();

            ObjCarregado = _objBo.Find(s => s.ID_BANNER == ObjCarregado.ID_BANNER).First();

            _objArquivoDigital = null; _objBo = null; _objArquivoBo = null;

            CarregarArquivosDigitais();
        }

        protected void btnExcluirImagemEn_Click(object sender, ImageClickEventArgs e)
        {
            BannerBo _objBo = new BannerBo();

            ObjCarregado = _objBo.Find(s => s.ID_BANNER == ObjCarregado.ID_BANNER).First();

            ARQUIVO_DIGITAL _objArquivoDigital = ObjCarregado.ARQUIVO_DIGITAL1;

            ObjCarregado.ID_ARQUIVO_EN = null;

            _objBo.Update(ObjCarregado);
            _objBo.SaveChanges();

            ArquivoDigitalBo _objArquivoBo = new ArquivoDigitalBo();
            _objArquivoDigital = _objArquivoBo.Find(s => s.ID_ARQUIVO == _objArquivoDigital.ID_ARQUIVO).First();
            _objArquivoBo.Delete(_objArquivoDigital);
            _objArquivoBo.SaveChanges();

            ObjCarregado = _objBo.Find(s => s.ID_BANNER == ObjCarregado.ID_BANNER).First();

            _objArquivoDigital = null; _objBo = null; _objArquivoBo = null;

            CarregarArquivosDigitais();
        }

        protected void btnExluirMiniatura_Click(object sender, ImageClickEventArgs e)
        {
            BannerBo _objBo = new BannerBo();

            ObjCarregado = _objBo.Find(s => s.ID_BANNER == ObjCarregado.ID_BANNER).First();

            ARQUIVO_DIGITAL _objArquivoDigital = ObjCarregado.ARQUIVO_DIGITAL2;

            ObjCarregado.ID_MINIATURA = null;

            _objBo.Update(ObjCarregado);
            _objBo.SaveChanges();

            ArquivoDigitalBo _objArquivoBo = new ArquivoDigitalBo();
            _objArquivoDigital = _objArquivoBo.Find(s => s.ID_ARQUIVO == _objArquivoDigital.ID_ARQUIVO).First();
            _objArquivoBo.Delete(_objArquivoDigital);
            _objArquivoBo.SaveChanges();

            ObjCarregado = _objBo.Find(s => s.ID_BANNER == ObjCarregado.ID_BANNER).First();

            _objArquivoDigital = null; _objBo = null; _objArquivoBo = null;

            CarregarArquivosDigitais();
        }

        protected void btnExcluirMiniaturaEn_Click(object sender, ImageClickEventArgs e)
        {
            BannerBo _objBo = new BannerBo();

            ObjCarregado = _objBo.Find(s => s.ID_BANNER == ObjCarregado.ID_BANNER).First();

            ARQUIVO_DIGITAL _objArquivoDigital = ObjCarregado.ARQUIVO_DIGITAL3;

            ObjCarregado.ID_MINIATURA_EN = null;

            _objBo.Update(ObjCarregado);
            _objBo.SaveChanges();

            ArquivoDigitalBo _objArquivoBo = new ArquivoDigitalBo();
            _objArquivoDigital = _objArquivoBo.Find(s => s.ID_ARQUIVO == _objArquivoDigital.ID_ARQUIVO).First();
            _objArquivoBo.Delete(_objArquivoDigital);
            _objArquivoBo.SaveChanges();

            ObjCarregado = _objBo.Find(s => s.ID_BANNER == ObjCarregado.ID_BANNER).First();

            _objArquivoDigital = null; _objBo = null; _objArquivoBo = null;

            CarregarArquivosDigitais();
        }
    }
}