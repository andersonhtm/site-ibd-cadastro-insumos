﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Importacao;
using HTMSiteAdmin.Library.Exceptions;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class ImportacaoClienteView : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this, ViewToken, "ViewAspect", value); }
        }
        private IMPORTACAO_CLIENTE ObjCarregado
        {
            get { return (IMPORTACAO_CLIENTE)Global.RestoreObject(this, ViewToken, "ObjCarregado"); }
            set { Global.SaveObject(this, ViewToken, "ObjCarregado", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MapButtons();

            if (!IsPostBack && ObjCarregado == null)
                ViewAspect = enmUserInterfaceAspect.Start;

            if (!IsPostBack) UserInterfaceAspectChanged();
        }

        public void ValidateAcess() { throw new NotImplementedException(); }

        public void MapButtons()
        {
            ucButtons1.ButtonNew.CausesValidation = false;
            ucButtons1.ButtonNew.Click += new ImageClickEventHandler(ButtonNew_Click);

            ucButtons1.ButtonSave.ValidationGroup = "vgView";
            ucButtons1.ButtonSave.CausesValidation = true;
            ucButtons1.ButtonSave.Click += new ImageClickEventHandler(ButtonSave_Click);

            ucButtons1.ButtonSaveNew.ValidationGroup = "vgView";
            ucButtons1.ButtonSaveNew.CausesValidation = true;
            ucButtons1.ButtonSaveNew.Click += new ImageClickEventHandler(ButtonSaveNew_Click);

            ucButtons1.ButtonCancel.CausesValidation = false;
            ucButtons1.ButtonCancel.Click += new ImageClickEventHandler(ButtonCancel_Click);

            ucButtons1.ButtonRefresh.CausesValidation = false;
            ucButtons1.ButtonRefresh.Click += new ImageClickEventHandler(ButtonRefresh_Click);

            ucButtons1.ButtonDelete.CausesValidation = false;
            ucButtons1.ButtonDelete.Click += new ImageClickEventHandler(ButtonDelete_Click);
        }

        public void UserInterfaceAspectChanged()
        {
            StartPageControls();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    ucButtons1.ButtonNew.Visible = true;
                    ucButtons1.ButtonSave.Visible = false;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = false;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = false;

                    int currentPageIndex = gvView.PageIndex;
                    gvView.DataBind();
                    gvView.PageIndex = currentPageIndex;

                    pnlViewCollection.Visible = gvView.Rows.Count > 0;
                    break;
                case enmUserInterfaceAspect.New:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = true;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    RestartForm();
                    break;
                case enmUserInterfaceAspect.Update:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    ClearFields();

                    SetForm();
                    break;
                default:
                    break;
            }
        }

        public void StartPageControls()
        {
            try
            {
                if (!IsPostBack) { }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void RestartForm()
        {
            ClearMemory();
            ClearFields();
        }

        public void ButtonNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSaveNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonRefresh_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                switch (ViewAspect)
                {
                    case enmUserInterfaceAspect.Start:
                        //Nada fazer
                        break;
                    case enmUserInterfaceAspect.New:
                        ClearFields();
                        break;
                    case enmUserInterfaceAspect.Update:
                        ClearFields();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonCancel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ClearMemory();
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonDelete_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void GetForm() { throw new NotImplementedException(); }

        public void SaveView()
        {
            try
            {
                ImportacaoClienteBo _objBo = new ImportacaoClienteBo();

                switch (ViewAspect)
                {
                    case enmUserInterfaceAspect.Start:
                        break;
                    case enmUserInterfaceAspect.New:
                        ObjCarregado = new IMPORTACAO_CLIENTE()
                        {
                            ID_CLIENTE = Guid.NewGuid().ToString(),
                            PF_PJ = int.Parse(ddlPf_pj.SelectedValue),
                            MATRICULA = !string.IsNullOrWhiteSpace(txtMatricula.Text) ? txtMatricula.Text : string.Empty,
                            NOME_RAZAO = !string.IsNullOrWhiteSpace(txtNome_razao.Text) ? txtNome_razao.Text : string.Empty,
                            APELIDO_FANTASIA = !string.IsNullOrWhiteSpace(txtApelido_fantasia.Text) ? txtApelido_fantasia.Text : string.Empty,
                            CPF_CNPJ = !string.IsNullOrWhiteSpace(txtCpf_cnpj.Text) ? txtCpf_cnpj.Text.Replace(".", "").Replace("-", "").Replace("/", "") : string.Empty,
                            RG_IE = !string.IsNullOrWhiteSpace(txtRg_ie.Text) ? txtRg_ie.Text.Replace(".", "").Replace("-", "").Replace("/", "") : string.Empty,
                            CEP = !string.IsNullOrWhiteSpace(txtCep.Text) ? txtCep.Text.Replace(".", "").Replace("-", "").Replace("/", "") : string.Empty,
                            ENDERECO = !string.IsNullOrWhiteSpace(txtEndereco.Text) ? txtEndereco.Text : string.Empty,
                            MUNICIPIO = !string.IsNullOrWhiteSpace(txtMunicipio.Text) ? txtMunicipio.Text : string.Empty,
                            ESTADO = !string.IsNullOrWhiteSpace(txtEstado.Text) ? txtEstado.Text : string.Empty,
                            ESTADO_SIGLA = !string.IsNullOrWhiteSpace(txtEstado_sigla.Text) ? txtEstado_sigla.Text : string.Empty,
                            PAIS = !string.IsNullOrWhiteSpace(txtPais.Text) ? txtPais.Text : string.Empty,
                            PAIS_EN = !string.IsNullOrWhiteSpace(txtPais_en.Text) ? txtPais_en.Text : string.Empty,
                            //PAIS_ES = !string.IsNullOrWhiteSpace(txtPais_es.Text) ? txtPais_es.Text : string.Empty,
                            EMAIL = !string.IsNullOrWhiteSpace(txtEmail.Text) ? txtEmail.Text : string.Empty,
                            DDI = !string.IsNullOrWhiteSpace(txtDdi.Text) ? txtDdi.Text : string.Empty,
                            TELEFONE = !string.IsNullOrWhiteSpace(txtTelefone.Text) ? txtTelefone.Text : string.Empty,
                            FAX = !string.IsNullOrWhiteSpace(txtFax.Text) ? txtFax.Text : string.Empty,
                            NUMERO = !string.IsNullOrWhiteSpace(txtNumero.Text) ? txtNumero.Text : string.Empty,
                            COMPLEMENTO = !string.IsNullOrWhiteSpace(txtComplemento.Text) ? txtComplemento.Text : string.Empty,
                            CXPOSTAL = !string.IsNullOrWhiteSpace(txtCxpostal.Text) ? txtCxpostal.Text : string.Empty,
                            LOGRADOURO = !string.IsNullOrWhiteSpace(txtLogradouro.Text) ? txtLogradouro.Text : string.Empty,
                            CELULAR = !string.IsNullOrWhiteSpace(txtCelular.Text) ? txtCelular.Text : string.Empty,
                            SITE = !string.IsNullOrWhiteSpace(txtSite.Text) ? txtSite.Text : string.Empty,
                            REGISTRO_ORIGEM = 1,
                            ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO
                        };

                        _objBo.Add(ObjCarregado);
                        break;
                    case enmUserInterfaceAspect.Update:
                        ObjCarregado = _objBo.Find(lbda => lbda.ID_CLIENTE == ObjCarregado.ID_CLIENTE).First();

                        ObjCarregado.PF_PJ = int.Parse(ddlPf_pj.SelectedValue);
                        ObjCarregado.MATRICULA = !string.IsNullOrWhiteSpace(txtMatricula.Text) ? txtMatricula.Text : string.Empty;
                        ObjCarregado.NOME_RAZAO = !string.IsNullOrWhiteSpace(txtNome_razao.Text) ? txtNome_razao.Text : string.Empty;
                        ObjCarregado.APELIDO_FANTASIA = !string.IsNullOrWhiteSpace(txtApelido_fantasia.Text) ? txtApelido_fantasia.Text : string.Empty;
                        ObjCarregado.CPF_CNPJ = !string.IsNullOrWhiteSpace(txtCpf_cnpj.Text) ? txtCpf_cnpj.Text.Replace(".", "").Replace("-", "").Replace("/", "") : string.Empty;
                        ObjCarregado.RG_IE = !string.IsNullOrWhiteSpace(txtRg_ie.Text) ? txtRg_ie.Text.Replace(".", "").Replace("-", "").Replace("/", "") : string.Empty;
                        ObjCarregado.CEP = !string.IsNullOrWhiteSpace(txtCep.Text) ? txtCep.Text.Replace(".", "").Replace("-", "").Replace("/", "") : string.Empty;
                        ObjCarregado.ENDERECO = !string.IsNullOrWhiteSpace(txtEndereco.Text) ? txtEndereco.Text : string.Empty;
                        ObjCarregado.MUNICIPIO = !string.IsNullOrWhiteSpace(txtMunicipio.Text) ? txtMunicipio.Text : string.Empty;
                        ObjCarregado.ESTADO = !string.IsNullOrWhiteSpace(txtEstado.Text) ? txtEstado.Text : string.Empty;
                        ObjCarregado.ESTADO_SIGLA = !string.IsNullOrWhiteSpace(txtEstado_sigla.Text) ? txtEstado_sigla.Text : string.Empty;
                        ObjCarregado.PAIS = !string.IsNullOrWhiteSpace(txtPais.Text) ? txtPais.Text : string.Empty;
                        ObjCarregado.PAIS_EN = !string.IsNullOrWhiteSpace(txtPais_en.Text) ? txtPais_en.Text : string.Empty;
                        //ObjCarregado.PAIS_ES = !string.IsNullOrWhiteSpace(txtPais_es.Text) ? txtPais_es.Text : string.Empty;
                        ObjCarregado.EMAIL = !string.IsNullOrWhiteSpace(txtEmail.Text) ? txtEmail.Text : string.Empty;
                        ObjCarregado.DDI = !string.IsNullOrWhiteSpace(txtDdi.Text) ? txtDdi.Text : string.Empty;
                        ObjCarregado.TELEFONE = !string.IsNullOrWhiteSpace(txtTelefone.Text) ? txtTelefone.Text : string.Empty;
                        ObjCarregado.FAX = !string.IsNullOrWhiteSpace(txtFax.Text) ? txtFax.Text : string.Empty;
                        ObjCarregado.SITE = !string.IsNullOrWhiteSpace(txtSite.Text) ? txtSite.Text : string.Empty;
                        ObjCarregado.NUMERO = !string.IsNullOrWhiteSpace(txtNumero.Text) ? txtNumero.Text : string.Empty;
                        ObjCarregado.COMPLEMENTO = !string.IsNullOrWhiteSpace(txtComplemento.Text) ? txtComplemento.Text : string.Empty;
                        ObjCarregado.CXPOSTAL = !string.IsNullOrWhiteSpace(txtCxpostal.Text) ? txtCxpostal.Text : string.Empty;
                        ObjCarregado.LOGRADOURO = !string.IsNullOrWhiteSpace(txtLogradouro.Text) ? txtLogradouro.Text : string.Empty;
                        ObjCarregado.CELULAR = !string.IsNullOrWhiteSpace(txtCelular.Text) ? txtCelular.Text : string.Empty;
                        ObjCarregado.ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO;

                        _objBo.Update(ObjCarregado);
                        break;
                    default:
                        break;
                }

                _objBo.SaveChanges();

                _objBo = null; ObjCarregado = null;
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao salvar os dados. Mensagem: " + ex.Message);
            }
        }

        public void SetForm()
        {
            try
            {
                ddlPf_pj.SelectedValue = ObjCarregado.PF_PJ.Value.ToString();
                txtMatricula.Text = !string.IsNullOrWhiteSpace(ObjCarregado.MATRICULA) ? ObjCarregado.MATRICULA : string.Empty;
                txtNome_razao.Text = !string.IsNullOrWhiteSpace(ObjCarregado.NOME_RAZAO) ? ObjCarregado.NOME_RAZAO : string.Empty;
                txtApelido_fantasia.Text = !string.IsNullOrWhiteSpace(ObjCarregado.APELIDO_FANTASIA) ? ObjCarregado.APELIDO_FANTASIA : string.Empty;
                txtCpf_cnpj.Text = !string.IsNullOrWhiteSpace(ObjCarregado.CPF_CNPJ) ? ObjCarregado.CPF_CNPJ : string.Empty;
                txtRg_ie.Text = !string.IsNullOrWhiteSpace(ObjCarregado.RG_IE) ? ObjCarregado.RG_IE : string.Empty;
                txtCep.Text = !string.IsNullOrWhiteSpace(ObjCarregado.CEP) ? ObjCarregado.CEP : string.Empty;
                txtEndereco.Text = !string.IsNullOrWhiteSpace(ObjCarregado.ENDERECO) ? ObjCarregado.ENDERECO : string.Empty;
                txtMunicipio.Text = !string.IsNullOrWhiteSpace(ObjCarregado.MUNICIPIO) ? ObjCarregado.MUNICIPIO : string.Empty;
                txtEstado.Text = !string.IsNullOrWhiteSpace(ObjCarregado.ESTADO) ? ObjCarregado.ESTADO : string.Empty;
                txtEstado_sigla.Text = !string.IsNullOrWhiteSpace(ObjCarregado.ESTADO_SIGLA) ? ObjCarregado.ESTADO_SIGLA : string.Empty;
                txtPais.Text = !string.IsNullOrWhiteSpace(ObjCarregado.PAIS) ? ObjCarregado.PAIS : string.Empty;
                txtPais_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.PAIS_EN) ? ObjCarregado.PAIS_EN : string.Empty;
                //txtPais_es.Text = !string.IsNullOrWhiteSpace(ObjCarregado.PAIS_ES) ? ObjCarregado.PAIS_ES : string.Empty;
                txtEmail.Text = !string.IsNullOrWhiteSpace(ObjCarregado.EMAIL) ? ObjCarregado.EMAIL : string.Empty;
                txtDdi.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DDI) ? ObjCarregado.DDI : string.Empty;
                txtTelefone.Text = !string.IsNullOrWhiteSpace(ObjCarregado.TELEFONE) ? ObjCarregado.TELEFONE : string.Empty;
                txtFax.Text = !string.IsNullOrWhiteSpace(ObjCarregado.FAX) ? ObjCarregado.FAX : string.Empty;
                txtSite.Text = !string.IsNullOrWhiteSpace(ObjCarregado.SITE) ? ObjCarregado.SITE : string.Empty;
                txtNumero.Text = !string.IsNullOrWhiteSpace(ObjCarregado.NUMERO) ? ObjCarregado.NUMERO : string.Empty;
                txtComplemento.Text = !string.IsNullOrWhiteSpace(ObjCarregado.COMPLEMENTO) ? ObjCarregado.COMPLEMENTO : string.Empty;
                txtCxpostal.Text = !string.IsNullOrWhiteSpace(ObjCarregado.CXPOSTAL) ? ObjCarregado.CXPOSTAL : string.Empty;
                txtLogradouro.Text = !string.IsNullOrWhiteSpace(ObjCarregado.LOGRADOURO) ? ObjCarregado.LOGRADOURO : string.Empty;
                txtCelular.Text = !string.IsNullOrWhiteSpace(ObjCarregado.CELULAR) ? ObjCarregado.CELULAR : string.Empty;
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao carregar o formulário. Mensagem: " + ex.Message);
            }
        }

        public void EnableFields() { throw new NotImplementedException(); }

        public void DisableFields() { throw new NotImplementedException(); }

        public void ClearFields()
        {
            try
            {
                txtMatricula.Text = txtNome_razao.Text = txtApelido_fantasia.Text = txtCpf_cnpj.Text = txtRg_ie.Text = string.Empty;
                txtCep.Text = txtEndereco.Text = txtMunicipio.Text = txtEstado.Text = string.Empty;
                txtEstado_sigla.Text = txtPais.Text = txtPais_en.Text = /*txtPais_es.Text = */string.Empty;
                txtEmail.Text = txtDdi.Text = txtTelefone.Text = txtFax.Text = txtSite.Text = string.Empty;
                txtNumero.Text = txtComplemento.Text = txtCxpostal.Text = txtLogradouro.Text = txtCelular.Text = string.Empty;
                ddlPf_pj.SelectedValue = "2";
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao limpar os campos do formulário. Mensagem: " + ex.Message);
            }
        }

        public void ClearMemory() { ObjCarregado = null; }

        protected void btnFiltrar_Click(object sender, ImageClickEventArgs e)
        {
            try { gvView.DataBind(); }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void gvView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((Literal)e.Row.FindControl("ltrRegistroOrigem")).Text = ((IMPORTACAO_CLIENTE)e.Row.DataItem).REGISTRO_ORIGEM == 2 ? "Importação" : "Manual";
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string _SelectedKey = gvView.SelectedDataKey.Values["ID_CLIENTE"].ToString();

                ObjCarregado = new ImportacaoClienteBo().Find(lbda => lbda.ID_CLIENTE == _SelectedKey).First();

                ViewAspect = enmUserInterfaceAspect.Update;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int currentPageIndex;
                string _CommandKey;

                switch (e.CommandName)
                {
                    case "Excluir":
                        _CommandKey = e.CommandArgument.ToString();

                        ImportacaoClienteBo _objBo = new ImportacaoClienteBo();
                        _objBo.Delete(_objBo.Find(lbda => lbda.ID_CLIENTE == _CommandKey).First());
                        _objBo.SaveChanges();
                        _objBo = null;

                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex >= 0 && currentPageIndex <= gvView.PageCount - 1)
                            gvView.PageIndex = currentPageIndex;
                        break;
                    case "FirstPage":
                        gvView.DataBind();
                        gvView.PageIndex = 0;
                        break;
                    case "BackPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex > 0)
                            gvView.PageIndex = currentPageIndex - 1;
                        break;
                    case "GoToPage":
                        currentPageIndex = gvView.PageIndex;

                        int goToTop = 1, goToBottom = 1, goTo = 1;
                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text))
                            goToTop = int.Parse(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text);

                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text))
                            goToBottom = int.Parse(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text);

                        gvView.DataBind();

                        goTo = (goToTop != currentPageIndex + 1) ? goToTop : goToBottom;

                        if (goTo >= 1 && goTo <= gvView.PageCount)
                            gvView.PageIndex = goTo - 1;
                        break;
                    case "NextPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex < gvView.PageCount)
                            gvView.PageIndex = currentPageIndex + 1;
                        break;
                    case "LastPage":
                        gvView.DataBind();
                        gvView.PageIndex = gvView.PageCount - 1;
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void edsView_QueryCreated(object sender, QueryCreatedEventArgs e)
        {
            try
            {
                var result = e.Query.Cast<IMPORTACAO_CLIENTE>();

                int idOrigem = int.Parse(ddlFiltroOrigem.SelectedValue);

                if (idOrigem == 0)
                {
                    if (!string.IsNullOrWhiteSpace(txtFiltroDescricao.Text))
                    {
                        if (!string.IsNullOrWhiteSpace(txtFiltroMatricula.Text))
                        {
                            e.Query = from item in result
                                      where
                                        item.NOME_RAZAO.Contains(txtFiltroDescricao.Text) || item.APELIDO_FANTASIA.Contains(txtFiltroDescricao.Text)
                                        && item.MATRICULA.Contains(txtFiltroMatricula.Text)
                                      select item;
                        }
                        else
                        {
                            e.Query = from item in result
                                      where item.NOME_RAZAO.Contains(txtFiltroDescricao.Text) || item.APELIDO_FANTASIA.Contains(txtFiltroDescricao.Text)
                                      select item;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(txtFiltroMatricula.Text))
                        {
                            e.Query = from item in result
                                      where
                                        item.MATRICULA.Contains(txtFiltroMatricula.Text)
                                      select item;
                        }
                        else
                        {
                            e.Query = from item in result
                                      select item;
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(txtFiltroDescricao.Text))
                    {
                        if (!string.IsNullOrWhiteSpace(txtFiltroMatricula.Text))
                        {
                            e.Query = from item in result
                                      where
                                        item.NOME_RAZAO.Contains(txtFiltroDescricao.Text) || item.APELIDO_FANTASIA.Contains(txtFiltroDescricao.Text)
                                        && item.REGISTRO_ORIGEM == idOrigem
                                        && item.MATRICULA.Contains(txtFiltroMatricula.Text)
                                      select item;
                        }
                        else
                        {
                            e.Query = from item in result
                                      where
                                        item.NOME_RAZAO.Contains(txtFiltroDescricao.Text) || item.APELIDO_FANTASIA.Contains(txtFiltroDescricao.Text)
                                        && item.REGISTRO_ORIGEM == idOrigem
                                      select item;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(txtFiltroMatricula.Text))
                        {
                            e.Query = from item in result
                                      where
                                        item.REGISTRO_ORIGEM == idOrigem
                                        && item.MATRICULA.Contains(txtFiltroMatricula.Text)
                                      select item;
                        }
                        else
                        {
                            e.Query = from item in result
                                      where
                                        item.REGISTRO_ORIGEM == idOrigem
                                      select item;
                        }
                    }
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void btnLimparCamposFiltro_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                txtFiltroDescricao.Text = string.Empty;

                gvView.DataBind();
            }
            catch (Exception ex) { throw; }
        }
    }
}