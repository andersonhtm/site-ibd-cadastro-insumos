﻿<%@ Page Title=":: MasterPainel - HTM Gestão e Tecnologia ::" Language="C#" MasterPageFile="~/Admin/Admin.Master"
    AutoEventWireup="true" CodeBehind="ImportacaoCertificadoView.aspx.cs" Inherits="HTMSiteAdmin.Web.Admin.ImportacaoCertificadoView" %>

<%@ Register Src="UserControls/ucShowException.ascx" TagName="ucShowException" TagPrefix="uc1" %>
<%@ Register Src="UserControls/Bars/ucButtons.ascx" TagName="ucButtons" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upView" runat="server">
        <ContentTemplate>
            <h1>
                ..:: Certificados
            </h1>
            <asp:Panel ID="pnlView" runat="server">
                <fieldset class="Fieldset">
                    <legend>:: Dados Básicos</legend>
                    <div class="Fields">
                        <p>
                            <asp:Label ID="lblNome" runat="server" Text="Nome:" AssociatedControlID="txtNome"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtNome" MaxLength="255" CssClass="Field" Width="200px" />
                        </p>
                        <p>
                            <asp:Label ID="lblNome_en" runat="server" Text="Nome (Inglês):" AssociatedControlID="txtNome_en"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtNome_en" MaxLength="255" CssClass="Field" Width="200px" />
                        </p>
                        <%--<p>
                            <asp:Label ID="lblNome_es" runat="server" Text="Nome (Espanhol):" AssociatedControlID="txtNome_es"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtNome_es" MaxLength="255" CssClass="Field" Width="200px" />
                        </p>--%>
                        <p>
                            <asp:Label ID="lblSigla" runat="server" Text="Sigla:" AssociatedControlID="txtSigla"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtSigla" MaxLength="100" CssClass="Field" Width="75px" />
                        </p>
                        <p>
                            <asp:Label ID="lblSigla_en" runat="server" Text="Sigla (Inglês):" AssociatedControlID="txtSigla_en"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtSigla_en" MaxLength="100" CssClass="Field" Width="75px" />
                        </p>
                        <%--<p>
                            <asp:Label ID="lblSigla_es" runat="server" Text="Sigla (Espanhol):" AssociatedControlID="txtSigla_es"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtSigla_es" MaxLength="100" CssClass="Field" Width="75px" />
                        </p>--%>
                    </div>
                </fieldset>
            </asp:Panel>
            <uc2:ucButtons ID="ucButtons1" runat="server" />
            <uc1:ucShowException ID="ucShowException1" runat="server" />
            <asp:Panel ID="pnlViewCollection" runat="server">
                <div class="Fields">
                    <p>
                        <asp:Label ID="Label1" runat="server" Text="Origem:" AssociatedControlID="ddlFiltroOrigem"
                            CssClass="LabelFilters" />
                        <asp:DropDownList runat="server" ID="ddlFiltroOrigem" CssClass="Field">
                            <asp:ListItem Text="Todos" Selected="True" Value="0" />
                            <asp:ListItem Text="Manual" Value="1" />
                            <asp:ListItem Text="Importação" Value="2" />
                        </asp:DropDownList>
                    </p>
                    <p>
                        <asp:Label ID="Label2" runat="server" Text="Descrição:" AssociatedControlID="txtFiltroDescricao"
                            CssClass="LabelFilters" />
                        <asp:TextBox runat="server" ID="txtFiltroDescricao" MaxLength="255" Width="250px"
                            CssClass="Field" />
                        <asp:ImageButton ID="btnFiltrar" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/botao_pesquisar_1.png"
                            OnClick="btnFiltrar_Click" onmouseover="javascript:ChangeButtonsImage('pesquisar', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('pesquisar', 'out', this);" Style="margin-left: 50px;" />
                    </p>
                </div>
                <asp:GridView runat="server" ID="gvView" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataSourceID="edsView" Width="100%" CssClass="GridView" OnRowCommand="gvView_RowCommand"
                    DataKeyNames="ID_CERTIFICADO" CellPadding="4" GridLines="None" OnRowDataBound="gvView_RowDataBound"
                    OnSelectedIndexChanged="gvView_SelectedIndexChanged">
                    <AlternatingRowStyle CssClass="gvAlternatingRowStyle" />
                    <Columns>
                        <asp:BoundField DataField="ID_CERTIFICADO" HeaderText="Código" ReadOnly="True" SortExpression="ID_CERTIFICADO"
                            ItemStyle-CssClass="gvCenter" ItemStyle-Width="40px" />
                        <asp:BoundField DataField="NOME" HeaderText="Nome" SortExpression="NOME" ItemStyle-CssClass="gvCenter"
                            ReadOnly="True" />
                        <asp:BoundField DataField="NOME_EN" HeaderText="Nome (Inglês)" SortExpression="NOME_EN"
                            ItemStyle-CssClass="gvCenter" ReadOnly="True" />
                        <asp:TemplateField HeaderText="Origem" SortExpression="REGISTRO_ORIGEM">
                            <ItemTemplate>
                                <asp:Literal ID="ltrRegistroOrigem" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEditar" runat="server" CausesValidation="False" CommandName="Select"
                                    ImageUrl="~/Admin/Media/Images/GridViews/edit.png" Text="Select" ToolTip="Exibir/Editar" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnExcluir" runat="server" CausesValidation="False" CommandName="Excluir"
                                    OnClientClick="javascript:return confirm('Deseja excluir este registro?');" ImageUrl="~/Admin/Media/Images/GridViews/delete.png"
                                    CommandArgument='<%# Bind("ID_CERTIFICADO") %>' ToolTip="Excluir" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle CssClass="EditRowStyle" />
                    <HeaderStyle CssClass="gvHeaderStyle" />
                    <PagerSettings Position="TopAndBottom" />
                    <PagerStyle CssClass="gvPagerStyle" />
                    <PagerTemplate>
                        <asp:ImageButton ID="btnGvFirst" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/first.png"
                            CommandName="FirstPage" ToolTip="Primeira Página" />
                        <asp:ImageButton ID="btnGvBack" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/back.png"
                            CommandName="BackPage" ToolTip="Página Anterior" />
                        <asp:TextBox ID="txtGvPage" runat="server" Text="<%# gvView.PageIndex + 1 %>" MaxLength="4"
                            Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:Label ID="ltrPager" Text="de" runat="server" CssClass="gvPagerLiteral" EnableTheming="false" />
                        <asp:TextBox ID="txtGvPages" runat="server" Text="<%# gvView.PageCount %>" Enabled="false"
                            ReadOnly="true" Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:ImageButton ID="btnGvGo" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/botao_ir_1.png"
                            CommandName="GoToPage" ToolTip="Ir para a página" onmouseover="javascript:ChangeButtonsImage('gotopage', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('gotopage', 'out', this);" />
                        <asp:ImageButton ID="btnGvNext" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/next.png"
                            CommandName="NextPage" ToolTip="Próxima Página" />
                        <asp:ImageButton ID="btnGvLast" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/last.png"
                            CommandName="LastPage" ToolTip="Última Página" />
                    </PagerTemplate>
                    <RowStyle CssClass="gvRowStyle" />
                    <SelectedRowStyle CssClass="gvSelectedRowStyle" />
                    <SortedAscendingCellStyle CssClass="gvSortedAscendingCellStyle" />
                    <SortedAscendingHeaderStyle CssClass="gvSortedAscendingHeaderStyle" />
                    <SortedDescendingCellStyle CssClass="gvSortedDescendingCellStyle" />
                    <SortedDescendingHeaderStyle CssClass="gvSortedDescendingHeaderStyle" />
                </asp:GridView>
                <asp:EntityDataSource ID="edsView" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                    DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="IMPORTACAO_CERTIFICADO"
                    EntityTypeFilter="IMPORTACAO_CERTIFICADO" OnQueryCreated="edsView_QueryCreated" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
