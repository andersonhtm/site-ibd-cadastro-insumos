﻿using System;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class UploadFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ArquivoEnviado"] != null)
                Response.BinaryWrite((byte[])Session["ArquivoEnviado"]);
        }

        protected void afuArquivoEnviado_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            if (((AjaxControlToolkit.AsyncFileUpload)sender).HasFile)
                Session["ArquivoEnviado"] = ((AjaxControlToolkit.AsyncFileUpload)sender).FileBytes;
        }
    }
}