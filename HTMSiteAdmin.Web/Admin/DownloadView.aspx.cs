﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Data;
using System.Text;
using HTMSiteAdmin.Library.Exceptions;
using HTMSiteAdmin.Business.Downloads;
using HTMSiteAdmin.Business.Sistema;
using HTMSiteAdmin.Business.Pessoas;
using HTMSiteAdmin.Business.Sistema.ControleAcesso;
using System.Configuration;
using HTMSiteAdmin.Library;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class DownloadView : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this, ViewToken, "ViewAspect", value); }
        }
        private DOWNLOAD ObjCarregado
        {
            get { return (DOWNLOAD)Global.RestoreObject(this, ViewToken, "ObjCarregado"); }
            set { Global.SaveObject(this, ViewToken, "ObjCarregado", value); }
        }
        public UploadedFile ArquivoEnviado
        {
            get { return (UploadedFile)Global.RestoreObject(this, ViewToken, "ArquivoEnviado"); }
            set { Global.SaveObject(this, ViewToken, "ArquivoEnviado", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MapButtons();

            if (!IsPostBack && ObjCarregado == null)
                ViewAspect = enmUserInterfaceAspect.Start;

            if (!IsPostBack) UserInterfaceAspectChanged();
        }

        public void ValidateAcess() { throw new NotImplementedException(); }

        public void MapButtons()
        {
            ucButtons1.ButtonNew.CausesValidation = false;
            ucButtons1.ButtonNew.Click += new ImageClickEventHandler(ButtonNew_Click);

            ucButtons1.ButtonSave.ValidationGroup = "vgView";
            ucButtons1.ButtonSave.CausesValidation = true;
            ucButtons1.ButtonSave.Click += new ImageClickEventHandler(ButtonSave_Click);

            ucButtons1.ButtonSaveNew.ValidationGroup = "vgView";
            ucButtons1.ButtonSaveNew.CausesValidation = true;
            ucButtons1.ButtonSaveNew.Click += new ImageClickEventHandler(ButtonSaveNew_Click);

            ucButtons1.ButtonCancel.CausesValidation = false;
            ucButtons1.ButtonCancel.Click += new ImageClickEventHandler(ButtonCancel_Click);

            ucButtons1.ButtonRefresh.CausesValidation = false;
            ucButtons1.ButtonRefresh.Click += new ImageClickEventHandler(ButtonRefresh_Click);

            ucButtons1.ButtonDelete.CausesValidation = false;
            ucButtons1.ButtonDelete.Click += new ImageClickEventHandler(ButtonDelete_Click);
        }

        public void UserInterfaceAspectChanged()
        {
            StartPageControls();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    ucButtons1.ButtonNew.Visible = true;
                    ucButtons1.ButtonSave.Visible = false;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = false;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = false;

                    int currentPageIndex = gvView.PageIndex;
                    gvView.DataBind();
                    gvView.PageIndex = currentPageIndex;

                    pnlViewCollection.Visible = gvView.Rows.Count > 0;
                    break;
                case enmUserInterfaceAspect.New:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = true;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    RestartForm();
                    break;
                case enmUserInterfaceAspect.Update:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    ClearFields();

                    SetForm();
                    break;
                default:
                    break;
            }
        }

        public void RestartForm()
        {
            ClearMemory();
            ClearFields();

            pnlArquivoAtual.Visible = false;
        }

        public void StartPageControls()
        {
            try
            {
                if (!IsPostBack)
                {
                    ddlId_download_tipo.DataBind();
                    ddlId_download_categoria.DataBind();
                    ddlFiltroTipo.DataBind();
                    ddlFiltroCategoria.DataBind();
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSaveNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void SaveView()
        {
            StringBuilder sbValidacoes = new StringBuilder();

            if (ViewAspect == enmUserInterfaceAspect.New && ArquivoEnviado == null)
                sbValidacoes.AppendLine("Um arquivo precisa ser enviado.");

            if (sbValidacoes.Length > 0)
                throw new HTMSiteAdminException(sbValidacoes.ToString());

            DownloadBo _objBo = new DownloadBo();

            var idDownloadCategoria = Convert.ToInt32(ddlId_download_categoria.SelectedValue) != 0 ? Convert.ToInt32(ddlId_download_categoria.SelectedValue) : (int?)null;

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    break;
                case enmUserInterfaceAspect.New:
                    ObjCarregado = new DOWNLOAD()
                    {
                        ID_DOWNLOAD_TIPO = int.Parse(ddlId_download_tipo.SelectedValue),
                        ID_DOWNLOAD_CATEGORIA = idDownloadCategoria,
                        DESCRICAO = !string.IsNullOrWhiteSpace(txtDescricao.Text) ? txtDescricao.Text : string.Empty,
                        DESCRICAO_EN = !string.IsNullOrWhiteSpace(txtDescricao_en.Text) ? txtDescricao_en.Text : string.Empty,
                        DESCRICAO_DETALHADA = !string.IsNullOrWhiteSpace(txtSubDescricao.Text) ? txtSubDescricao.Text : string.Empty,
                        DESCRICAO_DETALHADA_EN = !string.IsNullOrWhiteSpace(txtSubDescricao_en.Text) ? txtSubDescricao_en.Text : string.Empty,
                        ORDEM = !string.IsNullOrWhiteSpace(txtOrdem.Text) ? int.Parse(txtOrdem.Text) : 9999,
                        ATIVO = chkAtivo.Checked,
                        ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO
                    };

                    FillArquivoDigital();

                    _objBo.Add(ObjCarregado);
                    break;
                case enmUserInterfaceAspect.Update:
                    ObjCarregado = _objBo.Find(lbda => lbda.ID_DOWNLOAD == ObjCarregado.ID_DOWNLOAD).First();

                    ObjCarregado.ID_DOWNLOAD_TIPO = int.Parse(ddlId_download_tipo.SelectedValue);
                    //ObjCarregado.ID_DOWNLOAD_CATEGORIA = int.Parse(ddlId_download_categoria.SelectedValue);
                    ObjCarregado.ID_DOWNLOAD_CATEGORIA = idDownloadCategoria;
                    ObjCarregado.DESCRICAO = !string.IsNullOrWhiteSpace(txtDescricao.Text) ? txtDescricao.Text : string.Empty;
                    ObjCarregado.DESCRICAO_EN = !string.IsNullOrWhiteSpace(txtDescricao_en.Text) ? txtDescricao_en.Text : string.Empty;
                    ObjCarregado.DESCRICAO_DETALHADA = !string.IsNullOrWhiteSpace(txtSubDescricao.Text) ? txtSubDescricao.Text : string.Empty;
                    ObjCarregado.DESCRICAO_DETALHADA_EN = !string.IsNullOrWhiteSpace(txtSubDescricao_en.Text) ? txtSubDescricao_en.Text : string.Empty;
                    ObjCarregado.ATIVO = chkAtivo.Checked;
                    ObjCarregado.ORDEM = !string.IsNullOrWhiteSpace(txtOrdem.Text) ? int.Parse(txtOrdem.Text) : 9999;
                    ObjCarregado.ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO;

                    FillArquivoDigital();

                    _objBo.Update(ObjCarregado);
                    break;
                default:
                    break;
            }

            _objBo.SaveChanges();

            _objBo = null; ObjCarregado = null;
        }

        private void FillArquivoDigital()
        {
            string path = Server.MapPath(ConfigurationManager.AppSettings["RELATIVEPATH_ARQUIVODIGITAL"]);

            try
            {
                if (ArquivoEnviado != null)
                {
                    if (ViewAspect == enmUserInterfaceAspect.Update)
                    {
                        //ObjCarregado.ARQUIVO_DIGITAL.ARQUIVO = ArquivoEnviado.FileBytes;
                        ObjCarregado.ARQUIVO_DIGITAL.NOME = ArquivoEnviado.FileName;
                        ObjCarregado.ARQUIVO_DIGITAL.EXTENSAO = ArquivoEnviado.FileExtension;
                        ObjCarregado.ARQUIVO_DIGITAL.DATA = DateTime.Now;
                    }
                    else
                    {
                        ObjCarregado.ARQUIVO_DIGITAL = new ARQUIVO_DIGITAL()
                        {
                            ID_ARQUIVO = Guid.NewGuid(),
                            NOME = ArquivoEnviado.FileName,
                            EXTENSAO = ArquivoEnviado.FileExtension,
                            //ARQUIVO = ArquivoEnviado.FileBytes,
                            DATA = DateTime.Now
                        };
                    }

                    Util.SaveFile(path, ObjCarregado.ARQUIVO_DIGITAL.ID_ARQUIVO.ToString(), ArquivoEnviado.FileExtension, ArquivoEnviado.FileBytes);
                }
            }
            catch (Exception ex) { throw; }
        }

        public void ButtonRefresh_Click(object sender, ImageClickEventArgs e)
        {
            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    //Nada fazer
                    break;
                case enmUserInterfaceAspect.New:
                    ClearFields();
                    break;
                case enmUserInterfaceAspect.Update:
                    ClearFields();
                    //TODO: Carregar dados do usuário
                    break;
                default:
                    break;
            }
        }

        public void ButtonCancel_Click(object sender, ImageClickEventArgs e)
        {
            ClearMemory();
            ViewAspect = enmUserInterfaceAspect.Start;
            UserInterfaceAspectChanged();
        }

        public void ButtonDelete_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void GetForm() { throw new NotImplementedException(); }

        public void SetForm()
        {
            try
            {

                ddlId_download_tipo.DataBind();
                if (ObjCarregado.ID_DOWNLOAD_TIPO > 0)
                    ddlId_download_tipo.SelectedValue = ObjCarregado.ID_DOWNLOAD_TIPO.ToString();

                ddlId_download_categoria.DataBind();
                if (ObjCarregado.ID_DOWNLOAD_CATEGORIA > 0)
                    ddlId_download_categoria.SelectedValue = ObjCarregado.ID_DOWNLOAD_CATEGORIA.ToString();

                txtDescricao.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO) ? ObjCarregado.DESCRICAO : string.Empty;
                txtDescricao_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO_EN) ? ObjCarregado.DESCRICAO_EN : string.Empty;
                txtSubDescricao.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO_DETALHADA) ? ObjCarregado.DESCRICAO_DETALHADA : string.Empty;
                txtSubDescricao_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO_DETALHADA_EN) ? ObjCarregado.DESCRICAO_DETALHADA_EN : string.Empty;
                txtOrdem.Text = ObjCarregado.ORDEM.HasValue ? ObjCarregado.ORDEM.ToString() : string.Empty;

                chkAtivo.Checked = ObjCarregado.ATIVO;

                CarregarArquivoAtual();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        private void CarregarArquivoAtual()
        {
            pnlArquivoAtual.Visible = true;

            switch (HTMSiteAdmin.Library.FileTools.GetFileType(ObjCarregado.ARQUIVO_DIGITAL.EXTENSAO))
            {
                case HTMSiteAdmin.Library.FileTools.enmFileTypes.Image:
                    imgMiniaturaAtual.Visible = true;
                    btnDownloadAtual.Visible = false;

                    imgMiniaturaAtual.ImageUrl = string.Format("~/ShowFile.aspx?action=1&fileid={0}&ViewMethod={1}&Dimension={2}&Size={3}",
                        ObjCarregado.ARQUIVO_DIGITAL.ID_ARQUIVO.ToString(),
                        (int)HTMSiteAdmin.Library.ImageTools.UseMethod.ConstrainProportions,
                        (int)HTMSiteAdmin.Library.ImageTools.Dimensions.Height,
                        300);
                    break;
                case HTMSiteAdmin.Library.FileTools.enmFileTypes.DocumentWord:
                    imgMiniaturaAtual.Visible = false;

                    btnDownloadAtual.Visible = true;
                    btnDownloadAtual.ImageUrl = "~/Admin/Media/Images/arquivo_word.png";
                    btnDownloadAtual.PostBackUrl = string.Format("~/ShowFile.aspx?action=2&fileid={0}", ObjCarregado.ARQUIVO_DIGITAL.ID_ARQUIVO.ToString());
                    btnDownloadAtual.ToolTip = ObjCarregado.ARQUIVO_DIGITAL.NOME;
                    break;
                case HTMSiteAdmin.Library.FileTools.enmFileTypes.DocumentPdf:
                    imgMiniaturaAtual.Visible = false;

                    btnDownloadAtual.Visible = true;
                    btnDownloadAtual.ImageUrl = "~/Admin/Media/Images/arquivo_pdf.png";
                    btnDownloadAtual.PostBackUrl = string.Format("~/ShowFile.aspx?action=2&fileid={0}", ObjCarregado.ARQUIVO_DIGITAL.ID_ARQUIVO.ToString());
                    btnDownloadAtual.ToolTip = ObjCarregado.ARQUIVO_DIGITAL.NOME;
                    break;
                case HTMSiteAdmin.Library.FileTools.enmFileTypes.DocumentExcel:
                    imgMiniaturaAtual.Visible = false;

                    btnDownloadAtual.Visible = true;
                    btnDownloadAtual.ImageUrl = "~/Admin/Media/Images/arquivo_excel.png";
                    btnDownloadAtual.PostBackUrl = string.Format("~/ShowFile.aspx?action=2&fileid={0}", ObjCarregado.ARQUIVO_DIGITAL.ID_ARQUIVO.ToString());
                    btnDownloadAtual.ToolTip = ObjCarregado.ARQUIVO_DIGITAL.NOME;
                    break;
                case HTMSiteAdmin.Library.FileTools.enmFileTypes.Generic:
                    imgMiniaturaAtual.Visible = false;

                    btnDownloadAtual.Visible = true;
                    btnDownloadAtual.ImageUrl = "~/Admin/Media/Images/arquivo_geral.png";
                    btnDownloadAtual.PostBackUrl = string.Format("~/ShowFile.aspx?action=2&fileid={0}", ObjCarregado.ARQUIVO_DIGITAL.ID_ARQUIVO.ToString());
                    btnDownloadAtual.ToolTip = ObjCarregado.ARQUIVO_DIGITAL.NOME;
                    break;
            }
        }

        public void EnableFields() { throw new NotImplementedException(); }

        public void DisableFields() { throw new NotImplementedException(); }

        public void ClearFields()
        {
            try
            {
                txtDescricao.Text = txtDescricao_en.Text = txtOrdem.Text = txtSubDescricao.Text = txtSubDescricao_en.Text = string.Empty;
                ddlId_download_tipo.DataBind();
                ddlId_download_categoria.DataBind();
                chkAtivo.Checked = true;
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ClearMemory() { ObjCarregado = null; }

        protected void btnFiltrar_Click(object sender, ImageClickEventArgs e)
        {
            try { gvView.DataBind(); }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void gvView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    SESSAO _objSessao = new SessaoBo().Find(s => s.ID_SESSAO == ((DOWNLOAD)e.Row.DataItem).ID_SESSAO).First();
                    ((Literal)e.Row.FindControl("ltrData")).Text = _objSessao.DATA_INICIO.ToShortDateString();

                    PESSOA _objPessoa = new PessoaBo().Find(p => p.ID_PESSOA == _objSessao.ID_USUARIO_LOGADO).First();
                    switch ((enmTipoPessoa)_objPessoa.ID_PESSOA_TIPO)
                    {
                        case enmTipoPessoa.PessoaFisica:
                            ((Literal)e.Row.FindControl("ltrUsuario")).Text = _objPessoa.PF_NOME_COMPLETO;
                            break;
                        case enmTipoPessoa.PessoaJuridica:
                            ((Literal)e.Row.FindControl("ltrUsuario")).Text = _objPessoa.PJ_NOME_FANTASIA;
                            break;
                    }

                    ARQUIVO_DIGITAL _objArquivoDigital = new ArquivoDigitalBo().Find(l => l.ID_ARQUIVO == ((DOWNLOAD)e.Row.DataItem).ID_ARQUIVO_DIGITAL).First();

                    switch (HTMSiteAdmin.Library.FileTools.GetFileType(_objArquivoDigital.EXTENSAO))
                    {
                        case HTMSiteAdmin.Library.FileTools.enmFileTypes.Image:
                            ((Image)e.Row.FindControl("imgMiniatura")).Visible = true;
                            ((ImageButton)e.Row.FindControl("btnDownload")).Visible = false;

                            ((Image)e.Row.FindControl("imgMiniatura")).ImageUrl = string.Format("~/ShowFile.aspx?action=1&fileid={0}&width={1}&height={2}", _objArquivoDigital.ID_ARQUIVO.ToString(), 40, 40);

                            ((Literal)e.Row.FindControl("ltrLink")).Text = HTMSiteAdmin.Library.Util.GetServerPath(this) + string.Format("/ShowFile.aspx?action=1&fileid={0}&width=LARGURA&height=ALTURA", _objArquivoDigital.ID_ARQUIVO.ToString().ToString());
                            break;
                        case HTMSiteAdmin.Library.FileTools.enmFileTypes.DocumentWord:
                            ((Image)e.Row.FindControl("imgMiniatura")).Visible = false;

                            ((ImageButton)e.Row.FindControl("btnDownload")).Visible = true;
                            ((ImageButton)e.Row.FindControl("btnDownload")).ImageUrl = "~/Admin/Media/Images/arquivo_word.png";
                            ((ImageButton)e.Row.FindControl("btnDownload")).PostBackUrl = string.Format("~/ShowFile.aspx?action=2&fileid={0}", _objArquivoDigital.ID_ARQUIVO.ToString());
                            ((ImageButton)e.Row.FindControl("btnDownload")).ToolTip = _objArquivoDigital.NOME;

                            ((Literal)e.Row.FindControl("ltrLink")).Text = HTMSiteAdmin.Library.Util.GetServerPath(this) + string.Format("/ShowFile.aspx?action=2&fileid={0}", _objArquivoDigital.ID_ARQUIVO.ToString());
                            break;
                        case HTMSiteAdmin.Library.FileTools.enmFileTypes.DocumentPdf:
                            ((Image)e.Row.FindControl("imgMiniatura")).Visible = false;

                            ((ImageButton)e.Row.FindControl("btnDownload")).Visible = true;
                            ((ImageButton)e.Row.FindControl("btnDownload")).ImageUrl = "~/Admin/Media/Images/arquivo_pdf.png";
                            ((ImageButton)e.Row.FindControl("btnDownload")).PostBackUrl = string.Format("~/ShowFile.aspx?action=2&fileid={0}", _objArquivoDigital.ID_ARQUIVO.ToString());
                            ((ImageButton)e.Row.FindControl("btnDownload")).ToolTip = _objArquivoDigital.NOME;

                            ((Literal)e.Row.FindControl("ltrLink")).Text = HTMSiteAdmin.Library.Util.GetServerPath(this) + string.Format("/ShowFile.aspx?action=2&fileid={0}", _objArquivoDigital.ID_ARQUIVO.ToString());
                            break;
                        case HTMSiteAdmin.Library.FileTools.enmFileTypes.DocumentExcel:
                            ((Image)e.Row.FindControl("imgMiniatura")).Visible = false;

                            ((ImageButton)e.Row.FindControl("btnDownload")).Visible = true;
                            ((ImageButton)e.Row.FindControl("btnDownload")).ImageUrl = "~/Admin/Media/Images/arquivo_excel.png";
                            ((ImageButton)e.Row.FindControl("btnDownload")).PostBackUrl = string.Format("~/ShowFile.aspx?action=2&fileid={0}", _objArquivoDigital.ID_ARQUIVO.ToString());
                            ((ImageButton)e.Row.FindControl("btnDownload")).ToolTip = _objArquivoDigital.NOME;

                            ((Literal)e.Row.FindControl("ltrLink")).Text = HTMSiteAdmin.Library.Util.GetServerPath(this) + string.Format("/ShowFile.aspx?action=2&fileid={0}", _objArquivoDigital.ID_ARQUIVO.ToString());
                            break;
                        case HTMSiteAdmin.Library.FileTools.enmFileTypes.Generic:
                            ((Image)e.Row.FindControl("imgMiniatura")).Visible = false;

                            ((ImageButton)e.Row.FindControl("btnDownload")).Visible = true;
                            ((ImageButton)e.Row.FindControl("btnDownload")).ImageUrl = "~/Admin/Media/Images/arquivo_geral.png";
                            ((ImageButton)e.Row.FindControl("btnDownload")).PostBackUrl = string.Format("~/ShowFile.aspx?action=2&fileid={0}", _objArquivoDigital.ID_ARQUIVO.ToString());
                            ((ImageButton)e.Row.FindControl("btnDownload")).ToolTip = _objArquivoDigital.NOME;

                            ((Literal)e.Row.FindControl("ltrLink")).Text = HTMSiteAdmin.Library.Util.GetServerPath(this) + string.Format("/ShowFile.aspx?action=2&fileid={0}", _objArquivoDigital.ID_ARQUIVO.ToString());
                            break;
                    }
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _SelectedKey = Convert.ToInt32(gvView.SelectedDataKey.Values["ID_DOWNLOAD"]);

                ObjCarregado = new DownloadBo().Find(lbda => lbda.ID_DOWNLOAD == _SelectedKey).First();

                ViewAspect = enmUserInterfaceAspect.Update;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int currentPageIndex, _SelectedKey;

                switch (e.CommandName)
                {
                    case "Excluir":
                        _SelectedKey = Convert.ToInt32(e.CommandArgument);

                        DownloadBo _objBo = new DownloadBo();
                        _objBo.Delete(_objBo.Find(lbda => lbda.ID_DOWNLOAD == _SelectedKey).First());
                        _objBo.SaveChanges();
                        _objBo = null;

                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex >= 0 && currentPageIndex <= gvView.PageCount - 1)
                            gvView.PageIndex = currentPageIndex;
                        break;
                    case "FirstPage":
                        gvView.DataBind();
                        gvView.PageIndex = 0;
                        break;
                    case "BackPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex > 0)
                            gvView.PageIndex = currentPageIndex - 1;
                        break;
                    case "GoToPage":
                        currentPageIndex = gvView.PageIndex;

                        int goToTop = 1, goToBottom = 1, goTo = 1;
                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text))
                            goToTop = int.Parse(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text);

                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text))
                            goToBottom = int.Parse(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text);

                        gvView.DataBind();

                        goTo = (goToTop != currentPageIndex + 1) ? goToTop : goToBottom;

                        if (goTo >= 1 && goTo <= gvView.PageCount)
                            gvView.PageIndex = goTo - 1;
                        break;
                    case "NextPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex < gvView.PageCount)
                            gvView.PageIndex = currentPageIndex + 1;
                        break;
                    case "LastPage":
                        gvView.DataBind();
                        gvView.PageIndex = gvView.PageCount - 1;
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void edsView_QueryCreated(object sender, QueryCreatedEventArgs e)
        {
            try
            {
                var result = e.Query.Cast<DOWNLOAD>();

                int idCategoria = int.Parse(ddlFiltroCategoria.SelectedValue);
                int idTipo = int.Parse(ddlFiltroTipo.SelectedValue);

                if (!string.IsNullOrWhiteSpace(txtFiltroDescricao.Text))
                {
                    if (idCategoria > 0)
                    {
                        if (idTipo > 0)
                        {
                            e.Query = from item in result
                                      where
                                        (item.DESCRICAO.Contains(txtFiltroDescricao.Text) || item.DESCRICAO_EN.Contains(txtFiltroDescricao.Text))
                                        && item.ID_DOWNLOAD_CATEGORIA == idCategoria
                                        && item.ID_DOWNLOAD_TIPO == idTipo
                                      select item;
                        }
                        else
                        {
                            e.Query = from item in result
                                      where
                                        (item.DESCRICAO.Contains(txtFiltroDescricao.Text) || item.DESCRICAO_EN.Contains(txtFiltroDescricao.Text))
                                        && item.ID_DOWNLOAD_CATEGORIA == idCategoria
                                      select item;
                        }
                    }
                    else
                    {
                        if (idTipo > 0)
                        {
                            e.Query = from item in result
                                      where
                                        (item.DESCRICAO.Contains(txtFiltroDescricao.Text) || item.DESCRICAO_EN.Contains(txtFiltroDescricao.Text))
                                        && item.ID_DOWNLOAD_TIPO == idTipo
                                      select item;
                        }
                        else
                        {
                            e.Query = from item in result
                                      where
                                        item.DESCRICAO.Contains(txtFiltroDescricao.Text) ||
                                        item.DESCRICAO_EN.Contains(txtFiltroDescricao.Text)
                                      select item;
                        }
                    }
                }
                else
                {
                    if (idCategoria > 0)
                    {
                        if (idTipo > 0)
                        {
                            e.Query = from item in result
                                      where
                                        item.ID_DOWNLOAD_CATEGORIA == idCategoria
                                        && item.ID_DOWNLOAD_TIPO == idTipo
                                      select item;
                        }
                        else
                        {
                            e.Query = from item in result
                                      where
                                        item.ID_DOWNLOAD_CATEGORIA == idCategoria
                                      select item;
                        }
                    }
                    else
                    {
                        if (idTipo > 0)
                        {
                            e.Query = from item in result
                                      where
                                        item.ID_DOWNLOAD_TIPO == idTipo
                                      select item;
                        }
                    }
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void afuArquivoEvento_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e) { ArquivoEnviado = HTMSiteAdmin.Library.UserInterface.WebControlsUtility.AsyncFileUpload_UploadedComplete(sender, e); }

        protected void ddlId_download_categoria_DataBound(object sender, EventArgs e) { HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender, "pt"); }

        protected void ddlFiltroCategoria_DataBound(object sender, EventArgs e) { HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender, "pt"); }

        protected void ddlId_download_tipo_DataBound(object sender, EventArgs e) { HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender, "pt"); }

        protected void ddlFiltroTipo_DataBound(object sender, EventArgs e) { HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender, "pt"); }
    }
}