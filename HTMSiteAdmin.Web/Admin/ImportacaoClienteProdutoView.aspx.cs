﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Business.Importacao;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Library.Exceptions;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class ImportacaoClienteProdutoView : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this, ViewToken, "ViewAspect", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
        }

        public void UserInterfaceAspectChanged()
        {
            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    ddlCliente.DataBind();
                    CarregarDadosCliente();

                    pnlProdutosVinculados.Visible = false;
                    gvView.DataSource = null;
                    gvView.DataBind();

                    pnlProduto.Visible = false;
                    ddlProduto.DataSource = null;
                    ddlProduto.DataBind();
                    break;
                case enmUserInterfaceAspect.New:
                    break;
                case enmUserInterfaceAspect.Update:
                    break;
                default:
                    break;
            }
        }

        protected void gvView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((Literal)e.Row.FindControl("ltrProduto")).Text = new ImportacaoProdutoBo().Find(lbda => lbda.ID_PRODUTO == ((IMPORTACAO_CLIENTE_PRODUTO)e.Row.DataItem).ID_PRODUTO).First().NOME;
                    ((Literal)e.Row.FindControl("ltrRegistroOrigem")).Text = ((IMPORTACAO_CLIENTE_PRODUTO)e.Row.DataItem).REGISTRO_ORIGEM == 2 ? "Importação" : "Manual";
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int currentPageIndex, _CommandKey;

                switch (e.CommandName)
                {
                    case "Excluir":
                        _CommandKey = int.Parse(e.CommandArgument.ToString());

                        ImportacaoClienteProdutoBo _objBo = new ImportacaoClienteProdutoBo();
                        _objBo.Delete(_objBo.Find(lbda => lbda.ID_CLIENTE_PRODUTO == _CommandKey).First());
                        _objBo.SaveChanges();
                        _objBo = null;

                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex >= 0 && currentPageIndex <= gvView.PageCount - 1)
                            gvView.PageIndex = currentPageIndex;
                        break;
                    case "FirstPage":
                        gvView.DataBind();
                        gvView.PageIndex = 0;
                        break;
                    case "BackPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex > 0)
                            gvView.PageIndex = currentPageIndex - 1;
                        break;
                    case "GoToPage":
                        currentPageIndex = gvView.PageIndex;

                        int goToTop = 1, goToBottom = 1, goTo = 1;
                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text))
                            goToTop = int.Parse(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text);

                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text))
                            goToBottom = int.Parse(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text);

                        gvView.DataBind();

                        goTo = (goToTop != currentPageIndex + 1) ? goToTop : goToBottom;

                        if (goTo >= 1 && goTo <= gvView.PageCount)
                            gvView.PageIndex = goTo - 1;
                        break;
                    case "NextPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex < gvView.PageCount)
                            gvView.PageIndex = currentPageIndex + 1;
                        break;
                    case "LastPage":
                        gvView.DataBind();
                        gvView.PageIndex = gvView.PageCount - 1;
                        break;
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException.Message.Contains("FK_IMPORTACAO_CLIENTE_PRODUTO_CERTIFICADO_IMPORTACAO_CLIENTE_PRODUTO"))
                    ucShowException1.ShowExceptions(new HTMSiteAdminException("Não é possível remover este registro, pois o mesmo está vinculado a um ou mais certificados."));
                else ucShowException1.ShowExceptions(new HTMSiteAdminErrorException("Erro desconhecido"));
            }
        }

        protected void ddlCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ddlCliente.SelectedValue.Equals("0")) { CarregarDadosCliente(); }
                else
                {
                    ViewAspect = enmUserInterfaceAspect.Start;
                    UserInterfaceAspectChanged();
                }
            }
            catch (Exception) { throw; }
        }

        private void CarregarDadosCliente()
        {
            pnlProdutosVinculados.Visible = true;
            gvView.DataBind();

            pnlProduto.Visible = true;
            ddlProduto.DataSource = new HTMSiteAdminEntities().GetProdutosNaoVinculados(ddlCliente.SelectedValue);
            ddlProduto.DataBind();

        }

        protected void edsView_QueryCreated(object sender, QueryCreatedEventArgs e)
        {
            try
            {
                var result = e.Query.Cast<IMPORTACAO_CLIENTE_PRODUTO>();

                string idCliente = ddlCliente.SelectedValue;

                if (!string.IsNullOrWhiteSpace(idCliente) && !idCliente.Equals("0"))
                {
                    e.Query = from item in result
                              where
                                item.ID_CLIENTE == idCliente
                              select item;
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void ddlCliente_DataBound(object sender, EventArgs e) { HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender, "pt"); }

        protected void ddlProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ddlProduto.SelectedValue.Equals("0"))
                {
                    ImportacaoClienteProdutoBo _objBo = new ImportacaoClienteProdutoBo();

                    IMPORTACAO_CLIENTE_PRODUTO _objNovoVinculo = new IMPORTACAO_CLIENTE_PRODUTO()
                    {
                        ID_CLIENTE = ddlCliente.SelectedValue,
                        ID_PRODUTO = int.Parse(ddlProduto.SelectedValue),
                        REGISTRO_ORIGEM = 1,
                        ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO
                    };

                    _objBo.Add(_objNovoVinculo);
                    _objBo.SaveChanges();

                    _objBo = null; _objNovoVinculo = null;
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void ddlProduto_DataBound(object sender, EventArgs e) { HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender, "pt"); }
    }
}