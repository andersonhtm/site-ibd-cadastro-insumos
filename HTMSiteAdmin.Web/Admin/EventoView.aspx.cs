﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Eventos;
using System.Text;
using HTMSiteAdmin.Library.Exceptions;
using HTMSiteAdmin.Business.Sistema;
using System.Configuration;
using HTMSiteAdmin.Library;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class EventoView : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this, ViewToken, "ViewAspect", value); }
        }
        private EVENTO ObjCarregado
        {
            get { return (EVENTO)Global.RestoreObject(this, ViewToken, "ObjCarregado"); }
            set { Global.SaveObject(this, ViewToken, "ObjCarregado", value); }
        }
        public UploadedFile ArquivoEnviado
        {
            get { return (UploadedFile)Global.RestoreObject(this, ViewToken, "ArquivoEnviado"); }
            set { Global.SaveObject(this, ViewToken, "ArquivoEnviado", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MapButtons();

            if (!IsPostBack && ObjCarregado == null)
                ViewAspect = enmUserInterfaceAspect.Start;

            if (!IsPostBack) UserInterfaceAspectChanged();
        }

        public void ValidateAcess() { throw new NotImplementedException(); }

        public void MapButtons()
        {
            ucButtons1.ButtonNew.CausesValidation = false;
            ucButtons1.ButtonNew.Click += new ImageClickEventHandler(ButtonNew_Click);

            ucButtons1.ButtonSave.ValidationGroup = "vgView";
            ucButtons1.ButtonSave.CausesValidation = true;
            ucButtons1.ButtonSave.Click += new ImageClickEventHandler(ButtonSave_Click);

            ucButtons1.ButtonSaveNew.ValidationGroup = "vgView";
            ucButtons1.ButtonSaveNew.CausesValidation = true;
            ucButtons1.ButtonSaveNew.Click += new ImageClickEventHandler(ButtonSaveNew_Click);

            ucButtons1.ButtonCancel.CausesValidation = false;
            ucButtons1.ButtonCancel.Click += new ImageClickEventHandler(ButtonCancel_Click);

            ucButtons1.ButtonRefresh.CausesValidation = false;
            ucButtons1.ButtonRefresh.Click += new ImageClickEventHandler(ButtonRefresh_Click);

            ucButtons1.ButtonDelete.CausesValidation = false;
            ucButtons1.ButtonDelete.Click += new ImageClickEventHandler(ButtonDelete_Click);
        }

        public void UserInterfaceAspectChanged()
        {
            StartPageControls();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    ucButtons1.ButtonNew.Visible = true;
                    ucButtons1.ButtonSave.Visible = false;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = false;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = false;

                    int currentPageIndex = gvView.PageIndex;
                    gvView.DataBind();
                    gvView.PageIndex = currentPageIndex;

                    pnlViewCollection.Visible = gvView.Rows.Count > 0;
                    break;
                case enmUserInterfaceAspect.New:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = true;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    RestartForm();
                    break;
                case enmUserInterfaceAspect.Update:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    ClearFields();

                    SetForm();
                    break;
                default:
                    break;
            }
        }

        public void RestartForm()
        {
            ClearMemory();
            ClearFields();
        }

        public void StartPageControls()
        {
            try
            {
                if (!IsPostBack) { ddlId_evento_categoria.DataBind(); }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSaveNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void SaveView()
        {
            StringBuilder sbValidacoes = new StringBuilder();

            //if (!string.IsNullOrWhiteSpace(txtData_inicio.Text)
            //    && !string.IsNullOrWhiteSpace(txtData_fim.Text)
            //    && DateTime.Compare(Convert.ToDateTime(txtData_inicio.Text).Date, Convert.ToDateTime(txtData_fim.Text).Date) >= 0)
            //    sbValidacoes.AppendLine("A data de fim deve ser maior que a data de início.");

            if (sbValidacoes.Length > 0)
                throw new HTMSiteAdminException(sbValidacoes.ToString());

            EventoBo _objBo = new EventoBo();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    break;
                case enmUserInterfaceAspect.New:
                    ObjCarregado = new EVENTO()
                    {
                        ID_EVENTO_CATEGORIA = int.Parse(ddlId_evento_categoria.SelectedValue),
                        DESCRICAO = !string.IsNullOrWhiteSpace(txtDescricao.Text) ? txtDescricao.Text : string.Empty,
                        DESCRICAO_EN = !string.IsNullOrWhiteSpace(txtDescricao_en.Text) ? txtDescricao_en.Text : string.Empty,
                        SUB_DESCRICAO = !string.IsNullOrWhiteSpace(txtSubDescricao.Text) ? txtSubDescricao.Text : string.Empty,
                        SUB_DESCRICAO_EN = !string.IsNullOrWhiteSpace(txtSubDescricao_en.Text) ? txtSubDescricao_en.Text : string.Empty,
                        ATIVO = chkAtivo.Checked,
                        //ID_ARQUIVO_DIGITAL
                        DESCRICAO_DETALHADA = !string.IsNullOrWhiteSpace(txtDescricaoDetalhada.Text) ? txtDescricaoDetalhada.Text : string.Empty,
                        DESCRICAO_DETALHADA_EN = !string.IsNullOrWhiteSpace(txtDescricaoDetalhada_en.Text) ? txtDescricaoDetalhada_en.Text : string.Empty,
                        OBSERVACAO = !string.IsNullOrWhiteSpace(txtObservacao.Text) ? txtObservacao.Text : string.Empty,
                        ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO
                    };

                    FillNullableObjectValues();

                    FillArquivoDigital();

                    _objBo.Add(ObjCarregado);
                    break;
                case enmUserInterfaceAspect.Update:
                    ObjCarregado = _objBo.Find(lbda => lbda.ID_EVENTO == ObjCarregado.ID_EVENTO).First();

                    ObjCarregado.ID_EVENTO_CATEGORIA = int.Parse(ddlId_evento_categoria.SelectedValue);
                    ObjCarregado.DESCRICAO = !string.IsNullOrWhiteSpace(txtDescricao.Text) ? txtDescricao.Text : string.Empty;
                    ObjCarregado.DESCRICAO_EN = !string.IsNullOrWhiteSpace(txtDescricao_en.Text) ? txtDescricao_en.Text : string.Empty;
                    ObjCarregado.SUB_DESCRICAO = !string.IsNullOrWhiteSpace(txtSubDescricao.Text) ? txtSubDescricao.Text : string.Empty;
                    ObjCarregado.SUB_DESCRICAO_EN = !string.IsNullOrWhiteSpace(txtSubDescricao_en.Text) ? txtSubDescricao_en.Text : string.Empty;
                    ObjCarregado.ATIVO = chkAtivo.Checked;
                    //ID_ARQUIVO_DIGITAL
                    ObjCarregado.DESCRICAO_DETALHADA = !string.IsNullOrWhiteSpace(txtDescricaoDetalhada.Text) ? txtDescricaoDetalhada.Text : string.Empty;
                    ObjCarregado.DESCRICAO_DETALHADA_EN = !string.IsNullOrWhiteSpace(txtDescricaoDetalhada_en.Text) ? txtDescricaoDetalhada_en.Text : string.Empty;
                    ObjCarregado.OBSERVACAO = !string.IsNullOrWhiteSpace(txtObservacao.Text) ? txtObservacao.Text : string.Empty;
                    ObjCarregado.ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO;

                    FillNullableObjectValues();

                    FillArquivoDigital();

                    _objBo.Update(ObjCarregado);
                    break;
                default:
                    break;
            }

            _objBo.SaveChanges();

            _objBo = null; ObjCarregado = null;
        }

        private void FillNullableObjectValues()
        {
            if (!string.IsNullOrWhiteSpace(txtData_inicio.Text))
                ObjCarregado.DATA_INICIO = Convert.ToDateTime(txtData_inicio.Text);

            if (!string.IsNullOrWhiteSpace(txtData_fim.Text))
                ObjCarregado.DATA_FIM = Convert.ToDateTime(txtData_fim.Text);

            if (!string.IsNullOrWhiteSpace(txtData_publicacao.Text))
                ObjCarregado.DATA_PUBLICACAO = Convert.ToDateTime(txtData_publicacao.Text);
        }

        private void FillArquivoDigital()
        {
            string path = Server.MapPath(ConfigurationManager.AppSettings["RELATIVEPATH_ARQUIVODIGITAL"]);

            try
            {
                if (ArquivoEnviado != null)
                {
                    if (ObjCarregado.ID_ARQUIVO_DIGITAL.HasValue)
                    {
                        //ObjCarregado.ARQUIVO_DIGITAL.ARQUIVO = ArquivoEnviado.FileBytes;
                        ObjCarregado.ARQUIVO_DIGITAL.NOME = ArquivoEnviado.FileName;
                        ObjCarregado.ARQUIVO_DIGITAL.EXTENSAO = ArquivoEnviado.FileExtension;
                        ObjCarregado.ARQUIVO_DIGITAL.DATA = DateTime.Now;
                    }
                    else
                    {
                        ObjCarregado.ARQUIVO_DIGITAL = new ARQUIVO_DIGITAL()
                        {
                            ID_ARQUIVO = Guid.NewGuid(),
                            NOME = ArquivoEnviado.FileName,
                            EXTENSAO = ArquivoEnviado.FileExtension,
                            //ARQUIVO = ArquivoEnviado.FileBytes,
                            DATA = DateTime.Now
                        };
                    }

                    Util.SaveFile(path, ObjCarregado.ARQUIVO_DIGITAL.ID_ARQUIVO.ToString(), ArquivoEnviado.FileExtension, ArquivoEnviado.FileBytes);
                }
            }
            catch (Exception ex) { throw; }
        }

        public void ButtonRefresh_Click(object sender, ImageClickEventArgs e)
        {
            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    //Nada fazer
                    break;
                case enmUserInterfaceAspect.New:
                    ClearFields();
                    break;
                case enmUserInterfaceAspect.Update:
                    ClearFields();
                    //TODO: Carregar dados do usuário
                    break;
                default:
                    break;
            }
        }

        public void ButtonCancel_Click(object sender, ImageClickEventArgs e)
        {
            ClearMemory();
            ViewAspect = enmUserInterfaceAspect.Start;
            UserInterfaceAspectChanged();
        }

        public void ButtonDelete_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void GetForm() { throw new NotImplementedException(); }

        public void SetForm()
        {
            try
            {
                ddlId_evento_categoria.DataBind();
                ddlId_evento_categoria.SelectedValue = ObjCarregado.ID_EVENTO_CATEGORIA.ToString();

                txtDescricao.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO) ? ObjCarregado.DESCRICAO : string.Empty;
                txtDescricao_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO_EN) ? ObjCarregado.DESCRICAO_EN : string.Empty;
                txtSubDescricao.Text = !string.IsNullOrWhiteSpace(ObjCarregado.SUB_DESCRICAO) ? ObjCarregado.SUB_DESCRICAO : string.Empty;
                txtSubDescricao_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.SUB_DESCRICAO_EN) ? ObjCarregado.SUB_DESCRICAO_EN : string.Empty;
                chkAtivo.Checked = ObjCarregado.ATIVO;
                txtDescricaoDetalhada.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO_DETALHADA) ? ObjCarregado.DESCRICAO_DETALHADA : string.Empty;
                txtDescricaoDetalhada_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO_DETALHADA_EN) ? ObjCarregado.DESCRICAO_DETALHADA_EN : string.Empty;
                txtObservacao.Text = !string.IsNullOrWhiteSpace(ObjCarregado.OBSERVACAO) ? ObjCarregado.OBSERVACAO : string.Empty;

                txtData_inicio.Text = (ObjCarregado.DATA_INICIO.HasValue) ? ObjCarregado.DATA_INICIO.Value.ToShortDateString() : string.Empty;
                txtData_fim.Text = (ObjCarregado.DATA_FIM.HasValue) ? ObjCarregado.DATA_FIM.Value.ToShortDateString() : string.Empty;
                txtData_publicacao.Text = (ObjCarregado.DATA_PUBLICACAO.HasValue) ? ObjCarregado.DATA_PUBLICACAO.Value.ToShortDateString() : string.Empty;

                CarregarArquivo();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        private void CarregarArquivo()
        {
            if (ObjCarregado.ID_ARQUIVO_DIGITAL.HasValue)
            {
                imgEvento.ImageUrl = string.Format("~/ShowFile.aspx?action=1&fileid={0}&width={1}&height={2}", ObjCarregado.ID_ARQUIVO_DIGITAL.ToString(), 400, 300);
                pnlArquivoAtual.Visible = true;
            }
            else
            {
                pnlArquivoAtual.Visible = false;
                imgEvento.ImageUrl = "~/ShowFile.aspx?action=1";
            }
        }

        public void EnableFields() { throw new NotImplementedException(); }

        public void DisableFields() { throw new NotImplementedException(); }

        public void ClearFields()
        {
            try
            {
                txtDescricao.Text = txtDescricao_en.Text = txtSubDescricao.Text = txtSubDescricao_en.Text = string.Empty;
                txtDescricaoDetalhada.Text = txtDescricaoDetalhada_en.Text = txtObservacao.Text = string.Empty;
                txtData_inicio.Text = txtData_fim.Text = txtData_publicacao.Text = string.Empty;
                ddlId_evento_categoria.SelectedIndex = 0;
                chkAtivo.Checked = true;
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ClearMemory() { ObjCarregado = null; }

        protected void btnFiltrar_Click(object sender, ImageClickEventArgs e)
        {
            try { gvView.DataBind(); }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void gvView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _SelectedKey = Convert.ToInt32(gvView.SelectedDataKey.Values["ID_EVENTO"]);

                ObjCarregado = new EventoBo().Find(lbda => lbda.ID_EVENTO == _SelectedKey).First();

                ViewAspect = enmUserInterfaceAspect.Update;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int currentPageIndex, _SelectedKey;

                switch (e.CommandName)
                {
                    case "Excluir":
                        _SelectedKey = Convert.ToInt32(e.CommandArgument);

                        EventoBo _objBo = new EventoBo();
                        _objBo.Delete(_objBo.Find(lbda => lbda.ID_EVENTO == _SelectedKey).First());
                        _objBo.SaveChanges();
                        _objBo = null;

                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex >= 0 && currentPageIndex <= gvView.PageCount - 1)
                            gvView.PageIndex = currentPageIndex;
                        break;
                    case "FirstPage":
                        gvView.DataBind();
                        gvView.PageIndex = 0;
                        break;
                    case "BackPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex > 0)
                            gvView.PageIndex = currentPageIndex - 1;
                        break;
                    case "GoToPage":
                        currentPageIndex = gvView.PageIndex;

                        int goToTop = 1, goToBottom = 1, goTo = 1;
                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text))
                            goToTop = int.Parse(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text);

                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text))
                            goToBottom = int.Parse(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text);

                        gvView.DataBind();

                        goTo = (goToTop != currentPageIndex + 1) ? goToTop : goToBottom;

                        if (goTo >= 1 && goTo <= gvView.PageCount)
                            gvView.PageIndex = goTo - 1;
                        break;
                    case "NextPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex < gvView.PageCount)
                            gvView.PageIndex = currentPageIndex + 1;
                        break;
                    case "LastPage":
                        gvView.DataBind();
                        gvView.PageIndex = gvView.PageCount - 1;
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void edsView_QueryCreated(object sender, QueryCreatedEventArgs e)
        {
            try
            {
                var result = e.Query.Cast<EVENTO>();

                if (!string.IsNullOrWhiteSpace(txtFiltroDescricao.Text))
                {
                    e.Query = from item in result
                              where
                                item.DESCRICAO.Contains(txtFiltroDescricao.Text)
                              select item;
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void afuArquivoEvento_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e) { ArquivoEnviado = HTMSiteAdmin.Library.UserInterface.WebControlsUtility.AsyncFileUpload_UploadedComplete(sender, e); }

        protected void btnExcluirImagem_Click(object sender, ImageClickEventArgs e)
        {
            EventoBo _objBo = new EventoBo();

            ObjCarregado = _objBo.Find(s => s.ID_EVENTO == ObjCarregado.ID_EVENTO).First();

            ARQUIVO_DIGITAL _objArquivoDigital = ObjCarregado.ARQUIVO_DIGITAL;

            ObjCarregado.ID_ARQUIVO_DIGITAL = null;

            _objBo.Update(ObjCarregado);
            _objBo.SaveChanges();

            ArquivoDigitalBo _objArquivoBo = new ArquivoDigitalBo();
            _objArquivoDigital = _objArquivoBo.Find(s => s.ID_ARQUIVO == _objArquivoDigital.ID_ARQUIVO).First();
            _objArquivoBo.Delete(_objArquivoDigital);
            _objArquivoBo.SaveChanges();

            ObjCarregado = _objBo.Find(s => s.ID_EVENTO == ObjCarregado.ID_EVENTO).First();

            _objArquivoDigital = null; _objBo = null; _objArquivoBo = null;

            CarregarArquivo();
        }

        protected void ddlId_evento_categoria_DataBound(object sender, EventArgs e)
        {
            HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender);
        }
    }
}