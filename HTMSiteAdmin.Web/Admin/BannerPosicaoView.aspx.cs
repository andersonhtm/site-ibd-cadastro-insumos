﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Library.Exceptions;
using HTMSiteAdmin.Business.Banners;
using HTMSiteAdmin.Library;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class BannerPosicaoView : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this, ViewToken, "ViewAspect", value); }
        }
        private BANNER_POSICAO ObjCarregado
        {
            get { return (BANNER_POSICAO)Global.RestoreObject(this, ViewToken, "ObjCarregado"); }
            set { Global.SaveObject(this, ViewToken, "ObjCarregado", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MapButtons();

            if (!IsPostBack && ObjCarregado == null)
                ViewAspect = enmUserInterfaceAspect.Start;

            if (!IsPostBack) UserInterfaceAspectChanged();
        }

        public void ValidateAcess() { throw new NotImplementedException(); }

        public void MapButtons()
        {
            ucButtons1.ButtonNew.CausesValidation = false;
            ucButtons1.ButtonNew.Click += new ImageClickEventHandler(ButtonNew_Click);

            ucButtons1.ButtonSave.ValidationGroup = "vgView";
            ucButtons1.ButtonSave.CausesValidation = true;
            ucButtons1.ButtonSave.Click += new ImageClickEventHandler(ButtonSave_Click);

            ucButtons1.ButtonSaveNew.ValidationGroup = "vgView";
            ucButtons1.ButtonSaveNew.CausesValidation = true;
            ucButtons1.ButtonSaveNew.Click += new ImageClickEventHandler(ButtonSaveNew_Click);

            ucButtons1.ButtonCancel.CausesValidation = false;
            ucButtons1.ButtonCancel.Click += new ImageClickEventHandler(ButtonCancel_Click);

            ucButtons1.ButtonRefresh.CausesValidation = false;
            ucButtons1.ButtonRefresh.Click += new ImageClickEventHandler(ButtonRefresh_Click);

            ucButtons1.ButtonDelete.CausesValidation = false;
            ucButtons1.ButtonDelete.Click += new ImageClickEventHandler(ButtonDelete_Click);
        }

        public void UserInterfaceAspectChanged()
        {
            StartPageControls();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    ucButtons1.ButtonNew.Visible = true;
                    ucButtons1.ButtonSave.Visible = false;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = false;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = false;

                    int currentPageIndex = gvView.PageIndex;
                    gvView.DataBind();
                    gvView.PageIndex = currentPageIndex;

                    pnlViewCollection.Visible = gvView.Rows.Count > 0;
                    break;
                case enmUserInterfaceAspect.New:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = true;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    RestartForm();
                    break;
                case enmUserInterfaceAspect.Update:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    ClearFields();

                    SetForm();
                    break;
                default:
                    break;
            }
        }

        public void StartPageControls()
        {
            try
            {
                if (!IsPostBack) { }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void RestartForm()
        {
            ClearMemory();
            ClearFields();
        }

        public void ButtonNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSaveNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonRefresh_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                switch (ViewAspect)
                {
                    case enmUserInterfaceAspect.Start:
                        //Nada fazer
                        break;
                    case enmUserInterfaceAspect.New:
                        ClearFields();
                        break;
                    case enmUserInterfaceAspect.Update:
                        ClearFields();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonCancel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ClearMemory();
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonDelete_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void GetForm() { throw new NotImplementedException(); }

        public void SaveView()
        {
            try
            {
                BannerPosicaoBo _objBo = new BannerPosicaoBo();

                switch (ViewAspect)
                {
                    case enmUserInterfaceAspect.Start:
                        break;
                    case enmUserInterfaceAspect.New:
                        ObjCarregado = new BANNER_POSICAO()
                        {
                            ID_BANNER_POSICAO_SITUACAO = int.Parse(ddlSituacao.SelectedValue),
                            RESOURCE_STRING_NAME = !string.IsNullOrWhiteSpace(txtResource_string_name.Text) ? txtResource_string_name.Text : string.Empty,
                            NOME = txtNome.Text,
                            DESCRICAO = !string.IsNullOrWhiteSpace(txtDescricao.Text) ? txtDescricao.Text : string.Empty,
                            APENAS_UM_ATIVO = chkApenas_um_ativo.Checked,
                            ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO
                        };

                        FillNullabeFields();

                        _objBo.Add(ObjCarregado);
                        break;
                    case enmUserInterfaceAspect.Update:
                        ObjCarregado = _objBo.Find(lbda => lbda.ID_BANNER_POSICAO == ObjCarregado.ID_BANNER_POSICAO).First();

                        ObjCarregado.ID_BANNER_POSICAO_SITUACAO = int.Parse(ddlSituacao.SelectedValue);
                        ObjCarregado.RESOURCE_STRING_NAME = !string.IsNullOrWhiteSpace(txtResource_string_name.Text) ? txtResource_string_name.Text : string.Empty;
                        ObjCarregado.NOME = txtNome.Text;
                        ObjCarregado.DESCRICAO = !string.IsNullOrWhiteSpace(txtDescricao.Text) ? txtDescricao.Text : string.Empty;
                        ; ObjCarregado.APENAS_UM_ATIVO = chkApenas_um_ativo.Checked;
                        ObjCarregado.ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO;

                        FillNullabeFields();

                        _objBo.Update(ObjCarregado);
                        break;
                    default:
                        break;
                }

                _objBo.SaveChanges();

                _objBo = null; ObjCarregado = null;
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao salvar os dados. Mensagem: " + ex.Message);
            }
        }

        private void FillNullabeFields()
        {
            if (!string.IsNullOrWhiteSpace(txtLargura_maxima.Text))
                ObjCarregado.LARGURA_MAXIMA = int.Parse(txtLargura_maxima.Text);

            if (!string.IsNullOrWhiteSpace(txtAltura_maxima.Text))
                ObjCarregado.ALTURA_MAXIMA = int.Parse(txtAltura_maxima.Text);
        }

        public void SetForm()
        {
            try
            {
                ddlSituacao.SelectedValue = ObjCarregado.ID_BANNER_POSICAO_SITUACAO.ToString();
                txtResource_string_name.Text = !string.IsNullOrWhiteSpace(ObjCarregado.RESOURCE_STRING_NAME) ? ObjCarregado.RESOURCE_STRING_NAME : string.Empty;
                txtNome.Text = ObjCarregado.NOME;
                txtLargura_maxima.Text = ObjCarregado.LARGURA_MAXIMA.HasValue ? ObjCarregado.LARGURA_MAXIMA.ToString() : string.Empty;
                txtAltura_maxima.Text = ObjCarregado.ALTURA_MAXIMA.HasValue ? ObjCarregado.ALTURA_MAXIMA.ToString() : string.Empty;
                txtDescricao.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO) ? ObjCarregado.DESCRICAO : string.Empty;
                chkApenas_um_ativo.Checked = ObjCarregado.APENAS_UM_ATIVO.HasValue ? ObjCarregado.APENAS_UM_ATIVO.Value : true;
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao carregar o formulário. Mensagem: " + ex.Message);
            }
        }

        public void EnableFields() { throw new NotImplementedException(); }

        public void DisableFields() { throw new NotImplementedException(); }

        public void ClearFields()
        {
            try
            {
                txtResource_string_name.Text = txtNome.Text = txtDescricao.Text = txtLargura_maxima.Text = txtAltura_maxima.Text = string.Empty;
                ddlSituacao.SelectedValue = "1";
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao limpar os campos do formulário. Mensagem: " + ex.Message);
            }
        }

        public void ClearMemory() { ObjCarregado = null; }

        protected void btnFiltrar_Click(object sender, ImageClickEventArgs e)
        {
            try { gvView.DataBind(); }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void gvView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((Literal)e.Row.FindControl("ltrSituacao")).Text = new BannerPosicaoSituacaoBo().Find(lbda => lbda.ID_BANNER_POSICAO_SITUACAO == ((BANNER_POSICAO)e.Row.DataItem).ID_BANNER_POSICAO_SITUACAO).First().NOME;
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _SelectedKey = Convert.ToInt32(gvView.SelectedDataKey.Values["ID_BANNER_POSICAO"]);

                ObjCarregado = new BannerPosicaoBo().Find(lbda => lbda.ID_BANNER_POSICAO == _SelectedKey).First();

                ViewAspect = enmUserInterfaceAspect.Update;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int currentPageIndex, _ID_BANNER_POSICAO;

                switch (e.CommandName)
                {
                    case "Excluir":
                        _ID_BANNER_POSICAO = _ID_BANNER_POSICAO = Convert.ToInt32(e.CommandArgument);

                        if (new BannerBo().Find(lbda => lbda.ID_BANNER_POSICAO == _ID_BANNER_POSICAO).Count() > 0)
                            throw new HTMSiteAdminException("Registro está sendo utilizado e não poderá ser excluído, remova todas as referências a este registro, depois tente novamente.");

                        BannerPosicaoBo _objBo = new BannerPosicaoBo();
                        _objBo.Delete(_objBo.Find(lbda => lbda.ID_BANNER_POSICAO == _ID_BANNER_POSICAO).First());
                        _objBo.SaveChanges();
                        _objBo = null;

                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex >= 0 && currentPageIndex <= gvView.PageCount - 1)
                            gvView.PageIndex = currentPageIndex;
                        break;
                    case "FirstPage":
                        gvView.DataBind();
                        gvView.PageIndex = 0;
                        break;
                    case "BackPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex > 0)
                            gvView.PageIndex = currentPageIndex - 1;
                        break;
                    case "GoToPage":
                        currentPageIndex = gvView.PageIndex;

                        int goToTop = 1, goToBottom = 1, goTo = 1;
                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text))
                            goToTop = int.Parse(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text);

                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text))
                            goToBottom = int.Parse(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text);

                        gvView.DataBind();

                        goTo = (goToTop != currentPageIndex + 1) ? goToTop : goToBottom;

                        if (goTo >= 1 && goTo <= gvView.PageCount)
                            gvView.PageIndex = goTo - 1;
                        break;
                    case "NextPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex < gvView.PageCount)
                            gvView.PageIndex = currentPageIndex + 1;
                        break;
                    case "LastPage":
                        gvView.DataBind();
                        gvView.PageIndex = gvView.PageCount - 1;
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void gvView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                switch (e.SortExpression)
                {
                    case "ID_BANNER_POSICAO":
                        switch (e.SortDirection)
                        {
                            case SortDirection.Ascending:
                                gvView.DataSource = new BannerPosicaoBo().GetAll().OrderBy(lbda => lbda.ID_BANNER_POSICAO);
                                break;
                            case SortDirection.Descending:
                                gvView.DataSource = new BannerPosicaoBo().GetAll().OrderByDescending(lbda => lbda.ID_BANNER_POSICAO);
                                break;
                            default:
                                gvView.DataSource = new BannerPosicaoBo().GetAll().OrderBy(lbda => lbda.ID_BANNER_POSICAO);
                                break;
                        }

                        break;
                    case "NOME":
                        switch (e.SortDirection)
                        {
                            case SortDirection.Ascending:
                                gvView.DataSource = new BannerPosicaoBo().GetAll().OrderBy(lbda => lbda.NOME);
                                break;
                            case SortDirection.Descending:
                                gvView.DataSource = new BannerPosicaoBo().GetAll().OrderByDescending(lbda => lbda.NOME);
                                break;
                            default:
                                gvView.DataSource = new BannerPosicaoBo().GetAll().OrderBy(lbda => lbda.NOME);
                                break;
                        }
                        break;
                    case "LARGURA_MAXIMA":
                        switch (e.SortDirection)
                        {
                            case SortDirection.Ascending:
                                gvView.DataSource = new BannerPosicaoBo().GetAll().OrderBy(lbda => lbda.LARGURA_MAXIMA);
                                break;
                            case SortDirection.Descending:
                                gvView.DataSource = new BannerPosicaoBo().GetAll().OrderByDescending(lbda => lbda.LARGURA_MAXIMA);
                                break;
                            default:
                                gvView.DataSource = new BannerPosicaoBo().GetAll().OrderBy(lbda => lbda.LARGURA_MAXIMA);
                                break;
                        }
                        break;
                    case "ALTURA_MAXIMA":
                        switch (e.SortDirection)
                        {
                            case SortDirection.Ascending:
                                gvView.DataSource = new BannerPosicaoBo().GetAll().OrderBy(lbda => lbda.ALTURA_MAXIMA);
                                break;
                            case SortDirection.Descending:
                                gvView.DataSource = new BannerPosicaoBo().GetAll().OrderByDescending(lbda => lbda.ALTURA_MAXIMA);
                                break;
                            default:
                                gvView.DataSource = new BannerPosicaoBo().GetAll().OrderBy(lbda => lbda.ALTURA_MAXIMA);
                                break;
                        }
                        break;
                    default:
                        gvView.DataSource = new BannerPosicaoBo().GetAll();
                        break;
                }

                gvView.DataBind();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void edsView_QueryCreated(object sender, QueryCreatedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(txtFiltroDescricao.Text))
                {
                    var result = e.Query.Cast<BANNER_POSICAO>();
                    e.Query = from item in result
                              where item.NOME.Contains(txtFiltroDescricao.Text) || item.DESCRICAO.Contains(txtFiltroDescricao.Text)
                              select item;
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void btnLimparCamposFiltro_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                txtFiltroDescricao.Text = string.Empty;

                gvView.DataBind();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}