﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Links;
using HTMSiteAdmin.Library;
using System.Configuration;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class LinkView : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this, ViewToken, "ViewAspect", value); }
        }
        private LINK ObjCarregado
        {
            get { return (LINK)Global.RestoreObject(this, ViewToken, "ObjCarregado"); }
            set { Global.SaveObject(this, ViewToken, "ObjCarregado", value); }
        }
        public UploadedFile ArquivoEnviado
        {
            get { return (UploadedFile)Global.RestoreObject(this, ViewToken, "ArquivoEnviado"); }
            set { Global.SaveObject(this, ViewToken, "ArquivoEnviado", value); }
        }
        public UploadedFile ArquivoEnviado_en
        {
            get { return (UploadedFile)Global.RestoreObject(this, ViewToken, "ArquivoEnviado_en"); }
            set { Global.SaveObject(this, ViewToken, "ArquivoEnviado_en", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MapButtons();

            if (!IsPostBack && ObjCarregado == null)
                ViewAspect = enmUserInterfaceAspect.Start;

            if (!IsPostBack) UserInterfaceAspectChanged();
        }

        public void ValidateAcess() { throw new NotImplementedException(); }

        public void MapButtons()
        {
            ucButtons1.ButtonNew.CausesValidation = false;
            ucButtons1.ButtonNew.Click += new ImageClickEventHandler(ButtonNew_Click);

            ucButtons1.ButtonSave.ValidationGroup = "vgView";
            ucButtons1.ButtonSave.CausesValidation = true;
            ucButtons1.ButtonSave.Click += new ImageClickEventHandler(ButtonSave_Click);

            ucButtons1.ButtonSaveNew.ValidationGroup = "vgView";
            ucButtons1.ButtonSaveNew.CausesValidation = true;
            ucButtons1.ButtonSaveNew.Click += new ImageClickEventHandler(ButtonSaveNew_Click);

            ucButtons1.ButtonCancel.CausesValidation = false;
            ucButtons1.ButtonCancel.Click += new ImageClickEventHandler(ButtonCancel_Click);

            ucButtons1.ButtonRefresh.CausesValidation = false;
            ucButtons1.ButtonRefresh.Click += new ImageClickEventHandler(ButtonRefresh_Click);

            ucButtons1.ButtonDelete.CausesValidation = false;
            ucButtons1.ButtonDelete.Click += new ImageClickEventHandler(ButtonDelete_Click);
        }

        public void UserInterfaceAspectChanged()
        {
            StartPageControls();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    ucButtons1.ButtonNew.Visible = true;
                    ucButtons1.ButtonSave.Visible = false;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = false;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = false;

                    int currentPageIndex = gvView.PageIndex;
                    gvView.DataBind();
                    gvView.PageIndex = currentPageIndex;

                    pnlViewCollection.Visible = gvView.Rows.Count > 0;
                    break;
                case enmUserInterfaceAspect.New:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = true;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    RestartForm();
                    break;
                case enmUserInterfaceAspect.Update:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    ClearFields();

                    SetForm();
                    break;
                default:
                    break;
            }
        }

        public void RestartForm()
        {
            ClearMemory();
            ClearFields();
        }

        public void StartPageControls()
        {
            try
            {
                if (!IsPostBack)
                {
                    ddlId_link_tipo.DataBind();
                    ddlId_link_categoria.DataBind();
                    ddlFiltroCategoria.DataBind();
                    ddlFiltroTipo.DataBind();
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSaveNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void SaveView()
        {
            LinkBo _objBo = new LinkBo();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    break;
                case enmUserInterfaceAspect.New:
                    ObjCarregado = new LINK()
                    {
                        DESCRICAO = !string.IsNullOrWhiteSpace(txtDescricao.Text) ? txtDescricao.Text : string.Empty,
                        DESCRICAO_EN = !string.IsNullOrWhiteSpace(txtDescricaoEn.Text) ? txtDescricaoEn.Text : string.Empty,
                        DESCRICAO_DET = !string.IsNullOrWhiteSpace(txtDescricao_det.Text) ? txtDescricao_det.Text : string.Empty,
                        DESCRICAO_DET_EN = !string.IsNullOrWhiteSpace(txtDescricao_det_en.Text) ? txtDescricao_det_en.Text : string.Empty,
                        ID_LINK_SITUACAO = int.Parse(ddlSituacao.SelectedValue),
                        ID_LINK_CATEGORIA = int.Parse(ddlId_link_categoria.SelectedValue),
                        ID_LINK_TIPO = int.Parse(ddlId_link_tipo.SelectedValue),
                        CAMINHO = !string.IsNullOrWhiteSpace(txtCaminho.Text) ? txtCaminho.Text : string.Empty,
                        CAMINHO_EN = !string.IsNullOrWhiteSpace(txtCaminho_en.Text) ? txtCaminho_en.Text : string.Empty,
                        ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO
                    };

                    FillArquivoDigital();

                    _objBo.Add(ObjCarregado);
                    break;
                case enmUserInterfaceAspect.Update:
                    ObjCarregado = _objBo.Find(lbda => lbda.ID_LINK == ObjCarregado.ID_LINK).First();

                    ObjCarregado.DESCRICAO = !string.IsNullOrWhiteSpace(txtDescricao.Text) ? txtDescricao.Text : string.Empty;
                    ObjCarregado.DESCRICAO_EN = !string.IsNullOrWhiteSpace(txtDescricaoEn.Text) ? txtDescricaoEn.Text : string.Empty;
                    ObjCarregado.DESCRICAO_DET = !string.IsNullOrWhiteSpace(txtDescricao_det.Text) ? txtDescricao_det.Text : string.Empty;
                    ObjCarregado.DESCRICAO_DET_EN = !string.IsNullOrWhiteSpace(txtDescricao_det_en.Text) ? txtDescricao_det_en.Text : string.Empty;
                    ObjCarregado.ID_LINK_SITUACAO = int.Parse(ddlSituacao.SelectedValue);
                    ObjCarregado.ID_LINK_CATEGORIA = int.Parse(ddlId_link_categoria.SelectedValue);
                    ObjCarregado.ID_LINK_TIPO = int.Parse(ddlId_link_tipo.SelectedValue);
                    ObjCarregado.CAMINHO = !string.IsNullOrWhiteSpace(txtCaminho.Text) ? txtCaminho.Text : string.Empty;
                    ObjCarregado.CAMINHO_EN = !string.IsNullOrWhiteSpace(txtCaminho_en.Text) ? txtCaminho_en.Text : string.Empty;
                    ObjCarregado.ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO;

                    FillArquivoDigital();

                    _objBo.Update(ObjCarregado);
                    break;
                default:
                    break;
            }

            _objBo.SaveChanges();

            _objBo = null; ObjCarregado = null;
        }

        private void FillArquivoDigital()
        {
            string path = Server.MapPath(ConfigurationManager.AppSettings["RELATIVEPATH_ARQUIVODIGITAL"]);

            try
            {
                if (ArquivoEnviado != null)
                {
                    if (ObjCarregado.ID_ARQUIVO.HasValue)
                    {
                        //ObjCarregado.ARQUIVO_DIGITAL.ARQUIVO = ArquivoEnviado.FileBytes;
                        ObjCarregado.ARQUIVO_DIGITAL.NOME = ArquivoEnviado.FileName;
                        ObjCarregado.ARQUIVO_DIGITAL.EXTENSAO = ArquivoEnviado.FileExtension;
                        ObjCarregado.ARQUIVO_DIGITAL.DATA = DateTime.Now;
                    }
                    else
                    {
                        ObjCarregado.ARQUIVO_DIGITAL = new ARQUIVO_DIGITAL()
                        {
                            ID_ARQUIVO = Guid.NewGuid(),
                            NOME = ArquivoEnviado.FileName,
                            EXTENSAO = ArquivoEnviado.FileExtension,
                            //ARQUIVO = ArquivoEnviado.FileBytes,
                            DATA = DateTime.Now
                        };
                    }

                    Util.SaveFile(path, ObjCarregado.ARQUIVO_DIGITAL.ID_ARQUIVO.ToString(), ArquivoEnviado.FileExtension, ArquivoEnviado.FileBytes);
                }


                if (ArquivoEnviado_en != null)
                {
                    if (ObjCarregado.ID_ARQUIVO_EN.HasValue)
                    {
                        //ObjCarregado.ARQUIVO_DIGITAL1.ARQUIVO = ArquivoEnviado_en.FileBytes;
                        ObjCarregado.ARQUIVO_DIGITAL1.NOME = ArquivoEnviado_en.FileName;
                        ObjCarregado.ARQUIVO_DIGITAL1.EXTENSAO = ArquivoEnviado_en.FileExtension;
                        ObjCarregado.ARQUIVO_DIGITAL1.DATA = DateTime.Now;
                    }
                    else
                    {
                        ObjCarregado.ARQUIVO_DIGITAL1 = new ARQUIVO_DIGITAL()
                        {
                            ID_ARQUIVO = Guid.NewGuid(),
                            NOME = ArquivoEnviado_en.FileName,
                            EXTENSAO = ArquivoEnviado_en.FileExtension,
                            //ARQUIVO = ArquivoEnviado_en.FileBytes,
                            DATA = DateTime.Now
                        };
                    }

                    Util.SaveFile(path, ObjCarregado.ARQUIVO_DIGITAL1.ID_ARQUIVO.ToString(), ArquivoEnviado_en.FileExtension, ArquivoEnviado_en.FileBytes);
                }
            }
            catch (Exception ex) { throw; }
        }

        public void ButtonRefresh_Click(object sender, ImageClickEventArgs e)
        {
            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    //Nada fazer
                    break;
                case enmUserInterfaceAspect.New:
                    ClearFields();
                    break;
                case enmUserInterfaceAspect.Update:
                    ClearFields();
                    //TODO: Carregar dados do usuário
                    break;
                default:
                    break;
            }
        }

        public void ButtonCancel_Click(object sender, ImageClickEventArgs e)
        {
            ClearMemory();
            ViewAspect = enmUserInterfaceAspect.Start;
            UserInterfaceAspectChanged();
        }

        public void ButtonDelete_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void GetForm() { throw new NotImplementedException(); }

        public void SetForm()
        {
            try
            {
                txtDescricao.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO) ? ObjCarregado.DESCRICAO : string.Empty;
                txtDescricaoEn.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO_EN) ? ObjCarregado.DESCRICAO_EN : string.Empty;
                txtDescricao_det.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO_DET) ? ObjCarregado.DESCRICAO_DET : string.Empty;
                txtDescricao_det_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO_DET_EN) ? ObjCarregado.DESCRICAO_DET_EN : string.Empty;
                txtCaminho.Text = !string.IsNullOrWhiteSpace(ObjCarregado.CAMINHO) ? ObjCarregado.CAMINHO : string.Empty;
                txtCaminho_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.CAMINHO_EN) ? ObjCarregado.CAMINHO_EN : string.Empty;

                ddlId_link_categoria.DataBind();
                ddlId_link_categoria.SelectedValue = ObjCarregado.ID_LINK_CATEGORIA.ToString();

                ddlId_link_tipo.DataBind();
                ddlId_link_tipo.SelectedValue = ObjCarregado.ID_LINK_TIPO.ToString();


                ddlSituacao.SelectedValue = ObjCarregado.ID_LINK_SITUACAO.ToString();

                CarregarArquivosDigitais();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void EnableFields() { throw new NotImplementedException(); }

        public void DisableFields() { throw new NotImplementedException(); }

        public void ClearFields()
        {
            try
            {
                txtDescricaoEn.Text = txtDescricao.Text = txtDescricao_det.Text = txtDescricao_det_en.Text = txtCaminho.Text = txtCaminho_en.Text = string.Empty;
                ddlId_link_categoria.SelectedIndex = 0;
                ddlId_link_tipo.SelectedIndex = 0;
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ClearMemory() { ObjCarregado = null; }

        protected void btnFiltrar_Click(object sender, ImageClickEventArgs e)
        {
            try { gvView.DataBind(); }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void gvView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((Literal)e.Row.FindControl("ltrSituacao")).Text = new LinkSituacaoBo().Find(lbda => lbda.ID_LINK_SITUACAO == ((LINK)e.Row.DataItem).ID_LINK_SITUACAO).First().DESCRICAO;
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _SelectedKey = Convert.ToInt32(gvView.SelectedDataKey.Values["ID_LINK"]);

                ObjCarregado = new LinkBo().Find(lbda => lbda.ID_LINK == _SelectedKey).First();

                ViewAspect = enmUserInterfaceAspect.Update;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int currentPageIndex, _SelectedKey;

                switch (e.CommandName)
                {
                    case "Excluir":
                        _SelectedKey = Convert.ToInt32(e.CommandArgument);

                        LinkBo _objBo = new LinkBo();
                        _objBo.Delete(_objBo.Find(lbda => lbda.ID_LINK == _SelectedKey).First());
                        _objBo.SaveChanges();
                        _objBo = null;

                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex >= 0 && currentPageIndex <= gvView.PageCount - 1)
                            gvView.PageIndex = currentPageIndex;
                        break;
                    case "FirstPage":
                        gvView.DataBind();
                        gvView.PageIndex = 0;
                        break;
                    case "BackPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex > 0)
                            gvView.PageIndex = currentPageIndex - 1;
                        break;
                    case "GoToPage":
                        currentPageIndex = gvView.PageIndex;

                        int goToTop = 1, goToBottom = 1, goTo = 1;
                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text))
                            goToTop = int.Parse(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text);

                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text))
                            goToBottom = int.Parse(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text);

                        gvView.DataBind();

                        goTo = (goToTop != currentPageIndex + 1) ? goToTop : goToBottom;

                        if (goTo >= 1 && goTo <= gvView.PageCount)
                            gvView.PageIndex = goTo - 1;
                        break;
                    case "NextPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex < gvView.PageCount)
                            gvView.PageIndex = currentPageIndex + 1;
                        break;
                    case "LastPage":
                        gvView.DataBind();
                        gvView.PageIndex = gvView.PageCount - 1;
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void edsView_QueryCreated(object sender, QueryCreatedEventArgs e)
        {
            try
            {
                var result = e.Query.Cast<LINK>();
                int idSituacao = int.Parse(ddlFiltroSituacao.SelectedValue);
                int idCategoria = int.Parse(ddlFiltroCategoria.SelectedValue);
                int idTipo = int.Parse(ddlFiltroTipo.SelectedValue);

                if (idTipo == 0)
                {
                    if (!string.IsNullOrWhiteSpace(txtFiltroDescricao.Text))
                    {
                        if (idCategoria > 0)
                        {
                            e.Query = from item in result
                                      where
                                        item.DESCRICAO_EN.Contains(txtFiltroDescricao.Text) || item.DESCRICAO.Contains(txtFiltroDescricao.Text)
                                        && item.ID_LINK_CATEGORIA == idCategoria
                                        && item.ID_LINK_SITUACAO == idSituacao
                                      select item;
                        }
                        else
                        {
                            e.Query = from item in result
                                      where
                                        item.DESCRICAO_EN.Contains(txtFiltroDescricao.Text) || item.DESCRICAO.Contains(txtFiltroDescricao.Text)
                                        && item.ID_LINK_SITUACAO == idSituacao
                                      select item;
                        }
                    }
                    else
                    {
                        if (idCategoria > 0)
                        {
                            e.Query = from item in result
                                      where
                                        item.ID_LINK_CATEGORIA == idCategoria
                                        && item.ID_LINK_SITUACAO == idSituacao
                                      select item;
                        }
                        else
                        {
                            e.Query = from item in result
                                      where
                                        item.ID_LINK_SITUACAO == idSituacao
                                      select item;
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(txtFiltroDescricao.Text))
                    {
                        if (idCategoria > 0)
                        {
                            e.Query = from item in result
                                      where
                                        item.DESCRICAO_EN.Contains(txtFiltroDescricao.Text) || item.DESCRICAO.Contains(txtFiltroDescricao.Text)
                                        && item.ID_LINK_CATEGORIA == idCategoria
                                        && item.ID_LINK_SITUACAO == idSituacao
                                        && item.ID_LINK_TIPO == idTipo
                                      select item;
                        }
                        else
                        {
                            e.Query = from item in result
                                      where
                                        item.DESCRICAO_EN.Contains(txtFiltroDescricao.Text) || item.DESCRICAO.Contains(txtFiltroDescricao.Text)
                                        && item.ID_LINK_SITUACAO == idSituacao
                                        && item.ID_LINK_TIPO == idTipo
                                      select item;
                        }
                    }
                    else
                    {
                        if (idCategoria > 0)
                        {
                            e.Query = from item in result
                                      where
                                        item.ID_LINK_CATEGORIA == idCategoria
                                        && item.ID_LINK_SITUACAO == idSituacao
                                        && item.ID_LINK_TIPO == idTipo
                                      select item;
                        }
                        else
                        {
                            e.Query = from item in result
                                      where
                                        item.ID_LINK_SITUACAO == idSituacao
                                        && item.ID_LINK_TIPO == idTipo
                                      select item;
                        }
                    }
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void afuArquivoBanner_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e) { ArquivoEnviado = HTMSiteAdmin.Library.UserInterface.WebControlsUtility.AsyncFileUpload_UploadedComplete(sender, e); }

        protected void afuArquivoBanner_en_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e) { ArquivoEnviado_en = HTMSiteAdmin.Library.UserInterface.WebControlsUtility.AsyncFileUpload_UploadedComplete(sender, e); }

        private void CarregarArquivosDigitais()
        {
            if (ObjCarregado.ID_ARQUIVO.HasValue)
            {
                imgBanner.ImageUrl = string.Format("~/ShowFile.aspx?action=1&fileid={0}&width={1}&height={2}", ObjCarregado.ID_ARQUIVO.ToString(), 200, 100);
                pnlArquivoAtual.Visible = true;
            }
            else
            {
                pnlArquivoAtual.Visible = false;
                imgBanner.ImageUrl = "~/ShowFile.aspx?action=1";
            }

            if (ObjCarregado.ID_ARQUIVO_EN.HasValue)
            {
                imgBanner_en.ImageUrl = string.Format("~/ShowFile.aspx?action=1&fileid={0}&width={1}&height={2}", ObjCarregado.ID_ARQUIVO_EN.ToString(), 200, 100);
                pnlArquivoAtual_en.Visible = true;
            }
            else
            {
                pnlArquivoAtual_en.Visible = false;
                imgBanner_en.ImageUrl = "~/ShowFile.aspx?action=1";
            }
        }

        protected void ddlId_link_tipo_DataBound(object sender, EventArgs e) { HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender); }

        protected void ddlId_link_categoria_DataBound(object sender, EventArgs e) { HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender); }

        protected void ddlFiltroCategoria_DataBound(object sender, EventArgs e) { HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender); }

        protected void ddlFiltroTipo_DataBound(object sender, EventArgs e) { HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender); }
    }
}