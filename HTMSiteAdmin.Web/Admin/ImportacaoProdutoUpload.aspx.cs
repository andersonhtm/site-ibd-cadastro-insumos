﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Importacao;
using HTMSiteAdmin.Library.Exceptions;
using System.Text;
using System.IO;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class ImportacaoProdutoUpload : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this, ViewToken, "ViewAspect", value); }
        }
        private UploadedFile ArquivoEnviado
        {
            get { return (UploadedFile)Global.RestoreObject(this, ViewToken, "ArquivoEnviado"); }
            set { Global.SaveObject(this, ViewToken, "ArquivoEnviado", value); }
        }
        private List<IMPORTACAO_PRODUTO> RegistrosLidos
        {
            get { return (List<IMPORTACAO_PRODUTO>)Global.RestoreObject(this, ViewToken, "RegistrosLidos"); }
            set { Global.SaveObject(this, ViewToken, "RegistrosLidos", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
        }

        public void ValidateAcess() { throw new NotImplementedException(); }

        public void MapButtons() { throw new NotImplementedException(); }

        public void UserInterfaceAspectChanged()
        {
            //StartPageControls();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    break;
                case enmUserInterfaceAspect.New:
                    break;
                case enmUserInterfaceAspect.Update:
                    break;
                default:
                    break;
            }
        }

        public void StartPageControls() { throw new NotImplementedException(); }

        public void RestartForm() { throw new NotImplementedException(); }

        public void ClearMemory() { throw new NotImplementedException(); }

        public void ClearFields() { throw new NotImplementedException(); }

        public void EnableFields() { throw new NotImplementedException(); }

        public void DisableFields() { throw new NotImplementedException(); }

        public void ButtonNew_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonSave_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonSaveNew_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonRefresh_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonCancel_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonDelete_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void GetForm() { throw new NotImplementedException(); }

        public void SaveView() { throw new NotImplementedException(); }

        public void SetForm() { throw new NotImplementedException(); }

        protected void afuArquivo_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e) { ArquivoEnviado = HTMSiteAdmin.Library.UserInterface.WebControlsUtility.AsyncFileUpload_UploadedComplete(sender, e); }

        protected void btnLerArquivo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (ArquivoEnviado == null)
                    throw new HTMSiteAdminException("Nenhum arquivo foi enviado.");

                if (!ArquivoEnviado.FileExtension.ToUpper().Contains("CSV"))
                    throw new HTMSiteAdminException("Arquivo enviado não é do tipo CSV.");

                StringBuilder sbErrosLeitura = new StringBuilder();

                using (MemoryStream _objMemoryStream = new MemoryStream(ArquivoEnviado.FileBytes))
                {
                    using (StreamReader _objReader = new StreamReader(_objMemoryStream, true))
                    {
                        bool isPrimeiraLinha = true;
                        string strLinha;
                        int nrLinha = 1;
                        while ((strLinha = _objReader.ReadLine()) != null)
                        {
                            if (isPrimeiraLinha)
                            {
                                if (!strLinha.Equals("ID_PRODUTO;NOME;NOME_EN"))
                                    throw new HTMSiteAdminException("O cabeçalho do arquivo é inválido. Por favor faça o download do arquivo modelo e siga o layout do mesmo.");

                                isPrimeiraLinha = false;
                            }
                            else
                            {
                                try
                                {
                                    if (RegistrosLidos == null)
                                        RegistrosLidos = new List<IMPORTACAO_PRODUTO>();

                                    string[] arrLinha = strLinha.Split(';');

                                    StringBuilder sbValidacoes = new StringBuilder();

                                    if (string.IsNullOrWhiteSpace(arrLinha[0]))
                                        sbValidacoes.AppendLine(string.Format("Erro ao ler a linha {0}. Motivo: o ID do produto é obrigatório.", nrLinha));

                                    if (sbValidacoes.Length > 0)
                                        throw new HTMSiteAdminException(sbValidacoes.ToString());

                                    RegistrosLidos.Add(new IMPORTACAO_PRODUTO()
                                    {
                                        ID_PRODUTO = int.Parse(arrLinha[0]),
                                        NOME = !string.IsNullOrWhiteSpace(arrLinha[1]) ? arrLinha[1] : string.Empty,
                                        NOME_EN = !string.IsNullOrWhiteSpace(arrLinha[2]) ? arrLinha[2] : string.Empty,
                                        //NOME_ES = !string.IsNullOrWhiteSpace(arrLinha[3]) ? arrLinha[3] : string.Empty,
                                        PRODUTO = true,
                                        INSUMO = false,
                                        REGISTRO_ORIGEM = 2,
                                        ID_SESSAO = Global.GetSessionToken(this.Page)
                                    });
                                }
                                catch (Exception ex)
                                {
                                    if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) sbErrosLeitura.AppendLine(ex.Message);
                                    else sbErrosLeitura.AppendLine(string.Format("Erro desconhecido ao ler a linha {0}. Motivo: {1}", nrLinha, ex.Message));
                                }
                            }

                            nrLinha++;
                        }
                    }
                }

                if (sbErrosLeitura.Length > 0)
                    ucShowException1.ShowExceptions(new HTMSiteAdminException(sbErrosLeitura.ToString()));

                gvView.DataSource = RegistrosLidos;
                gvView.DataBind();
            }
            catch (Exception ex)
            {
                RegistrosLidos = null;
                ucShowException1.ShowExceptions(ex);
            }
            finally { ArquivoEnviado = null; }
        }

        protected void btnEnviarDados_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                var _objBo = new ImportacaoProdutoBo();

                foreach (var item in RegistrosLidos)
                {
                    if (_objBo.Find(lbda => lbda.ID_PRODUTO == item.ID_PRODUTO && lbda.REGISTRO_ORIGEM == 2).Count() > 0)
                    {
                        var _objAtual = _objBo.Find(lbda => lbda.ID_PRODUTO == item.ID_PRODUTO && lbda.REGISTRO_ORIGEM == 2).First();

                        _objAtual.NOME = item.NOME;
                        _objAtual.NOME_EN = item.NOME_EN;
                        _objAtual.NOME_ES = item.NOME_ES;
                        _objAtual.PRODUTO = item.PRODUTO;
                        _objAtual.INSUMO = item.INSUMO;
                        _objAtual.ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO;

                        _objBo.Update(_objAtual);
                    }
                    else
                        _objBo.Add(item);
                }

                _objBo.SaveChanges();
                ucShowException1.Sucess();
                RegistrosLidos = null;
                gvView.DataSource = null;
                gvView.DataBind();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
            finally { ArquivoEnviado = null; }
        }
    }
}