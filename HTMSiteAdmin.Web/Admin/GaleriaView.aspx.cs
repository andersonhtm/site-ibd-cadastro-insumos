﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Galerias;
using System.Text;
using HTMSiteAdmin.Library.Exceptions;
using HTMSiteAdmin.Business.Sistema;
using System.Configuration;
using HTMSiteAdmin.Library;

namespace HTMSiteAdmin.Web.Admin
{
    public partial class GaleriaView : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this, ViewToken, "ViewAspect", value); }
        }
        private GALERIA ObjCarregado
        {
            get { return (GALERIA)Global.RestoreObject(this, ViewToken, "ObjCarregado"); }
            set { Global.SaveObject(this, ViewToken, "ObjCarregado", value); }
        }
        private UploadedFile ArquivoEnviado
        {
            get { return (UploadedFile)Global.RestoreObject(this, ViewToken, "ArquivoEnviado"); }
            set { Global.SaveObject(this, ViewToken, "ArquivoEnviado", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MapButtons();

            if (!IsPostBack && ObjCarregado == null)
                ViewAspect = enmUserInterfaceAspect.Start;

            if (!IsPostBack) UserInterfaceAspectChanged();
        }

        public void ValidateAcess() { throw new NotImplementedException(); }

        public void MapButtons()
        {
            ucButtons1.ButtonNew.CausesValidation = false;
            ucButtons1.ButtonNew.Click += new ImageClickEventHandler(ButtonNew_Click);

            ucButtons1.ButtonSave.ValidationGroup = "vgView";
            ucButtons1.ButtonSave.CausesValidation = true;
            ucButtons1.ButtonSave.Click += new ImageClickEventHandler(ButtonSave_Click);

            ucButtons1.ButtonSaveNew.ValidationGroup = "vgView";
            ucButtons1.ButtonSaveNew.CausesValidation = true;
            ucButtons1.ButtonSaveNew.Click += new ImageClickEventHandler(ButtonSaveNew_Click);

            ucButtons1.ButtonCancel.CausesValidation = false;
            ucButtons1.ButtonCancel.Click += new ImageClickEventHandler(ButtonCancel_Click);

            ucButtons1.ButtonRefresh.CausesValidation = false;
            ucButtons1.ButtonRefresh.Click += new ImageClickEventHandler(ButtonRefresh_Click);

            ucButtons1.ButtonDelete.CausesValidation = false;
            ucButtons1.ButtonDelete.Click += new ImageClickEventHandler(ButtonDelete_Click);
        }

        public void UserInterfaceAspectChanged()
        {
            StartPageControls();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    ucButtons1.ButtonNew.Visible = true;
                    ucButtons1.ButtonSave.Visible = false;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = false;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = false;

                    int currentPageIndex = gvView.PageIndex;
                    gvView.DataBind();
                    gvView.PageIndex = currentPageIndex;

                    pnlViewCollection.Visible = gvView.Rows.Count > 0;
                    break;
                case enmUserInterfaceAspect.New:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = true;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    RestartForm();
                    break;
                case enmUserInterfaceAspect.Update:
                    ucButtons1.ButtonNew.Visible = false;
                    ucButtons1.ButtonSave.Visible = true;
                    ucButtons1.ButtonRefresh.Visible = false;
                    ucButtons1.ButtonCancel.Visible = true;
                    ucButtons1.ButtonDelete.Visible = false;

                    pnlView.Visible = true;
                    pnlViewCollection.Visible = false;

                    ClearFields();

                    SetForm();
                    break;
                default:
                    break;
            }
        }

        public void RestartForm()
        {
            ClearMemory();
            ClearFields();

            CarregarImagensGaleria();
        }

        public void StartPageControls()
        {
            try
            {
                if (!IsPostBack)
                {
                    ddlId_galeria_tipo.DataBind();
                    ddlFiltroGaleriaTipo.DataBind();
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSave_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
                ucShowException1.Sucess();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ButtonSaveNew_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SaveView();
                ViewAspect = enmUserInterfaceAspect.New;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void SaveView()
        {
            StringBuilder sbValidacoes = new StringBuilder();

            //if (ViewAspect == enmUserInterfaceAspect.New && ArquivoEnviado == null)
            //    sbValidacoes.AppendLine("Necessário informar um arquivo para criar um novo banner.");

            if (!string.IsNullOrWhiteSpace(txtData_publicacao_inicio.Text)
                && !string.IsNullOrWhiteSpace(txtData_publicacao_fim.Text)
                && DateTime.Compare(Convert.ToDateTime(txtData_publicacao_inicio.Text).Date, Convert.ToDateTime(txtData_publicacao_fim.Text).Date) >= 0)
                sbValidacoes.AppendLine("A data de fim da publicação deve ser maior que a data de início.");

            if (sbValidacoes.Length > 0)
                throw new HTMSiteAdminException(sbValidacoes.ToString());

            GaleriaBo _objBo = new GaleriaBo();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    break;
                case enmUserInterfaceAspect.New:
                    ObjCarregado = new GALERIA()
                    {
                        ID_GALERIA_TIPO = int.Parse(ddlId_galeria_tipo.SelectedValue),
                        ID_GALERIA_SITUACAO = int.Parse(ddlSituacao.SelectedValue),
                        DESCRICAO = !string.IsNullOrWhiteSpace(txtDescricao.Text) ? txtDescricao.Text : string.Empty,
                        DESCRICAO_DETALHADA = !string.IsNullOrWhiteSpace(txtObservacao.Text) ? txtObservacao.Text : string.Empty,
                        SUBDESCRICAO = !string.IsNullOrWhiteSpace(txtSubDescricao.Text) ? txtSubDescricao.Text : string.Empty,
                        SUBDESCRICAO_EN = !string.IsNullOrWhiteSpace(txtSubDescricao_en.Text) ? txtSubDescricao_en.Text : string.Empty,
                        ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO
                    };

                    FillDates();

                    _objBo.Add(ObjCarregado);
                    break;
                case enmUserInterfaceAspect.Update:
                    ObjCarregado = _objBo.Find(lbda => lbda.ID_GALERIA == ObjCarregado.ID_GALERIA).First();

                    ObjCarregado.ID_GALERIA_TIPO = int.Parse(ddlId_galeria_tipo.SelectedValue);
                    ObjCarregado.ID_GALERIA_SITUACAO = int.Parse(ddlSituacao.SelectedValue);
                    ObjCarregado.DESCRICAO = !string.IsNullOrWhiteSpace(txtDescricao.Text) ? txtDescricao.Text : string.Empty;
                    ObjCarregado.DESCRICAO_DETALHADA = !string.IsNullOrWhiteSpace(txtObservacao.Text) ? txtObservacao.Text : string.Empty;
                    ObjCarregado.SUBDESCRICAO = !string.IsNullOrWhiteSpace(txtSubDescricao.Text) ? txtSubDescricao.Text : string.Empty;
                    ObjCarregado.SUBDESCRICAO_EN = !string.IsNullOrWhiteSpace(txtSubDescricao_en.Text) ? txtSubDescricao_en.Text : string.Empty;
                    ObjCarregado.ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO;

                    FillDates();

                    _objBo.Update(ObjCarregado);
                    break;
                default:
                    break;
            }

            _objBo.SaveChanges();

            _objBo = null; ObjCarregado = null;
        }

        private void FillDates()
        {
            if (!string.IsNullOrWhiteSpace(txtData_publicacao_inicio.Text))
                ObjCarregado.DATA_PUBLICACAO_INICIO = DateTime.Parse(txtData_publicacao_inicio.Text);

            if (!string.IsNullOrWhiteSpace(txtData_publicacao_fim.Text))
                ObjCarregado.DATA_PUBLICACAO_FIM = DateTime.Parse(txtData_publicacao_fim.Text);
        }

        public void ButtonRefresh_Click(object sender, ImageClickEventArgs e)
        {
            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    //Nada fazer
                    break;
                case enmUserInterfaceAspect.New:
                    ClearFields();
                    break;
                case enmUserInterfaceAspect.Update:
                    ClearFields();
                    //TODO: Carregar dados do usuário
                    break;
                default:
                    break;
            }
        }

        public void ButtonCancel_Click(object sender, ImageClickEventArgs e)
        {
            ClearMemory();
            ViewAspect = enmUserInterfaceAspect.Start;
            UserInterfaceAspectChanged();
        }

        public void ButtonDelete_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void GetForm() { throw new NotImplementedException(); }

        public void SetForm()
        {
            try
            {
                ddlId_galeria_tipo.DataBind();
                ddlId_galeria_tipo.SelectedValue = ObjCarregado.ID_GALERIA_TIPO.ToString();
                txtDescricao.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO) ? ObjCarregado.DESCRICAO : string.Empty;
                txtDescricao_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO_EN) ? ObjCarregado.DESCRICAO_EN : string.Empty;
                txtObservacao.Text = !string.IsNullOrWhiteSpace(ObjCarregado.DESCRICAO_DETALHADA) ? ObjCarregado.DESCRICAO_DETALHADA : string.Empty;
                txtData_publicacao_inicio.Text = ObjCarregado.DATA_PUBLICACAO_INICIO.HasValue ? ObjCarregado.DATA_PUBLICACAO_INICIO.Value.ToShortDateString() : string.Empty;
                txtData_publicacao_fim.Text = ObjCarregado.DATA_PUBLICACAO_FIM.HasValue ? ObjCarregado.DATA_PUBLICACAO_FIM.Value.ToShortDateString() : string.Empty;
                txtSubDescricao.Text = !string.IsNullOrWhiteSpace(ObjCarregado.SUBDESCRICAO) ? ObjCarregado.SUBDESCRICAO : string.Empty;
                txtSubDescricao_en.Text = !string.IsNullOrWhiteSpace(ObjCarregado.SUBDESCRICAO_EN) ? ObjCarregado.SUBDESCRICAO_EN : string.Empty;

                ddlSituacao.SelectedValue = ObjCarregado.ID_GALERIA_SITUACAO.ToString();

                CarregarImagensGaleria();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void EnableFields() { throw new NotImplementedException(); }

        public void DisableFields() { throw new NotImplementedException(); }

        public void ClearFields()
        {
            try
            {
                txtObservacao.Text = txtDescricao.Text = txtDescricao_en.Text = txtSubDescricao.Text = txtSubDescricao_en.Text = string.Empty;
                ddlId_galeria_tipo.DataBind();
                ddlId_galeria_tipo.SelectedIndex = 0;
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        public void ClearMemory() { ObjCarregado = null; }

        protected void btnFiltrar_Click(object sender, ImageClickEventArgs e)
        {
            try { gvView.DataBind(); }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void gvView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((Literal)e.Row.FindControl("ltrTipo")).Text = new GaleriaTipoBo().Find(t => t.ID_GALERIA_TIPO == ((GALERIA)e.Row.DataItem).ID_GALERIA_TIPO).First().DESCRICAO;
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _SelectedKey = Convert.ToInt32(gvView.SelectedDataKey.Values["ID_GALERIA"]);

                ObjCarregado = new GaleriaBo().Find(lbda => lbda.ID_GALERIA == _SelectedKey).First();

                ViewAspect = enmUserInterfaceAspect.Update;
                UserInterfaceAspectChanged();
            }
            catch (Exception ex) { throw; }
        }

        protected void gvView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int currentPageIndex, _SelectedKey;

                switch (e.CommandName)
                {
                    case "Excluir":
                        _SelectedKey = Convert.ToInt32(e.CommandArgument);

                        GaleriaBo _objBo = new GaleriaBo();
                        _objBo.Delete(_objBo.Find(lbda => lbda.ID_GALERIA == _SelectedKey).First());
                        _objBo.SaveChanges();
                        _objBo = null;

                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex >= 0 && currentPageIndex <= gvView.PageCount - 1)
                            gvView.PageIndex = currentPageIndex;
                        break;
                    case "FirstPage":
                        gvView.DataBind();
                        gvView.PageIndex = 0;
                        break;
                    case "BackPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex > 0)
                            gvView.PageIndex = currentPageIndex - 1;
                        break;
                    case "GoToPage":
                        currentPageIndex = gvView.PageIndex;

                        int goToTop = 1, goToBottom = 1, goTo = 1;
                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text))
                            goToTop = int.Parse(((TextBox)gvView.TopPagerRow.FindControl("txtGvPage")).Text);

                        if (!string.IsNullOrWhiteSpace(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text))
                            goToBottom = int.Parse(((TextBox)gvView.BottomPagerRow.FindControl("txtGvPage")).Text);

                        gvView.DataBind();

                        goTo = (goToTop != currentPageIndex + 1) ? goToTop : goToBottom;

                        if (goTo >= 1 && goTo <= gvView.PageCount)
                            gvView.PageIndex = goTo - 1;
                        break;
                    case "NextPage":
                        currentPageIndex = gvView.PageIndex;

                        gvView.DataBind();

                        if (currentPageIndex < gvView.PageCount)
                            gvView.PageIndex = currentPageIndex + 1;
                        break;
                    case "LastPage":
                        gvView.DataBind();
                        gvView.PageIndex = gvView.PageCount - 1;
                        break;
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void edsView_QueryCreated(object sender, QueryCreatedEventArgs e)
        {
            try
            {
                var result = e.Query.Cast<GALERIA>();

                int _idGaleriaTipo = int.Parse(ddlFiltroGaleriaTipo.SelectedValue);

                if (!string.IsNullOrWhiteSpace(txtFiltroDescricao.Text))
                {
                    if (_idGaleriaTipo <= 0)
                    {
                        e.Query = from item in result
                                  where
                                    item.DESCRICAO_DETALHADA.Contains(txtFiltroDescricao.Text) || item.DESCRICAO.Contains(txtFiltroDescricao.Text)
                                  select item;
                    }
                    else
                    {
                        e.Query = from item in result
                                  where
                                    item.DESCRICAO_DETALHADA.Contains(txtFiltroDescricao.Text) || item.DESCRICAO.Contains(txtFiltroDescricao.Text)
                                    && item.ID_GALERIA_TIPO == _idGaleriaTipo
                                  select item;
                    }
                }
                else
                {
                    if (_idGaleriaTipo > 0)
                    {
                        e.Query = from item in result
                                  where
                                    item.ID_GALERIA_TIPO == _idGaleriaTipo
                                  select item;
                    }
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void afuArquivoImagem_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try { ArquivoEnviado = HTMSiteAdmin.Library.UserInterface.WebControlsUtility.AsyncFileUpload_UploadedComplete(sender, e); }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        private void CarregarImagensGaleria()
        {
            try
            {
                if (ObjCarregado != null)
                    gvImagensGaleria.DataSource = ObjCarregado.GALERIA_ARQUIVO.OrderBy(lbda => lbda.PRIORIDADE);
                else
                    gvImagensGaleria.DataSource = null;

                gvImagensGaleria.DataBind();
            }
            catch (Exception ex)
            {
                if (ex is HTMSiteAdminException || ex is HTMSiteAdminErrorException) throw;
                else throw new HTMSiteAdminErrorException("Erro desconhecido ao carregar imagens da galeria.");
            }
        }

        protected void gvImagensGaleria_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((Image)e.Row.FindControl("imgItemGaleria")).ImageUrl = string.Format("~/ShowFile.aspx?action=1&fileid={0}&width={1}&height={2}", ((GALERIA_ARQUIVO)e.Row.DataItem).ID_ARQUIVO_DIGITAL, 200, 150);
                    ((TextBox)e.Row.FindControl("txtGvDetalhes")).Text = !string.IsNullOrWhiteSpace(((GALERIA_ARQUIVO)e.Row.DataItem).DESCRICAO) ? ((GALERIA_ARQUIVO)e.Row.DataItem).DESCRICAO : string.Empty;
                    ((TextBox)e.Row.FindControl("txtGvDetalhes_en")).Text = !string.IsNullOrWhiteSpace(((GALERIA_ARQUIVO)e.Row.DataItem).DESCRICAO_EN) ? ((GALERIA_ARQUIVO)e.Row.DataItem).DESCRICAO_EN : string.Empty;
                    ((TextBox)e.Row.FindControl("txtGvOrdem")).Text = ((GALERIA_ARQUIVO)e.Row.DataItem).PRIORIDADE.ToString();
                    ((CheckBox)e.Row.FindControl("chkImagemPrincipal")).Checked = ((GALERIA_ARQUIVO)e.Row.DataItem).ID_GALERIA_ARQUIVO_TIPO == 2;
                }
            }
            catch (Exception ex) { throw; }
        }

        protected void btnAtualizarImagensGaleria_Click(object sender, ImageClickEventArgs e)
        {
            string path = Server.MapPath(ConfigurationManager.AppSettings["RELATIVEPATH_ARQUIVODIGITAL"]);

            try
            {
                if (this.IsValid)
                {
                    if (ArquivoEnviado == null)
                        throw new HTMSiteAdminException("Nenhuma imagem foi carregada.");

                    GaleriaBo _objBo = new GaleriaBo();

                    if (ViewAspect == enmUserInterfaceAspect.New)
                    {
                        SaveView();

                        ObjCarregado = _objBo.Find(lbda => lbda.ID_SESSAO == ((SESSAO) Session["SessaoAberta"]).ID_SESSAO).OrderByDescending(lbda => lbda.ID_GALERIA).First();

                        ViewAspect = enmUserInterfaceAspect.Update;
                        UserInterfaceAspectChanged();
                    }

                    ObjCarregado = _objBo.Find(lbda => lbda.ID_GALERIA == ObjCarregado.ID_GALERIA).First();
                    Guid id = Guid.NewGuid();

                    ObjCarregado.GALERIA_ARQUIVO.Add(new GALERIA_ARQUIVO()
                    {
                        DESCRICAO = "",
                        PRIORIDADE = 0,
                        ID_GALERIA_ARQUIVO_TIPO = 1,
                        ID_SESSAO = ((SESSAO) Session["SessaoAberta"]).ID_SESSAO,
                        ARQUIVO_DIGITAL = new ARQUIVO_DIGITAL()
                        {
                            ID_ARQUIVO = id,
                            NOME = ArquivoEnviado.FileName,
                            EXTENSAO = ArquivoEnviado.FileExtension,
                            DATA = DateTime.Now,
                            //ARQUIVO = ArquivoEnviado.FileBytes
                        }
                    });

                    _objBo.Update(ObjCarregado);
                    _objBo.SaveChanges();

                    Util.SaveFile(path, id.ToString(), ArquivoEnviado.FileExtension, ArquivoEnviado.FileBytes);

                    _objBo = null;
                    ArquivoEnviado = null;

                    CarregarImagensGaleria();

                    ucShowException2.Sucess();
                }
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void gvImagensGaleria_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int _rowIndex = int.Parse(e.CommandArgument.ToString());
                GaleriaBo _objBo = new GaleriaBo();
                Guid _idArquivoDigital;
                switch (e.CommandName)
                {
                    case "SaveImage":
                        StringBuilder sbValidacoesImagem = new StringBuilder();

                        if (string.IsNullOrWhiteSpace(((TextBox)gvImagensGaleria.Rows[_rowIndex].FindControl("txtGvOrdem")).Text))
                            sbValidacoesImagem.AppendLine("Necessário informar a ordem.");

                        if (sbValidacoesImagem.Length > 0)
                            throw new HTMSiteAdminException(sbValidacoesImagem.ToString());

                        _idArquivoDigital = new Guid(gvImagensGaleria.DataKeys[_rowIndex].Values["ID_ARQUIVO_DIGITAL"].ToString());

                        ObjCarregado = _objBo.Find(lbda => lbda.ID_GALERIA == ObjCarregado.ID_GALERIA).First();

                        ObjCarregado.GALERIA_ARQUIVO.FirstOrDefault(
                            lbda => lbda.ID_ARQUIVO_DIGITAL == _idArquivoDigital).DESCRICAO =
                                !string.IsNullOrWhiteSpace(((TextBox)gvImagensGaleria.Rows[_rowIndex].FindControl("txtGvDetalhes")).Text) ?
                                    ((TextBox)gvImagensGaleria.Rows[_rowIndex].FindControl("txtGvDetalhes")).Text : string.Empty;

                        ObjCarregado.GALERIA_ARQUIVO.FirstOrDefault(
                            lbda => lbda.ID_ARQUIVO_DIGITAL == _idArquivoDigital).DESCRICAO_EN =
                                !string.IsNullOrWhiteSpace(((TextBox)gvImagensGaleria.Rows[_rowIndex].FindControl("txtGvDetalhes_en")).Text) ?
                                    ((TextBox)gvImagensGaleria.Rows[_rowIndex].FindControl("txtGvDetalhes_en")).Text : string.Empty;


                        ObjCarregado.GALERIA_ARQUIVO.FirstOrDefault(
                            lbda => lbda.ID_ARQUIVO_DIGITAL == _idArquivoDigital).PRIORIDADE =
                                int.Parse(((TextBox)gvImagensGaleria.Rows[_rowIndex].FindControl("txtGvOrdem")).Text);

                        ObjCarregado.GALERIA_ARQUIVO.FirstOrDefault(lbda => lbda.ID_ARQUIVO_DIGITAL == _idArquivoDigital).ID_GALERIA_ARQUIVO_TIPO =
                            ((CheckBox)gvImagensGaleria.Rows[_rowIndex].FindControl("chkImagemPrincipal")).Checked ? 2 : 1;

                        if (ObjCarregado.GALERIA_ARQUIVO.FirstOrDefault(lbda => lbda.ID_ARQUIVO_DIGITAL == _idArquivoDigital).ID_GALERIA_ARQUIVO_TIPO == 2)
                        {
                            ObjCarregado.GALERIA_ARQUIVO.ToList().ForEach(delegate(GALERIA_ARQUIVO _objItem)
                            {
                                if (_objItem.ID_GALERIA_ARQUIVO_TIPO == 2 && _objItem.ID_ARQUIVO_DIGITAL != _idArquivoDigital)
                                    _objItem.ID_GALERIA_ARQUIVO_TIPO = 1;
                            });
                        }

                        _objBo.Update(ObjCarregado);
                        _objBo.SaveChanges();
                        break;
                    case "Excluir":
                        _idArquivoDigital = new Guid(gvImagensGaleria.DataKeys[_rowIndex].Values["ID_ARQUIVO_DIGITAL"].ToString());

                        ObjCarregado = _objBo.Find(lbda => lbda.ID_GALERIA == ObjCarregado.ID_GALERIA).First();

                        GALERIA_ARQUIVO _objGaleriaArquivo = ObjCarregado.GALERIA_ARQUIVO.First(s => s.ID_ARQUIVO_DIGITAL == _idArquivoDigital);

                        ObjCarregado.GALERIA_ARQUIVO.Remove(_objGaleriaArquivo);

                        _objBo.Update(ObjCarregado);
                        _objBo.SaveChanges();
                        break;
                }

                _objBo = null;

                CarregarImagensGaleria();

                ucShowException2.Sucess();
            }
            catch (Exception ex) { ucShowException1.ShowExceptions(ex); }
        }

        protected void ddlFiltroGaleriaTipo_DataBound(object sender, EventArgs e)
        {
            HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender);
        }

        protected void ddlId_galeria_tipo_DataBound(object sender, EventArgs e)
        {
            HTMSiteAdmin.Library.UserInterface.WebControlsUtility.DropDownList_DataBound(sender);
        }
    }
}