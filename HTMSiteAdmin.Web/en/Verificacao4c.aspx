﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="Verificacao4c.aspx.cs" Inherits="HTMSiteAdmin.Web.en.Verificacao4c" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Socio-Environmental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px;
                    margin-right: 6px; margin-left: 6px;" />
                4C</h2>
            <a href="/">return to home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
       
            <img src="media/img/logoCertificado4C.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                About the seal</h2>
          
        4C Compliant Coffee is coffee that is produced in accordance with the 4C Code of Conduct, a set of baseline sustainable practices and principles for the production of green coffee beans. The 4C Code of Conduct compliance can be demonstrated through the 4C Verification System and the issued 4C Licenses. The responsibility to assure compliance against the 4C Code of Conduct through operating the 4C Verification System belongs to Coffee Assurance Services GmbH & Co. KG (CAS).
        <br /><br />
The 4C Code of Conduct applies to any type of producing entity (4C Units) based in any coffee producing country that wish to produce and sell coffee as 4C Compliant. The term 4C Unit is inclusive and covers any type of production facility and/or process: it can be a group of small-scale farmers, a cooperative or a farmers’ association, a collecting station, a mill, a local trader, an export organisation, or even a roaster (as long as it is based in a country where coffee is produced). There are two prerequisites to qualify as a 4C Unit:
        <br /><br />
- Be able to supply a minimum of one container of green coffee (20 tons); <br />
- Have a person or a group of people who can ensure the implementation of the 4C Code of Conduct. The 4C system calls this a Managing Entity.<br />
        <br />
Verification of adherence to the principles of the 4C Code of Conduct must be carried out by an auditing company, which is approved by the Coffee Assurance Services. A list of approved 4C verifiers can be found on the Coffee Assurance Services web page (<a href="http://www.cas-veri.com" target="_blank">www.cas-veri.com</a>). The entry-level approach of the 4C Code of Conduct enables coffee producers around the globe to embark on their sustainability journey. The inclusive nature of the 4C Code of Conduct aims to reach out to producers who are currently not participating in the market for sustainable coffee and bring them into compliance with a basic level of sustainability.  The intention is to gradually raise the social, economic and environmental conditions of coffee production and processing worldwide. <br /><br />
In order to achieve this, the 4C Code of Conduct comprises:<br /><br />
- 10 Unacceptable Practices which have to be excluded before applying for 4C Verification. <br />
- 27 principles across economic, social, and environmental dimensions. These principles are based on good agricultural and management practices as well as international conventions and recognized guidelines accepted in the coffee sector and; <br />
<br />
Regarding the 3 dimensions of sustainability, the 4C Code of Conduct encompasses the following principles: <br /><br />

<b>8 Economic principles, thereof:</b>
        <br /><br />
- 3 principles on coffee farming as a business (1.1 – 1.3);<br />
- 5 principles on the support to farmers by the Managing Entity of a 4C Unit (1.4 – 1.8). <br /><br />

<b>9 Social Principles, thereof:</b><br /><br />
- 2 principles applicable to all farmers and other business partners (2.1 - 2.2);<br />
- 7 principles applicable to workers (2.3 - 2.9).<br /><br />

<b>10 environmental principles comprising:</b> <br /><br />
- 6 principles applicable to natural resources (3.1, 3.4, 3.6 - 3.9);<br />
- 3 principles applicable to agrochemicals (3.2, 3.3, 3.5);<br />
- 1 principle applicable to energy (3.10).<br />


            <h2 class="tituloCertificaoInterno">
                Informations</h2>            
                Coffee Assurance Services has also compiled a comprehensive set of frequently asked questions (FAQs) which cover most aspects of the 4C Verification system. These are available on the CAS website (<a href="http://www.cas-veri.com" target="_blank">www.cas-veri.com</a>). For further details or clarification, you can contact Coffee Assurance Services at <a href="mailto:info@cas-veri.com">info@cas-veri.com</a>

    </div>
    <!--quemsomos-->
</asp:Content>
