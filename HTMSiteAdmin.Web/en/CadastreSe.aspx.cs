﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Text;
using System.Configuration;
using System.Net;
using HTMSiteAdmin.Web.Classes;

namespace HTMSiteAdmin.Web.en
{
    public partial class CadastreSe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                #region [ E-Mail para a IBD ]

                StringBuilder CorpoEmail = new StringBuilder(); // Corpo da Mensagem, conteudo da variavel criada acima

                CorpoEmail.AppendLine("Atendimento: CADASTRE-SE<br />");
                CorpoEmail.AppendLine("<br />");
                CorpoEmail.AppendLine("Data Hora: " + DateTime.Now.ToString() + "<br />");
                CorpoEmail.AppendLine("<br />");
                CorpoEmail.AppendLine("Nome: " + txtNome.Text + "<br />");
                CorpoEmail.AppendLine("E-mail: " + txtEmail.Text + "<br />");
                CorpoEmail.AppendLine("Telefone: " + txtCelular.Text + "<br />");
                CorpoEmail.AppendLine("Município: " + txtMunicipio.Text + "<br />");
                CorpoEmail.AppendLine("Estado: " + txtEstado.Text + "<br />");
                CorpoEmail.AppendLine("País: " + txtPais.Text + "<br />");

                HTMSiteAdmin.Web.Classes.Util.EnviarEmail(CorpoEmail.ToString(),
                    string.Format("[Cadastre-se] {0}", txtNome.Text),
                    Configuracao.REGISTER_EMAILTO, "", Configuracao.MAIL_BLINDCOPYTO);

                #endregion

                #region [ E-Mail para o usuário ]

                StringBuilder CorpoEmailUsuario = new StringBuilder(); // Corpo da Mensagem, conteudo da variavel criada acima

                CorpoEmailUsuario.AppendLine("Dear: " + txtNome.Text + "<br />");
                CorpoEmailUsuario.AppendLine("<br />");
                CorpoEmailUsuario.AppendLine("You have been registered on IBD website to receive information about us.<br />");
                CorpoEmailUsuario.AppendLine("<br />");
                CorpoEmailUsuario.AppendLine("Date: " + DateTime.Now.ToString() + "<br />");
                CorpoEmailUsuario.AppendLine("E-mail: " + txtEmail.Text + "<br />");
                CorpoEmailUsuario.AppendLine("<br />");
                CorpoEmailUsuario.AppendLine("Regards,<br />");
                CorpoEmailUsuario.AppendLine("<br />");
                CorpoEmailUsuario.AppendLine("IBD Team<br />");
                CorpoEmailUsuario.AppendLine(Configuracao.REGISTER_EMAILREPLY + "<br />");

                HTMSiteAdmin.Web.Classes.Util.EnviarEmail(CorpoEmailUsuario.ToString(),
                string.Format("[IBD Register] {0}", txtNome.Text),
                txtEmail.Text, "", Configuracao.MAIL_BLINDCOPYTO);

                #endregion

                Response.Redirect("CadastreSe.aspx");
            }
            catch (Exception ex) { throw ex; }
        }
    }
}