﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="Sai.aspx.cs" Inherits="HTMSiteAdmin.Web.en.Sai" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }

        .itemNormas {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }

        .limpaFloat {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Socio-Ambiental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px; margin-left: 6px;" />
                SAI PLATFORM</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />

        <img src="media/img/LOGO_FSA_SAI.png" alt="SAI PLATFORM" style="float: right; margin-left: 15px; margin-bottom: 10px;" />
        <h2 class="tituloCertificaoInterno">About the Seal</h2>
        <b>Farm Sustainability Assessment</b><br />

        <p>
            A simple, highly effective tool to assess, improve and communicate on-farm sustainability. 
            Used by leading food & drink companies to source sustainably produced agricultural raw materials. Used by farmers to assess their farms' sustainability.
        </p>
        <p>
            Farmers can use the FSA to assess and improve sustainable agriculture practices and communicate to customers in a consistent format. 
            Companies can use the FSA as a single tool to meet sustainable sourcing goals across many commodities.
        </p><br />

        <p>
            The easy scoring mechanism provides farmers and their customers with a visualized overview of their farms' sustainability. 
            Companies assess and compare the sustainable practices of multiple suppliers and farmers using a single tool and also can be used to benchmark existing standards and schemes.
        </p>
        <br />

        <p>
            Provides a single benchmark for comparing existing codes, schemes and legislation and supports aggregating farming data across countries, commodities, 
            and suppliers – serving as a basis for continuous improvement. Farmers can improve performance, save time and resources, reduce costs, gain market access by using the FSA tool.
        </p>
        <br />

        <b>Sustainable Agriculture Initiative Platform</b><br />

        <p>
            The Sustainable Agriculture Initiative Platform (SAI Platform) is the primary global food & drink value chain initiative for sustainable agriculture. 
            Food companies and retailers are the largest purchasers of agricultural raw materials. To ensure a constant, increasing and safe supply of 
            agricultural raw materials, these must be grown in a sustainable manner.
        </p><br />

        <p>
            SAI Platform is a non-profit organization to facilitate sharing, at precompetitive level, of knowledge and best practices to support the development and implementation of sustainable agriculture practices involving stakeholders throughout the food value chain. 
            Today, counts over 90 members, which actively share the same view on sustainable agriculture.
        </p><br />

        <p>
            SAI Platform develops tools and guidance to support global and local sustainable sourcing and agriculture practices. Examples of recently developed resources include: 
            Sector 'Principles and Practices' documents; Practitioner’s Guide for Sustainable Sourcing; recommendations for Sustainability Performance Assessment (SPA); 
            and the Farm Sustainability Assessment (FSA). 
        </p>
        <p>
            The overall aim of SAI Platform is to support the development of sustainable agriculture worldwide. 
        </p>

        <h2 class="tituloCertificaoInterno">DiretrizesStandards Served</h2>
        - Standards from the program FSA - SAI PLATFORM <a href="http://www.saiplatform.org">www.saiplatform.org</a>  / <a href="http://www.fsatool.com">www.fsatool.com</a>
        <br />
        <br />

        <h2 class="tituloCertificaoInterno">Segments Served</h2>
        - Agriculture<br />
        - Food and Beverage Industry<br />

    </div>
    <!--quemsomos-->
</asp:Content>
