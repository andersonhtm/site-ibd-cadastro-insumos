﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="EventosFeiras.aspx.cs" Inherits="HTMSiteAdmin.Web.en.EventosFeiras" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Events and Fairs</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="eventos">
        <div>
            <p>
                In this section: remain updated on Events and Fairs that IBD will attend or promote:</p>
        </div>
        <asp:DataList ID="dtEventos" runat="server" OnItemDataBound="dtEventos_ItemDataBound"
            RepeatLayout="Flow" ShowFooter="False" ShowHeader="False" EnableTheming="False"
            Width="100%">
            <ItemTemplate>
                <asp:Literal ID="ltrMesAno" Text="ltrMesAno" runat="server" />
                <asp:DataList ID="dtListaEventos" runat="server" RepeatLayout="Flow" ShowFooter="False"
                    ShowHeader="False">
                    <ItemTemplate>
                        <div class="itemEvento">
                            <h3>
                                &nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal ID="ltrLinkNome" Text="ltrLinkNome" runat="server" /></h3>
                            <div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Period:
                                <asp:Literal ID="ltrDataEvento" Text="ltrDataEvento" runat="server" />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
            </ItemTemplate>
        </asp:DataList>
        <asp:Panel ID="litEventos" runat="server" Visible="false"><br /><p>No events in the coming months</p></asp:Panel>
        <!--itemEvento-->
    </div>
    <!--eventos-->
</asp:Content>
