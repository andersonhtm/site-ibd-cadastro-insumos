﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Conteudos;
using HTMSiteAdmin.Business.Eventos;
using HTMSiteAdmin.Business.Banners;

namespace HTMSiteAdmin.Web.en
{

    public partial class NewsletterInformativo : System.Web.UI.Page
    {
        private List<CONTEUDO> _objResult { get; set; }
        private int totalRegistros { get; set; }

        private struct strAnoMes
        {
            public int Dia;
            public int Ano;
            public int Mes;
            public DateTime Data;
        }

        IQueryable<EVENTO> _objEventosCollection;
        List<strAnoMes> _objDataSource;

        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime DataPeriodo = DateTime.Now.AddMonths(-3);
            var news = new ConteudoBo().Find(c => c.ID_CONTEUDO_SITUACAO == 1 && c.ID_CONTEUDO_TIPO == 1 && c.DATA_PUBLICACAO > DataPeriodo && !string.IsNullOrEmpty(c.TITULO_EN)).OrderByDescending(o => o.DATA_PUBLICACAO).Take(5);
            if (!news.Any())
                noticias.Visible = false;
            dtNoticias.DataSource = news;
            dtNoticias.DataBind();

            CarregaDadosEventos();
            CarregarBanner();
        }

        protected void dtNoticias_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer)
            {
                return;
            }
            Literal ltrDiaMesNews = (Literal)e.Item.FindControl("ltrDiaMesNews");
            HyperLink ltrLink = ((HyperLink)e.Item.FindControl("ltrLink"));
            Literal litSubTitulo = ((Literal)e.Item.FindControl("litSubTitulo"));

            ltrDiaMesNews.Text = ((CONTEUDO)e.Item.DataItem).DATA_PUBLICACAO.Value.ToString("dd/MMM");
            litSubTitulo.Visible = ((CONTEUDO)e.Item.DataItem).SUB_TITULO_EN.Length > 0 ? true : false;
            litSubTitulo.Text = string.Format("<h4 style=\"font-weight: normal; color: #625863; margin: 3px 0 0 10px;\">{0}</h4>", ((CONTEUDO)e.Item.DataItem).SUB_TITULO_EN);
            ltrLink.NavigateUrl = new System.Uri(Page.Request.Url, string.Format("NoticiasDetalhes.aspx?id_conteudo={0}", ((CONTEUDO)e.Item.DataItem).ID_CONTEUDO.ToString())).ToString();
            ltrLink.Text = ((CONTEUDO)e.Item.DataItem).TITULO_EN;
        }

        private void CarregaDadosEventos()
        {
            HashSet<int> lstAnosComEventos = new HashSet<int>();
            _objDataSource = new List<strAnoMes>();

            _objEventosCollection = new EventoBo().Find(c => c.ATIVO && (c.DATA_INICIO > DateTime.Today)).OrderByDescending(lbda => lbda.DATA_INICIO).Take(5);

            if (_objEventosCollection.Count() > 0)
            {

                int MaiorAno = _objEventosCollection.Max(lbda => lbda.DATA_FIM.Value.Year);
                lstAnosComEventos.Add(MaiorAno);

                int MenorAno = _objEventosCollection.Min(lbda => lbda.DATA_FIM.Value.Year);

                if (!MenorAno.Equals(MaiorAno))
                {
                    lstAnosComEventos.Add(MenorAno);

                    while (MenorAno < MaiorAno)
                    {
                        if (_objEventosCollection.Count(lbda => lbda.DATA_FIM.Value.Year.Equals(MenorAno)) > 0)
                            lstAnosComEventos.Add(MenorAno);

                        MenorAno++;
                    }
                }

                foreach (int Ano in lstAnosComEventos)
                {
                    int MaiorMes = _objEventosCollection.Max(lbda => lbda.DATA_FIM.Value.Month);
                    int MenorMes = _objEventosCollection.Min(lbda => lbda.DATA_FIM.Value.Month);
                    int Dia = _objEventosCollection.Min(lbda => lbda.DATA_INICIO.Value.Day);
                    DateTime Date = _objEventosCollection.Select(lbda => lbda.DATA_INICIO.Value).FirstOrDefault();

                    if (!MenorMes.Equals(MaiorMes))
                    {
                        while (MenorMes <= MaiorMes)
                        {
                            if (_objEventosCollection.Count(lbda => lbda.DATA_FIM.Value.Month.Equals(MenorMes)) > 0
                            && _objDataSource.Count(lbda => lbda.Ano.Equals(Ano) && lbda.Mes.Equals(MenorMes)) == 0)
                                _objDataSource.Add(new strAnoMes() { Ano = Ano, Mes = MenorMes, Dia = Dia, Data = Date });

                            MenorMes++;
                        }
                    }
                    else
                    {
                        if (_objEventosCollection.Count(lbda => lbda.DATA_FIM.Value.Month.Equals(MenorMes)) > 0
                            && _objDataSource.Count(lbda => lbda.Ano.Equals(Ano) && lbda.Mes.Equals(MenorMes)) == 0)
                            _objDataSource.Add(new strAnoMes() { Ano = Ano, Mes = MenorMes, Dia = Dia, Data = Date });
                    }
                }

                dtEventos.DataSource = _objDataSource;
                dtEventos.DataBind();
            }
            else
            {
                eventos.Visible = false;
            }
        }

        protected void dtEventos_ItemDataBound(object sender, DataListItemEventArgs e)
        {

            ((DataList)e.Item.FindControl("dtListaEventos")).ItemDataBound += new DataListItemEventHandler(dtListaEventos_ItemDataBound);
            ((DataList)e.Item.FindControl("dtListaEventos")).DataSource = new EventoBo().Find(lbda => lbda.ATIVO && lbda.DATA_FIM.Value.Year.Equals(((strAnoMes)e.Item.DataItem).Ano) && lbda.DATA_FIM.Value.Month.Equals(((strAnoMes)e.Item.DataItem).Mes)).OrderBy(lbda => lbda.DATA_INICIO).Take(5);
            ((DataList)e.Item.FindControl("dtListaEventos")).DataBind();
        }

        private void CarregarBanner()
        {

            var _objBanner1 = new BannerBo().Find(lbda => lbda.ID_BANNER_POSICAO == 7 && lbda.ID_BANNER_SITUACAO == 1 && DateTime.Now <= lbda.DATA_PUBLICACAO_FIM).OrderBy(c => c.DATA_PUBLICACAO_INICIO).FirstOrDefault();
            var _objBanner2 = new BannerBo().Find(lbda => lbda.ID_BANNER_POSICAO == 8 && lbda.ID_BANNER_SITUACAO == 1 && DateTime.Now <= lbda.DATA_PUBLICACAO_FIM).OrderBy(c => c.DATA_PUBLICACAO_INICIO).FirstOrDefault();
            var _objBannerTopo = new BannerBo().Find(lbda => lbda.ID_BANNER_POSICAO == 12 && lbda.ID_BANNER_SITUACAO == 1 && DateTime.Now <= lbda.DATA_PUBLICACAO_FIM).OrderBy(c => c.DATA_PUBLICACAO_INICIO).FirstOrDefault();

            //IMAGENS MAIORES
            if (_objBanner1 != null)
                ltrLinkImagemBanner1.Text = "<a style=\"border: none\" href=\"" + new System.Uri(Page.Request.Url, _objBanner1.CAMINHO_EN) + "\"><img style=\"border:none\" src=\"" + new System.Uri(Page.Request.Url, ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", _objBanner1.ID_ARQUIVO_EN))) + "\" alt=\"" + _objBanner1.TITULO_EN + "\" width=\"278\" height=\"129\" /></a>";
            if (_objBanner2 != null)
                ltrLinkImagemBanner2.Text = "<a style=\"border: none\" href=\"" + new System.Uri(Page.Request.Url, _objBanner2.CAMINHO_EN) + "\"><img style=\"border:none\" src=\"" + new System.Uri(Page.Request.Url, ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", _objBanner2.ID_ARQUIVO_EN))) + "\" alt=\"" + _objBanner2.TITULO_EN + "\" width=\"278\" height=\"129\" /></a>";
            if (_objBannerTopo != null)
                ltrLinkImagemBannerTopo.Text = "<a style=\"border: none\" href=\"" + new System.Uri(Page.Request.Url, _objBannerTopo.CAMINHO_EN) + "\"><img style=\"border:none\" src=\"" + new System.Uri(Page.Request.Url, ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", _objBannerTopo.ID_ARQUIVO_EN))) + "\" alt=\"" + _objBannerTopo.TITULO_EN + "\" width=\"594\" height=\"334\" /></a>";            
        }

        void dtListaEventos_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer || e.Item.ItemType == ListItemType.Header)
                return;

            EVENTO _objEvento = (EVENTO)e.Item.DataItem;
            HyperLink hlLinkNome = ((HyperLink)e.Item.FindControl("hlLinkNome"));

            hlLinkNome.NavigateUrl = new System.Uri(Page.Request.Url, string.Format("EventosFeirasDetalhes.aspx?id_conteudo={0}", _objEvento.ID_EVENTO)).ToString();
            hlLinkNome.Text = _objEvento.DESCRICAO_EN;

            ((Literal)e.Item.FindControl("ltrDiaMes")).Text = _objEvento.DATA_INICIO.Value.ToString("dd/MMM");

        }
    }
}