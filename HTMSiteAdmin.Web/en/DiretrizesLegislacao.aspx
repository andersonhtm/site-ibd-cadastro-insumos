﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="DiretrizesLegislacao.aspx.cs" Inherits="HTMSiteAdmin.Web.en.DiretrizesLegislacao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Standards and Legislation</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="diretrizes">
        <div>
            <p style="color: #595941; font-size: 15px;">
                Access the Standards and Legislation here by merely clinking on the item to show
                the file.</p>
        </div>

         <br />   

        <div style="clear: both">
             <asp:DataList ID="dtLinksInteressantes" runat="server" OnItemDataBound="dtLinksInteressantes_ItemDataBound"
                RepeatLayout="Flow" ShowFooter="False" ShowHeader="False">
                <ItemTemplate>                
                    <asp:DataList ID="dtLinks" runat="server" RepeatLayout="Flow" ShowFooter="False"
                        ShowHeader="False">
                        <ItemTemplate>
                            <div class="listaDiretrizesLegislacao">
                                <asp:Literal ID="ltrLink" Text="ltrLink" runat="server" />
                            </div>    
                        </ItemTemplate>
                    </asp:DataList>
                </ItemTemplate>
            </asp:DataList>
        </div>   
       
        <br />     

        <asp:DataList ID="dtDiretrizesCategorias" runat="server" OnItemDataBound="dtDiretrizesCategorias_ItemDataBound"
            RepeatLayout="Flow" ShowFooter="False" ShowHeader="False">
            <ItemTemplate>
                <h3>
                    <asp:Literal ID="ltrCategoria" Text="ltrCategoria" runat="server" /></h3>
                <div>
                    <asp:DataList ID="dtDownloads" runat="server" RepeatLayout="Flow" ShowFooter="False"
                        ShowHeader="False">
                        <ItemTemplate>
                            <div class="listaDiretrizesLegislacao">
                                <asp:Literal ID="ltrLink" Text="ltrLink" runat="server" />
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </ItemTemplate>
        </asp:DataList>
    </div>
    <!--diretrizes-->
</asp:Content>
