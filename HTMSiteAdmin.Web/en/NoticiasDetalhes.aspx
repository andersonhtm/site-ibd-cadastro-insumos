﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="NoticiasDetalhes.aspx.cs" Inherits="HTMSiteAdmin.Web.en.NoticiasDetalhes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                News</h2>
            <a href="/">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="noticias">
        <div class="itemNoticiasInterno">
            <div class="texto2 texto">
                <div class="dataTipo">
                    <span>
                        <asp:Literal ID="ltrMes" Text="Janeiro" runat="server" />
                        |<asp:Literal ID="ltrDia" Text="1" runat="server" />
                        
                        </span>
                    <h4>
                        <asp:Literal ID="ltrCategoria" Text="Notícia" runat="server" /></h4>
                    <p>
                        published
                        <asp:Literal ID="ltrDataPublicacao" Text="30/01/2012" runat="server" /></p>
                </div>
                <!--dataTipo-->
                <br />
                <h3><asp:Literal ID="ltrTitulo" Text="Título da notícia" runat="server" /></h3>
                <asp:Literal ID="ltrSubTitulo" Text="" runat="server" />
                <br />
                <asp:Literal ID="ltrConteudo" Text="Conteúdo" runat="server" />
                <br />
                <i style="font-size:11px;"><asp:Literal ID="ltrFonte" Text="" runat="server" /></i>
            </div>
            <!--texto-->
        </div>
        <!--itemNoticias-->
    </div>
    <!--noticias-->
</asp:Content>
