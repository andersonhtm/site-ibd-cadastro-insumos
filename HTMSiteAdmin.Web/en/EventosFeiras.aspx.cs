﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Eventos;

namespace HTMSiteAdmin.Web.en
{
    public partial class EventosFeiras : System.Web.UI.Page
    {
        private int? id_categoria
        {
            get
            {
                try { return Convert.ToInt32(Request.QueryString["id_categoria"]); }
                catch (Exception) { return null; }
            }
            set { }
        }

        private struct strAnoMes
        {
            public int Ano;
            public int Mes;
            public DateTime Data;
        }

        IQueryable<EVENTO> _objEventosCollection;
        List<strAnoMes> _objDataSource;

        protected void Page_Load(object sender, EventArgs e)
        {
            CarregaDadosEventos();
        }

        private void CarregaDadosEventos()
        {
            HashSet<int> lstAnosComEventos = new HashSet<int>();
            _objDataSource = new List<strAnoMes>();

            // se data final do evento for menor que data atual (evento acontecendo, ou já aconteceu)
            // exluir os eventos que começaram e terminaram.

            if (id_categoria.HasValue && id_categoria > 0)
                _objEventosCollection = new EventoBo().Find(c => c.ATIVO && c.ID_EVENTO_CATEGORIA == id_categoria && !string.IsNullOrEmpty(c.DESCRICAO_EN) && DateTime.Today >= c.DATA_PUBLICACAO).Where(c => (c.DATA_INICIO < DateTime.Today && c.DATA_FIM < DateTime.Today) == false).OrderByDescending(lbda => lbda.DATA_FIM);
            else
                _objEventosCollection = new EventoBo().Find(c => c.ATIVO && !string.IsNullOrEmpty(c.DESCRICAO_EN) && DateTime.Today >= c.DATA_PUBLICACAO).Where(c => (c.DATA_INICIO < DateTime.Today && c.DATA_FIM < DateTime.Today) == false).OrderByDescending(lbda => lbda.DATA_FIM);

            if (_objEventosCollection.Count() > 0)
            {

                int MaiorAno = _objEventosCollection.Max(lbda => lbda.DATA_FIM.Value.Year);
                lstAnosComEventos.Add(MaiorAno);

                int MenorAno = _objEventosCollection.Min(lbda => lbda.DATA_FIM.Value.Year);

                if (!MenorAno.Equals(MaiorAno))
                {
                    lstAnosComEventos.Add(MenorAno);

                    while (MenorAno < MaiorAno)
                    {
                        if (_objEventosCollection.Count(lbda => lbda.DATA_FIM.Value.Year.Equals(MenorAno)) > 0)
                            lstAnosComEventos.Add(MenorAno);

                        MenorAno++;
                    }
                }

                foreach (int Ano in lstAnosComEventos)
                {
                    int MaiorMes = _objEventosCollection.Max(lbda => lbda.DATA_FIM.Value.Month);
                    int MenorMes = _objEventosCollection.Min(lbda => lbda.DATA_FIM.Value.Month);

                    if (!MenorMes.Equals(MaiorMes))
                    {
                        while (MenorMes <= MaiorMes)
                        {
                            if (_objEventosCollection.Count(lbda => lbda.DATA_FIM.Value.Month.Equals(MenorMes)) > 0
                            && _objDataSource.Count(lbda => lbda.Ano.Equals(Ano) && lbda.Mes.Equals(MenorMes)) == 0)
                                _objDataSource.Add(new strAnoMes() { Ano = Ano, Mes = MenorMes, Data = new DateTime(Ano, MenorMes, 1) });

                            MenorMes++;
                        }
                    }
                    else
                    {
                        if (_objEventosCollection.Count(lbda => lbda.DATA_FIM.Value.Month.Equals(MenorMes)) > 0
                            && _objDataSource.Count(lbda => lbda.Ano.Equals(Ano) && lbda.Mes.Equals(MenorMes)) == 0)
                            _objDataSource.Add(new strAnoMes() { Ano = Ano, Mes = MenorMes, Data = new DateTime(Ano, MenorMes, 1) });
                    }
                }

                dtEventos.DataSource = _objDataSource;
                dtEventos.DataBind();
            }
            else
            {
                litEventos.Visible = true;
            }
        }

        protected void dtEventos_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            ((Literal)e.Item.FindControl("ltrMesAno")).Text = string.Format("<div class=\"mesAno\"><span></span>&nbsp;&nbsp;&nbsp;{0}</div>", ((strAnoMes)e.Item.DataItem).Data.ToString("MMM yyyy"));

            ((DataList)e.Item.FindControl("dtListaEventos")).ItemDataBound += new DataListItemEventHandler(dtListaEventos_ItemDataBound);
            ((DataList)e.Item.FindControl("dtListaEventos")).DataSource = new EventoBo().Find(lbda => lbda.ATIVO && !string.IsNullOrEmpty(lbda.DESCRICAO_EN) && lbda.DATA_FIM.Value.Year.Equals(((strAnoMes)e.Item.DataItem).Ano) && lbda.DATA_FIM.Value.Month.Equals(((strAnoMes)e.Item.DataItem).Mes)).Where(c => (c.DATA_INICIO < DateTime.Today && c.DATA_FIM < DateTime.Today) == false).OrderBy(lbda => lbda.DATA_INICIO);
            ((DataList)e.Item.FindControl("dtListaEventos")).DataBind();
        }

        void dtListaEventos_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            EVENTO _objEvento = (EVENTO)e.Item.DataItem;
            ((Literal)e.Item.FindControl("ltrLinkNome")).Text = string.Format("<a href=\"EventosFeirasDetalhes.aspx?id_conteudo={0}\">{1}</a>", _objEvento.ID_EVENTO, _objEvento.DESCRICAO_EN);

            if (_objEvento.DATA_INICIO.Value.Year.Equals(_objEvento.DATA_FIM.Value.Year)
                && _objEvento.DATA_INICIO.Value.Month.Equals(_objEvento.DATA_FIM.Value.Month)
                && _objEvento.DATA_INICIO.Value.Day.Equals(_objEvento.DATA_FIM.Value.Day))
            {
                ((Literal)e.Item.FindControl("ltrDataEvento")).Text =
                    string.Format("<span>{0} de {1} de {2}</span>",
                    _objEvento.DATA_INICIO.Value.Day.ToString("00"),
                    HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objEvento.DATA_FIM.Value.Month).ToLower(),
                    _objEvento.DATA_FIM.Value.Year);
            }
            else if (_objEvento.DATA_INICIO.Value.Year.Equals(_objEvento.DATA_FIM.Value.Year)
                && _objEvento.DATA_INICIO.Value.Month.Equals(_objEvento.DATA_FIM.Value.Month))
            {
                ((Literal)e.Item.FindControl("ltrDataEvento")).Text =
                    string.Format("<span> {0} to {1}</span>",
                    _objEvento.DATA_INICIO.Value.ToShortDateString(),
                    _objEvento.DATA_FIM.Value.ToShortDateString());
            }
            else if (_objEvento.DATA_INICIO.Value.Year.Equals(_objEvento.DATA_FIM.Value.Year)
                && !_objEvento.DATA_INICIO.Value.Month.Equals(_objEvento.DATA_FIM.Value.Month))
            {
                ((Literal)e.Item.FindControl("ltrDataEvento")).Text =
                    string.Format("<span> {0} to {1}</span>",
                    _objEvento.DATA_INICIO.Value.ToShortDateString(),
                    _objEvento.DATA_FIM.Value.ToShortDateString());
            }
            else
            {
                ((Literal)e.Item.FindControl("ltrDataEvento")).Text =
                    string.Format("<span> {0} to {1}</span>",
                    _objEvento.DATA_INICIO.Value.ToShortDateString(),
                    _objEvento.DATA_FIM.Value.ToShortDateString());
            }
        }
    }
}