﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="ProdutosClientesAprovados.aspx.cs" Inherits="HTMSiteAdmin.Web.en.ProdutosClientesAprovados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Clients</span><br />
                Clients and Certified Products
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Search</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="certificadosConsultar">
        <div class="texto">
            <p>
                Research companies and products certified by IBD. To facilitate your search, select
                the desired criteria:</p>
            <fieldset>
                <ul>
                    <li>
                        <label>
                            certificate</label>
                        <asp:DropDownList runat="server" ID="ddlCertificado" DataTextField="NOME_EN" DataValueField="ID_CERTIFICADO"
                            OnDataBound="ddlCertificado_DataBound" Width="250px" Style="float: left;" />
                    </li>
                    <li>
                        <label>
                            product</label>
                        <asp:TextBox runat="server" ID="txtClienteCertificadoProduto" Width="250px" Style="float: left;" />
                    </li>
                    <li>
                        <label>
                            client</label>
                        <asp:TextBox runat="server" ID="txtClienteCertificadoCliente" Width="250px" Style="float: left;" />
                    </li>
                    <li>
                        <label>
                            country</label>
                        <asp:DropDownList runat="server" ID="ddlClienteCertificadoPais" DataTextField="PAIS"
                            DataValueField="PAIS" OnSelectedIndexChanged="ddlClienteCertificadoPais_SelectedIndexChanged"
                            AutoPostBack="True" OnDataBound="ddlClienteCertificadoPais_DataBound" Width="250px"
                            Style="float: left;" />
                    </li>
                    <li>
                        <label>
                            state</label>
                            <span id="spanTextoEstado" runat="server" visible="true" 
                                style="font-weight:normal;color:#A2AC94;font-style:italic;">
                                [select a country]
                            </span>
                            <asp:DropDownList runat="server" ID="ddlClienteCertificadoUf" OnDataBound="ddlClienteCertificadoUf_DataBound"
                                DataTextField="ESTADO" DataValueField="ESTADO_SIGLA" Visible="false" style="float:left;width:250px;" />
                    </li>
                    <li></li>
                    <li>
                        <asp:LinkButton ID="btnClienteCertificadoBuscar" runat="server" CssClass="btBuscar"
                            OnClick="btnClienteCertificadoBuscar_Click">search</asp:LinkButton>
                    </li>
                </ul>
            </fieldset>
        </div>
        <!--texto-->
        <div class="imagem">
            <img src="media/img/produtosCertificados.jpg" class="border3pxfff" alt="" /></div>
        <!--imagem-->
    </div>
    <!--certificadosConsultar-->
</asp:Content>
