﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Business.Galerias;
using HTMSiteAdmin.Data;

namespace HTMSiteAdmin.Web.en
{
    public partial class QuemSomos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindImages();
        }

        void BindImages() {
            List<GALERIA_ARQUIVO> arquivos = new GaleriaBo().Find(x => x.ID_GALERIA_TIPO == 1).ToList()[0].GALERIA_ARQUIVO.OrderBy(x => x.PRIORIDADE).ToList();

            rptImages.DataSource = arquivos;
            rptImages.DataBind();
        }
    }
}