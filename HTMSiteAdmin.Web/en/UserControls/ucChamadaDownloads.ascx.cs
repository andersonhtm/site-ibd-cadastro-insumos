﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Downloads;

namespace HTMSiteAdmin.Web.en.UserControls
{
    public partial class ucChamadaDownloads : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var ctx = new HTMSiteAdminEntities())
            {
                List<DOWNLOAD> data = new DownloadBo().GetAll().Where(lbda => lbda.ID_DOWNLOAD_TIPO == 1 && lbda.ID_DOWNLOAD_CATEGORIA == 21 && lbda.ATIVO).OrderBy(x => new { x.ORDEM, x.DESCRICAO_EN }).ToList();

                if (data != null)
                {
                    var r = new Random();
                    dtDiretrizesLegislacao.DataSource = data.OrderBy(x => r.Next()).ToList().Take(2);
                    dtDiretrizesLegislacao.DataBind();
                }
            }
        }

        protected void dtDiretrizesLegislacao_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            ((Literal)e.Item.FindControl("ltrNomeArquivo")).Text = ((DOWNLOAD)e.Item.DataItem).DESCRICAO_EN;
            ((Literal)e.Item.FindControl("ltrLinkArquivo")).Text = "<a style=\"color: #797949;\" href='" + string.Format(@"/ShowFile.aspx?action=2&fileid={0}", ((DOWNLOAD)e.Item.DataItem).ID_DOWNLOAD_CATEGORIA.ToString()) + "'>download</a>";
        }
    }
}