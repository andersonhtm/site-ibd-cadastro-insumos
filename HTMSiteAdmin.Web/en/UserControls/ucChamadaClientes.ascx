﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucChamadaClientes.ascx.cs"
    Inherits="HTMSiteAdmin.Web.en.UserControls.ucChamadaClientes" %>
<style type="text/css">
    a.linkhover:hover{color:#809e25;}
    a.linkhover{color:#797949;}
</style>

<asp:DataList ID="dtDiretrizesLegislacao" runat="server" OnItemDataBound="dtDiretrizesLegislacao_ItemDataBound"
    RepeatLayout="Flow" ShowFooter="False" ShowHeader="False">
    <ItemTemplate>
        <div class="itemNews">
            <div class="dataTipo">
                <h4 style="margin-left: 0; padding-left: 0;">
                    <asp:Literal ID="ltrCertificado" Text="ltrCertificado" runat="server" /></h4>
            </div>
            <p>
                <asp:Literal ID="ltrLinkCliente" Text="ltrLinkCliente" runat="server" />
            </p>
        </div>
    </ItemTemplate>
</asp:DataList>
