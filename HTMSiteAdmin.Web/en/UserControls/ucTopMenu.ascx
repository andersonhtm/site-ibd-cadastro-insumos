﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucTopMenu.ascx.cs" Inherits="HTMSiteAdmin.Web.en.UserControls.ucTopMenu" %>
<div class="menu">
    <a href="Default.aspx">Home</a><span></span> <a href="QuemSomos.aspx">About Us</a><span></span>
    <a href="servicos">Services</a><span></span> <a href="certificacoes">Certifications</a><span></span>
    <a href="Acreditadores.aspx">Accreditations</a><span></span> <a href="clientes">clients</a><span></span>
    <a href="biblioteca">resources</a><span></span> <a href="atendimento">CUSTOMER SERVICE</a>
</div>
<!--menu-->
<div class="subMenu" id="subServicos">
    <div class="organicos">
        <a href="ServicosCertificacoes.aspx">Certification</a>
        <a href="ServicosCursosTreinamentos.aspx">Trainings and courses</a>
        <a href="ServicosMapeamentoGeoprocessamento.aspx">Mapping and Geoprocessing Services</a>
    </div>
</div>
<!--subServicos-->
<div class="subMenu" id="subCliente">
    <div class="organicos">
        <a href="SejaUmCliente.aspx">Become a client</a> 
        <a href="ProdutosClientesAprovados.aspx">Certified producers and products</a>
        <a href="InsumosClientesAprovados.aspx"> Approved companies and material inputs</a>
        <a href="ProdutosClientesAprovadosEuropeu.aspx"> Producers certified in accordance with EU standard</a>
    </div>
</div>
<!--subCliente-->
<div class="subMenu" id="subBiblioteca">
    <div class="organicos">
        <a href="Noticias.aspx">News Room</a>
        <a href="EventosFeiras.aspx">Events and Fairs</a>
        <a href="IBDNews.aspx">IBD News</a>
        <a href="DiretrizesLegislacao.aspx">Standards and Legislation</a> <a href="LinksInteressantes.aspx">Interesting Links</a>
        <a href="ContratosDetalhes.aspx">Contract</a>
    </div>
</div>
<!--subBiblioteca-->
<div class="subMenu" id="subAtendimento">
    <div class="organicos">
        <a href="CadastreSe.aspx">Register</a> <a href="OndeEstamos.aspx">Our Location</a>
        <a href="FaleConosco.aspx">Contact us</a> <a href="TrabalheConosco.aspx">Work with us</a>
    </div>
</div>
<!--subAtendimento-->
<div class="subMenu" id="subCertificacoes">
    <div class="organicos">
        <a href="#">Organic and Related Certification</a>
        <div class="sub">
            <a href="IbdOrganico.aspx">IBD Organic</a>
            <a href="UsdaOrganic.aspx">USDA Organic</a>
            <a href="JasOrganic.aspx">JAS Organic</a>
            <a href="CanadaOrganic.aspx">Canada Organic (US/NOP Equivalence) </a>
            <a href="Natrue.aspx">Natural and Organic Cosmetics (Natrue)</a>
            <a href="BioSuisse.aspx">Bio Suisse</a>
            <a href="Krav.aspx">KRAV</a>
            <a href="OrganicoCoreia.aspx">Korea Organic</a>
            <a href="InsumoAprovadoIbd.aspx">Approved Input</a> 
        </div>
    </div>
    <%--  <div class="biodinamicos">        
    </div>--%>
    <div class="aprovacoes">
        <a href="#">Fair Trade, Social, Environmental and Sustainability Certification</a>
        <div class="sub">
            <a href="IngredienteNaturalIbd.aspx">Natural Ingredient</a>
            <a href="NaoOgmIbd.aspx">IBD non-GMO</a>
            <a href="FairTradeIbd.aspx">IBD Fair Trade</a>            
            <a href="Rspo.aspx">Roundtable on Sustainable Palm Oil (RSPO)</a>
            <a href="Ras.aspx">Rainforest Alliance (RFA)</a>
            <a href="Verificacao4C.aspx">4C Verification</a>
            <a href="Uebt.aspx">Union for Ethical Bio Trade (UEBT)</a>
            <a href="Utz.aspx">UTZ</a>
            <a href="Sai.aspx">SAI PLATFORM</a>
            <%--<a href="Iscc.aspx">ISCC</a>--%>
        </div>
    </div>
    <div class="qualidade-certificada">
        <a href="#">Procedures Certification</a>
        <div class="sub">
            <a href="QualidadeCertificada.aspx">IBD Qualidade Certificada</a>
        </div>
    </div>
    <div class="socioAmbientais">
        <a href="#">Demeter Certification</a>
        <div class="sub">
            <a href="Demeter.aspx">Demeter International</a>
            <%--<a href="AgroturismoIbd.aspx">IBD Ecological Agritourism</a>--%>
        </div>
    </div>
</div>
<!--subCertificacoes-->
