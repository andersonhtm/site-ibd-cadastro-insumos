﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucChamadaDownloads.ascx.cs"
    Inherits="HTMSiteAdmin.Web.en.UserControls.ucChamadaDownloads" %>
<asp:DataList ID="dtDiretrizesLegislacao" runat="server" OnItemDataBound="dtDiretrizesLegislacao_ItemDataBound"
    RepeatLayout="Flow" ShowFooter="False" ShowHeader="False">
    <ItemTemplate>
        <p>
            <asp:Literal ID="ltrNomeArquivo" Text="Instrução Normativa 46 " runat="server" /><span><asp:Literal
                ID="ltrLinkArquivo" Text="Link do arquivo" runat="server" /></span>
        </p>
    </ItemTemplate>
</asp:DataList>
