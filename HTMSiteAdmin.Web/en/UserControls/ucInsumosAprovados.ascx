﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucInsumosAprovados.ascx.cs"
    Inherits="HTMSiteAdmin.Web.en.UserControls.ucInsumosAprovados" %>
<asp:UpdatePanel runat="server" ID="upInsumosAprovados">
    <ContentTemplate>
        <h3>
            Approved Material Inputs</h3>
        <fieldset>
            <ul>
                <li>
                    <label>
                        by certificate</label>
                    <asp:DropDownList runat="server" ID="ddlCertificado" DataTextField="NOME_EN" DataValueField="ID_CERTIFICADO"
                        OnDataBound="ddlCertificado_DataBound" />
                </li>
                <li>
                    <label>
                        by client</label>
                    <asp:TextBox runat="server" ID="txtClienteCertificadoCliente" />
                </li>
                <li>
                    <label>
                        by input</label>
                    <asp:TextBox runat="server" ID="txtInsumo" />
                </li>
                <li>
                    <label>by category</label>
                    <asp:DropDownList ID="ddlCategoria" DataTextField="DESCRICAO" DataValueField="ID_CATEGORIAPRODUTO" runat="server" />
                </li>
                <li>
                    <label>by purpose</label>
                    <asp:DropDownList ID="ddlFinalidade" DataTextField="DESCRICAO" DataValueField="ID_FINALIDADE" runat="server" />
                </li>
                <li>
                    <asp:LinkButton ID="btnClienteCertificadoBuscar" runat="server" CssClass="btBuscar"
                        OnClick="btnClienteCertificadoBuscar_Click">search</asp:LinkButton></li>
            </ul>
        </fieldset>
    </ContentTemplate>
</asp:UpdatePanel>
