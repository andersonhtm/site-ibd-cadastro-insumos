﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Conteudos;

namespace HTMSiteAdmin.Web.en.UserControls
{
    public partial class ucUltimasNoticias : UserControl
    {
        IQueryable<CONTEUDO> _objConteudoCollection;

        protected void Page_Load(object sender, EventArgs e)
        {
            _objConteudoCollection = new ConteudoBo().Find(c => !string.IsNullOrEmpty(c.TITULO_EN) && c.ID_CONTEUDO_TIPO == 1 && (!c.DATA_PUBLICACAO.HasValue || (c.DATA_PUBLICACAO.HasValue && c.DATA_PUBLICACAO.Value <= DateTime.Now))).OrderByDescending(lbda => lbda.DATA_PUBLICACAO);
            var list = _objConteudoCollection.ToList();
            dtUltimasNoticias.DataSource = list.Count > 4 ? _objConteudoCollection.ToList().GetRange(0, 5) : _objConteudoCollection.ToList();
            dtUltimasNoticias.DataBind();
        }

        protected void dtUltimasNoticias_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (((CONTEUDO)e.Item.DataItem).DATA_OCORRENCIA.HasValue)
            {
                ((Literal)e.Item.FindControl("ltrDiaNoticia")).Text = ((CONTEUDO)e.Item.DataItem).DATA_OCORRENCIA.Value.Day.ToString("00");
                ((Literal)e.Item.FindControl("ltrMesNoticia")).Text = ((CONTEUDO)e.Item.DataItem).DATA_OCORRENCIA.Value.ToString("MMM");
            }
            else
            {
                ((Literal)e.Item.FindControl("ltrDiaNoticia")).Text = DateTime.Now.Day.ToString("00");
                ((Literal)e.Item.FindControl("ltrMesNoticia")).Text = DateTime.Now.ToString("MMM");
            }

            ((Literal)e.Item.FindControl("ltrCategoria")).Text = new ConteudoCategoriaBo().Find(lbda => lbda.ID_CONTEUDO_CATEGORIA == ((CONTEUDO)e.Item.DataItem).ID_CONTEUDO_CATEGORIA).First().NOME;
            ((Literal)e.Item.FindControl("ltrLinkNoticia")).Text = string.Format("<a style=\"color: #797949;\" href=\"NoticiasDetalhes.aspx?id_conteudo={0}\">{1}</a>", ((CONTEUDO)e.Item.DataItem).ID_CONTEUDO.ToString(), ((CONTEUDO)e.Item.DataItem).TITULO_EN);
        }
    }
}