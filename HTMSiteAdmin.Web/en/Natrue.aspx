﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="Natrue.aspx.cs" Inherits="HTMSiteAdmin.Web.en.Natrue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Organic
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Natrue Cosmetics</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <a href="http://www.natrue.org">
                <img src="media/img/logo_natrue_ibd.png" alt="" style="float: right; margin-left: 15px;
                    margin-bottom: 10px;" />
            </a>
            <h2 class="tituloCertificaoInterno">
                About the Seal</h2>
            <span style="font-weight: bold">For cosmetics certified by IBD, the rules are simple
                and easy to apply:</span>
            <br />
            <br />
            <span style="font-weight: bold">Organic:</span> with at least 95% organic ingredients.
            orgânicos.
            <br />
            <br />
            <span style="font-weight: bold">Made with Organic or Organic Raw Materials:</span>
            with at least 70% organic ingredients.
            <br />
            <br />
            <span style="font-weight: bold">Natural:</span> less than 70% organic ingredients.
            <br />
            <br />
            This is a very reliable trend: the organic consumer is aware and wants products
            that are truly organic, with a significant percentage of organic raw materials!
            <br /><br />

            <h2 class="tituloCertificaoInterno">Step by Step</h2>
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=40023888-bb5f-4877-b1e1-add1c50e695f"> - Step by Step to certification (English)</a><br />
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=93114085-8a56-41fe-91f4-59758d7f3f32"> - Step by Step to certification (Portuguese)</a> 
            <br /><br />

            <h2 class="tituloCertificaoInterno">
                Standards Served</h2>
                <!-- <span class="itemNormas">Natrue</span> -->
                - Natrue
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segments Served</h2>
            - Cosmetics<br />
            <br />
            <!--
            <h2 class="tituloCertificaoInterno">
                Additional Information</h2>
            <a class="itemNormas" href="/Certificacoes/DownloadFile?name=8_1_2_C_Diretrizes_IBD_Cosmeticos_3aEd_02102010.pdf">
                Natural and Organic Cosmetics Standards (IBD)</a> <span class="itemNormas">Cosmetics
                    Folder IBD</span> <span class="itemNormas">Certifica&ccedil;&atilde;o de Cosmetics Certification
                        Organic</span> <a class="itemNormas" href="/Certificacoes/DownloadFile?name=MP_Permitidas_Cosmticos_IBD_042009.pdf">
                            Raw Material of Natural and Organic Cosmetics (IBD)</a> <span class="itemNormas">Comparison
                                between Cosmetics certifications IBD x Ecocert</span>
            <br />
            <br />
            <br /> -->
            <br />
    </div>
    <!--quemsomos-->
</asp:Content>
