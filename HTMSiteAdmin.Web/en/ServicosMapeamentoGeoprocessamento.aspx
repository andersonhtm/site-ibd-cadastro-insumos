﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="ServicosMapeamentoGeoprocessamento.aspx.cs" Inherits="HTMSiteAdmin.Web.en.ServicosMapeamentoGeoprocessamento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Servi&ccedil;os
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Mapping and Geoprocessing</h2>
            <a href="/">return to home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos" class="texto-justificado">
        <br />
        
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>

            <h2 class="tituloCertificaoInterno">What are Geoprocessing and Geographic Information System?</h2>
            <img src="../Media/img/mapa-geo-1.jpg" alt="" style="float: right; margin-left: 15px; margin-bottom: 10px; max-width: 300px" />
            <p>Geoprocessing is the technology capable of geographically spatializing and cross-referencing information from a variety of sources. Through layers of information that can be studied and analyzed allows a better understanding of how geographic factors interfere in productive and everyday activities.</p>
            <img src="../Media/img/mapa-geo-2.png" alt="" style="float: left; margin: 15px 15px 10px 0; max-width: 220px" />
            <p>The geographic information system (GIS) is a system of hardware, software, spatial information, computational procedures and human resources that allows and facilitates the analysis, management or representation of physical space (landscape) and phenomena that occur in it. With a GIS system it is possible to collect, store, retrieve, manipulate, visualize and analyze spatially referenced data in a known coordinate system. As results are generated maps or charts that can be physical maps (lanscape, climate, vegetation, hydrography, etc.); Political maps (territories, countries, states, cities, etc.) and thematic maps (specific elements or phenomena).</p>

            <p style="clear: both; padding-top: 20px"><b>With a geographic information system it is possible to:</b></p>
        
        • Interpretation and analysis of the physical and biotic environment: landscape, hydrography, drainage, soil, slope, land use, topography, species distribution, occurrence location, etc .;
       <br />• Interpretation and analysis of cadastral data: properties, rural and urban areas, protection areas, transport system - logistics;
       <br />• Spatial analyzes: neighborhood, overlap, surfaces, networks, direct relation, correlation, contiguity, connectivity, proximity.
       <br />• Various analyzes: classification, generalization, density, indices, multi-criteria, statistics, geostatistics, trend.


        <p style="clear: both; padding-top: 20px"><b>Among the applications can be highlighted:</b></p>

        <img src="../Media/img/mapa-geo-3.jpg" alt="" style="float: right; margin-left: 15px; margin-bottom: 10px; max-width: 300px" />

              •	Geolocation and georeferencing;
        <br />•	Calculation of routes (better, shorter, etc.);
        <br />•	Area calculations (area, perimeter, volume, density, zoning, etc.);
        <br />•	Land use and vegetation cover, history of soil use;
        <br />•	Vegetation density index (NDVI, EVI, etc.);
        <br />•	Compliance with and compliance with Environmental Legislation (Areas of permanent preservation and legal reserve);
        <br />•	Risk analysis (areas of risk to burn, buffer zone of protected areas and protected areas, risk of erosion, etc.);
        <br />•	Maps of spatial distribution of soil attributes, parameters, frequency or any other quantitative information that has a spatial location through different geostatistical analysis methods. As examples: spatial distribution of physical-chemical soil attributes, spatial distribution of rainfall, etc.;
        <br />

        <div style="display: flex; justify-content: space-between; align-items: center">
            <img src="../Media/img/mapa-geo-4.jpg" alt="" style="margin: 20px 0 20px 0; max-width: 320px" />
            <img src="../Media/img/seta.png" alt="" style="margin: 10px; max-width: 50px" />
            <img src="../Media/img/mapa-geo-5.jpg" alt="" style="margin:  20px 0 20px 0; max-width: 320px;" />
        </div>

         <img src="../Media/img/mapa-geo-6.jpg" alt="" style="float: right; max-width: 150px; margin-left: 20px" />

        •	Online hosting of geographic information for consultation, visualization and / or editing of data via the internet (webmapping).
        <br />
        <br />

        <p>IBD has a variety of services and applications in geoprocessing and geographic information system. Access the link below and see the portfolio of products and services IBD offers.</p>

        <p style="margin: 10px 0 10px 0"><a href="ServicosMapeamentoGeoprocessamentoDetalhes.aspx" style="text-decoration: underline">Click and learn more about IBD's mapping and geoprocessing products and services</a></p>
       
            
            
    </div>
    <!--quemsomos-->
</asp:Content>
