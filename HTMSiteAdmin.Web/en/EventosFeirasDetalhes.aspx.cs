﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Eventos;

namespace HTMSiteAdmin.Web.en
{
    public partial class EventosFeirasDetalhes : System.Web.UI.Page
    {
        private int? idConteudo
        {
            get
            {
                try { return Convert.ToInt32(Request.QueryString["id_conteudo"]); }
                catch (Exception) { return null; }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (idConteudo.HasValue && idConteudo.Value > 0)
            {
                EVENTO _objEvento = new EventoBo().Find(c => c.ATIVO && c.ID_EVENTO == idConteudo.Value).FirstOrDefault();

                if (_objEvento == null)
                    return;


                if (_objEvento.DATA_INICIO.Value.Year.Equals(_objEvento.DATA_FIM.Value.Year)
                && _objEvento.DATA_INICIO.Value.Month.Equals(_objEvento.DATA_FIM.Value.Month)
                && _objEvento.DATA_INICIO.Value.Day.Equals(_objEvento.DATA_FIM.Value.Day))
                {
                    ltrPeriodo.Text = string.Format("{0}",
                        _objEvento.DATA_INICIO.Value.ToShortDateString());
                } else if (_objEvento.DATA_INICIO.Value.Year.Equals(_objEvento.DATA_FIM.Value.Year)
                && _objEvento.DATA_INICIO.Value.Month.Equals(_objEvento.DATA_FIM.Value.Month))
                {
                    ltrPeriodo.Text =
                        string.Format("{0} to {1} ",
                        _objEvento.DATA_INICIO.Value.ToShortDateString(),
                        _objEvento.DATA_FIM.Value.ToShortDateString());
                }
                else if (_objEvento.DATA_INICIO.Value.Year.Equals(_objEvento.DATA_FIM.Value.Year)
                    && !_objEvento.DATA_INICIO.Value.Month.Equals(_objEvento.DATA_FIM.Value.Month))
                {
                    ltrPeriodo.Text =
                        string.Format("{0} to {1} ",
                        _objEvento.DATA_INICIO.Value.ToShortDateString(),
                        _objEvento.DATA_FIM.Value.ToShortDateString());
                }
                else
                {
                    ltrPeriodo.Text =
                        string.Format("{0} to {1} ",
                        _objEvento.DATA_INICIO.Value.ToShortDateString(),
                        _objEvento.DATA_FIM.Value.ToShortDateString());
                }

                if (_objEvento.DATA_PUBLICACAO.HasValue)
                    ltrDataPublicacao.Text = _objEvento.DATA_PUBLICACAO.Value.ToShortDateString();
                else
                    ltrDataPublicacao.Text = _objEvento.SESSAO.DATA_INICIO.ToShortDateString();

                ltrCategoria.Text = new EventoCategoriaBo().Find(c => c.ID_EVENTO_CATEGORIA == _objEvento.ID_EVENTO_CATEGORIA).First().DESCRICAO;
                ltrTitulo.Text = _objEvento.DESCRICAO_EN;
                ltrConteudo.Text = _objEvento.DESCRICAO_DETALHADA_EN;
            }
        }
    }
}