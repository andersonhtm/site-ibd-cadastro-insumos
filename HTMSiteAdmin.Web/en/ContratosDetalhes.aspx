﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true" CodeBehind="ContratosDetalhes.aspx.cs" Inherits="HTMSiteAdmin.Web.en.ContratosDetalhes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Resources
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Contracts</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
    <br />
          
    <h2 class="tituloCertificaoInterno">
        <a href= "http://www.ibd.com.br/mala/Contrato_de_Presta%C3%A7%C3%A3o_de_Servi%C3%A7os_IBD.pdf">Contract of Provided Services IBD Reg021977</a> 
    </h2>
    <h2 class="tituloCertificaoInterno">
        <a href= "http://www.ibd.com.br/mala/ContratoRegistrato_Reg022412.pdf">Contract of Provided Services IBD Reg022412</a> 
    </h2>
    <h2 class="tituloCertificaoInterno">
        <a href= "http://ibd.com.br/ShowFile.aspx?action=2&fileid=e5f223b5-791c-462c-9794-51f062a1e032">Contract of Provided Services IBD Reg010695</a> 
    </h2>

    </div>
    <!--quemsomos-->
</asp:Content>
