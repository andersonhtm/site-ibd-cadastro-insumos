﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Text;
using HTMSiteAdmin.Web.Classes;

namespace HTMSiteAdmin.Web.en
{
    public partial class FaleConosco : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder corpoEmail = new StringBuilder();
                corpoEmail.AppendLine("Atendimento: Fale Conosco<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("Data Hora: " + DateTime.Now.ToString() + "<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("Nome: " + txtNome.Text + "<br />");
                corpoEmail.AppendLine("Telefone: " + txtTelefone.Text + "<br />");
                corpoEmail.AppendLine("E-mail: " + txtEmail.Text + "<br />");
                corpoEmail.AppendLine("Mensagem: " + txtMensagem.Text + "<br />");
                Util.EnviarEmail(corpoEmail.ToString(),
                    string.Format("[Fale Conosco] {0}", txtNome.Text),
                    Configuracao.CONTACT_EMAILTO, "", Configuracao.MAIL_BLINDCOPYTO);

                corpoEmail = new StringBuilder();
                corpoEmail.AppendLine("Dear: " + txtNome.Text + "<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("You have contacted IBD Certifications.<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("We will contact you soon.<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("Regards,<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("IBD Team<br />");
                corpoEmail.AppendLine(ConfigurationManager.AppSettings["CONTACT_EMAILREPLY"] + "<br />"); 
                Util.EnviarEmail(corpoEmail.ToString(),
                    string.Format("[Contact Us - IBD] {0}", txtNome.Text),
                    txtEmail.Text, "", Configuracao.MAIL_BLINDCOPYTO);

                Response.Redirect("FaleConosco.aspx", true);
            }
            catch (Exception ex) { 
                throw ex; 
            }
        }
    }
}