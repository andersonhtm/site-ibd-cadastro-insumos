﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Conteudos;

namespace HTMSiteAdmin.Web.en
{
    public partial class Clientes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dtClientes.DataSource = new ConteudoBo().Find(lbda => lbda.ID_CONTEUDO_TIPO == 3 && lbda.ID_CONTEUDO_SITUACAO == 1 && !string.IsNullOrEmpty(lbda.TITULO_EN)).OrderBy(lbda => lbda.TITULO);
            dtClientes.DataBind();
        }

        protected void dtClientes_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            CONTEUDO _objConteudo = (CONTEUDO)e.Item.DataItem;

            ((Literal)e.Item.FindControl("ltrLinkClienteTopo")).Text = "<a href=\"" + ResolveUrl(string.Format("~/en/ClientesDetalhes.aspx?id_conteudo={0}", _objConteudo.ID_CONTEUDO)) + "\"></a>";
            ((Literal)e.Item.FindControl("ltrClienteTitulo")).Text = new ConteudoCategoriaBo().Find(lbda => lbda.ID_CONTEUDO_CATEGORIA == _objConteudo.ID_CONTEUDO_CATEGORIA).First().NOME;
            ((Literal)e.Item.FindControl("ltrLinkClienteBot")).Text = "<a href=\"" + ResolveUrl(string.Format("~/en/ClientesDetalhes.aspx?id_conteudo={0}", _objConteudo.ID_CONTEUDO)) + "\">" + _objConteudo.TITULO + "</a>";
        }
    }
}