﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="NaoOgmIbd.aspx.cs" Inherits="HTMSiteAdmin.Web.en.NaoOgmIbd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications and Approvals</span><br />
                Diverse
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                IBD Non-GMO</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoNaoOGMIBD.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                About the Seal</h2>
            Everyone knows that Genetically Modified Organisms (GMOs) have come to stay. However,
            the organic sector and a large part of the consumer market do not want to consume
            such products.
            <br />
            <br />
            Given this, the Certification Program for NON-GMOs was created by IBD in order to
            segregate products within the market that contain GMO from those that truly do not
            contain GMO material, or do contain GMOs, but within pre-established limits.<br />
            <br />
            The food sector should be labeling food items that contain GMOs (see specific legislation
            in attachment). However, this is not happening. Consumers also demand greater transparency
            regarding the effect of GMO consumption on animals and humans. We have included
            text regarding this theme as well.
            <br />
            <h2 class="tituloCertificaoInterno">
                Standards Served</h2>
                <!-- <span class="itemNormas">IBD Standards for NON-GMO Products</span> -->
                - IBD Standards for NON-GMO Products
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segments Served</h2>
            - Agriculture<br />
            - Livestock<br />
            - Processing<br />
            - Cosmetics<br />
            <br />
            <br />
            <br />
    </div>
    <!--quemsomos-->
</asp:Content>
