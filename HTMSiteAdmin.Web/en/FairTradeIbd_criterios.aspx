﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="FairTradeIbd_criterios.aspx.cs" Inherits="HTMSiteAdmin.Web.en.FairTradeIbd_criterios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Socio-Environmental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px;
                    margin-right: 6px; margin-left: 6px;" />
                IBD Fair Trade Ecosocial</h2>
            <a href="/">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoEcoSocialIBD.png" alt="" style="float: right;
                margin-left: 15px; margin-bottom: 10px;" />
            <span class="itemNormas">&lt;&lt;
                <a href="FairTradeIbd.aspx">Return to Socio-Environmental IBD Fair Trade Ecosocial page</a></span>
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">Criteria</h2>
            <p>Fair Trade Ecosocial audits confirm that the following practices are in place.</p>
            <h2 class="tituloCertificaoInterno">
                Critical</h2>
            <p>
                - Zero gaps in traceability;<br />
                - Legal and traceable land ownership;<br />
                - Documented procedures for contract labor;<br />
                - Social, cultural, political, religious, ethnic, racial, gender and age discrimination is forbidden;<br />
                - No child labour;<br />
                - No forced labour;<br />
                - Healthy and safe working conditions;<br />
                - Planned, legal and justified forest clearing allowed only;<br />
                - No unauthorized contamination of rivers with irregular solid or liquid disposals;<br />
                - No hunting, capture or trade of wild animals;<br />
                - Use of GMOs is not allowed. <br />
            </p>
            <br />
            <h2 class="tituloCertificaoInterno">Environmental</h2>
            <p>
                - Protected natural areas;<br />
                - Natural resources management (forestry, subsoil and soil resources);<br />
                - Agricultural management;<br />
                - No use of fire;<br />
                - Water resources management;<br />
                - Solid residues management;<br />
                - Liquid effluent management;<br />
                - Gas effluent management;<br />
                - Biodiversity and seed bank management;<br />
                - Genetically Modified Organisms;<br />
                - Sustainability in Conventional products Management.<br />
            </p>
            <br />
            <h2 class="tituloCertificaoInterno">Social</h2>
            <p>
                - Support to labour union’s work; Safety at work;<br />
                - Social benefits: permanent and seasonal workers;<br />
                - Profit sharing;<br />
                - Qualification for employees;<br />
                - Qualification for group management;<br />
                - Internal quality control system;<br />
                - System of positions and salaries;<br />
                - Basic and fundamental education;<br />
                - Housing;<br />
                - Nutrition;<br />
                - Health;<br />
                - Support to working women;<br />
                - Support to pregnant and nursing women;<br />
                - Support to aged people.<br />
            </p>
            <br />
            <h2 class="tituloCertificaoInterno">Economic Development</h2>
            <p>
                - Fair trade;<br />
                - Free price negotiation;<br />
                - Profits and Fair Trade Ecosocial fees are re-invested in the project.<br />
            </p>
            <br />
            <br />
    </div>
    <!--quemsomos-->
</asp:Content>
