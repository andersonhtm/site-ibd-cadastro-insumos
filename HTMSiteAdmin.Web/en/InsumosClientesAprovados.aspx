﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="InsumosClientesAprovados.aspx.cs" Inherits="HTMSiteAdmin.Web.en.InsumosClientesAprovados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Clients</span><br />
                Certified Clients and Inputs
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Search</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="certificadosConsultar">
        <div class="texto">
            <p>
                Research companies and products certified by IBD. To facilitate your search, select
                the desired criteria:</p>
            <fieldset>
                <ul>
                    <li>
                        <label>
                            by certificate</label>
                        <asp:DropDownList runat="server" ID="ddlCertificado" DataTextField="NOME" DataValueField="ID_CERTIFICADO"
                            OnDataBound="ddlCertificado_DataBound" Width="250px" Style="float: left;" />
                    </li>
                    <li>
                        <label>
                            by client</label>
                        <asp:TextBox runat="server" ID="txtClienteCertificadoCliente" Width="250px" Style="float: left;" />
                    </li>
                    <li>
                        <label>
                            by input</label>
                        <asp:TextBox runat="server" ID="txtInsumo" Width="250px" Style="float: left;" />
                    </li>
                    <li>
                        <label>by category</label>
                        <asp:DropDownList ID="ddlCategoria" DataTextField="DESCRICAO" DataValueField="ID_CATEGORIAPRODUTO" runat="server" Style="float: left;" width="250" />
                    </li>
                    <li>
                        <label>by purpose</label>
                        <asp:DropDownList ID="ddlFinalidade" DataTextField="DESCRICAO" DataValueField="ID_FINALIDADE" runat="server" Style="float: left;" width="250" />
                    </li>
                    <li>
                        <asp:LinkButton ID="btnClienteCertificadoBuscar" runat="server" CssClass="btBuscar"
                            OnClick="btnClienteCertificadoBuscar_Click">search</asp:LinkButton></li>
                </ul>
            </fieldset>
        </div>
        <!--texto-->
        <div class="imagem">
            <img src="media/img/produtosCertificados.jpg" class="border3pxfff" alt="" /></div>
        <!--imagem-->
    </div>
    <!--certificadosConsultar-->
</asp:Content>
