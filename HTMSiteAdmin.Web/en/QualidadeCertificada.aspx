﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="QualidadeCertificada.aspx.cs" Inherits="HTMSiteAdmin.Web.en.QualidadeCertificada" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Organic
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                IBD Qualidade Certificada</h2>
            <a href="/">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        
            <a href="#">
                <img src="../Media/img/logo_ibd_qualidade_certificada.png" alt="IBD Qualidade Certificada" style="float: right; margin-left: 15px; margin-bottom: 10px;" />
            </a>

            <h2 class="tituloCertificaoInterno">Sobre o Selo</h2>

            A sociedade cobra cada vez mais coerência do setor produtivo para minimizar as contaminações do ambiente, do ser humano e de toda a cadeia produtiva.<br /><br />
            Existem demandas por uma certificação de produtos aptos para o consumo humano. Há aumento na procura pelo consumidor e pelo produtor por produtos produzidos sem o uso de agrotóxicos mas com modernas técnicas de equilíbrio nutricional, com o uso de insumos naturais e controle biológico de pragas e doenças. <br /><br />
            Neste sentido o IBD gerou estas normas que permitirão às empresas e produtores venderem seus produtos com destaque no mercado e atendendo a esta demanda do consumidor.<br /><br />

            <h2 class="tituloCertificaoInterno"> Diretrizes Atendidas</h2> 
                - Qualidade Certificada IBD
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segmentos Atendidos</h2>
            - Todos os setores produtivos de alimentos.<br />
            <br />
            <p>AS DIRETRIZES SOMENTE SERÃO REPASSADAS A UMA PESSOA SOMENTE APÓS PAGAMENTO DE R$ 50. FAVOR CONTATAR: <a href="mailto:ibd@ibd.com.br">ibd@ibd.com.br</a></p>
        
    </div>            
</asp:Content>
