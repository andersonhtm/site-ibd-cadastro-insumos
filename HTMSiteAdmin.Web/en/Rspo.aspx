﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="Rspo.aspx.cs" Inherits="HTMSiteAdmin.Web.en.Rspo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Socio- Environmental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px;
                    margin-right: 6px; margin-left: 6px;" />
                RSPO</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoRSPO.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                About the Seal</h2>
            The RSPO movement (Roundtable on Sustainable Palm Oil) is rapidly growing throughout the world (see portfolio of members at www.rspo.org), with 
            membership by the major multinational consumers of palm oil at the global level. IBD Certifications is the only Brazilian certifier within RSPO and, as such, 
            qualified to offer auditing services for this scope of certification at global level.
            <br />
            <br />
            For African palm fruit and oil producing companies, processors of palm oil and derivatives, as well as traders of these products, IBD offers a certification 
            package based on RSPO Principles and Criteria or on the RSPO Chain of Custody system.<br /><br />

            For Latin America, Africa and Asia: Please contact Leonardo Gomes at leonardo@ibd.com.br<br />
            For Europe: Please contact Bernhard Schulz at ceres@ceres.com.br . Ceres is IBDs auditing part for Germany and Europe<br /><br />
            Please find attached the electronical IBD RSPO folder for download.<br />

            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=920d2f1f-a16e-4bd3-8c22-7a7bf2a32671">Folder IBD-RSPO</a><br />
        </p>

            <h2 class="tituloCertificaoInterno">Step by Step</h2>
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=a0183378-f10e-4e35-8c3f-f9c73f5f8119"> - Step by Step to certification (Portuguese)</a><br />
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=55e1f784-742b-4a45-b7ae-c97e96495177"> - Step by Step to certification (English)</a><br />
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=8d540a92-95be-46a2-93fa-6ec8cc4ca01b"> - Step by Step to certification (Spanish)</a>
            <br /><br />


            <h2 class="tituloCertificaoInterno">
                Principles</h2>
            - Environmental responsibility and conservation of natural resources and biodiversity;<br />
            - Commitment to transparency;<br />
            - Use of appropriate best practices by growers and millers;<br />
            - Compliance with applicable laws and regulations;<br />
            - Responsible development of new plantings;<br />
            - Commitment to long-term economic and financial viability;<br />
            - Responsible consideration of employees and of individuals and communities affected
            by growers and mills;
            <br />
            - Commitment to continuous improvement in key areas of activity.
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Additional Information</h2>
            <p>
                For more information see the files below.<br />
                <br />
                <!-- <span class="itemNormas">Guidelines for RSPO Certification</span><span class="itemNormas">
                    RSPO Certification Standards </span> -->
                <a href="DiretrizesLegislacao.aspx" class="itemNormas">Standards and Legislation</a>

                <br />
                <br />
            </p>
            <br />
            
            <h2 class="tituloCertificaoInterno">
                Certified Clients
            </h2>
            
            <asp:Repeater ID="rpt" runat="server">
                <HeaderTemplate>
                    <ul class="list">
                </HeaderTemplate>
                <ItemTemplate>                    
                    <li><a href="<%# Eval("Caminho_EN") %>" class="imagem"><%# Eval("Descricao_EN")%></a></li>                    
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
            <br />
            <br />
    </div>
    <!--quemsomos-->
</asp:Content>
