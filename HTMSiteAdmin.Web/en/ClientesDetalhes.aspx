﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true" CodeBehind="ClientesDetalhes.aspx.cs" Inherits="HTMSiteAdmin.Web.en.ClientesDetalhes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Cases</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="noticias">
        <div class="itemNoticiasInterno">
            <div class="texto2 texto">
                <div class="dataTipo">
                    <span style="display:none;">
                        <asp:Literal ID="ltrDia" Text="1" runat="server" />
                        |
                        <asp:Literal ID="ltrMes" Text="Janeiro" runat="server" /></span>
                    <h4 style="margin-left:-10px;">
                        <asp:Literal ID="ltrCategoria" Text="" runat="server" /></h4>
                    <p style="display:none;">
                        published 
                        <asp:Literal ID="ltrDataPublicacao" Text="30/01/2012" runat="server" /></p>
                </div>
                <!--dataTipo-->
                <h3>
                    <asp:Literal ID="ltrTitulo" Text="Not Found" runat="server" /></h3>
                <p>
                    <asp:Literal ID="ltrConteudo" Text="The case informed could not be found." runat="server" /></p>
            </div>
            <!--texto-->
        </div>
        <!--itemNoticias-->
    </div>
    <!--noticias-->
</asp:Content>
