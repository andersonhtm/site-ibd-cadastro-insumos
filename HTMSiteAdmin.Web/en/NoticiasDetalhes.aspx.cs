﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Business.Conteudos;
using HTMSiteAdmin.Data;

namespace HTMSiteAdmin.Web.en
{
    public partial class NoticiasDetalhes : System.Web.UI.Page
    {
        private int? idConteudo
        {
            get
            {
                try { return Convert.ToInt32(Request.QueryString["id_conteudo"]); }
                catch (Exception) { return null; }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (idConteudo.HasValue && idConteudo.Value > 0)
            {
                CONTEUDO _objConteudo = new ConteudoBo().Find(c => c.ID_CONTEUDO == idConteudo.Value).First();
                
                if (_objConteudo.ID_CONTEUDO_SITUACAO != 1)
                    return;

                if (_objConteudo.DATA_PUBLICACAO.HasValue)
                {
                    ltrDia.Text = _objConteudo.DATA_PUBLICACAO.Value.Day.ToString();
                    ltrMes.Text = (_objConteudo.DATA_PUBLICACAO.Value.ToString("MMM"));
                    ltrDataPublicacao.Text = _objConteudo.DATA_PUBLICACAO.Value.ToShortDateString();
                }
                else
                {
                    ltrDia.Text = _objConteudo.SESSAO.DATA_INICIO.Day.ToString();
                    ltrMes.Text = HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objConteudo.SESSAO.DATA_INICIO.Month);
                    ltrDataPublicacao.Text = _objConteudo.SESSAO.DATA_INICIO.ToShortDateString();
                }

                ltrCategoria.Text = new ConteudoCategoriaBo().Find(c => c.ID_CONTEUDO_CATEGORIA == _objConteudo.ID_CONTEUDO_CATEGORIA).First().NOME;
                ltrTitulo.Text = _objConteudo.TITULO_EN;
                if (!String.IsNullOrEmpty(_objConteudo.SUB_TITULO_EN))
                    ltrSubTitulo.Text = "<h2>" + _objConteudo.SUB_TITULO_EN + "</h2><br />";
                ltrConteudo.Text = _objConteudo.CONTENT_DATA_EN;
                if (!String.IsNullOrEmpty(_objConteudo.FONTE))
                    ltrFonte.Text = "Source: " + _objConteudo.FONTE;
            }
        }
    }
}