﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="IbdOrganico.aspx.cs" Inherits="HTMSiteAdmin.Web.en.IbdOrganico" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Organic
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                IBD Organic and Brazil Organic</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoIBDOrganico.png" alt="" width="140" height="206"
                style="float: right; margin-left: 15px; margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                About the Seal</h2>
            <p>
                The IBD Organic seal has two characteristics:<br />
                <br />
                1) It applies to all organic certifications carried out by IBD Certifications for
                the internal market, used in conjunction with the Brazilian Seal for organic product.
            </p>
            <img src="media/img/logoCertificadoSisOrg_meio.png" alt="" />
            <p>
                2) Since the IBD standard is approved by European accreditation agencies as equivalent
                to the European rule, it applies to all certifications intended for the European
                Common Market.</p>
            <img src="media/img/logoCertificadoEUOrganic_meio.png" alt="" />
            <p>
                3) provides all certifications necessary for access tothe North American Market
                (USA), with accreditation for the USDA NOP, the only standard applicable to this
                market.</p>
            <img src="media/img/selo_nop.png" alt="" />

            <h2 class="tituloCertificaoInterno">Step by Step</h2>

            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=93014008-7780-4669-90c7-e90ccffbab87">Step by Step to certification (English)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=07511992-13b3-4b3f-80a2-3d7cee456607">Step by Step to certification (Portuguese)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=787619fb-fc31-4eed-a1bf-404738c0ecb0">Step by Step to certification (Spanish)</a><br />
            <br /><br />

            <h2 class="tituloCertificaoInterno">Step by Step Producers Groups certification</h2>

            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=b66b1167-f68e-4810-a581-53bbf9d4e7c6">Step by Step to certification (English)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=40b81a87-3a1c-4e7b-990f-4e0ca875c0e2">Step by Step to certification (Portuguese)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=5ad12599-e08f-4656-a880-76c4304a73e2">Step by Step to certification (Spanish)</a><br />
            <br /><br />
            
            <h2 class="tituloCertificaoInterno">
                Standards Served</h2><!--
            <span class="itemNormas">IBD Standards</span> <span class="itemNormas">IFOAM Standards
            </span><span class="itemNormas">Quebec Standards</span><span class="itemNormas">CEE
                834/2007 </span><span class="itemNormas">Law 10.831 (SisOrg)</span><span class="itemNormas">
                    NOP Extrato (Dez/06 - Português)</span> <span class="itemNormas">Lista Nacional (Set/06
                        - Portugês)</span> <span class="itemNormas">NOP (Dez/06 - Inglês)</span> -->
                - IBD Standards<br />
                - IFOAM Standards<br />
                - USDA Standards<br />
                - CEE 834/2007<br />
                - Law 10.831 (SisOrg)<br />
                
            <br />
            <h2 class="tituloCertificaoInterno">
                Segments Served</h2>
            - Agriculture<br />
            - Livestock<br />
            - Fibers<br />
            - Aquaculture<br />
            - Processing<br />
            - Material inputs<br />
            - Wild Harvesting<br />
            - Cosmetics<br />
            - Wine<br />
            - Cleaning products
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Additional Information</h2>
            <p>
                The use of the IFOAM ACCREDITED Seal is optional.</p>
            <br />
            <p>
                In order to re-certify the following is necessary:<br />
                - Annual Certificate +
                <br />
                - Transaction Certificate +
                <br />
                - Review of reports +
                <br />
                - Socioenvironmental Check-list.
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
    </div>
    <!--quemsomos-->
</asp:Content>
