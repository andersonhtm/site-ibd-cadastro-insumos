﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="SejaUmCliente.aspx.cs" Inherits="HTMSiteAdmin.Web.en.SejaUmCliente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Client</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="clientes">
        <p>
            <img src="media/img/fotoCliente.jpg" alt="" width="266" height="166" style="float: right;
                margin-left: 15px; margin-bottom: 10px;" />We offer personalized service, protocols
            appropriate to your needs, and a specialized team with vast experience in the certification
            market.
            <br />
            <br />
            Contact our customer service department to obtain more information and request documents
            for initiating the certification process. We are happy to serve you.
        </p>
        <div style="clear: both;">
            <!-- -->
        </div>
        <div class="motivos">
            <h3>
                Discover the 10 reasons to become an IBD client:</h3>
            <div class="itens">
                <span>1</span>
                <p>
                    Throughout the world and also in Brazil, the name IBD is linked to professionalism,
                    seriousness and dedication. With an IBD certificate, your products will be accepted
                    globally.</p>
            </div>
            <div class="itens">
                <span>2</span>
                <p>
                    In addition to being the largest certifier of organic products in Latin America,
                    IBD has extended its services to protocols related to sustainability, recently obtaining
                    accreditation for RSPO, UEBT and BONSUCRO, demonstrating vanguardism and professionalism.
                </p>
            </div>
            <div class="itens">
                <span>3</span>
                <p>
                    The Fair Trade IBD seal for social, environmental and fair trade adequacy of organic
                    products.
                </p>
            </div>
            <div class="itens">
                <span>4</span>
                <p>
                    Estimates are clear and transparent, permitting complete planning by the client
                    for investment, structure and meeting deadlines.
                </p>
            </div>
            <div class="itens">
                <span>5</span>
                <p>
                    Nearly 90% of the projects certified by IBD are family farmers. IBD’s structure
                    permits accessibility and differentiated costs, adapted to the economic situation
                    of each producer.
                </p>
            </div>
            <div class="itens">
                <span>6</span>
                <p>
                    IBD works with a vast network of inspectors distributed regionally, providing for
                    efficiency and cost accessibility.
                </p>
            </div>
            <div class="itens">
                <span>7</span>
                <p>
                    For those interested in importing organic product into Brazil under the Brazilian
                    Law 10.831, IBD relies on a network of more than 50 inspectors located in more than
                    20 countries worldwide.
                </p>
            </div>
            <div class="itens">
                <span>8</span>
                <p>
                    Inspectors receive annual internal trainings, including specialized lectures and
                    technical audit content, giving the IBD inspector superior technical qualifications.
                </p>
            </div>
            <div class="itens">
                <span>9</span>
                <p>
                    IBD was the pioneer for incorporating social issues, based on international labor
                    agreements, into certification, serving as a model for other certifiers internationally.
                </p>
            </div>
            <div class="itens">
                <span>10</span>
                <p>
                    IBD is a pioneer in guaranteeing the preservation of forests and water resources
                    within certified projects. As a result, tens of thousands of native species have
                    been replanted.
                </p>
            </div>
        </div>
        <!--motivos-->
    </div>
    <!--quemsomos-->
</asp:Content>
