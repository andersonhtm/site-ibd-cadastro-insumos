﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="OndeEstamos.aspx.cs" Inherits="HTMSiteAdmin.Web.en.OndeEstamos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".itemNews a:odd").css("color", "#797949");
            $(".itemNews a:odd").hover(function () { $(this).css("color", "#809e25"); }, function () { $(this).css("color", "#797949"); });

            initialize();

        });

        function initialize() {
            var latlng = new google.maps.LatLng(-22.90347, -48.44084);
            var myOptions = { zoom: 16, center: latlng, mapTypeId: google.maps.MapTypeId.ROADMAP };
            var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            var image = 'media/img/iconMapa.png';
            var marker = new google.maps.Marker({ position: latlng, map: map, title: "IBD Certifications", icon: image });
            var contentString = '<span style="display:block; line-height:19px; font-size:14px; color:#111;">IBD Certifications</span><span style="display:block; overflow:hidden; height:24px; font-size:11px; line-height:12px; color:#666;">Rua Amando de Barros, 2275 <br>Botucatu - SP - Brazil</span>';
            var infowindow = new google.maps.InfoWindow({ content: contentString });
            infowindow.open(map, marker);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Our Location</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <div>
            <p style="font-size: 15px;">
                IBD Certification is headquartered in the municipality of <strong>Botucatu</strong>,
                in the state of <strong><u>São Paulo,</u> in Brazil</strong>.
                <br />
                <br />
                The address is <strong><u>Rua Amando de Barros, 2275</u></strong> in the city center.</p>
        </div>
        <br />
        <div id="map_canvas" style="width: 717px; height: 383px; border: solid 4px #fff;">
        </div>
    </div>
    <!--quemsomos-->
</asp:Content>
