$(document).ready(function(){
	
	//Load Produtos
	$("#carregando").hide();
	$("#destino").load('listagem.php?id=<?=clean($_GET["id"]?>');
	
		$(".menuFiltro a").click(function(){
			
			var id = $(this).attr('href');
			$(id + " a").removeClass('ativo');
			$(this).addClass('ativo');
			
			
			var busca = $(this).attr('rel');
			$('html,body').stop().animate({scrollTop: 260, easing: 'easeOutBack'},1500, 
				
				function(){
					$("#destino").fadeTo(600,0);
					$("#carregando").delay(200).slideDown('400','easeOutBack', 
						function(){
							$("#destino").load(busca, 
							function(){
								$("#destino").delay(200).fadeTo(600,1,
									function(){
										$("#carregando").slideUp('100','easeOutBack');
									});
							});
						});	
				}); 
		});
	//Load Produtos*/
	
});
