﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Conteudos;

namespace HTMSiteAdmin.Web.en
{
       
    public partial class Noticias : System.Web.UI.Page
    {
        public int nrPagina
        {
            get
            {
                try { return int.Parse(Request.QueryString["pagina"].ToString()); }
                catch (Exception) { return 1; }
            }
        }
        private int pageIndex
        {
            get
            {
                try { return Convert.ToInt32(Request.QueryString["page_index"]); }
                catch (Exception) { return 0; }
            }
        }

        private List<CONTEUDO> _objResult { get; set; }
        private int totalRegistros { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var query = new ConteudoBo().Find(c => c.ID_CONTEUDO_SITUACAO == 1 && c.ID_CONTEUDO_TIPO == 1 && !string.IsNullOrEmpty(c.TITULO_EN)).OrderByDescending(o => o.DATA_PUBLICACAO);
            _objResult = query.Skip(((nrPagina *20) -20)).Take(20).ToList();
            totalRegistros = query.Count();

            dtNoticias.DataSource = _objResult;
            dtNoticias.DataBind();

            Paginacao();
        }

        private void Paginacao()
        {
            string formattedUrl = Request.Url.ToString().Replace("?pagina=" + nrPagina, "");
            //Descobrindo o total de páginas
            int totalPaginas = 1;

            if (totalRegistros > 20)
                totalPaginas = (totalRegistros / 20) + 1;

            //Se o total de página for igual a 1, não precisa exibir o paginador
            if (totalPaginas == 1)
                pnlPaginadorTopo.Visible = pnlPaginador.Visible = false;

            //Somente se o total de páginas for MAIOR que 1, o paginador será exibido
            else if (totalPaginas > 1)
            {
                pnlPaginadorTopo.Visible = pnlPaginador.Visible = true;

                if (nrPagina == 1)
                    ltrAnteriorTopo.Text = ltrAnterior.Text = string.Format("<a href=\"{0}?pagina={1}\" class=\"anterior\">previous</a>", formattedUrl, 1);
                else
                    ltrAnteriorTopo.Text = ltrAnterior.Text = string.Format("<a href=\"{0}?pagina={1}\" class=\"anterior\">previous</a>", formattedUrl, nrPagina - 1);

                int nrPaginadores = (nrPagina - 5) < 0 ? 1 : (nrPagina - 5);
                ltrPaginasTopo.Text = ltrPaginas.Text = "";

                while (nrPaginadores <= (nrPagina + 5) && nrPaginadores <= totalPaginas)
                {
                    ltrPaginas.Text += string.Format("<a href=\"{0}?pagina={1}\" class=\"bt\">{1}</a>", formattedUrl, nrPaginadores);
                    nrPaginadores++;
                }
                ltrPaginasTopo.Text = ltrPaginas.Text;

                if (totalPaginas == nrPagina)
                    ltrProximaTopo.Text = ltrProxima.Text = string.Format("<a href=\"{0}?pagina={1}\" class=\"proxima\">next</a>", formattedUrl, nrPagina);
                else
                    ltrProximaTopo.Text = ltrProxima.Text = string.Format("<a href=\"{0}?pagina={1}\" class=\"proxima\">next</a>", formattedUrl, nrPagina + 1);
            }

            ltrContagemPaginacao.Text = string.Format("page {0}/{1} - listing {2}/{3}",
                nrPagina
                , totalPaginas
                , ((nrPagina *20) -20) + _objResult.Count
                , totalRegistros);
        }

        protected void dtNoticias_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            ((Literal)e.Item.FindControl("ltrLinkTopo")).Text = string.Format("<a href=\"NoticiasDetalhes.aspx?id_conteudo={0}\"></a>", ((CONTEUDO)e.Item.DataItem).ID_CONTEUDO.ToString());

            if (((CONTEUDO)e.Item.DataItem).DATA_PUBLICACAO.HasValue)
            {
                ((Literal)e.Item.FindControl("ltrDia")).Text = ((CONTEUDO)e.Item.DataItem).DATA_PUBLICACAO.Value.Day.ToString("00");
                ((Literal)e.Item.FindControl("ltrMes")).Text = ((CONTEUDO)e.Item.DataItem).DATA_PUBLICACAO.Value.ToString("MMM");
                ((Literal)e.Item.FindControl("ltrAno")).Text = ((CONTEUDO)e.Item.DataItem).DATA_PUBLICACAO.Value.Year.ToString();
            }
            else
            {
                ((Literal)e.Item.FindControl("ltrDia")).Text = ((CONTEUDO)e.Item.DataItem).SESSAO.DATA_INICIO.Day.ToString("00");
                ((Literal)e.Item.FindControl("ltrMes")).Text = ((CONTEUDO)e.Item.DataItem).SESSAO.DATA_INICIO.ToString("MMM");
                ((Literal)e.Item.FindControl("ltrAno")).Text = ((CONTEUDO)e.Item.DataItem).SESSAO.DATA_INICIO.Year.ToString();
            }

            ((Literal)e.Item.FindControl("ltrCategoria")).Text = new ConteudoCategoriaBo().Find(cc => cc.ID_CONTEUDO_CATEGORIA == ((CONTEUDO)e.Item.DataItem).ID_CONTEUDO_CATEGORIA).First().NOME;
            ((Literal)e.Item.FindControl("ltrLink")).Text = string.Format("<a href=\"NoticiasDetalhes.aspx?id_conteudo={0}\">{1}</a>", ((CONTEUDO)e.Item.DataItem).ID_CONTEUDO.ToString(), ((CONTEUDO)e.Item.DataItem).TITULO_EN);
        }
    }
}