﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="Bonsucro.aspx.cs" Inherits="HTMSiteAdmin.Web.en.Bonsucro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Socio-Environmental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px;
                    margin-right: 6px; margin-left: 6px;" />
                Bonsucro</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoBonsucro.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                About the Seal</h2>
            Bonsucro is a “multi-stakeholder” association created to reduce the environmental
            and social impacts of the sugarcane production through the development of standards
            and a certification program for transforming the sugarcane industry. This is an
            industry initiative in which Bonsucro has been working to unite stakeholders and
            develop a means for reaching its objectives within the sugarcane market. WWF oversees
            and supports this protocol.
            <br />
            <br />
            The Bonsucro standard was created with the objective of supplying a mechanism for
            reaching sustainable production for sugarcane and its derivatives, with regards
            to economic, social, and environmental aspects of the activity. It incorporates
            a conjunction of Principles, Criteria, Indicators and Verifiers that are used to
            certify sugar cane producers, instruct companies that intend to acquire sustainable
            raw material/supplies, as well as provide the financial sector with a tool for carrying
            out sustainable investments.
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Criteria</h2>
            The standard is based on a conjuncture of measurement tools that permit the aggregation
            of information and transparency regarding the impacts caused by the sector. Although
            the certification unit is the sugar factory, audit evaluations are carried out for
            the factory, as well as for the productive areas that supply the factories with
            raw materials.
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                How to Certify</h2>
            In order for a company to obtain the Bonsucro certificate through IBD it should
            first become registered as a member of Bonsucro (www.bonsucro.com), and then follow
            the following steps in the certification process:<br />
            <br />
            - Pre-audit: Optional step allowing the operation to visualize its position in relation
            to the Bonsucro standards and necessary actions for obtaining certification;<br />
            <br />
            - Documental Audit: Step carried out prior to the on-site audit held at the productive
            unit. This step is intended to verify company compliance with Bonsucro standards
            through the verification of documentation requested for review.
            <br />
            <br />
            - Site audit at the production unit: Every three years there IBD carries out a complete
            audit at the sugarcane processing and production facilities. During the interim,
            complementary yearly audits are carried out in order to verify traceability and
            the resolution of non-compliances identified in the prior year. In the rare case
            that a company fails to comply with the Bonsucro standards, the company will be
            subject to a complete audit during the interim period.
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Additional Information</h2>
            IBD clients that produce sugarcane and other products derived from sugarcane will
            be provided with differentiated cost estimates. This program also applies to conventional
            sugarcane producers.
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </p>
        <br />
        <br />
        <br />
    </div>
    <!--quemsomos-->
</asp:Content>
