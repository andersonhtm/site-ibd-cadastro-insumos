﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="ServicosAprovacoesInsumos.aspx.cs" Inherits="HTMSiteAdmin.Web.en.ServicosAprovacoesInsumos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Services
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Material Input Approval</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/imagem_interna_02.jpg" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <p>
                IBD Certifications has created a program for approving inputs used in organic agriculture
                in order to evaluate the possible use of commercially available inputs in accordance
                with the primary organic production standards (USA, European, IFOAM, Japanese, Canadian
                and Brazilian). IBD has created its own unique standard and procedures for guaranteeing
                the safety, credibility and reliability of approved inputs and of producers and
                companies interested in using these inputs.<br />
                <br />
                In 2009, IBD achieved ISO 65 accreditation through the IOAS with the objective of
                adding greater credibility to the materials input approval program and distinguishing
                it as one of the few input certification programs worldwide to have ISO 65 accreditation.<br />
                <br />
                Through this program, producers obtain an important research tool for identifying
                the principal inputs for their operations, guaranteeing the sustainability and success
                of their organic production activities.<br />
                <br />
                The Approval Program, geared towards manufacturers, importers and distributors of
                inputs located in Brazil and worldwide, evaluates inputs in accordance with organic
                agricultural production and processing standards.
            </p>
            <br />
            <br />
            <br />
            <br />
            <br />
    </div>
    <!--quemsomos-->
</asp:Content>
