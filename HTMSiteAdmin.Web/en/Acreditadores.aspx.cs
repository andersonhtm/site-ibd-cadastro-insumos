﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Business.Links;
using HTMSiteAdmin.Data;

namespace HTMSiteAdmin.Web.en
{
    public partial class Acreditadores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dtAcreditadores.DataSource = new LinkBo().Find(lbda => lbda.ID_LINK_TIPO == 2 && lbda.ID_LINK_SITUACAO == 1).OrderByDescending(lbda => lbda.SESSAO.DATA_INICIO);
            dtAcreditadores.DataBind();
        }

        protected void dtAcreditadores_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (((LINK)e.Item.DataItem).ID_ARQUIVO.HasValue)
                ((Literal)e.Item.FindControl("ltrImagem")).Text = "<div class=\"imagem\"><a href=\"" + ((LINK)e.Item.DataItem).CAMINHO_EN + "\"><img src=\"" + ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", ((LINK)e.Item.DataItem).ID_ARQUIVO_EN.ToString())) + "\" alt=\"\" /></a></div>";
            else
                ((Literal)e.Item.FindControl("ltrImagem")).Text = string.Empty;

            ((Literal)e.Item.FindControl("ltrNome")).Text = "<a href=\"" + ((LINK)e.Item.DataItem).CAMINHO_EN + "\">" + ((LINK)e.Item.DataItem).DESCRICAO_EN + "</a>";
            ((Literal)e.Item.FindControl("ltrDetalhes")).Text = ((LINK)e.Item.DataItem).DESCRICAO_DET_EN;
        }
    }
}