﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="Acreditadores.aspx.cs" Inherits="HTMSiteAdmin.Web.en.Acreditadores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Accreditation agencies</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="clientes">
        <asp:DataList runat="server" ID="dtAcreditadores" OnItemDataBound="dtAcreditadores_ItemDataBound">
            <ItemTemplate>
                <div id="01" class="listaParceiros">
                    <asp:Literal ID="ltrImagem" Text="ltrImagem" runat="server" />
                    <!--imagem-->
                    <div class="texto">
                        <h3>
                            <asp:Literal ID="ltrNome" Text="ltrNome" runat="server" /></h3>
                        <p>
                            <asp:Literal ID="ltrDetalhes" Text="ltrDetalhes" runat="server" /></p>
                    </div>
                    <!--texto-->
                </div>
            </ItemTemplate>
        </asp:DataList>
        <!--listaParceiros-->
    </div>
    <!--clientes-->
</asp:Content>
