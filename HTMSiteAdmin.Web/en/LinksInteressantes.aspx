﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="LinksInteressantes.aspx.cs" Inherits="HTMSiteAdmin.Web.en.LinksInteressantes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Interesting Links</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="links">
        <div>
            <p style="color: #595941; font-size: 15px;">
                In this section you will have access to interesting and useful links:</p>
        </div>
        <asp:DataList ID="dtLinksInteressantes" runat="server" OnItemDataBound="dtLinksInteressantes_ItemDataBound"
            RepeatLayout="Flow" ShowFooter="False" ShowHeader="False">
            <ItemTemplate>
                <h3>
                    <asp:Literal ID="ltrCategoria" Text="ltrCategoria" runat="server" /></h3>
                <asp:DataList ID="dtLinks" runat="server" RepeatLayout="Flow" ShowFooter="False"
                    ShowHeader="False">
                    <ItemTemplate>
                        <div class="itemLinks">
                            <asp:Literal ID="ltrImageLink" Text="ltrImageLink" runat="server" />
                            <div class="texto">
                                <asp:Literal ID="ltrLink" Text="ltrLink" runat="server" />
                                <h4>
                                    <asp:Literal ID="ltrLinkTitle" Text="ltrLinkTitle" runat="server" /></h4>
                                <p>
                                    <asp:Literal ID="ltrLinkName" Text="ltrLinkName" runat="server" /></p>
                            </div>
                            <!--texto-->
                        </div>
                        <!--itemLinks-->
                    </ItemTemplate>
                </asp:DataList>
            </ItemTemplate>
        </asp:DataList>
    </div>
    <!--links-->
</asp:Content>
