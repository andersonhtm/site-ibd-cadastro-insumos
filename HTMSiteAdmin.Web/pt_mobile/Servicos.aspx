﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="Servicos.aspx.cs" Inherits="HTMSiteAdmin.Web.pt_mobile.Servicos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="interna servicos">

        <h3 class="tituloGeral">Serviços</h3>

        <img src="../media_mobile/img/servicos/1.jpg" alt="" width="280" height="100" />

        <h4 class="tituloInterno">Certificação</h4>

        <p>
            Para atender às exigências da certificação, o IBD conta com uma equipe especializada de inspetores que fiscalizam as propriedades agrícolas e os processos de produção para verificar se o produto está sendo cultivado e/ou processado de acordo com as normas de produção orgânicas e biodinâmicas.
        <br />
            <br />
            O processo de certificação tem uma importância fundamental na viabilização da agricultura orgânica, sendo uma importante ferramenta no processo de desenvolvimento da consciência ecológica e social. Nesse sentido, o IBD, ciente de sua responsabilidade enquanto agente de transformação social vem financiando apoia projetos de pesquisas no campo agrícola e de assessoria e acompanhamento de projetos de pequenos agricultores.
        <br />
            <br />
            Exercendo um monitoramento constante, o IBD atua promovendo o equilíbrio entre a atividade econômica e a preservação da natureza. Nos protocolos de certificação socioambiental, grandes projetos certificados possuem programas de reflorestamento e de proteção à vida selvagem, como por exemplo, a manutenção de viveiros com essências nativas para serem utilizadas na recomposição de matas ciliares, corredores ecológicos e proteção de recursos hídricos, programas de prevenção a incêndios em áreas de vegetação nativa e a criação de espécies animais com risco de extinção para posterior devolução ao seu habitat.

        </p>

        <div class="clear"></div>

        <img src="../media_mobile/img/servicos/2.jpg" alt="" width="280" height="100" />

        <h4 class="tituloInterno">Auditoria</h4>

        <p>
            O IBD Certificações criou o Programa de Aprovação de Insumos no intuito de avaliar a possibilidade de uso dos insumos comerciais disponíveis no mercado de acordo com as principais diretrizes de produção orgânica (Normas EUA, Européia, IFOAM, Japonesa, Canadense e Brasileira). Possui uma diretriz e procedimentos próprios e únicos a nível mundial que garantem segurança, credibilidade e confiabilidade aos insumos aprovados e aos produtores e empresas interessadas no uso.
        <br />
            <br />
            O Programa de Aprovação avalia os insumos de acordo com as normas de produção agrícola, processamento de alimentos e pecuária orgânica, sendo destinado a fabricantes, importadores e distribuidores de insumos localizados no Brasil e no exterior.
        </p>

        <div class="clear"></div>

        <img src="../media_mobile/img/servicos/3.jpg" alt="" width="280" height="100" />

        <h4 class="tituloInterno">Cursos e Treinamentos</h4>

        <p>
            O IBD promove cursos próprios para seus inspetores.
            <br />
            <br />
            Além disso promove cursos abertos para técnicos e candidatos a serem auditores.
            <br />
            <br />
            Entre em contato para maiores informações e saber quando será o próximo curso ou consulte a Agenda de Eventos. <a href="Eventos.aspx">[clique aqui]</a>
        </p>

        <h4 class="tituloInterno">Mapas e Geoprocessamento</h4>

        <div style="display: flex; flex-direction: column; align-items: center">
            <img src="../Media/img/mapa-geo-detalhes-1.png" alt="" style="margin: 15px auto; text-align: center" />
            <img src="../Media/img/seta.png" alt="" style="transform: rotate(90deg); max-height: 28px" />
            <img src="../Media/img/mapa-geo-detalhes-2.png" alt="" style="margin: 15px auto; text-align: center" />
        </div>

        <p>
            O  geoprocessamento é a tecnologia capaz de espacializar e cruzar, geograficamente, 
            informações de diversas fontes. Através de camadas de informações que podem ser estudados 
            e analisados permite uma melhor compreensão de como os fatores geográficos interferem nas 
            atividades produtivas e cotidianas.
            O IBD dispõe de uma variedade de serviços e aplicações em geoprocessamento e sistema de 
            informação geográfica. Acesse o link abaixo e veja o portfólio de produtos e serviços 
            que o IBD oferece.
        </p>
        <a href="http://ibd.com.br/pt/ServicosMapeamentoGeoprocessamentoDetalhes.aspx">Acesse Aqui</a>


    </div>
    <!--interna-->
</asp:Content>
