﻿using HTMSiteAdmin.Business.Banners;
using HTMSiteAdmin.Web.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HTMSiteAdmin.Web.pt_mobile
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CarregarBannerTopo();
        }        
       
        private void CarregarBannerTopo()
        {
            try
            {
                var _objBanner1 = new BannerBo().Find(lbda => lbda.ID_BANNER_POSICAO == 9 && lbda.ID_BANNER_SITUACAO == 1).FirstOrDefault();
                var _objBanner2 = new BannerBo().Find(lbda => lbda.ID_BANNER_POSICAO == 10 && lbda.ID_BANNER_SITUACAO == 1).FirstOrDefault();
                var _objBanner3 = new BannerBo().Find(lbda => lbda.ID_BANNER_POSICAO == 11 && lbda.ID_BANNER_SITUACAO == 1).FirstOrDefault();

                if (_objBanner1 != null)
                {
                    var banner = _objBanner1;
                    ctnBannerTopo1.Visible = true;
                    litTituloBanner1.Text = banner.TITULO;
                    linkBanner1.NavigateUrl = string.IsNullOrEmpty(banner.CAMINHO) ? "#" : banner.CAMINHO;
                    imgBannerTopo1.ImageUrl = ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", banner.ID_ARQUIVO));
                }

                if (_objBanner2 != null)
                {
                    var banner = _objBanner2;
                    ctnBannerTopo2.Visible = true;
                    litTituloBanner2.Text = banner.TITULO;
                    linkBanner2.NavigateUrl = string.IsNullOrEmpty(banner.CAMINHO) ? "#" : banner.CAMINHO;
                    imgBannerTopo2.ImageUrl = ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", banner.ID_ARQUIVO));
                }

                if (_objBanner3 != null)
                {
                    var banner = _objBanner3;
                    ctnBannerTopo3.Visible = true;
                    litTituloBanner3.Text = banner.TITULO;
                    linkBanner3.NavigateUrl = string.IsNullOrEmpty(banner.CAMINHO) ? "#" : banner.CAMINHO;
                    imgBannerTopo3.ImageUrl = ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", banner.ID_ARQUIVO));
                }

            } catch (Exception ex) {
                ctnBanners.Visible = false;
            }
        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder corpoEmail = new StringBuilder();
                corpoEmail.AppendLine("Atendimento: CADASTRE-SE<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("Data Hora: " + DateTime.Now.ToString() + "<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("Nome: " + txtCadastreSeNome.Text + "<br />");
                corpoEmail.AppendLine("E-mail: " + txtCadastreSeEmail.Text + "<br />");
                Util.EnviarEmail(corpoEmail.ToString(), "IBD Certificações - Cadastre-se", Configuracao.REGISTER_EMAILTO, "", Configuracao.MAIL_BLINDCOPYTO);

                corpoEmail = new StringBuilder();
                corpoEmail.AppendLine("Prezado (a): " + txtCadastreSeNome.Text + "<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("Você se cadastrou no site do IBD para receber informações.<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("Data: " + DateTime.Now.ToString() + "<br />");
                corpoEmail.AppendLine("E-mail: " + txtCadastreSeEmail.Text + "<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("Atenciosamente,<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("Equipe IBD<br />");
                corpoEmail.AppendLine(Configuracao.REGISTER_EMAILREPLY + "<br />");
                Util.EnviarEmail(corpoEmail.ToString(), "IBD Certificações - Cadastre-se", txtCadastreSeEmail.Text, "", Configuracao.MAIL_BLINDCOPYTO);

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "cadastresealert", "alert('E-mail cadastrado com sucesso.');", true);
                txtCadastreSeEmail.Text = txtCadastreSeNome.Text = "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
    }
}