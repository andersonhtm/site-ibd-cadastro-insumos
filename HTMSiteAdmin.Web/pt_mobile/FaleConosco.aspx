﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="FaleConosco.aspx.cs" Inherits="HTMSiteAdmin.Web.pt_mobile.FaleConosco" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="interna faleconosco">
    
    	<h3 class="tituloGeral">Fale Conosco</h3>
        
		<h4 class="tituloInterno" style="border-top:none; padding-top:0px;">Telefone</h4>
        <span class="telefone">+55 (14) 3811 9800</span>
        
        <h4 class="tituloInterno">Fax</h4>        
        <span class="telefone">+55 (14) 3811 9801</span>
        
        <h4 class="tituloInterno">Email</h4>
        <span class="email">ibd@ibd.com.br</span>
        
        <h4 class="tituloInterno">Endereço</h4>
        <span class="email">
        Rua Amando de Barros, 2275 - Centro<br />
        CEP: 18.602.150  Botucatu - SP
        </span>
        
        
            
        <div class="clear"></div>
        


        
        <fieldset>

             <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="sumario-erros" ForeColor="Red" DisplayMode="BulletList" />

            <asp:Label ID="Label1" runat="server" Text="Assunto" AssociatedControlID="ddlAssunto"></asp:Label>
            <asp:DropDownList runat="server" ID="ddlAssunto">
                <asp:ListItem Text="Certifique-se pelo IBD" Selected="True" />
                <asp:ListItem Text="Dúvidas e Suporte" />
                <asp:ListItem Text="Departamento Financeiro" />

            </asp:DropDownList>

            <asp:Label ID="Label2" runat="server" Text="Nome" AssociatedControlID="txtNome"></asp:Label>
            <asp:TextBox ID="txtNome" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Nome obrigatório" ControlToValidate="txtNome"></asp:RequiredFieldValidator>            

            <asp:Label ID="Label3" runat="server" Text="E-mail *" AssociatedControlID="txtEmail"></asp:Label>
            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail" ErrorMessage="E-mail obrigatório"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator Display="None" SetFocusOnError="true" ID="RegularExpressionValidator1" runat="server" ErrorMessage="E-mail inválido" ControlToValidate="txtEmail" ValidationExpression="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"></asp:RegularExpressionValidator>

            <asp:Label ID="Label4" runat="server" Text="Telefone" AssociatedControlID="txtTelefone"></asp:Label>
            <asp:TextBox ID="txtTelefone" runat="server" style="width:110px"></asp:TextBox>            

            <asp:Label ID="Label5" runat="server" Text="Mensagem *" AssociatedControlID="txtMensagem"></asp:Label>
            <asp:TextBox ID="txtMensagem" TextMode="MultiLine" runat="server"></asp:TextBox>            
            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Mensagem obrigatória" ControlToValidate="txtMensagem"></asp:RequiredFieldValidator>
            
            <asp:LinkButton CssClass="enviar" OnClick="btnEnviar_Click" ID="btnEnviar" runat="server">enviar »</asp:LinkButton>               
                        
        </fieldset>

        
        
        <h4 class="tituloCertificaoInterno">Reclamação</h4>
        
            <ul>
            	<li><span>1</span>Caso deseje reportar uma reclamação e/ou denuncia, por gentileza, utilize o link: <br />
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSf3O2KEgYomA7bCBfBtm2wPXydpEV6nmNqShb366AlozQKpkw/viewform">docs.google.com/forms/d/e/1FAIpQLSf3O2KEgYomA7bCBfBtm2wPXydpEV6nmNqShb366AlozQKpkw/viewform</a></li>
                <li><span>2</span>Será informado o número do protocolo ao contato indicado.</li>
                <li><span>3</span>Todas as reclamações ou denúncias deverão ser processadas no prazo máximo de 60 dias. Devido ao processos certificação e ações necessárias de esclarecimento o processo poderá se alongar. O reclamante/denunciante será informado do status de sua reclamação.</li>
            </ul>
        
        
</div><!--interna-->
</asp:Content>
