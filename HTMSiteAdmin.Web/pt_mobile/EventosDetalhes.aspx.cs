﻿using HTMSiteAdmin.Business.Eventos;
using HTMSiteAdmin.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HTMSiteAdmin.Web.pt_mobile
{
    public partial class EventosDetalhes : System.Web.UI.Page 
    {
        private int? idConteudo
        {
            get
            {
                try { return Convert.ToInt32(Request.QueryString["id_conteudo"]); }
                catch (Exception) { return null; }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (idConteudo.HasValue && idConteudo.Value > 0)
            {
                EVENTO _objEvento = new EventoBo().Find(c => c.ATIVO && c.ID_EVENTO == idConteudo.Value).FirstOrDefault();

                if (_objEvento == null)
                    return;

                if (_objEvento.DATA_INICIO.Value.Year.Equals(_objEvento.DATA_FIM.Value.Year)
                && _objEvento.DATA_INICIO.Value.Month.Equals(_objEvento.DATA_FIM.Value.Month))
                {
                    ltrPeriodo.Text =
                        string.Format("de {0} a {1} de {2} de {3}",
                        _objEvento.DATA_INICIO.Value.Day.ToString("00"),
                        _objEvento.DATA_FIM.Value.Day.ToString("00"),
                        HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objEvento.DATA_FIM.Value.Month).ToLower(),
                        _objEvento.DATA_FIM.Value.Year);
                }
                else if (_objEvento.DATA_INICIO.Value.Year.Equals(_objEvento.DATA_FIM.Value.Year)
                    && !_objEvento.DATA_INICIO.Value.Month.Equals(_objEvento.DATA_FIM.Value.Month))
                {
                    ltrPeriodo.Text =
                        string.Format("de {0} de {1} a {2} de {3} de {4}",
                        _objEvento.DATA_INICIO.Value.Day.ToString("00"),
                        HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objEvento.DATA_INICIO.Value.Month).ToLower(),
                        _objEvento.DATA_FIM.Value.Day.ToString("00"),
                        HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objEvento.DATA_FIM.Value.Month).ToLower(),
                        _objEvento.DATA_FIM.Value.Year);
                }
                else
                {
                    ltrPeriodo.Text =
                        string.Format("de {0} de {1} de {2} a {3} de {4} de {5}",
                        _objEvento.DATA_INICIO.Value.Day.ToString("00"),
                        HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objEvento.DATA_INICIO.Value.Month).ToLower(),
                        _objEvento.DATA_INICIO.Value.Year,
                        _objEvento.DATA_FIM.Value.Day.ToString("00"),
                        HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objEvento.DATA_FIM.Value.Month).ToLower(),
                        _objEvento.DATA_FIM.Value.Year);
                }

                if (_objEvento.DATA_PUBLICACAO.HasValue)
                    ltrDataPublicacao.Text = _objEvento.DATA_PUBLICACAO.Value.ToShortDateString();
                else
                    ltrDataPublicacao.Text = _objEvento.SESSAO.DATA_INICIO.ToShortDateString();
                
                ltrTitulo.Text = _objEvento.DESCRICAO;
                ltrConteudo.Text = _objEvento.DESCRICAO_DETALHADA;
            }
        }
    }
}