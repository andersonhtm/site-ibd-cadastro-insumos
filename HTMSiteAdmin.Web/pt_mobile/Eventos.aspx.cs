﻿using HTMSiteAdmin.Business.Eventos;
using HTMSiteAdmin.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HTMSiteAdmin.Web.pt_mobile
{
    public partial class Eventos : System.Web.UI.Page
    {

        private struct strAnoMes
        {
            public int Ano;
            public int Mes;
        }

        private int? id_categoria
        {
            get
            {
                try { return Convert.ToInt32(Request.QueryString["id_categoria"]); }
                catch (Exception) { return null; }
            }
            set { }
        }

        IQueryable<EVENTO> _objEventosCollection;
        List<strAnoMes> _objDataSource;

        protected void Page_Load(object sender, EventArgs e)
        {
            CarregaDadosEventos();
        }

        private void CarregaDadosEventos()
        {
            HashSet<int> lstAnosComEventos = new HashSet<int>();
            _objDataSource = new List<strAnoMes>();

            if (id_categoria.HasValue && id_categoria > 0)
                _objEventosCollection = new EventoBo().Find(c => c.ATIVO && c.ID_EVENTO_CATEGORIA == id_categoria && (c.DATA_INICIO > DateTime.Today)).OrderByDescending(lbda => lbda.DATA_FIM);
            else
                _objEventosCollection = new EventoBo().Find(c => c.ATIVO && (c.DATA_INICIO > DateTime.Today)).OrderByDescending(lbda => lbda.DATA_FIM);

            if (_objEventosCollection.Count() > 0)
            {

                int MaiorAno = _objEventosCollection.Max(lbda => lbda.DATA_FIM.Value.Year);
                lstAnosComEventos.Add(MaiorAno);

                int MenorAno = _objEventosCollection.Min(lbda => lbda.DATA_FIM.Value.Year);

                if (!MenorAno.Equals(MaiorAno))
                {
                    lstAnosComEventos.Add(MenorAno);

                    while (MenorAno < MaiorAno)
                    {
                        if (_objEventosCollection.Count(lbda => lbda.DATA_FIM.Value.Year.Equals(MenorAno)) > 0)
                            lstAnosComEventos.Add(MenorAno);

                        MenorAno++;
                    }
                }

                foreach (int Ano in lstAnosComEventos)
                {
                    int MaiorMes = _objEventosCollection.Max(lbda => lbda.DATA_FIM.Value.Month);
                    int MenorMes = _objEventosCollection.Min(lbda => lbda.DATA_FIM.Value.Month);

                    if (!MenorMes.Equals(MaiorMes))
                    {
                        while (MenorMes <= MaiorMes)
                        {
                            if (_objEventosCollection.Count(lbda => lbda.DATA_FIM.Value.Month.Equals(MenorMes)) > 0
                            && _objDataSource.Count(lbda => lbda.Ano.Equals(Ano) && lbda.Mes.Equals(MenorMes)) == 0)
                                _objDataSource.Add(new strAnoMes() { Ano = Ano, Mes = MenorMes });

                            MenorMes++;
                        }
                    }
                    else
                    {
                        if (_objEventosCollection.Count(lbda => lbda.DATA_FIM.Value.Month.Equals(MenorMes)) > 0
                            && _objDataSource.Count(lbda => lbda.Ano.Equals(Ano) && lbda.Mes.Equals(MenorMes)) == 0)
                            _objDataSource.Add(new strAnoMes() { Ano = Ano, Mes = MenorMes });
                    }
                }

                rptEventos.DataSource = _objDataSource;
                rptEventos.DataBind();
            }
            else
            {
                litEventos.Visible = true;
            }
        }


        protected void rptEventos_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer)
                return;



            ((Repeater)e.Item.FindControl("rptListaEventos")).ItemDataBound += new RepeaterItemEventHandler(rptListaEventos_ItemDataBound);
            ((Repeater)e.Item.FindControl("rptListaEventos")).DataSource = new EventoBo().Find(lbda => lbda.ATIVO && lbda.DATA_FIM.Value.Year.Equals(((strAnoMes)e.Item.DataItem).Ano) && lbda.DATA_FIM.Value.Month.Equals(((strAnoMes)e.Item.DataItem).Mes)).OrderByDescending(lbda => lbda.DATA_FIM);
            ((Repeater)e.Item.FindControl("rptListaEventos")).DataBind();
        }

        string tituloMes = DateTime.Now.ToString("y");

        protected void rptListaEventos_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            EVENTO _objEvento = (EVENTO)e.Item.DataItem;
            string dataEvento;

            if (!tituloMes.Equals(_objEvento.DATA_INICIO.Value.ToString("y")))
            {
                ((Literal)e.Item.FindControl("litTituloMes")).Text = "<h5 class=\"tituloMes\">" + _objEvento.DATA_INICIO.Value.ToString("y") + "</h5>";
            }
            tituloMes = _objEvento.DATA_INICIO.Value.ToString("y");

            if (_objEvento.DATA_INICIO.Value.Year.Equals(_objEvento.DATA_FIM.Value.Year)
                && _objEvento.DATA_INICIO.Value.Month.Equals(_objEvento.DATA_FIM.Value.Month))
            {
                dataEvento =
                    string.Format("de {0} a {1} de {2} de {3}",
                    _objEvento.DATA_INICIO.Value.Day.ToString("00"),
                    _objEvento.DATA_FIM.Value.Day.ToString("00"),
                    HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objEvento.DATA_FIM.Value.Month).ToLower(),
                    _objEvento.DATA_FIM.Value.Year);
            }
            else if (_objEvento.DATA_INICIO.Value.Year.Equals(_objEvento.DATA_FIM.Value.Year)
                && !_objEvento.DATA_INICIO.Value.Month.Equals(_objEvento.DATA_FIM.Value.Month))
            {
                dataEvento =
                    string.Format("de {0} de {1} a {2} de {3} de {4}",
                    _objEvento.DATA_INICIO.Value.Day.ToString("00"),
                    HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objEvento.DATA_INICIO.Value.Month).ToLower(),
                    _objEvento.DATA_FIM.Value.Day.ToString("00"),
                    HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objEvento.DATA_FIM.Value.Month).ToLower(),
                    _objEvento.DATA_FIM.Value.Year);
            }
            else
            {
                dataEvento =
                    string.Format("de {0} de {1} de {2} a {3} de {4} de {5}",
                    _objEvento.DATA_INICIO.Value.Day.ToString("00"),
                    HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objEvento.DATA_INICIO.Value.Month).ToLower(),
                    _objEvento.DATA_INICIO.Value.Year,
                    _objEvento.DATA_FIM.Value.Day.ToString("00"),
                    HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objEvento.DATA_FIM.Value.Month).ToLower(),
                    _objEvento.DATA_FIM.Value.Year);
            }

            ((Literal)e.Item.FindControl("ltrLinkNome")).Text = string.Format("<a href=\"EventosDetalhes.aspx?id_conteudo={0}\">{1} <span>{2}</span></a>", _objEvento.ID_EVENTO, _objEvento.DESCRICAO, dataEvento);

        }
    }
}