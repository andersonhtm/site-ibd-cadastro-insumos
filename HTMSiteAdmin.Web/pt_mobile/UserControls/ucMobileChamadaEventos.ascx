﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucMobileChamadaEventos.ascx.cs" Inherits="HTMSiteAdmin.Web.pt_mobile.UserControls.ucChamadaEventos" %>
<asp:DataList ID="dtUltimasNoticias" runat="server" DataKeyField="ID_EVENTO" OnItemDataBound="dtUltimasNoticias_ItemDataBound" CellPadding="0" RepeatLayout="Flow" ShowFooter="False" ShowHeader="False">
    <ItemTemplate>
        <div class="item">
            <asp:Literal ID="ltrLinkNoticia" runat="server" Text="<a href='EventosDetalhes.aspx?id_conteudo={0}\'>Link</a>" />
        </div>
    </ItemTemplate>
</asp:DataList>