﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucMobileUltimasNoticias.ascx.cs" Inherits="HTMSiteAdmin.Web.pt_mobile.UserControls.ucUltimasNoticias" %>

<asp:Repeater ID="rptUltimasNoticias" runat="server" OnItemDataBound="rptUltimasNoticias_ItemDataBound">
    <ItemTemplate>
        <asp:Literal ID="ltrLinkNoticia" runat="server" Text="" /> 
    </ItemTemplate>
</asp:Repeater>

<asp:Panel ID="pnlPaginacao" runat="server">
    <div class="paginacao">
        <span><asp:Literal ID="ltrContagemPaginacao" Text="ltrContagemPaginacao" runat="server" /></span>

        <asp:Panel runat="server" ID="pnlPaginador">
            <div class="nav">
                <asp:Literal ID="ltrAnterior" Text="ltrAnterior" runat="server" /> 
                <asp:Literal ID="ltrPaginas" Text="ltrPaginas" runat="server" /> 
                <asp:Literal ID="ltrProxima" Text="ltrProxima" runat="server" /> 
            </div>
        </asp:Panel>
    </div>
</asp:Panel>

