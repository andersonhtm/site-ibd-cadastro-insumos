﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="Eventos.aspx.cs" Inherits="HTMSiteAdmin.Web.pt_mobile.Eventos" %>

<%@ Register Src="~/pt_mobile/UserControls/ucMobileChamadaEventos.ascx" TagPrefix="uc1" TagName="ucMobileChamadaEventos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">

    <div class="interna eventosLista">
        <h3 class="tituloGeral">Eventos</h3>

        <asp:Repeater ID="rptEventos" runat="server" OnItemDataBound="rptEventos_ItemDataBound">
            <ItemTemplate>                
                <asp:Repeater ID="rptListaEventos" runat="server" OnItemDataBound="rptListaEventos_ItemDataBound">
                    <ItemTemplate>
                        <asp:Literal ID="litTituloMes" Text="" runat="server" />
                        <div class="item">
                            <asp:Literal ID="ltrLinkNome" Text="ltrLinkNome" runat="server" />                            
                        </div>
                    </ItemTemplate>
                </asp:Repeater>               
            </ItemTemplate>
        </asp:Repeater>

        <asp:Panel ID="litEventos" runat="server" Visible="false"><p>Nenhum evento nos próximos meses</p></asp:Panel>

    </div>
    
</asp:Content>
