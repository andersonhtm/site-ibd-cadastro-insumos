﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="SejaIBD.aspx.cs" Inherits="HTMSiteAdmin.Web.pt_mobile.SejaIBD" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">
     <div class="interna sejaibd">
    
    	<h3 class="tituloGeral">Seja IBD</h3>
        
        <p>Com uma equipe especializada, com vasta experiência no mercado de certificações, oferecemos atendimento personalizado, com protocolos adequados às suas necessidades.<br /><br />Entre em contato com nosso atendimento, para obter maiores informações e receber os documentos para iniciar seu processo de certificação. Teremos prazer em atendê-lo.</p>
        
        <h4 class="tituloInterno">Conheça 10 motivos para se tornar cliente IBD:</h4>
        
        <ul>
        	<li><span>1</span>Em todo o mundo e também no Brasil o IBD goza de um nome ligado ao profissionalismo, seriedade e dedicação. Seus produtos serão aceitos globalmente com certificados IBD.</li>
            
            <li><span>2</span> Além de ser a maior certificadora para produtos orgânicos na América Latina estendeu seus serviços a protocolos afins e de sustentabilidade e recentemente obteve credenciamento para RSPO, UEBT e BONSUCRO, atestando a vanguarda e profissionalismo.</li>

            <li><span>3</span> O selo Fair Trade IBD de adequação social, ambiental e comercio ético para produtos orgânicos.</li>

            <li><span>4</span> Os orçamentos são claros, transparentes e permitem planejamento completo do cliente para investimento, estrutura e prazos.</li>

            <li><span>5</span> Cerca de 90% dos projetos certificados pelo IBD são de agricultores familiares. Sua estrutura permite a prática de preços acessíveis e diferenciados, adequados à situação econômica de cada produtor.</li>

            <li><span>6</span> O IBD tem a maior rede de inspetores, agora regionalizados, conferindo rapidez e preço mais acessível. A rede de inspetores continua crescendo, estando presentes na maioria dos estados brasileiros.</li>

            <li><span>7</span> Conta com uma rede de mais de 50 inspetores em mais de 20 países para certificação de produtos a serem importados ao Brasil pela lei Brasileira, 10.831 para produtos orgânicos.</li>

            <li><span>8</span> Forma os seus inspetores com cursos internos anualmente com palestras de especialistas e com conteúdos técnicos de auditoria, o que confere ao inspetor do IBD um nível técnico superior.</li>

            <li><span>9</span> Foi a pioneira na incorporação de questões sociais, baseadas nos acordos internacionais do trabalho, que serviram como modelo para outras certificadoras no exterior.</li>

            <li><span>10</span> Foi a pioneira a garantir a proteção das matas e dos recursos hídricos nos projetos certificados. A consequência foi o replantio de dezenas de milhares de essências nativas.</li>       
        </ul>
    
    </div><!--interna-->
</asp:Content>
