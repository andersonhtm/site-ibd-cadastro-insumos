﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="Certificados.aspx.cs" Inherits="HTMSiteAdmin.Web.en_mobile.Certificados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="interna certificados">
    
    	<h3 class="tituloGeral">Certification</h3>
            	
        <img src="../media_mobile/img/certificados/2.jpg" alt="" style="float:left;" />
    	<img src="../media_mobile/img/certificados/1.jpg" alt="" style="float:left;" />
        <img src="../media_mobile/img/certificados/3.jpg" alt="" />
        <h4 class="tituloInterno">IBD Organic</h4>
        <p> 
        <span class="subtit">About the Seal</span>
			The IBD Organic seal has two characteristics:<br />
			1) It applies to all organic certifications carried out by IBD Certifications for the internal market, used in conjunction with the Brazilian Seal for organic product.<br />
            2) Since the IBD standard is approved by European accreditation agencies as equivalent to the European rule, it applies to all certifications intended for the European Common Market.<br />
            3) Provides all certifications necessary for access to the North American Market (USA), with accreditation for the USDA NOP, the only standard applicable to this market.
		<span class="subtit">Standards Served</span>
			- IBD Standards<br />
            - IFOAM Standards<br />
            - USDA Standards<br />
            - CEE 834/2007<br />
            - Law 10.831 (SisOrg)
         <br />   
         <span class="subtit">Segments Served</span>
			- Agriculture<br />
            - Livestock<br />
            - Fibers<br />
            - Aquaculture<br />
            - Processing<br />
            - Material inputs<br />
            - Wild Harvesting<br />
            - Cosmetics<br />
            - Wine<br />
            - Cleaning products<br /><br />
			The use of the IFOAM ACCREDITED Seal is optional.
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/4.jpg" alt="" />
    	<h4 class="tituloInterno">USDA NOP Organic</h4>
        <p>
        <span class="subtit">About the Seal</span>
			The USDA Organic Seal is a North American Seal created by the United States Department of Agriculture (USDA) for the NOP- National Organic Program.<br /><br />
			This seal is offered with the objective of developing, maintaining and expanding access of  organic products from national production and from various countries to the North American market.
		<span class="subtit">Standards Served</span>
        - NOP<br />
        <span class="subtit">Segments Served</span>
        - Agriculture<br />
        - Livestock<br />
        - Fibers<br />
        - Processing<br />
        - Wild harvesting<br />
        - Cosmetics
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/5.jpg" alt="" />
    	<h4 class="tituloInterno">JAS Organic </h4>
        <p>
        <span class="subtit">About the Seal</span>
			The JAS Organic seal is the Japanese seal accredited by the Japanese Department of Agriculture. <br /><br />
	        This seal is offered with the objective of developing, maintaining, and expanding access of products from diverse countries to the Japanese market.
        <span class="subtit">Standards Served</span>
    	    - JAS
        <span class="subtit">Segments Served</span>
    	    - Agriculture<br />
            - Livestock<br />
            - Processing<br />
            - Wild harvesting
        </p>
        
        <div class="clear"></div>
        
        <h4 class="tituloInterno">Republic of Korea</h4>
        <p>
        	<span class="subtit">About the seal</span>
				This seal is referent to the Act on Promotion of Eco-Friendly Agriculture and Management of Organic Products which includes organic certification of the Ministry for Food, Agriculture, Forestry and Fisheries (MIFAFF) of the Republic of Korea.<br /><br />
				This seal is offered with the objective of developing, maintaining, and expanding access of organic products from diverse countries to the South Korea market. 
			<span class="subtit">Standards Served</span>
            	Act on Promotion of Eco-Friendly Agriculture and Management of Organic Products of MIFAFF 
            <span class="subtit">Segments Served</span>
            	- Agriculture 
				- Processed Food products
        </p>
        
        <div class="clear"></div>
        
        <h4 class="tituloInterno">Orgânico Europa</h4>
        <p>
        O IBD esta acreditado pela Comunidade Européia para certificar produtos a serem vendidos na Europa pelo critério da Equivalencia- as normas IBD Orgânico são aprovadas como equivalentes para as nomas orgânicas Européias.<br />
        <span class="subtit">Diretrizes atendidas</span>
        	IBD Orgânico- reconhecido pelas autoridades Européias como equivalente à norma Européia CCE 834.
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/7.jpg" alt="" />
    	<h4 class="tituloInterno">Natrue</h4>
        <p>
        	<span class="subtit">About the Seal</span>
				IBD and NATRUE have a cooperation agreement. For details please check IBD desktop website.<br />
                <b>For cosmetics certified by IBD, the rules are simple and easy to apply: </b><br />
                <b>Organic:</b> with at least 95% organic ingredients.<br />
                <b>Made with Organic or Organic Raw Materials:</b> with at least 70% organic ingredients. <br />
                <b>Natural:</b> less than 70% organic ingredients. <br />
                This is a very reliable trend: the organic consumer is aware and wants products that are truly organic, with a significant percentage of organic raw materials!<br />
           <span class="subtit">Standards Served</span>
		        - Natrue <br />
                - IBD Cosmetic Standards
			<span class="subtit">Segments Served</span>
		        - Cosmetics <br /> 
                - Cleaning Products           
        </p>
        
        <div class="clear"></div>
        
        <h4 class="tituloInterno">Bio Suisse</h4>
        <p>
        <span class="subtit">About the seal</span>
            Bio Suisse is a private-sector organization of Swiss organic farmers whose label stands for:  <br />
            - Natural biodiversity on the organic farm <br />
            - Ethologically sound livestock management and feeding<br />
            - No use of chemically synthesized pesticides or fertilizers<br />
            - No use of genetic engineering<br />
            - No use of unnecessary additives such as flavorings and colorings<br />
            - Non-aggressive processing of foodstuffs
        <span class="subtit">Standards Served</span>
        	- Bio Suisse
        <span class="subtit">Segments Served</span>
        	- Agriculture <br />
            - Livestock <br />
            - Food processed products <br />
            - Wild collection 
        </p>
        
        <div class="clear"></div>
        
        <h4 class="tituloInterno">KRAV</h4>
        <p>
        <span class="subtit">About the Seal</span>
			KRAV is leading organic label in Sweden. <br />
IBD offers verification of KRAVs extra requirements of the EC 834/2007 regulation for our clients that sell products or ingredients to KRAV-certified Sweden operators.
        <span class="subtit">Standards Served</span>
        	- KRAV 
        <span class="subtit">Segments Served</span>
        	- Agriculture<br />
            - Livestock <br />
            - Food processed products <br />
            - Wild collection 

        </p>
        
        <div class="clear"></div>
               
        <img src="../media_mobile/img/certificados/8.jpg" alt="" />
    	<h4 class="tituloInterno">Demeter Internacional</h4>
        <p>
        <span class="subtit">About the Seal</span>
        	The Demeter seal identifies biodynamic products worldwide. Demeter products are part of an international network linked to Demeter International, with headquarters in Germany.
        <span class="subtit">Standards Served</span>
        	- Demeter 
        <span class="subtit">Segments Served</span>
        	- Agriculture<br />
            - Livestock<br />
            - Processing<br />
            - Cosmetics
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/14.jpg" alt="" />
    	<h4 class="tituloInterno">IBD Approved Input Certification</h4>
        <p>
        	<span class="subtit">About the Seal</span>
                IBD Certifications created the Material Input Approval Program with the objective of evaluating the possible use of commercially available inputs for conformity with primary organic production standards (USA, European, IFOAM, Japanese, Canadian, and Brazilian). IBD has developed its own unique standards and procedures for certifying inputs at a worldwide level, guaranteeing the safety, credibility and reliability of approved inputs and providing this guarantee to producers and companies interested in using these products.
                <br /><br />
                Through this program, producers gain an important research tool for identifying the principal inputs for their operations, guaranteeing sustainability and success in their organic production activities. 
<br /><br />
The approval program, geared towards manufacturers, importers, and distributors of inputs located in Brazil and worldwide, evaluates inputs in accordance with organic agricultural, processing and livestock standards. 
  
            <span class="subtit">Input Categories</span>
                
                Use in Agriculture:<br />
                • Mineral and/or organic fertilizers and soil conditioners;<br />
                • Pesticides for controlling pests and disease, as well as herbicides;<br />
                • Inputs for use in post-harvest handling (sanitation, cleaning materials);<br />
                • All other inputs for use in agricultural operations (cleaning irrigation systems and equipment; insect traps, among others) <br /><br />
                
                USE IN LIVESTOCK:<br />
                • Ingredients for feed formulas;<br />
                • Inputs for veterinary treatments;<br />
                • Inputs for controlling ecto and endo parasites;<br />
                • Inputs for equipment and animal sanitization; <br /><br />
                
                USE IN FOOD PROCESSING:<br />
                • Non-organic Ingredients of agricultural origin;<br />
                • Non-agricultural ingredients;<br />
                • Pest control products;<br />
                • Inputs for sanitizing equipment and organic food.
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/15.jpg" alt="" />
    	<h4 class="tituloInterno">IBD Natural Ingredients</h4>
        <p>
        	<span class="subtit">About the Seal</span>
        		The Environmental impact caused by our industrial society is influenced by the choices that we make on a daily basis regarding the consumer products that we bring into our homes and factories. Better choices will bring a high level of environmental sustainability.<br /><br />
				Making a choice to consume environmentally correct products is not simple,  it demandsing information and awareness on the part of from the consumers. Transparency regarding the industrial manufacturing processes is pertinent and important in this regard. Certification of natural and biodegradable cleaning products is a fundamental part of the information provided to consumers, influencing consumer choice.
                
			<span class="subtit">Objectives of the standard</span>
                - Stimulate and favor the use of products and processes, as well as packaging, which causes the least possible environmental impact, with priority given to the use of renewable natural resources. <br />
                - Avoid contact between allergenic products or irritants and the consumer. <br />
                - Promote the use of natural, organic and wild harvested certified products. <br />
                - Promote and guarantee cleaning products that do not use petrochemicals. 
			<span class="subtit">Standards Served</span>
        		- IBD Standards 
            <span class="subtit">Segments Served</span>
        		- Cosmetics
				- Cleaning products 
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/16.jpg" alt="" />
    	<h4 class="tituloInterno">Non GMO </h4>
        <p>
        <span class="subtit">Sobre o Selo</span>
			Everyone knows that Genetically Modified Organisms (GMOs) have come to stay. However, the organic sector and a large part of the consumer market do not want to consume such products. <br /><br />
			Given this, the Certification Program for NON-GMOs was created by IBD in order to segregate products within the market that contain GMO from those that truly do not contain GMO material, or do contain GMOs, but within pre-established  limits.<br /><br />
			The food sector should be labeling food items that contain GMOs. However, this is not happening. Consumers also demand greater transparency regarding the effect of GMO consumption on animals and humans. 
		<span class="subtit">Standards Served</span>
        	- IBD Standards for NON-GMO Products 
		<span class="subtit">Segments Served</span>
        	- Agriculture<br />
            - Livestock<br />
            - Processing<br />
            - Cosmetics
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/9.jpg" alt="" />
    	<h4 class="tituloInterno">Fair Trade IBD</h4>
        <p>
        <span class="subtit">Sobre o Selo</span>
			Fair Trade IBD is the Fairtrade Program (Fair Trade) IBD applies only to products and processes certified as organic. <br /><br />

			Fair Trade IBD Certification applies to businesses, properties and producer groups that are aimed to stimulate an internal process of human, social and environmental development fostered by commercial relations based on the principles of Fair Trade. From an initial diagnosis that characterizes the environmental reality of the organization, a Steering Committee, composed of the stakeholders in the enterprise, identifies and prioritizes the key environmental and social demands that interfere negatively in this reality and from then on, Action Plans aimed at continuous improvement in the social and environmental aspects (of Living and Working Conditions, Environmental Conservation and Recovery) are outlined.<br /><br />

            The certification projects are audited based on the following criteria:<br />
            - Critical: related to some international conventions and agreements;<br />
            - Minimum: related to the country's legislation;<br />
            - Progress: related to the promotion of local development<br />
            All In addition to those already provided by national laws.<br />
            
            Being Fair Trade certified means fostering local social and environmental development and fair trade in practice.
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/27.png" alt="" />
    	<h4 class="tituloInterno">(RSPO) </h4>
        <p>
        <span class="subtit">About the Seal</span>

            The RSPO movement (Roundtable on Sustainable Palm Oil) is rapidly growing throughout the world (see portfolio of members at www.rspo.org), with membership by the major multinational consumers of palm oil at the global level. IBD Certifications is the only Latin American certifier within RSPO and, as such, qualified to offer auditing services for this scope of certification.  <br /><br />
            
            For African palm producing companies, processors of palm oil and derivatives, as well as traders of these products, IBD offers a certification package based on RSPO Principles and Criteria or on the RSPO Chain of Custody system.
            
        <span class="subtit">Principles</span>
            
            - Commitment to transparency;<br />
            - Compliance with applicable laws and regulations;<br />
            - Commitment to long-term economic and financial viability;<br />
            - Use of appropriate best practices by growers and millers;<br />
            - Environmental responsibility and conservation of natural resources and biodiversity;<br />
            - Responsible consideration of employees and of individuals and communities affected by growers and mills; <br />
            - Responsible development of new plantings;<br />
            - Commitment to continuous improvement in key areas of activity.  
            
        <span class="subtit">Standards</span>
        	Supply Chain Certification-RSPO- 2014<br />
			Principles and Criteria-RSPO- 2013
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/24.png" alt="" />
    	<h4 class="tituloInterno">Rainforest Alliance Certified (RAS) </h4>
        <p>
        <span class="subtit">About the seal</span>

	        The standard for the use of the Rainforest Alliance Certified™ seal was developed by the Sustainable Agriculture Network, composed of international conservationist and independent organizations and aims to define proper agricultural practices that cause less impact on consumers’ health, as well as establishing standards for management of the environment and workers involved in the production process. 
        
        <span class="subtit">Standards Served</span>
        	- RAS 
        </p>
        
        <span class="subtit">Segments Served</span>
        	- Agriculture <br />
            - Livestock<br />
            - Chain of custody 
        <p></p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/25.png" alt="" />
    	<h4 class="tituloInterno">4C</h4>
        <p>
        <span class="subtit">About the seal</span>
			The 4C Association is a multi-stakeholder organization of actors committed to addressing the sustainability issues of the coffee sector.<br /><br />
			The 4C Association works to achieve 100% coffee sector compliance with its sustainability standards baseline at least.

		<span class="subtit">Standards Served</span>
        	- 4C
        </p>
        <span class="subtit">Segments Served</span>
        	- Coffee producers <br />
            - Industries and warehouses <br />
            - Traders
        <p></p>
        
        <div class="clear"></div>
        
        <h4 class="tituloInterno">UEBT</h4>
        <p>
        <span class="subtit">Union for Ethical BioTrade</span>
        	The Union for Ethical BioTrade (UEBT) is a nonprofit association that promotes the ‘Sourcing with Respect’ of ingredients that come from biodiversity. Members commit to gradually ensuring that their sourcing practices promote the conservation of biodiversity, respect traditional knowledge and assure the equitable sharing of benefits all along the supply chain.  <br /><br />
	        In joining UEBT, the company agrees to comply with the principles of Ethical BioTrade. This means using practices that promote the sustainable use of natural ingredients, while ensuring that all contributors along the supply chain are paid fair prices and share the benefits derived from the use of biodiversity. By adopting the Ethical BioTrade practices of UEBT, companies foster long-term relationships with their source communities, creating employment, contributing to local development and helping to preserve local ecologies. 
            
        <span class="subtit">Ethical BioTrade Standard</span>
        	1- Biodiversity conservation - Preserving ecosystems and promoting practices that conserve and restore biodiversity.<br />
            2- Sustainable use - Practices that safeguard the long-term functions of ecosystems and limit negative environmental impacts.<br />
            3- Fair and equitable benefit sharing - Use of biodiversity and traditional knowledge recognizes rights, promotes dialogue and provides equitable prices.<br />
            4- Socio-economic sustainability - Adopting quality management and financial practices that are sustainable and socially acceptable.<br />
            5- Legal compliance - Respecting all relevant international, national and local regulations.<br />
            6- Respect for the rights of actors - Taking into account human rights, working conditions, and the rights of indigenous and local communities.<br />
            7- Clarity about land tenure - Respecting rights over land and natural resources.
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/26.png" alt="" />
    	<h4 class="tituloInterno">UTZ</h4>
        <p>
        <span class="subtit">About the Seal</span>
        	When you buy UTZ coffee, cocoa or tea you are helping to build a better future. UTZ stands for sustainable farming and better opportunities for farmers, their families and our planet. The UTZ program enables farmers to learn better farming methods, improve working conditions and take better care of their children and the environment.<br /><br />
            Through the UTZ program farmers grow better crops, generate more income and create better opportunities while safeguarding the environment and securing the earth’s natural resources. Now and in the future. And that tastes a lot better.
            <br />
            Making a difference around the world<br />
UTZ wants sustainable farming to become the most natural thing in the world. And we’re on the right track because an increasing share of the world’s coffee, cocoa and tea is grown responsibly. In fact, almost 50% of all certified sustainable coffee is now UTZ. We work with companies like Mars, Ahold, IKEA, D.E Master Blenders 1753, Migros, Tchibo and Nestlé. So when you buy these and other UTZ certified products, we are able to support more and more farmers, their workers and their families to achieve their ambitions.
        <span class="subtit">Standards Served</span>
        	- UTZ (Code of Conduct and Chain of Custody) 
        <span class="subtit">Segments Served</span>
        	- Agriculture <br />
            - Chain of custody  
        </p>
        
        

        
    </div><!--interna-->

</asp:Content>
