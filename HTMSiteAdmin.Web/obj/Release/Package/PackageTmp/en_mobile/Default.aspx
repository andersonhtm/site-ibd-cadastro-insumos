﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HTMSiteAdmin.Web.en_mobile.Default" %>

<%@ Register Src="~/en_mobile/UserControls/ucMobileUltimasNoticias.ascx" TagPrefix="uc1" TagName="ucMobileUltimasNoticias" %>
<%@ Register Src="~/en_mobile/UserControls/ucMobileChamadaEventos.ascx" TagPrefix="uc1" TagName="ucMobileChamadaEventos" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="media/js/swiper.min.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
			var swiper = new Swiper('.swiper-container', { 
				pagination: '.bannerPaginacao',
				slidesPerView: 1,
				paginationClickable: true,
				spaceBetween: 30,
				loop: true,
				autoplay: 5000,
				speed: 800
			});
		}); 
	</script>
    
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="../media_mobile/css/swiper.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">
    
    <div class="banner" runat="server" id="ctnBanners">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide" runat="server" id="ctnBannerTopo1" visible="false">
                    <asp:HyperLink ID="linkBanner1" runat="server">
                        <asp:Image ID="imgBannerTopo1" runat="server" width="278" height="150" /> 
                        <span><asp:Literal ID="litTituloBanner1" runat="server"></asp:Literal></span>                                       
                    </asp:HyperLink>                    
                </div>
                <div class="swiper-slide" runat="server" id="ctnBannerTopo2" visible="false">
                    <asp:HyperLink ID="linkBanner2" runat="server">
                        <asp:Image ID="imgBannerTopo2" runat="server" width="278" height="150" />                    
                        <span><asp:Literal ID="litTituloBanner2" runat="server"></asp:Literal></span>                   
                    </asp:HyperLink>
                </div>
                <div class="swiper-slide" runat="server" id="ctnBannerTopo3" visible="false">
                    <asp:HyperLink ID="linkBanner3" runat="server">
                        <asp:Image ID="imgBannerTopo3" runat="server" width="278" height="150" />              
                        <span><asp:Literal ID="litTituloBanner3" runat="server"></asp:Literal></span>                   
                    </asp:HyperLink>
                </div>
            </div>
        </div>
        <div class="bannerPaginacao"></div>
    </div>
    <!--banner-->

    <div class="noticias">
        <h3 class="tituloGeral">Lastest News</h3>
        <!--while-->
        <uc1:ucMobileUltimasNoticias runat="server" id="ucMobileUltimasNoticias" chamada="true" />
        <!--end while-->
       
        <a href="Noticias.aspx" class="linkMais">» more news</a>
    </div>
    <!--noticias-->

    <div class="sejaIBD">
        <a href="SejaIBD.aspx">
            <img src="../media_mobile/img/modelos/sejaIBD.jpg" alt="" width="280" height="64" /></a>
    </div>
    <!--sejaIBD-->

    <div class="eventos">
        <h3 class="tituloGeral">Events</h3>
        <!--while-->
        <uc1:ucMobileChamadaEventos runat="server" ID="ucMobileChamadaEventos" chamada="true" />
        <!--end while-->       
        <a href="Eventos.aspx" class="linkMais">» more events</a>
    </div>
    <!--noticias-->

    <div class="clear"></div>

    <div class="pesquisas">
        <h3 class="tituloGeral">Search</h3>
        <a href="/pt/ClientesResultadoPesquisa.aspx">Certified Clients</a>
        <a href="/pt/ClientesResultadoPesquisaInsumos.aspx">Approved Material Inputs</a>
        <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=492e3387-7e05-460f-b325-b77f9816b428">Clients EU</a>
    </div>
    <!--pesquisas-->

    <div class="clear"></div>

    <div class="cadastrese">
        <h3 class="tituloGeral">Register</h3>
        
        <fieldset>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="sumario-erros" ForeColor="Red" DisplayMode="BulletList" />
            <div class="form-group">
                <label>Name</label>                
                <asp:TextBox ID="txtCadastreSeNome" runat="server"></asp:TextBox> 
            </div>
            <asp:RequiredFieldValidator Display="None" SetFocusOnError="true" ID="RequiredFieldValidator1" ControlToValidate="txtCadastreSeNome" runat="server" ErrorMessage="Name required"></asp:RequiredFieldValidator>                        
            <div class="form-group">
                <label>E-mail</label>
                <asp:TextBox ID="txtCadastreSeEmail" runat="server"></asp:TextBox>   
            </div>
            <asp:RequiredFieldValidator Display="None" SetFocusOnError="true" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCadastreSeEmail" ErrorMessage="E-mail required"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator Display="None" SetFocusOnError="true" ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid e-mail" ControlToValidate="txtCadastreSeEmail" ValidationExpression="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"></asp:RegularExpressionValidator>
            <asp:LinkButton ID="btnCadastrar" OnClick="btnCadastrar_Click" CssClass="enviar" runat="server">register »</asp:LinkButton>                   
        </fieldset>
        
    </div>
    <!--cadastrese-->

</asp:Content>
