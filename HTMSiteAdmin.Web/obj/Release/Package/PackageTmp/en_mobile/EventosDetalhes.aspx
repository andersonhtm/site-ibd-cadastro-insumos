﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="EventosDetalhes.aspx.cs" Inherits="HTMSiteAdmin.Web.en_mobile.EventosDetalhes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">
       
    <div class="interna eventosDetalhes">
        <h3 class="tituloGeral">Events</h3>

        <div class="titulo">            
            <h3><asp:Literal ID="ltrTitulo" Text="Título da notícia" runat="server" /></h3>
            <p><asp:Literal ID="ltrPeriodo" Text="de 01 a 10 de Janeiro de 2012" runat="server" /></p>
            <span>released on <asp:Literal ID="ltrDataPublicacao" Text="30/01/2012" runat="server" /></span>                
        </div>
        <div class="conteudo">
            <asp:Literal ID="ltrConteudo" Text="Conteúdo" runat="server" />
        </div>
    </div>
   
</asp:Content>
