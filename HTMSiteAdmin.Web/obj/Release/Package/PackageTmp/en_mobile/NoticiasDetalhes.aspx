﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="NoticiasDetalhes.aspx.cs" Inherits="HTMSiteAdmin.Web.en_mobile.NoticiasDetalhes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">
    
        <div class="interna noticiasDetalhes">

            <h3 class="tituloGeral">News</h3>
                
            <div class="titulo">

                <asp:Literal ID="ltrImagem" runat="server"></asp:Literal>

                <h3><asp:Literal ID="ltrTitulo" Text="Título da notícia" runat="server" /></h3>
                <p><asp:Literal ID="ltrSubTitulo" Text="" runat="server" /></p>
                <span>released on <asp:Literal ID="ltrDataPublicacao" Text="30/01/2012" runat="server" /></span>
            </div>
               
            <asp:Literal ID="ltrConteudo" Text="Conteúdo" runat="server" />
                
            <p class="fonte"><asp:Literal ID="ltrFonte" Text="" runat="server" /></p>           
            
        </div>        
    
</asp:Content>
