﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="Noticias.aspx.cs" Inherits="HTMSiteAdmin.Web.en_mobile.Noticias" %>

<%@ Register Src="~/en_mobile/UserControls/ucMobileUltimasNoticias.ascx" TagPrefix="uc1" TagName="ucMobileUltimasNoticias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">
     <div class="interna noticiasLista">
        <h3 class="tituloGeral">News</h3> 

        <uc1:ucMobileUltimasNoticias runat="server" ID="ucMobileUltimasNoticias" chamada="false" />

    </div>
</asp:Content>
