﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucMobileClientesCertificados.ascx.cs" Inherits="HTMSiteAdmin.Web.en_mobile.UserControls.ucClientesCertificados" %>
<asp:UpdatePanel runat="server" ID="upClientesCertificados">
    <ContentTemplate>
        <h3>
            Certified Clients</h3>
        <fieldset>
            <ul>
                <li>
                    <label>
                        by certificate</label>
                    <asp:DropDownList runat="server" ID="ddlCertificado" DataTextField="NOME" DataValueField="ID_CERTIFICADO"
                        OnDataBound="ddlCertificado_DataBound" />
                </li>
                <li>
                    <label>
                        by product</label>
                    <asp:TextBox runat="server" ID="txtClienteCertificadoProduto" />
                </li>
                <li>
                    <label>
                        by client</label>
                    <asp:TextBox runat="server" ID="txtClienteCertificadoCliente" />
                </li>
                <li>
                    <label>
                        by country</label>
                    <asp:DropDownList runat="server" ID="ddlClienteCertificadoPais" DataTextField="PAIS"
                        DataValueField="PAIS" OnSelectedIndexChanged="ddlClienteCertificadoPais_SelectedIndexChanged"
                        AutoPostBack="True" OnDataBound="ddlClienteCertificadoPais_DataBound" />
                </li>
                <li>
                    <label>by state</label>
                    <span id="spanTextoEstado" runat="server" visible="true" 
                        style="float:right;font-weight:normal;color:#A2AC94;font-style:italic;">
                        [escolha um país]
                    </span>
                    <asp:DropDownList runat="server" ID="ddlClienteCertificadoUf" OnDataBound="ddlClienteCertificadoUf_DataBound"
                        DataTextField="ESTADO" DataValueField="ESTADO_SIGLA" Visible="false" />
                </li>
                <li></li>
                <li>
                    <asp:LinkButton ID="btnClienteCertificadoBuscar" runat="server" CssClass="btBuscar"
                        OnClick="btnClienteCertificadoBuscar_Click" Style="margin-right: 0px;">search</asp:LinkButton>
                </li>
            </ul>
        </fieldset>
    </ContentTemplate>
</asp:UpdatePanel>
