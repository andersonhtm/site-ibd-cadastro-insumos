﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="Acreditadores.aspx.cs" Inherits="HTMSiteAdmin.Web.pt_mobile.Acreditadores" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="interna acreditadores">
    
    	<h3 class="tituloGeral">Acreditadores</h3>
        
    	<img src="../media_mobile/img/acreditadores/1.jpg" alt=""/>
    	<h4 class="tituloInterno">Accreditation Services International (ASI)</h4>
        <p>Accreditation Services International (ASI) é um dos acreditadores mundiais líder em sistemas de normas de sustentabilidade. ASI avalia organizações que emitem certificados para diversas normas, garantindo que as auditorias são conduzidas com competência e consistência globais. Como único órgão acreditador de normas ambientais tais como o Forest Stewardship Council (FSC), o Marine Stewardship Council (MSC), o Aquaculture Stewardship Council (ASC), a Roundtable on Sustainable Palm Oil (RSPO), e a Roundtable on Sustainable Biomaterials (RSB), ASI monitora a aplicação correta dos critérios das normas para operadores certificados mundialmente.</p>       
        
        <img src="../media_mobile/img/acreditadores/2.jpg" alt=""/>
    	<h4 class="tituloInterno">Ministry of Agriculture, Livestock and Food Supply</h4>
        <p>O Ministério da Agricultura, Pecuária e Abastecimento (Mapa) é responsável pela gestão das políticas públicas de estímulo à agropecuária, pelo fomento do agronegócio e pela regulação e normatização de serviços vinculados ao setor. No Brasil, o agronegócio contempla o pequeno, o médio e o grande produtor rural e reúne atividades de fornecimento de bens e serviços à agricultura, produção agropecuária, processamento, transformação e distribuição de produtos de origem agropecuária até o consumidor final.</p> 
        
        <img src="../media_mobile/img/acreditadores/3.jpg" alt=""/>
    	<h4 class="tituloInterno">Coordenação Geral de Acreditação do Inmetro (Cgcre)</h4>
        <p>A Coordenação Geral de Acreditação do Inmetro (Cgcre) é o organismo de acreditação de organismos de avaliação da conformidade reconhecido pelo Governo Brasileiro. O Decreto nº 6.275, de 28 de novembro de 2007, estabelece que compete à Coordenação Geral de Acreditação do Inmetro (Cgcre) atuar como organismo de acreditação de organismos de avaliação da conformidade. A Cgcre é, portanto, dentro da estrutura organizacional do Inmetro, a unidade organizacional principal que tem total responsabilidade e autoridade sobre todos os aspectos referentes à acreditação, incluindo as decisões de acreditação. </p>   
        
        <img src="../media_mobile/img/acreditadores/4.jpg" alt=""/>
    	<h4 class="tituloInterno">IOAS</h4>
        <p>A Coordenação Geral de Acreditação do Inmetro (Cgcre) é o organismo de acreditação de organismos de avaliação da conformidade reconhecido pelo Governo Brasileiro. O Decreto nº 6.275, de 28 de novembro de 2007, estabelece que compete à Coordenação Geral de Acreditação do Inmetro (Cgcre) atuar como organismo de acreditação de organismos de avaliação da conformidade. A Cgcre é, portanto, dentro da estrutura organizacional do Inmetro, a unidade organizacional principal que tem total responsabilidade e autoridade sobre todos os aspectos referentes à acreditação, incluindo as decisões de acreditação. </p> 
        
        <img src="../media_mobile/img/acreditadores/5.jpg" alt=""/>
    	<h4 class="tituloInterno">IFOAM - ifoam.org | International Federation of Organic Agriculture Movements</h4>
        <p>A IFOAM é a organização mundial para o movimento orgânico, unindo mais de 750 organizações-membro em 116 países. A IFOAM oferece uma garantia para o mercado para orgânicos. O Sistema de Garantia Orgânica (OGS) une o mundo orgânico através de um sistema comum de normas, verificação e identidade de mercado, promove a equivalência entre participantes (certificadoras acreditadas) IFOAM, abrindo o caminho para oum comércio mais ordenado e confiável dando credibilidade ao consumidor da "marca" orgânica.</p>    
        
        <img src="../media_mobile/img/acreditadores/6.jpg" alt=""/>
    	<h4 class="tituloInterno">USDA</h4>
        <p>O Departamento de Agricultura dos Estados Unidos (USDA) é representado em mais de 90 países através da sua agência Foreign Agricultural Service (FAS). A missão da agência é desenvolver, manter e expandir o acesso para produtos dos EUA nos mercados mundiais.</p>  
        
        <img src="../media_mobile/img/acreditadores/7.jpg" alt=""/>
    	<h4 class="tituloInterno">Demeter</h4>
        <p>Deméter é a única associação ecológica que construiu uma rede de organizações de certificação individuais em todo o mundo. Em 1997 a Demeter International foi fundada para uma cooperação mais estreita nos domínios jurídico, econômico e espiritual. Atualmente a Demeter International tem 16 organizações membros da Europa, América, África e Nova Zelândia. Assim Demeter International representa mais de 4,200 produtores Demeter em 43 países.</p>
        
        
    </div><!--interna-->

</asp:Content>
