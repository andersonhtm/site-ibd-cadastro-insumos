﻿<%@ Page Title=":: MasterPainel - HTM Gestão e Tecnologia ::" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true"
    CodeBehind="ConteudoView.aspx.cs" Inherits="HTMSiteAdmin.Web.Admin.ConteudoView" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="UserControls/ucShowException.ascx" TagName="ucShowException" TagPrefix="uc1" %>
<%@ Register Src="UserControls/Bars/ucButtons.ascx" TagName="ucButtons" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upView" runat="server">
        <ContentTemplate>
            <h1>
                ..:: Conteúdo
            </h1>
            <asp:Panel ID="pnlView" runat="server">
                <fieldset class="Fieldset">
                    <legend>:: Dados Básicos</legend>
                    <div class="Fields">
                        <p>
                            <asp:Label ID="lblTitulo" runat="server" Text="Título:" AssociatedControlID="txtTitulo"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtTitulo" MaxLength="500" CssClass="Field" Width="450px" />
                            <span class="Validators">*</span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTitulo"
                                CssClass="Validators" Display="Dynamic" ErrorMessage="Campo Requerido" SetFocusOnError="True"
                                ValidationGroup="vgView" />
                        </p>
                        <p>
                            <asp:Label ID="Label7" runat="server" Text="Título (Inglês):" AssociatedControlID="txtTitulo_en"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtTitulo_en" MaxLength="500" CssClass="Field" Width="450px" />
                        </p>
                        <p>
                            <asp:Label ID="lblSub_titulo" runat="server" Text="Sub título:" AssociatedControlID="txtSub_titulo"
                                CssClass="Label" Style="vertical-align: top;" />
                            <asp:TextBox runat="server" ID="txtSub_titulo" MaxLength="500" CssClass="Field" Rows="2"
                                TextMode="MultiLine" Width="450px" />
                        </p>
                        <p>
                            <asp:Label ID="Label8" runat="server" Text="Sub título (Inglês):" AssociatedControlID="txtSub_titulo_en"
                                CssClass="Label" Style="vertical-align: top;" />
                            <asp:TextBox runat="server" ID="txtSub_titulo_en" MaxLength="500" CssClass="Field"
                                Rows="2" TextMode="MultiLine" Width="450px" />
                        </p>
                        <p>
                            <asp:Label ID="lblId_conteudo_tipo" runat="server" Text="Tipo:" AssociatedControlID="ddlId_conteudo_tipo"
                                CssClass="Label" />
                            <asp:DropDownList ID="ddlId_conteudo_tipo" runat="server" OnDataBound="ddlId_conteudo_tipo_DataBound"
                                CssClass="Field" />
                            <span class="Validators">*</span>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Campo Requerido"
                                ControlToValidate="ddlId_conteudo_tipo" CssClass="Validators" Display="Dynamic"
                                Operator="GreaterThan" SetFocusOnError="True" Type="Integer" ValidationGroup="vgView"
                                ValueToCompare="0"></asp:CompareValidator>
                        </p>
                        <p>
                            <asp:Label ID="Label4" runat="server" Text="Categoria:" AssociatedControlID="ddlCategoriaConteudo"
                                CssClass="Label" />
                            <asp:DropDownList ID="ddlCategoriaConteudo" runat="server" OnSelectedIndexChanged="ddlCategoriaConteudo_SelectedIndexChanged"
                                OnDataBound="ddlCategoriaConteudo_DataBound" CssClass="Field" />
                            <span class="Validators">*</span>
                            <asp:CompareValidator ID="cvCategoriaConteudo" runat="server" ErrorMessage="Campo Requerido"
                                ControlToValidate="ddlCategoriaConteudo" CssClass="Validators" Display="Dynamic"
                                Operator="GreaterThan" SetFocusOnError="True" Type="Integer" ValidationGroup="vgView"
                                ValueToCompare="0"></asp:CompareValidator>
                        </p>
                        <p>
                            <asp:Label ID="lblFonte" runat="server" Text="Fonte:" AssociatedControlID="txtFonte"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtFonte" MaxLength="500" CssClass="Field" Width="450px" />
                        </p>
                        <%--<p>
                            <asp:Label ID="Label11" runat="server" Text="Data de Ocorrência:" AssociatedControlID="txtData_ocorrencia"
                                CssClass="Label" />
                            <asp:TextBox ID="txtData_ocorrencia" runat="server" Width="80px" MaxLength="1" Style="text-align: justify"
                                ValidationGroup="mkeData_ocorrencia" CssClass="Field" />
                            <asp:ImageButton ID="btnData_ocorrencia" runat="server" ImageUrl="~/Admin/Media/Images/Calendar_scheduleHS.png"
                                CausesValidation="False" />
                            <ajaxToolkit:MaskedEditExtender ID="mskeeData_ocorrencia" runat="server" TargetControlID="txtData_ocorrencia"
                                Mask="99/99/9999" MessageValidatorTip="true" MaskType="Date" DisplayMoney="Left"
                                AcceptNegative="Left" ErrorTooltipEnabled="True" />
                            <ajaxToolkit:MaskedEditValidator ID="mskevData_ocorrencia" runat="server" ControlExtender="mskeeData_ocorrencia"
                                ControlToValidate="txtData_ocorrencia" InvalidValueMessage="Data Inválida" Display="Dynamic"
                                InvalidValueBlurredMessage="Data Inválida" ValidationGroup="mkeData_ocorrencia"
                                CssClass="Validators" IsValidEmpty="False" SetFocusOnError="True" />
                            <ajaxToolkit:CalendarExtender ID="ceData_ocorrencia" runat="server" TargetControlID="txtData_ocorrencia"
                                PopupButtonID="btnData_ocorrencia" />
                        </p>--%>
                        <p>
                            <asp:Label ID="Label10" runat="server" Text="Data de Publicação:" AssociatedControlID="txtData_publicacao"
                                CssClass="Label" />
                            <asp:TextBox ID="txtData_publicacao" runat="server" Width="80px" MaxLength="1" Style="text-align: justify"
                                ValidationGroup="MKEData_publicacao" CssClass="Field" />
                            <asp:ImageButton ID="btnData_publicacao" runat="server" ImageUrl="~/Admin/Media/Images/Calendar_scheduleHS.png"
                                CausesValidation="False" />
                            <ajaxToolkit:MaskedEditExtender ID="mskeeData_publicacao" runat="server" TargetControlID="txtData_publicacao"
                                Mask="99/99/9999" MessageValidatorTip="true" MaskType="Date" DisplayMoney="Left"
                                AcceptNegative="Left" ErrorTooltipEnabled="True" />
                            <ajaxToolkit:MaskedEditValidator ID="mskevData_publicacao" runat="server" ControlExtender="mskeeData_publicacao"
                                ControlToValidate="txtData_publicacao" InvalidValueMessage="Data Inválida" Display="Dynamic"
                                InvalidValueBlurredMessage="Data Inválida" ValidationGroup="MKEData_publicacao"
                                CssClass="Validators" IsValidEmpty="False" SetFocusOnError="True" />
                            <ajaxToolkit:CalendarExtender ID="ceData_publicacao" runat="server" TargetControlID="txtData_publicacao"
                                PopupButtonID="btnData_publicacao" />
                        </p>
                        <p>
                            <asp:Label ID="Label1" runat="server" Text="Situação:" AssociatedControlID="ddlSituacao"
                                CssClass="Label" />
                            <asp:DropDownList ID="ddlSituacao" runat="server" CssClass="Field">
                                <asp:ListItem Selected="True" Value="1">Ativo</asp:ListItem>
                                <asp:ListItem Value="2">Inativo</asp:ListItem>
                            </asp:DropDownList>
                        </p>
                        <p>
                            <asp:Label ID="lblObservacao" runat="server" Text="Observações:" AssociatedControlID="txtObservacao"
                                CssClass="Label" Style="vertical-align: top;" />
                            <asp:TextBox runat="server" ID="txtObservacao" MaxLength="255" CssClass="Field" Rows="3"
                                TextMode="MultiLine" Width="450px" />
                        </p>
                    </div>
                    <div style="display:none">
                        <asp:Label ID="Label9" runat="server" Text="Arquivo:" AssociatedControlID="afuArquivoEvento"
                            CssClass="Label" Style="float: left;" />
                        <ajaxToolkit:AsyncFileUpload ID="afuArquivoEvento" runat="server" Style="display: inline-block;
                            float: left; margin-left: 2px;" OnUploadedComplete="afuArquivoEvento_UploadedComplete" />
                    </div>
                </fieldset>
                <asp:Panel ID="pnlArquivoAtual" runat="server" Visible="false">
                    <fieldset class="Fieldset">
                        <legend>..:: Arquivo Atual</legend>
                        <p>
                            <asp:ImageButton ID="btnExcluirImagem" ImageUrl="~/Admin/Media/Images/botao_excluir_2.png"
                                runat="server" onmouseover="javascript:ChangeButtonsImage('excluir_imagem', 'over', this);"
                                onmouseout="javascript:ChangeButtonsImage('excluir_imagem', 'out', this);" OnClick="btnExcluirImagem_Click" />
                        </p>
                        <div>
                            <asp:Image ID="imgEvento" runat="server" ImageUrl="~/ShowFile.aspx?action=1" />
                        </div>
                    </fieldset>
                </asp:Panel>
                <fieldset class="Fieldset">
                    <legend>..:: Conteúdo</legend>
                    <div class="Fields">
                        <p>
                            <CKEditor:CKEditorControl ID="txtConteudo" runat="server" BasePath="~/Scripts/ckeditor"
                                ContentsCss="~/Scripts/ckeditor/contents.css" 
                                TemplatesFiles="~/Scripts/ckeditor/plugins/templates/templates/default.js" 
                                Toolbar="MasterPainelToolbar" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtConteudo"
                                CssClass="Validators" Display="Dynamic" ErrorMessage="Campo Requerido" SetFocusOnError="True"
                                ValidationGroup="vgView" />
                        </p>
                    </div>
                </fieldset>
                <fieldset class="Fieldset">
                    <legend>..:: Conteúdo (Inglês)</legend>
                    <div class="Fields">
                        <p>
                            <CKEditor:CKEditorControl ID="txtConteudo_en" runat="server" BasePath="~/Scripts/ckeditor"
                                ContentsCss="~/Scripts/ckeditor/contents.css" 
                                TemplatesFiles="~/Scripts/ckeditor/plugins/templates/templates/default.js" 
                                Toolbar="MasterPainelToolbar" />
                        </p>
                    </div>
                </fieldset>
            </asp:Panel>
            <uc1:ucShowException ID="ucShowException1" runat="server" />
            <uc2:ucButtons ID="ucButtons1" runat="server" />
            <asp:Panel ID="pnlViewCollection" runat="server">
                <div class="Fields">
                    <p>
                        <asp:Label ID="Label6" runat="server" Text="Situação:" AssociatedControlID="ddlFiltroSituacao"
                            CssClass="LabelFilters" />
                        <asp:DropDownList ID="ddlFiltroSituacao" runat="server" CssClass="Field">
                            <asp:ListItem Selected="True" Value="1">Ativo</asp:ListItem>
                            <asp:ListItem Value="2">Inativo</asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    <p>
                        <asp:Label ID="Label2" runat="server" Text="Tipo:" AssociatedControlID="ddlFiltroTipoConteudo"
                            CssClass="LabelFilters" />
                        <asp:DropDownList ID="ddlFiltroTipoConteudo" runat="server" OnDataBound="ddlId_conteudo_tipo_DataBound"
                            CssClass="Field" />
                    </p>
                    <p>
                        <asp:Label ID="Label5" runat="server" Text="Categoria:" AssociatedControlID="ddlFiltroCategoria"
                            CssClass="LabelFilters" />
                        <asp:DropDownList ID="ddlFiltroCategoria" runat="server" OnSelectedIndexChanged="ddlCategoriaConteudo_SelectedIndexChanged"
                            OnDataBound="ddlCategoriaConteudo_DataBound" CssClass="Field" />
                    </p>
                    <p>
                        <asp:Label ID="Label3" runat="server" Text="Título:" AssociatedControlID="txtFiltroTitulo"
                            CssClass="LabelFilters" />
                        <asp:TextBox runat="server" ID="txtFiltroTitulo" MaxLength="255" CssClass="Field"
                            Width="300px" />
                        <asp:ImageButton ID="btnFiltrar" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/botao_pesquisar_1.png"
                            OnClick="btnFiltrar_Click" onmouseover="javascript:ChangeButtonsImage('pesquisar', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('pesquisar', 'out', this);" Style="margin-left: 50px;" />
                    </p>
                </div>
                <asp:GridView runat="server" ID="gvView" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="ID_CONTEUDO" DataSourceID="edsView" Width="100%" CssClass="GridView"
                    OnRowCommand="gvView_RowCommand" CellPadding="4" GridLines="None" OnRowDataBound="gvView_RowDataBound"
                    OnSelectedIndexChanged="gvView_SelectedIndexChanged">
                    <AlternatingRowStyle CssClass="gvAlternatingRowStyle" />
                    <Columns>
                        <asp:BoundField DataField="ID_CONTEUDO" HeaderText="Código" ReadOnly="True" SortExpression="ID_CONTEUDO"
                            ItemStyle-CssClass="gvCenter" ItemStyle-Width="40px" >
                        <ItemStyle CssClass="gvCenter" Width="40px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Tipo" SortExpression="ID_CONTEUDO_TIPO">
                            <ItemTemplate>
                                <asp:Literal ID="ltrTipo" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Categoria" SortExpression="ID_CONTEUDO_CATEGORIA">
                            <ItemTemplate>
                                <asp:Literal ID="ltrCategoria" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Título" SortExpression="TITULO">
                            <ItemTemplate>
                                <asp:Literal ID="ltrTitulo" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Situação" SortExpression="ID_CONTEUDO_SITUACAO">
                            <ItemTemplate>
                                <asp:Literal ID="ltrSituacao" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Data Publicação" 
                            SortExpression="DATA_PUBLICACAO">
                                <ItemTemplate>
                                    <asp:Literal ID="ltrDataPublicacao" runat="server" />                                
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEditar" runat="server" CausesValidation="False" CommandName="Select"
                                    ImageUrl="~/Admin/Media/Images/GridViews/edit.png" Text="Select" ToolTip="Exibir/Editar" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnExcluir" runat="server" CausesValidation="False" CommandName="Excluir"
                                    OnClientClick="javascript:return confirm('Deseja excluir este registro?');" ImageUrl="~/Admin/Media/Images/GridViews/delete.png"
                                    CommandArgument='<%# Bind("ID_CONTEUDO") %>' ToolTip="Excluir" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle CssClass="EditRowStyle" />
                    <HeaderStyle CssClass="gvHeaderStyle" />
                    <PagerSettings Position="TopAndBottom" />
                    <PagerStyle CssClass="gvPagerStyle" />
                    <PagerTemplate>
                        <asp:ImageButton ID="btnGvFirst" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/first.png"
                            CommandName="FirstPage" ToolTip="Primeira Página" />
                        <asp:ImageButton ID="btnGvBack" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/back.png"
                            CommandName="BackPage" ToolTip="Página Anterior" />
                        <asp:TextBox ID="txtGvPage" runat="server" Text="<%# gvView.PageIndex + 1 %>" MaxLength="4"
                            Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:Label ID="ltrPager" Text="de" runat="server" CssClass="gvPagerLiteral" EnableTheming="false" />
                        <asp:TextBox ID="txtGvPages" runat="server" Text="<%# gvView.PageCount %>" Enabled="false"
                            ReadOnly="true" Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:ImageButton ID="btnGvGo" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/botao_ir_1.png"
                            CommandName="GoToPage" ToolTip="Ir para a página" onmouseover="javascript:ChangeButtonsImage('gotopage', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('gotopage', 'out', this);" />
                        <asp:ImageButton ID="btnGvNext" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/next.png"
                            CommandName="NextPage" ToolTip="Próxima Página" />
                        <asp:ImageButton ID="btnGvLast" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/last.png"
                            CommandName="LastPage" ToolTip="Última Página" />
                    </PagerTemplate>
                    <RowStyle CssClass="gvRowStyle" />
                    <SelectedRowStyle CssClass="gvSelectedRowStyle" />
                    <SortedAscendingCellStyle CssClass="gvSortedAscendingCellStyle" />
                    <SortedAscendingHeaderStyle CssClass="gvSortedAscendingHeaderStyle" />
                    <SortedDescendingCellStyle CssClass="gvSortedDescendingCellStyle" />
                    <SortedDescendingHeaderStyle CssClass="gvSortedDescendingHeaderStyle" />
                </asp:GridView>
                <asp:EntityDataSource ID="edsView" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                    DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="CONTEUDO"
                    EntityTypeFilter="CONTEUDO" OnQueryCreated="edsView_QueryCreated" 
                    OrderBy="it.DATA_PUBLICACAO DESC" Select="" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
