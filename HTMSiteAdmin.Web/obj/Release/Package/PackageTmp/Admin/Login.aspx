﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="HTMSiteAdmin.Web.Admin.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>:: MasterPainel - HTM Gestão e Tecnologia ::</title>
    <link href="Styles/Login.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function ChangeButtonsImage(_btn, _action, _obj) {
            switch (_btn) {
                case "entrar":
                    switch (_action) {
                        case "over":
                            _obj.src = "Media/Login/botao_entrar_1_on.png";
                            break;
                        case "out":
                            _obj.src = "Media/Login/botao_entrar_1.png";
                            break;
                    }
                    break;
                case "limpar":
                    switch (_action) {
                        case "over":
                            _obj.src = "Media/Images/Bars/Buttons/botao_limpar_1_on.png";
                            break;
                        case "out":
                            _obj.src = "Media/Images/Bars/Buttons/botao_limpar_1.png";
                            break;
                    }
                    break;
            }
        }
    </script>
</head>
<body>
    <div id="page">
        <form runat="server">
        <div id="fields">
            <p class="title">
                <strong>PAINEL ADMINISTRATIVO</strong>
            </p>
            <p>
                Seja bem vindo!
            </p>
            <p>
                Identifique-se para acesso ao sistema.
            </p>
            <p class="fields" style="margin-top: 20px;">
                <asp:Label ID="lblUsuario" runat="server" Text="Usuario:" AssociatedControlID="txtUsuario" />
                <asp:TextBox runat="server" ID="txtUsuario" MaxLength="255" />
            </p>
            <p class="fields">
                <asp:Label ID="lblSenha" runat="server" Text="Senha:" AssociatedControlID="txtSenha" />
                <asp:TextBox runat="server" ID="txtSenha" MaxLength="255" TextMode="Password" />
            </p>
            <p class="fields">
                <asp:ImageButton ID="btnLogin" runat="server" ImageUrl="~/Admin/Media/Login/botao_entrar_1.png"
                    OnClick="btnLogin_Click" onmouseover="javascript:ChangeButtonsImage('entrar', 'over', this);"
                    onmouseout="javascript:ChangeButtonsImage('entrar', 'out', this);" />
                <asp:ImageButton ID="btnLimpar" runat="server" ImageUrl="~/Admin/Media/Login/botao_limpar_1.png"
                    OnClick="btnLimpar_Click" onmouseover="javascript:ChangeButtonsImage('limpar', 'over', this);"
                    onmouseout="javascript:ChangeButtonsImage('limpar', 'out', this);" />
            </p>
            <asp:Panel runat="server" ID="pnlMensagemErro" Visible="false" Style="background-color: #ffcccc !important;
                border: 1px solid #7f0101; border-radius: 5px; padding: 5px 0px; margin: 5px 10px 0px 10px;">
                <p class="error">
                    <asp:Image ID="Image2" ImageUrl="~/Admin/Media/Login/mensagem_alerta_1.png" runat="server" />
                    <asp:Literal ID="ltrMensagemErro" Text="Usuário ou Senha Inválido. Verique!" runat="server" />
                </p>
            </asp:Panel>
        </div>
        <div id="logo">
            <asp:Image ID="Image1" ImageUrl="~/Admin/Media/Images/logo_cliente_ibd_1.png" runat="server" />
        </div>
        </form>
        <p class="logo">
            <a href="http://www.htm.com.br" target="_blank">
                <img src="Media/Login/etiqueta_desenvolvido_por_HTM_azul_escura.png" alt="HTM Gestão e Tecnologia"
                    width="152" height="33" border="0" /></a>
        </p>
    </div>
</body>
</html>
