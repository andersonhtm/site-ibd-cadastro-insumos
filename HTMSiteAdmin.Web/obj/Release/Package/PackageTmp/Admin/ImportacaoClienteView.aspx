﻿<%@ Page Title=":: MasterPainel - HTM Gestão e Tecnologia ::" Language="C#" MasterPageFile="~/Admin/Admin.Master"
    AutoEventWireup="true" CodeBehind="ImportacaoClienteView.aspx.cs" Inherits="HTMSiteAdmin.Web.Admin.ImportacaoClienteView" %>

<%@ Register Src="UserControls/ucShowException.ascx" TagName="ucShowException" TagPrefix="uc1" %>
<%@ Register Src="UserControls/Bars/ucButtons.ascx" TagName="ucButtons" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upView" runat="server">
        <ContentTemplate>
            <h1>
                ..:: Clientes
            </h1>
            <asp:Panel ID="pnlView" runat="server">
                <fieldset class="Fieldset">
                    <legend>:: Dados Básicos</legend>
                    <div class="Fields">
                        <p>
                            <asp:Label ID="lblPf_pj" runat="server" Text="Tipo de Pessoa:" AssociatedControlID="ddlPf_pj"
                                CssClass="Label" />
                            <asp:DropDownList runat="server" ID="ddlPf_pj" CssClass="Field">
                                <asp:ListItem Text="Pessoa Física" Value="1" />
                                <asp:ListItem Text="Pessoa Jurídica" Selected="True" Value="2" />
                            </asp:DropDownList>
                        </p>
                        <p>
                            <asp:Label ID="lblMatricula" runat="server" Text="Matrícula:" AssociatedControlID="txtMatricula"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtMatricula" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblNome_razao" runat="server" Text="Nome/Razão Social:" AssociatedControlID="txtNome_razao"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtNome_razao" CssClass="Field" Width="300px" />
                        </p>
                        <p>
                            <asp:Label ID="lblApelido_fantasia" runat="server" Text="Apelido/Nome Fantasia:"
                                AssociatedControlID="txtApelido_fantasia" CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtApelido_fantasia" Width="300px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblCpf_cnpj" runat="server" Text="CPF/CNPJ:" AssociatedControlID="txtCpf_cnpj"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtCpf_cnpj" MaxLength="50" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblRg_ie" runat="server" Text="RG/IE:" AssociatedControlID="txtRg_ie"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtRg_ie" MaxLength="50" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblCep" runat="server" Text="CEP:" AssociatedControlID="txtCep" CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtCep" MaxLength="50" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="Label4" runat="server" Text="Logradouro:" AssociatedControlID="txtLogradouro"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtLogradouro" Width="400px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblEndereco" runat="server" Text="Endereco:" AssociatedControlID="txtEndereco"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtEndereco" Width="400px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblNumero" runat="server" Text="Numero" AssociatedControlID="txtNumero"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtNumero" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblComplemento" runat="server" Text="Complemento" AssociatedControlID="txtComplemento"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtComplemento" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblCxpostal" runat="server" Text="Cxpostal" AssociatedControlID="txtCxpostal"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtCxpostal" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblMunicipio" runat="server" Text="Município:" AssociatedControlID="txtMunicipio"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtMunicipio" Width="200px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblEstado" runat="server" Text="Estado (Nome):" AssociatedControlID="txtEstado"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtEstado" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblEstado_sigla" runat="server" Text="Estado (Sigla):" AssociatedControlID="txtEstado_sigla"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtEstado_sigla" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblPais" runat="server" Text="País:" AssociatedControlID="txtPais"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtPais" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblPais_en" runat="server" Text="Pais (Inglês):" AssociatedControlID="txtPais_en"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtPais_en" CssClass="Field" />
                        </p>
                        <%--<p>
                            <asp:Label ID="lblPais_es" runat="server" Text="País (Espanhol):" AssociatedControlID="txtPais_es"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtPais_es" CssClass="Field" />
                        </p>--%>
                        <p>
                            <asp:Label ID="lblEmail" runat="server" Text="E-mail:" AssociatedControlID="txtEmail"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtEmail" Width="300px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblDdi" runat="server" Text="DDI:" AssociatedControlID="txtDdi" CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtDdi" MaxLength="10" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblTelefone" runat="server" Text="Telefone:" AssociatedControlID="txtTelefone"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtTelefone" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblFax" runat="server" Text="Fax:" AssociatedControlID="txtFax" CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtFax" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblCelular" runat="server" Text="Celular:" AssociatedControlID="txtCelular"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtCelular" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblSite" runat="server" Text="Site:" AssociatedControlID="txtSite"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtSite" Width="300px" CssClass="Field" />
                        </p>
                    </div>
                </fieldset>
            </asp:Panel>
            <uc2:ucButtons ID="ucButtons1" runat="server" />
            <uc1:ucShowException ID="ucShowException1" runat="server" />
            <asp:Panel ID="pnlViewCollection" runat="server">
                <div class="Fields">
                    <p>
                        <asp:Label ID="Label1" runat="server" Text="Origem:" AssociatedControlID="ddlFiltroOrigem"
                            CssClass="LabelFilters" />
                        <asp:DropDownList runat="server" ID="ddlFiltroOrigem">
                            <asp:ListItem Text="Todos" Selected="True" Value="0" />
                            <asp:ListItem Text="Manual" Value="1" />
                            <asp:ListItem Text="Importação" Value="2" />
                        </asp:DropDownList>
                    </p>
                    <p>
                        <asp:Label ID="Label3" runat="server" Text="Matricula:" AssociatedControlID="txtFiltroMatricula"
                            CssClass="LabelFilters" />
                        <asp:TextBox runat="server" ID="txtFiltroMatricula" MaxLength="255" Width="250px"
                            CssClass="Field" />
                    </p>
                    <p>
                        <asp:Label ID="Label2" runat="server" Text="Descrição:" AssociatedControlID="txtFiltroDescricao"
                            CssClass="LabelFilters" />
                        <asp:TextBox runat="server" ID="txtFiltroDescricao" MaxLength="255" Width="250px"
                            CssClass="Field" />
                        <asp:ImageButton ID="btnFiltrar" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/botao_pesquisar_1.png"
                            OnClick="btnFiltrar_Click" onmouseover="javascript:ChangeButtonsImage('pesquisar', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('pesquisar', 'out', this);" Style="margin-left: 50px;" />
                    </p>
                </div>
                <asp:GridView runat="server" ID="gvView" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataSourceID="edsView" Width="100%" CssClass="GridView" OnRowCommand="gvView_RowCommand"
                    DataKeyNames="ID_CLIENTE" CellPadding="4" GridLines="None" OnRowDataBound="gvView_RowDataBound"
                    OnSelectedIndexChanged="gvView_SelectedIndexChanged">
                    <AlternatingRowStyle CssClass="gvAlternatingRowStyle" />
                    <Columns>
                        <asp:BoundField DataField="MATRICULA" HeaderText="Matrícula" ReadOnly="True" SortExpression="MATRICULA"
                            ItemStyle-CssClass="gvCenter">
                            <ItemStyle CssClass="gvCenter" />
                        </asp:BoundField>
                        <asp:BoundField DataField="APELIDO_FANTASIA" HeaderText="Apelido/Nome Fantasia" SortExpression="APELIDO_FANTASIA"
                            ItemStyle-CssClass="gvCenter" ReadOnly="True">
                            <ItemStyle CssClass="gvCenter" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CPF_CNPJ" HeaderText="CPF/CNPJ" SortExpression="CPF_CNPJ"
                            ItemStyle-CssClass="gvCenter" ReadOnly="True">
                            <ItemStyle CssClass="gvCenter" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MUNICIPIO" HeaderText="Município" SortExpression="MUNICIPIO"
                            ItemStyle-CssClass="gvCenter" ReadOnly="True">
                            <ItemStyle CssClass="gvCenter" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Origem" SortExpression="REGISTRO_ORIGEM">
                            <ItemTemplate>
                                <asp:Literal ID="ltrRegistroOrigem" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" Width="75px" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEditar" runat="server" CausesValidation="False" CommandName="Select"
                                    ImageUrl="~/Admin/Media/Images/GridViews/edit.png" Text="Select" ToolTip="Exibir/Editar" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnExcluir" runat="server" CausesValidation="False" CommandName="Excluir"
                                    OnClientClick="javascript:return confirm('Deseja excluir este registro?');" ImageUrl="~/Admin/Media/Images/GridViews/delete.png"
                                    CommandArgument='<%# Bind("ID_CLIENTE") %>' ToolTip="Excluir" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle CssClass="EditRowStyle" />
                    <HeaderStyle CssClass="gvHeaderStyle" />
                    <PagerSettings Position="TopAndBottom" />
                    <PagerStyle CssClass="gvPagerStyle" />
                    <PagerTemplate>
                        <asp:ImageButton ID="btnGvFirst" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/first.png"
                            CommandName="FirstPage" ToolTip="Primeira Página" />
                        <asp:ImageButton ID="btnGvBack" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/back.png"
                            CommandName="BackPage" ToolTip="Página Anterior" />
                        <asp:TextBox ID="txtGvPage" runat="server" Text="<%# gvView.PageIndex + 1 %>" MaxLength="4"
                            Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:Label ID="ltrPager" Text="de" runat="server" CssClass="gvPagerLiteral" EnableTheming="false" />
                        <asp:TextBox ID="txtGvPages" runat="server" Text="<%# gvView.PageCount %>" Enabled="false"
                            ReadOnly="true" Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:ImageButton ID="btnGvGo" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/botao_ir_1.png"
                            CommandName="GoToPage" ToolTip="Ir para a página" onmouseover="javascript:ChangeButtonsImage('gotopage', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('gotopage', 'out', this);" />
                        <asp:ImageButton ID="btnGvNext" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/next.png"
                            CommandName="NextPage" ToolTip="Próxima Página" />
                        <asp:ImageButton ID="btnGvLast" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/last.png"
                            CommandName="LastPage" ToolTip="Última Página" />
                    </PagerTemplate>
                    <RowStyle CssClass="gvRowStyle" />
                    <SelectedRowStyle CssClass="gvSelectedRowStyle" />
                    <SortedAscendingCellStyle CssClass="gvSortedAscendingCellStyle" />
                    <SortedAscendingHeaderStyle CssClass="gvSortedAscendingHeaderStyle" />
                    <SortedDescendingCellStyle CssClass="gvSortedDescendingCellStyle" />
                    <SortedDescendingHeaderStyle CssClass="gvSortedDescendingHeaderStyle" />
                </asp:GridView>
                <asp:EntityDataSource ID="edsView" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                    DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="IMPORTACAO_CLIENTE"
                    EntityTypeFilter="IMPORTACAO_CLIENTE" OnQueryCreated="edsView_QueryCreated" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
