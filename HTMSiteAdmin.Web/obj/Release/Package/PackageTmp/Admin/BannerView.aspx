﻿<%@ Page Title=":: MasterPainel - HTM Gestão e Tecnologia ::" Language="C#" MasterPageFile="~/Admin/Admin.Master"
    AutoEventWireup="true" CodeBehind="BannerView.aspx.cs" Inherits="HTMSiteAdmin.Web.Admin.BannerView" %>

<%@ Register Src="UserControls/ucShowException.ascx" TagName="ucShowException" TagPrefix="uc1" %>
<%@ Register Src="UserControls/Bars/ucButtons.ascx" TagName="ucButtons" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upView" runat="server">
        <ContentTemplate>
            <h1>
                ..:: Banners
            </h1>
            <asp:Panel ID="pnlView" runat="server">
                <fieldset class="Fieldset">
                    <legend>:: Dados Básicos</legend>
                    <div class="Fields">
                        <p>
                            <asp:Label ID="lblDescricao" runat="server" Text="Descrição:" AssociatedControlID="txtDescricao"
                                CssClass="LabelTop" />
                            <asp:TextBox runat="server" ID="txtDescricao" MaxLength="255" Rows="3" TextMode="MultiLine"
                                Width="300px" CssClass="Field" />
                            <span class="Validators" style="vertical-align: top;">*</span>
                            <asp:RequiredFieldValidator ID="rfvDescricao" runat="server" ControlToValidate="txtDescricao"
                                ErrorMessage="Campo Requerido" Display="Dynamic" SetFocusOnError="True" CssClass="Validators"
                                ValidationGroup="vgView" Style="vertical-align: top;" />
                        </p>
                        <p>
                            <asp:Label ID="lblTitulo" runat="server" Text="Título:" AssociatedControlID="txtTitulo"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtTitulo" MaxLength="255" Width="300px" CssClass="Field" />
                            <span class="Validators">*</span>
                            <asp:RequiredFieldValidator ID="rfvTitulo" runat="server" ControlToValidate="txtTitulo"
                                ErrorMessage="Campo Requerido" Display="Dynamic" SetFocusOnError="True" CssClass="Validators"
                                ValidationGroup="vgView" />
                        </p>
                        <p>
                            <asp:Label ID="Label11" runat="server" Text="Título (Inglês):" AssociatedControlID="txtTitulo_en"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtTitulo_en" MaxLength="255" Width="300px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="Label12" runat="server" Text="Sub-Título:" AssociatedControlID="txtSubTitulo"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtSubTitulo" MaxLength="255" Width="300px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="Label13" runat="server" Text="Sub-Título (Inglês):" AssociatedControlID="txtSubTitulo_en"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtSubTitulo_en" MaxLength="255" Width="300px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="Label14" runat="server" Text="Caminho (URL):" AssociatedControlID="txtCaminho"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtCaminho" MaxLength="255" Width="300px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="Label15" runat="server" Text="Caminho (URL - Inglês):" AssociatedControlID="txtCaminho_en"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtCaminho_en" MaxLength="255" Width="300px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="lblId_banner_posicao" runat="server" Text="Posição:" AssociatedControlID="ddlId_banner_posicao"
                                CssClass="Label" />
                            <asp:DropDownList ID="ddlId_banner_posicao" runat="server" OnDataBound="ddlId_banner_posicao_DataBound"
                                CssClass="Field" />
                            <span class="Validators">*</span>
                            <asp:CompareValidator ID="cvId_banner_posicao" runat="server" ErrorMessage="Campo Requerido"
                                ControlToValidate="ddlId_banner_posicao" Type="Integer" ValueToCompare="0" Display="Dynamic"
                                SetFocusOnError="True" ValidationGroup="vgView" Operator="GreaterThan" CssClass="Validators" />
                        </p>
                        <p>
                            <asp:Label ID="Label1" runat="server" Text="Situação:" AssociatedControlID="ddlSituacao"
                                CssClass="Label" />
                            <asp:DropDownList ID="ddlSituacao" runat="server" CssClass="Field">
                                <asp:ListItem Selected="True" Value="1">Ativo</asp:ListItem>
                                <asp:ListItem Value="2">Inativo</asp:ListItem>
                            </asp:DropDownList>
                        </p>
                        <p>
                            <asp:Label ID="Label4" runat="server" Text="Período de publicação:" AssociatedControlID="txtData_publicacao_inicio"
                                CssClass="Label" />
                            <asp:TextBox ID="txtData_publicacao_inicio" runat="server" Width="80px" MaxLength="1"
                                Style="text-align: justify" ValidationGroup="MKEData_publicacao_inicio" CssClass="Field" />
                            <asp:ImageButton ID="btnData_publicacao_inicio" runat="server" ImageUrl="~/Admin/Media/Images/Calendar_scheduleHS.png"
                                CausesValidation="False" />
                            <ajaxToolkit:MaskedEditExtender ID="mskeeData_publicacao_inicio" runat="server" TargetControlID="txtData_publicacao_inicio"
                                Mask="99/99/9999" MessageValidatorTip="true" MaskType="Date" DisplayMoney="Left"
                                AcceptNegative="Left" ErrorTooltipEnabled="True" />
                            <ajaxToolkit:MaskedEditValidator ID="mskevData_publicacao_inicio" runat="server"
                                ControlExtender="mskeeData_publicacao_inicio" ControlToValidate="txtData_publicacao_inicio"
                                InvalidValueMessage="Data Inválida" Display="Dynamic" InvalidValueBlurredMessage="Data Inválida"
                                ValidationGroup="MKEData_publicacao_inicio" CssClass="Validators" IsValidEmpty="False"
                                SetFocusOnError="True" />
                            <ajaxToolkit:CalendarExtender ID="ceData_publicacao_inicio" runat="server" TargetControlID="txtData_publicacao_inicio"
                                PopupButtonID="btnData_publicacao_inicio" />
                            <asp:Label ID="Label7" runat="server" Text="até" AssociatedControlID="txtData_publicacao_fim"
                                CssClass="Label" Width="30px" Style="text-align: center;" />
                            <asp:TextBox ID="txtData_publicacao_fim" runat="server" Width="80px" MaxLength="1"
                                Style="text-align: justify" ValidationGroup="MKEData_publicacao_fim" CssClass="Field" />
                            <asp:ImageButton ID="btnData_publicacao_fim" runat="server" ImageUrl="~/Admin/Media/Images/Calendar_scheduleHS.png"
                                CausesValidation="False" />
                            <ajaxToolkit:MaskedEditExtender ID="mskeeData_publicacao_fim" runat="server" TargetControlID="txtData_publicacao_fim"
                                Mask="99/99/9999" MessageValidatorTip="true" MaskType="Date" DisplayMoney="Left"
                                AcceptNegative="Left" ErrorTooltipEnabled="True" />
                            <ajaxToolkit:MaskedEditValidator ID="mskevData_publicacao_fim" runat="server" ControlExtender="mskeeData_publicacao_fim"
                                ControlToValidate="txtData_publicacao_fim" InvalidValueMessage="Data Inválida"
                                Display="Dynamic" InvalidValueBlurredMessage="Data Inválida" ValidationGroup="MKEData_publicacao_fim"
                                CssClass="Validators" IsValidEmpty="False" SetFocusOnError="True" />
                            <ajaxToolkit:CalendarExtender ID="ceData_publicacao_fim" runat="server" TargetControlID="txtData_publicacao_fim"
                                PopupButtonID="btnData_publicacao_fim" />
                        </p>
                        <div style="display: block; float: left; width: 100%">
                            <asp:Label ID="Label3" runat="server" Text="Arquivo:" AssociatedControlID="afuArquivoBanner"
                                CssClass="Label" Style="float: left;" />
                            <ajaxToolkit:AsyncFileUpload ID="afuArquivoBanner" runat="server" OnUploadedComplete="afuArquivoBanner_UploadedComplete"
                                Style="display: inline-block; float: left; margin-left: 2px;" />
                            <span class="Validators">*</span>
                        </div>
                        <div style="display: block; float: left; width: 100%">
                            <asp:Label ID="Label8" runat="server" Text="Arquivo (Inglês):" AssociatedControlID="afuArquivoBanner_en"
                                CssClass="Label" Style="float: left;" />
                            <ajaxToolkit:AsyncFileUpload ID="afuArquivoBanner_en" runat="server" Style="display: inline-block;
                                float: left; margin-left: 2px;" OnUploadedComplete="afuArquivoBanner_en_UploadedComplete" />
                        </div>
                        <div style="display: block; float: left; width: 100%">
                            <asp:Label ID="Label9" runat="server" Text="Miniatura:" AssociatedControlID="afuMiniatura"
                                CssClass="Label" Style="float: left;" />
                            <ajaxToolkit:AsyncFileUpload ID="afuMiniatura" runat="server" Style="display: inline-block;
                                float: left; margin-left: 2px;" OnUploadedComplete="afuMiniatura_UploadedComplete" />
                        </div>
                        <div style="display: block; float: left; width: 100%">
                            <asp:Label ID="Label10" runat="server" Text="Miniatura (Inglês):" AssociatedControlID="afuMiniatura_en"
                                CssClass="Label" Style="float: left;" />
                            <ajaxToolkit:AsyncFileUpload ID="afuMiniatura_en" runat="server" Style="display: inline-block;
                                float: left; margin-left: 2px;" OnUploadedComplete="afuMiniatura_en_UploadedComplete" />
                        </div>
                    </div>
                </fieldset>
                <asp:Panel ID="pnlArquivoAtual" runat="server" Visible="false">
                    <fieldset class="Fieldset">
                        <legend>..:: Arquivo Atual</legend>
                        <div>
                            <asp:ImageButton ID="btnExcluirImagem" ImageUrl="~/Admin/Media/Images/botao_excluir_2.png"
                                runat="server" onmouseover="javascript:ChangeButtonsImage('excluir_imagem', 'over', this);"
                                onmouseout="javascript:ChangeButtonsImage('excluir_imagem', 'out', this);" OnClick="btnExcluirImagem_Click" />
                            <asp:Image ID="imgBanner" runat="server" ImageUrl="~/ShowFile.aspx?action=1" />
                        </div>
                    </fieldset>
                </asp:Panel>
                <asp:Panel ID="pnlArquivoAtual_en" runat="server" Visible="false">
                    <fieldset class="Fieldset">
                        <legend>..:: Arquivo Atual (Inglês)</legend>
                        <div>
                            <asp:ImageButton ID="btnExcluirImagemEn" ImageUrl="~/Admin/Media/Images/botao_excluir_2.png"
                                runat="server" onmouseover="javascript:ChangeButtonsImage('excluir_imagem', 'over', this);"
                                onmouseout="javascript:ChangeButtonsImage('excluir_imagem', 'out', this);" OnClick="btnExcluirImagemEn_Click" />
                            <asp:Image ID="imgBanner_en" runat="server" ImageUrl="~/ShowFile.aspx?action=1" />
                        </div>
                    </fieldset>
                </asp:Panel>
                <asp:Panel ID="pnlMiniatura" runat="server" Visible="false">
                    <fieldset class="Fieldset">
                        <legend>..:: Miniatura Atual</legend>
                        <div>
                            <asp:ImageButton ID="btnExluirMiniatura" ImageUrl="~/Admin/Media/Images/botao_excluir_2.png"
                                runat="server" onmouseover="javascript:ChangeButtonsImage('excluir_imagem', 'over', this);"
                                onmouseout="javascript:ChangeButtonsImage('excluir_imagem', 'out', this);" OnClick="btnExluirMiniatura_Click" />
                            <asp:Image ID="imgBannerMiniatura" runat="server" ImageUrl="~/ShowFile.aspx?action=1" />
                        </div>
                    </fieldset>
                </asp:Panel>
                <asp:Panel ID="pnlMiniatura_en" runat="server" Visible="false">
                    <fieldset class="Fieldset">
                        <legend>..:: Miniatura Atual (Inglês)</legend>
                        <div>
                            <asp:ImageButton ID="btnExcluirMiniaturaEn" ImageUrl="~/Admin/Media/Images/botao_excluir_2.png"
                                runat="server" onmouseover="javascript:ChangeButtonsImage('excluir_imagem', 'over', this);"
                                onmouseout="javascript:ChangeButtonsImage('excluir_imagem', 'out', this);" OnClick="btnExcluirMiniaturaEn_Click" />
                            <asp:Image ID="imgBannerMiniatura_en" runat="server" ImageUrl="~/ShowFile.aspx?action=1" />
                        </div>
                    </fieldset>
                </asp:Panel>
            </asp:Panel>
            <uc2:ucButtons ID="ucButtons1" runat="server" />
            <uc1:ucShowException ID="ucShowException1" runat="server" />
            <asp:Panel ID="pnlViewCollection" runat="server">
                <div class="Fields" style="margin-top: 10px;">
                    <p>
                        <asp:Label ID="Label2" runat="server" Text="Descrição:" AssociatedControlID="txtFiltroDescricao"
                            CssClass="LabelFilters" />
                        <asp:TextBox runat="server" ID="txtFiltroDescricao" MaxLength="255" />
                    </p>
                    <p>
                        <asp:Label ID="Label5" runat="server" Text="Posição:" AssociatedControlID="ddlId_banner_posicao"
                            CssClass="LabelFilters" />
                        <asp:DropDownList ID="ddlFiltroBannerPosicao" runat="server" OnDataBound="ddlId_banner_posicao_DataBound" />
                    </p>
                    <p>
                        <asp:Label ID="Label6" runat="server" Text="Situação:" AssociatedControlID="ddlFiltroSituacao"
                            CssClass="LabelFilters" />
                        <asp:DropDownList ID="ddlFiltroSituacao" runat="server">
                            <asp:ListItem Selected="True" Value="1">Ativo</asp:ListItem>
                            <asp:ListItem Value="2">Inativo</asp:ListItem>
                        </asp:DropDownList>
                        <asp:ImageButton ID="btnFiltrar" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/botao_pesquisar_1.png"
                            OnClick="btnFiltrar_Click" onmouseover="javascript:ChangeButtonsImage('pesquisar', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('pesquisar', 'out', this);" CausesValidation="False"
                            Style="margin-left: 50px;" />
                    </p>
                </div>
                <asp:GridView runat="server" ID="gvView" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataSourceID="edsView" Width="100%" CssClass="GridView" OnRowCommand="gvView_RowCommand"
                    DataKeyNames="ID_BANNER" CellPadding="4" GridLines="None" OnRowDataBound="gvView_RowDataBound"
                    OnSelectedIndexChanged="gvView_SelectedIndexChanged">
                    <AlternatingRowStyle CssClass="gvAlternatingRowStyle" />
                    <Columns>
                        <asp:BoundField DataField="ID_BANNER" HeaderText="Código" ReadOnly="True" SortExpression="ID_BANNER"
                            ItemStyle-CssClass="gvCenter" />
                        <asp:TemplateField HeaderText="Posição" SortExpression="ID_BANNER_POSICAO">
                            <ItemTemplate>
                                <asp:Literal ID="ltrPosicao" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCRICAO" HeaderText="Descrição" SortExpression="DESCRICAO"
                            ReadOnly="True" ItemStyle-CssClass="gvCenter" />
                        <asp:TemplateField HeaderText="Situação" SortExpression="ID_BANNER_POSICAO_SITUACAO">
                            <ItemTemplate>
                                <asp:Literal ID="ltrSituacao" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEditar" runat="server" CausesValidation="False" CommandName="Select"
                                    ImageUrl="~/Admin/Media/Images/GridViews/edit.png" Text="Select" ToolTip="Exibir/Editar" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnExcluir" runat="server" CausesValidation="False" CommandName="Excluir"
                                    OnClientClick="javascript:return confirm('Deseja excluir este registro?');" ImageUrl="~/Admin/Media/Images/GridViews/delete.png"
                                    CommandArgument='<%# Bind("ID_BANNER") %>' ToolTip="Excluir" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle CssClass="EditRowStyle" />
                    <%--<FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />--%>
                    <HeaderStyle CssClass="gvHeaderStyle" />
                    <PagerSettings Position="TopAndBottom" />
                    <PagerStyle CssClass="gvPagerStyle" />
                    <PagerTemplate>
                        <asp:ImageButton ID="btnGvFirst" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/first.png"
                            CommandName="FirstPage" ToolTip="Primeira Página" />
                        <asp:ImageButton ID="btnGvBack" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/back.png"
                            CommandName="BackPage" ToolTip="Página Anterior" />
                        <asp:TextBox ID="txtGvPage" runat="server" Text="<%# gvView.PageIndex + 1 %>" MaxLength="4"
                            Width="30px" CssClass="gvPagerTextBox" />
                        <asp:Label ID="ltrPager" Text="de" runat="server" CssClass="gvPagerLiteral" EnableTheming="false" />
                        <asp:TextBox ID="txtGvPages" runat="server" Text="<%# gvView.PageCount %>" Enabled="false"
                            ReadOnly="true" Width="30px" CssClass="gvPagerTextBox" />
                        <asp:ImageButton ID="btnGvGo" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/botao_ir_1.png"
                            CommandName="GoToPage" Width="16px" ToolTip="Ir para a página" onmouseover="javascript:ChangeButtonsImage('gotopage', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('gotopage', 'out', this);" />
                        <asp:ImageButton ID="btnGvNext" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/next.png"
                            CommandName="NextPage" ToolTip="Próxima Página" />
                        <asp:ImageButton ID="btnGvLast" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/last.png"
                            CommandName="LastPage" ToolTip="Última Página" />
                    </PagerTemplate>
                    <RowStyle CssClass="gvRowStyle" />
                    <SelectedRowStyle CssClass="gvSelectedRowStyle" />
                    <SortedAscendingCellStyle CssClass="gvSortedAscendingCellStyle" />
                    <SortedAscendingHeaderStyle CssClass="gvSortedAscendingHeaderStyle" />
                    <SortedDescendingCellStyle CssClass="gvSortedDescendingCellStyle" />
                    <SortedDescendingHeaderStyle CssClass="gvSortedDescendingHeaderStyle" />
                </asp:GridView>
                <asp:EntityDataSource ID="edsView" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                    DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="BANNER"
                    EntityTypeFilter="BANNER" OnQueryCreated="edsView_QueryCreated" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
