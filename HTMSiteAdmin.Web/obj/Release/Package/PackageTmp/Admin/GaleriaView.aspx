﻿<%@ Page Title=":: MasterPainel - HTM Gestão e Tecnologia ::" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true"
    CodeBehind="GaleriaView.aspx.cs" Inherits="HTMSiteAdmin.Web.Admin.GaleriaView" %>

<%@ Register Src="UserControls/ucShowException.ascx" TagName="ucShowException" TagPrefix="uc1" %>
<%@ Register Src="UserControls/Bars/ucButtons.ascx" TagName="ucButtons" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upView" runat="server">
        <ContentTemplate>
            <h1>
                ..:: Cadastro de Galerias
            </h1>
            <asp:Panel ID="pnlView" runat="server">
                <fieldset class="Fieldset">
                    <legend>:: Dados Básicos</legend>
                    <div class="Fields">
                        <p>
                            <asp:Label ID="lblDescricao" runat="server" Text="Descrição:" AssociatedControlID="txtDescricao"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtDescricao" MaxLength="255" CssClass="Field" Width="300px" />
                            <span class="Validators">*</span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescricao"
                                CssClass="Validators" Display="Dynamic" ErrorMessage="Campo Requerido" SetFocusOnError="True"
                                ValidationGroup="vgView" />
                        </p>
                        <p>
                            <asp:Label ID="Label9" runat="server" Text="Descrição (Inglês):" AssociatedControlID="txtDescricao_en"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtDescricao_en" MaxLength="255" CssClass="Field"
                                Width="300px" />
                        </p>
                        <p>
                            <asp:Label ID="Label6" runat="server" Text="Detalhes:" AssociatedControlID="txtSubDescricao"
                                CssClass="LabelTop" />
                            <asp:TextBox runat="server" ID="txtSubDescricao" MaxLength="1000" CssClass="Field"
                                Width="300px" Rows="3" TextMode="MultiLine" />
                        </p>
                        <p>
                            <asp:Label ID="Label8" runat="server" Text="Detalhes (Inglês):" AssociatedControlID="txtSubDescricao_en"
                                CssClass="LabelTop" />
                            <asp:TextBox runat="server" ID="txtSubDescricao_en" MaxLength="1000" CssClass="Field"
                                Width="300px" Rows="3" TextMode="MultiLine" />
                        </p>
                        <p>
                            <asp:Label ID="lblId_galeria_tipo" runat="server" Text="Tipo:" AssociatedControlID="ddlId_galeria_tipo"
                                CssClass="Label" />
                            <asp:DropDownList runat="server" ID="ddlId_galeria_tipo" CssClass="Field" DataSourceID="edsGaleriaTipo"
                                DataTextField="DESCRICAO" DataValueField="ID_GALERIA_TIPO" OnDataBound="ddlId_galeria_tipo_DataBound" />
                            <span class="Validators">*</span>
                            <asp:CompareValidator ID="rfvId_galeria_tipo" ErrorMessage="Campo Requerido" ControlToValidate="ddlId_galeria_tipo"
                                runat="server" CssClass="Validators" Display="Dynamic" Operator="GreaterThan"
                                Type="Integer" ValidationGroup="vgView" ValueToCompare="0" />
                            <asp:EntityDataSource ID="edsGaleriaTipo" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                                DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="GALERIA_TIPO"
                                EntityTypeFilter="GALERIA_TIPO" Select="it.[ID_GALERIA_TIPO], it.[DESCRICAO]" />
                        </p>
                        <p>
                            <asp:Label ID="Label4" runat="server" Text="Período de publicação:" AssociatedControlID="txtData_publicacao_inicio"
                                CssClass="Label" />
                            <asp:TextBox ID="txtData_publicacao_inicio" runat="server" Width="80px" MaxLength="1"
                                Style="text-align: justify" ValidationGroup="MKEData_publicacao_inicio" CssClass="Field" />
                            <asp:ImageButton ID="btnData_publicacao_inicio" runat="server" ImageUrl="~/Admin/Media/Images/Calendar_scheduleHS.png"
                                CausesValidation="False" />
                            <ajaxToolkit:MaskedEditExtender ID="mskeeData_publicacao_inicio" runat="server" TargetControlID="txtData_publicacao_inicio"
                                Mask="99/99/9999" MessageValidatorTip="true" MaskType="Date" DisplayMoney="Left"
                                AcceptNegative="Left" ErrorTooltipEnabled="True" />
                            <ajaxToolkit:MaskedEditValidator ID="mskevData_publicacao_inicio" runat="server"
                                ControlExtender="mskeeData_publicacao_inicio" ControlToValidate="txtData_publicacao_inicio"
                                InvalidValueMessage="Data Inválida" Display="Dynamic" InvalidValueBlurredMessage="Data Inválida"
                                ValidationGroup="MKEData_publicacao_inicio" CssClass="Validators" IsValidEmpty="False"
                                SetFocusOnError="True" />
                            <ajaxToolkit:CalendarExtender ID="ceData_publicacao_inicio" runat="server" TargetControlID="txtData_publicacao_inicio"
                                PopupButtonID="btnData_publicacao_inicio" />
                            <asp:Label ID="Label7" runat="server" Text="até" AssociatedControlID="txtData_publicacao_fim"
                                CssClass="Label" Width="30px" Style="text-align: center;" />
                            <asp:TextBox ID="txtData_publicacao_fim" runat="server" Width="80px" MaxLength="1"
                                Style="text-align: justify" ValidationGroup="MKEData_publicacao_fim" CssClass="Field" />
                            <asp:ImageButton ID="btnData_publicacao_fim" runat="server" ImageUrl="~/Admin/Media/Images/Calendar_scheduleHS.png"
                                CausesValidation="False" />
                            <ajaxToolkit:MaskedEditExtender ID="mskeeData_publicacao_fim" runat="server" TargetControlID="txtData_publicacao_fim"
                                Mask="99/99/9999" MessageValidatorTip="true" MaskType="Date" DisplayMoney="Left"
                                AcceptNegative="Left" ErrorTooltipEnabled="True" />
                            <ajaxToolkit:MaskedEditValidator ID="mskevData_publicacao_fim" runat="server" ControlExtender="mskeeData_publicacao_fim"
                                ControlToValidate="txtData_publicacao_fim" InvalidValueMessage="Data Inválida"
                                Display="Dynamic" InvalidValueBlurredMessage="Data Inválida" ValidationGroup="MKEData_publicacao_fim"
                                CssClass="Validators" IsValidEmpty="False" SetFocusOnError="True" />
                            <ajaxToolkit:CalendarExtender ID="ceData_publicacao_fim" runat="server" TargetControlID="txtData_publicacao_fim"
                                PopupButtonID="btnData_publicacao_fim" />
                        </p>
                        <p>
                            <asp:Label ID="Label1" runat="server" Text="Situação:" AssociatedControlID="ddlSituacao"
                                CssClass="Label" />
                            <asp:DropDownList ID="ddlSituacao" runat="server" CssClass="Field">
                                <asp:ListItem Selected="True" Value="1">Ativo</asp:ListItem>
                                <asp:ListItem Value="2">Inativo</asp:ListItem>
                            </asp:DropDownList>
                        </p>
                        <p>
                            <asp:Label ID="lblObservacao" runat="server" Text="Observações:" AssociatedControlID="txtObservacao"
                                CssClass="LabelTop" />
                            <asp:TextBox runat="server" ID="txtObservacao" MaxLength="255" CssClass="Field" Rows="3"
                                TextMode="MultiLine" Width="300px" />
                        </p>
                    </div>
                </fieldset>
                <fieldset class="Fieldset">
                    <legend>..:: Imagens da galeria</legend>
                    <div>
                        <asp:Label ID="Label3" runat="server" Text="Arquivo:" AssociatedControlID="afuArquivoImagem"
                            CssClass="Label" Style="float: left;" />
                        <ajaxToolkit:AsyncFileUpload ID="afuArquivoImagem" runat="server" Style="display: inline-block;
                            float: left; margin-left: 2px;" OnUploadedComplete="afuArquivoImagem_UploadedComplete"
                            UploadingBackColor="Yellow" />
                        <asp:ImageButton ID="btnAtualizarImagensGaleria" runat="server" OnClick="btnAtualizarImagensGaleria_Click"
                            ValidationGroup="vgView" ImageUrl="~/Admin/Media/Images/Bars/Buttons/botao_adicionar_1.png"
                            ToolTip="Adicionar Imagem" onmouseover="javascript:ChangeButtonsImage('Add', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('Add', 'out', this);" />
                    </div>
                    <uc1:ucShowException ID="ucShowException2" runat="server" />
                    <asp:GridView runat="server" ID="gvImagensGaleria" CssClass="GridView" Width="100%"
                        AutoGenerateColumns="False" DataKeyNames="ID_ARQUIVO_DIGITAL" OnRowDataBound="gvImagensGaleria_RowDataBound"
                        OnRowCommand="gvImagensGaleria_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Imagem">
                                <ItemTemplate>
                                    <asp:Image ID="imgItemGaleria" runat="server" ImageUrl="~/ShowFile.aspx?action=1" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Configurações">
                                <ItemTemplate>
                                    <div class="Fields">
                                        <p>
                                            <asp:Label ID="lblDescricao" runat="server" Text="Descrição:" AssociatedControlID="txtGvDetalhes"
                                                CssClass="Label" />
                                            <asp:TextBox runat="server" ID="txtGvDetalhes" MaxLength="255" CssClass="Field" Width="300px" />
                                        </p>
                                        <p>
                                            <asp:Label ID="Label10" runat="server" Text="Descrição (Inglês):" AssociatedControlID="txtGvDetalhes_en"
                                                CssClass="Label" />
                                            <asp:TextBox runat="server" ID="txtGvDetalhes_en" MaxLength="255" CssClass="Field"
                                                Width="300px" />
                                        </p>
                                        <p>
                                            <asp:Label ID="Label3" runat="server" Text="Ordem:" AssociatedControlID="txtGvOrdem"
                                                CssClass="Label" />
                                            <asp:TextBox runat="server" ID="txtGvOrdem" MaxLength="255" CssClass="Field" Width="300px" />
                                            <asp:Label ID="Label5" runat="server" Text="Principal:" AssociatedControlID="chkImagemPrincipal"
                                                CssClass="Label" Width="50px" />
                                            <asp:CheckBox runat="server" ID="chkImagemPrincipal" />
                                        </p>
                                        <p>
                                            Tamanho Original: L px x A px - Tamanho do Arquivo: xxxxx Kb.
                                        </p>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnSalvarAlteracoes" runat="server" CausesValidation="true"
                                        ImageUrl="~/Admin/Media/Images/GridViews/icone_salvar_1.png" ValidationGroup="vgView"
                                        CommandName="SaveImage" CommandArgument='<%# Container.DataItemIndex %>' Text="Salvar Alterações" />
                                </ItemTemplate>
                                <ItemStyle CssClass="gvCommandFieldItem" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnExcluir" runat="server" CausesValidation="False" CommandName="Excluir"
                                        OnClientClick="javascript:return confirm('Deseja excluir este registro?');" ImageUrl="~/Admin/Media/Images/GridViews/delete.png"
                                        CommandArgument='<%# Container.DataItemIndex %>' ToolTip="Excluir" />
                                </ItemTemplate>
                                <ItemStyle CssClass="gvCommandFieldItem" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </fieldset>
            </asp:Panel>
            <uc2:ucButtons ID="ucButtons1" runat="server" />
            <asp:Panel ID="pnlViewCollection" runat="server">
                <div class="Fields">
                    <p>
                        <asp:Label ID="lblFiltroGaleriaTipo" runat="server" Text="Tipo:" AssociatedControlID="ddlFiltroGaleriaTipo"
                            CssClass="LabelFilters" />
                        <asp:DropDownList runat="server" ID="ddlFiltroGaleriaTipo" CssClass="Field" DataSourceID="edsFiltroGaleriaTipo"
                            DataTextField="DESCRICAO" DataValueField="ID_GALERIA_TIPO" OnDataBound="ddlFiltroGaleriaTipo_DataBound" />
                        <asp:EntityDataSource ID="edsFiltroGaleriaTipo" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                            DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="GALERIA_TIPO"
                            EntityTypeFilter="GALERIA_TIPO" Select="it.[ID_GALERIA_TIPO], it.[DESCRICAO]" />
                    </p>
                    <p>
                        <asp:Label ID="Label2" runat="server" Text="Descrição:" AssociatedControlID="txtFiltroDescricao"
                            CssClass="LabelFilters" />
                        <asp:TextBox runat="server" ID="txtFiltroDescricao" MaxLength="255" Width="250px"
                            CssClass="Field" />
                        <asp:ImageButton ID="btnFiltrar" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/botao_pesquisar_1.png"
                            OnClick="btnFiltrar_Click" onmouseover="javascript:ChangeButtonsImage('pesquisar', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('pesquisar', 'out', this);" Style="margin-left: 50px;" />
                    </p>
                </div>
                <asp:GridView runat="server" ID="gvView" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="ID_GALERIA" DataSourceID="edsView" Width="100%" CssClass="GridView"
                    OnRowCommand="gvView_RowCommand" CellPadding="4" GridLines="None" OnRowDataBound="gvView_RowDataBound"
                    OnSelectedIndexChanged="gvView_SelectedIndexChanged">
                    <AlternatingRowStyle CssClass="gvAlternatingRowStyle" />
                    <Columns>
                        <asp:BoundField DataField="ID_GALERIA" HeaderText="Código" ReadOnly="True" SortExpression="ID_GALERIA"
                            ItemStyle-CssClass="gvCenter" ItemStyle-Width="40px" />
                        <asp:BoundField DataField="DESCRICAO" HeaderText="Descrição" SortExpression="DESCRICAO"
                            ItemStyle-CssClass="gvCenter" />
                        <asp:TemplateField HeaderText="Tipo" SortExpression="ID_GALERIA_TIPO">
                            <ItemTemplate>
                                <asp:Literal ID="ltrTipo" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEditar" runat="server" CausesValidation="False" CommandName="Select"
                                    ImageUrl="~/Admin/Media/Images/GridViews/edit.png" Text="Select" ToolTip="Exibir/Editar" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnExcluir" runat="server" CausesValidation="False" CommandName="Excluir"
                                    OnClientClick="javascript:return confirm('Deseja excluir este registro?');" ImageUrl="~/Admin/Media/Images/GridViews/delete.png"
                                    CommandArgument='<%# Bind("ID_GALERIA") %>' ToolTip="Excluir" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle CssClass="EditRowStyle" />
                    <HeaderStyle CssClass="gvHeaderStyle" />
                    <PagerSettings Position="TopAndBottom" />
                    <PagerStyle CssClass="gvPagerStyle" />
                    <PagerTemplate>
                        <asp:ImageButton ID="btnGvFirst" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/first.png"
                            CommandName="FirstPage" ToolTip="Primeira Página" />
                        <asp:ImageButton ID="btnGvBack" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/back.png"
                            CommandName="BackPage" ToolTip="Página Anterior" />
                        <asp:TextBox ID="txtGvPage" runat="server" Text="<%# gvView.PageIndex + 1 %>" MaxLength="4"
                            Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:Label ID="ltrPager" Text="de" runat="server" CssClass="gvPagerLiteral" EnableTheming="false" />
                        <asp:TextBox ID="txtGvPages" runat="server" Text="<%# gvView.PageCount %>" Enabled="false"
                            ReadOnly="true" Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:ImageButton ID="btnGvGo" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/botao_ir_1.png"
                            CommandName="GoToPage" ToolTip="Ir para a página" onmouseover="javascript:ChangeButtonsImage('gotopage', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('gotopage', 'out', this);" />
                        <asp:ImageButton ID="btnGvNext" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/next.png"
                            CommandName="NextPage" ToolTip="Próxima Página" />
                        <asp:ImageButton ID="btnGvLast" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/last.png"
                            CommandName="LastPage" ToolTip="Última Página" />
                    </PagerTemplate>
                    <RowStyle CssClass="gvRowStyle" />
                    <SelectedRowStyle CssClass="gvSelectedRowStyle" />
                    <SortedAscendingCellStyle CssClass="gvSortedAscendingCellStyle" />
                    <SortedAscendingHeaderStyle CssClass="gvSortedAscendingHeaderStyle" />
                    <SortedDescendingCellStyle CssClass="gvSortedDescendingCellStyle" />
                    <SortedDescendingHeaderStyle CssClass="gvSortedDescendingHeaderStyle" />
                </asp:GridView>
                <asp:EntityDataSource ID="edsView" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                    DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="GALERIA"
                    EntityTypeFilter="GALERIA" OnQueryCreated="edsView_QueryCreated" />
            </asp:Panel>
            <uc1:ucShowException ID="ucShowException1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
