﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true"
    CodeBehind="ImportacaoClienteProdutoUpload.aspx.cs" Inherits="HTMSiteAdmin.Web.Admin.ImportacaoClienteProdutoUpload" %>

<%@ Register Src="UserControls/ucShowException.ascx" TagName="ucShowException" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upView" runat="server">
        <ContentTemplate>
            <h1>
                ..:: Importação de Clientes x Produtos
            </h1>
            <asp:Panel ID="pnlView" runat="server">
                <fieldset class="Fieldset">
                    <legend>:: Arquivo</legend>
                    <div style="display: block; width: 100%">
                        <asp:Label ID="Label9" runat="server" Text="Arquivo:" AssociatedControlID="afuArquivo"
                            CssClass="Label" Style="float: left;" />
                        <ajaxToolkit:AsyncFileUpload ID="afuArquivo" runat="server" Style="display: inline-block;
                            float: left; margin-left: 2px;" OnUploadedComplete="afuArquivo_UploadedComplete" />
                    </div>
                    <div class="Fields">
                        <p>
                            <asp:Label ID="Label1" runat="server" Text="&nbsp;" AssociatedControlID="btnLerArquivo"
                                CssClass="Label" />
                            <asp:ImageButton ImageUrl="~/Admin/Media/Images/botao_ler_arquivo_1.png" runat="server"
                                ID="btnLerArquivo" OnClick="btnLerArquivo_Click" onmouseover="javascript:ChangeButtonsImage('ler_arquivo', 'over', this);"
                                onmouseout="javascript:ChangeButtonsImage('ler_arquivo', 'out', this);" />
                            <asp:ImageButton ImageUrl="~/Admin/Media/Images/botao_enviar_dados_1.png" runat="server"
                                ID="btnEnviarDados" OnClick="btnEnviarDados_Click" onmouseover="javascript:ChangeButtonsImage('enviar_dados', 'over', this);"
                                onmouseout="javascript:ChangeButtonsImage('enviar_dados', 'out', this);" />
                        </p>
                        <p>
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Admin/Media/ModelosImportacao/ClienteProduto.csv">Download Modelo</asp:HyperLink></p>
                    </div>
                </fieldset>
            </asp:Panel>
            <uc1:ucShowException ID="ucShowException1" runat="server" />
            <asp:Panel ID="pnlViewCollection" runat="server">
                <asp:GridView runat="server" ID="gvView" Width="100%" CssClass="GridView" CellPadding="4"
                    GridLines="None" AutoGenerateColumns="False" OnRowDataBound="gvView_RowDataBound">
                    <AlternatingRowStyle CssClass="gvAlternatingRowStyle" />
                    <Columns>
                        <asp:BoundField HeaderText="Código" DataField="ID_CLIENTE_PRODUTO" ItemStyle-CssClass="gvCenter"
                            ItemStyle-Width="45px">
                            <ItemStyle CssClass="gvCenter" Width="45px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Cliente">
                            <ItemTemplate>
                                <asp:Literal ID="ltrCliente" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Produto">
                            <ItemTemplate>
                                <asp:Literal ID="ltrProduto" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle CssClass="EditRowStyle" />
                    <HeaderStyle CssClass="gvHeaderStyle" />
                    <PagerSettings Position="TopAndBottom" />
                    <PagerStyle CssClass="gvPagerStyle" />
                    <PagerTemplate>
                        <asp:ImageButton ID="btnGvFirst" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/first.png"
                            CommandName="FirstPage" ToolTip="Primeira Página" />
                        <asp:ImageButton ID="btnGvBack" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/back.png"
                            CommandName="BackPage" ToolTip="Página Anterior" />
                        <asp:TextBox ID="txtGvPage" runat="server" Text="<%# gvView.PageIndex + 1 %>" MaxLength="4"
                            Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:Label ID="ltrPager" Text="de" runat="server" CssClass="gvPagerLiteral" EnableTheming="false" />
                        <asp:TextBox ID="txtGvPages" runat="server" Text="<%# gvView.PageCount %>" Enabled="false"
                            ReadOnly="true" Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:ImageButton ID="btnGvGo" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/botao_ir_1.png"
                            CommandName="GoToPage" ToolTip="Ir para a página" onmouseover="javascript:ChangeButtonsImage('gotopage', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('gotopage', 'out', this);" />
                        <asp:ImageButton ID="btnGvNext" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/next.png"
                            CommandName="NextPage" ToolTip="Próxima Página" />
                        <asp:ImageButton ID="btnGvLast" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/last.png"
                            CommandName="LastPage" ToolTip="Última Página" />
                    </PagerTemplate>
                    <RowStyle CssClass="gvRowStyle" />
                    <SelectedRowStyle CssClass="gvSelectedRowStyle" />
                    <SortedAscendingCellStyle CssClass="gvSortedAscendingCellStyle" />
                    <SortedAscendingHeaderStyle CssClass="gvSortedAscendingHeaderStyle" />
                    <SortedDescendingCellStyle CssClass="gvSortedDescendingCellStyle" />
                    <SortedDescendingHeaderStyle CssClass="gvSortedDescendingHeaderStyle" />
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
