﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true"
    CodeBehind="ImportacaoClienteProdutoCertificadoView.aspx.cs" Inherits="HTMSiteAdmin.Web.Admin.ImportacaoClienteProdutoCertificadoView" %>

<%@ Register Src="UserControls/ucShowException.ascx" TagName="ucShowException" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upView" runat="server">
        <ContentTemplate>
            <h1>
                ..:: Cliente x Produto X Certificado</h1>
            <fieldset class="Fieldset">
                <legend>:: Clientes</legend>
                <div class="Fields">
                    <p>
                        <asp:Label ID="lblNome" runat="server" Text="Cliente:" AssociatedControlID="ddlCliente"
                            CssClass="Label" />
                        <asp:DropDownList runat="server" ID="ddlCliente" DataSourceID="edsClientes" DataTextField="NOME_RAZAO"
                            DataValueField="ID_CLIENTE" AutoPostBack="True" OnSelectedIndexChanged="ddlCliente_SelectedIndexChanged"
                            OnDataBound="ddlCliente_DataBound" CssClass="Field" />
                        <asp:EntityDataSource ID="edsClientes" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                            DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="IMPORTACAO_CLIENTE"
                            EntityTypeFilter="IMPORTACAO_CLIENTE" 
                            Select="it.[ID_CLIENTE], it.[NOME_RAZAO]" OrderBy="it.[NOME_RAZAO]" />
                    </p>
                    <asp:Panel runat="server" ID="pnlProduto" Visible="false">
                        <p>
                            <asp:Label ID="Label1" runat="server" Text="Produto:" AssociatedControlID="ddlProduto"
                                CssClass="Label" />
                            <asp:DropDownList runat="server" ID="ddlProduto" DataTextField="NOME" DataValueField="ID_PRODUTO"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlProduto_SelectedIndexChanged"
                                OnDataBound="ddlProduto_DataBound" CssClass="Field" />
                        </p>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlCertificado" Visible="false">
                        <p>
                            <asp:Label ID="Label2" runat="server" Text="Certificado:" AssociatedControlID="ddlCertificado"
                                CssClass="Label" />
                            <asp:DropDownList runat="server" ID="ddlCertificado" DataTextField="NOME" DataValueField="ID_CERTIFICADO"
                                AutoPostBack="True" OnDataBound="ddlCertificado_DataBound" OnSelectedIndexChanged="ddlCertificado_SelectedIndexChanged"
                                CssClass="Field" />
                        </p>
                    </asp:Panel>
                </div>
            </fieldset>
            <asp:Panel runat="server" ID="pnlProdutosVinculados" Visible="false">
                <fieldset class="Fieldset">
                    <legend>:: Produtos já vinculados</legend>
                    <asp:GridView runat="server" ID="gvView" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                        DataSourceID="edsView" Width="100%" CssClass="GridView" OnRowCommand="gvView_RowCommand"
                        DataKeyNames="ID_CLIENTE, ID_PRODUTO, ID_CERTIFICADO" CellPadding="4" GridLines="None"
                        OnRowDataBound="gvView_RowDataBound">
                        <AlternatingRowStyle CssClass="gvAlternatingRowStyle" />
                        <Columns>
                            <asp:TemplateField HeaderText="Produto" SortExpression="ID_PRODUTO">
                                <ItemTemplate>
                                    <asp:Literal ID="ltrProduto" runat="server" />
                                </ItemTemplate>
                                <ItemStyle CssClass="gvCenter" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Certificado" SortExpression="ID_CERTIFICADO">
                                <ItemTemplate>
                                    <asp:Literal ID="ltrCertificado" runat="server" />
                                </ItemTemplate>
                                <ItemStyle CssClass="gvCenter" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Origem" SortExpression="REGISTRO_ORIGEM">
                                <ItemTemplate>
                                    <asp:Literal ID="ltrRegistroOrigem" runat="server" />
                                </ItemTemplate>
                                <ItemStyle CssClass="gvCenter" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnExcluir" runat="server" CausesValidation="False" CommandName="Excluir"
                                        OnClientClick="javascript:return confirm('Deseja excluir este registro?');" ImageUrl="~/Admin/Media/Images/GridViews/delete.png"
                                        CommandArgument='<%# Container.DataItemIndex %>' ToolTip="Excluir" />
                                </ItemTemplate>
                                <ItemStyle CssClass="gvCommandFieldItem" />
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle CssClass="EditRowStyle" />
                        <HeaderStyle CssClass="gvHeaderStyle" />
                        <PagerSettings Position="TopAndBottom" />
                        <PagerStyle CssClass="gvPagerStyle" />
                        <PagerTemplate>
                            <asp:ImageButton ID="btnGvFirst" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/first.png"
                                CommandName="FirstPage" ToolTip="Primeira Página" />
                            <asp:ImageButton ID="btnGvBack" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/back.png"
                                CommandName="BackPage" ToolTip="Página Anterior" />
                            <asp:TextBox ID="txtGvPage" runat="server" Text="<%# gvView.PageIndex + 1 %>" MaxLength="4"
                                Width="30px" CssClass="gvPagerTextBox Field" />
                            <asp:Label ID="ltrPager" Text="de" runat="server" CssClass="gvPagerLiteral" EnableTheming="false" />
                            <asp:TextBox ID="txtGvPages" runat="server" Text="<%# gvView.PageCount %>" Enabled="false"
                                ReadOnly="true" Width="30px" CssClass="gvPagerTextBox Field" />
                            <asp:ImageButton ID="btnGvGo" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/botao_ir_1.png"
                                CommandName="GoToPage" ToolTip="Ir para a página" onmouseover="javascript:ChangeButtonsImage('gotopage', 'over', this);"
                                onmouseout="javascript:ChangeButtonsImage('gotopage', 'out', this);" />
                            <asp:ImageButton ID="btnGvNext" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/next.png"
                                CommandName="NextPage" ToolTip="Próxima Página" />
                            <asp:ImageButton ID="btnGvLast" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/last.png"
                                CommandName="LastPage" ToolTip="Última Página" />
                        </PagerTemplate>
                        <RowStyle CssClass="gvRowStyle" />
                        <SelectedRowStyle CssClass="gvSelectedRowStyle" />
                        <SortedAscendingCellStyle CssClass="gvSortedAscendingCellStyle" />
                        <SortedAscendingHeaderStyle CssClass="gvSortedAscendingHeaderStyle" />
                        <SortedDescendingCellStyle CssClass="gvSortedDescendingCellStyle" />
                        <SortedDescendingHeaderStyle CssClass="gvSortedDescendingHeaderStyle" />
                    </asp:GridView>
                    <asp:EntityDataSource ID="edsView" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                        DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="IMPORTACAO_PRODUTO_CERTIFICADO"
                        EntityTypeFilter="IMPORTACAO_PRODUTO_CERTIFICADO" OnQueryCreated="edsView_QueryCreated" />
                </fieldset>
            </asp:Panel>
            <uc1:ucShowException ID="ucShowException1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
