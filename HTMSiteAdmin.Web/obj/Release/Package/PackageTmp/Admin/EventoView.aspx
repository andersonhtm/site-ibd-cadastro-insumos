﻿<%@ Page Title=":: MasterPainel - HTM Gestão e Tecnologia ::" Language="C#" MasterPageFile="~/Admin/Admin.Master"
    AutoEventWireup="true" CodeBehind="EventoView.aspx.cs" Inherits="HTMSiteAdmin.Web.Admin.EventoView" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="UserControls/ucShowException.ascx" TagName="ucShowException" TagPrefix="uc1" %>
<%@ Register Src="UserControls/Bars/ucButtons.ascx" TagName="ucButtons" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upView" runat="server">
        <ContentTemplate>
            <h1>
                ..:: Evento
            </h1>
            <asp:Panel ID="pnlView" runat="server">
                <fieldset class="Fieldset">
                    <legend>:: Dados Básicos</legend>
                    <div class="Fields">
                        <p>
                            <asp:Label ID="lblNome" runat="server" Text="Descrição:" AssociatedControlID="txtDescricao"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtDescricao" MaxLength="100" Width="300px" CssClass="Field" />
                            <span class="Validators">*</span>
                            <asp:RequiredFieldValidator ID="rfvDescricao" runat="server" ControlToValidate="txtDescricao"
                                CssClass="Validators" Display="Dynamic" ErrorMessage="Campo Requerido" ValidationGroup="vgView"></asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <asp:Label ID="Label1" runat="server" Text="Descrição (Inglês):" AssociatedControlID="txtDescricao_en"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtDescricao_en" MaxLength="100" Width="300px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="Label4" runat="server" Text="Detalhes:" AssociatedControlID="txtSubDescricao"
                                CssClass="LabelTop" />
                            <asp:TextBox runat="server" ID="txtSubDescricao" MaxLength="1000" Width="300px" CssClass="Field"
                                Rows="3" TextMode="MultiLine" />
                        </p>
                        <p>
                            <asp:Label ID="Label5" runat="server" Text="Detalhes (Inglês):" AssociatedControlID="txtSubDescricao_en"
                                CssClass="LabelTop" />
                            <asp:TextBox runat="server" ID="txtSubDescricao_en" MaxLength="1000" Width="300px"
                                CssClass="Field" Rows="3" TextMode="MultiLine" />
                        </p>
                        <p>
                            <asp:Label ID="Label6" runat="server" Text="Duração:" AssociatedControlID="txtData_inicio"
                                CssClass="Label" />
                            <asp:TextBox ID="txtData_inicio" runat="server" Width="80px" MaxLength="1" Style="text-align: justify"
                                ValidationGroup="MKEData_inicio" CssClass="Field" />
                            <span class="Validators">*</span>
                            <asp:RequiredFieldValidator ID="rfvData_inicio" runat="server" ControlToValidate="txtData_inicio"
                                CssClass="Validators" Display="Dynamic" ErrorMessage="Campo Requerido" ValidationGroup="vgView"></asp:RequiredFieldValidator>
                            <asp:ImageButton ID="btnData_inicio" runat="server" ImageUrl="~/Admin/Media/Images/Calendar_scheduleHS.png"
                                CausesValidation="False" />
                            <ajaxToolkit:MaskedEditExtender ID="mskeeData_inicio" runat="server" TargetControlID="txtData_inicio"
                                Mask="99/99/9999" MessageValidatorTip="true" MaskType="Date" DisplayMoney="Left"
                                AcceptNegative="Left" ErrorTooltipEnabled="True" />
                            <ajaxToolkit:MaskedEditValidator ID="mskevData_inicio" runat="server" ControlExtender="mskeeData_inicio"
                                ControlToValidate="txtData_inicio" InvalidValueMessage="Data Inválida" Display="Dynamic"
                                InvalidValueBlurredMessage="Data Inválida" ValidationGroup="MKEData_inicio" CssClass="Validators"
                                IsValidEmpty="False" SetFocusOnError="True" />
                            <ajaxToolkit:CalendarExtender ID="ceData_inicio" runat="server" TargetControlID="txtData_inicio"
                                PopupButtonID="btnData_inicio" />
                            <asp:Label ID="Label7" runat="server" Text="até" AssociatedControlID="txtData_fim"
                                CssClass="Label" Width="30px" Style="text-align: center;" />
                            <asp:TextBox ID="txtData_fim" runat="server" Width="80px" MaxLength="1" Style="text-align: justify"
                                ValidationGroup="MKEData_fim" CssClass="Field" />
                            <span class="Validators">*</span>
                            <asp:RequiredFieldValidator ID="rfvData_fim" runat="server" ControlToValidate="txtData_fim"
                                CssClass="Validators" Display="Dynamic" ErrorMessage="Campo Requerido" ValidationGroup="vgView"></asp:RequiredFieldValidator>
                            <asp:ImageButton ID="btnData_fim" runat="server" ImageUrl="~/Admin/Media/Images/Calendar_scheduleHS.png"
                                CausesValidation="False" />
                            <ajaxToolkit:MaskedEditExtender ID="mskeeData_fim" runat="server" TargetControlID="txtData_fim"
                                Mask="99/99/9999" MessageValidatorTip="true" MaskType="Date" DisplayMoney="Left"
                                AcceptNegative="Left" ErrorTooltipEnabled="True" />
                            <ajaxToolkit:MaskedEditValidator ID="mskevData_fim" runat="server" ControlExtender="mskeeData_fim"
                                ControlToValidate="txtData_fim" InvalidValueMessage="Data Inválida" Display="Dynamic"
                                InvalidValueBlurredMessage="Data Inválida" ValidationGroup="MKEData_fim" CssClass="Validators"
                                IsValidEmpty="False" SetFocusOnError="True" />
                            <ajaxToolkit:CalendarExtender ID="ceData_fim" runat="server" TargetControlID="txtData_fim"
                                PopupButtonID="btnData_fim" />
                        </p>
                        <p>
                            <asp:Label ID="Label8" runat="server" Text="Data de Publicação:" AssociatedControlID="txtData_publicacao"
                                CssClass="Label" />
                            <asp:TextBox ID="txtData_publicacao" runat="server" Width="80px" MaxLength="1" Style="text-align: justify"
                                ValidationGroup="MKEData_publicacao" CssClass="Field" />
                            <span class="Validators">*</span>
                            <asp:RequiredFieldValidator ID="rfvData_publicacao" runat="server" ControlToValidate="txtData_publicacao"
                                CssClass="Validators" Display="Dynamic" ErrorMessage="Campo Requerido" ValidationGroup="vgView"></asp:RequiredFieldValidator>
                            <asp:ImageButton ID="btnData_publicacao" runat="server" ImageUrl="~/Admin/Media/Images/Calendar_scheduleHS.png"
                                CausesValidation="False" />
                            <ajaxToolkit:MaskedEditExtender ID="mskeeData_publicacao" runat="server" TargetControlID="txtData_publicacao"
                                Mask="99/99/9999" MessageValidatorTip="true" MaskType="Date" DisplayMoney="Left"
                                AcceptNegative="Left" ErrorTooltipEnabled="True" />
                            <ajaxToolkit:MaskedEditValidator ID="mskevData_publicacao" runat="server" ControlExtender="mskeeData_publicacao"
                                ControlToValidate="txtData_publicacao" InvalidValueMessage="Data Inválida" Display="Dynamic"
                                InvalidValueBlurredMessage="Data Inválida" ValidationGroup="MKEData_publicacao"
                                CssClass="Validators" IsValidEmpty="False" SetFocusOnError="True" />
                            <ajaxToolkit:CalendarExtender ID="ceData_publicacao" runat="server" TargetControlID="txtData_publicacao"
                                PopupButtonID="btnData_publicacao" />
                        </p>
                        <p>
                            <asp:Label ID="lblId_evento_categoria" runat="server" Text="Categoria:" AssociatedControlID="ddlId_evento_categoria"
                                CssClass="Label" />
                            <asp:DropDownList runat="server" ID="ddlId_evento_categoria" CssClass="Field" DataSourceID="edsEventoCategoria"
                                DataTextField="DESCRICAO" DataValueField="ID_EVENTO_CATEGORIA" OnDataBound="ddlId_evento_categoria_DataBound" />
                            <span class="Validators">*</span>
                            <asp:CompareValidator ID="rfvId_galeria_tipo" ErrorMessage="Campo Requerido" ControlToValidate="ddlId_evento_categoria"
                                runat="server" CssClass="Validators" Display="Dynamic" Operator="GreaterThan"
                                Type="Integer" ValidationGroup="vgView" ValueToCompare="0" />
                            <asp:EntityDataSource ID="edsEventoCategoria" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                                DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="EVENTO_CATEGORIA"
                                EntityTypeFilter="EVENTO_CATEGORIA" Select="it.[ID_EVENTO_CATEGORIA], it.[DESCRICAO]" />
                        </p>
                        <p>
                            <asp:Label ID="Label3" runat="server" Text="Situação:" AssociatedControlID="chkAtivo"
                                CssClass="Label" />
                            <asp:CheckBox ID="chkAtivo" Text="Ativo" runat="server" Checked="true" />
                        </p>
                        <p>
                            <asp:Label ID="Label10" runat="server" Text="Observação:" AssociatedControlID="txtObservacao"
                                CssClass="LabelTop" />
                            <asp:TextBox runat="server" ID="txtObservacao" MaxLength="1000" Width="300px" CssClass="Field"
                                Rows="3" TextMode="MultiLine" />
                        </p>
                    </div>
                    <div>
                        <asp:Label ID="Label9" runat="server" Text="Arquivo:" AssociatedControlID="afuArquivoEvento"
                            CssClass="Label" Style="float: left;" />
                        <ajaxToolkit:AsyncFileUpload ID="afuArquivoEvento" runat="server" Style="display: inline-block;
                            float: left; margin-left: 2px;" OnUploadedComplete="afuArquivoEvento_UploadedComplete" />
                    </div>
                </fieldset>
                <asp:Panel ID="pnlArquivoAtual" runat="server" Visible="false">
                    <fieldset class="Fieldset">
                        <legend>..:: Arquivo Atual</legend>
                        <p>
                            <asp:ImageButton ID="btnExcluirImagem" ImageUrl="~/Admin/Media/Images/botao_excluir_2.png"
                                runat="server" onmouseover="javascript:ChangeButtonsImage('excluir_imagem', 'over', this);"
                                onmouseout="javascript:ChangeButtonsImage('excluir_imagem', 'out', this);" OnClick="btnExcluirImagem_Click" />
                        </p>
                        <div>
                            <asp:Image ID="imgEvento" runat="server" ImageUrl="~/ShowFile.aspx?action=1" />
                        </div>
                    </fieldset>
                </asp:Panel>
                <fieldset class="Fieldset">
                    <legend>..:: Descrição Detalhada</legend>
                    <div class="Fields">
                        <p>
                            <CKEditor:CKEditorControl ID="txtDescricaoDetalhada" runat="server" BasePath="~/Scripts/ckeditor"
                                ContentsCss="~/Scripts/ckeditor/contents.css" 
                                TemplatesFiles="~/Scripts/ckeditor/plugins/templates/templates/default.js" 
                                Toolbar="MasterPainelToolbar" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDescricaoDetalhada"
                                CssClass="Validators" Display="Dynamic" ErrorMessage="Campo Requerido" SetFocusOnError="True"
                                ValidationGroup="vgView" />
                        </p>
                    </div>
                </fieldset>
                <fieldset class="Fieldset">
                    <legend>..:: Descrição Detalhada (Inglês)</legend>
                    <div class="Fields">
                        <p>
                            <CKEditor:CKEditorControl ID="txtDescricaoDetalhada_en" runat="server" BasePath="~/Scripts/ckeditor"
                                ContentsCss="~/Scripts/ckeditor/contents.css" 
                                TemplatesFiles="~/Scripts/ckeditor/plugins/templates/templates/default.js" 
                                Toolbar="MasterPainelToolbar" />
                        </p>
                    </div>
                </fieldset>
            </asp:Panel>
            <uc2:ucButtons ID="ucButtons1" runat="server" />
            <asp:Panel ID="pnlViewCollection" runat="server">
                <div class="Fields">
                    <p>
                        <asp:Label ID="Label2" runat="server" Text="Descrição:" AssociatedControlID="txtFiltroDescricao"
                            CssClass="LabelFilters" />
                        <asp:TextBox runat="server" ID="txtFiltroDescricao" MaxLength="255" Width="250px"
                            CssClass="Field" />
                        <asp:ImageButton ID="btnFiltrar" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/botao_pesquisar_1.png"
                            OnClick="btnFiltrar_Click" onmouseover="javascript:ChangeButtonsImage('pesquisar', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('pesquisar', 'out', this);" Style="margin-left: 50px;" />
                    </p>
                </div>
                <asp:GridView runat="server" ID="gvView" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="ID_EVENTO" DataSourceID="edsView" Width="100%" CssClass="GridView"
                    OnRowCommand="gvView_RowCommand" CellPadding="4" GridLines="None" OnRowDataBound="gvView_RowDataBound"
                    OnSelectedIndexChanged="gvView_SelectedIndexChanged">
                    <AlternatingRowStyle CssClass="gvAlternatingRowStyle" />
                    <Columns>
                        <asp:BoundField DataField="ID_EVENTO" HeaderText="Código" ReadOnly="True" SortExpression="ID_EVENTO"
                            ItemStyle-CssClass="gvCenter" ItemStyle-Width="40px" >
                        <ItemStyle CssClass="gvCenter" Width="40px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DESCRICAO" HeaderText="Descrição" SortExpression="DESCRICAO"
                            ItemStyle-CssClass="gvCenter" >
                        <ItemStyle CssClass="gvCenter" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DATA_INICIO" HeaderText="Data Início" SortExpression="DATA_INICIO"
                            ItemStyle-CssClass="gvCenter" >
                         <ItemStyle CssClass="gvCenter" />
                        </asp:BoundField>
                         <asp:BoundField DataField="DATA_FIM" HeaderText="Data Fim" SortExpression="DATA_FIM"
                            ItemStyle-CssClass="gvCenter" >
                        <ItemStyle CssClass="gvCenter" />
                        </asp:BoundField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEditar" runat="server" CausesValidation="False" CommandName="Select"
                                    ImageUrl="~/Admin/Media/Images/GridViews/edit.png" Text="Select" ToolTip="Exibir/Editar" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnExcluir" runat="server" CausesValidation="False" CommandName="Excluir"
                                    OnClientClick="javascript:return confirm('Deseja excluir este registro?');" ImageUrl="~/Admin/Media/Images/GridViews/delete.png"
                                    CommandArgument='<%# Bind("ID_EVENTO") %>' ToolTip="Excluir" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle CssClass="EditRowStyle" />
                    <HeaderStyle CssClass="gvHeaderStyle" />
                    <PagerSettings Position="TopAndBottom" />
                    <PagerStyle CssClass="gvPagerStyle" />
                    <PagerTemplate>
                        <asp:ImageButton ID="btnGvFirst" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/first.png"
                            CommandName="FirstPage" ToolTip="Primeira Página" />
                        <asp:ImageButton ID="btnGvBack" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/back.png"
                            CommandName="BackPage" ToolTip="Página Anterior" />
                        <asp:TextBox ID="txtGvPage" runat="server" Text="<%# gvView.PageIndex + 1 %>" MaxLength="4"
                            Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:Label ID="ltrPager" Text="de" runat="server" CssClass="gvPagerLiteral" EnableTheming="false" />
                        <asp:TextBox ID="txtGvPages" runat="server" Text="<%# gvView.PageCount %>" Enabled="false"
                            ReadOnly="true" Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:ImageButton ID="btnGvGo" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/botao_ir_1.png"
                            CommandName="GoToPage" ToolTip="Ir para a página" onmouseover="javascript:ChangeButtonsImage('gotopage', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('gotopage', 'out', this);" />
                        <asp:ImageButton ID="btnGvNext" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/next.png"
                            CommandName="NextPage" ToolTip="Próxima Página" />
                        <asp:ImageButton ID="btnGvLast" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/last.png"
                            CommandName="LastPage" ToolTip="Última Página" />
                    </PagerTemplate>
                    <RowStyle CssClass="gvRowStyle" />
                    <SelectedRowStyle CssClass="gvSelectedRowStyle" />
                    <SortedAscendingCellStyle CssClass="gvSortedAscendingCellStyle" />
                    <SortedAscendingHeaderStyle CssClass="gvSortedAscendingHeaderStyle" />
                    <SortedDescendingCellStyle CssClass="gvSortedDescendingCellStyle" />
                    <SortedDescendingHeaderStyle CssClass="gvSortedDescendingHeaderStyle" />
                </asp:GridView>
                <asp:EntityDataSource ID="edsView" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                    DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="EVENTO"
                    EntityTypeFilter="EVENTO" OnQueryCreated="edsView_QueryCreated" 
                    OrderBy="it.DATA_INICIO DESC" Select="" />
            </asp:Panel>
            <uc1:ucShowException ID="ucShowException1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
