﻿<%@ Page Title=":: MasterPainel - HTM Gestão e Tecnologia ::" Language="C#" MasterPageFile="~/Admin/Admin.Master"
    AutoEventWireup="true" CodeBehind="LinkView.aspx.cs" Inherits="HTMSiteAdmin.Web.Admin.LinkView" %>

<%@ Register Src="UserControls/ucShowException.ascx" TagName="ucShowException" TagPrefix="uc1" %>
<%@ Register Src="UserControls/Bars/ucButtons.ascx" TagName="ucButtons" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="upView" runat="server">
        <ContentTemplate>
            <h1>
                ..:: Links
            </h1>
            <asp:Panel ID="pnlView" runat="server">
                <fieldset class="Fieldset">
                    <legend>:: Dados Básicos</legend>
                    <div class="Fields">
                        <p>
                            <asp:Label ID="lblNome" runat="server" Text="Descrição:" AssociatedControlID="txtDescricao"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtDescricao" MaxLength="100" Width="300px" CssClass="Field" />
                            <span class="Validators">*</span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDescricao"
                                CssClass="Validators" Display="Dynamic" ErrorMessage="Campo Requerido" ValidationGroup="vgView"></asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <asp:Label ID="Label3" runat="server" Text="Descrição (Inglês):" AssociatedControlID="txtDescricao"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtDescricaoEn" MaxLength="100" Width="300px" CssClass="Field" />
                        </p>
                        <p> 
                            <asp:Label ID="Label7" runat="server" Text="Caminho:" AssociatedControlID="txtCaminho"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtCaminho" MaxLength="200" Width="300px" CssClass="Field" />
                            <span class="Validators">*</span>
                            <asp:RequiredFieldValidator ID="rfvCaminho" runat="server" ControlToValidate="txtCaminho"
                                CssClass="Validators" Display="Dynamic" ErrorMessage="Campo Requerido" ValidationGroup="vgView"></asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <asp:Label ID="Label8" runat="server" Text="Caminho (Inglês):" AssociatedControlID="txtCaminho_en"
                                CssClass="Label" />
                            <asp:TextBox runat="server" ID="txtCaminho_en" MaxLength="200" Width="300px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="Label9" runat="server" Text="Tipo:" AssociatedControlID="ddlId_link_tipo"
                                CssClass="Label" />
                            <asp:DropDownList ID="ddlId_link_tipo" runat="server" CssClass="Field" DataSourceID="edsLinkTipo"
                                DataTextField="NOME" DataValueField="ID_LINK_TIPO" OnDataBound="ddlId_link_tipo_DataBound" />
                            <span class="Validators">*</span>
                            <asp:CompareValidator ID="cvId_link_tipo" ErrorMessage="Campo Requerido" ControlToValidate="ddlId_link_tipo"
                                runat="server" CssClass="Validators" Display="Dynamic" Operator="GreaterThan"
                                Type="Integer" ValidationGroup="vgView" ValueToCompare="0" />
                            <asp:EntityDataSource ID="edsLinkTipo" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                                DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="LINK_TIPO"
                                EntityTypeFilter="LINK_TIPO" Select="it.[ID_LINK_TIPO], it.[NOME]">
                            </asp:EntityDataSource>
                        </p>
                        <p>
                            <asp:Label ID="Label5" runat="server" Text="Categoria:" AssociatedControlID="ddlId_link_categoria"
                                CssClass="Label" />
                            <asp:DropDownList ID="ddlId_link_categoria" runat="server" CssClass="Field" DataSourceID="edsLinkCategoria"
                                DataTextField="DESCRICAO" DataValueField="ID_LINK_CATEGORIA" OnDataBound="ddlId_link_categoria_DataBound" />
                            <span class="Validators">*</span>
                            <asp:CompareValidator ID="cvId_link_categoria" ErrorMessage="Campo Requerido" ControlToValidate="ddlId_link_categoria"
                                runat="server" CssClass="Validators" Display="Dynamic" Operator="GreaterThan"
                                Type="Integer" ValidationGroup="vgView" ValueToCompare="0" />
                            <asp:EntityDataSource ID="edsLinkCategoria" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                                DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="LINK_CATEGORIA"
                                EntityTypeFilter="LINK_CATEGORIA" Select="it.[ID_LINK_CATEGORIA], it.[DESCRICAO]">
                            </asp:EntityDataSource>
                        </p>
                        <p>
                            <asp:Label ID="lblDescricao" runat="server" Text="Detalhes:" AssociatedControlID="txtDescricao_det"
                                CssClass="LabelTop" />
                            <asp:TextBox runat="server" ID="txtDescricao_det" MaxLength="255" Rows="3" TextMode="MultiLine"
                                Width="300px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="Label4" runat="server" Text="Detalhes (Inglês):" AssociatedControlID="txtDescricao_det_en"
                                CssClass="LabelTop" />
                            <asp:TextBox runat="server" ID="txtDescricao_det_en" MaxLength="255" Rows="3" TextMode="MultiLine"
                                Width="300px" CssClass="Field" />
                        </p>
                        <p>
                            <asp:Label ID="Label1" runat="server" Text="Situação:" AssociatedControlID="ddlSituacao"
                                CssClass="Label" />
                            <asp:DropDownList ID="ddlSituacao" runat="server" CssClass="Field">
                                <asp:ListItem Selected="True" Value="1">Ativo</asp:ListItem>
                                <asp:ListItem Value="2">Inativo</asp:ListItem>
                            </asp:DropDownList>
                        </p>
                    </div>
                    <div style="display: block; float: left; width: 100%">
                        <asp:Label ID="Label10" runat="server" Text="Arquivo:" AssociatedControlID="afuArquivoBanner"
                            CssClass="Label" Style="float: left;" />
                        <ajaxToolkit:AsyncFileUpload ID="afuArquivoBanner" runat="server" OnUploadedComplete="afuArquivoBanner_UploadedComplete"
                            Style="display: inline-block; float: left; margin-left: 2px;" />
                    </div>
                    <div style="display: block; float: left; width: 100%">
                        <asp:Label ID="Label11" runat="server" Text="Arquivo (Inglês):" AssociatedControlID="afuArquivoBanner_en"
                            CssClass="Label" Style="float: left;" />
                        <ajaxToolkit:AsyncFileUpload ID="afuArquivoBanner_en" runat="server" Style="display: inline-block;
                            float: left; margin-left: 2px;" OnUploadedComplete="afuArquivoBanner_en_UploadedComplete" />
                    </div>
                </fieldset>
                <asp:Panel ID="pnlArquivoAtual" runat="server" Visible="false">
                    <fieldset class="Fieldset">
                        <legend>..:: Arquivo Atual</legend>
                        <div>
                            <asp:Image ID="imgBanner" runat="server" ImageUrl="~/ShowFile.aspx?action=1" />
                        </div>
                    </fieldset>
                </asp:Panel>
                <asp:Panel ID="pnlArquivoAtual_en" runat="server" Visible="false">
                    <fieldset class="Fieldset">
                        <legend>..:: Arquivo Atual (Inglês)</legend>
                        <div>
                            <asp:Image ID="imgBanner_en" runat="server" ImageUrl="~/ShowFile.aspx?action=1" />
                        </div>
                    </fieldset>
                </asp:Panel>
            </asp:Panel>
            <uc1:ucShowException ID="ucShowException1" runat="server" />
            <uc2:ucButtons ID="ucButtons1" runat="server" />
            <asp:Panel ID="pnlViewCollection" runat="server">
                <div class="Fields">
                    <p>
                        <asp:Label ID="Label6" runat="server" Text="Situação:" AssociatedControlID="ddlFiltroSituacao"
                            CssClass="LabelFilters" />
                        <asp:DropDownList ID="ddlFiltroSituacao" runat="server" CssClass="Field">
                            <asp:ListItem Selected="True" Value="1">Ativo</asp:ListItem>
                            <asp:ListItem Value="2">Inativo</asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    <p>
                            <asp:Label ID="Label13" runat="server" Text="Tipo:" AssociatedControlID="ddlFiltroTipo"
                                CssClass="LabelFilters" />
                            <asp:DropDownList ID="ddlFiltroTipo" runat="server" CssClass="Field" DataSourceID="edsFiltroTipo"
                                DataTextField="NOME" DataValueField="ID_LINK_TIPO" 
                                ondatabound="ddlFiltroTipo_DataBound" />
                            <asp:EntityDataSource ID="edsFiltroTipo" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                                DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="LINK_TIPO"
                                EntityTypeFilter="LINK_TIPO" Select="it.[ID_LINK_TIPO], it.[NOME]">
                            </asp:EntityDataSource>
                        </p>
                    <p>
                        <asp:Label ID="Label12" runat="server" Text="Categoria:" AssociatedControlID="ddlFiltroCategoria"
                            CssClass="LabelFilters" />
                        <asp:DropDownList ID="ddlFiltroCategoria" runat="server" CssClass="Field" DataSourceID="edsFiltroLinkCategoria"
                            DataTextField="DESCRICAO" DataValueField="ID_LINK_CATEGORIA" OnDataBound="ddlFiltroCategoria_DataBound" />
                        <asp:EntityDataSource ID="edsFiltroLinkCategoria" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                            DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="LINK_CATEGORIA"
                            EntityTypeFilter="LINK_CATEGORIA" Select="it.[ID_LINK_CATEGORIA], it.[DESCRICAO]">
                        </asp:EntityDataSource>
                    </p>
                    <p>
                        <asp:Label ID="Label2" runat="server" Text="Descrição:" AssociatedControlID="txtFiltroDescricao"
                            CssClass="LabelFilters" />
                        <asp:TextBox runat="server" ID="txtFiltroDescricao" MaxLength="255" Width="250px"
                            CssClass="Field" />
                        <asp:ImageButton ID="btnFiltrar" runat="server" ImageUrl="~/Admin/Media/Images/Bars/Buttons/botao_pesquisar_1.png"
                            OnClick="btnFiltrar_Click" onmouseover="javascript:ChangeButtonsImage('pesquisar', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('pesquisar', 'out', this);" Style="margin-left: 50px;" />
                    </p>
                </div>
                <asp:GridView runat="server" ID="gvView" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    DataKeyNames="ID_LINK" DataSourceID="edsView" Width="100%" CssClass="GridView"
                    OnRowCommand="gvView_RowCommand" CellPadding="4" GridLines="None" OnRowDataBound="gvView_RowDataBound"
                    OnSelectedIndexChanged="gvView_SelectedIndexChanged">
                    <AlternatingRowStyle CssClass="gvAlternatingRowStyle" />
                    <Columns>
                        <asp:BoundField DataField="ID_LINK" HeaderText="Código" ReadOnly="True" SortExpression="ID_LINK"
                            ItemStyle-CssClass="gvCenter" />
                        <asp:BoundField DataField="DESCRICAO" HeaderText="Descrição" SortExpression="DESCRICAO"
                            ItemStyle-CssClass="gvCenter" />
                        <asp:TemplateField HeaderText="Situação" SortExpression="ID_LINK_SITUACAO">
                            <ItemTemplate>
                                <asp:Literal ID="ltrSituacao" runat="server" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCenter" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEditar" runat="server" CausesValidation="False" CommandName="Select"
                                    ImageUrl="~/Admin/Media/Images/GridViews/edit.png" Text="Select" ToolTip="Exibir/Editar" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnExcluir" runat="server" CausesValidation="False" CommandName="Excluir"
                                    OnClientClick="javascript:return confirm('Deseja excluir este registro?');" ImageUrl="~/Admin/Media/Images/GridViews/delete.png"
                                    CommandArgument='<%# Bind("ID_LINK") %>' ToolTip="Excluir" />
                            </ItemTemplate>
                            <ItemStyle CssClass="gvCommandFieldItem" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle CssClass="EditRowStyle" />
                    <HeaderStyle CssClass="gvHeaderStyle" />
                    <PagerSettings Position="TopAndBottom" />
                    <PagerStyle CssClass="gvPagerStyle" />
                    <PagerTemplate>
                        <asp:ImageButton ID="btnGvFirst" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/first.png"
                            CommandName="FirstPage" ToolTip="Primeira Página" />
                        <asp:ImageButton ID="btnGvBack" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/back.png"
                            CommandName="BackPage" ToolTip="Página Anterior" />
                        <asp:TextBox ID="txtGvPage" runat="server" Text="<%# gvView.PageIndex + 1 %>" MaxLength="4"
                            Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:Label ID="ltrPager" Text="de" runat="server" CssClass="gvPagerLiteral" EnableTheming="false" />
                        <asp:TextBox ID="txtGvPages" runat="server" Text="<%# gvView.PageCount %>" Enabled="false"
                            ReadOnly="true" Width="30px" CssClass="gvPagerTextBox Field" />
                        <asp:ImageButton ID="btnGvGo" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/botao_ir_1.png"
                            CommandName="GoToPage" ToolTip="Ir para a página" onmouseover="javascript:ChangeButtonsImage('gotopage', 'over', this);"
                            onmouseout="javascript:ChangeButtonsImage('gotopage', 'out', this);" />
                        <asp:ImageButton ID="btnGvNext" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/next.png"
                            CommandName="NextPage" ToolTip="Próxima Página" />
                        <asp:ImageButton ID="btnGvLast" runat="server" ImageUrl="~/Admin/Media/Images/GridViews/last.png"
                            CommandName="LastPage" ToolTip="Última Página" />
                    </PagerTemplate>
                    <RowStyle CssClass="gvRowStyle" />
                    <SelectedRowStyle CssClass="gvSelectedRowStyle" />
                    <SortedAscendingCellStyle CssClass="gvSortedAscendingCellStyle" />
                    <SortedAscendingHeaderStyle CssClass="gvSortedAscendingHeaderStyle" />
                    <SortedDescendingCellStyle CssClass="gvSortedDescendingCellStyle" />
                    <SortedDescendingHeaderStyle CssClass="gvSortedDescendingHeaderStyle" />
                </asp:GridView>
                <asp:EntityDataSource ID="edsView" runat="server" ConnectionString="name=HTMSiteAdminEntities"
                    DefaultContainerName="HTMSiteAdminEntities" EnableFlattening="False" EntitySetName="LINK"
                    EntityTypeFilter="LINK" OnQueryCreated="edsView_QueryCreated" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
