﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="FairTradeIbd.aspx.cs" Inherits="HTMSiteAdmin.Web.en.FairTradeIbd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Socio-Environmental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px;
                    margin-right: 6px; margin-left: 6px;" />
                IBD Fair Trade</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoEcoSocialIBD.png" alt="" style="float: right;
                margin-left: 15px; margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                About the Seal</h2>
            All EcoSocial Fair is the Fairtrade Program (Fair Trade) IBD applies only to products and processes certified as organic.
            <br />
            <br />
            The EcoSocial Certification All Fair applies to businesses, properties and producer groups that are aimed to stimulate an internal process of human, social and environmental development fostered by commercial relations based on the principles of Fair Trade. From an initial diagnosis that characterizes the environmental reality of the organization, a Steering Committee, composed of the stakeholders in the enterprise, identifies and prioritizes the key environmental and social demands that interfere negatively in this reality and from then on, Action Plans aimed at continuous improvement in the social and environmental aspects (of Living and Working Conditions, Environmental Conservation and Recovery) are outlined.<br />
            <br />
            The certification projects are audited based on the following criteria:<br />
            - Critical: related to some international conventions and agreements;<br />
            - Minimum: related to the country's legislation;<br />
            - Progress: related to the promotion of local development<br />
            All In addition to those already provided for in national laws.<br />

            <br />            
            Being Fair Trade certified means fostering local social and environmental development and fair trade in practice.
            <br />
            <br />
           
            <p>Please also click below to download the most recent brochre.</p>
            <a style="display: block" href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=fdb57265-a876-4c9c-bbf2-d4a2eca6a2be" target="_blank" class="itemNormas">Brochure - IBD Fair Trade Cooperation</a>
            <br /><br />

            <h2 class="tituloCertificaoInterno">Step by Step</h2>
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=b87883d3-dcec-4eba-8ed9-9b54f7903d9e"> - Step by Step to certification (English)</a>
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=b3922e67-079a-4368-8b0e-f8ed317ebf5f"> - Step by Step to certification (Portuguese)</a><br />
            <br /><br />

            <h2 id="criterios" class="tituloCertificaoInterno">Criteria</h2>
            <a href="EcoSocialIBD_criterios.aspx#01" class="itemNormas">Critical</a>
            <a href="EcoSocialIBD_criterios.aspx#02" class="itemNormas">Environmental</a>
            <a href="EcoSocialIBD_criterios.aspx#03" class="itemNormas">Social</a>
            <a href="EcoSocialIBD_criterios.aspx#04" class="itemNormas">Economic Development</a>
            <br />
            <br />

            

            <h2 class="tituloCertificaoInterno">
                Additional Information</h2>
            <p>
                The Fair Trade standard has as its theoretical basis the OIT Conventions, diverse
                international protocols such as Agenda 21, the Global Pact Program and Millennium
                Goals, as well as standards such as SA 8000, ISO 14.000 and BS 8800. IBD is certified
                under international rules for standardization within ISO 65, guaranteeing quality
                throughout the entire certification process.
                <br />
                <br />
                Given this, the IBD Fair Trade certificate guarantees that the certified operation
                is engaged in processes aimed at sustainable development, attending to the demands
                of attentive and aware consumers, which are more and more numerous throughout the
                world today. It was through the Fair Trade program that IBD ended its exclusive work
                in Brazil, and began certifying in nine countries worldwide as to date.
                <br />
                <br />
                <a href="DiretrizesLegislacao.aspx" class="itemNormas">Standards and Legislation</a>
                <!-- 
                <span class="itemNormas">IBD EcoSocial Fair Trade Ecosocial Standards</span> <span class="itemNormas">
                    IBD Fair Trade Ecosocial Presentation (2008)</span> <span class="itemNormas">IBD Fair Trade Ecosocial Handbook</span> -->
                <br />
            </p>
            <br />
            <h2 class="tituloCertificaoInterno">
                A few clients</h2>
        
                       <!-- AgroPalma -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=586" class="imagem">
                    <img src="media/img/logo_agropalma.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=586" class="titulo">AgroPalma</a>
            </div>

            <!-- ASR Group - Tate and Lyle Sugars ltd.-->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=602" class="imagem">
                    <img src="media/img/logo_asr.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=602" class="titulo">ASR Group - Tate and Lyle Sugars Ltd</a>
            </div>

            <!-- Bees Brothers Ltd-->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=610" class="imagem">
                    <img src="media/img/logo_bees.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=610" class="titulo">Bees Brothers</a>
            </div>

            <!-- Biorgânica-->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=587" class="imagem">
                    <img src="media/img/logo_biorganica.jpg" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=587" class="titulo">Biorgânica</a>
            </div>

            <!-- Ceres -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=588" class="imagem">
                    <img src="media/img/logo_ceres.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=588" class="titulo">Ceres</a>
            </div>

            <!-- Ciranda -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=589" class="imagem">
                    <img src="media/img/logo_ciranda.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=589" class="titulo">Ciranda</a>
            </div>

            <!-- Crofters Food Ltd. -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=591" class="imagem">
                    <img src="media/img/logo_crofters.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=591" class="titulo">Crofter&acute;s</a>
            </div>

            <!-- Cumberland Packaging Corporation -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=609" class="imagem">
                    <img src="media/img/logo_cumberland.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=609" class="titulo">Cumberland Packaging Corporatio</a>
            </div>

            <!-- Dalian Huaen -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=592" class="imagem">
                    <img src="media/img/logo_he.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=592" class="titulo">Dalian Huaen</a>
            </div>

            <!-- Davert GmbH -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=590" class="imagem">
                    <img src="media/img/logo_davert.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=590" class="titulo">Davert GmbH</a>
            </div>

            <!-- Do-It Dutch Organic International Trade -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=611" class="imagem">
                    <img src="media/img/logo_doit.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=611" class="titulo">Do-It Dutch Organic International Trade</a>
            </div>

            <!-- Exportadora Agricola Organica SAC -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=605" class="imagem">
                    <img src="media/img/logo_exportadoraagricolaorganica.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=605" class="titulo">Exportadora Agricola Organica SAC</a>
            </div>

            <!-- GFI Greenfood international BV -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=593" class="imagem">
                    <img src="media/img/logo_greenfoodinternational.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=593" class="titulo">GFI  Greenfoodinternational BV</a>
            </div>

            <!-- Global Organics, Ltd -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=607" class="imagem">
                    <img src="media/img/logo_globalorganic.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=607" class="titulo">Global Organics, Ltd</a>
            </div>

            <!-- Goiasa Goiatuba Álcool Ltda. -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=603" class="imagem">
                    <img src="media/img/logo_goiasa.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=603" class="titulo">Goiasa Goiatuba Álcool Ltda</a>
            </div>

            <!-- Jalles Machado -->
            <div class="itemClientesEcoSocial ">
                <a href="ClientesDetalhes.aspx?id_conteudo=594" class="imagem">
                    <img src="media/img/logo_jallesmachado.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=594" class="titulo">Jalles Machado</a>
            </div>

            <!-- La Grange -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=601" class="imagem">
                    <img src="media/img/logo_LaGrange.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=601" class="titulo">La Grange </a>
            </div>

            <!-- Matrunita - Grupo Maranhão -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=599" class="imagem">
                    <img src="media/img/logo_matrunita.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=599" class="titulo">Matrunita - Grupo Maranhão</a>
            </div>

            <!-- Native -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=595" class="imagem">
                    <img src="media/img/logo_native.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=595" class="titulo">Native Alimentos</a>
            </div>

            <!-- Reudink B.V. -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=604" class="imagem">
                    <img src="media/img/logo_reudink.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=604" class="titulo">Reudink B.V.</a>
            </div>

            <!-- Santa Hevea Comércio de Alimentos Ltda -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=596" class="imagem">
                    <img src="media/img/logo_organicsaudenatural.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=596" class="titulo">Santa Hevea Comércio de Alimentos Ltda</a>
            </div>

            <!-- Senfas -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=600" class="imagem">
                    <img src="media/img/logo_senfas.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=600" class="titulo">Santa Hevea Comércio de Alimentos Ltda</a>
            </div>

            <!-- The Natural Growth Co. Ltd -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=597" class="imagem">
                    <img src="media/img/logo_ngc.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=597" class="titulo">The Natural Growth Co. Ltd.</a>
            </div>

            <!--  Usina Sao Francisco S/A (Native) -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=606" class="imagem">
                    <img src="media/img/logo_usinasaofrancisco.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=606" class="titulo">Usina Sao Francisco S/A (Native)</a>
            </div>

            <!--  Van Gorp Biologische Voeders B.V. -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=598" class="imagem">
                    <img src="media/img/logo_biologische_diervoeders.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=598" class="titulo">Van Gorp Biologische Voeders B.V.</a>
            </div>
    </div>
    <!--quemsomos-->
</asp:Content>
