﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucUltimasNoticias.ascx.cs"
    Inherits="HTMSiteAdmin.Web.en.UserControls.ucUltimasNoticias" %>
<asp:DataList ID="dtUltimasNoticias" runat="server" DataKeyField="ID_CONTEUDO" OnItemDataBound="dtUltimasNoticias_ItemDataBound"
    CellPadding="0" RepeatLayout="Flow" ShowFooter="False" ShowHeader="False">
    <ItemTemplate>
        <div class="itemNews">
            <div class="dataTipo">
                <span>
                    <asp:Literal ID="ltrMesNoticia" runat="server" Text="Janeiro" />
                    |
                    <asp:Literal ID="ltrDiaNoticia" runat="server" Text="1" /></span><h4>
                        <asp:Literal ID="ltrCategoria" runat="server" Text="Nacional" />
                    </h4>
            </div>
            <asp:Literal ID="ltrLinkNoticia" runat="server" Text="<a href='NoticiasDetalhes.aspx?id_conteudo={0}\'>Link</a>" />
        </div>
    </ItemTemplate>
</asp:DataList>