﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="FaleConosco.aspx.cs" Inherits="HTMSiteAdmin.Web.en.FaleConosco" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Contact us</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="painelCliente" class="faleConosco">
        <div>
            <p style="font-size: 15px;">
                Contact IBD Certification using the form below or through the following contacts:</p>
        </div>
        <br />
        <div style="float: left; width: 370px;">
            <ul>
                <li>
                    <asp:Label ID="Label1" Text="subject" runat="server" AssociatedControlID="ddlAssunto" />
                    <asp:DropDownList runat="server" ID="ddlAssunto" Style="float: left;" Width="260px">
                        <asp:ListItem Text="Get Certified with IBD" Selected="True" />
                        <asp:ListItem Text="Questions and Help Desk" />
                        <asp:ListItem Text="Financial Department" />
                    </asp:DropDownList>
                </li>
                <li>
                    <asp:Label ID="Label2" Text="name" runat="server" AssociatedControlID="txtNome" />
                    <asp:TextBox runat="server" ID="txtNome" Style="float: left;" Width="250px" />
                    <asp:RequiredFieldValidator ID="rfvAssunto" ControlToValidate="txtNome" runat="server"
                        ErrorMessage="- Name" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label3" Text="e-mail" runat="server" AssociatedControlID="txtEmail" />
                    <asp:TextBox runat="server" ID="txtEmail" Style="float: left;" Width="250px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtEmail"
                        runat="server" ErrorMessage="- E-mail" Display="None" SetFocusOnError="True"
                        ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="- Invalid E-mail" Display="None" SetFocusOnError="true" ValidationGroup="vgForm"
                        ValidationExpression="\S+@\S+.\S{2,3}"></asp:RegularExpressionValidator>
                </li>
                <li>
                    <asp:Label ID="Label4" Text="telephone" runat="server" AssociatedControlID="txtTelefone" />
                    <asp:TextBox runat="server" ID="txtTelefone" Style="float: left;" Width="250px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtTelefone"
                        runat="server" ErrorMessage="- Telephone" Display="None" SetFocusOnError="True"
                        ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label5" Text="message" runat="server" AssociatedControlID="txtMensagem" />
                    <asp:TextBox runat="server" ID="txtMensagem" Style="float: left;" Rows="10" TextMode="MultiLine"
                        Width="250px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtMensagem"
                        runat="server" ErrorMessage="- Message" Display="None" SetFocusOnError="True"
                        ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:LinkButton ID="btnEnviar" Text="enviar" runat="server" CssClass="cadastrar"
                        Style="float: right;" OnClick="btnEnviar_Click" ValidationGroup="vgForm" /></li>
            </ul>
            <asp:ValidationSummary ID="vsForm" runat="server" ValidationGroup="vgForm" CssClass="erroLogin"
                HeaderText="Os seguintes campos contêm erros ou são obrigatórios:" />
        </div>
        <div class="dados">
            <img src="media/img/fotoFaleconosco.jpg" alt="" width="266" height="166" /><br />
            <br />
            <span>Telephone</span>
            <div class="num">
                +55 (14) 3811 9800</div>
            <br />
            <span>Fax</span>
            <div class="num">
                +55 (14) 3811 9801</div>
            <br />
            <span>E-mail</span><br />
            ibd@ibd.com.br<br />
            <br />
            <span>Address</span>
            <address>
                Rua Amando de Barros, 2275 - Centro
                <br />
                CEP: 18.602.150 – Botucatu - SP - Brazil
            </address>
            <br />
        </div>

        <div style="clear:both">
            <div class="quemSomos">
                <h2 class="tituloCertificaoInterno"> Complaint </h2>
                <ol class="list-number">
                <li>
                    If you wish to report a complaint and/or investigations, please use the link: <br />
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLScWixdWY6YcZhDY31nUnWE1qBwa2pC9MIeOPOgsFEN28i5Umg/viewform">docs.google.com/forms/d/e/1FAIpQLScWixdWY6YcZhDY31nUnWE1qBwa2pC9MIeOPOgsFEN28i5Umg/viewform</a>
                </li>
                <li>
                    The protocol number will be informed to the contact indicated.
                </li>
                <li>
                    All claims or complaints will be processed within 60 days. Due to accreditation processes and actions needed to clarify the process can stretch. The appellant / complainant will be notified of the status of your complaint
                </li>
            </ol>
            </div>
        </div>

    </div>
    <!--painelCliente-->
</asp:Content>
