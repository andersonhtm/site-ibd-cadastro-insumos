﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="ServicosCursosTreinamentos.aspx.cs" Inherits="HTMSiteAdmin.Web.en.ServicosCursosTreinamentos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>Services
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px; margin-left: 6px;" />
                Training</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }

                .itemNormas {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }

                .limpaFloat {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/imagem_interna_04.jpg" alt="" style="float: right; margin-left: 15px; margin-bottom: 10px;" />
            <p>
                To enable participants to understand the concepts and requirements of organic production standards for the Brazilian, European and American markets and the essential operational controls to be followed by establishments and services involved or to start these forms of production, with special attention to region. The producer group organization and operation of the internal control system may also be part of the program.
                <br />
                <br />
                This course has been part of the existing training system at IBD for years and is done in several parts of the country, according to demand.
            </p>
            <br />
            <br />
            <br />
            <br />
            <br />
            <p>
                <h2 class="tituloCertificaoInterno">Training Course and Interpretation of Organic Norms</h2>
                <br />
                <p style="color: #4f6f0a">- Target Audience</p>
                <p>Companies, consultants and rural producers, associations and cooperatives that seek to obtain understanding and safety in food production and better management control of agricultural and professional production that aim at the achievement and maintenance of organic certification or technical and services related to this form of production</p>

                <br />
                <%--<p style="color: #4f6f0a">- Goal</p>
                <p>To qualify the professional in the interpretation of organic norms, to know the procedures of certification, procedures of audit and the basic documentation of the certification.</p>

                <br />--%>
                Contact us for more information or access the course schedule. <a href="EventosFeiras.aspx?id_categoria=7">[click here] </a>
                <br />
            </p>
        </p>
    </div>
    <!--quemsomos-->
</asp:Content>
