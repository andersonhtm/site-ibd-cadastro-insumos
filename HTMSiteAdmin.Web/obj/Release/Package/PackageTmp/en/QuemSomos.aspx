﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="QuemSomos.aspx.cs" Inherits="HTMSiteAdmin.Web.en.QuemSomos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <link rel="stylesheet" type="text/css" href="media/css/adGallery.css" />
    <script type="text/javascript" src="media/js/adGallery.js"></script>
    <script type="text/javascript">        $(function () {
            $('.ad-gallery').adGallery();
            $('.toggle').hide();
            $(".box2 a").click(function () { $(this).toggleClass("btativo").next().slideToggle("slow"); return false })
        });</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                About Us</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <div class="box1" style="margin-bottom: 15px;">
            <div class="galeria">
                <div class="ad-gallery">
                    <div class="ad-image-wrapper">
                    </div>
                    <div class="ad-controls">
                    </div>
                    <div class="ad-nav">
                        <div class="ad-thumbs">
                            <ul class="ad-thumb-list">
                                <asp:Repeater ID="rptImages" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <a href='../ShowFile.aspx?action=1&fileid=<%# (Eval("ARQUIVO_DIGITAL") as HTMSiteAdmin.Data.ARQUIVO_DIGITAL).ID_ARQUIVO.ToString() %>'>
                                                <img src='../ShowFile.aspx?action=1&fileid=<%# (Eval("ARQUIVO_DIGITAL") as HTMSiteAdmin.Data.ARQUIVO_DIGITAL).ID_ARQUIVO.ToString() %>&width=67' width="67" alt='' />
                                            </a>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                        <!--ad-thumbs-->
                    </div>
                    <!--ad-nav-->
                </div>
                <!--ad-gallery-->
            </div>
            <!--galeria-->
            <p>
                IBD prides itself in being the largest certifier in Latin America and the only Brazilian
                certifier of organic products that is accredited under IFOAM (international market),
                ISO/IEC 17065 (European market, rule CE 834/2007), Demeter (international market),
                USDA/NOP (North American market) and approved for use of SISORG seal (Brazilian
                market), making it’s certificate accepted globally.
                <br />
                <br />
                In addition to organic certification protocols, IBD offers socioenvironmental certifications:
                RSPO (Roundtable on Sustainable Palm Oil), UEBT (Union for Ethical BioTrade), Fair Trade IBD.
            </p>
        </div>
        <!--box1-->
        <p>
            <br />
            Located in Botucatu/SP (Brazil) and founded in 1982, IBD operates throughout all
            the Brazilian states, as well as in diverse countries such as Argentina, Bolivia,
            Columbia, Ecuador, Mexico, Paraguay, Uruguay, the United States, Canada , Belgium,
            Holland, New Zealand, China, India, and Thailand, working towards sustainable production
            standards and stimulating fair trade.<br />
            <br />
            Involved in this work are more than 5,000 producers and companies, which manage
            a total of 520,000 hectares of cultivated land and 3,000,000 hectares of wild harvest
            areas. At the core of IBD’s philosophy is a commitment to the land and to human
            beings, guaranteeing respect for the environment, good working conditions, and highly
            reliable products.<br />
            <br />
        </p>
    </div>
    <!--quemsomos-->
</asp:Content>
