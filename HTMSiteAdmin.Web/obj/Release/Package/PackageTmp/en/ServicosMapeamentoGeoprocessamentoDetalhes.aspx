﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="ServicosMapeamentoGeoprocessamentoDetalhes.aspx.cs" Inherits="HTMSiteAdmin.Web.en.ServicosMapeamentoGeoprocessamentoDetalhes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Servi&ccedil;os
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Mapping and Geoprocessing</h2>
            <a href="/">return to home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos" class="texto-justificado">
        <br />        
        <style>
            .tituloCertificaoInterno
            {
                color: #2D312E;
                font-size: 20px;
                margin: 15px 0 10px;
                width: 100%;
            }
            .itemNormas
            {
                display: block;
                background: #fffcec;
                padding: 6px 10px 6px 10px;
                margin: 0px 8px 10px 0px;
                float: left;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
            }
            .limpaFloat
            {
                clear: both;
                height: 10px;
            }
        </style>

            <h2 class="tituloCertificaoInterno">Orbital Satellite Imagery</h2>
            
            <p>It is a product generated from a satellite image in which information can be extracted from a specific area. With orbital satellites it is possible to obtain images of large extensions of the terrestrial surface in a repetitive way and at a relatively low cost. The images are intended to represent the physical or artificial physical aspects of a region which allows for various evaluations with precision and in detail. The better the image resolution used (high-precision satellites) the better the detail of the information that can be generated for the property. Orbital images also have a historical collection, which makes it possible to search for an image of a specific previous date.</p>
                
            <div style="display: flex; flex-direction: column; align-items: center">
                <img src="../Media/img/mapa-geo-detalhes-1.png" alt="" style="margin: 15px auto;  text-align: center" />
                <img src="../Media/img/seta.png" alt="" style="transform: rotate(90deg); max-height: 28px" />
                <img src="../Media/img/mapa-geo-detalhes-2.png" alt="" style="margin: 15px auto;  text-align: center" />
            </div>    

        <p>
            <b>Product:</b> Satellite image of the property under study and the environment, georeferenced and in composition (RGB) of true color and / or false-color, depending on the need. The images are base of the mapping, from which other products can be generated.</p>
        <br />

        <b>Image options and resolution detailing:</b>
        <br />
              •	Sentinel 2 - resolution of 10m or 20m;
        <br />•	Landsat 8 - resolution of 15m or 30m;
        <br />•	Cbers 4 - resolution 10m or 20m;
        <br />•	Images from the collection (from the date of launch of the satellite): LandSat 1 to 7 - Resolution of 30m. Cbers 2 / 2b - resolution 20m and Aster or Srtm - resolution from 15 to 90m;
        <br />•	High resolution satellites: QuickBird, Geoeye, Spot 6/7, among others - by purchasing a specialized supplier.


        <h2 class="tituloCertificaoInterno">Landscape features and land use maps</h2>

        <img src="../Media/img/mapa-geo-detalhes-3.png" alt="" style="margin: 15px 15px 15px 0;  text-align: center; float: left" />

        <p>From a survey of topographic, whose purpose is to spatially describe a landscape, with its areas, dimensions and formats. At the time these data are spatialized it is possible to generate a variety of important information for the diagnosis and planning of a property and at that time IBD mapping services are able to transform a topographic survey into several maps that bring a variety of important information.
If the property does not have an accurate and detailed topographical survey there are some orbital satellite images that can provide topographic and hydrographic profiles, meeting required parameters:</p>
        <br />
        <p>1 - <u>SRTM</u>: with 90m spatial resolution at a 1:100.000 mapping scale, it can generate contours with 50 meters equidistance;</p>
        <p>2 - <u>TopoData</u>: with spatial resolution of 30m, at a mapping scale of 1:50.000, can generate contours with equidistance of 20 meters;</p>
        <p>3 - <u>Aster Gdem</u>: with spatial resolution of 30m, at a mapping scale of 1:25.000, can generate contours with equidistance of 10m.</p>
        <br />

        <p>From the contours it is possible to generate maps of altimetry and hypsometry, digital elevation models, slope orientation map, declivity map, drainage, surface runoff and flow direction, among others.</p>

        <br />
        <b>Products:</b><br />

              •	Digitalization of the topographic survey of the property (if available);
        <br />•	Creation of land use with boundary and calculated areas;
        <br />•	Creation of contour lines from orbital image;
        <br />•	Creation of the Digital Elevation Model (TIN model) from the contour lines;
        <br />•	Creation of altimetry quotas;
        <br />•	Creation of hypsometry classes;
        <br />•	Creation of slope classes;
        <br />•	Creation of strand orientations;
        <br />•	Delimitation of basin and micro basins;
        <br />•	Creation of drainage lines;
        <br />•	Creation of the surface runoff and flow direction;


        <div style="display: flex; justify-content: center; align-items: center">
            <img src="../Media/img/mapa-geo-detalhes-4.png" alt="" style="margin: 20px 0 20px 0; max-width: 450px" />            
            <img src="../Media/img/mapa-geo-detalhes-5.png" alt="" style="margin:  20px 0 20px 20px; max-width: 320px;" />
        </div>

        <h2 class="tituloCertificaoInterno">Maps of Environmental Diagnosis and Environmental Legislation</h2>
            
        <p>Starting from the surveys and maps previously described, such as altimetry, slope, orientation of slopes, surface runoff, among others; as well as with the identification of land use, it is possible to cross-reference these information and generate environmental diagnosis of the property under study. </p>
        
        <img src="../Media/img/mapa-geo-detalhes-6.jpg" alt="" style="margin: 15px 0 15px 15px;  text-align: center; float: right" />

        <p>With the rules of the New Brazilian Forest Code of 2012, it is necessary to calculate and sample the Permanent Preservation Areas and Legal Reserve for the declaration within the Rural Environmental Registry (CAR) system. With the GIS technologies and remote sensing it is possible to generate some cartographic products for this purpose. </p>
        <br />

        <b>Products:</b>
        <br />•	Creation of the property boundary (according to descriptive memorandum, sketch, owner / responsible orientation or field survey);
        <br />•	Digitalization of water resources from the image or other reference;
        <br />•	Scanning of Aster image level curves (30m);
        <br />•	Creation of land use with boundary and calculated areas;
        <br />•	Creation of the Permanent Areas of Preservation duly delimited and calculated;
        <br />•	Delimitation of the Legal Reserve (recommendation of allocation).



        <h2 class="tituloCertificaoInterno">Spatial Distribution of Soil Attributes Maps</h2>

        <img src="../Media/img/mapa-geo-detalhes-7.png" alt="" style="margin: 0 15px 15px 0;  text-align: center; float: left" />        
        <img src="../Media/img/mapa-geo-detalhes-8.png" alt="" style="margin: 15px 0 15px 15px;  text-align: center; float: right" />
        <p> From the previously collected data and the laboratory analytical results of these data of the sampled area can be generated maps of the components and attributes of the soil. Clay, fine sand, coarse sand, total sand, organic matter, cation exchange capacity (CTC), calcium, magnesium, base saturation, phosphorus, potassium or any other attribute or characteristic that has been previously collected can be accurately spatialized . The maps are generated using geostatistical methods such as kriging, inverse distance, co-kriging, depending on the attribute investigated and the customer's need.</p>
        <br />

        <b>Products:</b><br />
        • Creation of the grid and sampling points for data collection in the field;
        <br />•	Creation of the spatial distribution areas of each of the soil attributes collected and analyzed in the laboratory;



        <h2 class="tituloCertificaoInterno">Density and Quality Vegetation Index Maps</h2>
        
        <p>An analysis of vegetation density and quality that seeks to discriminate the differences in vegetation installed in a given area. It is based on images obtained by remote sensors and is presented in a visual and numerical way, indicating the variations between the lowest and the highest value of vegetative content. EVI, SAVI, NDVI, SR are examples of vegetation indexes that may indicate nutrient deficiency, pests, planting quality, areas under natural regeneration, vegetation density and vigor. </p>
        
        <div style="display: flex; flex-direction: column; align-items: center">
            <img src="../Media/img/mapa-geo-detalhes-9.png" alt="" style="margin: 15px auto;  text-align: center" />
            <img src="../Media/img/seta.png" alt="" style="transform: rotate(90deg); max-height: 28px" />
            <img src="../Media/img/mapa-geo-detalhes-10.png" alt="" style="margin: 15px auto;  text-align: center" />
        </div>    

        <b>Products:</b><br />
              • Creation of a vegetation index map;
        <br />• Identification, delimitation and calculation of areas of different plant density and vegetative vigor;



        <h2 class="tituloCertificaoInterno">Online Publishing of GIS Data (Webmapping)</h2>
        
        <p>Online publication of gis data and maps of interest that can be accessed remotely. Customers can view information on computers, phones, tablets, on any platform. You can link this information on your own website, you can send it by email, or limit access to it. There is also the possibility to edit the hosted data, update information or insert new information into the geographic database depending on the customer's needs.</p>

        <div style="display: flex; justify-content: center; align-items: center">
            <div style="text-align: center">
                <b>WEB</b>
                <img src="../Media/img/mapa-geo-detalhes-11.png" alt="" style="margin: 20px 0 20px 0; max-width: 320px" />
            </div>
            <div style="text-align: center">
                <b>MOBILE</b>
                <img src="../Media/img/mapa-geo-detalhes-12.png" alt="" style="margin:  20px 0 20px 0; max-width: 320px;" />
            </div>
        </div>

        <b>Products:</b><br />
        •	Hosting of gis data in server that allows visualization and edition in internet browsers (computer, cellular and tablet) by determined time.

        <br /><br />
        <b>Acquisition of products and services</b><br />
        <p>Contact IBD's commercial sector, which will send you a proposal request form. The form should contain some basic information about the property and the needs and products of interest. Thus, the IBD will return with a proposal of services / products and with a contract linked to the acceptance of the proposal. Costs and delivery times may vary depending on the size of the property and the quantity of products / services requested.</p>

        <br />
       <b>Comments:</b><br />
    <p>All files will be delivered in shapefiles, raster or other format, depending on the type of gis data. Also, a map with scale, legend, coordinate system, in PDF format, of each product requested, will be delivered.</p>
<p>In some situations it will be necessary to move some IBD staff to collect data on the property. The costs of transportation and lodging are the responsibility of the contractor and will be agreed in advance.</p>
<p>High resolution images require the purchase of third parties. If it is in the client's interest, the IBD can guide or intermediate this purchase and the amount will be indicated in the proposal.</p>
<p>At present, the IBD does not perform services of topography, georeferencing of rural property, collection and classification of soils and their attributes. However, some products require such analyzes. IBD is empowered to guide and suggest methodologies for such surveys.</p>
    </div>
</asp:Content>
