﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="Utz.aspx.cs" Inherits="HTMSiteAdmin.Web.en.Utz" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Socio-Environmental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px;
                    margin-right: 6px; margin-left: 6px;" />
                UTZ</h2>
            <a href="/">return to home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
       
            <img src="media/img/logoCertificadoUtz.png" alt="Utz - Better farming, better future" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                About the Seal</h2>
            <p>
            When you buy UTZ coffee, cocoa or tea you are helping build a better future. 
UTZ stands for sustainable farming and better opportunities for farmers, their families and our planet. The UTZ program enables farmers to learn better farming methods, improve working conditions and take better care of their children and the environment. 
            </p><br />
            <p>
                Through the UTZ program farmers grow better crops, generate more income and create better opportunities while safeguarding the environment and securing the earth’s natural resources. Now and in the future. And that tastes a lot better. 
            </p><br />
            <h3 style="font-weight: bold; margin-bottom: 7px">Making a difference around the world </h3>
            <p>UTZ wants sustainable farming to become the most natural thing in the world. And we’re on the right track because an increasing share of the world’s coffee, cocoa and tea is grown responsibly. In fact, almost 50% of all certified sustainable coffee is now UTZ. 
We work with companies like Mars, Ahold, IKEA, D.E Master Blenders 1753, Migros, Tchibo and Nestlé. So when you buy these and other UTZ products, we are able to support more and more farmers, their workers and their families achieve their ambitions. 
</p>
            <br />
            <h3 style="font-weight: bold; margin-bottom: 7px">Strict requirements, close monitoring </h3>
            <p>Coffee, cocoa and tea products do not get the UTZ label easily. The compliance with our strict requirements by UTZ farms and businesses, is closely monitored by independent third parties. These requirements include Good Agricultural Practices and farming management, safe and healthy working conditions, abolition of child labor and protection of the environment. UTZ offers brands the possibility to track and trace the coffee, cocoa and tea, from the shelf in the store to the farmer. The UTZ logo on your product ensures that your favorite brand supports sustainable farming.
            </p>
            <br />
            <br />

            <h2 class="tituloCertificaoInterno">Step by Step</h2>
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=22e33984-3533-47bb-b347-f32e0c61b8f5"> - Step by Step to certification (Portuguese)</a>
            <br /><br />
                   
            
            <h2 class="tituloCertificaoInterno">
                Standards Served</h2>            
               - UTZ (Code of Conduct and Chain of Custody)
            <br />
            <br />
           
            <h2 class="tituloCertificaoInterno">
                Segments Served</h2>            
                - Agriculture <br />
                - Chain of custody 


          
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Certified Clients
            </h2>

            <asp:Repeater ID="rpt" runat="server">
                <HeaderTemplate>
                    <ul class="list">
                </HeaderTemplate>
                <ItemTemplate>                    
                    <li><a href="<%# Eval("Caminho") %>" class="imagem"><%# Eval("Descricao")%></a></li>                    
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>

    </div>
    <!--quemsomos-->
</asp:Content>
