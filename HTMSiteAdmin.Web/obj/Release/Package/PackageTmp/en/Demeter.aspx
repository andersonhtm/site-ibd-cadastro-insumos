﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="Demeter.aspx.cs" Inherits="HTMSiteAdmin.Web.en.Demeter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Biodynamic
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Demeter</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoDemeter.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                About the Seal</h2>
           Demeter is a global brand that identifies biodynamic farming products. Demeter products are part of an international ecological network connected to the Demeter International, based in Germany.<br /><br />

            Demeter products certified in the country by audit certification system, are certified by Demeter IBD exclusively.<br /><br />
            Demeter products to be certified in the participatory system, the Biodynamic Association will make the certification.<br /><br />
            For all Demeter imported products with Demeter certification in the country of origin, there will be no need to re-certification in Brazil, but the importer will be required to keep a contract and license sales  with IBD Certifications.<br /><br />
            For producers and exporters to Brazil it will not be necessary a contract with IBD Certifications specific to Demeter.<br /><br />
            For the Brazilian organic product certification: all Demeter certified products abroad are usually also certified as organic. Therefore, these products must go through a certification for organic according to Brazilian law. IBD can perform this service or guide you to all information about this process.<br /><br />
            Please contact <a href="mailto:ibd@ibd.com.br">ibd@ibd.com.br</a> or visit <a href="http://ibd.com.br/en/FaleConosco.aspx">http://ibd.com.br/en/FaleConosco.aspx</a> and send us your demand for information.<br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Standards Served</h2>
                <!-- <span class="itemNormas">Demeter</span> -->
                - Demeter
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segments Served</h2>
            - Agriculture<br />
            - Livestock<br />
            - Processing<br />
            - Cosmetics<br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Biodynamic wines</h2>
            <p>
                What are biodynamic wines?<br />
                <br />
                Biodynamic farming began in 1924 with lectures given by Rudolf Steiner in Europe and today corresponds to a movement in more than 40 countries and involves more than 4,900 producers. Biodynamic farming was a pioneer for quality seal products. Demeter seal was launched already in the 50s of the last century.<br /><br />
                There is an international association and all Demeter International data are available on the website: www.demeter.net where IBD (in Brazil) is a part. The standards for biodynamic wine are followed for the production of certified biodynamic wines.<br /><br />
                Biodynamic wines are produced within a technical and comprehensive approach that encompasses even the stellar rhythms but this practice is not mandatory and is not the only and main point that differentiates wines and that are required for Demeter certification. The following criteria are very important to obtain Demeter certification:<br /><br />
                - Each farm is an integrated individuality;<br />
                - Soil conservation practices;<br />
                - No use of chemical or synthetic fertilizers, only natural control of pesticide products;<br />
                - Nature conservation Practices;<br />
                - Quality in social work;<br />
                - Homeopathic biodynamic preparations application that enhance the vitality of the environment, of plants and the final product;<br />
                - No use of GMO (Genetically Modified Organisms) products.<br /><br />
                The biodynamic wines have been acclaimed by its aromas, bouquets and textures. The non-use of pesticides allows the emergence of natural fungi during cultivation and subsequent fermentation that give quality and unique regional flavor, characteristics of each individual "terroir". The biodynamic homeopathic preparations give the vitality, authenticity, character and individuality to wines. There are several wines currently awarded.<br /><br />
                Biodynamic wines internationally acclaimed among others are: Vignoble de la Coulée de Serrant of Nicolas Jolly, France; Nikolaihof of Austria; Champagne Fleury of France.<br /><br />
                We welcome all who are interested in biodynamics and are available for any questions.
            </p>
            <br />
            <!--
            <h2 class="tituloCertificaoInterno">
                Additional Information</h2>
            <a class="itemNormas" href="/Certificacoes/DownloadFile?name=NORMAS%20PARA%20VINHO%20DEMETER%202008.pdf">
                Demeter Production Standards </a><a class="itemNormas" href="/Certificacoes/DownloadFile?name=2011%20APRESENTA%C3%87%C3%83O%20IBD-%20QUALITY%20WINE%20%5BModo%20de%20Compatibilidade%5D.pdf">
                    IBD Q. WINE Presentation</a>
            <br />
            <br />
            <br />
            <br /> -->
            <br />
    </div>
    <!--quemsomos-->
</asp:Content>
