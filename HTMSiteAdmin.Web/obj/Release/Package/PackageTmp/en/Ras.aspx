﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="Ras.aspx.cs" Inherits="HTMSiteAdmin.Web.en.Ras" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Socio-Environmental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px;
                    margin-right: 6px; margin-left: 6px;" />
                Rainforest Alliance (RFA) </h2>
            <a href="/">return to home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
       
            <img src="media/img/logoCertificadoRAS.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                About the seal</h2>
           The standard for use of the Rainforest Alliance Certified™ seal was developed by the Sustainable Agriculture Network, composed of international conservationist and independent organizations and aims at defining proper agricultural practices that cause less impact on consumers’ health, as well as establishing standards for management of the environment and workers involved in the production process.
            <br />
            <br />  
        
            <h2 class="tituloCertificaoInterno">Step by Step</h2>
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=a8bda9a3-b657-45dd-b0c7-400c9371a1f5"> - Step by Step to certification (Portuguese)</a>
            <br /><br />         
            
            <h2 class="tituloCertificaoInterno">
                Standards Served</h2>            
               - RFA
            <br />
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segments Served</h2>            
                - Agriculture <br />
                - Livestock<br />
                - Chain of custody <br />

          
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Certified Clients
            </h2>

            <asp:Repeater ID="rpt" runat="server">
                <HeaderTemplate>
                    <ul class="list">
                </HeaderTemplate>
                <ItemTemplate>                    
                    <li><a href="<%# Eval("Caminho") %>" class="imagem"><%# Eval("Descricao")%></a></li>                    
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
    </div>
    <!--quemsomos-->
</asp:Content>
