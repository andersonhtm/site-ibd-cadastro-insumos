﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="JasOrganic.aspx.cs" Inherits="HTMSiteAdmin.Web.en.JasOrganic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Organic
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                JAS Organic</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoJASOrganic.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                About the Seal</h2>
            The JAS Organic seal is the Japanese seal accredited by the Japanese Department
            of Agriculture.
            <br />
            <br />
            This seal is offered with the objective of developing, maintaining, and expanding
            access of products from diverse countries to the Japanese market.<br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Standards Served</h2>
                <!-- <span class="itemNormas">JAS</span> -->
                - JAS
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segments Served</h2>
            - Agriculture<br />
            - Livestock<br />
            - Processing<br />
            - Wild harvesting<br />
            <br />
            <!--
            <h2 class="tituloCertificaoInterno">
                Additional Information</h2>
            <span class="itemNormas">JAS Procedures</span> <span class="itemNormas">JAS Production</span>
            <br />
            <br />
            -->
        </p>
        <br />
        <br />
        <br />
    </div>
    <!--quemsomos-->
</asp:Content>
