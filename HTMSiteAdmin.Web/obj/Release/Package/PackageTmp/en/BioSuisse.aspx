﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="BioSuisse.aspx.cs" Inherits="HTMSiteAdmin.Web.en.BioSuisse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }

        .itemNormas {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }

        .limpaFloat {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Organic
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px; margin-left: 6px;" />
                Bio Suisse</h2>
            <a href="/">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">

        <img src="media/img/logo_biosuisse.png" alt="Bio Suisse" style="float: right; margin-left: 15px; margin-bottom: 10px; margin-top: 10px" />

        <h2 class="tituloCertificaoInterno">About the seal</h2>
        Bio Suisse is a private-sector organization of Swiss organic farmers whose label stands for:
        <br />
        <br />
        - 100% organic management of the farm including animal husbandry<br />
        - Natural diversity on the organic farm<br />
        - Prohibition of deforesting
        <br />
        - Ethologically sound livestock management and feeding<br />
        - No use of chemically synthesized pesticides or fertilizers<br />
        - Nitrogen and Phosphorus limits in fertilization<br />
        - Prevention of erosion<br />
        - Obligation of rotation with a minimum of 20 % soil-building crops and 12 month break for the same crop<br />
        - Green cover in vineyards and orchards<br />
        - Limits on the use of copper<br />
        - Prohibition of synthetic pyrethroids / bioherbicides / growth regulators<br />
        - No use of genetic engineering<br />
        - No use of unnecessary additives such as flavourings and colourings<br />
        - Prohibition of transport by air<br />
        - Full traceability from buyer back to the farmer 
        <br />
        <br />

        <h2 class="tituloCertificaoInterno">Bio Suisse</h2>
        Bio Suisse is the owner of the registered trademark Bud. Its standards are private law guidelines and exceed the minimum legal requirements in essential respects (EU-Eco-Regulation 834/2007 or equivalent). 
        Operations abroad complying with the necessary standards and wishing to supply their products to Switzerland can obtain the Bio Suisse certification.
        
        <br />
        <br />
        <h2 class="tituloCertificaoInterno">Bio Suisse certification</h2>
        A valid organic certification equivalent to the Swiss Ordinance on Organic Farming (for example, Council Regulation (EC) No. 834/2007 or NOP) is the basis for a certification according to Bio Suisse standards abroad. 
        In general, a buyer/importer of the products in Switzerland must be named. He will carry all the costs of the Bio Suisse certification.
        <br />
        International Certification Bio Suisse AG (ICB ), the subsidiary of Bio Suisse, carries out the certification. Detailed information is available under: <a href="www.icbag.ch">www.icbag.ch</a>

        <br />
        <br />
        <h2 class="tituloCertificaoInterno">Procedure</h2>
        IBD Certificações Ltda. offers a Bio Suisse inspection together with the regular organic inspection. Please contact us should you wish a Bio Suisse inspection in addition to the organic inspection. We are happy to help you with any questions.
        
        <br />
        <br />
        <h2 class="tituloCertificaoInterno">Standards Served</h2>
        - Bio Suisse          
        <br />
        <br />
        <h2 class="tituloCertificaoInterno">Segments Served</h2>
        - Agriculture
        <br />
        - Livestock
        <br />
        - Food processed products
        <br />
        - Wild collection 
        <br />


        <br />
        <br />
        <br />
        <br />
    </div>
</asp:Content>
