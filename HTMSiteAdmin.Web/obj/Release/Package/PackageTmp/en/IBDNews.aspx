﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="IBDNews.aspx.cs" Inherits="HTMSiteAdmin.Web.en.IBDNews" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                IBD News</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="news">
        <div>
            <p style="color: #595941; font-size: 15px;">
                IBD News is a periodical publication produced by IBD Certifications.<br />
                Access IBD News by choosing the edition you would like to read.</p>
        </div>
        <div class="ibdNews">
            <asp:DataList runat="server" ID="dtIBDNews" OnItemDataBound="dtIBDNews_ItemDataBound"
                RepeatLayout="Flow" ShowFooter="False" ShowHeader="False">
                <ItemTemplate>
                    <asp:Literal ID="ltrIBDNews" Text="ltrIBDNews" runat="server" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <!--ibdNews-->
    </div>
    <!--news-->
</asp:Content>
