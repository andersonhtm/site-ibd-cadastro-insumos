﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="ServicosCertificacoes.aspx.cs" Inherits="HTMSiteAdmin.Web.en.ServicosCertificacoes" %>

<%@ Register Src="~/en/UserControls/ucSelosRodape.ascx" TagPrefix="uc1" TagName="ucSelosRodape" %>


<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Services
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Certifications</h2>
            <a href="default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos" class="interno">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/imagem_interna_13.jpg" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <p>
                In order to meet certification requirements, IBD relies on a team of specialized
                inspectors that audit agricultural properties and production processes to verify
                conformity of cultivation and/or processes with organic and biodynamic production
                norms.<br />
                <br />
                Certification requires a series of precautions, such as the detoxification of the
                soil for 1 to 3 years in areas under transition from chemical to organic agriculture,
                the absence of chemical fertilizers and pesticides, attention to ecological aspects
                (for example, the conservation of Permanent Preservation Areas through reforestation
                of riparian forests), preservation of native species and natural springs, respect
                for Indian reserves and social standards based on international labor agreements,
                humane treatment of animals, and, for the Fair Trade Ecosocial standards, involvement with
                social and environmental conservation projects.
                <br />
                <br />
                The certification process is of fundamental importance to the viability of organic
                agriculture, serving as an important tool for stimulating ecological and social
                awareness. In this sense, IBD, aware of its responsibility as an agent of social
                transformation, financially supports agricultural research projects as well as projects
                in assistance and monitoring for small farmers.
                <h2 class="tituloCertificaoInterno">
                    Inspection</h2>
                Through regular monitoring activities, IBD acts to promote equilibrium between economic
                activities and environmental conservation. Under the socioenvironmental certification
                scopes, large certified projects maintain reforestation programs and projects aimed
                at protecting wildlife, such as nurseries for the production of native species,
                reforestation projects in riparian areas, the creation of wildlife corridors, the
                protection of water resources, fire prevention programs for conserving native vegetation,
                and the raising of animals at risk of extinction for repopulation of natural habitats.<br />
                <br />
                Certification contributes to a process of profound transformation of the agricultural
                and industrial environment. Social responsibility is stimulated and companies are
                motivated to provide fair wages, dignified working conditions, and professional
                training and retraining, causing a positive impact on the quality of life for workers.
                <h2 class="tituloCertificaoInterno">
                    Family Agriculture</h2>
                Among family farmers, organic and socioenvironmental agriculture has been stimulating
                productive systems based on the use of existing tree species, agroforestry systems,
                which have proven a viable alternative for environmental and energetic sustainability.<br />
                <br />
                Through the formation of producer groups, the participation of small farmers within
                the certification process has become viable. Farmers are recognized in many cases
                as those responsible for maintaining ecosystems, conserving the forests, rivers,
                and wildlife.<br />
                <br />
            </p>
            <h2 class="tituloCertificaoInterno">
                Segments served</h2>
            <p>
                &bull; Apiculture<br />
                &bull; Meat and Dairy Cattle
                <br />
                &bull; Fish farming and aquaculture
                <br />
                &bull; Food processing
                <br />
                &bull; Agricultural production
                <br />
                &bull; Cosmetics
                <br />
                &bull; Material inputs
                <br />
                &bull; Raw materials for cosmetics
                <br />
                &bull; Production of sanitizers
                <br />
                &bull; Wine Production
                <br />
                &bull; Textiles
                <br />
                &bull; Non Genetically Modified Products
                <br />
                &bull; Restaurants and hotels
                <br />
                &bull; Forestry and other wild harvested products<br />
            </p>
        </p>
        <uc1:ucSelosRodape runat="server" id="ucSelosRodape" />
    </div>
    <!--quemsomos-->
</asp:Content>
