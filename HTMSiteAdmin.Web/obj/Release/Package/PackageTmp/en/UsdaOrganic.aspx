﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="UsdaOrganic.aspx.cs" Inherits="HTMSiteAdmin.Web.en.UsdaOrganic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Organic
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                USDA Organic</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoUSDAOrganic.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                About the Seal</h2>
            The USDA Organic Seal is a North American Seal accredited by the Department of Agriculture
            of the United States (USDA).<br />
            <br />
            This seal is offered with the objective of developing, maintaining and expanding
            access of products from various countries to the North American market.
            <br />
            <br />

            <h2 class="tituloCertificaoInterno">Step by Step</h2>

            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=07511992-13b3-4b3f-80a2-3d7cee456607">Step by Step to certification (English)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=93014008-7780-4669-90c7-e90ccffbab87">Step by Step to certification (Portuguese)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=787619fb-fc31-4eed-a1bf-404738c0ecb0">Step by Step to certification (Spanish)</a><br />
            <br /><br />

            <h2 class="tituloCertificaoInterno">Step by Step Producers Groups certification</h2>

            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=b66b1167-f68e-4810-a581-53bbf9d4e7c6">Step by Step to certification (English)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=40b81a87-3a1c-4e7b-990f-4e0ca875c0e2">Step by Step to certification (Portuguese)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=5ad12599-e08f-4656-a880-76c4304a73e2">Step by Step to certification (Spanish)</a><br />
            <br /><br />

            <h2 class="tituloCertificaoInterno">
                Standards Served</h2>
                <!-- <span class="itemNormas">NOP</span> -->
                - NOP
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segments Served</h2>
            - Agriculture<br />
            - Livestock<br />
            - Fibers<br />
            - Processing
            <br />
            - Wild harvesting<br />
            - Cosmetics<br />
            <br />
            <!--
            <h2 class="tituloCertificaoInterno">
                Additional Information</h2>
            <span class="itemNormas">NOP Extract (December/06 - Portuguese)</span> <span class="itemNormas">
                National List(Sept./06 -Portuguese)</span> <span class="itemNormas">NOP (December/06
                    - English)</span>
            <br />
            -->
        </p>
        <br />
        <br />
        <br />
    </div>
    <!--quemsomos-->
</asp:Content>
