﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="TrabalheConosco.aspx.cs" Inherits="HTMSiteAdmin.Web.en.TrabalheConosco" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Work with Us</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="painelCliente" class="trabalheConosco">
        <div>
            <p style="font-size: 15px;">
                Become a part of the IBD team. Send your resume to: <a href="mailto:ibd@ibd.com.br">
                    ibd@ibd.com.br</a>
                <br />
                If you prefer, use the form below:</p>
        </div>
        <div style="float: left; width: 370px;">
            <ul>
                <li>
                    <asp:Label ID="Label1" Text="name" runat="server" AssociatedControlID="txtNome" />
                    <asp:TextBox ID="txtNome" runat="server" Style="float: left;" />
                    <asp:RequiredFieldValidator ID="rfvNome" ControlToValidate="txtNome" runat="server"
                        ErrorMessage="- Name" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label2" Text="e-mail" runat="server" AssociatedControlID="txtEmail" />
                    <asp:TextBox ID="txtEmail" runat="server" Style="float: left;" />
                    <asp:RequiredFieldValidator ID="rfvEmail" ControlToValidate="txtEmail" runat="server"
                        ErrorMessage="- E-Mail" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="- Invalid E-mail" Display="None" SetFocusOnError="true" ValidationGroup="vgForm"
                        ValidationExpression="\S+@\S+.\S{2,3}"></asp:RegularExpressionValidator>
                </li>
                <li>
                    <asp:Label ID="Label3" Text="telephone" runat="server" AssociatedControlID="txtCelular" />
                    <asp:TextBox ID="txtCelular" runat="server" Style="float: left;" />
                    <asp:RequiredFieldValidator ID="rfvCelular" ControlToValidate="txtCelular" runat="server"
                        ErrorMessage="- Telephone" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label7" Text="address" runat="server" AssociatedControlID="txtEndereco" />
                    <asp:TextBox ID="txtEndereco" runat="server" Style="float: left;" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtEndereco"
                        runat="server" ErrorMessage="- Address" Display="None" SetFocusOnError="True"
                        ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label5" Text="city" runat="server" AssociatedControlID="txtMunicipio" />
                    <asp:TextBox ID="txtMunicipio" runat="server" Style="float: left;" />
                    <asp:RequiredFieldValidator ID="rfvMunicipio" ControlToValidate="txtMunicipio" runat="server"
                        ErrorMessage="- City" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label4" Text="state" runat="server" AssociatedControlID="txtEstado" />
                    <asp:TextBox ID="txtEstado" runat="server" Style="float: left;" />
                    <asp:RequiredFieldValidator ID="rfvEstado" ControlToValidate="txtEstado" runat="server"
                        ErrorMessage="- State" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label6" Text="country" runat="server" AssociatedControlID="txtPais" />
                    <asp:TextBox ID="txtPais" runat="server" Style="float: left;" />
                    <asp:RequiredFieldValidator ID="rfvPais" ControlToValidate="txtPais" runat="server"
                        ErrorMessage="- Country" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label8" Text="comments" runat="server" AssociatedControlID="txtComentarios" />
                    <asp:TextBox ID="txtComentarios" runat="server" Style="float: left;" Rows="10" TextMode="MultiLine" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtComentarios"
                        runat="server" ErrorMessage="- Comments" Display="None" SetFocusOnError="True"
                        ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label9" Text="file" runat="server" AssociatedControlID="afuAnexo" />
                </li>
                <li>
                    <ajaxToolkit:AsyncFileUpload ID="afuAnexo" runat="server" Style="display: inline-block;
                        float: right;" OnUploadedComplete="afuAnexo_UploadedComplete" 
                        OnClientUploadStarted="uploadStarted"
                        OnClientUploadComplete="uploadComplete" />
                </li>
                <li>
                    <label>
                        &nbsp;</label>
                    <asp:LinkButton Style="float: left;" ID="btnCadastrar" runat="server" OnClick="btnCadastrar_Click"
                        CssClass="cadastrar btnCadastrar" ValidationGroup="vgForm">send</asp:LinkButton>
                </li>
            </ul>
            <asp:ValidationSummary ID="vsForm" runat="server" ValidationGroup="vgForm" CssClass="erroLogin"
                HeaderText="Os seguintes campos contêm erros ou são obrigatórios:" />
        </div>
        <div style="float: right;">
            <img src="media/img/fotoTrabalheConosc.jpg" alt="" width="266" height="200" /></div>
    </div>

    <script language="javascript" type="text/javascript">
        function CheckExtension(fileName) {
            var ext = fileName.substring(fileName.lastIndexOf(".") + 1).toUpperCase();
            return !(ext != 'TXT' && ext != 'DOC' && ext != 'DOCX' && ext != 'PDF')
        }

        function uploadStarted(sender, args) {
            var fileName = args.get_fileName();

            if (!CheckExtension(fileName))
                alert('Apenas arquivos com extensões dos tipos TXT, DOC, DOCX e PDF são permitidos.');

            $(".btnCadastrar").attr("disabled", "disabled").hide();
        }

        function uploadComplete(sender, args) {
            var fileName = args.get_fileName();

            if (CheckExtension(fileName))
                $(".btnCadastrar").removeAttr('disabled').show();
        }

    </script>
    <!--painelCliente-->
</asp:Content>
