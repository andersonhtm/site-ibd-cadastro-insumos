﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="Uebt.aspx.cs" Inherits="HTMSiteAdmin.Web.en.Uebt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Socio-Environmental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px;
                    margin-right: 6px; margin-left: 6px;" />
                UEBT</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoUebt.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">Union for Ethical BioTrade</h2>
            The Union for Ethical BioTrade (UEBT) is a nonprofit association that promotes the ‘Sourcing with
            Respect’ of ingredients that come from biodiversity. Members commit to gradually ensuring that
            their sourcing practices promote the conservation of biodiversity, respect traditional knowledge and
            assure the equitable sharing of benefits all along the supply chain.
            <br />
            <br />

            <h2 class="tituloCertificaoInterno">Ethical BioTrade</h2>
            In joining UEBT, a company agrees to comply with the principles of Ethical BioTrade. This means using practices that promote the sustainable use of natural ingredients, while ensuring that all contributors along the supply chain are paid fair prices and share the benefits derived from the use of biodiversity. By adopting the Ethical BioTrade practices of UEBT, companies foster long-term relationships with their source communities, creating employment, contributing to local development and helping to preserve
            local ecologies.
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">Why join?</h2>
            Consumers are becoming increasingly aware of biodiversity. Ethical sourcing of biodiversity will eventually become a key requirement for preferred brands and their suppliers. Adopting ethical and sustainable practices allows companies to differentiate themselves in the market, taking the lead in meeting the demand for ethical products. UEBT membership reassures investors and consumers that steps are being taken to ensure ethical and sustainable use of biodiversity. UEBT membership does not only contribute to a better world; it also makes good business sense!
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">UEBT logo</h2>
            UEBT membership means setting up a Biodiversity Management System that covers all natural ingredient
            sourcing, not just certain supply chains. A member of UEBT can use its logo to illustrate that it is working
            to bring its practices in line with Ethical BioTrade. However, the logo applies to the organisation and its sourcing principles, rather than the compliance of particular products. In this respect it has been designed
            for use in a company’s corporate communication rather than in product specific marketing.
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">Ethical BioTrade Standard</h2>
            <ul>
            <li>
            1-	Biodiversity conservation - Preserving ecosystems and promoting practices that conserve and restore biodiversity.
            </li>
            <li>
            2-	Sustainable use - Practices that safeguard the long-term functions of ecosystems and limit negative environmental impacts.
            </li>
            <li>
            3-	Fair and equitable benefit sharing - Use of biodiversity and traditional knowledge recognizes rights, promotes dialogue and provides equitable prices.
            </li>
            <li>
            4-	Socio-economic sustainability - Adopting quality management and financial practices that are sustainable and socially acceptable.
            </li>
            <li>
            5-	Legal compliance - Respecting all relevant international, national and local regulations.
            </li>
            <li>
            6-	Respect for the rights of actors - Taking into account human rights, working conditions, and therights of indigenous and local communities.
            </li>
            <li>
            7-	Clarity about land tenure - Respecting rights over land and natural resources.
            </li>
            </ul>
            <br />
            <h2 class="tituloCertificaoInterno">
                A few clients</h2>
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=29" class="imagem">
                    <img src="media/img/logo_vegeflora.png" width="192" height="126" /></a> <a href="ClientesDetalhes.aspx?id_conteudo=29"
                        class="titulo">Vegeflora</a>
            </div>
            <br />
            <br />
    </div>
    <!--quemsomos-->
</asp:Content>
