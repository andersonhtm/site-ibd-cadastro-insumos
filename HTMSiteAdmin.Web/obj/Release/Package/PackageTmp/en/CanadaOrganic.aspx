﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="CanadaOrganic.aspx.cs" Inherits="HTMSiteAdmin.Web.en.CanadaOrganic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Organic
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Canada Organic (US/NOP Equivalence)</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoCanadaOrganic_.png" alt="" style="float: right;
                margin-left: 15px; margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                About the Seal</h2>
            Clients certified NOP are also able to be certified in compliance with the terms of the US-Canada Organic Equivalency Arrangement.
            <br /><br />
            It is possible to use both of the seals on packaging and to trade the products in the US and Canada.<br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Standards Served</h2>
                <!-- <span class="itemNormas">Canada Organic</span> -->
                NOP and the specific terms of the equivalence agreement:<br />
                <br />
                - No use of sodium nitrate <br />
                - No hydroponic or aeroponic production methods used <br />
                - Agricultural products derived from animals (with the exception of ruminants) must be produced according to livestock stocking rates as set out in CAN /CGSB32.310-2006 (amended October 2008).<br />

            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segments Served</h2>
            - Agriculture<br />
            - Livestock<br />
            - Processing<br />
            - Extraction<br />
            <br />
            <br />
            <br />
            <br />
    </div>
    <!--quemsomos-->
</asp:Content>
