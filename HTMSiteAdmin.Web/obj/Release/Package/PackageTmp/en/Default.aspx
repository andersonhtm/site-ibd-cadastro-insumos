﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HTMSiteAdmin.Web.en.Default" %>

<%@ Register Src="UserControls/ucTopMenu.ascx" TagName="ucTopMenu" TagPrefix="uc1" %>
<%@ Register Src="UserControls/ucUltimasNoticias.ascx" TagName="ucUltimasNoticias"
    TagPrefix="uc2" %>
<%@ Register Src="UserControls/ucChamadaEventos.ascx" TagName="ucChamadaEventos"
    TagPrefix="uc3" %>
<%@ Register Src="UserControls/ucChamadaDownloads.ascx" TagName="ucChamadaDownloads"
    TagPrefix="uc4" %>
<%@ Register Src="UserControls/ucChamadaClientes.ascx" TagName="ucChamadaClientes"
    TagPrefix="uc5" %>
<%@ Register Src="UserControls/ucClientesCertificados.ascx" TagName="ucClientesCertificados"
    TagPrefix="uc6" %>
<%@ Register Src="UserControls/ucInsumosAprovados.ascx" TagName="ucInsumosAprovados"
    TagPrefix="uc7" %>
<%@ Register Src="~/en/UserControls/ucSelosRodape.ascx" TagPrefix="uc1" TagName="ucSelosRodape" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="classification" content="" />
    <meta name="Description" content="" />
    <meta name="Keywords" content="" />
    <meta name="robots" content="ALL" />
    <meta name="distribution" content="Global" />
    <meta name="rating" content="General" />
    <meta name="Author" content="" />
    <meta name="doc-class" content="Completed" />
    <meta name="doc-rights" content="Public" />
    <link href="media/css/estilos.css" rel="stylesheet" type="text/css" />
    <link href="../Media/css/common.css" rel="stylesheet" type="text/css" /> 
    <link rel="shortcut icon" type="image/x-icon" href="media/img/favicon.ico" />
    <link href="media/css/nivo.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="media/js/jquery.js"></script>
    <script type="text/javascript" src="media/js/jquery.nivo.js"></script>
    <script type="text/javascript" src="media/js/jquery.innerfade.js"></script>
    <script type="text/javascript" src="media/js/jquery.easing.1.2.js"></script>
    <script type="text/javascript" src="media/js/util.js"></script>
    <script type="text/javascript" src="media/js/index.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $(".itemNews a:odd").css("color", "#797949");
            $(".itemNews a:odd").hover(function () { $(this).css("color", "#809e25"); }, function () { $(this).css("color", "#797949"); });
            $("#painelCliente a").attr("Target", "_self");
        });

    </script>
    <style type="text/css">
        form a.cadastrar
        {
            width: 73px;
            height: 21px;
            color: #fbfff0;
            background: url(media/img/btCadastrar.jpg) no-repeat;
            display: block;
            font: normal 12px Georgia, "Times New Roman" , Times, serif;
            text-align: center;
            line-height: 21px;
            float: right;
            margin-right: 81px;
            margin-top: 15px;
        }
    </style>
    <title>IBD Certifica&ccedil;&otilde;es</title>
</head>
<body>
    <form runat="server" id="form1" target="_self">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div class="wrap">
        <div class="centro" style="overflow: visible;">
            <div class="topo">
                <div class="logo">
                    <a href="/en">
                        <img src="media/img/logo.png" alt="IBD Certificações" title="IBD Certificações" width="180"
                            height="79" /></a></div>
                <!--logo-->
                <h1>
                    Agricultural and Food Inspections and Certifications<span> The only 100%-Brazilian certifier
                        with international reach</span></h1>
                <div class="idiomas">
                    <a href="../pt/Default.aspx" class="br" title="Este site em português."></a>
                </div>
                <!--idiomas-->
                <uc1:ucTopMenu ID="ucTopMenu1" runat="server" />
            </div>
            <!--topo-->
        </div>
        <!--centro-->
        <div class="centro">
            <div class="bloco">
                <div class="banner">
                    <div id="slider">
                        <asp:Literal ID="ltrLinkImagemBanner1" Text="ltrLinkImagemBanner1" runat="server" />
                        <asp:Literal ID="ltrLinkImagemBanner2" Text="ltrLinkImagemBanner2" runat="server" />
                        <asp:Literal ID="ltrLinkImagemBanner3" Text="ltrLinkImagemBanner3" runat="server" />
                    </div>
                </div>
                <div class="opcoesSlider">
                    <div class="itemSlider">
                        <asp:Literal ID="ltrMiniaturaBanner1" Text="ltrMiniaturaBanner1" runat="server" />
                        <h2>
                            <asp:Literal ID="ltrTituloBanner1" Text="ltrTituloBanner1" runat="server" /></h2>
                        <p>
                            <asp:Literal ID="ltrSubTituloBanner1" Text="ltrSubTituloBanner1" runat="server" /></p>
                    </div>
                    <!--itemSlider-->
                    <div class="itemSlider">
                        <asp:Literal ID="ltrMiniaturaBanner2" Text="ltrMiniaturaBanner2" runat="server" />
                        <h2>
                            <asp:Literal ID="ltrTituloBanner2" Text="ltrTituloBanner2" runat="server" /></h2>
                        <p>
                            <asp:Literal ID="ltrSubTituloBanner2" Text="ltrSubTituloBanner2" runat="server" /></p>
                    </div>
                    <!--itemSlider-->
                    <div class="itemSlider">
                        <asp:Literal ID="ltrMiniaturaBanner3" Text="ltrMiniaturaBanner3" runat="server" />
                        <h2>
                            <asp:Literal ID="ltrTituloBanner3" Text="ltrTituloBanner3" runat="server" /></h2>
                        <p>
                            <asp:Literal ID="ltrSubTituloBanner3" Text="ltrSubTituloBanner3" runat="server" /></p>
                    </div>
                    <!--itemSlider-->
                </div>
                <!--opcoesSlider-->
            </div>
            <!--bloco-->
            <div class="bloco">

                <table class="table-home">
                    <tr>
                        <td rowspan="2">
                            <div class="box" id="noticias">
                                <h3>
                                    Lastest News</h3>
                                <uc2:ucUltimasNoticias ID="ucUltimasNoticias1" runat="server" />
                                <a href="Noticias.aspx" class="mais">more news</a>
                            </div>
                        </td>
                        <td>
                             <!--#noticias.box-->
                            <div class="box" id="eventos">
                                <h3>
                                    Events</h3>
                                <uc3:ucChamadaEventos ID="ucChamadaEventos1" runat="server" />
                                <a href="EventosFeiras.aspx" class="mais">more events</a>
                            </div>
                        </td>
                        <td>
                            <div class="box" id="quemSomos">
                                <h3>
                                    About Us</h3>
                                <p>
                                    IBD Certifications is a 100%-Brazilian company that carries out agricultural, processing,
                                    wild harvesting, organic, biodynamic and fair trade inspection and certification
                                    activities.</p>
                                <a href="QuemSomos.aspx" class="mais2" style="font-size:11px;">learn more</a>
                            </div>
                            <!--quemSomos-->

                        </td>
                    </tr>
                    <tr>                       
                        <td>
                            <div class="box" id="diretrizes">
                                <h3>
                                    Standards and Legislations</h3>
                                <uc4:ucChamadaDownloads ID="ucChamadaDownloads1" runat="server" />
                                <div class="mais">
                                    <a href="DiretrizesLegislacao.aspx">more documents</a>
                                </div>
                                <!--mais-->
                            </div>
                            <!--box-->
                        </td>
                        <td>
                            <div class="box" id="certificados">
                                <uc6:ucClientesCertificados ID="ucClientesCertificados1" runat="server" />
                            </div>
                            <!--#certificados.box-->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="box" id="cadastre">
                                <h3>
                                    Register</h3>
                                <fieldset>
                                        <p>Register to receive newsletters and updates about IBD Certifications.</p>
                                        <br />
                                        <p>
                                        <label>
                                            Name</label>
                                        <asp:TextBox runat="server" ID="txtCadastreSeNome" Width="300px" Style="float: left;" />
                                    </p>
                                    <p>
                                        <label>
                                            E-mail</label>
                                        <asp:TextBox runat="server" ID="txtCadastreSeEmail" Width="300px" Style="float: left;" />
                                    </p>
                                    <p>
                                        <asp:LinkButton Text="send" CssClass="cadastrar" runat="server" ID="btnCadastrar"
                                            OnClick="btnCadastrar_Click" /></p>
                                </fieldset>
                            </div>
                            <!--#cadastre.box-->
                        </td>
                        <td>
                            <div class="box" id="painelCliente">
                                <asp:Literal ID="ltrBannerInferior4" Text="ltrBannerInferior4" runat="server" />
                                <div style="height: 85px; width: 142px; float: left;">
                                    <asp:Literal ID="ltrBannerInferior5" Text="ltrBannerInferior5" runat="server" />
                                </div>
                                <div style="height: 85px; width: 142px; float: right;">
                                    <asp:Literal ID="ltrBannerInferior6" Text="ltrBannerInferior6" runat="server" />
                                </div>
                            </div>
                        </td>
                        <td>
                            <!--#painelCliente.box-->
                            <div class="box" id="insumos">
                                <uc7:ucInsumosAprovados ID="ucInsumosAprovados1" runat="server" />
                            </div>
                            <!--#insumos.box-->
                        </td>
                    </tr>
                </table>
            </div>
            <!--bloco-->
        </div>
        <!--centro-->
    </div>
    <!--wrap-->
    <div class="wrap3">
        <uc1:ucSelosRodape runat="server" id="ucSelosRodape" />
    </div>
    <!--wrap3-->
    <div class="rodape">
        <div class="pattern">
            <div class="centro">
                <img src="media/img/arvore.png" alt="" />
                <div class="rodapeInterno">
                    <div class="menu">
                        <a href="QuemSomos.aspx">About Us</a> <a href="ServicosCertificacoes.aspx">Services</a>
                        <a href="IbdOrganico.aspx">Certifications</a> <a href="Acreditadores.aspx">Accreditations</a>
                        <a href="CadastreSe.aspx">Clients</a> <a href="Noticias.aspx">Resources</a> <a href="CadastreSe.aspx">
                            Costumer Service</a>
                    </div>
                    <div class="dads">
                        <address>
                            <em>Address</em>: Rua Amando de Barros, 2275 - Centro - CEP: 18.602.150 – Botucatu - SP
                        </address>
                        <p>
                            <em>(14)</em> 3811-9800</p>
                    </div>
                </div>
                <!---->
                <div class="creditos">
                    <span>© Copyright – The publication or use of text and images contained within this
                        site will only be permitted with authorization from IBD.</span><a target="_blank" href="http://www.htm.com.br"></a></div>
                <!--creditos-->
            </div>
            <!--centro-->
        </div>
        <!--pattern-->
    </div>
    <!--rodape-->
    </form>
    <script type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-44692999-12', 'ibd.com.br');
        ga('send', 'pageview');
    </script>
</body>
</html>
