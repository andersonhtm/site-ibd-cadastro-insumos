﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="InsumoAprovadoIbd.aspx.cs" Inherits="HTMSiteAdmin.Web.en.InsumoAprovadoIbd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Diverse<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                IBD Approved Inputs</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoInsumosAprovados.png" alt="" style="float: right;
                margin-left: 15px; margin-bottom: 10px;" />
        </p>
        <h2 class="tituloCertificaoInterno">
            About the Seal</h2>
        <p>
            IBD Certifications created the Material Input Approval Program with the objective
            of evaluating the possible use of commercially available inputs for conformity with
            primary organic production standards (USA, European, IFOAM, Japanese, Canadian, 
            Brazilian and Demeter). IBD has developed its own unique standards and procedures for certifying
            inputs at a worldwide level, guaranteeing the safety, credibility and reliability
            of approved inputs and providing this guarantee to producers and companies interested
            in using these products.
            <br />
            <br />
            In order to confer greater credibility in the program, IBD became ISO 65 accredited
            through IOAS in 2009, making it one of the few input certification programs in the
            world to obtain this accreditation.<br />
            <br />
            Through this program, producers gain an important research tool for identifying
            the principal inputs for their operations, guaranteeing sustainability and success
            in their organic production activities.
            <br />
            <br />
            The approval program, geared towards manufacturers, importers, and distributors
            of inputs located in Brazil and worldwide, evaluates inputs in accordance with organic
            agricultural, processing and livestock standards.
            <br />
        </p>
        <br />
        <h2 class="tituloCertificaoInterno">
            Input Categories</h2>
        <p>
            Use in Agriculture:<br />
            &bull; Mineral and/or organic fertilizers and soil conditioners;<br />
            &bull; Pesticides for controlling pests and disease, as well as herbicides;<br />
            &bull; Inputs for use in post-harvest handling (sanitation, cleaning materials);<br />
            &bull; All other inputs for use in agricultural operations (cleaning irrigation
            systems and equipment; insect traps, among others)
            <br />
            <br />
            USE IN LIVESTOCK:<br />
            &bull; Ingredients for feed formulas;<br />
            &bull; Inputs for veterinary treatments;<br />
            &bull; Inputs for controlling ecto and endo parasites;<br />
            &bull; Inputs for equipment and animal sanitization;
            <br />
            <br />
            USE IN FOOD PROCESSING:<br />
            &bull; Non-organic Ingredients of agricultural origin;<br />
            &bull; Non-agricultural ingredients;<br />
            &bull; Pest control products;<br />
            &bull; Inputs for sanitizing equipment and organic food.<br />
        </p>
        <br />
        <h2 class="tituloCertificaoInterno">
            Requirements for approval</h2>
        <p>
            During the process of evaluation IBD carries out the following activities:<br />
            &bull; Documental audit;<br />
            &bull; Audit of the input production process at the factory;<br />
            &bull; Collection and analysis of samples taken from the input production unit and/or
            market;<br />
            <br />
            In order for an input to be approved the company should:<br />
            &bull; Be in conformity with public agencies (City government, ANVISA, MAPA, IBAMA,
            among others);<br />
            &bull; Be able to show product registration with MAPA (Agricultural Ministry) and/or
            ANVISA for commercialization in accordance with declared final use;<br />
            &bull; Be able to guarantee absence of substances/raw materials that are prohibited;<br />
            &bull; Not use production techniques that are prohibited;<br />
            &bull; Guarantee that any contaminants are within allowed limits (microorganisms,
            heavy metals, among others);<br />
            &bull; Have quality control to guarantee the maintenance of the input’s characteristics;<br />
            &bull; Have traceability controls.</p>
        <h2 class="tituloCertificaoInterno">
            Additional Information</h2>
        <p>
            IBD CREDIBILITY GUARANTEE:<br />
            &bull; International accreditations that allow access to the organic market worldwide;<br />
            &bull; Confidentiality policy that provides protection of confidential information
            regarding approved inputs;<br />
            &bull; A professional, experienced, and specialized team involved in the certification
            of organic products;<br />
            &bull; Transparency regarding publicity for all approved inputs on the IBD website.<br />
        </p>
        <br />

        <h2 class="tituloCertificaoInterno">Step by Step</h2>
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=d8785e40-98b0-426e-8962-029edaba2f54"> - Step by Step to certification (English)</a><br />
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=23c1eb35-9e58-4d72-b0ce-bd7456fa9237"> - Step by Step to certification (Portuguese) </a> <br />
        <br />

        <h2 class="tituloCertificaoInterno">
            Contacts</h2>
        &Aacute;lvaro Garcia<br />
        Coordinator of the IBD Material Inputs Approval Program
        <br />
        Contact: +55 (14) 98104-2148<br />
        Skype: alvarogarciarj<br />
        <br />
        <br />
        <br />
    </div>
    <!--quemsomos-->
</asp:Content>
