﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="OrganicoCoreia.aspx.cs" Inherits="HTMSiteAdmin.Web.en.OrganicoCoreia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifications</span><br />
                Organic
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Korea Organic</h2>
            <a href="/">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
       
        <h2 class="tituloCertificaoInterno">
            About the seal</h2>
        This seal is referent to the Act on Promotion of Eco-Friendly Agriculture and Management of Organic Products which includes organic certification of the Ministry for Food, Agriculture, Forestry and Fisheries (MIFAFF) of the Republic of Korea.<br /><br />
        This seal is offered with the objective of developing, maintaining, and expanding access of organic products from diverse countries to the South Korea market.
        <br />
        <br />        
        <br />
        <h2 class="tituloCertificaoInterno">
            Standards Served</h2>
        
            Act on Promotion of Eco-Friendly Agriculture and Management of Organic Products of MIFAFF
            <br />           
           
        <br />
        <br />
        <h2 class="tituloCertificaoInterno">
            Segments Served</h2>
            - Agriculture <br />
            - Food processed products

        <br />
        <br />
        <br />
        <br />
    </div>
</asp:Content>
