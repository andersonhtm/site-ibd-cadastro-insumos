$(document).ready(function(){

	//$('a[href^=http]').click( function() { window.open(this.href); return false; });
	 
	$('.menu a[href=certificacoes], .menu a[href=servicos], .menu a[href=clientes], .menu a[href=biblioteca], .menu a[href=atendimento]').click(function(){return false;});
	
	$('.menu a[href=certificacoes], #subCertificacoes').hover(function(){
		$('#subCertificacoes').show();
		$('.menu a[href=certificacoes]').addClass('ativo')
	},function(){
		$('#subCertificacoes').hide()
		$('.menu a[href=certificacoes]').removeClass('ativo')
	});
	$("#subCertificacoes div a:last-child").css("border","none");
	
	
	$('.menu a[href=servicos], #subServicos').hover(function(){
		$('#subServicos').show()
		$('.menu a[href=servicos]').addClass('ativo')
	},function(){
		$('#subServicos').hide()
		$('.menu a[href=servicos]').removeClass('ativo')
	});
	$("#subServicos div a:last-child").css("border","none");
	
	
	$('.menu a[href=clientes], #subCliente').hover(function(){
		$('#subCliente').show()
		$('.menu a[href=clientes]').addClass('ativo')
	},function(){
		$('#subCliente').hide()
		$('.menu a[href=clientes]').removeClass('ativo')
	});
	$("#subCliente div a:last-child").css("border","none");
		
	
	$('.menu a[href=biblioteca], #subBiblioteca').hover(function(){
		$('#subBiblioteca').show()
		$('.menu a[href=biblioteca]').addClass('ativo')
	},function(){
		$('#subBiblioteca').hide()
		$('.menu a[href=biblioteca]').removeClass('ativo')
	});
	$("#subBiblioteca div a:last-child").css("border","none");
	
	
	$('.menu a[href=atendimento], #subAtendimento').hover(function(){
		$('#subAtendimento').show()
		$('.menu a[href=atendimento]').addClass('ativo')
	},function(){
		$('#subAtendimento').hide()
		$('.menu a[href=atendimento]').removeClass('ativo')
	});
	$("#subAtendimento div a:last-child").css("border","none");

	// collapsible
	$('.collapsible').bind('click', function () {
	    var header = $(this);
	    var container = header.next('.container');
	    if (container.is(':visible')) {
	        container.slideUp();
	        header.removeClass('opened');
	    } else {
	        container.slideDown();
	        header.addClass('opened');
	    }
	});

});


