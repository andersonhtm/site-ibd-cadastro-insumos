﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en/InternaEn.Master" AutoEventWireup="true"
    CodeBehind="IngredienteNaturalIbd.aspx.cs" Inherits="HTMSiteAdmin.Web.en.IngredienteNaturalIbd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifica&ccedil;&otilde;es e Aprova&ccedil;&otilde;es</span><br />
                Diverse
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                IBD Natural ingredients</h2>
            <a href="Default.aspx">return to home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoIngredienteNatural.png" alt="" style="float: right;
                margin-left: 15px; margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                About the Seal</h2>
            Environment impact caused by our industrial society is influenced by the choices
            that we make on a daily basis regarding the consumer products that we bring into
            our homes and factories. Better choices will bring about a high level of environmental
            sustainability.
            <br />
            <br />
            Making a choice to consume environmentally correct products is not simple, demanding
            information and awareness on the part of the consumer. Transparency regarding the
            industrial manufacturing processes is pertinent and important in this regard. Certification
            of natural and biodegradable cleaning products is a fundamental part of the information
            provided to consumers, influencing consumer choice.
            <br />
            <br />
            Objectives of the standard:<br />
            <br />
            Stimulate and favor the use of products and processes, as well as packaging, which
            causes the least possible environmental impact, with priority given to the use of
            renewable natural resources.
            <br />
            Avoid contact between allergenic products or irritants and the consumer.
            <br />
            Promote the use of natural, organic and wild harvested certified products.
            <br />
            Promote and guarantee cleaning products that do not use petrochemicals.
            <br />
            <br />

             <h2 class="tituloCertificaoInterno">Step by step</h2>

            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=263a6411-a7ad-4d25-99bc-99f6111acfda"> - Natural Ingredients Certification Step by step (English)</a>
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=016e8327-fa0c-4028-acf7-811c35991a8a"> - Natural Ingredients Certification Step by step (Portuguese)</a>

            <br />
            <br />

            <h2 class="tituloCertificaoInterno">
                Standards Served</h2>
            
                <!-- <span class="itemNormas">IBD Standards</span> -->
                - IBD Standards
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segments Served</h2>
            <p>
                - Cosmetics<br />
                - Cleaning products<br />
            </p>
            <br />
            <h2 class="tituloCertificaoInterno">
                More information: Cleaning products</h2>
            <p>
                The question of cleaning products is essentially environmental and goes in two directions.
                <br />
                <br />
                While on the one hand humanity struggles to reduce emissions from fossil fuels,
                these are used at will to manufacture cleaning products. It is important, as such,
                to use raw materials of vegetable origin, renewable and independent of petroleum.
                <br />
                <br />
                The environmental pollution caused by residues that are not rapidly biodegradable
                fills our rivers and oceans. Petrochemicals are not easily biodegradable and cause
                serious environmental damage while they remain unassimilated by nature. To the contrary,
                products derived from plants are easily biodegraded and allow for rapid assimilation.
                <br />
                <br />
                Please review the IBD standards for Organic and Natural Cleaning Products. The development
                of this standard is a victory, considering the difficulty faced currently when researching
                this sector. For IBD, advancement in this area will be slow, however viable. Firstly,
                we should concentrate on eliminating the use of petrochemicals, and then, use inputs
                and raw materials that are certified organic or natural.
            </p>
            <br />
            <a href="../ShowFile.aspx?action=1&fileid=3d42a9f2-2aeb-49af-abb2-7c86acfe855c"
                class="itemNormas" target="_blank">IBD Standards for Cleaning Products</a>
            <br />
            <br />
    </div>
    <!--quemsomos-->
</asp:Content>
