$(document).ready(function(){

	$('input[name=telefone]').mask('(99) 9999-9999')
	initialize();

});


function initialize() {
	var latlng = new google.maps.LatLng(-21.194142, -47.8189001);
	var myOptions = {zoom: 16, center: latlng, mapTypeId: google.maps.MapTypeId.ROADMAP};
	var map = new google.maps.Map(document.getElementById("mapa"), myOptions);
	var image = 'media/img/iconMapa.png';
	var marker = new google.maps.Marker({position: latlng, map: map, title:"Sanetech", icon: image}); 	
	var contentString = '<span style="display:block; line-height:19px; font-size:14px; color:#111;">Sanetech</span><span style="display:block; overflow:hidden; height:24px; font-size:11px; line-height:12px; color:#666;">N&eacute;lio Guimar&atilde;es, 86 <br /> Alto da Boa Vista - Ribeir&atilde;o Preto - SP</span>';
	var infowindow = new google.maps.InfoWindow({content: contentString});	
	infowindow.open(map,marker);
}

                    