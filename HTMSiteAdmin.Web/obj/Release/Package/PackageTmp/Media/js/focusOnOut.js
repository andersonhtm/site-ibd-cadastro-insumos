(function($){
	$.fn.focusOnOut = function(parametros){
		var padrao
			,padroes = {
				corativo : '#000'
				,corinativo : '#CCC'
			}
		padrao = $.extend({},padroes,parametros);
		this.each(function(){

			$(this).focusin(function(){
				foomask = $(this).attr('title');
				if($(this).val() == foomask) {$(this).val("") }
				$(this).css('color',padrao.corativo)
			})

			$(this).focusout(function(){
				if($(this).val() == ''){ $(this).css('color',padrao.corinativo).val(foomask);}
				if($(this).val() != foomask){  $(this).css('color',padrao.corativo)}
			})
		})
	}
})(jQuery);// JavaScript Document