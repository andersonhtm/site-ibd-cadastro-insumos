﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucMobileClientesCertificados.ascx.cs" Inherits="HTMSiteAdmin.Web.pt_mobile.UserControls.ucClientesCertificados" %>
<asp:UpdatePanel runat="server" ID="upClientesCertificados">
    <ContentTemplate>
        <h3>
            Clientes Certificados</h3>
        <fieldset>
            <ul>
                <li>
                    <label>
                        certificado</label>
                    <asp:DropDownList runat="server" ID="ddlCertificado" DataTextField="NOME" DataValueField="ID_CERTIFICADO"
                        OnDataBound="ddlCertificado_DataBound" />
                </li>
                <li>
                    <label>
                        produto</label>
                    <asp:TextBox runat="server" ID="txtClienteCertificadoProduto" />
                </li>
                <li>
                    <label>
                        cliente</label>
                    <asp:TextBox runat="server" ID="txtClienteCertificadoCliente" />
                </li>
                <li>
                    <label>
                        país</label>
                    <asp:DropDownList runat="server" ID="ddlClienteCertificadoPais" DataTextField="PAIS"
                        DataValueField="PAIS" OnSelectedIndexChanged="ddlClienteCertificadoPais_SelectedIndexChanged"
                        AutoPostBack="True" OnDataBound="ddlClienteCertificadoPais_DataBound" />
                </li>
                <li>
                    <label>estado</label>
                    <span id="spanTextoEstado" runat="server" visible="true" 
                        style="float:right;font-weight:normal;color:#A2AC94;font-style:italic;">
                        [escolha um país]
                    </span>
                    <asp:DropDownList runat="server" ID="ddlClienteCertificadoUf" OnDataBound="ddlClienteCertificadoUf_DataBound"
                        DataTextField="ESTADO" DataValueField="ESTADO_SIGLA" Visible="false" />
                </li>
                <li></li>
                <li>
                    <asp:LinkButton ID="btnClienteCertificadoBuscar" runat="server" CssClass="btBuscar"
                        OnClick="btnClienteCertificadoBuscar_Click" Style="margin-right: 0px;">buscar</asp:LinkButton>
                </li>
            </ul>
        </fieldset>
    </ContentTemplate>
</asp:UpdatePanel>
