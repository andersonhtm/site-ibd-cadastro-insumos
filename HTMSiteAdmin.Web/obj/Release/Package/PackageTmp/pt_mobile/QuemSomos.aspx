﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="QuemSomos.aspx.cs" Inherits="HTMSiteAdmin.Web.pt_mobile.QuemSomos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">

    <div class="interna quemsomos">
    
    	<h3 class="tituloGeral">Quem Somos</h3>
        
    	<img src="../media_mobile/img/quemsomos/1.jpg" alt="" width="280" height="93" />
    
		<p>O IBD orgulha-se em ser a maior certificadora da América Latina e a única certificadora brasileira de produtos orgânicos com credenciamento IFOAM (mercado internacional), ISO/IEC 17065 (mercado europeu-regulamento CE 834/2007), Demeter (mercado internacional), USDA/NOP (mercado norte-americano) e INMETRO / MAPA (mercado brasileiro), o que torna seu certificado aceito globalmente.</p>
        
        <img src="../media_mobile/img/quemsomos/2.jpg" alt="" width="280" height="93" />
    
		<p>Além dos protocolos de certificação orgânica, o IBD oferece também certificações sócio-ambientais: RSPO (Roundtable on Sustainable Palm Oil), UEBT (Union for Ethical BioTrade), RAS-Rain Forest Alliance, 4 C Association, UTZ e Fair Trade Ecosocial</p>
        
        <img src="../media_mobile/img/quemsomos/3.jpg" alt="" width="280" height="93" />
    
		<p>Localizado em Botucatu/SP (Brasil), desde sua fundação em 1983, o IBD vem atuando em todos os Estados brasileiros, bem como internacionalmente em mais de 20 países. 
            O IBD tem como filosofia o compromisso com a Terra e o com o Homem, assegurando o respeito ao meio ambiente, boas condições de trabalho e produtos altamente confiáveis.</p>
    
    </div><!--interna-->
</asp:Content>
