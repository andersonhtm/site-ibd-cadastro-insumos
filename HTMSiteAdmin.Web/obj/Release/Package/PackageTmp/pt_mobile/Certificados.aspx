﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="Certificados.aspx.cs" Inherits="HTMSiteAdmin.Web.en_mobile.Certificados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="interna certificados">
    
    	<h3 class="tituloGeral">Certificados</h3>
            	
        <img src="../media_mobile/img/certificados/2.jpg" alt="" style="float:left;"/>
    	<img src="../media_mobile/img/certificados/1.jpg" alt="" style="float:left;"/>
        <img src="../media_mobile/img/certificados/3.jpg" alt=""/>
        <h4 class="tituloInterno">IBD Orgânico</h4>
        <p> 
        <span class="subtit">Sobre o Selo</span>
			O selo IBD Orgânico tem duas características. Ao matricular-se no IBD deverá ser especificado para qual norma deseja ser certificado:<br />
			1) Atende a todas as certificações orgânicas feitas pelo IBD Certificações no mercado interno e é usado em conjunto com o selo de produtos orgânicos do Brasil.
		<span class="subtit">Diretrizes Atendidas</span>
			- Lei 10.831 (SisOrg)<br />
			O uso do selo IFOAM ACCREDITED é opcional.
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/4.jpg" alt=""/>
    	<h4 class="tituloInterno">USDA Organic(Equivalência EUA/NOP e Canadá)</h4>
        <p>
        <span class="subtit">Sobre o Selo</span>
			O selo USDA Organic é o selo norte americano acreditado pelo Departamento de Agricultura dos Estados Unidos (USDA).<br /><br />
			Este selo tem o objetivo de desenvolver, manter e expandir o acesso para produtos de países diversos ao mercado Americano.
		<span class="subtit">Diretrizes Atendidas</span>
        - NOP<br />
        Clientes certificados NOP podem também ser certificados conforme os termos do acordo de equivalência EUA-Canada para venda de produtos no Canadá.
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/5.jpg" alt=""/>
    	<h4 class="tituloInterno">JAS Organic </h4>
        <p>
        <span class="subtit">Sobre o Selo</span>
			O selo JAS Organic é o selo japonês acreditado pelo Departamento de Agricultura Japonês. <br /><br />
	        Este selo tem o objetivo de desenvolver, manter e expandir o acesso para produtos de países diversos ao mercado Japonês.
        <span class="subtit">Diretrizes Atendidas</span>
    	    - JAS
        </p>
        
        <div class="clear"></div>
        
        <h4 class="tituloInterno">Orgânico Coréia</h4>
        <p>
        	<span class="subtit">Sobre o Selo Coreano</span>
				Este selo é referente ao Act on Promotion of Eco-Friendly Agriculture and Management of Organic Products que inclui a certificação orgânica do Ministério da Alimentação, Agricultura, Floresta e Pesca (MIFAFF) da República da Coreia.<br /><br />
				Este selo tem o objetivo de desenvolver, manter e expandir o acesso para produtos orgânicos de países diversos ao mercado Sul Coreano.
			<span class="subtit">Diretrizes Atendidas</span>
            	Norma orgânica do MIFAFF
        </p>
        
        <div class="clear"></div>
        
        <h4 class="tituloInterno">Orgânico Europa</h4>
        <p>
        O IBD esta acreditado pela Comunidade Européia para certificar produtos a serem vendidos na Europa pelo critério da Equivalencia- as normas IBD Orgânico são aprovadas como equivalentes para as nomas orgânicas Européias.<br />
        <span class="subtit">Diretrizes atendidas</span>
        	IBD Orgânico- reconhecido pelas autoridades Européias como equivalente à norma Européia CCE 834.
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/7.jpg" alt=""/>
    	<h4 class="tituloInterno">Natural and Organic Cosmetics (Natrue e IBD)</h4>
        <p>
        	<span class="subtit">Acordo entre IBD e Natrue</span>
				IBD e NATRUE formaram uma poderosa parceria para ajudar a coloca-lo com seu produto cosmético no mercado Europeu. As empresas que já são NATRUE na Europa também poderão exportar ao Brasil com selo IBD. Para maiores informações favor entrar em contato com o IBD através do nosso Fale Conosco.
           <span class="subtit">Diretrizes Atendidas</span>
		        - Natrue
        </p>
        
        <div class="clear"></div>
        
        <h4 class="tituloInterno">Bio Suisse</h4>
        <p>
        <span class="subtit">Sobre o Selo</span>
            Bio Suisse é uma organização privada dos produtores orgânicos suíços, cujo selo representa: <br />
            - Diversidade natural na fazenda orgânica <br />
            - Manejo e alimentação etologicamente corretos na pecuária <br />
            - Não uso de pesticidas ou fertilizantes sintéticos <br />
            - Não uso de engenharia genética <br />
            - Não uso de aditivos alimentares desnecessários como corantes e aromatizantes <br />
            - Processamento não agressivo de alimentos
        <span class="subtit">Diretrizes Atendidas</span>
        	- Bio Suisse
        </p>
        
        <div class="clear"></div>
        
        <h4 class="tituloInterno">KRAV</h4>
        <p>
        <span class="subtit">Sobre o Selo</span>
			KRAV é o selo líder de produtos orgânicos na Suécia. <br />
	        O IBD oferece a verificação dos requisitos extra da KRAV em relação ao regulamento EC834/2007, para os clientes que desejam comercializar seus produtos ou ingredientes para importadores suecos certificados KRAV.
        <span class="subtit">Diretrizes Atendidas</span>
        	- KRAV 
        </p>
        
        <div class="clear"></div>
               
        <img src="../media_mobile/img/certificados/8.jpg" alt=""/>
    	<h4 class="tituloInterno">Demeter Internacional</h4>
        <p>
        <span class="subtit">Sobre o Selo</span>
        	Demeter é a marca que identifica, mundialmente, os produtos biodinâmicos. Os produtos Demeter fazem parte de uma rede ecológica internacional ligada ao Demeter International, sediado na Alemanha.
        <span class="subtit">Diretrizes Atendidas</span>
        	- Demeter 
            
        <span class="subtit">Vinhos Biodinâmicos - O que são vinhos biodinâmicos?</span>
        	A Biodinamica começou em 1924 com as palestras ministradas por Rudolf Steiner na Europa e hoje corresponde a um movimento em mais de 40 países e envolve mais de 4900 produtores.A biodinâmica foi precursora dos orgânicos e o selo de produtos biodinâmicos, o selo Demeter , foi lançado já na década de 50 do século passado.<br /><br />
			Há uma associação internacional e todos os dados estão acessíveis no site: www.demeter.net da Demeter International do qual a organização em que trabalho IBD (no Brasil) faz parte. As normas para vinho biodinâmico existem e são seguidas nos vinhos certificados.<br /><br />
			Os vinhos biodinâmicos são produzidos dentro de uma técnica e abordagem ampla que engloba inclusive os ritmos estelares porém esta prática não é obrigatória e não é o único e principal ponto diferencial e necessário para ter o vinho certificado mas sim:<br />
            - Cada fazenda é uma individualidade integrada;<br />
            - Práticas de conservação de solo;<br />
            - Não uso de fertilizantes químicos e de agrotóxicos sintéticos-somente produtos de controle natural;<br />
            - Praticas de conservação da natureza;<br />
            - Qualidade social dos trabalhos;<br />
            - Aplicação de preparados biodinâmicos homeopáticos que incrementam a vitalidade do ambiente, plantas e do produto final;<br />
            - Não uso de produtos transgênicos.<br /><br />
			Os vinhos biodinâmicos têm sido aclamados pelo seus aromas, bouquets e texturas. O não uso de agrotóxicos permite o surgimento de fungos naturais durante o cultivo e posterior fermentação que dão qualidade e aroma regionais únicos. Os preparados biodinâmicos conferem aos vinhos vitalidade, autenticidade e individualidade do terroir especiais. Vários são os vinhos premiados atualmente.
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/14.jpg" alt=""/>
    	<h4 class="tituloInterno">Insumo Aprovado IBD </h4>
        <p>
        	<span class="subtit">Sobre o Selo</span>
                O IBD Certificações criou o Programa de Aprovação de Insumos no intuito de avaliar a possibilidade de uso dos insumos comerciais disponíveis no mercado de acordo com as principais diretrizes de produção orgânica (Normas EUA, Européia, IFOAM, Japonesa, Canadense e Brasileira). Possui uma diretriz e procedimentos próprios e únicos a nível mundial que garantem segurança, credibilidade e confiabilidade aos insumos aprovados e aos produtores e empresas interessadas no uso.<br /> <br />               
                O Programa de Aprovação avalia os insumos de acordo com as normas de produção agrícola, processamento de alimentos e pecuária orgânica, sendo destinado a fabricantes, importadores e distribuidores de insumos localizados no Brasil e no exterior.        
            <span class="subtit">Categorias de Insumos</span>
                
                USO NA AGRICULTURA:<br />
                 Fertilizantes minerais e/ou orgânicos e condicionadores de solo;<br />
                 Defensivos para controle de pragas, doenças e plantas expontâneas;<br />
                 Insumos para uso na pós-colheita (Higienização e sanitização);<br />
                 Demais insumos para uso no manejo agrícola (limpeza sistema irrigação e equipamentos; armadilhas para insetos; entre outros) <br /><br />
                
                USO NA PECUÁRIA:<br />
                 Ingredientes para formulação de ração;<br />
                 Insumos para tratamento veterinário;<br />
                 Insumos para controle de ecto e endoparasitas;<br />
                 Insumos para sanitização de equipamentos e dos animais; <br /><br />
                
                USO EM PROCESSAMENTO DE ALIMENTOS:<br />
                 Ingredientes de origem agrícola não orgânicos;<br />
                 Ingredientes de origem não agrícola;<br />
                 Defensivos para controle de pragas;<br />
                 Insumos para sanitização de equipamentos e alimentos orgânicos.
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/15.jpg" alt=""/>
    	<h4 class="tituloInterno">Ingrediente Natural IBD - Cosméticos </h4>
        <p>
        	<span class="subtit">Sobre o Selo</span>
        		A certificação de produtos de limpeza naturais biodegradáveis é peça fundamental na orientação a ser dada ao consumidor sobre a sua escolha de consumo.
                
			<span class="subtit">Objetivos da Norma</span>
                - Estimular e favorecer o uso de produtos e processos, assim como embalagens com menor impacto ambiental possível, privilegiando o uso de matérias primas renováveis.<br />
                - Evitar que produtos alergênicos e irritantes cheguem ao consumidor.<br />
                - Promover a utilização de produtos certificados naturais, orgânicos, e extrativistas certificados.<br />
                - Promover e garantir produtos de limpeza sem petroquímicos.<br />
			<span class="subtit">Diretrizes Atendidas</span>
        		- Normas IBD 
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/15.jpg" alt=""/>
    	<h4 class="tituloInterno">Ingrediente Natural IBD - Produtos de Limpeza </h4>
        <p>
    	    A poluição ambiental causada pelos resíduos que não são rapidamente biodegradados inunda rios e oceanos. Os petroquímicos não são facilmente biodegradados e causam sérios danos ambientais, enquanto não são assimilados pela natureza. Ao contrário, produtos derivados das plantas são prontamente biodegradados e permitem rápida assimilação. As diretrizes e sua aplicação garantes isenção no uso de petroquímicos.
		<span class="subtit">Diretrizes Atendidas</span>
	        - Produtos de Limpeza.
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/16.jpg" alt=""/>
    	<h4 class="tituloInterno">Não OGM IBD </h4>
        <p>
        <span class="subtit">Sobre o Selo</span>
			Todos sabem que os transgênicos (Organismo Geneticamente Modificados-OGM) vieram para ficar. Ocorre que o setor orgânico e uma boa parte dos consumidores não querem consumir tais produtos.<br /><br />
			Para tanto foi criado o Programa de Certificação de Não OGMs IBD, para segregar no mercado os produtos que realmente não contém, ou contém dentro de limites pré-estabelecidos materias Não OGM.<br /><br />
			O setor de alimentos deveria estar rotulando os alimentos com OGMs (vide em anexo a legislação específica). Mas isto não está ocorrendo. Além disso os consumidores reclamam por mais transparência sobre efeito de consumo de OGMs em animais e humanos. Adicionamos texto sobre este tema também.
		<span class="subtit">Diretrizes Atendidas</span>
        	- Normas IBD de Produtos Não OGM 

        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/9.jpg" alt=""/>
    	<h4 class="tituloInterno">Fair Trade IBD</h4>
        <p>
        <span class="subtit">Sobre o Selo</span>
			Fair Trade IBD é o Programa Fairtrade (Comércio Justo) do IBD aplicável exclusivamente a produtos e processos certificados como orgânicos.<br /><br />

			A Certificação Fair Trade IBD se aplica a empresas, propriedades e grupos de produtores que visam desencadear um processo interno de desenvolvimento humano, social e ambiental fomentado por relações comerciais baseadas nos princípios do Comércio Justo. A partir de um diagnostico inicial que caracteriza a realidade socioambiental da organização, um Comitê Gestor, composto pelas partes interessadas no empreendimento, identifica e prioriza as principais demandas ambientais e sociais que interfere negativamente nesta realidade e a partir delas são traçados Planos de Ações visando a melhoria contínua nos aspectos socioambientais (Condições de Vida e de Trabalho, Conservação e Recuperação Ambiental).<br /><br />

            Os empreendimentos em certificação são auditados com base nos critérios:<br />
            - Críticos: relacionados a algumas Convenções e Acordos Internacionais;<br />
            - Mínimos: relacionados à legislação do país;<br />
            - Progresso: relacionados à promoção do desenvolvimento local<br />
            - Além daqueles já previstos nas leis nacionais. 

		<span class="subtit">Diretrizes atendidas</span>
	        - Fair Trade IBD
        </p>
        
        <div class="clear"></div>       
       
        
        <img src="../media_mobile/img/certificados/27.png" alt=""/>
    	<h4 class="tituloInterno">Roundtable on Sustainable Palm Oil (RSPO) </h4>
        <p>
        <span class="subtit">Sobre o Selo</span>

            O movimento RSPO (Roundtable on Sustainable Palm Oil) está crescendo rapidamente em todo o mundo (vide portólio de membros no site www.rspo.org), com adesão das maiores multinacionais consumidoras de óleo de palma a nível mundial. O IBD Certificações é a única certificadora da América Latina credenciada junto ao RSPO e portanto, qualificada para oferecer serviços de auditoria neste escopo de certificação. <br /><br />
            
            Às empresas produtoras da palma africana, processadoras de óleo de palma e derivados, bem como às comercializadoras destes produtos, o IBD oferece um pacote de certificação calcado nos Princípios e Critérios RSPO ou no Sistema RSPO de Certificação de Cadeia de Custódia.
            
        <span class="subtit">Princípios</span>
            
            - Responsabilidade Ambiental e Conservação dos Recursos Naturais da Biodiversidade;<br />
            - Comprometimento com a Transparência;<br />
            - Uso das Melhores práticas nas áreas agrícolas e Industriais;<br />
            - Cumprimento das Leis e Normas Aplicáveis;<br />
            - Desenvolvimento responsável de novas áreas de plantio;<br />
            - Comprometimento com a viabilidade Financeira e Econômica de longo prazo;<br />
            - Responsabilidade com os colaboradores, indivíduos e comunidades afetadas pelas plantações e usinas;<br />
            - Comprometimento com a melhoria contínua. 
            
        <span class="subtit">Diretrizes atendidas</span>
        	www.rspo.org
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/24.png" alt=""/>
    	<h4 class="tituloInterno">Rainforest Alliance Certified (RAS) </h4>
        <p>
        <span class="subtit">Sobre o Selo</span>

	        A norma para uso do selo Rainforest Alliance Certified foi desenvolvida pela Rede de Agricultura Sustentável composta por organizações internacionais conservacionistas e independentes e tem como objetivo definir práticas agrícolas corretas e que causem menos impacto à saúde dos consumidores, assim como estabelecer normas para gestão do meio ambiente e dos trabalhadores envolvidos com a atividade produtiva. 
        
        <span class="subtit">Diretrizes Atendidas</span>
        	- RAS http://www.rainforest-alliance.org/certification-verification
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/25.png" alt=""/>
    	<h4 class="tituloInterno">Verificação 4C </h4>
        <p>
        <span class="subtit">Sobre o Selo</span>
			A 4C Association é uma organização multilateral que se dedicada às questões de sustentabilidade do setor cafeeiro.<br /><br />
			A 4C Association trabalha para atingir 100% de conformidade do setor cafeeiro com padrões mínimos de sustentabilidade.

		<span class="subtit">Diretrizes Atendidas</span>
        - 4C http://www.4c-coffeeassociation.org/the-standard/verification-system
        </p>
        
        <div class="clear"></div>
        
        <h4 class="tituloInterno">Union for Ethical Bio Trade (UEBT)</h4>
        <p>
        <span class="subtit">Sobre a União para BioComércio Ético</span>
        	A União para BioComércio Ético (UEBT- na sigla em inglês) é uma associação sem fins lucrativos que promove o Abastecimento com Respeito dos ingredientes provenientes da biodiversidade. Os seus membros se comprometem a assegurar que suas práticas de abastecimento promovam gradualmente a conservação da biodiversidade, respeitem o conhecimento tradicional e garantam a distribuição equitativa de benefícios ao longo da cadeia produtiva. <br /><br />
	        Ser membro da UEBT implica o estabelecimento de um Sistema de Gestão de Biodiversidade que abrange todas as práticas de abastecimento de ingredientes naturais e não apenas determinadas cadeias de abastecimento. Um membro da UEBT pode usar seu logotipo para ilustrar que está trabalhando para alinhar as suas práticas com as de BioComércio Ético. No entanto, o logotipo se aplica à organização e a seus princípios de abastecimento, ao invés do cumprimento específico para determinados produtos. Nesse sentido, o logotipo foi projetado para ser usado pela área de comunicação corporativa da empresa e não no marketing de produto específico.
        <span class="subtit">Norma de BioComércio Ético</span>
        	http://ethicalbiotrade.org/
        </p>
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/26.png" alt=""/>
    	<h4 class="tituloInterno">UTZ</h4>
        <p>
        <span class="subtit">Sobre o Selo</span>
        	Se compra café, cacau ou chá da UTZ, contribui para construir um futuro melhor. Porque UTZ representa a agricultura sustentável com melhores perspectivas para os agricultores, suas famílias e nosso planeta. Graças ao programa UTZ, os agricultores aprendem melhores práticas agrícolas, criam melhores condições de trabalho e podem cuidar melhor de seus filhos(as) e da natureza.        
        <span class="subtit">Diretrizes Atendidas</span>
        	- UTZ (Código de Conduta e Cadeia de Custódia) https://www.utz.org/
        </p>   
        
        <div class="clear"></div>
        
        <img src="../media_mobile/img/certificados/28.png" alt="" />
    	<h4 class="tituloInterno">IBD Qualidade Certificada</h4>
        <p>
        <span class="subtit">Sobre o Selo</span>
        	
        A sociedade cobra cada vez mais coerência do setor produtivo para minimizar as contaminações do ambiente, do ser humano e de toda a cadeia produtiva.<br />
        Existem demandas por uma certificação de produtos aptos para o consumo humano. Há aumento na procura pelo consumidor e pelo produtor por produtos produzidos sem o uso de agrotóxicos mas com modernas técnicas de equilíbrio nutricional, com o uso de insumos naturais e controle biológico de pragas e doenças. <br />
        Neste sentido o IBD gerou estas normas que permitirão às empresas e produtores venderem seus produtos com destaque no mercado e atendendo a esta demanda do consumidor.<br />

        <span class="subtit">Diretrizes Atendidas</span>
        	- Qualidade Certificada IBD
        <span class="subtit">Segmentos atendidos</span>
        	- Todos os setores produtivos de alimentos.
        </p>
        <p>AS DIRETRIZES SOMENTE SERÃO REPASSADAS A UMA PESSOA SOMENTE APÓS PAGAMENTO DE R$ 50. FAVOR CONTATAR: <a href="mailto:ibd@ibd.com.br">ibd@ibd.com.br</a></p>
        

            
        
    </div><!--interna-->

</asp:Content>
