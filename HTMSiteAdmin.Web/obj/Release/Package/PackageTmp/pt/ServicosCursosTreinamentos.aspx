﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="ServicosCursosTreinamentos.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.ServicosCursosTreinamentos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>Servi&ccedil;os
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px; margin-left: 6px;" />
                Cursos e Treinamentos</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }

                .itemNormas {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }

                .limpaFloat {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/imagem_interna_04.jpg" alt="" style="float: right; margin-left: 15px; margin-bottom: 10px;" />
            <p>
                Capacitar os participantes a compreender os conceitos e requisitos das normas de produção orgânica para o mercado brasileiro, europeu e americano e dos controles operacionais essenciais a serem seguidos por estabelecimentos e serviços envolvidos ou se iniciarem estas formas de produção, com atenção especial aos produtos foco da região demandadora. A organização de grupo de produtores e funcionamento do sistema de controle interno também pode fazer parte do programa.
            <br />
            <br />
                Este curso faz parte do sistema de treinamento existente no IBD há anos e que é feito em varias partes do país, conforme demanda.
            </p>
        </p>
        <br />
        <br />
        <br />

        <p>
            <h2 class="tituloCertificaoInterno">Curso de Capacitação e Interpretação de Normas Orgânicas</h2>
            <br />
            <p style="color:#4f6f0a">- Público Alvo</p>
            <p>Empresas, consultores e produtores rurais, associações e cooperativas que buscam obter entendimento e segurança na produção de alimentos e melhor controle gerencial da produção agrícolas e profissionais que almejam a conquista e manutenção de certificação orgânica ou técnicos e serviços relativos a esta forma de produção</p>

            <br />

            Entre em contato para maiores informações ou acesse a agenda de Cursos. <a href="EventosFeiras.aspx?id_categoria=7">[clique aqui] </a>
            <br />
        </p>
    </div>
    <!--quemsomos-->
</asp:Content>
