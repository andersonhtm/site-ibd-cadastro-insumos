﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true" CodeBehind="ContratosDetalhes.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.ContratosDetalhes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Biblioteca
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Contratos</h2>
            <a href="default.aspx">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />

        <h2 class="tituloCertificaoInterno" style="font-size: 17px">
            <a href= "http://www.ibd.com.br/mala/Contrato_de_Prestação_de_Serviços_IBD.pdf">Contrato de Prestação de Serviços IBD - Clientes no Brasil (Reg. 021977)</a> 
        </h2>
        <h2 class="tituloCertificaoInterno" style="font-size: 17px">
            <a href= "http://www.ibd.com.br/mala/ContratoRegistrato_Reg022412.pdf">Contrato de Prestação de Serviços IBD - Clientes no Brasil (Reg. 022412)</a> 
        </h2>
        <h2 class="tituloCertificaoInterno" style="font-size: 17px">
            <a href= "http://ibd.com.br/ShowFile.aspx?action=2&fileid=e5f223b5-791c-462c-9794-51f062a1e032">Contrato de Prestação de Serviços IBD - Clientes no exterior (Foreign Operators Contract)</a> 
        </h2>
       
        <!--centro-->
    </div>
    <!--quemsomos-->
</asp:Content>