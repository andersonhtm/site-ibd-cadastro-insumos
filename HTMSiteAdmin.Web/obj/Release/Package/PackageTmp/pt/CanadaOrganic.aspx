﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="CanadaOrganic.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.CanadaOrganic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Orgânico
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Canada Organic (Equivalência EUA/NOP)</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <img src="media/img/logoCertificadoCanadaOrganic_.png" alt="" style="float: right;
            margin-left: 15px; margin-bottom: 10px;" />
        <h2 class="tituloCertificaoInterno">
            Sobre o Selo</h2>
        Clientes certificados NOP podem também ser certificados conforme os termos do acordo de equivalência EUA-Canada.
        <br />
        <br />
        Neste caso o uso dos dois selos USDA Organic e Canada Organic seria possível e estes produtos podem ser comercializados na EUA e no Canada.
        <br />
        <br />
        <h2 class="tituloCertificaoInterno">
            Diretrizes Atendidas</h2>
            <!-- <span class="itemNormas">Canada Organic</span> -->
            NOP e Termos específicos do acordo de equivalência:<br />
            <br />            
            - Não foi usado nitrato de sódio<br />
            - Ausência de hidropônica/aeroponia  <br />
            - No caso de animais (com exceção de ruminantes) conforme taxas de lotação Canadense<br />
        <br />
        <br />
        <h2 class="tituloCertificaoInterno">
            Segmentos Atendidos</h2>
        - Agricultura<br />
        - Pecuária<br />
        - Processamento<br />
        - Extrativismo<br />
        <br />
        <br />
        <br />
        <br />
    </div>
</asp:Content>
