﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="UsdaOrganic.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.UsdaOrganic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Orgânico
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                USDA Organic</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <img src="media/img/logoCertificadoUSDAOrganic.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                Sobre o Selo</h2>
            O selo USDA Organic &eacute; o selo norte americano acreditado pelo Departamento
            de Agricultura dos Estados Unidos (USDA).<br />
            <br />
            Este selo tem o objetivo de desenvolver, manter e expandir o acesso para produtos
            de pa&iacute;ses diversos ao mercado Americano.<br />
            <br />

            <h2 class="tituloCertificaoInterno">Passo a Passo</h2>

            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=07511992-13b3-4b3f-80a2-3d7cee456607">Passo a Passo para certificação (Português)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=93014008-7780-4669-90c7-e90ccffbab87">Passo a Passo para certificação (Inglês)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=787619fb-fc31-4eed-a1bf-404738c0ecb0">Passo a Passo para certificação (Espanhol)</a><br />
            <br /><br />

            <h2 class="tituloCertificaoInterno">Passo a Passo certificação Grupos de Produtores</h2>

            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=40b81a87-3a1c-4e7b-990f-4e0ca875c0e2">Passo a Passo para certificação (Português)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=b66b1167-f68e-4810-a581-53bbf9d4e7c6">Passo a Passo para certificação (Inglês)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=5ad12599-e08f-4656-a880-76c4304a73e2">Passo a Passo para certificação (Espanhol)</a><br />
            <br /><br />

            <h2 class="tituloCertificaoInterno">
                Diretrizes Atendidas</h2>
                <!-- <span class="itemNormas">NOP</span>-->
                - NOP 
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segmentos Atendidos</h2>
            - Agricultura<br />
            - Pecuária<br />
            - Fibras<br />
            - Processamento<br />
            - Extrativismo<br />
            - Cosméticos<br />
            <br />
            <!-- 
            <h2 class="tituloCertificaoInterno">
                Informa&ccedil;&otilde;es Adicionais</h2>
            <span class="itemNormas">NOP Extrato (Dez/06 - Português)</span> <span class="itemNormas">
                Lista Nacional (Set/06 - Portugês)</span> <span class="itemNormas">NOP (Dez/06 - Inglês)</span>
            <br />
            -->
        </p>
        <br />
        <br />
        <br />
    </div>
    <!--quemsomos-->
</asp:Content>
