﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="Uebt.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.Uebt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Socio-Ambiental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px;
                    margin-right: 6px; margin-left: 6px;" />
                UEBT</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoUebt.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">Sobre a União para BioComércio Ético</h2>
            A União para BioComércio Ético (UEBT- na sigla em inglês) é uma associação sem fins lucrativos que promove o “Abastecimento com Respeito” dos ingredientes provenientes da biodiversidade. Os seus membros se comprometem a assegurar que suas práticas de abastecimento promovam gradualmente a conservação da biodiversidade, respeitem o conhecimento tradicional e garantam a distribuição equitativa de benefícios ao longo da cadeia produtiva.
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">BioComércio Ético</h2>
            Ao ingressar na UEBT, a empresa se compromete a respeitar os princípios do BioComércio Ético. Isso
            significa usar práticas que promovam o uso sustentável dos ingredientes naturais, garantindo simultaneamente que todos os que fazem parte da cadeia de abastecimento, recebam uma remuneração justa e compartilhem os beneficios derivados do uso da biodiversidade. Ao adotar as práticas do BioComércio Ético da UEBT, as empresas promovem relacionamentos de longo prazo com suas comunidades abastecedoras, criando empregos, contribuindo para o desenvolvimento local e
            ajudando a preservar a biodiversidade local.
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">Por que ingressar?</h2>
            Os consumidores estão cada vez mais conscientes da biodiversidade. A prática ética de abastecimento
            da biodiversidade acabará se tornando um requisito fundamental para marcas preferidas e seus fornecedores. Adotar práticas éticas e sustentáveis permite que as empresas se diferenciem no mercado, tomando a liderança ao satisfazer a demanda dos consumidores por produtos éticos. Pertencer à UEBT, garante aos investidores e consumidores, que providências para assegurar o uso ético e sustentável da biodiversidade estão sendo tomadas. Essas práticas não só contribuem para um mundo melhor, mas também faz todo sentido ao negócio!
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">Logotipo da UEBT</h2>
            Ser membro da UEBT implica o estabelecimento de um Sistema de Gestão de Biodiversidade que abrange todas as práticas de abastecimento de ingredientes naturais e não apenas determinadas cadeias de abastecimento. Um membro da UEBT pode usar seu logotipo para ilustrar que está trabalhando para alinhar as suas práticas com as de BioComércio Ético. No entanto, o logotipo se aplica à organização e a seus princípios de abastecimento, ao invés do cumprimento específico para determinados produtos. Nesse sentido, o logotipo foi projetado para ser usado pela área de comunicação corporativa da empresa e não no marketing de produto específico.
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">Norma de BioComércio Ético</h2>
            Os 7 princípios de BioComercio da UEBT são:
            <br />
            <br />
            <ul>
                <li>
                    1-	Conservação da biodiversidade - Preservar os ecossistemas e promover práticas que conservem e restaurem a biodiversidade.
                </li>
                <li>
                    2-	Uso Sustentável - Práticas que garantam as funções de longo prazo dos ecossistemas e limitem os impactos ambientais negativos.
                </li>
                <li>
                    3-	Repartição justa e equitativa dos benefícios - O uso da biodiversidade e do conhecimento tradicional reconhece os direitos, promove o diálogo e permite remuneração justa.
                </li>
                <li>
                    4-	Sustentabilidade socioeconômica - Adoção de gestão da qualidade e práticas financeiras que sejam sustentáveis e socialmente aceitáveis
                </li>
                <li>
                    5-	Conformidade legal - Respeito a todas as normas internacionais, nacionais e locais pertinentes.
                </li>
                <li>
                    6-	Respeito pelos direitos dos atores - Levar em conta os direitos humanos, as condições de trabalho e os direitos das comunidades indígenas e locais.
                </li>
                <li>
                    7-	Clareza sobre a posse de terra - Respeitar os direitos sobre a terra e seus recursos naturais.
                </li>
            </ul>
            <br />
            <h2 class="tituloCertificaoInterno">Alguns Clientes</h2>
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=29" class="imagem">
                    <img src="media/img/logo_vegeflora.png" width="192" height="126" /></a> <a href="ClientesDetalhes.aspx?id_conteudo=29"
                        class="titulo">Vegeflora</a>
            </div>
            <br />
            <br />
    </div>
    <!--quemsomos-->
</asp:Content>
