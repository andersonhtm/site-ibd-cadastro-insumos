﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="Noticias.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.Noticias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>Not&iacute;cias</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="noticias">
        <div>
            <p>Nesta seção fique por dentro das notícias do IBD.</p>
        </div>
        <div>
            <asp:Panel ID="pnlpaginacaoTopo" runat="server">
                <div class="paginacao">
                    <span>
                        <asp:Literal ID="Literal1" Text="" runat="server" /></span>
                    <asp:Panel runat="server" ID="pnlPaginadorTopo">
                        <div class="nav">
                            <asp:Literal ID="ltrAnteriorTopo" Text="ltrAnterior" runat="server" />
                            <asp:Literal ID="ltrPaginasTopo" Text="ltrPaginas" runat="server" />
                            <asp:Literal ID="ltrProximaTopo" Text="ltrProxima" runat="server" />
                        </div>
                    </asp:Panel>
                </div>
            </asp:Panel>
        </div>

        <asp:DataList ID="dtNoticias" runat="server" RepeatLayout="Flow" ShowFooter="False"
            ShowHeader="False" OnItemDataBound="dtNoticias_ItemDataBound" DataKeyField="ID_CONTEUDO">
            <ItemTemplate>
                <div class="itemNoticias">
                    <div>
                        <asp:Literal ID="ltrLinkTopo" Text="ltrLinkTopo" runat="server" />
                    </div>
                    <div class="texto">
                        <div class="dataTipo">
                            <span>
                                <asp:Literal ID="ltrDia" Text="ltrDia" runat="server" />
                                |
                                <asp:Literal ID="ltrMes" Text="ltrMes" runat="server" />
                                |
                                <asp:Literal ID="ltrAno" Text="ltrAno" runat="server" /></span>
                            <h4>
                                <asp:Literal ID="ltrCategoria" Text="ltrCategoria" runat="server" /></h4>
                        </div>
                        <!--dataTipo-->
                        <h3><asp:Literal ID="ltrLink" Text="ltrLink" runat="server" /></h3>
                        <h2><%# Eval("SUB_TITULO") %></h2>
                        <i style="font-size:11px;"><%# (!string.IsNullOrEmpty(Eval("FONTE").ToString()) ? "Fonte: " + Eval("FONTE").ToString():""  ) %></i>
                    </div>
                    <!--texto-->
                </div>
            </ItemTemplate>
        </asp:DataList>
        <!--itemNoticias-->
        <asp:Panel ID="pnlPaginacao" runat="server">
            <div class="paginacao">
                <span>
                    <asp:Literal ID="ltrContagemPaginacao" Text="ltrContagemPaginacao" runat="server" /></span>
                <asp:Panel runat="server" ID="pnlPaginador">
                    <div class="nav">
                        <asp:Literal ID="ltrAnterior" Text="ltrAnterior" runat="server" />
                        <asp:Literal ID="ltrPaginas" Text="ltrPaginas" runat="server" />
                        <asp:Literal ID="ltrProxima" Text="ltrProxima" runat="server" />
                    </div>
                </asp:Panel>
            </div>
        </asp:Panel>
        <!--paginacao-->
    </div>
    <!--noticias-->
</asp:Content>
