﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="Demeter.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.Demeter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Biodin&acirc;mico
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Demeter</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <img src="media/img/logoCertificadoDemeter.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                Sobre o Selo</h2>
            Demeter é a marca que identifica, mundialmente, os produtos biodinâmicos. Os produtos Demeter fazem parte de uma rede ecológica internacional ligada ao Demeter International, sediado na Alemanha.<br /><br />
            Os produtos Demeter produzidos no país no sistema de certificação por auditoria, são certificados como Demeter pelo IBD Certificações com exclusividade.<br /><br />
            Para produtos Demeter a serem certificados no sistema participativo, a Associação Biodinâmica fará a certificação.<br /><br />
            Para todos os produtos importados, quais quer que sejam, para a certificação Demeter feita no país de origem, não haverá necessidade de re-certificação no Brasil, mas o importador deverá manter obrigatoriamente um contrato e licença de venda e operação com a marca Demeter para o território Brasileiro com o IBD certificações.<br /><br />
            Para os produtores e exportadores ao Brasil não será necessário contrato com o IBD Certificações específico para Demeter.<br /><br />
            Para a norma de produtos orgânicos Brasileira: todos os produtos Demeter certificados no exterior em seus países de origem normalmente são também certificados como orgânicos. Para tanto, estes produtos deverão passar por uma certificação para orgânico segundo a lei Brasileira. O IBD poderá executar este serviço ou orientar ao interessado em como faze-lo. <br /><br />

            Por favor entre em contato com <a href="mailto:ibd@ibd.com.br">ibd@ibd.com.br</a> ou pelo <a href="http://ibd.com.br/pt/FaleConosco.aspx">fale conosco</a> e envie-nos sua demanda de informações.
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Diretrizes Atendidas</h2>
                <!-- <span class="itemNormas">Demeter</span> -->
                - Demeter
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segmentos Atendidos</h2>
            - Agricultura<br />
            - Pecuária<br />
            - Processamento<br />
            - Cosméticos<br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Vinhos Biodin&acirc;micos</h2>
            <p>
                O que são vinhos biodinâmicos?<br />
                <br />
                A Biodinamica começou em 1924 com as palestras ministradas por Rudolf Steiner na Europa e hoje corresponde a um movimento em mais de 40 países e envolve mais de 4900 produtores. A biodinâmica foi precursora dos orgânicos e o selo de produtos biodinâmicos, o selo Demeter , foi lançado já na década de 50 do século passado.<br /><br />
                Há uma associação internacional e todos os dados estão acessíveis no site: www.demeter.net  da Demeter International do qual o IBD Certificações (no Brasil) faz parte. As normas para vinho biodinâmico existem e são seguidas para a produção de vinhos biodinâmicos certificados.<br /><br />
                Os vinhos biodinâmicos são produzidos dentro de uma técnica e abordagem ampla que engloba inclusive os ritmos estelares porém esta prática não é obrigatória e não é o único e principal ponto diferencial e necessário para ter o vinho certificado mas sim:<br /><br />
                - Cada fazenda é uma individualidade integrada;<br />
                - Práticas de conservação de solo;<br />
                - Não uso de fertilizantes químicos e de agrotóxicos sintéticos-somente produtos de controle natural;<br />
                - Praticas de conservação da natureza;<br />
                - Qualidade social dos trabalhos;<br />
                - Aplicação de preparados biodinâmicos homeopáticos que incrementam a vitalidade do ambiente, plantas e do produto final;<br />
                - Não uso de produtos transgênicos.<br /><br />

                Os vinhos biodinâmicos têm sido aclamados pelo seus aromas, bouquets e texturas. O não uso de agrotóxicos permite o surgimento de fungos naturais durante o cultivo e posterior fermentação que dão qualidade e aroma regionais únicos, características de cada “terroir” individual. Os preparados biodinâmicos conferem aos vinhos vitalidade, autenticidade, caráter e individualidade. Vários são os vinhos premiados atualmente.<br /><br />

                Os vinhos biodinâmicos aclamados internacionalmente entre outros são:  Vignoble de la Coulée de Serrant de Nicolas Jolly da França; Nikolaihof, da Austria; Champagne Fleury da França.<br /><br />

                Saudamos todos que tenham interesse em biodinâmica e estamos às ordens para quaisquer questões.<br />

            <br />

            <div style="border-bottom: solid 1px #dadbd7;"></div>

            <h2 class="tituloCertificaoInterno" style="background: none; padding-left: 0">Vinho orgânico tem má qualidade ou é melhor do que o convencional?  <span style="font-size: 15px; margin-top: -6px; display: block; color: #3a723d;">Le Monde Stéphane Foucart - 11/09/2016</span></h2>
               
                <br /><br />

            <img style="width: 719px; margin-bottom: 20px; display: block;" alt="O que pode existir em uma garrafa de vinho além de uva?" src="media/img/demeter-vinho.jpg" title="O que pode existir em uma garrafa de vinho além de uva?" />



Seria o vinho orgânico na maior parte das vezes um lamentável vinho de má qualidade? Ou seria o contrário, equivalente ou até mesmo melhor que o vinho convencional dependendo do parâmetro? Essa não é somente uma discussão de beberrões ou um papo de mesa; é também uma questão cuja resposta poderá ter consequências ambientais, sanitárias e industriais de primeira importância.<br /><br />

Em geral, cada um segue suas inclinações. Os mais preocupados com a preservação do meio ambiente tendem a apreciar o vinho orgânico (sob suas diversas certificações). Já os mais ecofóbicos costumam acreditar que as restrições da certificação (que dizem respeito ao cultivo da uva e os métodos de vinificação) necessariamente resultam em uma perda de qualidade do produto final.<br /><br />

Até hoje, quase nada conseguia dar objetividade ao debate e cada um dos lados podia se ater a suas opções ideológicas e a seus preconceitos, uma vez que a ignorância permite esse tipo de conforto.<br /><br />

Mas a ignorância vem diminuindo. Três economistas acabam de contribuir, na última edição do "The Journal of Wine Economics", com um primeiro elemento para uma resposta decisiva.<br /><br />

Magali Delmas e Jinghui Lim, da Universidade da Califórnia em Los Angeles (UCLA), e Olivier Gergaud, professor da Kedge Business School, em Bordeaux, compilaram 74.148 avaliações de vinhos californianos, publicadas entre 1998 e 2009 em três revistas ("Wine Spectator", "Wine Advocate" e "Wine Enthusiast") cujas avaliações resultaram de degustações às cegas.<br /><br />

Resultado: os vinhos orgânicos (segundo a certificação oficial californiana) ou biodinâmicos (segundo a certificação privada da Demeter) apresentam em média uma pontuação 4 pontos maior que a dos vinhos convencionais, em uma escala de 50 a 100 —50 sendo a pontuação de um vinho de péssima qualidade, e 90 a 100 para um vinho de safra excepcional.<br /><br />

<strong>Uma desconfiança arraigada</strong><br /><br />
Os autores, financiados pela UCLA, abordaram a questão com sua base de dados e controlando todo tipo de parâmetro para garantir que o efeito favorável medido seja de fato atribuível ao modo de produção orgânico do vinho e não às cepas utilizadas, ao produtor, à sua região de produção, à sua safra etc.<br /><br />

Seria isso tão surpreendente assim? Já se sabe com certeza que os pesticidas sintéticos utilizados na agricultura convencional reduzem a biodiversidade dos ecossistemas e afetam seriamente a vida microbiana dos solos. Trabalhos publicados em março de 2015 na revista "mBio" avaliaram o impacto sobre a vinha dessas miríades de bactérias subterrâneas e concluíram que estas tinham "o potencial de influenciar as propriedades organolépticas do vinho". Esse "potencial" agora foi confirmado empiricamente<br /><br />

Um implicante poderia alegar que esses resultados dizem respeito somente aos vinhos californianos, que seria verdade para um lado do Atlântico, mas não para o outro. "Nós conduzimos o mesmo experimento com vinhos franceses, a partir das avaliações da Gault&Millau", explica Olivier Gergaud. "E os resultados que obtivemos, ainda que preliminares, vão na mesma direção."<br /><br />

Se os resultados apresentados são importantes, é primeiramente porque a desconfiança em relação ao vinho orgânico está muito arraigada entre muitos enófilos. Tanto que trabalhos anteriores dos mesmos autores terem mostrado que quase dois terços dos viticultores californianos que adotaram as práticas da agricultura orgânica preferem não informar isso nas garrafas!<br /><br />

Evidentemente, essa desconfiança é um freio para a transição dos cultivos, e esse freio tem consequências ambientais e sanitárias potencialmente importantes. De fato, a videira é um dos cultivos que mais usam pesticidas. Na França, segundo números citados em 2013 pela perícia coletiva do Instituto Nacional da Saúde e da Pesquisa Médica (Inserm), a videira ocupa somente 3% da superfície agrícola útil, mas concentra cerca de 20% das quantidades de pesticidas utilizados no país.<br /><br />

Esses trabalhos fazem parte de uma "literatura emergente", para usar os termos de Olivier Gergaud. Do ponto de vista agronômico ou ambiental, os benefícios e os inconvenientes da agricultura orgânica foram objeto de inúmeras pesquisas nas últimas décadas. Mas seu interesse (ou sua falta de interesse) para o consumidor foi muito menos investigado. Em especial, os efeitos do consumo de produtos orgânicos sobre a saúde até hoje foram pouco pesquisados.<br /><br />

Assim como os trabalhos sobre as qualidades do vinho orgânico, publicações recentes tentam levar em conta as circunstâncias. Uma equipe norueguesa publicou recentemente na "Environmental Health Perspectives" trabalhos que sugerem, a partir de um grupo de mais de 35 mil mulheres grávidas, uma redução de 40% no risco de hipospádia (uma malformação do aparelho genital masculino) entre os recém-nascidos cuja mãe tenha consumido produtos orgânicos —cujos resíduos de pesticidas são inferiores aos dos produtos convencionais— ao longo de sua gestação. Isso ainda precisa ser confirmado por pesquisas mais amplas.<br /><br />

Além de ser plausivelmente melhor (pelo menos segundo um parâmetro) do que sua versão convencional, seria o vinho orgânico melhor (ou menos ruim) para a saúde? Essa questão ainda se encontra em aberto. Cada um ainda pode se guiar por suas inclinações. Alguns fãs de vinho orgânico garantem que um consumo excessivo de vinho convencional provavelmente leva a um estado de enxaqueca muito pior no dia seguinte do que um consumo igualmente excessivo de vinho orgânico. Verdade ou mentira? Seria um projeto de pesquisa divertido de se conduzir, e cujos resultados podem ser bem mais importantes do que parecem.<br /><br />


        </p>

    </div>
    <!--quemsomos-->
</asp:Content>
