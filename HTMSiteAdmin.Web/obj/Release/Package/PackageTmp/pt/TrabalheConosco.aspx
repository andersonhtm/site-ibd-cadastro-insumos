﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="TrabalheConosco.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.TrabalheConosco" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Trabalhe Conosco</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="painelCliente" class="trabalheConosco">
        <div>
            <p style="font-size: 15px;">
                Faça parte da equipe IBD mande seu currículo para:
                <br />
                <a href="mailto:ibd@ibd.com.br">ibd@ibd.com.br</a>
                <br />
                <br />
                Se preferir utilize o formulário a seguir:</p>
        </div>
        <div style="float: left; width: 370px;">
            <ul>
                <li>
                    <asp:Label ID="Label1" Text="nome" runat="server" AssociatedControlID="txtNome" />
                    <asp:TextBox ID="txtNome" runat="server" Style="float: left;" />
                    <asp:RequiredFieldValidator ID="rfvNome" ControlToValidate="txtNome" runat="server"
                        ErrorMessage="- Nome" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label2" Text="e-mail" runat="server" AssociatedControlID="txtEmail" />
                    <asp:TextBox ID="txtEmail" runat="server" Style="float: left;" />
                    <asp:RequiredFieldValidator ID="rfvEmail" ControlToValidate="txtEmail" runat="server"
                        ErrorMessage="- E-Mail" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="- E-mail inválido" Display="None" SetFocusOnError="true" ValidationGroup="vgForm"
                        ValidationExpression="\S+@\S+.\S{2,3}"></asp:RegularExpressionValidator>
                </li>
                <li>
                    <asp:Label ID="Label3" Text="telefone" runat="server" AssociatedControlID="txtCelular" />
                    <asp:TextBox ID="txtCelular" runat="server" Style="float: left;" />
                    <asp:RequiredFieldValidator ID="rfvCelular" ControlToValidate="txtCelular" runat="server"
                        ErrorMessage="- Telefone" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label7" Text="endereço" runat="server" AssociatedControlID="txtEndereco" />
                    <asp:TextBox ID="txtEndereco" runat="server" Style="float: left;" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtEndereco"
                        runat="server" ErrorMessage="- Endereço" Display="None" SetFocusOnError="True"
                        ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label5" Text="município" runat="server" AssociatedControlID="txtMunicipio" />
                    <asp:TextBox ID="txtMunicipio" runat="server" Style="float: left;" />
                    <asp:RequiredFieldValidator ID="rfvMunicipio" ControlToValidate="txtMunicipio" runat="server"
                        ErrorMessage="- Município" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label4" Text="estado" runat="server" AssociatedControlID="txtEstado" />
                    <asp:TextBox ID="txtEstado" runat="server" Style="float: left;" />
                    <asp:RequiredFieldValidator ID="rfvEstado" ControlToValidate="txtEstado" runat="server"
                        ErrorMessage="- Estado" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label6" Text="pais" runat="server" AssociatedControlID="txtPais" />
                    <asp:TextBox ID="txtPais" runat="server" Style="float: left;" />
                    <asp:RequiredFieldValidator ID="rfvPais" ControlToValidate="txtPais" runat="server"
                        ErrorMessage="- Pais" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label8" Text="comentários" runat="server" AssociatedControlID="txtComentarios" />
                    <asp:TextBox ID="txtComentarios" runat="server" Style="float: left;" Rows="10" TextMode="MultiLine" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtComentarios"
                        runat="server" ErrorMessage="- Comentários" Display="None" SetFocusOnError="True"
                        ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label9" Text="arquivo" runat="server" AssociatedControlID="afuAnexo" />
                </li>
                <li>
                    <ajaxToolkit:AsyncFileUpload ID="afuAnexo" runat="server" Style="display: inline-block;
                        float: right;" OnUploadedComplete="afuAnexo_UploadedComplete" 
                        OnClientUploadStarted="uploadStarted"
                        OnClientUploadComplete="uploadComplete" />
                </li>
                <li>
                    <label>
                        &nbsp;</label>
                    <asp:LinkButton Style="float: left;" ID="btnCadastrar" runat="server" OnClick="btnCadastrar_Click"
                        ValidationGroup="vgForm" CssClass="cadastrar btnCadastrar">cadastrar</asp:LinkButton>
                </li>
            </ul>
            <asp:ValidationSummary ID="vsForm" runat="server" ValidationGroup="vgForm" CssClass="erroLogin"
                HeaderText="Os seguintes campos contêm erros ou são obrigatórios:" />
        </div>
        <div style="float: right; margin-top: 0px;">
            <img src="media/img/fotoTrabalheConosc.jpg" alt="" width="266" height="200" /></div>
    </div>

    <script language="javascript" type="text/javascript">
        function CheckExtension(fileName) {
            var ext = fileName.substring(fileName.lastIndexOf(".") + 1).toUpperCase();
            return !(ext != 'TXT' && ext != 'DOC' && ext != 'DOCX' && ext != 'PDF')
        }

        function uploadStarted(sender, args) {
            var fileName = args.get_fileName();

            if (!CheckExtension(fileName))
                alert('Apenas arquivos com extensões dos tipos TXT, DOC, DOCX e PDF são permitidos.');

            $(".btnCadastrar").attr("disabled", "disabled").hide();
        }

        function uploadComplete(sender, args) {
            var fileName = args.get_fileName();

            if (CheckExtension(fileName))
                $(".btnCadastrar").removeAttr('disabled').show();
        }

    </script>
    <!--painelCliente-->
</asp:Content>
