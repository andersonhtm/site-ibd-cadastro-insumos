﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="QualidadeCertificada.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.QualidadeCertificada" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Orgânico
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                IBD Qualidade Certificada</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        
            <a href="#">
                <img src="../Media/img/logo_ibd_qualidade_certificada.png" alt="IBD Qualidade Certificada" style="float: right; margin-left: 15px; margin-bottom: 10px;" />
            </a>

            <h2 class="tituloCertificaoInterno">Sobre o Selo</h2>

            Consumidores exigem cada vez mais que os produtos sejam produzidos, distribuídos e descartados de forma compatível com os valores expressos nos conceitos do desenvolvimento sustentável e negócio justo e que sejam saudáveis. <br /><br />
            O IBD é uma entidade reconhecida internacionalmente como tendo o vínculo histórico com a verificação independente sobre a adequação dos produtos, processos e serviços quanto ao negócio justo e desenvolvimento sustentável. <br /><br />
            Visando apoiar organizações que se pautam pelos princípios da qualidade assegurada e que necessitam demonstrar que cumprem com seus próprios requisitos relativos ao desenvolvimento sustentável e negócio justo, o IBD instituiu o selo “Qualidade Certificada”, que atesta de forma independente e continuada que a organização cumpre com seus próprios critérios, em conformidade com os conceitos do “ambientalmente correto, socialmente justo e economicamente adequado e viável”. Esta Diretriz indica como o IBD monitora os programas das organizações que utilizam o selo por meio de auditorias e certificação.<br /><br />

            O selo visa demonstrar para a sociedade que o produto, processo ou serviço atendem aos padrões normativos e/ou regulamentos técnicos estabelecidos pela própria empresa, que decidiu produzir de forma consciente e de acordo com os modernos conceitos de respeito à natureza e ao ser humano, aprovados de acordo com os requisitos mínimos prescritos pelo selo IBD “Qualidade Certificada”.<br /><br />

        Aplicações para este selo:<br />
        - normas e protocolos próprios;<br />
        - auditorias de segunda parte (dentro do mesmo grupo- empresa operadora)<br />
        - programas de monitoramento como por exemplo, glúten, OGM, qualquer indicador necessário para o operador.





            <%--<h2 class="tituloCertificaoInterno"> Diretrizes Atendidas</h2> 
                - Qualidade Certificada IBD
            <br />
            <br />--%>
            <h2 class="tituloCertificaoInterno">
                Segmentos Atendidos</h2>
            - Todos os setores produtivos de alimentos.<br />
            <br />
            <%--<p>AS DIRETRIZES SOMENTE SERÃO REPASSADAS A UMA PESSOA SOMENTE APÓS PAGAMENTO DE R$ 50. FAVOR CONTATAR: <a href="mailto:ibd@ibd.com.br">ibd@ibd.com.br</a></p>--%>
        
    </div>            
</asp:Content>
