﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="InsumosClientesAprovados.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.InsumosClientesAprovados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Clientes</span><br />
                Insumos e Clientes Aprovados
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Consultar</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="certificadosConsultar">
        <div class="texto">
            <p>
                Pesquise as empresas e os produtos certificados pelo IBD. Para facilitar sua busca
                escolha os critérios que deseja:</p>
            <fieldset>
                <ul>
                    <li>
                        <label>
                            por esquema</label>
                        <asp:DropDownList runat="server" ID="ddlCertificado" DataTextField="NOME" DataValueField="ID_CERTIFICADO"
                            OnDataBound="ddlCertificado_DataBound" Width="250px" Style="float: left;" />
                    </li>
                    <li>
                        <label>
                            por cliente</label>
                        <asp:TextBox runat="server" ID="txtClienteCertificadoCliente" Width="250px" Style="float: left;" />
                    </li>
                    <li>
                        <label>
                            por insumo</label>
                        <asp:TextBox runat="server" ID="txtInsumo" Width="250px" Style="float: left;" />
                    </li>
                    
                    <li>
                        <label>por categoria</label>
                        <asp:DropDownList ID="ddlCategoria" DataTextField="DESCRICAO" DataValueField="ID_CATEGORIAPRODUTO" runat="server" Width="250px" Style="float: left;" />
                    </li>
                    <li>
                        <label>por finalidade</label>
                        <asp:DropDownList ID="ddlFinalidade" DataTextField="DESCRICAO" DataValueField="ID_FINALIDADE" runat="server" Width="250px" Style="float: left;" />
                    </li>

                    <li>
                        <asp:LinkButton ID="btnClienteCertificadoBuscar" runat="server" CssClass="btBuscar"
                            OnClick="btnClienteCertificadoBuscar_Click">buscar</asp:LinkButton></li>
                </ul>
            </fieldset>
        </div>
        <!--texto-->
        <div class="imagem">
            <img src="media/img/produtosCertificados.jpg" class="border3pxfff" alt="" /></div>
        <!--imagem-->
    </div>
    <!--certificadosConsultar-->
</asp:Content>
