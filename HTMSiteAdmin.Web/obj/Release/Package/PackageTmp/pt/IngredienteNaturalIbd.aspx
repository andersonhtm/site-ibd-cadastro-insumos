﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="IngredienteNaturalIbd.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.IngredienteNaturalIbd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifica&ccedil;&otilde;es e Aprova&ccedil;&otilde;es</span><br />
                Diversas
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Ingredientes Naturais IBD</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <img src="media/img/logoCertificadoIngredienteNatural.png" alt="" style="float: right;
                margin-left: 15px; margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                Sobre o Selo</h2>
            O impacto sobre o meio ambiente da nossa sociedade industrial vai depender das escolhas
            que fazemos constantemente a cada dia, quanto aos produtos de consumo que trazemos
            para dentro de nossas casas e fábricas. As melhores escolhas levarão a um alto nível
            de sustentabilidade ambiental.<br />
            <br />
            A escolha por produtos corretos não é simples e demanda por parte do consumidor,
            informação e consciência. A transparência dos processos de fabricação industrial
            é hoje tema importante e atual. A certificação de produtos de limpeza naturais biodegradáveis
            será peça fundamental na orientação a ser dada ao consumidor sobre a sua escolha
            de consumo.<br />
            <br />
            Objetivos da Norma:<br />
            <br />
             Estimular e favorecer o uso de produtos e processos, assim como embalagens com
            menor impacto ambiental possível, privilegiando o uso de matérias primas renováveis.<br />
             Evitar que produtos alergênicos e irritantes cheguem ao consumidor.<br />
             Promover a utilização de produtos certificados naturais, orgânicos, e extrativistas
            certificados.<br />
             Promover e garantir produtos de limpeza sem petroquímicos.
            <br />
            <br />
             <h2 class="tituloCertificaoInterno">Passo a Passo</h2>

            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=016e8327-fa0c-4028-acf7-811c35991a8a"> - Passo a Passo para Certificação de Ingredientes Naturais (Português)</a>
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=263a6411-a7ad-4d25-99bc-99f6111acfda"> - Passo a Passo para Certificação de Ingredientes Naturais (Inglês)</a>
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Diretrizes Atendidas</h2>
                <!-- <span class="itemNormas">Normas IBD</span> -->
                - Normas IBD
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segmentos Atendidos</h2>
            <p>
                - Cosméticos<br />
                - Produtos de Limpeza<br />
            </p>
            <br />
            <h2 class="tituloCertificaoInterno">
                Mais informa&ccedil;&otilde;es: Produtos de Limpeza</h2>
            <p>
                A questão dos produtos de limpeza é essencialmente ambiental e caminha em duas direções.
                <br />
                <br />
                Ao mesmo tempo em que a humanidade luta para conseguir a redução na emissão de carbono
                fóssil, ela o usa à vontade na fabricação de produtos de limpeza. É importante,
                portanto, usar matérias-primas de origem vegetal, renováveis e independentes do
                petróleo.<br />
                <br />
                A poluição ambiental causada pelos resíduos que não são rapidamente biodegradados
                inunda rios e oceanos. Os petroquímicos não são facilmente biodegradados e causam
                sérios danos ambientais, enquanto não são assimilados pela natureza. Ao contrário,
                produtos derivados das plantas são prontamente biodegradados e permitem rápida assimilação.
                <br />
                <br />
                Veja a seguir as Diretrizes IBD para Produtos de Limpeza Orgânicos e Naturais. É
                uma vitória, considerando-se as dificuldades de pesquisa do setor nos dias de hoje.
                Para o IBD, os avanços nesta área serão mais lentos, porém viáveis. Em primeiro
                lugar, devemos eliminar o uso de petroquímicos. Depois, usar insumos e matérias-primas
                certificadas orgânicas ou naturais.
            </p>
            <br />
            <a href="../ShowFile.aspx?action=1&fileid=3d42a9f2-2aeb-49af-abb2-7c86acfe855c"
                class="itemNormas" target="_blank">Diretrizes IBD para Produtos de Limpeza</a>

            <br />
            <br />
        </p>
    </div>
    <!--quemsomos-->
</asp:Content>
