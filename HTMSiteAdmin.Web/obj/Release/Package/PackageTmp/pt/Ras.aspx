﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="Ras.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.Ras" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Socio-Ambiental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px;
                    margin-right: 6px; margin-left: 6px;" />
                Rainforest Alliance (RFA)</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
       
            <img src="media/img/logoCertificadoRAS.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                Sobre o Selo</h2>
           A norma para uso do selo Rainforest Alliance Certified™ foi desenvolvida pela Rede de 
           Agricultura Sustentável composta por organizações internacionais conservacionistas e 
           independentes e tem como objetivo definir práticas agrícolas corretas e que causem menos 
           impacto à saúde dos consumidores, assim como estabelecer normas para gestão do meio 
           ambiente e dos trabalhadores envolvidos com a atividade produtiva.
            <br />
            <br />   
        
            <h2 class="tituloCertificaoInterno">Passo a Passo</h2>
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=a8bda9a3-b657-45dd-b0c7-400c9371a1f5"> - Passo a Passo para certificação (Português)</a>
            <br /><br />        
            
            <h2 class="tituloCertificaoInterno">
                Diretrizes Atendidas</h2>            
               - Rainforest Alliance (RFA)            
            <br />
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segmentos Atendidos</h2>            
                - Agricultura<br />
                - Pecuária<br />
                - Cadeia de custódia   
          
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Clientes Certificados
            </h2>

            <asp:Repeater ID="rpt" runat="server">
                <HeaderTemplate>
                    <ul class="list">
                </HeaderTemplate>
                <ItemTemplate>                    
                    <li><a href="<%# Eval("Caminho") %>" class="imagem"><%# Eval("Descricao")%></a></li>                    
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>

    </div>
    <!--quemsomos-->
</asp:Content>
