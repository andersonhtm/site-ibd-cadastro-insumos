﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.Default" %>

<%@ Register Src="UserControls/ucTopMenu.ascx" TagName="ucTopMenu" TagPrefix="uc1" %>
<%@ Register Src="UserControls/ucUltimasNoticias.ascx" TagName="ucUltimasNoticias"
    TagPrefix="uc2" %>
<%@ Register Src="UserControls/ucChamadaEventos.ascx" TagName="ucChamadaEventos"
    TagPrefix="uc3" %>
<%@ Register Src="UserControls/ucChamadaDownloads.ascx" TagName="ucChamadaDownloads"
    TagPrefix="uc4" %>
<%@ Register Src="UserControls/ucChamadaClientes.ascx" TagName="ucChamadaClientes"
    TagPrefix="uc5" %>
<%@ Register Src="UserControls/ucClientesCertificados.ascx" TagName="ucClientesCertificados"
    TagPrefix="uc6" %>
<%@ Register Src="UserControls/ucInsumosAprovados.ascx" TagName="ucInsumosAprovados"
    TagPrefix="uc7" %>
<%@ Register Src="~/pt/UserControls/ucSelosRodape.ascx" TagPrefix="uc1" TagName="ucSelosRodape" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="classification" content="" />
    <meta name="Description" content="" />
    <meta name="Keywords" content="" />
    <meta name="robots" content="ALL" />
    <meta name="distribution" content="Global" />
    <meta name="rating" content="General" />
    <meta name="Author" content="" />
    <meta name="doc-class" content="Completed" />
    <meta name="doc-rights" content="Public" />
    <link href="media/css/estilos.css" rel="stylesheet" type="text/css" />
    <link href="../Media/css/common.css" rel="stylesheet" type="text/css" />  
    <link rel="shortcut icon" type="image/x-icon" href="media/img/favicon.ico" />
    <link href="media/css/nivo.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="media/js/jquery.js"></script>
    <script type="text/javascript" src="media/js/jquery.nivo.js"></script>
    <script type="text/javascript" src="media/js/jquery.innerfade.js"></script>
    <script type="text/javascript" src="media/js/jquery.easing.1.2.js"></script>
    <script type="text/javascript" src="media/js/util.js"></script>
    <script type="text/javascript" src="media/js/index.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $(".itemNews a:odd").css("color", "#797949");
            $(".itemNews a:odd").hover(function () { $(this).css("color", "#809e25"); }, function () { $(this).css("color", "#797949"); });

        });

    </script>
    <style type="text/css">
        form a.cadastrar
        {
            width: 73px;
            height: 21px;
            color: #fbfff0;
            background: url(media/img/btCadastrar.jpg) no-repeat;
            display: block;
            font: normal 12px Georgia, "Times New Roman" , Times, serif;
            text-align: center;
            line-height: 21px;
            float: right;
            margin-right: 81px;
            margin-top: 15px;
        }
    </style>    
    <title>IBD Certifica&ccedil;&otilde;es</title>
</head>
<body>
    <form runat="server" id="form1">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <div class="wrap">
        <div class="centro" style="overflow: visible;">
            <div class="topo">
                <div class="logo">
                    <a href="/">
                        <img src="media/img/logo.png" alt="IBD Certificações" title="IBD Certificações" width="180"
                            height="79" /></a></div>
                <!--logo-->
                <h1>
                    Inspe&ccedil;&otilde;es e Certifica&ccedil;&otilde;es Agropecu&aacute;rias e Aliment&iacute;cias<span>
                        &uacute;nica certificadora 100% brasileira com atua&ccedil;&atilde;o internacional</span></h1>
                <div class="idiomas">
                    <a href="../en/Default.aspx" class="en" title="This site in english"></a>
                </div>
                <!--idiomas-->
                <uc1:ucTopMenu ID="ucTopMenu1" runat="server" />
            </div>
            <!--topo-->
        </div>
        <!--centro-->
        <div class="centro">
            <div class="bloco">
                <div class="banner">
                    <div id="slider">
                        <asp:Literal ID="ltrLinkImagemBanner1" Text="ltrLinkImagemBanner1" runat="server" />
                        <asp:Literal ID="ltrLinkImagemBanner2" Text="ltrLinkImagemBanner2" runat="server" />
                        <asp:Literal ID="ltrLinkImagemBanner3" Text="ltrLinkImagemBanner3" runat="server" />
                    </div>
                </div>
                <div class="opcoesSlider">
                    <div class="itemSlider">
                        <asp:Literal ID="ltrMiniaturaBanner1" Text="ltrMiniaturaBanner1" runat="server" />
                        <h2>
                            <asp:Literal ID="ltrTituloBanner1" Text="ltrTituloBanner1" runat="server" /></h2>
                        <p>
                            <asp:Literal ID="ltrSubTituloBanner1" Text="ltrSubTituloBanner1" runat="server" /></p>
                    </div>
                    <!--itemSlider-->
                    <div class="itemSlider">
                        <asp:Literal ID="ltrMiniaturaBanner2" Text="ltrMiniaturaBanner2" runat="server" />
                        <h2>
                            <asp:Literal ID="ltrTituloBanner2" Text="ltrTituloBanner2" runat="server" /></h2>
                        <p>
                            <asp:Literal ID="ltrSubTituloBanner2" Text="ltrSubTituloBanner2" runat="server" /></p>
                    </div>
                    <!--itemSlider-->
                    <div class="itemSlider">
                        <asp:Literal ID="ltrMiniaturaBanner3" Text="ltrMiniaturaBanner3" runat="server" />
                        <h2>
                            <asp:Literal ID="ltrTituloBanner3" Text="ltrTituloBanner3" runat="server" /></h2>
                        <p>
                            <asp:Literal ID="ltrSubTituloBanner3" Text="ltrSubTituloBanner3" runat="server" /></p>
                    </div>
                    <!--itemSlider-->
                </div>
                <!--opcoesSlider-->
            </div>
            <!--bloco-->
            <div class="bloco" style="padding-top: 10px; vertical-align: top">

                <table class="table-home">
                    <tr>
                        <td rowspan="2">
                            <div class="box" id="noticias">
                                <h3>
                                    Últimas Notícias</h3>
                                <uc2:ucUltimasNoticias ID="ucUltimasNoticias1" runat="server" />
                                <a href="Noticias.aspx" class="mais">mais notícias</a>
                            </div>
                        </td>
                        <td>
                            <!--#noticias.box-->
                            <div class="box" id="eventos">
                                <h3>Eventos</h3>
                                <uc3:ucChamadaEventos ID="ucChamadaEventos1" runat="server" />
                                <a href="EventosFeiras.aspx" class="mais">mais eventos</a>
                            </div>
                        </td>
                        <td>
                             <!--#eventos.box-->
                            <div class="box" id="quemSomos">
                                <h3>
                                    Quem Somos</h3>
                                <p>
                                    IBD Certificações - é uma empresa 100% brasileira que desenvolve atividades de inspeção
                                    e certificação agropecuária, de processamento e de produtos extrativistas, orgânicos,
                                    biodinâmicos e de mercado justo (Fair Trade).</p>
                                <a href="QuemSomos.aspx" class="mais2">saiba mais</a>
                            </div>
                        </td>
                    </tr>
                    <tr>                        
                        <td>
                            <div id="diretrizes" class="box">
                                <h3>
                                    Diretrizes e Legislações</h3>
                                <uc4:ucChamadaDownloads ID="ucChamadaDownloads1" runat="server" />
                                <div class="mais">
                                    <a href="DiretrizesLegislacao.aspx">mais documentos</a>
                                </div>
                                <!--mais-->
                            </div>
                            <!--box-->
                        </td>
                        <td>
                             <!--box-->
                            <div class="box" id="certificados">
                                <uc6:ucClientesCertificados ID="ucClientesCertificados1" runat="server" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="box" id="cadastre">
                                <h3>Cadastre-se</h3>
                                <fieldset>
                                    <p>Cadastre-se para receber as novidades e informativos do IBD Certificações.</p>
                                    <br />
                                    <p style="display: block; width: 100%;">
                                        <label style="display: inline-block; width: 50px; text-align: left;">
                                            Nome</label>
                                        <asp:TextBox runat="server" ID="txtCadastreSeNome" Width="300px" Style="float: left;" />
                                        <asp:RequiredFieldValidator ID="rfvCadastreSeNome" ControlToValidate="txtCadastreSeNome"
                                            runat="server" ErrorMessage="- Informe o Nome" Display="None" SetFocusOnError="True"
                                            ValidationGroup="vgFormCadastreSe"></asp:RequiredFieldValidator>
                                    </p>
                                    <p style="display: block; padding-top: 10px; width: 100%;">
                                        <label style="display: inline-block; width: 50px; text-align: left;">
                                            E-mail</label>
                                        <asp:TextBox runat="server" ID="txtCadastreSeEmail" Width="300px" Style="float: left;" />
                                        <asp:RequiredFieldValidator ID="rfvCadastreSeEmail" ControlToValidate="txtCadastreSeEmail"
                                            runat="server" ErrorMessage="- Informe o E-Mail" Display="None" SetFocusOnError="True"
                                            ValidationGroup="vgFormCadastreSe"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revCadastreSeEmail" runat="server" ControlToValidate="txtCadastreSeEmail"
                                            ErrorMessage="- E-mail inválido" Display="None" SetFocusOnError="true" ValidationGroup="vgFormCadastreSe"
                                            ValidationExpression="\S+@\S+.\S{2,3}"></asp:RegularExpressionValidator>
                                    </p>
                                    <p style="width: 100%; text-align: right;">
                                        <asp:LinkButton Text="cadastrar" CssClass="cadastrar" runat="server" ID="btnCadastrar"
                                            OnClick="btnCadastrar_Click" Style="margin-right: 0px; float: right;" ValidationGroup="vgFormCadastreSe" /></p>
                                </fieldset>
                                <asp:ValidationSummary ID="vsFormCadastreSe" runat="server" ValidationGroup="vgFormCadastreSe"
                                    CssClass="erroLogin" />
                            </div>
                        </td>
                        <td>
                            <!--#cadastre.box-->
                            <div class="box" id="painelCliente">
                                <asp:Literal ID="ltrBannerInferior4" Text="ltrBannerInferior4" runat="server" /><br />
                                <div style="height: 85px; width: 142px; float: left;">
                                    <asp:Literal ID="ltrBannerInferior5" Text="ltrBannerInferior5" runat="server" />
                                </div>
                                <div style="height: 85px; width: 142px; float: right;">
                                    <asp:Literal ID="ltrBannerInferior6" Text="ltrBannerInferior6" runat="server" />
                                </div>
                            </div>
                        </td>
                        <td>
                            <!--#painelCliente.box-->
                            <div class="box" id="insumos">
                                <uc7:ucInsumosAprovados ID="ucInsumosAprovados1" runat="server" />
                            </div>
                        </td>
                    </tr>
                </table>

                
                
               
                <!--quemSomos-->
                
                
               
               
               
                
                
                
                <!--#insumos.box-->
            </div>
            <!--bloco-->
        </div>
        <!--centro-->
    </div>
    <!--wrap-->
    <div class="wrap3">
        <uc1:ucSelosRodape runat="server" id="ucSelosRodape" />
    </div>
    <!--wrap3-->
    <div class="rodape">
        <div class="pattern">
            <div class="centro">
                <img src="media/img/arvore.png" alt="" />
                <div class="rodapeInterno">
                    <div class="menu">
                        <a href="QuemSomos.aspx">Quem Somos</a> <a href="ServicosCertificacoes.aspx">Servi&#231;os</a>
                        <a href="IbdOrganico.aspx">Certifica&#231;&#245;es</a> <a href="Acreditadores.aspx">
                            Acreditadores</a> <a href="CadastreSe.aspx">Clientes</a> <a href="Noticias.aspx">Biblioteca</a>
                        <a href="CadastreSe.aspx">Atendimento</a>
                    </div>
                    <div class="dads">
                        <address>
                            <em>Endereço</em>: Rua Amando de Barros, 2275 - Centro - CEP: 18.602.150 – Botucatu - SP
                        </address>
                        <p>
                            <em>(14)</em> 3811-9800</p>
                    </div>
                </div>
                <!---->
                <div class="creditos">
                    <span>© Copyright - A publicação ou uso dos textos e imagens contidas nesse site só
                        serão permitidas com a autorização devida do IBD</span><a target="_blank" href="http://www.htm.com.br"></a></div>
                <!--creditos-->
            </div>
            <!--centro-->
        </div>
        <!--pattern-->
    </div>
    <!--rodape-->
    </form>
    <script type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-44692999-12', 'ibd.com.br');
        ga('send', 'pageview');
    </script>
</body>
</html>
