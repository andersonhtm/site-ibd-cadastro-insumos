﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="ServicosMapeamentoGeoprocessamentoDetalhes.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.ServicosMapeamentoGeoprocessamentoDetalhes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Servi&ccedil;os
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Mapeamento e Geoprocessamento</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos" class="texto-justificado">
        <br />        
        <style>
            .tituloCertificaoInterno
            {
                color: #2D312E;
                font-size: 20px;
                margin: 15px 0 10px;
                width: 100%;
            }
            .itemNormas
            {
                display: block;
                background: #fffcec;
                padding: 6px 10px 6px 10px;
                margin: 0px 8px 10px 0px;
                float: left;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
            }
            .limpaFloat
            {
                clear: both;
                height: 10px;
            }
        </style>

            <h2 class="tituloCertificaoInterno">Imagem de Satélite Orbital</h2>
            
            <p>É um produto gerado a partir de uma imagem de satélite em que podem ser extraídas informações de uma área específica. Com os satélites orbitais é possível obter imagens de grandes extensões da superfície terrestre de forma repetitiva e a um custo relativamente baixo. As imagens têm como finalidade representar os aspectos físicos naturais ou artificiais de uma região o que permite diversas avaliações com precisão e em detalhes. Quanto melhor for a resolução da imagem utilizada (satélites de alta precisão) melhor será o detalhamento das informações que podem ser geradas para a propriedade. As imagens orbitais possuem também um acervo histórico, que torna possível buscar uma imagem de uma data anterior específica. </p>
                
            <div style="display: flex; flex-direction: column; align-items: center">
                <img src="../Media/img/mapa-geo-detalhes-1.png" alt="" style="margin: 15px auto;  text-align: center" />
                <img src="../Media/img/seta.png" alt="" style="transform: rotate(90deg); max-height: 28px" />
                <img src="../Media/img/mapa-geo-detalhes-2.png" alt="" style="margin: 15px auto;  text-align: center" />
            </div>    

        <p>
            <b>Produto:</b> Imagem de Satélite da propriedade em estudo e do entorno, georeferenciada e em composição (RGB) de cor verdadeira e/ou falsa-cor, dependendo da necessidade. As imagens são base do mapeamento, a partir dela outros produtos podem ser gerados. 
        </p>
        <br />

        <b>Opções de imagens e detalhamento da resolução:</b>
        <br />
        •	Sentinel 2 – resolução de 10m ou 20m;<br />
        •	Landsat 8 – resolução de 15m ou 30m;<br />
        •	Cbers 4 – resolução 10m ou 20m;<br />
        •	Imagens de acervo (a partir da data do lançamento do satélite): LandSat 1 a 7 – Resolução de 30m. Cbers 2/2b – resolução 20m e Aster ou Srtm – resolução de 15 a 90m;<br />
        •	Satélites de alta resolução: QuickBird, Geoeye, Spot 6/7, entre outros – mediante compra de fornecedor especializado.<br />

        <h2 class="tituloCertificaoInterno">Mapas de feição do relevo e uso do solo</h2>

        <img src="../Media/img/mapa-geo-detalhes-3.png" alt="" style="margin: 15px 15px 15px 0;  text-align: center; float: left" />

        <p>A partir de um levantamento de topográfico, que tem por finalidade descrever espacialmente uma paisagem, com suas áreas, dimensões e formatos. No momento em que esses dados são espacializados é possível gerar uma variedade de informações importantes para o diagnóstico e planejamento de uma propriedade e é nesse momento que os serviços de mapeamento IBD são capazes de transformar um levantamento topográfico em diversos mapas que trazem uma variedade de informações.
Se a propriedade não possuir um levantamento topográfico preciso e detalhado existem algumas imagens de satélite orbitais que podem fornecer perfis topográficos e hidrográficos, atendendo os parâmetros requeridos:</p>
        <br />
        <p>1 - <u>SRTM</u>: com resolução espacial de 90m, em escala de mapeamento de 1:100.000, pode gerar curvas de nível com equidistância de 50 metros;</p>
        <p>2 -	<u>TopoData</u>: com resolução espacial de 30m, em escala de mapeamento de 1:50.000, pode gerar curvas de nível com equidistância de 20 metros;</p>
        <p>3-	<u>Aster Gdem</u>: com resolução espacial de 30m, em escala de mapeamento de 1:25.000, pode gerar curvas de nível com equidistância de 10m.</p>

        <br />
        <b>Produtos:</b><br />

              •	Digitalização do levantamento topográfico da propriedade (caso tenha um levantamento);
        <br />•	Criação do uso do solo com as áreas delimitadas e calculadas;
        <br />•	Criação das curvas de nível a partir de imagem orbital;
        <br />•	Criação do Modelo Digital de Elevação (modelo TIN) a partir das curvas de nível;
        <br />•	Criação das cotas de altimetria;
        <br />•	Criação das classes de hipsometria;
        <br />•	Criação das classes de declividade;
        <br />•	Criação das orientações de vertentes;
        <br />•	Delimitação de bacia e micro bacias;
        <br />•	Criação das linhas de drenagem;
        <br />•	Criação do escoamento superficial e direção de escoamento;

        <div style="display: flex; justify-content: center; align-items: center">
            <img src="../Media/img/mapa-geo-detalhes-4.png" alt="" style="margin: 20px 0 20px 0; max-width: 450px" />            
            <img src="../Media/img/mapa-geo-detalhes-5.png" alt="" style="margin:  20px 0 20px 20px; max-width: 320px;" />
        </div>

        <h2 class="tituloCertificaoInterno">Mapas de Diagnóstico Ambiental e Legislação Ambiental </h2>
            
        <p>Partindo dos levantamentos e mapas anteriormente descritos, como altimetria, declividade, orientação de vertentes, escoamento superficial, entre outros; bem como com a identificação do uso do solo é possível cruzar essas informações e gerar um diagnóstico ambiental da propriedade e adequação com a legislação ambiental.</p>
        
        <img src="../Media/img/mapa-geo-detalhes-6.jpg" alt="" style="margin: 15px 0 15px 15px;  text-align: center; float: right" />

        <p>Com as regras do Novo Código Florestal Brasileiro de 2012 se faz necessário calcular e amostrar as Áreas de Preservação Permanente e a Reserva Legal para a declaração dentro do sistema do Cadastro Ambiental Rural (CAR). Com as tecnologias GIS e sensoriamento remoto é possível gerar alguns produtos cartográficos para esta finalidade.</p>
        <br />

        <b>Produtos:</b>
        <br />•	Criação do limite da propriedade (conforme memorial descritivo, croqui, orientação do proprietário / responsável ou levantamento de campo);
        <br />•	Digitalização de recursos hídricos a partir de imagens ou outras referências;
        <br />•	Digitalização das curvas de nível a partir de imagens orbitais;
        <br />•	Criação das cotas de altimetria e declividade;
        <br />•	Criação das Áreas de Preservação Permanentes devidamente delimitadas e calculadas;
        <br />•	Delimitação da Reserva Legal (recomendação de alocação).
        <br />•	Criação do uso do solo com as áreas delimitadas e calculadas;


        <h2 class="tituloCertificaoInterno">Mapas de Distribuição Espacial de Atributos do Solo</h2>

        <img src="../Media/img/mapa-geo-detalhes-7.png" alt="" style="margin: 0 15px 15px 0;  text-align: center; float: left" />        
        <img src="../Media/img/mapa-geo-detalhes-8.png" alt="" style="margin: 15px 0 15px 15px;  text-align: center; float: right" />
        <p>A partir de dados previamente coletados e dos resultados analíticos de laboratório desses dados da área amostrada podem ser gerados mapas dos componentes e atributos do solo. Teor de argila, areia fina, areia grossa, areia total, matéria orgânica, capacidade de troca catiônica (CTC), cálcio, magnésio, saturação de bases, fósforo, potássio ou qualquer outro atributo ou característica que foi coletado previamente pode ser espacializado com precisão. Os mapas são gerados utilizando os métodos geoestatísticos como a krigagem, inverso da distância, co-krigagem, dependendo do atributo investigado e da necessidade do cliente.</p>
        <br />

        <b>Produtos:</b><br />
        •	Criação do grid e pontos de amostragem para as coletas de dados em campo;
        <br />•	Criação das áreas de distribuição espacial de cada um dos atributos do solo coletados e analisados em laboratório;


        <h2 class="tituloCertificaoInterno">Mapas de Índices de Densidade e Qualidade Vegetal</h2>
        
        <p>Uma análise de densidade e qualidade vegetal busca discriminar as diferenças na vegetação instalada em uma determinada área. Baseia-se em imagens obtidas por sensores remotos e é apresentada de forma visual e numérica, indicando as variações existentes entre o menor e o maior valor de teor vegetativo. EVI, SAVI, NDVI, SR são exemplos de índices de vegetação que podem apontar para deficiência de nutrientes, pragas, qualidade de plantio, áreas em regeneração natural, densidade e vigor vegetativo. </p>
        
        <div style="display: flex; flex-direction: column; align-items: center">
            <img src="../Media/img/mapa-geo-detalhes-9.png" alt="" style="margin: 15px auto;  text-align: center" />
            <img src="../Media/img/seta.png" alt="" style="transform: rotate(90deg); max-height: 28px" />
            <img src="../Media/img/mapa-geo-detalhes-10.png" alt="" style="margin: 15px auto;  text-align: center" />
        </div>    

        <b>Produtos:</b><br />
        •	Criação de mapa de índice de vegetação;
        <br />•	Identificação, delimitação e cálculo de áreas de diferente densidade vegetal e vigor vegetativo;


        <h2 class="tituloCertificaoInterno">Publicação Online dos Geodados (Webmapping)</h2>
        
        <p>Publicação online de dados e mapas de interesse que podem ser acessados remotamente. Os clientes podem visualizar as informações em computadores, celulares, tablets, em qualquer PLATFORMa. É possível linkar essas informações em seu próprio site, podem enviar por e-mail ou limitar o acesso à visualização. Existe ainda a possibilidade de editar os dados hospedados, atualizar informações ou inserir novas informações no banco de dados geográfico dependendo da necessidade do cliente.</p>

        <div style="display: flex; justify-content: center; align-items: center">
            <div style="text-align: center">
                <b>WEB</b>
                <img src="../Media/img/mapa-geo-detalhes-11.png" alt="" style="margin: 20px 0 20px 0; max-width: 320px" />
            </div>
            <div style="text-align: center">
                <b>MOBILE</b>
                <img src="../Media/img/mapa-geo-detalhes-12.png" alt="" style="margin:  20px 0 20px 0; max-width: 320px;" />
            </div>
        </div>

        <b>Produtos:</b><br />
        •	Hospedagem de geodados gerados em servidor que permite visualização e edição em navegadores de internet (computador, celular e tablet) por tempo determinado.

        <br /><br />
        <b>Aquisição dos produtos e serviços</b><br />
        <p>Entre em contato com setor comercial do IBD que lhe enviará um formulário de solicitação de proposta. O formulário deverá conter algumas informações básicas sobre a propriedade e quais as necessidades e os produtos de interesse. Assim, o IBD retornará com uma proposta de serviços/produtos e com um contrato atrelado à aceitação da proposta. Os custos e prazos de entrega podem variar conforme o tamanho da propriedade e com a quantidade de produtos/serviços solicitados.</p>

        <br />
       <b>Observações:</b><br />
    <p>Todos os arquivos serão entregues em shapefiles, raster ou outro formato, dependendo do tipo de geodado. Será confeccionando um mapa com escala, legenda, sistema de coordenadas, em formato PDF, de cada produto solicitado. </p>
    <p>Em algumas situações será necessário o deslocamento de algum funcionário do IBD para coletas de dados na propriedade. Os custos de transporte e hospedagem são de responsabilidade do contratante e serão previamente acordados.</p>
    <p>Imagens de alta resolução requerem a compra de terceiros. Caso seja interesse do cliente, o IBD pode orientar ou intermediar essa compra e o valor será indicado na proposta.</p>
    <p>No momento, o IBD não faz serviços de topografia, georeferenciamento de imóvel rural, coleta e classificação de solos e seus atributos. Entretanto, alguns produtos requerem essas análises. O IBD se habilita a orientar e sugerir metodologias para tais levantamentos. </p>
    </div>
</asp:Content>
