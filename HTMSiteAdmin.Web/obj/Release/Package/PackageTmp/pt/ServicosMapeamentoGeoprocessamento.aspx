﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="ServicosMapeamentoGeoprocessamento.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.ServicosMapeamentoGeoprocessamento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Servi&ccedil;os
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Mapeamento e Geoprocessamento</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos" class="texto-justificado">
        <br />
        
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>

            <h2 class="tituloCertificaoInterno">O que é Geoprocessamento e Sistema de Informação Geográfica?</h2>
            <img src="../Media/img/mapa-geo-1.jpg" alt="" style="float: right; margin-left: 15px; margin-bottom: 10px; max-width: 300px" />
            <p>O geoprocessamento é a tecnologia capaz de espacializar e cruzar, geograficamente, informações de diversas fontes. Através de camadas de informações que podem ser estudados e analisados permite uma melhor compreensão de como os fatores geográficos interferem nas atividades produtivas e cotidianas.</p>
            <p>O sistema de informação geográfica (SIG) ou geographic information system (GIS) é um sistema de hardware, software, informação espacial, <img src="../Media/img/mapa-geo-2.png" alt="" style="float: left; margin: 15px 15px 10px 0; max-width: 220px" /> procedimentos computacionais e recursos humanos que permite e facilita a análise, gestão ou representação do espaço (paisagem) e dos fenômenos que nele ocorrem. Com um sistema GIS é possível coletar, armazenar, recuperar, manipular, visualizar e analisar dados espacialmente referenciados em um sistema de coordenadas conhecido. 
Como resultados são gerados mapas ou cartas que podem ser mapas físicos (relevo, clima, vegetação, hidrografia, etc.); mapas políticos-administrativos (território, países, estados, cidades, etc.) e mapas temáticos (elementos ou fenômenos específicos).
</p>
        <p style="clear: both; padding-top: 20px"><b>Com um sistema de informação geográfica é possível: </b></p>
        
        •	Interpretação e análise do meio físico e biótico: relevo, hidrografia, drenagem, solos, declividade, uso do solo, topografia, distribuição de espécies, localização de ocorrência, etc.;<br />
        •	Interpretação e análise de dados cadastrais: propriedades, áreas rurais e urbanas, áreas de proteção, sistema de transporte – logística;<br />
        •	Análises espaciais: de vizinhança, sobreposição, superfícies, redes, relação direta, correlação, contiguidade, conectividade, proximidade. <br />
        •	Análises diversas: classificação, generalização, densidade, índices, multicritérios, estatística, geoestatística, tendência.<br />

        <p style="clear: both; padding-top: 20px"><b>Dentre as aplicações pode-se destacar:</b></p>

        <img src="../Media/img/mapa-geo-3.jpg" alt="" style="float: right; margin-left: 15px; margin-bottom: 10px; max-width: 300px" />

        •	Geolocalização e georeferenciamento; <br />
        •	Cálculo de rotas (melhor, mais curto, etc.); <br />
        •	Cálculos de áreas (área, perímetro, volume, densidade, zoneamento, etc.); <br />
        •	Uso do solo e cobertura vegetal, histórico de uso solo do solo; <br />
        •	Índice de densidade vegetal (NDVI, EVI, etc.); <br />
        •	Cumprimento e adequação à Legislação Ambiental (Áreas de preservação permanente e reserva legal);<br />
        •	Análises de risco (áreas de risco a queimadas, zona de amortecimento de áreas de proteção e unidades de conservação, risco a erosões, etc.); <br />
        •	Mapas de distribuição espacial de atributos do solo, parâmetros, frequência ou qualquer outra informação quantitativa que tenha uma localização espacial, através de métodos geoestatísticos. Como exemplos: distribuição espacial de atributos físico-químicos do solo, distribuição espacial de chuvas, etc.; <br />

        <div style="display: flex; justify-content: space-between; align-items: center">
            <img src="../Media/img/mapa-geo-4.jpg" alt="" style="margin: 20px 0 20px 0; max-width: 320px" />
            <img src="../Media/img/seta.png" alt="" style="margin: 10px; max-width: 50px" />
            <img src="../Media/img/mapa-geo-5.jpg" alt="" style="margin:  20px 0 20px 0; max-width: 320px;" />
        </div>

         <img src="../Media/img/mapa-geo-6.jpg" alt="" style="float: right; max-width: 150px; margin-left: 20px" />

        •	Hospedagem online de informações geográficas para consulta, visualização e/ou edição dos dados via internet (webmapping).
        <br />
        <br />

        <p>O IBD dispõe de uma variedade de serviços e aplicações em geoprocessamento e sistema de informação geográfica. Acesse o link abaixo e veja o portfólio de produtos e serviços que o IBD oferece.</p>
        <p style="margin: 10px 0 10px 0"><a href="ServicosMapeamentoGeoprocessamentoDetalhes.aspx" style="text-decoration: underline">Clique e saiba mais sobre os produtos e serviços de mapeamento e geoprocessamento do IBD</a></p>
       
            
            
    </div>
    <!--quemsomos-->
</asp:Content>
