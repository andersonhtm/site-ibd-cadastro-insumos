﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="Acreditadores.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.Acreditadores" %>

<asp:Content ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Acreditadores</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="clientes">
        <asp:DataList runat="server" ID="dtAcreditadores" OnItemDataBound="dtAcreditadores_ItemDataBound">
            <ItemTemplate>
                <div id="01" class="listaParceiros">
                    <asp:Literal ID="ltrImagem" Text="ltrImagem" runat="server" />
                    <!--imagem-->
                    <div class="texto">
                        <h3>
                            <asp:Literal ID="ltrNome" Text="ltrNome" runat="server" /></h3>
                        <p>
                            <asp:Literal ID="ltrDetalhes" Text="ltrDetalhes" runat="server" /></p>
                    </div>
                    <!--texto-->
                </div>
            </ItemTemplate>
        </asp:DataList>
        <!--listaParceiros-->
    </div>
    <!--clientes-->
</asp:Content>
