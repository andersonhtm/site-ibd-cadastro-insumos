﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucChamadaEventos.ascx.cs"
    Inherits="HTMSiteAdmin.Web.pt.UserControls.ucChamadaEventos" %>
<asp:DataList ID="dtUltimasNoticias" runat="server" DataKeyField="ID_EVENTO" OnItemDataBound="dtUltimasNoticias_ItemDataBound"
    CellPadding="0" RepeatLayout="Flow" ShowFooter="False" ShowHeader="False">
    <ItemTemplate>
        <div class="itemNews">
            <div class="dataTipo">
                <span>
                    <asp:Literal ID="ltrDiaNoticia" runat="server" Text="1" />
                    |
                    <asp:Literal ID="ltrMesNoticia" runat="server" Text="Janeiro" /></span><h4>
                        <asp:Literal ID="ltrCategoria" runat="server" Text="Nacional" />
                    </h4>
            </div>
            <asp:Literal ID="ltrLinkNoticia" runat="server" Text="<a href='EventosFeirasDetalhes.aspx?id_conteudo={0}\'>Link</a>" />
        </div>
    </ItemTemplate>
</asp:DataList>