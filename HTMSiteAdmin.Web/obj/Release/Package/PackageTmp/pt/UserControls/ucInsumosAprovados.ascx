﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucInsumosAprovados.ascx.cs"
    Inherits="HTMSiteAdmin.Web.pt.UserControls.ucInsumosAprovados" %>
<asp:UpdatePanel runat="server" ID="upInsumosAprovados">
    <ContentTemplate>
        <h3>
            Insumos Aprovados</h3>
        <fieldset>
            <ul>
                <li>
                    <label>
                        esquema</label>
                    <asp:DropDownList runat="server" ID="ddlCertificado" DataTextField="NOME" DataValueField="ID_CERTIFICADO"
                        OnDataBound="ddlCertificado_DataBound" />
                </li>
                <li>
                    <label>
                        cliente</label>
                    <asp:TextBox runat="server" ID="txtClienteCertificadoCliente" />
                </li>
                <li>
                    <label>
                        insumo</label>
                    <asp:TextBox runat="server" ID="txtInsumo" />
                </li>
                <li>
                    <label>categoria</label>
                    <asp:DropDownList ID="ddlCategoria" DataTextField="DESCRICAO" DataValueField="ID_CATEGORIAPRODUTO" runat="server" />
                </li>
                <li>
                    <label>finalidade</label>
                    <asp:DropDownList ID="ddlFinalidade" DataTextField="DESCRICAO" DataValueField="ID_FINALIDADE" runat="server" />
                </li>
                <li>
                    <asp:LinkButton ID="btnClienteCertificadoBuscar" runat="server" CssClass="btBuscar"
                        OnClick="btnClienteCertificadoBuscar_Click" Style="margin-right: 0px;">buscar</asp:LinkButton></li>
            </ul>
        </fieldset>
    </ContentTemplate>
</asp:UpdatePanel>
