﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="FairTradeIbd_criterios.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.FairTradeIbd_criterios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Socio-Ambiental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px;
                    margin-right: 6px; margin-left: 6px;" />
                IBD Fair Trade Ecosocial</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoEcoSocialIBD.png" alt="" style="float: right;
                margin-left: 15px; margin-bottom: 10px;" />
            <span class="itemNormas">&lt;&lt;
                <asp:HyperLink ID="HyperLink1" runat="server" 
                NavigateUrl="~/pt/FairTradeIbd.aspx">Voltar para a p&aacute;gina incial do Selo Fair Trade Ecosocial</asp:HyperLink></span>
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">Critérios para certificação</h2>
            <h2 class="tituloCertificaoInterno">Críticos</h2>
            <p>
                - Quebra de rastreabilidade;<br />
                - Existência de desmatamentos não autorizados por órgão competente;<br />
                - Lançamento de efluente em corpo hídrico em desconformidade com a Legislação Ambiental vigente;<br />
                - Existência de Caça, captura e comercialização de animais silvestres;<br />
                - Exploração indevida do direito de propriedade;<br />
                - Uso e manipulação de Organismos Geneticamente Modificados;<br />
                - Não existência de procedimento de contratação e remuneração dos funcionários;<br />
                - Presença de discriminação social, cultural, política, religiosa, étnica, racial, sexual, idade;<br />
                - Trabalho infantil;<br />
                - Trabalho forçado;<br />
                - Trabalhadores expostos a risco sem a devida proteção individual;<br />
                 Outros aspectos sociais específicos para cada empreendimento.</p>
            <br />
            <h2 class="tituloCertificaoInterno">Ambientais</h2>
            <p>
                 - Adequação à legislação ambiental e regularização junto aos órgãos ambientais;<br />
                 - Conservação Ambiental; <br />
                 - Recuperação Ambiental; <br />
                 - Gerenciamento adequado dos recursos hídricos.<br />
            </p>
            <br />
            <h2 class="tituloCertificaoInterno">Sociais</h2>
            <p>
                - Comprometimento com a transparência;<br />
                - Adequação à legislação trabalhista;<br />
                - Apoio ao trabalho sindicalizado;<br />
                - Incremento da Segurança e Salubridade no trabalho;<br />
                - Igualdade de benefícios a trabalhadores fixos e temporários;<br />
                - Participação nos resultados;<br />
                - Capacitação de funcionários;<br />
                - Capacitação para gestão de grupos de produtores;<br />
                - Capacitação técnica de grupos produtores;<br />
                - Incentivo à Educação básica e continuada;<br />
                - Melhorias nas condições de Habitação;<br />
                - Alimentação e Saúde;<br />
                - Apoio à mulher trabalhadora, apoio à gestante e à lactante;<br />
                - Apoio ao idoso;<br />
                - Prevenção e apoio aos adictos (usuários de fumo, álcool e drogas);<br />
                - Incentivo à Organização e Participação Social.</p>
            <br />
            <h2 class="tituloCertificaoInterno">Desenvolvimento Econômico</h2>
            <p>
                - Promoção do Relacionamento comercial justo;<br />
                - Incentivo às Negociações abertas, transparentes e duradouras entre as partes interessadas;<br />
                - Fomento ao Desenvolvimento Social e Ambiental na cadeia produtiva, através do pagamento do Premium EcoSocial All Fair;<br />
                - Implantação das melhores práticas de produção visando a qualidade do produto;</p>
            <br />
    </div>
    <!--quemsomos-->
</asp:Content>
