﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="IBDNews.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.IBDNews" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                IBD News</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="news">
        <div>
            <p style="color: #595941; font-size: 15px;">
                O IBD News é uma publicação periódica realizada pela IBD Certificações.<br />
                Acesse o IBD News escolhendo a edição que deseja visualizar.</p>
        </div>
        <div class="ibdNews">
            <asp:DataList runat="server" ID="dtIBDNews" 
                onitemdatabound="dtIBDNews_ItemDataBound" RepeatLayout="Flow" 
                ShowFooter="False" ShowHeader="False">
                <ItemTemplate>
                    <asp:Literal ID="ltrIBDNews" Text="ltrIBDNews" runat="server" />
                </ItemTemplate>
            </asp:DataList>
        </div>
        <!--ibdNews-->
    </div>
    <!--news-->
</asp:Content>
