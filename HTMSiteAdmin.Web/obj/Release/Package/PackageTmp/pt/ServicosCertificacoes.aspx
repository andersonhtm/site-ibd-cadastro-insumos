﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="ServicosCertificacoes.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.ServicosCertificacoes" %>

<%@ Register Src="~/pt/UserControls/ucSelosRodape.ascx" TagPrefix="uc1" TagName="ucSelosRodape" %>


<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">        
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Servi&ccedil;os
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Certifica&ccedil;&otilde;es</h2>
            <a href="default.aspx">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos" class="interno">
        <br />
        <p>
            <img src="media/img/imagem_interna_13.jpg" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <p>
                Para atender às exigências da certificação, o IBD conta com uma equipe especializada
                de inspetores que fiscalizam as propriedades agrícolas e os processos de produção
                para verificar se o produto está sendo cultivado e/ou processado de acordo com as
                normas de produção orgânicas e biodinâmicas.<br />
                <br />
                A certificação exige uma série de cuidados, como a desintoxicação do solo por 1
                a 3 anos para áreas em transição de agricultura química para orgânica, a não utilização
                de adubos químicos e agrotóxicos, a obediência a aspectos ecológicos (manutenção
                de Áreas de Preservação Permanente por exemplo com a recomposição de matas ciliares)
                e a preservação de espécies nativas e mananciais, o respeito às reservas indígenas
                e às normas sociais baseadas nos acordos internacionais do trabalho, o tratamento
                humanitário de animais e para o protocolo Fair Trade Ecosocial, o envolvimento com projetos
                sociais e de preservação ambiental.<br />
                <br />
                O processo de certificação tem uma importância fundamental na viabilização da agricultura
                orgânica, sendo uma importante ferramenta no processo de desenvolvimento da consciência
                ecológica e social. Nesse sentido, o IBD, ciente de sua responsabilidade enquanto
                agente de transformação social vem financiando apoia projetos de pesquisas no campo
                agrícola e de assessoria e acompanhamento de projetos de pequenos agricultores.
                <h2 class="tituloCertificaoInterno">
                    Auditoria</h2>
                Exercendo um monitoramento constante, o IBD atua promovendo o equilíbrio entre a
                atividade econômica e a preservação da natureza. Nos protocolos de certificação
                socioambiental, grandes projetos certificados possuem programas de reflorestamento
                e de proteção à vida selvagem, como por exemplo, a manutenção de viveiros com essências
                nativas para serem utilizadas na recomposição de matas ciliares, corredores ecológicos
                e proteção de recursos hídricos, programas de prevenção a incêndios em áreas de
                vegetação nativa e a criação de espécies animais com risco de extinção para posterior
                devolução ao seu habitat.<br />
                <br />
                A certificação vem contribuindo com um processo de profundas transformações no ambiente
                agrícola e industrial. Ações de responsabilidade social são estimuladas e as empresas
                são motivadas a concederem salários justos, condições dignas de trabalho, treinamento
                e reciclagem profissional, causando um impacto positivo na elevação da qualidade
                de vida dos trabalhadores.
                <h2 class="tituloCertificaoInterno">
                    Agricultura familiar</h2>
                A agricultura orgânica e socioambiental tem incentivado, entre os agricultores familiares,
                o uso de sistemas de produção que aproveitam as árvores, os assim chamados sistemas
                agroflorestais, que têm se apresentado como uma alternativa para a sustentabilidade
                ecológica e energética.<br />
                <br />
                Além disso, viabiliza-se a participação de pequenos produtores no processo de certificação,
                incentivando a criação de grupos. Os agricultores são, reconhecidamente em muitos
                casos, os mantenedores dos ecossistemas, conservando e preservando as matas, rios,
                bosques e animais silvestres.<br />
                <br />
            </p>
            <h2 class="tituloCertificaoInterno">
                Segmentos Atendidos</h2>
            <p>
                &bull; Apicultura<br />
                &bull; Pecu&aacute;ria de corte e leite
                <br />
                &bull; Piscicultura e aq&uuml;icultura
                <br />
                &bull; Processamento de alimentos
                <br />
                &bull; Produ&ccedil;&atilde;o agr&iacute;cola
                <br />
                &bull; Produ&ccedil;&atilde;o de cosm&eacute;ticos
                <br />
                &bull; Produ&ccedil;&atilde;o de insumos
                <br />
                &bull; Produ&ccedil;&atilde;o de mat&eacute;rias-primas para cosm&eacute;ticos
                <br />
                &bull; Produ&ccedil;&atilde;o de sanitizantes
                <br />
                &bull; Produ&ccedil;&atilde;o de vinho
                <br />
                &bull; Produ&ccedil;&atilde;o t&ecirc;xtil
                <br />
                &bull; Produtos N&atilde;o Geneticamente Modificados
                <br />
                &bull; Restaurantes, pousadas, hot&eacute;is
                <br />
                &bull; Silvicultura e outros produtos extrativistas<br />
            </p>
        </p>
        <uc1:ucSelosRodape runat="server" id="ucSelosRodape" />
    </div>
    <!--quemsomos-->
</asp:Content>
