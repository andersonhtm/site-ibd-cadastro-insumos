$(document).ready(function(){
	
	$(".formulario form input, form textarea").click(function() {
		$(this).css("border-top","solid 1px #c4d5e3");
		$(this).css("border-left","solid 1px #c4d5e3");
		$(this).css("border-right","solid 1px #b5c0cb");
		$(this).css("border-bottom","solid 1px #b5c0cb");
	})
	
	$("#telefone").mask("99-9999-9999");
});

function validaFormContato(){
	
	if ($("#nome").val().length < 1){
		$("#valida-nome").fadeIn(300, function() { $(this).delay(2000).fadeOut(500);});
		$("#nome").css("border-bottom","solid 1px #168dd3"); 	
		mailres = false;		
	}
	
	if ($("#email").val().length < 1){
		$("#valida-email").fadeIn(300, function() { $(this).delay(2000).fadeOut(500);});
		$("#email").css("border-bottom","solid 1px #168dd3");		
		mailres = false;		
	}
 
	if ($("#cidade").val().length < 1){
		$("#valida-cidade").fadeIn(300, function() { $(this).delay(2000).fadeOut(500);});
		$("#cidade").css("border-bottom","solid 1px #168dd3");		
		mailres = false;		
	}
 
	if ($("#observacoes").val().length < 1){
		$("#valida-observacoes").fadeIn(300, function() { $(this).delay(2000).fadeOut(500);});
		$("#observacoes").css("border-bottom","solid 1px #168dd3");		
		mailres = false;
	}
	
	if (mailres != false){
	
	var texto = document.contatoForm.email.value;
    var mailres = true;             
    var cadena = "abcdefghijklmnnopqrstuvwxyzABCDEFGHIJKLMNNOPQRSTUVWXYZ1234567890@._-"; 
    
	var arroba = texto.indexOf("@",0); 
    if ((texto.lastIndexOf("@")) != arroba) arroba = -1; 
    
	var punto = texto.lastIndexOf("."); 
    	 for (var contador = 0 ; contador < texto.length ; contador++){ if (cadena.indexOf(texto.substr(contador, 1),0) == -1){ mailres = false; break; } } 
	if ((arroba > 1) && (arroba + 1 < punto) && (punto + 1 < (texto.length)) && (mailres == true) && (texto.indexOf("..",0) == -1)) mailres = true; else mailres = false; 
	}
	if(mailres == true){
	$('#contatoForm').submit();
	} else {
		$("#valida-email2").fadeIn(300, function() { $(this).delay(2000).fadeOut(500);});
		$("#email").css("border-bottom","solid 1px #168dd3");		
	}
	}
	
