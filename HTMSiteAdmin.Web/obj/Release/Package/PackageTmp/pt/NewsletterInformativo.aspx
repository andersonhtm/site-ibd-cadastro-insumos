﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="NewsletterInformativo.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.NewsletterInformativo" Theme="" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Newsletter</title>
    </head>
    <body bgcolor="#E0D8BA" style="background: #E0D8BA; margin: 0; padding: 0; font-family: Georgia, 'Times New Roman';">
        <center>
            <table id="content" style="background: #F7F2DF; width: 600px; margin: 10px auto; box-shadow: 0 0 10px #999">
                <tr>
                    <td>
                        <table bgcolor="#9AC92F" style="background: #9AC92F; width: 100%" cellpadding="5">
                            <tr>
                                <td>                                
                                    <a href="<%= new System.Uri(Page.Request.Url, "../pt/Default.aspx").AbsoluteUri %>" style="border: none">
                                        <img src="<%= new System.Uri(Page.Request.Url, "../pt/media/img/logo.png").AbsoluteUri %>" border="0" alt="IBD Certificações" style="padding: 15px; border: none" />                                    
                                    </a>                                
                                </td>
                                <td style="padding-left: 60px; color: #fff; vertical-align: bottom" valign="bottom">
                                    <table cellpadding="2">
                                        <tr>
                                            <td align="left" style="text-align: left; color: #ffffff; vertical-align: bottom" valign="bottom">
                                                <h2 style="margin: 0 0 5px 0">Newsletter Trimestral</h2>
                                                <h3 style="margin: 0"><%= DateTime.Now.ToString("MMMM / yy") %></h3>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="text-align: right;">
                                        <a href="<%= new System.Uri(Page.Request.Url, "../en/NewsletterInformativo.aspx")%>" style="border: none; visibility: hidden">
                                            <img src="<%= new System.Uri(Page.Request.Url, "../Media/img/en.png")%>" alt="View this e-mail in English" />                                        
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </table>  
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="10" width="100%" style="width: 100%">
                            <tr>
                                <td style="text-align: center; color: #625863;">IBD Certificações - é uma empresa 100% brasileira que desenvolve atividades de auditoria e certificação de processos e produtos agropecuários e extrativistas, em sistemas sustentáveis de forma confiável e inovadora.</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        <asp:Literal ID="ltrLinkImagemBannerTopo" runat="server"></asp:Literal>                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="noticias" cellpadding="10" runat="server" width="100%" style="width: 100%">                               
                            <tr>
                                <td>
                                    <h2 style="margin: 9px 0; color: #3a723d; font-size: 22px; font-style: italic; letter-spacing: -1px; font-weight: normal">Notícias</h2>
                                    <asp:DataList ID="dtNoticias" runat="server" RepeatLayout="Flow" ShowFooter="False" ShowHeader="False" OnItemDataBound="dtNoticias_ItemDataBound" DataKeyField="ID_CONTEUDO">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <table cellpadding="10">
                                                            <tr>
                                                                <td bgcolor="#5E8214" style="background: #5E8214">
                                                                    <div style="color: #fff; font-size: 20px; font-weight: normal">
                                                                        <asp:Literal ID="ltrDiaMesNews" Text="ltrDia" runat="server" />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>                                            
                                                    </td>                                
                                                    <td>
                                                        <h3 style="margin: 0 0 0 10px">
                                                            <asp:HyperLink ForeColor="#797949" Font-Bold="false" Font-Underline="false" ID="ltrLink" runat="server"></asp:HyperLink>
                                                        </h3>
                                                        <asp:Literal ID="litSubTitulo" runat="server"></asp:Literal>    
                                                        <div style="margin: 0 0 0 10px;">
                                                            <i style="font-size:11px;  color: #333;"><%# (!string.IsNullOrEmpty(Eval("FONTE").ToString()) ? "Fonte: " + Eval("FONTE").ToString():""  ) %></i>
                                                        </div>                                                                                  
                                                    </td>
                                                </tr>
                                            </table> 
                                        </ItemTemplate>
                                    </asp:DataList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="10" id="eventos" runat="server" width="100%" style="width: 100%">
                            <tr>
                                <td>
                                    <h2 style="margin: 9px 0; color: #3a723d; font-size: 22px; font-style: italic; letter-spacing: -1px; font-weight: normal">Próximos eventos</h2>
                                    <asp:DataList ID="dtEventos" runat="server" OnItemDataBound="dtEventos_ItemDataBound" ShowFooter="False" ShowHeader="False" EnableTheming="False" Visible="true">
                                        <ItemTemplate>
                                            <asp:DataList ID="dtListaEventos" runat="server" RepeatLayout="Table" ShowFooter="True" ShowHeader="True">                            
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <table cellpadding="10">
                                                                <tr>
                                                                    <td bgcolor="#5E8214" style="background: #5E8214">
                                                                        <div style="color: #fff; font-size: 20px; font-weight: normal">
                                                                            <asp:Literal ID="ltrDiaMes" runat="server" />
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="padding-left: 10px">
                                                            <h3>
                                                                <asp:HyperLink ForeColor="#797949" Font-Bold="false" Font-Underline="false" ID="hlLinkNome" runat="server" />
                                                            </h3>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>                           
                                            </asp:DataList>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="banners" cellpadding="5" cellspacing="0" width="100%" style="width: 100%">
                            <tr>
                                <td align="right" style="text-align: right">
                                    <asp:Literal ID="ltrLinkImagemBanner1" runat="server"></asp:Literal>
                                </td>
                                <td align="left" style="text-align: left">
                                     <asp:Literal ID="ltrLinkImagemBanner2" runat="server"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr style="margin: 10px 0 0 0" />
                        <table cellpadding="10" width="100%" style="width: 100%">
                            <tr>
                                <td>
                                    <address style="color: #625863; text-align: center;">Rua Amando de Barros, 2275, CEP: 18.602.150 - Botucatu - SP<br /> Telefone: 14 3811-9800</address>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>

