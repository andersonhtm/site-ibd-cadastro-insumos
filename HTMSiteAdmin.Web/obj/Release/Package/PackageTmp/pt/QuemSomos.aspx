﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="QuemSomos.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.QuemSomos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <link rel="stylesheet" type="text/css" href="media/css/adGallery.css" />
    <script type="text/javascript" src="media/js/adGallery.js"></script>
    <script type="text/javascript">        $(function () {
            $('.ad-gallery').adGallery();
            $('.toggle').hide();
            $(".box2 a").click(function () { $(this).toggleClass("btativo").next().slideToggle("slow"); return false })
        });</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Quem Somos</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <div class="box1" style="margin-bottom: 15px;">
            <div class="galeria">
                <div class="ad-gallery">
                    <div class="ad-image-wrapper">
                    </div>
                    <div class="ad-controls">
                    </div>
                    <div class="ad-nav">
                        <div class="ad-thumbs">
                            <ul class="ad-thumb-list">
                                <asp:Repeater ID="rptImages" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <a href='../ShowFile.aspx?action=1&fileid=<%# (Eval("ARQUIVO_DIGITAL") as HTMSiteAdmin.Data.ARQUIVO_DIGITAL).ID_ARQUIVO.ToString() %>'>
                                                <img src='../ShowFile.aspx?action=1&fileid=<%# (Eval("ARQUIVO_DIGITAL") as HTMSiteAdmin.Data.ARQUIVO_DIGITAL).ID_ARQUIVO.ToString() %>&width=67' width="67" alt='' />
                                            </a>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                        <!--ad-thumbs-->
                    </div>
                    <!--ad-nav-->
                </div>
                <!--ad-gallery-->
            </div>
            <!--galeria-->
            <p>
               O IBD orgulha-se em ser a maior certificadora da América Latina e a única certificadora brasileira de produtos orgânicos com credenciamento IFOAM (mercado internacional), ISO/IEC 17065 (mercado europeu-regulamento CE 834/2007), Demeter (mercado internacional), USDA/NOP (mercado norte-americano) e aprovado para uso do selo SISORG (mercado brasileiro), o que torna seu certificado aceito globalmente. 
                <br />
                <br />
               Além dos protocolos de certificação orgânica, o IBD oferece também <u><i>certificações de sustentabilidade</i></u>: RSPO <i>(Roundtable on Sustainable Palm Oil)</i>, UEBT <i>(Union for Ethical BioTrade)</i>, Fair Trade IBD, UTZ <i>(café, cacau)</i>, Rainforest Alliance, 4C<i> (café)</i>. 
            </p>
        </div>
        <!--box1-->
        <p>
            Localizado em Botucatu/SP (Brasil), desde sua fundação, o IBD vem atuando em todos os Estados brasileiros, bem como, em mais de 20 países como Argentina, Chile, Bolívia, Colômbia, Equador, México, Paraguai, Uruguai, Costa Rica, Guatemala, Estados Unidos, Canadá, Bélgica, Holanda, Alemanha, Portugal, Espanha, França, Itália, Nova Zelândia, China, Índia e Tailândia, auxiliando no desenvolvimento de padrões sustentáveis de produção e fomento ao Comércio Justo.<br />
            <br />
            O IBD tem como filosofia o compromisso com a Terra e o com o Homem, assegurando o respeito ao meio ambiente, boas condições de trabalho e produtos altamente confiáveis.<br />
            <br />
        </p>
    </div>
    <!--quemsomos-->
</asp:Content>
