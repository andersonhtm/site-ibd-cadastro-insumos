﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="FairTradeIbd.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.FairTradeIbd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }

        .itemNormas {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }

        .limpaFloat {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Socio-Ambiental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px; margin-left: 6px;" />
                IBD Fair Trade</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <img src="media/img/logoCertificadoEcoSocialIBD.png" alt="" style="float: right; margin-left: 15px; margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">Sobre o Selo</h2>
            Fair Trade é o Programa Fairtrade (Comércio Justo) do IBD aplicável a produtods agropecuários
            e naturais em geral.<br />
            <br />
            A Certificação Fair Trade se aplica a empresas, propriedades e grupos de produtores
            que visam desencadear um processo interno de desenvolvimento humano, social e ambiental
            fomentado por relações comerciais baseadas nos princípios do Comércio Justo. A partir
            de um diagnostico inicial que caracteriza a realidade socioambiental da organização,
            um Comitê Gestor, composto pelas partes interessadas no empreendimento, identifica
            e prioriza as principais demandas ambientais e sociais que interfere negativamente
            nesta realidade e a partir delas são traçados Planos de Ações visando a melhoria
            contínua nos aspectos socioambientais (Condições de Vida e de Trabalho, Conservação
            e Recuperação Ambiental).<br />
            <br />
            Os empreendimentos em certificação são auditados com base nos critérios:<br />
            - Críticos: relacionados a algumas Convenções e Acordos Internacionais;<br />
            - Mínimos: relacionados à legislação do país;<br />
            - Progresso: relacionados à promoção do desenvolvimento local<br />
            - Além daqueles já previstos nas leis nacionais.
            <br />
            <br />
            Estar certificado Fair Trade significa fomentar o desenvolvimento socioambiental
            local e praticar o Comércio Justo.
            <br />
            <br />

            Por favor clique abaixo para fazer  o download do mais recente folder do IBD Fair Trade<br />
            <a style="display: block" href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=fdb57265-a876-4c9c-bbf2-d4a2eca6a2be" target="_blank" class="itemNormas">Folder - IBD Cooperação Fair Trade</a>
            <br />
            <br />
            <h2 id="criterios" class="tituloCertificaoInterno">Critérios</h2>
            <a href="FairTradeIbd_criterios.aspx#01" class="itemNormas">Críticos</a> <a href="EcoSocialIBD_criterios.aspx#02"
                class="itemNormas">Ambiental</a> <a href="FairTradeIbd_criterios.aspx#03" class="itemNormas">Sociais</a> <a href="FairTradeIbd_criterios.aspx#04" class="itemNormas">Desenvolvimento
                        Econômico</a>
            <br />
            <br />

            <h2 class="tituloCertificaoInterno">Passo a Passo</h2>
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=b3922e67-079a-4368-8b0e-f8ed317ebf5f">- Passo a Passo para certificação (Português)</a><br />
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=b87883d3-dcec-4eba-8ed9-9b54f7903d9e">- Passo a Passo para certificação (Inglês)</a>
            <br />
            <br />

            <h2 class="tituloCertificaoInterno">Informa&ccedil;&otilde;es Adicionais</h2>
            <p>
                A Diretriz Fair Trade tem como base teórica Convenções da OIT, diversos Protocolos
                Internacionais como Agenda 21, Programa Pacto Global e Metas do Milênio, além de
                normas como SA 8000, ISO 14.000 e BS 8800. O IBD é certificado segundo padrões internacionais
                normatizados na ISO 65, garantindo qualidade a todo o processo de certificação.<br />
                <br />
                Mediante o exposto acima, estar certificado IBD Fair Trade é uma garantia de que
                os empreendimentos estão engajados em processos que visam o desenvolvimento sustentável,
                atendendo à demanda de consumidores atentos e conscientes, cada vez mais numerosos
                em todos os países. Com o Programa Fair Trade, o IBD deixou de atuar exclusivamente
                no Brasil e passou a operar certificações em 9 países do mundo, até o momento.<br />
                <br />

                <a href="DiretrizesLegislacao.aspx" class="itemNormas">Diretrizes e Legislação</a>
                <!--
                <span class="itemNormas">
                    Apresentação IBD Fair Trade Ecosocial (2008)</span> <span class="itemNormas">Cartilha EcoSocial
                        IBD</span> -->
                <br />
            </p>
            <br />
            <h2 class="tituloCertificaoInterno">Alguns Clientes</h2>

            <!-- AgroPalma -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=586" class="imagem">
                    <img src="media/img/logo_agropalma.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=586" class="titulo">AgroPalma</a>
            </div>

            <!-- ASR Group - Tate and Lyle Sugars ltd.-->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=602" class="imagem">
                    <img src="media/img/logo_asr.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=602" class="titulo">ASR Group - Tate and Lyle Sugars Ltd</a>
            </div>

            <!-- Bees Brothers Ltd-->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=610" class="imagem">
                    <img src="media/img/logo_bees.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=610" class="titulo">Bees Brothers</a>
            </div>

            <!-- Biorgânica-->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=587" class="imagem">
                    <img src="media/img/logo_biorganica.jpg" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=587" class="titulo">Biorgânica</a>
            </div>

            <!-- Ceres -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=588" class="imagem">
                    <img src="media/img/logo_ceres.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=588" class="titulo">Ceres</a>
            </div>

            <!-- Ciranda -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=589" class="imagem">
                    <img src="media/img/logo_ciranda.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=589" class="titulo">Ciranda</a>
            </div>

            <!-- Crofters Food Ltd. -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=591" class="imagem">
                    <img src="media/img/logo_crofters.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=591" class="titulo">Crofter&acute;s</a>
            </div>

            <!-- Cumberland Packaging Corporation -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=609" class="imagem">
                    <img src="media/img/logo_cumberland.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=609" class="titulo">Cumberland Packaging Corporatio</a>
            </div>

            <!-- Dalian Huaen -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=592" class="imagem">
                    <img src="media/img/logo_he.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=592" class="titulo">Dalian Huaen</a>
            </div>

            <!-- Davert GmbH -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=590" class="imagem">
                    <img src="media/img/logo_davert.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=590" class="titulo">Davert GmbH</a>
            </div>

            <!-- Do-It Dutch Organic International Trade -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=611" class="imagem">
                    <img src="media/img/logo_doit.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=611" class="titulo">Do-It Dutch Organic International Trade</a>
            </div>

            <!-- Exportadora Agricola Organica SAC -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=605" class="imagem">
                    <img src="media/img/logo_exportadoraagricolaorganica.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=605" class="titulo">Exportadora Agricola Organica SAC</a>
            </div>

            <!-- GFI Greenfood international BV -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=593" class="imagem">
                    <img src="media/img/logo_greenfoodinternational.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=593" class="titulo">GFI  Greenfoodinternational BV</a>
            </div>

            <!-- Global Organics, Ltd -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=607" class="imagem">
                    <img src="media/img/logo_globalorganic.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=607" class="titulo">Global Organics, Ltd</a>
            </div>

            <!-- Goiasa Goiatuba Álcool Ltda. -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=603" class="imagem">
                    <img src="media/img/logo_goiasa.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=603" class="titulo">Goiasa Goiatuba Álcool Ltda</a>
            </div>

            <!-- Jalles Machado -->
            <div class="itemClientesEcoSocial ">
                <a href="ClientesDetalhes.aspx?id_conteudo=594" class="imagem">
                    <img src="media/img/logo_jallesmachado.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=594" class="titulo">Jalles Machado</a>
            </div>

            <!-- La Grange -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=601" class="imagem">
                    <img src="media/img/logo_LaGrange.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=601" class="titulo">La Grange </a>
            </div>

            <!-- Matrunita - Grupo Maranhão -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=599" class="imagem">
                    <img src="media/img/logo_matrunita.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=599" class="titulo">Matrunita - Grupo Maranhão</a>
            </div>

            <!-- Native -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=595" class="imagem">
                    <img src="media/img/logo_native.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=595" class="titulo">Native Alimentos</a>
            </div>

            <!-- Reudink B.V. -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=604" class="imagem">
                    <img src="media/img/logo_reudink.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=604" class="titulo">Reudink B.V.</a>
            </div>

            <!-- Santa Hevea Comércio de Alimentos Ltda -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=596" class="imagem">
                    <img src="media/img/logo_organicsaudenatural.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=596" class="titulo">Santa Hevea Comércio de Alimentos Ltda</a>
            </div>

            <!-- Senfas -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=600" class="imagem">
                    <img src="media/img/logo_senfas.png" width="192" height="126" />
                </a>
                <a href="ClientesDetalhes.aspx?id_conteudo=600" class="titulo">Santa Hevea Comércio de Alimentos Ltda</a>
            </div>

            <!-- The Natural Growth Co. Ltd -->
            <div class="itemClientesEcoSocial">
                <a href="ClientesDetalhes.aspx?id_conteudo=597" class="imagem">
                    <img src="media/img/logo_ngc.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=597" class="titulo">The Natural Growth Co. Ltd.</a>
            </div>

            <!--  Usina Sao Francisco S/A (Native) -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=606" class="imagem">
                    <img src="media/img/logo_usinasaofrancisco.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=606" class="titulo">Usina Sao Francisco S/A (Native)</a>
            </div>

            <!--  Van Gorp Biologische Voeders B.V. -->
            <div class="itemClientesEcoSocial ecoSocialSemMargem">
                <a href="ClientesDetalhes.aspx?id_conteudo=598" class="imagem">
                    <img src="media/img/logo_biologische_diervoeders.png" width="192" height="126" /></a>
                <a href="ClientesDetalhes.aspx?id_conteudo=598" class="titulo">Van Gorp Biologische Voeders B.V.</a>
            </div>
            <!--quemsomos-->
    </div>
</asp:Content>
