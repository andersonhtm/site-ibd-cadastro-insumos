﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="Utz.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.Utz" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Socio-Ambiental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px;
                    margin-right: 6px; margin-left: 6px;" />
                UTZ</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
       
            <img src="media/img/logoCertificadoUtz.png" alt="Utz - Better farming, better future" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                Sobre o Selo</h2>
            <p>
            Se compra café, cacau ou chá UTZ, contribui para construir um futuro melhor. 
            Porque UTZ representa a agricultura sustentável com melhores perspectivas para os agricultores, suas famílias e nosso planeta. Graças ao programa UTZ, os agricultores aprendem melhores práticas agrícolas, criam melhores condições de trabalho e podem cuidar melhor de seus filhos(as) e da natureza. 
            </p><br />
            <p>
                Desta maneira UTZ faz uma contribuição positiva em grande escala: os agricultores obtêm melhores colheitas, melhor renda y melhores perspectivas e ademais cuidam do meio ambiente e asseguram os recursos naturais da terra. Agora e no futuro. 
                E isso tem sabor muito melhor. 
            </p><br />
            <h3 style="font-weight: bold; margin-bottom: 7px">Fazenda a diferença a nível mundial</h3>
            <p>UTZ quer que a agricultura sustentável seja algo completamente normal. E estamos no caminho certo: uma parte cada vez maior de todo o café, cacau e chá no mundo é cultivado de forma sustentável. De fato, quase a metade de todo o café sustentável é UTZ. Cooperamos com empresas como Mars, Nestlé, Ahold, IKEA, D.E. Master Blenders 1753, Migros y Tchibo. De maneira que quando você compra estes e outros produtos certificados por UTZ, nos podemos apoiar a mais agricultores, seus trabalhadores e suas famílias para que realizem suas metas. </p>
            <br />
            <h3 style="font-weight: bold; margin-bottom: 7px">Requisitos estritos, supervisão próxima</h3>
            <p>Os produtos de café, cacau e chá não obtém o selo UTZ facilmente. O cumprimento dos estritos requisitos para as fazendas e negócios certificados por UTZ, é monitorado de maneira rigorosa por terceiros. Estes requisitos incluem as boas práticas agrícolas e de gestão, condições de trabalho saudável y seguras, a abolição do trabalho infantil e a proteção da natureza. 
            UTZ oferece a possibilidade de rastrear o café, cacau e chá desde a prateleira da loja até o agricultor. O logotipo da UTZ sobre o produto te assegura que sua marca favorita apoia a agricultura sustentável.
            </p>
            <br />
            <br />

            <h2 class="tituloCertificaoInterno">Passo a Passo</h2>
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=22e33984-3533-47bb-b347-f32e0c61b8f5"> - Passo a Passo para certificação (Português)</a>
            <br /><br />
                   
            
            <h2 class="tituloCertificaoInterno">
                Diretrizes Atendidas</h2>            
               - UTZ (Código de Conduta e Cadeia de Custódia)  
            <br />
            <br />
           
            <h2 class="tituloCertificaoInterno">
                Segmentos Atendidos</h2>            
                - Agricultura<br />
                - Cadeia de custódia 

          
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Clientes Certificados
            </h2>

            <asp:Repeater ID="rpt" runat="server">
                <HeaderTemplate>
                    <ul class="list">
                </HeaderTemplate>
                <ItemTemplate>                    
                    <li><a href="<%# Eval("Caminho") %>" class="imagem"><%# Eval("Descricao")%></a></li>                    
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>

    </div>
    <!--quemsomos-->
</asp:Content>
