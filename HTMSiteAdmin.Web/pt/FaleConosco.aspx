﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="FaleConosco.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.FaleConosco" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Fale Conosco</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="painelCliente" class="faleConosco">
        <div>
            <p style="font-size: 15px;">
                Entre em contato com o IBD Certificações, pelo formulário abaixo ou pelos contatos
                a seguir:</p>
        </div>
        <br />
        <div style="float: left; width: 370px;">
            <ul>
                <li>
                    <asp:Label Text="assunto" runat="server" AssociatedControlID="ddlAssunto" />
                    <asp:DropDownList runat="server" ID="ddlAssunto" Style="float: left;" Width="260px">
                        <asp:ListItem Text="Certifique-se pelo IBD" Selected="True" />
                        <asp:ListItem Text="Dúvidas e Suporte" />
                        <asp:ListItem Text="Departamento Financeiro" />
                    </asp:DropDownList>
                </li>
                <li>
                    <asp:Label ID="Label1" Text="nome" runat="server" AssociatedControlID="txtNome" />
                    <asp:TextBox runat="server" ID="txtNome" Style="float: left;" Width="250px" />
                    <asp:RequiredFieldValidator ID="rfvAssunto" ControlToValidate="txtNome" runat="server"
                        ErrorMessage="- Nome" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label2" Text="e-mail" runat="server" AssociatedControlID="txtEmail" />
                    <asp:TextBox runat="server" ID="txtEmail" Style="float: left;" Width="250px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtEmail"
                        runat="server" ErrorMessage="- E-mail" Display="None" SetFocusOnError="True"
                        ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="- E-mail inválido" Display="None" SetFocusOnError="true" ValidationGroup="vgForm"
                        ValidationExpression="\S+@\S+.\S{2,3}"></asp:RegularExpressionValidator>
                </li>
                <li>
                    <asp:Label ID="Label3" Text="telefone" runat="server" AssociatedControlID="txtTelefone" />
                    <asp:TextBox runat="server" ID="txtTelefone" Style="float: left;" Width="250px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtTelefone"
                        runat="server" ErrorMessage="- Telefone" Display="None" SetFocusOnError="True"
                        ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:Label ID="Label4" Text="mensagem" runat="server" AssociatedControlID="txtMensagem" />
                    <asp:TextBox runat="server" ID="txtMensagem" Style="float: left;" Rows="10" TextMode="MultiLine"
                        Width="250px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtMensagem"
                        runat="server" ErrorMessage="- Mensagem" Display="None" SetFocusOnError="True"
                        ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                </li>
                <li>
                    <asp:LinkButton ID="btnEnviar" Text="enviar" runat="server" CssClass="cadastrar"
                        Style="float: right;" OnClick="btnEnviar_Click" ValidationGroup="vgForm" /></li>
            </ul>
            <asp:ValidationSummary ID="vsForm" runat="server" ValidationGroup="vgForm" CssClass="erroLogin"
                HeaderText="Os seguintes campos contêm erros ou são obrigatórios:" />
        </div>
        <div class="dados" style="margin-top: -2px;">
            <img src="media/img/fotoFaleconosco.jpg" alt="" width="266" height="166" /><br />
            <br />
            <span>Telefone</span>
            <div class="num">
                +55 (14) 3811 9800</div>
            <br />
            <span>Fax</span>
            <div class="num">
                +55 (14) 3811 9801</div>
            <br />
            <span>Email</span><br />
            ibd@ibd.com.br<br />
            <br />
            <span>Endereço</span>
            <address>
                Rua Amando de Barros, 2275 - Centro
                <br />
                CEP: 18.602.150 – Botucatu - SP
            </address>
            <br />
        </div>

        <div style="clear:both">
            <div class="quemSomos">
                <h2 class="tituloCertificaoInterno"> Reclamações </h2>
                <ol class="list-number">
                <li>
                    Caso deseje reportar uma reclamação e/ou denuncia, por gentileza, utilize o link: <br />
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSf3O2KEgYomA7bCBfBtm2wPXydpEV6nmNqShb366AlozQKpkw/viewform">docs.google.com/forms/d/e/1FAIpQLSf3O2KEgYomA7bCBfBtm2wPXydpEV6nmNqShb366AlozQKpkw/viewform</a>
                </li>
                <li>
                    Será informado o número do protocolo ao contato indicado.
                </li>
                <li>
                    Todas as reclamações ou denúncias deverão ser processadas no prazo máximo de 60 dias. Devido ao processos certificação e ações necessárias de esclarecimento o processo poderá se alongar. O reclamante/denunciante será informado do status de sua reclamação.
                </li>
            </ol>
            </div>
        </div>

    </div>
    <!--painelCliente-->
</asp:Content>
