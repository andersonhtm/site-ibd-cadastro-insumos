﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="Sai.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.Sai" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }

        .itemNormas {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }

        .limpaFloat {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Socio-Ambiental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px; margin-left: 6px;" />
                SAI PLATFORM</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />

        <img src="media/img/LOGO_FSA_SAI.png" alt="SAI PLATFORM" style="float: right; margin-left: 15px; margin-bottom: 10px;" />
        <h2 class="tituloCertificaoInterno">Sobre o Selo</h2>
        <b>Avaliação de Sustentabilidade Agrícola</b>
        <br />
        <p>
            Uma ferramenta simples e altamente eficaz para avaliar, melhorar e comunicar a sustentabilidade nas fazendas. 
            Usado pelas principais empresas de alimentos e bebidas para obter matérias-primas agrícolas produzidas de forma sustentável. Usado pelos agricultores para avaliar a sustentabilidade de suas fazendas.
        </p><br />
        <p>
            O mecanismo de pontuação fácil fornece aos agricultores e seus clientes uma visão geral da sustentabilidade de suas fazendas. As empresas avaliam e comparam as práticas sustentáveis de vários 
            fornecedores e agricultores usando uma única ferramenta e também podem ser usadas para avaliar outros padrões e esquemas existentes.
        </p><br />
        <p>
            Fornece uma referência única para comparar códigos, esquemas e legislação existentes e suporta a agregação de dados agrícolas entre países, commodities e fornecedores - servindo de base para a melhoria contínua. 
            Os agricultores podem melhorar o desempenho, economizar tempo e recursos, reduzir custos e obter acesso a diversos mercados usando a ferramenta FSA.
        </p><br />

        <b>Plataforma de Iniciativa Agrícola Sustentável</b><br />

        <p>
            A Plataforma de Iniciativa Agrícola Sustentável (SAI Platform) é a principal iniciativa global da cadeia de custódia de alimentos e bebidas para a agricultura sustentável.
        </p><br />
        <p>
            Empresas de alimentos e varejistas são os maiores compradores de matérias-primas agrícolas. Para garantir um suprimento constante, crescente e seguro de matérias-primas agrícolas, elas devem ser cultivadas de maneira sustentável. 
            O objetivo geral da Plataforma SAI é apoiar o desenvolvimento da agricultura sustentável em todo o mundo.
        </p><br />

        <p>
            A SAI Platform é uma organização sem fins lucrativos que facilita o compartilhamento, em nível pré-competitivo, do conhecimento e das melhores práticas para apoiar o desenvolvimento e a implementação de práticas agrícolas sustentáveis envolvendo as partes interessadas em toda a cadeia de valor dos alimentos. 
            Hoje, conta com mais de 90 membros, que compartilham ativamente a mesma visão sobre agricultura sustentável.
        </p><br />

        <p>
            A SAI Platform desenvolve ferramentas e orientação para apoiar práticas de agricultura sustentáveis locais e globais. Exemplos de recursos recentemente desenvolvidos incluem: “Documento dos Princípios e Práticas do Setor”; 
            “Guia para Fornecimento Sustentável”; recomendações para “Avaliação de Desempenho em Sustentabilidade (SPA)”; e a “Avaliação de Sustentabilidade Agrícola (FSA)”.
        </p><br />

        <h2 class="tituloCertificaoInterno">Diretrizes Atendidas</h2>
        - Diretrizes do programa FSA - SAI PLATFORM <a href="http://www.saiplatform.org">www.saiplatform.org</a>  / <a href="http://www.fsatool.com">www.fsatool.com</a>
        <br />
        <br />

        <h2 class="tituloCertificaoInterno">Segmentos Atendidos</h2>
        - Agricultura<br />
        - Industria de alimento e bebidas<br />

    </div>
    <!--quemsomos-->
</asp:Content>
