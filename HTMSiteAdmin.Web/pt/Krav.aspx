﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="Krav.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.Krav" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Orgânico
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                KRAV </h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">       
        <h2 class="tituloCertificaoInterno">
            Sobre o Selo</h2>
        KRAV é o selo líder de produtos orgânicos na Suécia. <br />
        O IBD oferece a verificação dos requisitos extra da KRAV em relação ao regulamento EC834/2007, para os clientes que desejam comercializar seus produtos ou ingredientes para importadores suecos certificados KRAV.

        <h2 class="tituloCertificaoInterno">
            Diretrizes Atendidas</h2>
            - KRAV
        <br />
        <br />
        <h2 class="tituloCertificaoInterno">
            Segmentos Atendidos</h2>
        - Agricultura <br />
        - Pecuária  <br />
        - Processamento <br />
        - Extrativismo <br /> 

        <br />
        <br />
        <br />
        <br />
    </div>
</asp:Content>
