﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Business.Links;

namespace HTMSiteAdmin.Web.pt
{
    public partial class Ras : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            rpt.DataSource = new LinkBo().Find(lbda => lbda.ID_LINK_TIPO == 4).OrderBy(x => x.DESCRICAO);
            rpt.DataBind();
        }
    }
}