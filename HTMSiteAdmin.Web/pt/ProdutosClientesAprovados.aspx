﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="ProdutosClientesAprovados.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.ProdutosClientesAprovados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Clientes</span><br />
                Produtos e Clientes IBD
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Consultar</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="certificadosConsultar">
        <div class="texto">
            <p>
                Pesquise os Produtos e Clientes IBD. <br />Para facilitar sua busca
                escolha os critérios que deseja:</p>
            <fieldset>
                <ul>
                    <li>
                        <label>
                            por esquema</label>
                        <asp:DropDownList runat="server" ID="ddlCertificado" DataTextField="NOME" DataValueField="ID_CERTIFICADO"
                            OnDataBound="ddlCertificado_DataBound" Width="250px" Style="float: left;" />
                    </li>
                    <li>
                        <label>
                            por produto</label>
                        <asp:TextBox runat="server" ID="txtClienteCertificadoProduto" Width="250px" Style="float: left;" />
                    </li>
                    <li>
                        <label>
                            por cliente</label>
                        <asp:TextBox runat="server" ID="txtClienteCertificadoCliente" Width="250px" Style="float: left;" />
                    </li>
                    <li>
                        <label>
                            por país</label>
                        <asp:DropDownList runat="server" ID="ddlClienteCertificadoPais" DataTextField="PAIS"
                            DataValueField="PAIS" OnSelectedIndexChanged="ddlClienteCertificadoPais_SelectedIndexChanged"
                            AutoPostBack="True" OnDataBound="ddlClienteCertificadoPais_DataBound" Width="250px"
                            Style="float: left;" />
                    </li>
                    <li>
                        <label>
                            por estado</label>
                            <asp:DropDownList runat="server" ID="ddlClienteCertificadoUf" OnDataBound="ddlClienteCertificadoUf_DataBound"
                                DataTextField="ESTADO" DataValueField="ESTADO_SIGLA" Visible="false" style="float:left;width:250px" />
                            <span id="spanTextoEstado" runat="server" visible="true" 
                                style="font-weight:normal;color:#A2AC94;font-style:italic;">
                                [escolha um país]
                            </span>
                    </li>
                    <li></li>
                    <li>
                        <asp:LinkButton ID="btnClienteCertificadoBuscar" runat="server" CssClass="btBuscar"
                            OnClick="btnClienteCertificadoBuscar_Click">buscar</asp:LinkButton>
                    </li>
                </ul>
            </fieldset>
        </div>
        <!--texto-->
        <div class="imagem">
            <img src="media/img/produtosCertificados.jpg" class="border3pxfff" alt="" /></div>
        <!--imagem-->
    </div>
    <!--certificadosConsultar-->
</asp:Content>
