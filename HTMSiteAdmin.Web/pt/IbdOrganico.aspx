﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="IbdOrganico.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.IbdOrganico" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Orgânico
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                IBD Orgânico e Org&acirc;nico Brasil</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <img src="media/img/logoCertificadoIBDOrganico.png" alt="" width="140" height="206"
                style="float: right; margin-left: 15px; margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                Sobre o Selo</h2>
            <p>
                O selo IBD Org&acirc;nico tem duas caracter&iacute;sticas. Ao matricular-se no IBD
                deverá ser especificado para qual norma deseja ser certificado:<br />
                <br />
                1) Atende a todas as certificações orgânicas feitas pelo IBD Certificações no mercado
                interno e é usado em conjunto com o selo de produtos orgânicos do Brasil.
            </p>
            <img src="media/img/logoCertificadoSisOrg_meio.png" alt="" />
            <p>
                2) Atende a todas as certificações feitas para o Mercado Comum Europeu, sendo que
                o IBD está acreditado com adoção de normas equivalentes, valendo para isso as Diretrizes
                IBD, aprovadas pelos credenciadores Europeus como equivalente à norma Européia.</p>
            <img src="media/img/logoCertificadoEUOrganic_meio.png" alt="" />
            <p>
                3) Atende a todas as certificações feitas para o Mercado Norte Americano (USA),
                sendo que o IBD está acreditado com adoção de norma USDA/NOP, valendo somente estas.</p>
            <img src="media/img/selo_nop.png" alt="" />
           
            <h2 class="tituloCertificaoInterno">Passo a Passo</h2>

            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=07511992-13b3-4b3f-80a2-3d7cee456607">Passo a Passo para certificação (Português)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=93014008-7780-4669-90c7-e90ccffbab87">Passo a Passo para certificação (Inglês)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=787619fb-fc31-4eed-a1bf-404738c0ecb0">Passo a Passo para certificação (Espanhol)</a><br />
            <br /><br />

            <h2 class="tituloCertificaoInterno">Passo a Passo certificação Grupos de Produtores</h2>

            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=40b81a87-3a1c-4e7b-990f-4e0ca875c0e2">Passo a Passo para certificação (Português)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=b66b1167-f68e-4810-a581-53bbf9d4e7c6">Passo a Passo para certificação (Inglês)</a><br />
            - <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=5ad12599-e08f-4656-a880-76c4304a73e2">Passo a Passo para certificação (Espanhol)</a><br />
            <br /><br />


            <h2 class="tituloCertificaoInterno">
                Diretrizes Atendidas</h2>
                <!-- 
            <span class="itemNormas">Normas IBD</span> <span class="itemNormas">Normas IFOAM
            </span><span class="itemNormas">Normas Quebec </span><span class="itemNormas">CEE 834/2007
            </span><span class="itemNormas">Lei 10.831 (SisOrg) </span><span class="itemNormas">
                NOP Extrato (Dez/06 - Português)</span> <span class="itemNormas">Lista Nacional (Set/06
                    - Portugês)</span> <span class="itemNormas">NOP (Dez/06 - Inglês)</span>
                    -->
                - Normas IBD<br />
                - Normas IFOAM<br />
                - Normas USDA<br />
                - Lei 10.831 (SisOrg)<br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segmentos Atendidos</h2>
            - Agricultura<br />
            - Pecuária<br />
            - Fibras<br />
            - Aquicultura<br />
            - Processamento<br />
            - Insumos<br />
            - Extrativismo<br />
            - Cosméticos<br />
            - Vinhos<br />
            - Produtos de Limpeza
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Informa&ccedil;&otilde;es Adicionais</h2>
            <p>
                O uso do selo IFOAM ACCREDITED é opcional.</p>
            <br />
            <p>
                Para re-certificação &eacute; necess&aacute;rio se realizar:<br />
                - Certificado Anual +
                <br />
                - Certificado de Transação +
                <br />
                - Revisão de relatórios +
                <br />
                - Check-list Sócio-ambiental.
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </p>
    </div>
    <!--quemsomos-->
</asp:Content>
