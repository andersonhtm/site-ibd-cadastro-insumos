﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Links;

namespace HTMSiteAdmin.Web.pt
{
    public partial class LinksInteressantes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var _ctx = new HTMSiteAdminEntities())
            {
                dtLinksInteressantes.DataSource = _ctx.GetCategoriasLinksInteressantes().Where(x=>x.ID_LINK_CATEGORIA != 9).OrderBy(x=>x.DESCRICAO);
                dtLinksInteressantes.DataBind();
            }
        }

        protected void dtLinksInteressantes_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            ((Literal)e.Item.FindControl("ltrCategoria")).Text = ((LINK_CATEGORIA)e.Item.DataItem).DESCRICAO;
            ((DataList)e.Item.FindControl("dtLinks")).ItemDataBound += new DataListItemEventHandler(dtDownloads_ItemDataBound);
            ((DataList)e.Item.FindControl("dtLinks")).DataSource = new LinkBo().Find(lbda => lbda.ID_LINK_TIPO == 1 && lbda.ID_LINK_CATEGORIA == ((LINK_CATEGORIA)e.Item.DataItem).ID_LINK_CATEGORIA).OrderBy(order => order.DESCRICAO);
            ((DataList)e.Item.FindControl("dtLinks")).DataBind();
        }

        void dtDownloads_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (((LINK)e.Item.DataItem).ID_ARQUIVO.HasValue)
                ((Literal)e.Item.FindControl("ltrImageLink")).Text = "<div class=\"imagem\"><a href=\"" + ((LINK)e.Item.DataItem).CAMINHO + "\"><img src=\"" + ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", ((LINK)e.Item.DataItem).ID_ARQUIVO.ToString())) + "\" alt=\"\" /></a></div>";
            else
                ((Literal)e.Item.FindControl("ltrImageLink")).Text = string.Empty;

            ((Literal)e.Item.FindControl("ltrLink")).Text = "<a href=\"" + ((LINK)e.Item.DataItem).CAMINHO + "\" class=\"link\">" + ((LINK)e.Item.DataItem).CAMINHO + "</a>";
            ((Literal)e.Item.FindControl("ltrLinktitle")).Text = "<a href=\"" + ((LINK)e.Item.DataItem).CAMINHO + "\">" + ((LINK)e.Item.DataItem).DESCRICAO + "</a>";
            ((Literal)e.Item.FindControl("ltrLinkName")).Text = !string.IsNullOrWhiteSpace(((LINK)e.Item.DataItem).DESCRICAO_DET) ? ((LINK)e.Item.DataItem).DESCRICAO_DET : string.Empty;
        }
    }
}