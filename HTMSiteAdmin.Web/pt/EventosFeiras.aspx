﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="EventosFeiras.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.EventosFeiras" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Eventos e Feiras</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="eventos">
        <div>
            <p>
                Nesta seção fique por dentro dos Eventos e Feiras que o IBD participará ou promoverá:</p>
        </div>
        <asp:DataList ID="dtEventos" runat="server" OnItemDataBound="dtEventos_ItemDataBound"
            RepeatLayout="Flow" ShowFooter="False" ShowHeader="False" EnableTheming="False"
            Width="100%">
            <ItemTemplate>
                <asp:Literal ID="ltrMesAno" Text="ltrMesAno" runat="server" />
                <asp:DataList ID="dtListaEventos" runat="server" RepeatLayout="Flow" ShowFooter="False"
                    ShowHeader="False">
                    <ItemTemplate>
                        <div class="itemEvento">
                            <h3>
                                &nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal ID="ltrLinkNome" Text="ltrLinkNome" runat="server" /></h3>
                            <div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data:
                                <asp:Literal ID="ltrDataEvento" Text="ltrDataEvento" runat="server" />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
            </ItemTemplate>
        </asp:DataList>
        <asp:Panel ID="litEventos" runat="server" Visible="false"><br /><p>Nenhum evento nos próximos meses</p></asp:Panel>
        <!--itemEvento-->
    </div>
    <!--eventos-->
</asp:Content>
