﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Downloads;

namespace HTMSiteAdmin.Web.pt
{
    public partial class IBDNews : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dtIBDNews.DataSource = new DownloadBo().Find(lbda => lbda.ATIVO && lbda.ID_DOWNLOAD_TIPO == 4).OrderBy(lbda => lbda.ORDEM).ThenBy(lbda => lbda.DESCRICAO);
            dtIBDNews.DataBind();
        }

        protected void dtIBDNews_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            ((Literal)e.Item.FindControl("ltrIBDNews")).Text = "<a href=\"" + HTMSiteAdmin.Library.Util.GetServerPath(this.Page) + string.Format("/ShowFile.aspx?action=2&fileid={0}", ((DOWNLOAD)e.Item.DataItem).ID_ARQUIVO_DIGITAL.ToString()) + "\">" + ((DOWNLOAD)e.Item.DataItem).DESCRICAO + "<span></span></a>";
        }
    }
}