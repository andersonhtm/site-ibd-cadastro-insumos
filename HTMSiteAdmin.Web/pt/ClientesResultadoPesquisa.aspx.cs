﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Importacao;
using System.Data.Objects;
using System.Text;
using System.Web.UI.HtmlControls;

namespace HTMSiteAdmin.Web.pt
{
    public partial class ClientesResultadoPesquisa : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        private struct strProdutoViewModel
        {
            public string ID_CLIENTE;
            public int ID_PRODUTO;
            public string NOME;
        }

        public string QueryStringToPrintPage{
            get
            {
                StringBuilder str = new StringBuilder();

                str.AppendFormat("ID_CERTIFICADO={0}", idCertificado.ToString());
                str.AppendFormat("&PRODUTO={0}", strProduto);
                str.AppendFormat("&CLIENTE={0}", strCliente);
                str.AppendFormat("&PAIS={0}", strPais);
                str.AppendFormat("&ESTADO_SIGLA={0}", strEstadoSigla);

                return str.ToString();
            }
        }

        #region Properties - QueryString
        public int nrPagina
        {
            get
            {
                try { return int.Parse(Request.QueryString["pagina"].ToString()); }
                catch (Exception) { return 1; }
            }
        }
        public int? idCertificado
        {
            get
            {
                try { return int.Parse(Request.QueryString["ID_CERTIFICADO"].ToString()); }
                catch (Exception) { return 0; }
            }
        }
        public string strProduto
        {
            get
            {
                try { return Request.QueryString["PRODUTO"].ToString(); }
                catch (Exception) { return null; }
            }
        }
        public string strCliente
        {
            get
            {
                try { return Request.QueryString["CLIENTE"].ToString(); }
                catch (Exception) { return null; }
            }
        }
        public string strPais
        {
            get
            {
                try
                {
                    if (Request.QueryString["PAIS"].ToString().Equals("0"))
                        return null;
                    else
                        return Request.QueryString["PAIS"].ToString();
                }
                catch (Exception) { return null; }
            }
        }
        public string strEstadoSigla
        {
            get
            {
                try
                {
                    if (Request.QueryString["ESTADO_SIGLA"].ToString().Equals("0"))
                        return null;
                    else
                        return Request.QueryString["ESTADO_SIGLA"].ToString();
                }
                catch (Exception) { return null; }
            }
        }
        #endregion


        private int totalRegistros { get; set; }

        private List<IMPORTACAO_CLIENTE> _objResult { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            ((DataList)Master.FindControl("dtModaisClientes")).ItemDataBound += new DataListItemEventHandler(dtModais_ItemDataBound);

            int starIndex = (nrPagina - 1) + 20;

            var query = new HTMSiteAdminEntities().ClientesCertificados(
                true
                , idCertificado == 0 ? null : idCertificado
                , strProduto == null ? null : strProduto.Replace("'","")
                , strCliente == null ? null : strCliente.Replace("'", "")
                , strPais == null ? null : strPais.Replace("'", "")
                , strEstadoSigla == null ? null : strEstadoSigla.Replace("'", ""), false, null, null).ToList();

            _objResult = query.Skip(((nrPagina * 20) - 20)).Take(20).ToList();
            totalRegistros = query.Count();

            dtRestultadoPesquisa.DataSource = _objResult;
            dtRestultadoPesquisa.DataBind();
            ((DataList)Master.FindControl("dtModaisClientes")).DataSource = _objResult;
            ((DataList)Master.FindControl("dtModaisClientes")).DataBind();

            //if (idCertificado == -1)
            //{
            //    ltrNovaPesquisa.Text = "<a href=\"ProdutosClientesAprovadosEuropeu.aspx\" class=\"novaPesquisa\" style=\"float: right;\">nova pesquisa</a>";
            //    ltrCertificadoSelecionado.Text = "Diretriz EU";
            //}
            //else if (idCertificado == 0)
            //{
            //    ltrNovaPesquisa.Text = "<a href=\"ProdutosClientesAprovados.aspx\" class=\"novaPesquisa\" style=\"float: right;\">nova pesquisa</a>";
            //    ltrCertificadoSelecionado.Text = "Todos";
            //}
            //else
            //{
            //    ltrNovaPesquisa.Text = "<a href=\"ProdutosClientesAprovados.aspx\" class=\"novaPesquisa\" style=\"float: right;\">nova pesquisa</a>";
            //    ltrCertificadoSelecionado.Text = new ImportacaoCertificadoBo().Find(lbda => lbda.ID_CERTIFICADO == idCertificado.Value).First().NOME;
            //}

            ltrEmpresaSelecionada.Text = string.IsNullOrWhiteSpace(strCliente) ? "Todos" : strCliente;
            ltrEstadoSelecionado.Text = string.IsNullOrWhiteSpace(strEstadoSigla) ? "Todos" : strEstadoSigla;
            ltrPaisSelecionado.Text = string.IsNullOrWhiteSpace(strPais) ? "Todos" : strPais;
            ltrProdutoSelecionado.Text = string.IsNullOrWhiteSpace(strProduto) ? "Todos" : strProduto;

            CarregarPaginacao();
        }

        private void CarregarPaginacao()
        {
            string formattedUrl = Request.Url.ToString().Replace("&pagina=" + nrPagina, "");
            //Descobrindo o total de páginas
            int totalPaginas = 1;

            if (totalRegistros > 20)
                totalPaginas = (totalRegistros / 20) + 1;

            //Se o total de página for igual a 1, não precisa exibir o paginador
            if (totalPaginas == 1)
                pnlPaginador.Visible = false;

            //Somente se o total de páginas for MAIOR que 1, o paginador será exibido
            else if (totalPaginas > 1)
            {
                pnlPaginador.Visible = true;

                if (nrPagina == 1)
                    ltrAnterior.Text = "<a href='#a' class='anterior inativa' name='a'>anterior</a>";
                else
                    ltrAnterior.Text = string.Format("<a href=\"{0}&pagina={1}\" class=\"anterior\">anterior</a>", formattedUrl, nrPagina - 1);

                int nrPaginadores = (nrPagina - 5) < 0 ? 1 : (nrPagina - 5);
                ltrPaginas.Text = "";
                while (nrPaginadores <= (nrPagina + 5) && nrPaginadores <= totalPaginas)
                {
                    ltrPaginas.Text += string.Format("<a href=\"{0}&pagina={1}\" class=\"bt\">{1}</a>", formattedUrl, nrPaginadores);
                    nrPaginadores++;
                }

                if (totalPaginas == nrPagina)
                    ltrProxima.Text = "<a href='#a' class='proxima inativa' name='a'>próxima</a>";
                else
                    ltrProxima.Text = string.Format("<a href=\"{0}&pagina={1}\" class=\"proxima\">próxima</a>", formattedUrl, nrPagina + 1);
            }

            ltrContagemTopo.Text = ltrContagemPaginacao.Text = string.Format("página {0}/{1} - listando {2}/{3}",
                nrPagina
                , totalPaginas
                , ((nrPagina * 20) - 20) + _objResult.Count
                , totalRegistros);
        }

        protected void dtRestultadoPesquisa_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            IMPORTACAO_CLIENTE _objCliente = (IMPORTACAO_CLIENTE)e.Item.DataItem;

            ((Literal)e.Item.FindControl("ltrNomeCliente")).Text = !string.IsNullOrWhiteSpace(_objCliente.APELIDO_FANTASIA) ? _objCliente.APELIDO_FANTASIA : string.Empty;
            ((Literal)e.Item.FindControl("ltrMatricula")).Text = !string.IsNullOrWhiteSpace(_objCliente.MATRICULA) ? _objCliente.MATRICULA : string.Empty;
            ((Literal)e.Item.FindControl("ltrIdCliente")).Text = string.Format("<a href=\"#\" rel=\"#janela_{0}\">", _objCliente.ID_CLIENTE);
        }

        protected void dtModais_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            IMPORTACAO_CLIENTE _objCliente = (IMPORTACAO_CLIENTE)e.Item.DataItem;

            //MODAL
            ((Literal)e.Item.FindControl("ltrModalIdCliente")).Text = string.Format("<div class=\"apple_overlay\" id=\"janela_{0}\">", _objCliente.ID_CLIENTE);
            ((Literal)e.Item.FindControl("ltrModalMatricula")).Text = !string.IsNullOrWhiteSpace(_objCliente.MATRICULA) ? _objCliente.MATRICULA : string.Empty;
            ((HtmlControl)e.Item.FindControl("linkPrint")).Attributes.Add("onClick", string.Format("window.open('ClienteResultadoPesquisaImpressao.aspx?CLIENTE={0}');$('.close').click();", _objCliente.MATRICULA));
            ((Literal)e.Item.FindControl("ltrModalNome")).Text = !string.IsNullOrWhiteSpace(_objCliente.APELIDO_FANTASIA) ? _objCliente.APELIDO_FANTASIA : string.Empty;
            ((Literal)e.Item.FindControl("ltrModalEndereco")).Text = "<div><span>Endereço</span><p>" + StringEndereco(ref _objCliente) + "</p></div>";

            if (!string.IsNullOrWhiteSpace(_objCliente.TELEFONE))
                ((Literal)e.Item.FindControl("ltrModalTelefone")).Text = "<div><span>Telefone</span><p>" + (!string.IsNullOrWhiteSpace(_objCliente.DDI) ? "+" + _objCliente.DDI + " " : " ") + _objCliente.TELEFONE + "</p></div>";

            if (!string.IsNullOrWhiteSpace(_objCliente.SITE))
                ((Literal)e.Item.FindControl("ltrModalSite")).Text = "<div><span>Site</span><p>" + _objCliente.SITE + "</p></div>";

            if (!string.IsNullOrWhiteSpace(_objCliente.EMAIL))
                ((Literal)e.Item.FindControl("ltrModalEmail")).Text = "<div><span>E-mail</span><p>" + _objCliente.EMAIL + "</p></div>";

            DataList dtContatos = ((DataList)e.Item.FindControl("dtContatos"));
            dtContatos.ItemDataBound += new DataListItemEventHandler(dtContatos_ItemDataBound);
            dtContatos.DataSource = new ImportacaoContatoBo().Find(lbda => lbda.ID_CLIENTE == _objCliente.ID_CLIENTE).OrderBy(lbda => lbda.NOME);
            dtContatos.DataBind();
            dtContatos.Visible = (dtContatos.Items.Count > 0);
            
            List<IMPORTACAO_PRODUTO> _objResult = new HTMSiteAdminEntities().GetProdutosVinculados(true, _objCliente.ID_CLIENTE).OrderBy(lbda => lbda.NOME).ToList();
            List<strProdutoViewModel> _objProdutoViewModelCollection = new List<strProdutoViewModel>();

            if (_objResult != null && _objResult.Count > 0)
            {
                foreach (var item in _objResult)
                {
                    _objProdutoViewModelCollection.Add(new strProdutoViewModel() { ID_CLIENTE = _objCliente.ID_CLIENTE, ID_PRODUTO = item.ID_PRODUTO, NOME = item.NOME });
                }
            }

            ((DataList)e.Item.FindControl("dtProdutos")).ItemDataBound += new DataListItemEventHandler(dtProdutos_ItemDataBound);
            ((DataList)e.Item.FindControl("dtProdutos")).DataSource = _objProdutoViewModelCollection;
            ((DataList)e.Item.FindControl("dtProdutos")).DataBind();

            _objResult = null; _objProdutoViewModelCollection = null;
        }

        private string StringEndereco(ref IMPORTACAO_CLIENTE _objCliente)
        {
            string strRetorno = "";

            if (!string.IsNullOrWhiteSpace(_objCliente.ENDERECO))
                strRetorno += _objCliente.ENDERECO;

            if (!string.IsNullOrWhiteSpace(_objCliente.MUNICIPIO))
                strRetorno += ", " + _objCliente.MUNICIPIO;

            if (!string.IsNullOrWhiteSpace(_objCliente.ESTADO_SIGLA))
                strRetorno += "/" + _objCliente.ESTADO_SIGLA;

            if (!string.IsNullOrWhiteSpace(_objCliente.CEP))
                strRetorno += " - CEP " + _objCliente.CEP;

            if (!string.IsNullOrWhiteSpace(_objCliente.PAIS))
                strRetorno += " - " + _objCliente.PAIS;

            return strRetorno;
        }

        protected void dtContatos_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (!(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
                return;

            IMPORTACAO_CONTATO _objContato = (IMPORTACAO_CONTATO)e.Item.DataItem;
            string strContato = "";

            if (!string.IsNullOrWhiteSpace(_objContato.NOME))
                strContato += _objContato.NOME;

            if (!string.IsNullOrWhiteSpace(_objContato.TELEFONE))
                strContato += " - Telefone: " + _objContato.TELEFONE;

            if (!string.IsNullOrWhiteSpace(_objContato.EMAIL))
                strContato += " - E-Mail: " + _objContato.EMAIL;

            ((Literal)e.Item.FindControl("ltrContato")).Text = strContato;
        }

        protected void dtProdutos_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            strProdutoViewModel _objProduto = (strProdutoViewModel)e.Item.DataItem;

            if (!string.IsNullOrWhiteSpace(_objProduto.NOME))
                ((Literal)e.Item.FindControl("ltrNomeProduto")).Text = "<div><span style=\"color: #d29d2b;\">" + _objProduto.NOME + ":</span><p>";

            List<IMPORTACAO_CERTIFICADO> _objCertificadoCollection = new HTMSiteAdminEntities().GetCertificadosBy_IdCliente_IdProduto(_objProduto.ID_CLIENTE, _objProduto.ID_PRODUTO).ToList();

            if (_objCertificadoCollection != null)
            {
                _objCertificadoCollection.ForEach(delegate(IMPORTACAO_CERTIFICADO _objCertificado)
                {
                    if (!string.IsNullOrWhiteSpace(_objCertificado.NOME))
                        ((Literal)e.Item.FindControl("ltrCertificados")).Text += _objCertificado.NOME + ";&nbsp;&nbsp;";
                });

                ((Literal)e.Item.FindControl("ltrCertificados")).Text += "</p></div>";
            }
        }
    }
}