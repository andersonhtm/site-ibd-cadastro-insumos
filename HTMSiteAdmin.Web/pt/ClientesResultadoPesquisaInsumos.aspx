﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="ClientesResultadoPesquisaInsumos.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.ClientesResultadoPesquisaInsumos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <link rel="stylesheet" type="text/css" href="media/css/estilos.css" />
    <script type='text/javascript' src='media/js/jquery.js'></script>
    <script type='text/javascript' src='media/js/jquery.tools.min.js'></script>
    <script type='text/javascript' src='media/js/interna.js'></script>
    <script type='text/javascript' src='media/js/util.js'></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $(".itemNews a:odd").css("color", "#797949");
            $(".itemNews a:odd").hover(function () { $(this).css("color", "#809e25"); }, function () { $(this).css("color", "#797949"); });
            $('.listaResultados a:last').css('border', 'none')
            $(".listaResultados a").overlay({ mask: { color: '#6d5e16', loadSpeed: 600, opacity: 0.7 }, effect: 'apple' })
            $('.listaResultados a').click(function () { $('img:last').attr('src', 'media/img/fundoJanela.png'); $('.aba.selecaopadrao').click(); })
            $('.abas a').click(function () {
                $('.abas a').removeClass('ativo');
                $(this).addClass('ativo');
                $('.produtosDiretrizes, .empresaTexto').hide();
                aba = $(this).attr('href');
                if (aba == 'empresaTexto') $('.' + aba).show();
                if (aba == 'produtosDiretrizes') $('.' + aba).show();

                return false;
            })
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Clientes</span><br />
                Clientes e Insumos Aprovados
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Resultados</h2>
            <a href="#">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="certificadosResultados">
        <div class="resultadosCabecalho">
            <p style="color: #595941; font-size: 15px; letter-spacing: -1px;">
                Veja abaixo a lista de de empresas e produtos encontrados.</p>
            <h3 style="float: left; width: 300px; font-size: 25px;">
                Sua pesquisa</h3>
            <div style="overflow: hidden; width: 300px; float: right; margin: 10px 0px 10px 0px;">
                <a href="InsumosClientesAprovados.aspx" class="novaPesquisa" style="float: right;">nova pesquisa</a>
                <a href="ClienteResultadoPesquisaImpressao.aspx?<%=QueryStringToPrintPage %>" target="_blank" class="imprimir" style="">imprimir todos</a>
            </div>
            <div style="clear: both; height: 2px;">
                <!-- -->
            </div>
            <div class="tabs">
               <%-- <div>
                    <span>certificado</span>
                    <p>
                        <asp:Literal ID="ltrCertificadoSelecionado" Text="ltrCertificadoSelecionado" runat="server" /></p>
                </div>--%>
                <div>
                    <span>insumo</span>
                    <p>
                        <asp:Literal ID="ltrProdutoSelecionado" Text="ltrProdutoSelecionado" runat="server" /></p>
                </div>
                <div>
                    <span>empresa</span>
                    <p>
                        <asp:Literal ID="ltrEmpresaSelecionada" Text="ltrEmpresaSelecionada" runat="server" /></p>
                </div>
                <div>
                    <span>categoria</span>
                    <p><asp:Literal ID="ltrCategoria" Text="" runat="server" /></p>
                </div>
                <div>
                    <span>finalidade de uso</span>
                    <p><asp:Literal ID="ltrFinalidade" Text="" runat="server" /></p>
                </div>
            </div>
        </div>
        <!--resultadosCabecalho-->
        <div class="resultadosPesquisa">
            <div class="topoResultados">
                <h3>
                    Resultados Pesquisa</h3>
                <div>
                    clique sobre a empresa para mais informações <span>
                        <asp:Literal ID="ltrContagemTopo" Text="ltrContagemTopo" runat="server" /></span></div>
            </div>
            <!--topoResultados-->
            <div class="listaResultados">
                <asp:DataList runat="server" ID="dtRestultadoPesquisa" RepeatLayout="Table" ShowFooter="False"
                    ShowHeader="False" OnItemDataBound="dtRestultadoPesquisa_ItemDataBound" style="width:100%;">
                    <ItemTemplate>
                        <asp:Literal ID="ltrIdCliente" Text="ltrIdCliente" runat="server" />
                        <span class="tx">
                            <asp:Literal ID="ltrNomeCliente" Text="ltrNomeCliente" runat="server" />&nbsp;
                            <em>(<asp:Literal ID="ltrMatricula" Text="" runat="server" />)</em>
                        </span></a>
                    </ItemTemplate>
                </asp:DataList>
            </div>
            <!--listaResultados-->
            <div class="paginacao">
                <span>
                    <asp:Literal ID="ltrContagemPaginacao" Text="ltrContagemPaginacao" runat="server" /></span>
                <asp:Panel runat="server" ID="pnlPaginador">
                    <div class="nav">
                        <asp:Literal ID="ltrAnterior" Text="ltrAnterior" runat="server" />
                        <asp:Literal ID="ltrPaginas" Text="ltrPaginas" runat="server" />
                        <asp:Literal ID="ltrProxima" Text="ltrProxima" runat="server" />
                    </div>
                </asp:Panel>
            </div>
        </div>
        <!--resultadosPesquisa-->
    </div>
    <!--certificadosResultados-->
</asp:Content>
