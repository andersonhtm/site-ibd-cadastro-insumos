﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Conteudos;
using System.Web.UI.HtmlControls;

namespace HTMSiteAdmin.Web.pt
{
    public partial class ClientesDetalhes : System.Web.UI.Page
    {
        private int? idConteudo
        {
            get
            {
                try { return Convert.ToInt32(Request.QueryString["id_conteudo"]); }
                catch (Exception) { return null; }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (idConteudo.HasValue && idConteudo.Value > 0)
            {
                var conteudos = new ConteudoBo().Find(c => c.ID_CONTEUDO == idConteudo.Value && c.ID_CONTEUDO_TIPO == 3);

                if (conteudos.Count() == 0)
                {
                    return;
                }

                CONTEUDO _objConteudo = conteudos.FirstOrDefault();

                // caso inativo, adiciona "noindex,nofollow" para "robots"
                if (_objConteudo.ID_CONTEUDO_SITUACAO == 2)
                {
                    Header.Controls.Add(new HtmlMeta { Name = "robots", Content = "noindex, nofollow" });
                    return;
                }

                if (_objConteudo.DATA_PUBLICACAO.HasValue)
                {
                    ltrDia.Text = _objConteudo.DATA_PUBLICACAO.Value.Day.ToString();
                    ltrMes.Text = HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objConteudo.DATA_PUBLICACAO.Value.Month);
                    ltrDataPublicacao.Text = _objConteudo.DATA_PUBLICACAO.Value.ToShortDateString();
                }
                else
                {
                    ltrDia.Text = _objConteudo.SESSAO.DATA_INICIO.Day.ToString();
                    ltrMes.Text = HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objConteudo.SESSAO.DATA_INICIO.Month);
                    ltrDataPublicacao.Text = _objConteudo.SESSAO.DATA_INICIO.ToShortDateString();
                }

                ltrCategoria.Text = new ConteudoCategoriaBo().Find(c => c.ID_CONTEUDO_CATEGORIA == _objConteudo.ID_CONTEUDO_CATEGORIA).First().NOME;
                ltrTitulo.Text = _objConteudo.TITULO;
                ltrConteudo.Text = _objConteudo.CONTENT_DATA;
            }
        }
    }
}