﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="Clientes.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.Clientes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Clientes</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="noticias">
        <div>
            <p>
                Nesta seção conheça nossos clientes.</p>
        </div>
        <asp:DataList runat="server" ID="dtClientes" OnItemDataBound="dtClientes_ItemDataBound">
            <ItemTemplate>
                <div class="itemNoticias">
                    <div>
                        <asp:Literal ID="ltrLinkClienteTopo" Text="ltrLinkClienteTopo" runat="server" />
                    </div>
                    <div class="texto">
                        <div class="dataTipo">
                            <h4 style="margin-left: 0; padding-left: 0;">
                                <asp:Literal ID="ltrClienteTitulo" Text="ltrClienteTitulo" runat="server" /></h4>
                        </div>
                        <!--dataTipo-->
                        <h3>
                            <asp:Literal ID="ltrLinkClienteBot" Text="ltrLinkClienteBot" runat="server" /></h3>
                    </div>
                    <!--texto-->
                </div>
                <!--itemNoticias-->
            </ItemTemplate>
        </asp:DataList>
        <%--<div class="paginacao">
            <span style="color: #898975;">página 1/1</span>
            <div class="nav">
                <a class="bt" href="/Home/Clientes?page=1">1</a>
            </div>
            <!--nav-->
        </div>--%>
        <!--paginacao-->
    </div>
    <!--noticias-->
</asp:Content>
