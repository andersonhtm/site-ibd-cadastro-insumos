﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="Natrue.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.Natrue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Orgânico
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Natrue Cosmetics</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <a href="http://www.natrue.org">
                <img src="media/img/logo_natrue_ibd.png" alt="" style="float: right; margin-left: 15px;
                    margin-bottom: 10px;" />
            </a>
            <h2 class="tituloCertificaoInterno">
                Sobre o Selo</h2>
            <span style="font-weight: bold">Para cosméticos certificados pelo IBD, as regras são
                simples e fáceis de aplicar:</span>
            <br />
            <br />
            <span style="font-weight: bold">Orgânico:</span> com pelo menos 95% de ingredientes
            orgânicos.
            <br />
            <br />
            <span style="font-weight: bold">Feito com Ingredientes ou Materias Primas Orgânicas:</span>
            com pelo menos 70% de ingredientes orgânicos.
            <br />
            <br />
            <span style="font-weight: bold">Natural:</span> abaixo de 70% de ingredientes orgânicos.
            <br />
            <br />
            Esta é a tendência mais coerente: o consumidor de orgânicos é consciente e quer
            produtos realmente orgânicos, com uma porcentagem significativa de materias primas
            orgânicas!
            <br />
            <br />

            <h2 class="tituloCertificaoInterno">Passo a Passo</h2>
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=93114085-8a56-41fe-91f4-59758d7f3f32"> - Passo a Passo para certificação (Português)</a> <br />
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=40023888-bb5f-4877-b1e1-add1c50e695f"> - Passo a Passo para certificação (Inglês)</a>
            <br /><br />


            <h2 class="tituloCertificaoInterno">
                Diretrizes Atendidas</h2>
                <!-- <span class="itemNormas">Natrue</span> -->
                - Natrue
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segmentos Atendidos</h2>
            - Cosm&eacute;ticos<br />
            <br />
            <!--
            <h2 class="tituloCertificaoInterno">
                Informa&ccedil;&otilde;es Adicionais</h2>
            <span class="itemNormas">Diretrizes Cosm&#233;ticos Naturais e Org&#226;nicos (IBD)</span>
            <span class="itemNormas">Folder Cosméticos IBD</span> <span class="itemNormas">Certifica&ccedil;&atilde;o
                de Cosm&eacute;ticos Org&acirc;nicos</span> <span class="itemNormas">Mat&#233;ria Prima
                    para Cosm&#233;ticos Naturais e Org&#226;nicos (IBD)</span> <span class="itemNormas">
                        Comparativo sobre as certifica&ccedil;&otilde;es de Cosm&eacute;ticos IBD x Ecocert</span>
            <br />
            <br />
            <br /> -->
            <br />
        </p>
    </div>
    <!--quemsomos-->
</asp:Content>
