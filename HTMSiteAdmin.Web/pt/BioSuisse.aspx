﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="BioSuisse.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.BioSuisse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }

        .itemNormas {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }

        .limpaFloat {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Orgânico
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px; margin-left: 6px;" />
                Bio Suisse</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">

        <img src="media/img/logo_biosuisse.png" alt="Bio Suisse" style="float: right; margin-left: 15px; margin-bottom: 10px; margin-top: 10px" />

        <h2 class="tituloCertificaoInterno">Sobre o Selo</h2>
        Bio Suisse é uma organização privada dos produtores orgânicos suíços, cujo selo representa:
        <br />
        <br />
        - Manejo 100% orgânico da fazenda, inclusive a pecuária<br />
        - Diversidade natural na fazenda orgânica<br />
        - Proibição do desmatamento<br />
        - Manejo e alimentação etologicamente corretos na pecuária<br />
        - Não uso de pesticidas ou fertilizantes sintéticos<br />
        - Limites na fertilização em Nitrogênio e Fósforo<br />
        - Prevenção da erosão<br />
        - Obrigação de rotação com no mínimo 20 % de cultivos para melhoria do solo e 12 meses sem o mesmo cultivo<br />
        - Cobertura vegetal nos vinhedos e nos pomares<br />
        - Limites para uso de cobre<br />
        - Proibição de piretróides / bioherbicidas / reguladores de crescimento sintéticos<br />
        - Não uso de engenharia genética<br />
        - Não uso de aditivos alimentares desnecessários como corantes e aromatizantes<br />
        - Proibição do transporte aéreo<br />
        - Completa rastreabilidade, do consumidor até o produtor

        <br />
        <br />

        <h2 class="tituloCertificaoInterno">Bio Suisse</h2>
        Bio Suisse é o proprietário da marca registrada Bud (“broto”). Suas normas são diretrizes de direito privado e excedem os requisitos legais mínimos em aspectos essenciais 
        (Regulamento orgânico EU 834/2007 ou equivalente). Os produtores no exterior que cumprem os padrões necessários e que desejam fornecer seus produtos para a Suíça podem obter 
        a certificação Bio Suisse.
        <br />
        <br />

        <h2 class="tituloCertificaoInterno">Certificação Bio Suisse</h2>
        Uma certificação orgânica válida equivalente à Portaria Suiça sobre Agricultura Orgânica (por exemplo, Regulamento (CE) n.º 834/2007 ou NOP do Conselho) é a base para uma 
        certificação de acordo com os padrões da Bio Suisse no exterior. Em geral, um comprador / importador dos produtos na Suíça deve ser nomeado. 
        Ele irá suportar todos os custos da certificação Bio Suisse.
        <br />
        International Certification Bio Suisse AG (ICB), subsidiária da Bio Suisse, realiza a certificação. Informações detalhadas estão disponíveis em: <a href="www.icbag.ch">www.icbag.ch.</a>

        <br />
        <br />
        <h2 class="tituloCertificaoInterno">Procedimento</h2>

        IBD Certificações Ltda. oferece uma inspeção Bio Suisse junto com a inspeção orgânica regular. Entre em contato conosco se desejar uma inspeção Bio Suisse, além da inspeção orgânica. 
        Estamos á sua disposição para ajudá-lo com quaisquer perguntas.
        
        <br />
        <br />
        <h2 class="tituloCertificaoInterno">Diretrizes Atendidas</h2>
        - Bio Suisse          
        
        <br />
        <br />
        <h2 class="tituloCertificaoInterno">Segmentos Atendidos</h2>
        - Agricultura
        <br />
        - Pecuária 
        <br />
        - Processamento de alimentos 
        <br />
        - Extrativismo 
        <br />

        <br />
        <br />
        <br />
        <br />
    </div>
</asp:Content>
