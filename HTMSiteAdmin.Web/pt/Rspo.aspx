﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="Rspo.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.Rspo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Socio-Ambiental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px;
                    margin-right: 6px; margin-left: 6px;" />
                RSPO</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <img src="media/img/logoCertificadoRSPO.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                Sobre o Selo</h2>
            O RSPO (Roundtable on Sustainable Palm Oil) está crescendo rapidamente em todo o mundo (veja portfolio de membros em www.rspo.org ) , com 
            participação das principais empresas transnacionais que consomem óleo de palma a nível global. IBD é a  única certificadora Brasileira e com acreditação 
            RSPO e assim, qualificada a oferecer serviços de auditoria para este escopo de certificação globalmente. 
            <br />
            <br />
            Para empresas produtoras de frutos e óleos de palma, processadores de óleo e seus derivados assim como as empresas comerciantes destes produtos o IBD 
            oferece um pacote de certificação baseado nas normas de Princípios e Critérios  (P&C- Principles and Criteria) ou na Certificação de Cadeia de Custodia 
            (SCC-Supply Chain Certification). <br /><br />
            
            Para America Latina, Africa e Asia: Favor contatar Leonardo Gomes em leonardo@ibd.com.br <br />
            Para Europa: Favor contatar Bernhard Schulz em ceres@ceres.com.br . Ceres é a parte auditora do IBD para Alemanha e Europa. <br /><br />

            Favor encontre abaixo o folder eletrônico IBD-RSPO para baixar.<br />
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=920d2f1f-a16e-4bd3-8c22-7a7bf2a32671">Folder IBD-RSPO</a><br />
        </p>

            <h2 class="tituloCertificaoInterno">Passo a Passo</h2>
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=a0183378-f10e-4e35-8c3f-f9c73f5f8119"> - Passo a Passo para certificação (Português)</a><br />
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=55e1f784-742b-4a45-b7ae-c97e96495177"> - Passo a Passo para certificação (Inglês)</a><br />
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=8d540a92-95be-46a2-93fa-6ec8cc4ca01b"> - Passo a Passo para certificação (Espanhol)</a>
            <br /><br />

            <h2 class="tituloCertificaoInterno">
                Princ&iacute;pios</h2>
            - Responsabilidade Ambiental e Conservação dos Recursos Naturais da Biodiversidade;<br />
            - Comprometimento com a Transparência;<br />
            - Uso das Melhores práticas nas áreas agrícolas e Industriais;<br />
            - Cumprimento das Leis e Normas Aplicáveis;<br />
            - Desenvolvimento responsável de novas áreas de plantio;<br />
            - Comprometimento com a viabilidade Financeira e Econômica de longo prazo;<br />
            - Responsabilidade com os colaboradores, indivíduos e comunidades afetadas pelas
            plantações e usinas;<br />
            - Comprometimento com a melhoria contínua.
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Informa&ccedil;&otilde;es Adicionais</h2>
            <p>
                Para maiores informações acesse os arquivos abaixo.<br />
                <br />
                <a href="DiretrizesLegislacao.aspx" class="itemNormas">Diretrizes e Legislação</a>
                <!-- 
                <span class="itemNormas">Roteiro para Certificação RSPO</span> <span class="itemNormas">
                    Diretrizes de Certificação RSPO</span> -->
                <br />
                <br />
            </p>
            <br />
            <h2 class="tituloCertificaoInterno">
                Clientes Certificados
            </h2>

            <asp:Repeater ID="rpt" runat="server">
                <HeaderTemplate>
                    <ul class="list">
                </HeaderTemplate>
                <ItemTemplate>                    
                    <li><a href="<%# Eval("Caminho") %>" class="imagem"><%# Eval("Descricao")%></a></li>                    
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
            <br />
            <br />
    </div>
    <!--quemsomos-->
</asp:Content>
