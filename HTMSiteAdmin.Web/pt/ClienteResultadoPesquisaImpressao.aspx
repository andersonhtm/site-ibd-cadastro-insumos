﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClienteResultadoPesquisaImpressao.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.ClienteResultadoPesquisaImpressao" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>:: IBD - Relação de Clientes ::</title>

    <style type="text/css">
    <!--

    .titulo {
	    font-family: Calibri, "Trebuchet MS", Arial, Helvetica, sans-serif;
	    font-weight: bold;
	    font-size: 20px;
    }

    .rodape {
	    font-family: Calibri, "Trebuchet MS", Arial, Helvetica, sans-serif;
	    font-size: 13px;
    }

    body {
	    font-family: Calibri, "Trebuchet MS", Arial, Helvetica, sans-serif;
	    font-size: 13px;
    }

    -->

    </style>


</head>
<body>
    <form id="form1" runat="server">


    <table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td align="left" valign="top"><img src="media/img/logo_ibd_impressao.jpg" /></td>
            <td align="right" valign="top" class="titulo">
                RELAÇÃO DE CLIENTES<br />
            </td>
      </tr>
      <tr>
        <td colspan="2">
            <hr />
        </td>
      </tr>
        
      <tr>
        <td colspan="2">
            <asp:Repeater runat="server" ID="rptClientes" onitemdatabound="rptClientes_ItemDataBound">
                <HeaderTemplate>
                    <p>
                </HeaderTemplate>
            
                <ItemTemplate>
                    Matrícula: <strong><%# Eval("MATRICULA") %></strong><br />
                    Empresa: <strong><%# Eval("APELIDO_FANTASIA")%></strong><br /><br />

                    <span runat="server" id="spanEndereco" visible='<%# !String.IsNullOrEmpty(Eval("ENDERECO").ToString()) %>'>
                        Endereço: <%# Eval("ENDERECO")%>, <%# Eval("MUNICIPIO")%> / <%# Eval("ESTADO_SIGLA")%> - <%# Eval("CEP")%> - <%# Eval("PAIS")%><br />
                    </span>
                    <span runat="server" id="spanTelefone" visible='<%# !String.IsNullOrEmpty(Eval("TELEFONE").ToString()) %>'>
                        Telefone: <%# (!string.IsNullOrWhiteSpace(Eval("DDI").ToString()) ? "+" + Eval("DDI").ToString() + " " : " ") + Eval("TELEFONE")%><br />
                    </span>

                    <asp:Repeater ID="rptContatos" runat="server">
                        <HeaderTemplate>
                            <br />
                            <strong>Contatos:</strong>
                            <br />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# Eval("NOME") %> <br />
                            <span runat="server" id="spanTelefone" visible='<%# !String.IsNullOrEmpty((Eval("TELEFONE")+"").ToString()) %>'>
                                Telefone: <%# Eval("TELEFONE")%>
                            </span>
                            <span runat="server" id="spanEmail" visible='<%# !String.IsNullOrEmpty(Eval("EMAIL").ToString()) %>'>
                                E-mail: <%# Eval("EMAIL")%>
                            </span>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <br />
                        </AlternatingItemTemplate>
                        <FooterTemplate>
                            <br />
                        </FooterTemplate>
                    </asp:Repeater>

                    <asp:Repeater ID="rptProdutos" runat="server" onitemdatabound="rptProdutos_ItemDataBound">
                        <HeaderTemplate>
                            <br />
                            <strong>Produto(s) e Diretriz(es):</strong>
                            <br /><br />
                        </HeaderTemplate>
                        <SeparatorTemplate>
                            <br />
                        </SeparatorTemplate>
                        <ItemTemplate>
                            <u><%# Eval("NOME") %> </u>

                            <asp:Repeater ID="rptCertificados" runat="server">
                                <HeaderTemplate><br />Esquema(s): </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("NOME") %>
                                </ItemTemplate>
                                <SeparatorTemplate>; </SeparatorTemplate>
                            </asp:Repeater>

                            <asp:Repeater ID="rptCategoria" runat="server">
                                <HeaderTemplate><br /> Categoria(s): </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("DESCRICAO") %>
                                </ItemTemplate>
                                <SeparatorTemplate>; </SeparatorTemplate>
                            </asp:Repeater>

                            <asp:Repeater ID="rptFinalidade" runat="server">
                                <HeaderTemplate><br />Finalidade(s) de Uso:</HeaderTemplate>
                                <ItemTemplate>
                                    <%# Eval("DESCRICAO") %>
                                </ItemTemplate>
                                <SeparatorTemplate>; </SeparatorTemplate>
                            </asp:Repeater>

                            <asp:Literal runat="server" ID="ltrComentario" Text='<%# "<br />Permissão de Uso: " + (Eval("COMENTARIO")+"").ToString() %>'
                                Visible='<%# !String.IsNullOrEmpty((Eval("COMENTARIO")+"").ToString()) %>' />

                            <asp:Literal runat="server" ID="ltrRestricao" Text='<%# "<br />Não Permitido para uso de acordo com: " + (Eval("RESTRICAO")+"").ToString() %>'
                                Visible='<%# !String.IsNullOrEmpty((Eval("RESTRICAO")+"").ToString()) %>' />
                        
                            <br />
                        </ItemTemplate>
                    </asp:Repeater>

                </ItemTemplate>
                <SeparatorTemplate>
                    <hr />            
                </SeparatorTemplate>
                <FooterTemplate>
                    <hr />
                    </p>
                </FooterTemplate>
            </asp:Repeater>

        </td>
      </tr>
    </table>

    </form>

    <script language="javascript" type="text/javascript">
        try {
            window.print();
        } 
        catch (e) { }
    </script>
</body>
</html>
