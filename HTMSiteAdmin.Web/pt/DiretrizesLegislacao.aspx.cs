﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Business.Downloads;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Links;

namespace HTMSiteAdmin.Web.pt
{
    public partial class DiretrizesLegislacao : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var ctx = new HTMSiteAdminEntities())
            {
                dtDiretrizesCategorias.DataSource = ctx.GetCategoriasComDownload().Where(x => x.ATIVO && x.ID_DOWNLOAD_CATEGORIA != 18).OrderBy(x => x.DESCRICAO);
                dtDiretrizesCategorias.DataBind();

                dtLinksInteressantes.DataSource = ctx.GetCategoriasLinksInteressantes().Where(x => x.ID_LINK_CATEGORIA == 9).OrderBy(x => x.DESCRICAO);
                dtLinksInteressantes.DataBind();
            }
        }
        protected void dtLinksInteressantes_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            ((DataList)e.Item.FindControl("dtLinks")).ItemDataBound += new DataListItemEventHandler(dtLinks_ItemDataBound);
            ((DataList)e.Item.FindControl("dtLinks")).DataSource = new LinkBo().Find(lbda => lbda.ID_LINK_TIPO == 1 && lbda.ID_LINK_CATEGORIA == ((LINK_CATEGORIA)e.Item.DataItem).ID_LINK_CATEGORIA).OrderBy(order => order.DESCRICAO);
            ((DataList)e.Item.FindControl("dtLinks")).DataBind();
        }

        protected void dtDiretrizesCategorias_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            ((Literal)e.Item.FindControl("ltrCategoria")).Text = ((DOWNLOAD_CATEGORIA)e.Item.DataItem).DESCRICAO;
            ((DataList)e.Item.FindControl("dtDownloads")).ItemDataBound += new DataListItemEventHandler(dtDownloads_ItemDataBound);
            ((DataList)e.Item.FindControl("dtDownloads")).DataSource = new DownloadBo().Find(lbda => lbda.ID_DOWNLOAD_TIPO == 1 && lbda.ID_DOWNLOAD_CATEGORIA == ((DOWNLOAD_CATEGORIA)e.Item.DataItem).ID_DOWNLOAD_CATEGORIA && lbda.ATIVO).OrderBy(x => new { x.ORDEM, x.DESCRICAO });
            ((DataList)e.Item.FindControl("dtDownloads")).DataBind();
        }

        void dtDownloads_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            string det = "<div id=\"desc\">"+((DOWNLOAD)e.Item.DataItem).DESCRICAO_DETALHADA+"</div>";
            ((Literal)e.Item.FindControl("ltrLink")).Text = "<a target='_blank' href=\"" + HTMSiteAdmin.Library.Util.GetServerPath(this.Page) + string.Format("/ShowFile.aspx?action=1&fileid={0}", ((DOWNLOAD)e.Item.DataItem).ID_ARQUIVO_DIGITAL.ToString()) + "\">" + ((DOWNLOAD)e.Item.DataItem).DESCRICAO + (!string.IsNullOrEmpty(det) ? "" + det: "") + "</a>";
        }

        void dtLinks_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            ((Literal)e.Item.FindControl("ltrLink")).Text = "<a target='_blank' href=\"" + ((LINK)e.Item.DataItem).CAMINHO + "\" class=\"link\">" + ((LINK)e.Item.DataItem).DESCRICAO + "</a>";                    
        }
    }
}