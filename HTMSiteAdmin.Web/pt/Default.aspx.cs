﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Business.Localizacao;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Banners;
using System.Net.Mail;
using System.Text;
using System.Net;
using System.Configuration;
using HTMSiteAdmin.Web.Classes;

namespace HTMSiteAdmin.Web.pt
{
    public partial class Default : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["FRONT_END_TOKEN"] == null) ViewState["FRONT_END_TOKEN"] = Guid.NewGuid().ToString(); return ViewState["FRONT_END_TOKEN"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this.Page, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this.Page, ViewToken, "ViewAspect", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }

            CarregarBanner();
        }

        private void CarregarBanner()
        {
            var _objBanner1 = new BannerBo().Find(lbda => lbda.ID_BANNER_POSICAO == 1 && lbda.ID_BANNER_SITUACAO == 1).First();
            var _objBanner2 = new BannerBo().Find(lbda => lbda.ID_BANNER_POSICAO == 2 && lbda.ID_BANNER_SITUACAO == 1).First();
            var _objBanner3 = new BannerBo().Find(lbda => lbda.ID_BANNER_POSICAO == 3 && lbda.ID_BANNER_SITUACAO == 1).First();
            var _objBanner4 = new BannerBo().Find(lbda => lbda.ID_BANNER_POSICAO == 4 && lbda.ID_BANNER_SITUACAO == 1).First();
            var _objBanner5 = new BannerBo().Find(lbda => lbda.ID_BANNER_POSICAO == 5 && lbda.ID_BANNER_SITUACAO == 1).First();
            var _objBanner6 = new BannerBo().Find(lbda => lbda.ID_BANNER_POSICAO == 6 && lbda.ID_BANNER_SITUACAO == 1).First();

            //IMAGENS MAIORES
            ltrLinkImagemBanner1.Text = "<a href=\"" + _objBanner1.CAMINHO + "\"><img src=\"" + ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", _objBanner1.ID_ARQUIVO)) + "\" alt=\"\" width=\"618\" height=\"287\" /></a>";
            ltrLinkImagemBanner2.Text = "<a href=\"" + _objBanner2.CAMINHO + "\"><img src=\"" + ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", _objBanner2.ID_ARQUIVO)) + "\" alt=\"\" width=\"618\" height=\"287\" /></a>";
            ltrLinkImagemBanner3.Text = "<a href=\"" + _objBanner3.CAMINHO + "\"><img src=\"" + ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", _objBanner3.ID_ARQUIVO)) + "\" alt=\"\" width=\"618\" height=\"287\" /></a>";

            //BANNERS INFERIORES
            ltrBannerInferior4.Text = "<a href=\"" + _objBanner4.CAMINHO.Replace("http://ibd.htm.com.br/en/", "") + "\"><img src=\"" + ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", _objBanner4.ID_ARQUIVO)) + "\" alt=\"\" width=\"300\" height=\"85\" /></a>";
            ltrBannerInferior5.Text = "<a href=\"" + _objBanner5.CAMINHO.Replace("http://ibd.htm.com.br/en/", "") + "\"><img src=\"" + ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", _objBanner5.ID_ARQUIVO)) + "\" alt=\"\" width=\"142\" height=\"85\" /></a>";
            ltrBannerInferior6.Text = "<a href=\"" + _objBanner6.CAMINHO.Replace("http://ibd3.htm.com.br/en/", "") + "\"><img src=\"" + ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", _objBanner6.ID_ARQUIVO)) + "\" alt=\"\" width=\"142\" height=\"85\" /></a>";

            //MINIATURAS
            ltrMiniaturaBanner1.Text = "<img src=\"" + ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", _objBanner1.ID_MINIATURA)) + "\" width=\"105\" height=\"66\" />";
            ltrMiniaturaBanner2.Text = "<img src=\"" + ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", _objBanner2.ID_MINIATURA)) + "\" width=\"105\" height=\"66\" style=\"float:left\" />";
            ltrMiniaturaBanner3.Text = "<img src=\"" + ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", _objBanner3.ID_MINIATURA)) + "\" width=\"105\" height=\"66\" style=\"float:left\" />";

            //TÍTULOS
            ltrTituloBanner1.Text = _objBanner1.TITULO;
            ltrTituloBanner2.Text = _objBanner2.TITULO;
            ltrTituloBanner3.Text = _objBanner3.TITULO;

            //SUB-TÍTULOS
            ltrSubTituloBanner1.Text = _objBanner1.SUB_TITULO;
            ltrSubTituloBanner2.Text = _objBanner2.SUB_TITULO;
            ltrSubTituloBanner3.Text = _objBanner3.SUB_TITULO;
        }

        public void ValidateAcess() { throw new NotImplementedException(); }

        public void MapButtons() { throw new NotImplementedException(); }

        public void UserInterfaceAspectChanged()
        {
            StartPageControls();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    break;
                case enmUserInterfaceAspect.New:
                    break;
                case enmUserInterfaceAspect.Update:
                    break;
            }
        }

        public void StartPageControls()
        {
            try
            {

            }
            catch (Exception ex) { throw; }
        }

        public void RestartForm() { throw new NotImplementedException(); }

        public void ClearMemory() { throw new NotImplementedException(); }

        public void ClearFields() { throw new NotImplementedException(); }

        public void EnableFields() { throw new NotImplementedException(); }

        public void DisableFields() { throw new NotImplementedException(); }

        public void ButtonNew_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonSave_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonSaveNew_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonRefresh_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonCancel_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonDelete_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void GetForm() { throw new NotImplementedException(); }

        public void SaveView() { throw new NotImplementedException(); }

        public void SetForm() { throw new NotImplementedException(); }

        protected void btnInsumosAprovadosBuscar_Click(object sender, EventArgs e)
        {

        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder corpoEmail = new StringBuilder();
                corpoEmail.AppendLine("Atendimento: CADASTRE-SE<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("Data Hora: " + DateTime.Now.ToString() + "<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("Nome: " + txtCadastreSeNome.Text + "<br />");
                corpoEmail.AppendLine("E-mail: " + txtCadastreSeEmail.Text + "<br />");
                Util.EnviarEmail(corpoEmail.ToString(), "IBD Certificações - Cadastre-se", Configuracao.REGISTER_EMAILTO, "", Configuracao.MAIL_BLINDCOPYTO);
               
                corpoEmail = new StringBuilder();
                corpoEmail.AppendLine("Prezado (a): " + txtCadastreSeNome.Text + "<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("Você se cadastrou no site do IBD para receber informações.<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("Data: " + DateTime.Now.ToString() + "<br />");
                corpoEmail.AppendLine("E-mail: " + txtCadastreSeEmail.Text + "<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("Atenciosamente,<br />");
                corpoEmail.AppendLine("<br />");
                corpoEmail.AppendLine("Equipe IBD<br />");
                corpoEmail.AppendLine(Configuracao.REGISTER_EMAILREPLY + "<br />");
                Util.EnviarEmail(corpoEmail.ToString(), "IBD Certificações - Cadastre-se", txtCadastreSeEmail.Text, "", Configuracao.MAIL_BLINDCOPYTO);

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "cadastresealert", "alert('E-mail cadastrado com sucesso.');", true);
                txtCadastreSeEmail.Text = txtCadastreSeNome.Text = "";
            }
            catch (Exception ex) { 
                throw ex; 
            }
        }
    }
}