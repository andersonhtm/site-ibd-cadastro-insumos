﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="Bonsucro.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.Bonsucro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Socio-Ambiental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px;
                    margin-right: 6px; margin-left: 6px;" />
                Bonsucro</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <img src="media/img/logoCertificadoBonsucro.png" alt="" style="float: right; margin-left: 15px;
            margin-bottom: 10px;" />
        <h2 class="tituloCertificaoInterno">
            Sobre o Selo</h2>
        Bonsucro é uma associação multi-stakeholder criada com o objetivo de reduzir os
        impactos ambientais e sociais da produção de cana-de-açúcar, através do desenvolvimento
        de um padrão e um programa de certificação para transformar a indústria da cana.
        Esta é uma iniciativa das indústrias e o Bonsucro tem trabalhado para reunir todos
        os stakeholders e desenvolver um meio para alcançar seus objetivos dentro do mercado
        sucroalcoleiro. O WWF acompanhou e apóia este protocolo.<br />
        <br />
        A diretriz padrão Bonsucro foi criada com objetivo de fornecer um mecanismo para
        alcançar a produção sustentável da cana-de-açúcar (e todos os seus produtos) em
        relação aos aspectos econômicos, sociais e ambientais da atividade. Incorpora um
        conjunto de Princípios, Critérios, Indicadores e Verificadores que serão usados
        para certificar os produtores de açúcar que atendam estas exigências e orienta as
        empresas que desejam adquirir matéria-prima sustentável / suprimentos, e também
        o setor financeiro na realização de investimentos sustentáveis.
        <br />
        <br />
        <h2 class="tituloCertificaoInterno">
            Critérios</h2>
        O padrão é baseado em um conjunto de medidas do sistema métrico que permite a agregação,
        e uma demonstração mais clara do impacto causado pela atividade. A unidade de certificação
        será a usina de açúcar e as auditorias serão baseadas em avaliações da usina e área
        de fornecimento de cana.<br />
        <br />
        <h2 class="tituloCertificaoInterno">
            Como se certificar</h2>
        Para que uma empresa venha a obter o certificado Bonsucro pelo IBD deve primeiramente
        registrar-se como membro Bonsucro (www.bonsucro.com), e atender as seguintes etapas
        no processo de certificação:<br />
        <br />
        - Pré-auditoria: Etapa opcional para que o empreendimento consiga visualizar em
        qual o estágio se encontra de acordo com as normas Bonsucro e o que deverá fazer
        para obter a certificação;<br />
        <br />
        - Auditoria documental: Etapa prévia a auditoria na unidade de produção. Tem como
        objetivo verificar o cumprimento da empresa às normas Bonsucro, através da verificação
        da documentação solicitada para análise.<br />
        <br />
        - Auditoria na unidade de produção: De três em três anos será feita uma auditoria
        completa pelo IBD na unidade de processamento e produção de cana-de-açúcar e no
        intervalo de dois anos serão feitas auditorias anuais complementares para verificar
        a rastreabilidade e o cumprimento das não conformidades identificadas no ano anterior.
        Em situações excepcionais de não cumprimento às normas Bonsucro a empresa receberá
        uma auditoria completa durante este intervalo.<br />
        <br />
        <h2 class="tituloCertificaoInterno">
            Informa&ccedil;&otilde;es Adicionais</h2>
        Clientes IBD produtores de açúcar e demais produtos da cana-de-açúcar orgânica terão
        acesso a orçamentos diferenciados e este programa se destina também a produtores
        convencionais de cana-de-açúcar.<br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </div>
    <!--quemsomos-->
</asp:Content>
