﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Importacao;
using System.Web.UI.WebControls;

namespace HTMSiteAdmin.Web.pt
{
    public partial class ClienteResultadoPesquisaImpressao : Page
    {
        #region :: Properties ::

        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }

        private bool insumo
        {
            get
            {
                string str = Request.QueryString["INSUMO"];
                return (str == "1");
            }
        }

        private int? idCertificado
        {
            get
            {
                try { return int.Parse(Request.QueryString["ID_CERTIFICADO"].ToString()); }
                catch (Exception) { return 0; }
            }
        }

        private int? idCategoria
        {
            get
            {
                try { return int.Parse(Request.QueryString["ID_CATEGORIA"].ToString()); }
                catch (Exception) { return 0; }
            }
        }

        private int? idFinalidade
        {
            get
            {
                try { return int.Parse(Request.QueryString["ID_FINALIDADE"].ToString()); }
                catch (Exception) { return 0; }
            }
        }

        private string strProduto
        {
            get
            {
                try { return Request.QueryString["PRODUTO"].ToString(); }
                catch (Exception) { return null; }
            }
        }
        private string strCliente
        {
            get
            {
                try { return Request.QueryString["CLIENTE"].ToString(); }
                catch (Exception) { return null; }
            }
        }
        private string strPais
        {
            get
            {
                try
                {
                    if (Request.QueryString["PAIS"].ToString().Equals("0"))
                        return null;
                    else
                        return Request.QueryString["PAIS"].ToString();
                }
                catch (Exception) { return null; }
            }
        }
        private string strEstadoSigla
        {
            get
            {
                try
                {
                    if (Request.QueryString["ESTADO_SIGLA"].ToString().Equals("0"))
                        return null;
                    else
                        return Request.QueryString["ESTADO_SIGLA"].ToString();
                }
                catch (Exception) { return null; }
            }
        }
        #endregion

        public List<IMPORTACAO_CLIENTE> Clientes { get; set; }
        IMPORTACAO_CLIENTE cliente = null;
         
        protected void Page_Load(object sender, EventArgs e)
        {
            var container = new HTMSiteAdminEntities();

            Clientes = new HTMSiteAdminEntities().ClientesCertificados(
                !insumo
                , idCertificado == 0 ? null : idCertificado
                , strProduto == null ? null : strProduto.Replace("'", "")
                , strCliente == null ? null : strCliente.Replace("'", "")
                , strPais == null ? null : strPais.Replace("'", "")
                , strEstadoSigla == null ? null : strEstadoSigla.Replace("'", "") 
                , false, idCategoria, idFinalidade)
                .ToList();
            
            rptClientes.DataSource = Clientes;
            rptClientes.DataBind();
        }

        protected void rptClientes_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (!(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
                return;

            cliente = ((IMPORTACAO_CLIENTE)e.Item.DataItem);

            Repeater rptContatos = e.Item.FindControl("rptContatos") as Repeater;
            Repeater rptProdutos = e.Item.FindControl("rptProdutos") as Repeater;

            List<IMPORTACAO_CONTATO> contatos = new ImportacaoContatoBo().Find(lbda => lbda.ID_CLIENTE == cliente.ID_CLIENTE).OrderBy(lbda => lbda.NOME).ToList();
            List<IMPORTACAO_PRODUTO> produtos = new HTMSiteAdminEntities().GetProdutosVinculados(!insumo, cliente.ID_CLIENTE).OrderBy(lbda => lbda.NOME).ToList();

            if (contatos.Count > 0)
            {
                rptContatos.DataSource = contatos;
                rptContatos.DataBind();
            }

            if (produtos.Count > 0)
            {
                rptProdutos.DataSource = produtos;
                rptProdutos.DataBind();
            }
        }

        protected void rptProdutos_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (!(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
                return;

            IMPORTACAO_PRODUTO produto = (e.Item.DataItem as IMPORTACAO_PRODUTO);

            // Certificados
            Repeater rptCertificados = e.Item.FindControl("rptCertificados") as Repeater;
            List<IMPORTACAO_CERTIFICADO> certificados = new HTMSiteAdminEntities().GetCertificadosBy_IdCliente_IdProduto(cliente.ID_CLIENTE, produto.ID_PRODUTO).ToList();
            if (certificados .Count > 0)
            {
                rptCertificados.DataSource = certificados;
                rptCertificados.DataBind();
            }

            // Categoria
            List<IMPORTACAO_PRODUTO_CATEGORIAPRODUTO> catprd = new ImportacaoProdutoCategoriaBo().Find(x => x.ID_PRODUTO == produto.ID_PRODUTO).ToList();
            List<IMPORTACAO_CATEGORIAPRODUTO> cat = new ImportacaoCategoriaProdutoBo().GetAll().ToList();

            Repeater rptCategoria = e.Item.FindControl<Repeater>("rptCategoria");
            List<IMPORTACAO_CATEGORIAPRODUTO> categoria = cat.Where(x=> catprd.Exists(y=> y.ID_CATEGORIAPRODUTO == x.ID_CATEGORIAPRODUTO)).ToList();

            if (categoria.Count > 0)
            {
                rptCategoria.DataSource = categoria.ToList();
                rptCategoria.DataBind();
            }

            // Finalidade
            List<IMPORTACAO_PRODUTO_FINALIDADEUSO> finprd = new ImportacaoProdutoFinalidadeBo().Find(x => x.ID_PRODUTO == produto.ID_PRODUTO).ToList();
            List<IMPORTACAO_FINALIDADEUSO> fin = new ImportacaoFinalidadeUsoBo().GetAll().ToList();

            Repeater rptFinalidade = e.Item.FindControl<Repeater>("rptFinalidade");
            var finalidade = from x in fin where finprd.Exists(y=> y.ID_FINALIDADE == x.ID_FINALIDADE)
                             select new { DESCRICAO = string.Format("{0} » {1}", x.IMPORTACAO_FINALIDADEUSO2 == null ? "" : x.IMPORTACAO_FINALIDADEUSO2.DESCRICAO, x.DESCRICAO) };

            if (finalidade.Count() > 0)
            {
                rptFinalidade.DataSource = finalidade.ToList();
                rptFinalidade.DataBind();
            }
        }
    }
}