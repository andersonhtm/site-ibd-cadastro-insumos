﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="SejaUmCliente.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.SejaUmCliente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Cliente</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="clientes">
        <p>
            <img src="media/img/fotoCliente.jpg" alt="" width="266" height="166" style="float: right;
                margin-left: 15px; margin-bottom: 10px;" />Com uma equipe especializada, com
            vasta experiência no mercado de certificações, oferecemos atendimento personalizado,
            com protocolos adequados às suas necessidades.<br />
            <br />
            Entre em contato com nosso atendimento, para obter maiores informações e receber
            os documentos para iniciar seu processo de certificação. Teremos prazer em atendê-lo.
        </p>
        <div style="clear: both;">
            <!-- -->
        </div>
        <div class="motivos">
            <h3>
                Conheça 10 motivos para se tornar cliente IBD:</h3>
            <div class="itens">
                <span>1</span>
                <p>
                    Em todo o mundo e também no Brasil o IBD goza de um nome ligado ao profissionalismo,
                    seriedade e dedicação. Seus produtos serão aceitos globalmente com certificados
                    IBD.</p>
            </div>
            <div class="itens">
                <span>2</span>
                <p>
                    Além de ser a maior certificadora para produtos orgânicos na América Latina estendeu
                    seus serviços a protocolos afins e de sustentabilidade e recentemente obteve credenciamento
                    para RSPO, UEBT e BONSUCRO, atestando a vanguarda e profissionalismo.
                </p>
            </div>
            <div class="itens">
                <span>3</span>
                <p>
                    O selo Fair Trade IBD de adequação social, ambiental e comercio ético para produtos orgânicos.</p>
            </div>
            <div class="itens">
                <span>4</span>
                <p>
                    Os orçamentos são claros, transparentes e permitem planejamento completo do cliente
                    para investimento, estrutura e prazos.
                </p>
            </div>
            <div class="itens">
                <span>5</span>
                <p>
                    Cerca de 90% dos projetos certificados pelo IBD são de agricultores familiares.
                    Sua estrutura permite a prática de preços acessíveis e diferenciados, adequados
                    à situação econômica de cada produtor.</p>
            </div>
            <div class="itens">
                <span>6</span>
                <p>
                    O IBD tem a maior rede de inspetores, agora regionalizados, conferindo rapidez e
                    preço mais acessível. A rede de inspetores continua crescendo, estando presentes
                    na maioria dos estados brasileiros.
                </p>
            </div>
            <div class="itens">
                <span>7</span>
                <p>
                    Conta com uma rede de mais de 50 inspetores em mais de 20 países para certificação
                    de produtos a serem importados ao Brasil pela lei Brasileira, 10.831 para produtos
                    orgânicos.
                </p>
            </div>
            <div class="itens">
                <span>8</span>
                <p>
                    Forma os seus inspetores com cursos internos anualmente com palestras de especialistas
                    e com conteúdos técnicos de auditoria, o que confere ao inspetor do IBD um nível
                    técnico superior.
                </p>
            </div>
            <div class="itens">
                <span>9</span>
                <p>
                    Foi a pioneira na incorporação de questões sociais, baseadas nos acordos internacionais
                    do trabalho, que serviram como modelo para outras certificadoras no exterior.</p>
            </div>
            <div class="itens">
                <span>10</span>
                <p>
                    Foi a pioneira a garantir a proteção das matas e dos recursos hídricos nos projetos
                    certificados. A consequência foi o replantio de dezenas de milhares de essências
                    nativas.
                </p>
            </div>
        </div>
        <!--motivos-->
    </div>
    <!--quemsomos-->
</asp:Content>
