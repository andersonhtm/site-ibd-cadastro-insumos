﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="ServicosAprovacoesInsumos.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.ServicosAprovacoesInsumos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Servi&ccedil;os
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Aprova&ccedil;&atilde;o de Insumos</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <img src="media/img/imagem_interna_02.jpg" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <p>
                O IBD Certificações criou o Programa de Aprovação de Insumos no intuito de avaliar
                a possibilidade de uso dos insumos comerciais disponíveis no mercado de acordo com
                as principais diretrizes de produção orgânica (Normas EUA, Européia, IFOAM, Japonesa,
                Canadense e Brasileira). Possui uma diretriz e procedimentos próprios e únicos a
                nível mundial que garantem segurança, credibilidade e confiabilidade aos insumos
                aprovados e aos produtores e empresas interessadas no uso.<br />
                <br />
                Como forma de garantir maior credibilidade a este programa o IBD conquistou em 2009
                o credenciamento ISO 65 junto a IOAS, sendo um dos poucos programas de certificação
                de insumos a nível mundial a possuir este credenciamento.<br />
                <br />
                Através deste Programa os produtores empresas passam a ter uma importante ferramenta
                de consulta para identificar os principais insumos para suas atividades, garantindo
                sustentabilidade e o sucesso na produção orgânica.<br />
                <br />
                O Programa de Aprovação avalia os insumos de acordo com as normas de produção agrícola,
                processamento de alimentos e pecuária orgânica, sendo destinado a fabricantes, importadores
                e distribuidores de insumos localizados no Brasil e no exterior.</p>
            <br />
            <br />
            <br />
            <br />
            <br />
    </div>
    <!--quemsomos-->
</asp:Content>
