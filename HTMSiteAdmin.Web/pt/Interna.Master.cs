﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Localizacao;

namespace HTMSiteAdmin.Web.pt
{
    public partial class Interna : System.Web.UI.MasterPage, IPagina
    {
        private string ViewToken { get { if (ViewState["FRONT_END_TOKEN"] == null) ViewState["FRONT_END_TOKEN"] = Guid.NewGuid().ToString(); return ViewState["FRONT_END_TOKEN"].ToString(); } }
        private enmUserInterfaceAspect ViewAspect
        {
            get { return (enmUserInterfaceAspect)Global.RestoreObject(this.Page, ViewToken, "ViewAspect"); }
            set { Global.SaveObject(this.Page, ViewToken, "ViewAspect", value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewAspect = enmUserInterfaceAspect.Start;
                UserInterfaceAspectChanged();
            }
        }

        public void ValidateAcess() { throw new NotImplementedException(); }

        public void MapButtons() { throw new NotImplementedException(); }

        public void UserInterfaceAspectChanged()
        {
            StartPageControls();

            switch (ViewAspect)
            {
                case enmUserInterfaceAspect.Start:
                    break;
                case enmUserInterfaceAspect.New:
                    break;
                case enmUserInterfaceAspect.Update:
                    break;
            }
        }

        public void StartPageControls()
        {
            try
            {
                if (!IsPostBack)
                {

                }
            }
            catch (Exception ex) { throw; }
        }

        public void RestartForm() { throw new NotImplementedException(); }

        public void ClearMemory() { throw new NotImplementedException(); }

        public void ClearFields() { throw new NotImplementedException(); }

        public void EnableFields() { throw new NotImplementedException(); }

        public void DisableFields() { throw new NotImplementedException(); }

        public void ButtonNew_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonSave_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonSaveNew_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonRefresh_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonCancel_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void ButtonDelete_Click(object sender, ImageClickEventArgs e) { throw new NotImplementedException(); }

        public void GetForm() { throw new NotImplementedException(); }

        public void SaveView() { throw new NotImplementedException(); }

        public void SetForm() { throw new NotImplementedException(); }

        protected void btnInsumosAprovadosBuscar_Click(object sender, EventArgs e)
        {

        }
    }
}