﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Configuration;
using HTMSiteAdmin.Library.UserInterface;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Sistema;
using System.IO;
using HTMSiteAdmin.Web.Classes;

namespace HTMSiteAdmin.Web.pt
{
    public partial class TrabalheConosco : System.Web.UI.Page
    {
        private string ViewToken { get { if (ViewState["ViewToken"] == null) ViewState["ViewToken"] = Guid.NewGuid().ToString(); return ViewState["ViewToken"].ToString(); } }
        public UploadedFile ArquivoEnviado
        {
            get { return (UploadedFile)Global.RestoreObject(this, ViewToken, "ArquivoEnviado"); }
            set { Global.SaveObject(this, ViewToken, "ArquivoEnviado", value); }
        }

        protected void Page_Load(object sender, EventArgs e) { }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                #region [ E-Mail para a IBD ]

                StringBuilder CorpoEmail = new StringBuilder(); // Corpo da Mensagem, conteudo da variavel criada acima

                CorpoEmail.AppendLine("Atendimento: Fale Conosco<br />");
                CorpoEmail.AppendLine("<br />");
                CorpoEmail.AppendLine("Data Hora: " + DateTime.Now.ToString() + "<br />");
                CorpoEmail.AppendLine("<br />");
                CorpoEmail.AppendLine("Nome: " + txtNome.Text + "<br />");
                CorpoEmail.AppendLine("E-mail: " + txtEmail.Text + "<br />");
                CorpoEmail.AppendLine("Celular: " + txtCelular.Text + "<br />");
                CorpoEmail.AppendLine("Endereço: " + txtEndereco.Text + "<br />");
                CorpoEmail.AppendLine("Município: " + txtMunicipio.Text + "<br />");
                CorpoEmail.AppendLine("Estado: " + txtEstado.Text + "<br />");
                CorpoEmail.AppendLine("País: " + txtPais.Text + "<br />");
                CorpoEmail.AppendLine("<br />");
                CorpoEmail.AppendLine("Comentários: " + txtComentarios.Text + "<br />");
                CorpoEmail.AppendLine("<br />");

                if (afuAnexo.HasFile)
                {
                    ARQUIVO_DIGITAL _objArquivoDigital = new ARQUIVO_DIGITAL()
                    {
                        ID_ARQUIVO = ArquivoEnviado.FileId,
                        NOME = "CV-" + txtNome.Text,
                        EXTENSAO = Path.GetExtension(afuAnexo.FileName).Replace(".", ""),
                        //ARQUIVO = afuAnexo.FileBytes
                    };

                    string path = Server.MapPath(ConfigurationManager.AppSettings["RELATIVEPATH_ARQUIVODIGITAL"]);
                    HTMSiteAdmin.Library.Util.SaveFile(path, _objArquivoDigital.ID_ARQUIVO.ToString(), _objArquivoDigital.EXTENSAO, afuAnexo.FileBytes);

                    ArquivoDigitalBo _objBo = new ArquivoDigitalBo();
                    _objBo.Add(_objArquivoDigital);
                    _objBo.SaveChanges();

                    _objBo = null;

                    string link = string.Format("{0}/ShowFile.aspx?action=2&fileid={1}", GetWebAppRoot(), ArquivoEnviado.FileId);
                    CorpoEmail.AppendFormat("<a href='{0}'>Clique aqui para acessar o currículo</a><br />", link);
                    CorpoEmail.AppendFormat("Caso não consiga visualizar este link, copie o seguinte endereço em seu navegador:<br /> {0}", link);
                }

                CorpoEmail.AppendLine("<br />");

                HTMSiteAdmin.Web.Classes.Util.EnviarEmail(CorpoEmail.ToString(),
                string.Format("[Trabalhe Conosco] {0}", txtNome.Text),
                Configuracao.CV_EMAILTO.ToString(), "", Configuracao.MAIL_BLINDCOPYTO);

                #endregion

                #region [ E-Mail para o usuário ]

                StringBuilder CorpoEmailUsuario = new StringBuilder(); // Corpo da Mensagem

                CorpoEmailUsuario.AppendLine("Prezado (a): " + txtNome.Text + "<br />");
                CorpoEmailUsuario.AppendLine("<br />");
                CorpoEmailUsuario.AppendLine("Você enviou seu currículo para o IBD Certificações.<br />");
                CorpoEmailUsuario.AppendLine("<br />");
                CorpoEmailUsuario.AppendLine("Em breve retornaremos.<br />");
                CorpoEmailUsuario.AppendLine("<br />");
                CorpoEmailUsuario.AppendLine("Atenciosamente,<br />");
                CorpoEmailUsuario.AppendLine("<br />");
                CorpoEmailUsuario.AppendLine("Equipe IBD<br />");
                CorpoEmailUsuario.AppendLine(Configuracao.CV_EMAILREPLY + "<br />");

                HTMSiteAdmin.Web.Classes.Util.EnviarEmail(CorpoEmailUsuario.ToString(),
                string.Format("[Trabalhe Conosco IBD] {0}", txtNome.Text),
                txtEmail.Text, "", Configuracao.MAIL_BLINDCOPYTO);

                #endregion

                Response.Redirect("~/pt/Default.aspx", true);
            }
            catch (Exception ex) { 
                throw ex; 
            }
        }

        private string GetWebAppRoot()
        {
            string host = (HttpContext.Current.Request.Url.IsDefaultPort) ?
                HttpContext.Current.Request.Url.Host :
                HttpContext.Current.Request.Url.Authority;
            host = String.Format("{0}://{1}", HttpContext.Current.Request.Url.Scheme, host);
            if (HttpContext.Current.Request.ApplicationPath == "/")
                return host;
            else
                return host + HttpContext.Current.Request.ApplicationPath;
        }

        protected void afuAnexo_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e) { ArquivoEnviado = HTMSiteAdmin.Library.UserInterface.WebControlsUtility.AsyncFileUpload_UploadedComplete(sender, e); }

    }
}