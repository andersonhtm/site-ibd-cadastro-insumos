﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="Verificacao4c.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.Verificacao4c" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Socio-Ambiental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px;
                    margin-right: 6px; margin-left: 6px;" />
                4C</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
       
            <img src="media/img/logoCertificado4C.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                Sobre o Selo</h2>

                O café 4C é produzido de acordo com o Código de Conduta 4C, um conjunto de práticas e princípios básicos de sustentabilidade para a produção de grãos de café verde. O cumprimento do Código de Conduta da 4C pode ser demonstrado através do Sistema de Verificação 4C e das Licenças 4C emitidas. A responsabilidade de assegurar a conformidade com o Código de Conduta da 4C através do funcionamento do Sistema de Verificação 4C pertence à Coffee Assurance Services GmbH & Co. KG (CAS).
        <br /><br />
O Código de Conduta 4C aplica-se a qualquer tipo de entidade produtora (Unidades 4C) baseada em qualquer país produtor de café que queira produzir e vender café 4C. O termo Unidade 4C é inclusivo e abrange qualquer tipo de instalação e / ou processo de produção: pode ser um grupo de pequenos agricultores, uma cooperativa ou uma associação de agricultores, uma estação de coleta, uma usina, um comerciante local, uma organização exportadora, ou mesmo um torrefador (desde que seja baseado em um país onde o café é produzido). Existem dois pré-requisitos para se qualificar como uma Unidade 4C:
        <br /><br />
- Ser capaz de fornecer um mínimo de um Container de café verde (20 toneladas);<br />
- Ter uma pessoa ou um grupo de pessoas que possa assegurar a implementação do Código de Conduta da 4C. O Sistema 4C denomina esta como Entidade Gestora.<br />
        <br />
A verificação da adesão aos princípios do Código de Conduta da 4C deve ser realizada por uma empresa de auditoria, que por sua vez é aprovada pela Coffee Assurance Services. Uma lista de Empresas Verificadoras aprovadas 4C pode ser encontrada na página da Coffee Assurance Services (<a href="http://www.cas-veri.com/" target="_blank">www.cas-veri.com</a>). A abordagem “nível de entrada” do Código de Conduta da 4C permite aos produtores de café de todo o mundo embarcar em sua jornada de sustentabilidade. A natureza inclusiva do Código de Conduta da 4C tem como objetivo chegar aos produtores que atualmente não estão participando do mercado de café sustentável e colocá-los em conformidade com um nível básico de sustentabilidade. A intenção é aumentar gradualmente as condições sociais, econômicas e ambientais da produção e processamento do café em todo o mundo.
        <br /><br />
Para alcançar isso, o Código de Conduta 4C compreende:
        <br /><br />
- 10 Práticas Inaceitáveis que devem ser excluídas antes de solicitar a Verificação 4C.<br />
- 27 princípios em todas as dimensões econômicas, sociais e ambientais. Esses princípios baseiam-se em boas práticas agrícolas e de gestão, bem como em convenções internacionais e diretrizes reconhecidas aceitas no setor cafeeiro;<br />
        <br />
Quanto às três dimensões da sustentabilidade, o Código de Conduta 4C engloba os seguintes princípios:
        <br /><br />
<b>8 Princípios Econômicos:</b>
<br /><br />
- 3 princípios sobre a cafeicultura como negócio (1.1 - 1.3);<br />
- 5 princípios sobre o apoio aos agricultores pela Entidade Gestora de uma Unidade 4C (1.4 – 1.8).<br />
<br />
<b>9 Princípios Sociais:</b>
<br /><br />
- 2 princípios aplicáveis a todos os agricultores e outros sócios de negócio (2.1 - 2.2);<br />
- 7 princípios aplicáveis aos trabalhadores (2.3 - 2.9).<br />
<br />
<b>10 princípios Ambientais, que compreendem:</b>
<br /><br />
- 6 princípios aplicáveis aos recursos naturais (3.1, 3.4, 3.6 - 3.9);<br />
- 3 princípios aplicáveis aos agroquímicos (3.2, 3.3, 3.5);<br />
- 1 princípio aplicável à energia (3.10).<br />
<br />
            
            <h2 class="tituloCertificaoInterno">
                Informações</h2>            
                A Coffee Assurance Services também compilou um conjunto abrangente de perguntas freqüentes (FAQs) que cobrem a maioria dos aspectos do sistema de Verificação 4C. Estes estão disponíveis no site da CAS (<a href="http://www.cas-veri.com/" target="_blank">www.cas-veri.com</a>). Para obter mais detalhes ou esclarecimentos, entre em contato com a Coffee Assurance Services pelo e-mail <a href="mailto:info@cas-veri.com">info@cas-veri.com</a>
    </div>
    <!--quemsomos-->
</asp:Content>
