﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Importacao;

namespace HTMSiteAdmin.Web.pt.UserControls
{
    public partial class ucInsumosAprovados : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (HTMSiteAdminEntities _ctx = new HTMSiteAdminEntities())
                {
                    ddlCertificado.DataSource = _ctx.GetCertificadosInsumosComClientes();
                    ddlCertificado.DataBind();
                }

                ddlCategoria.DataSource = (from x in (new ImportacaoCategoriaProdutoBo().GetAll())
                                           select new
                                           {
                                               ID_CATEGORIAPRODUTO = x.ID_CATEGORIAPRODUTO,
                                               DESCRICAO = /*x.IMPORTACAO_CATEGORIAPRODUTO2.DESCRICAO + " » " + */x.DESCRICAO
                                           }).ToList().OrderBy(x => x.DESCRICAO);
                ddlCategoria.DataBind();
                ddlCategoria.Items.Insert(0, new ListItem("Todas", "0"));

                ddlFinalidade.DataSource = (from x in (new ImportacaoFinalidadeUsoBo().GetAll())
                                            select new
                                            {
                                                ID_FINALIDADE = x.ID_FINALIDADE,
                                                DESCRICAO = x.DESCRICAO
                                            }).ToList().OrderBy(x => x.DESCRICAO);
                ddlFinalidade.DataBind();
                ddlFinalidade.Items.Insert(0, new ListItem("Todas", "0"));

            }
        }

        protected void btnClienteCertificadoBuscar_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("ClientesResultadoPesquisaInsumos.aspx?ID_CERTIFICADO={0}&PRODUTO={1}&CLIENTE={2}&ID_CATEGORIA={3}&ID_FINALIDADE={4}",
                ddlCertificado.SelectedValue
                , txtInsumo.Text
                , txtClienteCertificadoCliente.Text
                , ddlCategoria.SelectedValue
                , ddlFinalidade.SelectedValue));
        }

        protected void ddlCertificado_DataBound(object sender, EventArgs e)
        {
            if (((DropDownList)sender).Items.FindByValue("0") == null)
                ((DropDownList)sender).Items.Insert(0, new ListItem("Todos", "0"));
        }
    }
}