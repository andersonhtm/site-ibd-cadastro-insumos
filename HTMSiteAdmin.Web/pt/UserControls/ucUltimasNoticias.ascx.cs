﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Business.Conteudos;
using HTMSiteAdmin.Data;

namespace HTMSiteAdmin.Web.pt.UserControls
{
    public partial class ucUltimasNoticias : System.Web.UI.UserControl
    {
        IQueryable<CONTEUDO> _objConteudoCollection;

        protected void Page_Load(object sender, EventArgs e)
        {
            _objConteudoCollection = new ConteudoBo().Find(c => c.ID_CONTEUDO_TIPO == 1 && (!c.DATA_PUBLICACAO.HasValue || (c.DATA_PUBLICACAO.HasValue && c.DATA_PUBLICACAO.Value <= DateTime.Now))).OrderByDescending(lbda => lbda.DATA_PUBLICACAO);

            if (_objConteudoCollection.Count() >= 5)
                dtUltimasNoticias.DataSource = _objConteudoCollection.ToList().GetRange(0, 5);
            else
                dtUltimasNoticias.DataSource = _objConteudoCollection.ToList();

            dtUltimasNoticias.DataBind();
        }

        protected void dtUltimasNoticias_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (((CONTEUDO)e.Item.DataItem).DATA_PUBLICACAO.HasValue)
                ((Literal)e.Item.FindControl("ltrDiaNoticia")).Text = ((CONTEUDO)e.Item.DataItem).DATA_PUBLICACAO.Value.Day.ToString();
            else
                ((Literal)e.Item.FindControl("ltrDiaNoticia")).Text = DateTime.Now.Day.ToString();

            if (((CONTEUDO)e.Item.DataItem).DATA_PUBLICACAO.HasValue)
                ((Literal)e.Item.FindControl("ltrMesNoticia")).Text = HTMSiteAdmin.Library.UserInterface.Converters.MesString(((CONTEUDO)e.Item.DataItem).DATA_PUBLICACAO.Value.Month);
            else
                ((Literal)e.Item.FindControl("ltrMesNoticia")).Text = HTMSiteAdmin.Library.UserInterface.Converters.MesString(DateTime.Now.Month);

            ((Literal)e.Item.FindControl("ltrCategoria")).Text = new ConteudoCategoriaBo().Find(lbda => lbda.ID_CONTEUDO_CATEGORIA == ((CONTEUDO)e.Item.DataItem).ID_CONTEUDO_CATEGORIA).First().NOME;
            ((Literal)e.Item.FindControl("ltrLinkNoticia")).Text = string.Format("<a style=\"color: #797949;\" href=\"NoticiasDetalhes.aspx?id_conteudo={0}\">{1}</a>", ((CONTEUDO)e.Item.DataItem).ID_CONTEUDO.ToString(), ((CONTEUDO)e.Item.DataItem).TITULO);
        }
    }
}