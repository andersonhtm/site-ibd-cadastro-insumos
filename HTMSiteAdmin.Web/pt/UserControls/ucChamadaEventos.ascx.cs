﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Eventos;

namespace HTMSiteAdmin.Web.pt.UserControls
{
    public partial class ucChamadaEventos : System.Web.UI.UserControl
    {
        IQueryable<EVENTO> _objCollection;

        protected void Page_Load(object sender, EventArgs e)
        {
            _objCollection = new EventoBo().Find(c => (!c.DATA_PUBLICACAO.HasValue || (c.DATA_PUBLICACAO.HasValue && c.DATA_PUBLICACAO.Value <= DateTime.Now)) && (c.DATA_INICIO.HasValue && c.DATA_INICIO.Value >= DateTime.Now)).OrderBy(lbda => lbda.DATA_INICIO);

            if (_objCollection.Count() >= 2)
                dtUltimasNoticias.DataSource = _objCollection.ToList().GetRange(0, 2);
            else
                dtUltimasNoticias.DataSource = _objCollection.ToList();

            dtUltimasNoticias.DataBind();
        }

        protected void dtUltimasNoticias_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            ((Literal)e.Item.FindControl("ltrDiaNoticia")).Text = ((EVENTO)e.Item.DataItem).DATA_INICIO.Value.Day.ToString("00");
            ((Literal)e.Item.FindControl("ltrMesNoticia")).Text = HTMSiteAdmin.Library.UserInterface.Converters.MesString(((EVENTO)e.Item.DataItem).DATA_INICIO.Value.Month);
            ((Literal)e.Item.FindControl("ltrCategoria")).Text = new EventoCategoriaBo().Find(lbda => lbda.ID_EVENTO_CATEGORIA == ((EVENTO)e.Item.DataItem).ID_EVENTO_CATEGORIA).First().DESCRICAO;
            ((Literal)e.Item.FindControl("ltrLinkNoticia")).Text = string.Format("<a style=\"color: #797949;\" href=\"EventosFeirasDetalhes.aspx?id_conteudo={0}\">{1}</a>", ((EVENTO)e.Item.DataItem).ID_EVENTO.ToString(), ((EVENTO)e.Item.DataItem).DESCRICAO);
        }
    }
}