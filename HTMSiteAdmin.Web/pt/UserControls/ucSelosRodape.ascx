﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucSelosRodape.ascx.cs" Inherits="HTMSiteAdmin.Web.pt.UserControls.ucSelosRodape" %>
<div class="centro">
    <div class="selos">
        <div>
            <h3>Nossos Selos</h3>
        </div>
        <div class="selosCat">
            <div class="seloOrganico">
                <h4>Orgânico</h4>
                <a href="IbdOrganico.aspx">
                    <img src="media/img/selo1.jpg" alt="" />
                </a>
                <a href="IbdOrganico.aspx">
                    <img src="media/img/selo2.jpg" alt="" />
                </a>
                <a href="IbdOrganico.aspx">
                    <img src="media/img/selo3.jpg" alt="" />
                </a>
                <a href="UsdaOrganic.aspx">
                    <img src="media/img/selo4.jpg" alt="" />
                </a>
                <a href="JasOrganic.aspx">
                    <img src="media/img/selo5.jpg" alt="" />
                </a>
                <a href="IbdOrganico.aspx">
                    <img src="media/img/selo23.jpg" width="79" style="margin-top: 21px" alt="" />
                </a>
                <a href="Natrue.aspx">
                    <img src="media/img/selo7.jpg" alt="" />
                </a>
            </div>
            <!--seloOrganico-->
            <div class="seloBio">
                <h4>
                    Biodinâmico</h4>
                <a href="Demeter.aspx">
                    <img src="media/img/selo8.jpg" alt="" />
                </a>
            </div>
            <!--seloBio-->
            <div class="seloSocio">
                <h4>Sócio-Ambientais</h4>

                <a href="FairTradeIbd.aspx">
                    <img src="media/img/selo9.jpg" alt=""  style="margin-right: 10px" />
                </a>                
                <a href="Rspo.aspx">
                    <img src="media/img/logo-rspo.png" alt=""/>
                </a>
                <a href="Uebt.aspx">
                    <img src="media/img/selo13.jpg" alt="" />
                </a>
                <br />
                <a href="Ras.aspx"><img alt="" src="media/img/logo-rainforest.png" style="clear:left; margin-top:13px" /></a>
                <a href="Verificacao4c.aspx"><img alt="" src="media/img/logo-4c.png" style="margin-top:13px; margin-left:10px" /></a>
                <a href="Utz.aspx"><img alt="" src="media/img/logo-utz.png" style="margin-top:13px; margin-left:10px; margin-top: 2px" /></a>
            </div>
            <!--seloSocio-->
            <div class="seloAprovacoes">
                <h4>
                    Aprovações</h4>
                <a href="InsumoAprovadoIbd.aspx">
                    <img src="media/img/selo14.jpg" alt="" />
                </a><a href="IngredienteNaturalIbd.aspx">
                    <img src="media/img/selo15.jpg" alt="" />
                </a><a href="NaoOgmIbd.aspx">
                    <img src="media/img/selo16.jpg" alt="" />
                </a>
                <%--<a href="AgroturismoIbd.aspx">
                    <img src="media/img/selo17.jpg" alt="" />
                </a>--%>
            </div>
            <!--seloAprovacoes-->
        </div>
        <!--selosCat-->
    </div>
    <!--selos-->
    <div class="selos2">
        <h3>Acreditações</h3>

        <a href="Acreditadores.aspx?acreditadores#01">
            <img src="media/img/selo18.jpg" alt="" style="margin-top: 20px; width: 86px;" />
        </a>                      
        <a href="Acreditadores.aspx?acreditadores#04">
            <img src="media/img/selo22.jpg" alt="" style="width: 57px; padding: 10px; margin-left: 36px;" />
        </a>  
        <a href="Acreditadores.aspx?acreditadores">
            <img style="width: 109px; margin-top: 22px;" src="media/img/selo25.png" alt="" />
        </a>
        <a href="Acreditadores.aspx?acreditadores#02">
            <img src="media/img/selo19.jpg" alt="" />
        </a>
    </div>
    <!--selos2-->
</div>
<!--centro-->
