﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Importacao;
using HTMSiteAdmin.Business.Conteudos;

namespace HTMSiteAdmin.Web.pt.UserControls
{
    public partial class ucChamadaClientes : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            /*
            List<CONTEUDO> _objConteudoCollection = new ConteudoBo().Find(lbda => lbda.ID_CONTEUDO_TIPO == 3).ToList();
            int MaxRange = _objConteudoCollection.Count() - 1;
            int StartIndex = new Random().Next(0, MaxRange);

            if (StartIndex == MaxRange || StartIndex > MaxRange - 2)
                StartIndex = MaxRange - 2;
            */
            dtDiretrizesLegislacao.DataSource = new ConteudoBo().Find(lbda => lbda.ID_CONTEUDO_TIPO == 3).OrderBy(x=> Guid.NewGuid()).Take(2).ToList();
            dtDiretrizesLegislacao.DataBind();
        }

        protected void dtDiretrizesLegislacao_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            ((Literal)e.Item.FindControl("ltrCertificado")).Text = new ConteudoCategoriaBo().Find(lbda => lbda.ID_CONTEUDO_CATEGORIA == ((CONTEUDO)e.Item.DataItem).ID_CONTEUDO_CATEGORIA).First().NOME;
            ((Literal)e.Item.FindControl("ltrLinkCliente")).Text = string.Format("<a class='linkhover' href=\"ClientesDetalhes.aspx?id_conteudo={0}\">{1}</a>", ((CONTEUDO)e.Item.DataItem).ID_CONTEUDO, ((CONTEUDO)e.Item.DataItem).TITULO);
        }
    }
}