﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucTopMenu.ascx.cs" Inherits="HTMSiteAdmin.Web.pt.UserControls.ucTopMenu" %>
<div class="menu">
    <a href="/">Inicial</a><span></span> 
    <a href="QuemSomos.aspx">Quem Somos</a><span></span>
    <a href="servicos">servi&ccedil;os</a><span></span> 
    <a href="certificacoes">certifica&ccedil;&otilde;es</a><span></span>
    <a href="Acreditadores.aspx">Acreditadores</a><span></span> 
    <a href="clientes">clientes</a><span></span>
    <a href="biblioteca">biblioteca</a><span></span> 
    <a href="atendimento">atendimento</a>
</div>
<!--menu-->
<div class="subMenu" id="subServicos">
    <div class="organicos">
        <a href="ServicosCertificacoes.aspx">Certifica&#231;&#245;es</a>         
        <a href="ServicosCursosTreinamentos.aspx">Treinamentos e Cursos</a>
        <a href="ServicosMapeamentoGeoprocessamento.aspx">Mapas e Geoprocessamento</a>
       
    </div>
</div>
<!--subServicos-->
<div class="subMenu" id="subCliente">
    <div class="organicos">
        <a href="SejaUmCliente.aspx">Seja um Cliente</a> 
        <a href="ProdutosClientesAprovados.aspx"> Produtos e Clientes IBD</a> 
        <a href="InsumosClientesAprovados.aspx">Insumos e Clientes Aprovados</a>
        <a href="ProdutosClientesAprovadosEuropeu.aspx">Produtos e Clientes com a Diretriz EU</a>
    </div>
</div>
<!--subCliente-->
<div class="subMenu" id="subBiblioteca">
    <div class="organicos">
        <a href="Noticias.aspx">Not&#237;cias</a> 
        <a href="EventosFeiras.aspx">Eventos e Feiras</a>
        <a href="IBDNews.aspx">IBD News</a> 
        <a href="DiretrizesLegislacao.aspx">Diretrizes e Legisla&#231;&#227;o</a> 
        <a href="LinksInteressantes.aspx">Links Interessantes</a>
        <a href="ContratosDetalhes.aspx">Contrato</a>
    </div>
</div>
<!--subBiblioteca-->
<div class="subMenu" id="subAtendimento">
    <div class="organicos">
        <a href="CadastreSe.aspx">Cadastre-se</a> 
        <a href="OndeEstamos.aspx">Onde Estamos</a>
        <a href="FaleConosco.aspx">Fale Conosco</a> 
        <a href="TrabalheConosco.aspx">Trabalhe Conosco</a>
    </div>
</div>
<!--subAtendimento-->
<div class="subMenu" id="subCertificacoes">
    <div class="organicos">
        <a href="#">Certificação Orgânica e Afins</a>
        <div class="sub">
            <a href="IbdOrganico.aspx">IBD Org&#226;nico</a> 
            <a href="UsdaOrganic.aspx">USDA Organic</a>
            <a href="JasOrganic.aspx">JAS Organic</a> 
            <a href="CanadaOrganic.aspx">Canada Organic (Equivalência EUA/NOP)</a>
            <a href="Natrue.aspx">Natural and Organic Cosmetics (Natrue)</a>
            <a href="BioSuisse.aspx">Bio Suisse</a>
            <a href="Krav.aspx">KRAV</a>
            <a href="OrganicoCoreia.aspx">Orgânico Coréia</a>            
            <a href="InsumoAprovadoIbd.aspx">Insumo Aprovado IBD</a> 
        </div>
    </div>
    <%--<div class="biodinamicos">
    </div>--%>    
    <div class="aprovacoes">
        <a href="#">Certificações de Fair Trade, Socioambiental e de Sustentabilidade</a>
        <div class="sub">
            <a href="IngredienteNaturalIbd.aspx">Ingrediente Natural IBD</a> 
            <a href="NaoOgmIbd.aspx">N&#227;o OGM IBD</a> 
            <a href="FairTradeIbd.aspx">IBD Fair Trade</a>           
            <a href="Rspo.aspx">Roundtable on Sustainable Palm Oil (RSPO)</a> 
            <a href="Ras.aspx">Rainforest Alliance (RFA)</a> 
            <a href="Verificacao4c.aspx">Verificação 4C</a> 
            <a href="Uebt.aspx">Union for Ethical Bio Trade (UEBT)</a>
            <a href="Utz.aspx">UTZ</a>
            <a href="Sai.aspx">SAI PLATFORM</a>
            <a href="Iscc.aspx">ISCC</a>
        </div>
    </div>
    <div class="qualidade-certificada">
        <a href="#">Certificação de Procedimentos</a>
        <div class="sub">
            <a href="QualidadeCertificada.aspx">IBD Qualidade Certificada (glúten, NOGM, parâmetros vários)</a> 
        </div>
    </div>
    <div class="socioAmbientais">        
        <a href="#">Certificação Demeter</a>
        <div class="sub">
            <a href="Demeter.aspx">Demeter Internacional</a>
        </div>
        <%--<a href="AgroturismoIbd.aspx">AgroEcoTurismo IBD</a>--%>
    </div>
</div>
<!--subCertificacoes-->
