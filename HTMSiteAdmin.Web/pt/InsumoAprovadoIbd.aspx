﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="InsumoAprovadoIbd.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.InsumoAprovadoIbd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Diversas<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                Insumo Aprovado IBD</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <img src="media/img/logoCertificadoInsumosAprovados.png" alt="" style="float: right;
                margin-left: 15px; margin-bottom: 10px;" />
        </p>
        <h2 class="tituloCertificaoInterno">
            Sobre o Selo</h2>
        <p>
            O IBD Certifica&ccedil;&otilde;es criou o Programa de Aprova&ccedil;&atilde;o de
            Insumos no intuito de avaliar a possibilidade de uso dos insumos comerciais dispon&iacute;veis
            no mercado de acordo com as principais diretrizes de produ&ccedil;&atilde;o org&acirc;nica
            (Normas EUA, Europ&eacute;ia, IFOAM, Japonesa, Canadense, Brasileira e Demeter). Possui uma
            diretriz e procedimentos pr&oacute;prios e &uacute;nicos a n&iacute;vel mundial
            que garantem seguran&ccedil;a, credibilidade e confiabilidade aos insumos aprovados
            e aos produtores e empresas interessadas no uso.<br />
            <br />
            Como forma de garantir maior credibilidade a este programa o IBD conquistou em 2009
            o credenciamento ISO 65 junto a IOAS, sendo um dos poucos programas de certifica&ccedil;&atilde;o
            de insumos a n&iacute;vel mundial a possuir este credenciamento.<br />
            <br />
            Atrav&eacute;s deste Programa os produtores empresas passam a ter uma importante
            ferramenta de consulta para identificar os principais insumos para suas atividades,
            garantindo sustentabilidade e o sucesso na produ&ccedil;&atilde;o org&acirc;nica.<br />
            <br />
            O Programa de Aprova&ccedil;&atilde;o avalia os insumos de acordo com as normas
            de produ&ccedil;&atilde;o agr&iacute;cola, processamento de alimentos e pecu&aacute;ria
            org&acirc;nica, sendo destinado a fabricantes, importadores e distribuidores de
            insumos localizados no Brasil e no exterior.<br />
        </p>
        <br />
        <h2 class="tituloCertificaoInterno">
            Categorias de Insumos</h2>
        <p>
            USO NA AGRICULTURA:<br />
            &bull; Fertilizantes minerais e/ou org&acirc;nicos e condicionadores de solo;<br />
            &bull; Defensivos para controle de pragas, doen&ccedil;as e plantas expont&acirc;neas;<br />
            &bull; Insumos para uso na p&oacute;s-colheita (Higieniza&ccedil;&atilde;o e sanitiza&ccedil;&atilde;o);<br />
            &bull; Demais insumos para uso no manejo agr&iacute;cola (limpeza sistema irriga&ccedil;&atilde;o
            e equipamentos; armadilhas para insetos; entre outros)
            <br />
            <br />
            USO NA PECU&Aacute;RIA:<br />
            &bull; Ingredientes para formula&ccedil;&atilde;o de ra&ccedil;&atilde;o;<br />
            &bull; Insumos para tratamento veterin&aacute;rio;<br />
            &bull; Insumos para controle de ecto e endoparasitas;<br />
            &bull; Insumos para sanitiza&ccedil;&atilde;o de equipamentos e dos animais;
            <br />
            <br />
            USO EM PROCESSAMENTO DE ALIMENTOS:<br />
            &bull; Ingredientes de origem agr&iacute;cola n&atilde;o org&acirc;nicos;<br />
            &bull; Ingredientes de origem n&atilde;o agr&iacute;cola;<br />
            &bull; Defensivos para controle de pragas;<br />
            &bull; Insumos para sanitiza&ccedil;&atilde;o de equipamentos e alimentos org&acirc;nicos.<br />
        </p>
        <br />
        <h2 class="tituloCertificaoInterno">
            Exig&ecirc;ncias para aprova&ccedil;&atilde;o</h2>
        <p>
            Durante o processo de avalia&ccedil;&atilde;o o IBD realiza as seguintes atividades:<br />
            &bull; Auditoria documental;<br />
            &bull; Auditoria do processo produtivo na unidade de produ&ccedil;&atilde;o do insumo;<br />
            &bull; Coleta e an&aacute;lise de amostras retiradas na unidade de produ&ccedil;&atilde;o
            do insumo e/ou no mercado;<br />
            <br />
            Para que um insumo possa ser aprovado a empresa deve:<br />
            &bull; Estar em conformidade com os &oacute;rg&atilde;os p&uacute;blicos (Prefeitura,
            ANVISA, MAPA, IBAMA, entre outros);<br />
            &bull; Estar com o insumo registrado no MAPA e/ou ANVISA para comercializa&ccedil;&atilde;o
            de acordo com a finalidade de uso;<br />
            &bull; Garantir aus&ecirc;ncia de subst&acirc;ncias/mat&eacute;rias-prima proibidas;<br />
            &bull; N&atilde;o utilizar t&eacute;cnicas de produ&ccedil;&atilde;o n&atilde;o
            permitidas;<br />
            &bull; Garantir que os contaminantes estejam dentro do limite (microorganismos,
            metais pesados, entre outros);<br />
            &bull; Possuir controle de qualidade que garanta a manuten&ccedil;&atilde;o das
            caracter&iacute;sticas do insumo;<br />
            &bull; Possuir controle de rastreabilidade.</p>
        <h2 class="tituloCertificaoInterno">
            Informa&ccedil;&otilde;es Adicionais</h2>
        <p>
            GARANTIA DE CREDIBILIDADE IBD:<br />
            &bull; Credenciamentos internacionais para acesso ao mercado org&acirc;nico no mundo
            inteiro;<br />
            &bull; Pol&iacute;tica de sigilo para prote&ccedil;&atilde;o &agrave;s informa&ccedil;&otilde;es
            confidenciais dos insumos aprovados;<br />
            &bull; Equipe de profissionais experientes e especializados na certifica&ccedil;&atilde;o
            de produtos org&acirc;nicos;<br />
            &bull; Transpar&ecirc;ncia com divulga&ccedil;&atilde;o de todos os insumos aprovados
            no site IBD.<br />
        </p>
        <br />

        <h2 class="tituloCertificaoInterno">Passo a Passo</h2>
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=23c1eb35-9e58-4d72-b0ce-bd7456fa9237"> - Passo a Passo para certificação (Português)</a> <br />
            <a href="http://ibd.com.br/ShowFile.aspx?action=2&fileid=d8785e40-98b0-426e-8962-029edaba2f54"> - Passo a Passo para certificação (Inglês)</a>
            <br />

        <h2 class="tituloCertificaoInterno">
            Contatos</h2>
        &Aacute;lvaro Garcia<br />
        Coordenador do Programa de Aprova&ccedil;&atilde;o de Insumos IBD<br />
        Contato: +55 (14) 98104-2148<br />
        Skype: alvarogarciarj<br />
        <br />
        <br />
        <br />
    </div>
    <!--quemsomos-->
</asp:Content>
