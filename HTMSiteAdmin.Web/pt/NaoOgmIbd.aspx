﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="NaoOgmIbd.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.NaoOgmIbd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno
        {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }
        .itemNormas
        {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
        .limpaFloat
        {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certifica&ccedil;&otilde;es e Aprova&ccedil;&otilde;es</span><br />
                Diversas
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                N&atilde;o OGM IBD</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <img src="media/img/logoCertificadoNaoOGMIBD.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                Sobre o Selo</h2>
            Todos sabem que os transgênicos (Organismo Geneticamente Modificados-OGM) vieram
            para ficar. Ocorre que o setor orgânico e uma boa parte dos consumidores não querem
            consumir tais produtos.<br />
            <br />
            Para tanto foi criado o Programa de Certificação de Não OGMs IBD, para segregar
            no mercado os produtos que realmente não contém, ou contém dentro de limites pré-estabelecidos
            materias Não OGM.<br />
            <br />
            O setor de alimentos deveria estar rotulando os alimentos com OGMs (vide em anexo
            a legislação específica). Mas isto não está ocorrendo. Além disso os consumidores
            reclamam por mais transparência sobre efeito de consumo de OGMs em animais e humanos.
            Adicionamos texto sobre este tema também.<br />
            <h2 class="tituloCertificaoInterno">
                Diretrizes Atendidas</h2>
                <!-- <span class="itemNormas">Normas IBD de Produtos Não OGM</span> -->
                - Normas IBD de Produtos Não OGM
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segmentos Atendidos</h2>
            - Agricultura<br />
            - Pecuária<br />
            - Processamento<br />
            - Cosméticos<br />
            <br />
            <br />
            <br />
    </div>
    <!--quemsomos-->
</asp:Content>
