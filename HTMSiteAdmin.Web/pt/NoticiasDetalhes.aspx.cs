﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Conteudos;

namespace HTMSiteAdmin.Web.pt
{
    public partial class NoticiasDetalhes : System.Web.UI.Page
    {
        private int? idConteudo
        {
            get
            {
                try { return Convert.ToInt32(Request.QueryString["id_conteudo"]); }
                catch (Exception) { return null; }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (idConteudo.HasValue && idConteudo.Value > 0)
            {
                //var container = new HTMSiteAdminEntities();
                var _objConteudo = new ConteudoBo().Find(c => c.ID_CONTEUDO == idConteudo.Value).First();

                if (_objConteudo.ID_CONTEUDO_SITUACAO != 1)
                    return;

                if (_objConteudo.DATA_PUBLICACAO.HasValue)
                {
                    ltrDia.Text = _objConteudo.DATA_PUBLICACAO.Value.Day.ToString();
                    ltrMes.Text = HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objConteudo.DATA_PUBLICACAO.Value.Month);
                    ltrDataPublicacao.Text = _objConteudo.DATA_PUBLICACAO.Value.ToShortDateString();
                }
                else
                {
                    ltrDia.Text = _objConteudo.SESSAO.DATA_INICIO.Day.ToString();
                    ltrMes.Text = HTMSiteAdmin.Library.UserInterface.Converters.MesString(_objConteudo.SESSAO.DATA_INICIO.Month);
                    ltrDataPublicacao.Text = _objConteudo.SESSAO.DATA_INICIO.ToShortDateString();
                }

                //var arquivoDigital = container.ARQUIVO_DIGITAL.FirstOrDefault(a => a.ID_ARQUIVO == _objConteudo.ID_ARQUIVO_DIGITAL);
                

                ltrCategoria.Text = new ConteudoCategoriaBo().Find(c => c.ID_CONTEUDO_CATEGORIA == _objConteudo.ID_CONTEUDO_CATEGORIA).First().NOME;
                ltrTitulo.Text = _objConteudo.TITULO;
                if (!String.IsNullOrEmpty(_objConteudo.SUB_TITULO))
                    ltrSubTitulo.Text = "<h2>" + _objConteudo.SUB_TITULO + "</h2><br />";
                
                ltrConteudo.Text = _objConteudo.CONTENT_DATA;
                if (!String.IsNullOrEmpty(_objConteudo.FONTE))
                    ltrFonte.Text = "Fonte: " + _objConteudo.FONTE;

                //ltrImagem.Text = "<img src=\"" + ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", _objConteudo.ID_ARQUIVO_DIGITAL)) + "\" alt=\"\" width=\"\" height=\"\" />";
            }
        }
    }
}