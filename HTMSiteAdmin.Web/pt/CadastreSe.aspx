﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="CadastreSe.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.CadastreSe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Cadastre-se</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="certificadosConsultar">
        <div class="texto">
            <p>
                Fique por dentro das novidades do IBD. Se cadastre e receba periodicamente por email
                os informativos.</p>
            <fieldset>
                <ul>
                    <li>
                        <asp:Label Text="nome" runat="server" AssociatedControlID="txtNome" />
                        <asp:TextBox ID="txtNome" runat="server" Style="float: left;" />
                        <asp:RequiredFieldValidator ID="rfvNome" ControlToValidate="txtNome" runat="server"
                            ErrorMessage="- Nome" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <asp:Label ID="Label1" Text="e-mail" runat="server" AssociatedControlID="txtEmail" />
                        <asp:TextBox ID="txtEmail" runat="server" Style="float: left;" />
                        <asp:RequiredFieldValidator ID="rfvEmail" ControlToValidate="txtEmail" runat="server"
                            ErrorMessage="- E-Mail" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                            ErrorMessage="- E-mail inválido" Display="None" SetFocusOnError="true" ValidationGroup="vgForm"
                            ValidationExpression="\S+@\S+.\S{2,3}"></asp:RegularExpressionValidator>
                    </li>
                    <li>
                        <asp:Label ID="Label2" Text="telefone" runat="server" AssociatedControlID="txtCelular" />
                        <asp:TextBox ID="txtCelular" runat="server" Style="float: left;" />
                        <asp:RequiredFieldValidator ID="rfvCelular" ControlToValidate="txtCelular" runat="server"
                            ErrorMessage="- Telefone" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <asp:Label ID="Label5" Text="município" runat="server" AssociatedControlID="txtMunicipio" />
                        <asp:TextBox ID="txtMunicipio" runat="server" Style="float: left;" />
                        <asp:RequiredFieldValidator ID="rfvMunicipio" ControlToValidate="txtMunicipio" runat="server"
                            ErrorMessage="- Município" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <asp:Label ID="Label4" Text="estado" runat="server" AssociatedControlID="txtEstado" />
                        <asp:TextBox ID="txtEstado" runat="server" Style="float: left;" />
                        <asp:RequiredFieldValidator ID="rfvEstado" ControlToValidate="txtEstado" runat="server"
                            ErrorMessage="- Estado" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <asp:Label ID="Label3" Text="pais" runat="server" AssociatedControlID="txtPais" />
                        <asp:TextBox ID="txtPais" runat="server" Style="float: left;" />
                        <asp:RequiredFieldValidator ID="rfvPais" ControlToValidate="txtPais" runat="server"
                            ErrorMessage="- Pais" Display="None" SetFocusOnError="True" ValidationGroup="vgForm"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <label>
                            &nbsp;</label>
                        <asp:LinkButton Style="float: left;" ID="btnCadastrar" runat="server" OnClick="btnCadastrar_Click"
                            ValidationGroup="vgForm" CssClass="cadastrar">cadastrar</asp:LinkButton>
                    </li>
                </ul>
            </fieldset>
            <asp:ValidationSummary ID="vsForm" runat="server" ValidationGroup="vgForm" CssClass="erroLogin"
                HeaderText="Os seguintes campos contêm erros ou são obrigatórios:" />
        </div>
        <!--texto-->
        <div class="imagem">
            <img src="media/img/imagem_interna_06.jpg" class="border3pxfff" alt="" /></div>
        <!--imagem-->
    </div>
    <!--certificadosConsultar-->
</asp:Content>
