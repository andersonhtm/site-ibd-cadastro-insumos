﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="JasOrganic.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.JasOrganic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Orgânico
                <img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px;
                    margin-left: 6px;" />
                JAS Organic</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />
        <p>
            <style>
                .tituloCertificaoInterno
                {
                    color: #2D312E;
                    font-size: 20px;
                    margin: 15px 0 10px;
                    width: 100%;
                }
                .itemNormas
                {
                    display: block;
                    background: #fffcec;
                    padding: 6px 10px 6px 10px;
                    margin: 0px 8px 10px 0px;
                    float: left;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }
                .limpaFloat
                {
                    clear: both;
                    height: 10px;
                }
            </style>
            <img src="media/img/logoCertificadoJASOrganic.png" alt="" style="float: right; margin-left: 15px;
                margin-bottom: 10px;" />
            <h2 class="tituloCertificaoInterno">
                Sobre o Selo</h2>
            O selo JAS Organic &eacute; o selo japon&ecirc;s acreditado pelo Departamento de
            Agricultura Japon&ecirc;s.<br />
            <br />
            Este selo tem o objetivo de desenvolver, manter e expandir o acesso para produtos
            de pa&iacute;ses diversos ao mercado Japon&ecirc;s.<br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Diretrizes Atendidas</h2>
                <!-- <span class="itemNormas">JAS</span> -->
                - JAS
            <br />
            <br />
            <h2 class="tituloCertificaoInterno">
                Segmentos Atendidos</h2>
            - Agricultura<br />
            - Pecuária<br />
            - Processamento<br />
            - Extrativismo<br />
            <br />
            <!--
            <h2 class="tituloCertificaoInterno">
                Informa&ccedil;&otilde;es Adicionais</h2>
            <span class="itemNormas">JAS Procedimento</span> <span class="itemNormas">JAS Produção</span>
            <br />
            <br />
            -->
        </p>
        <br />
        <br />
        <br />
    </div>
    <!--quemsomos-->
</asp:Content>
