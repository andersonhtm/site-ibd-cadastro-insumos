﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="Iscc.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.Iscc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
    <style type="text/css">
        .tituloCertificaoInterno {
            color: #2D312E;
            font-size: 20px;
            margin: 15px 0 10px;
            width: 100%;
        }

        .itemNormas {
            display: block;
            background: #fffcec;
            padding: 6px 10px 6px 10px;
            margin: 0px 8px 10px 0px;
            float: left;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }

        .limpaFloat {
            clear: both;
            height: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno2" class="topoInterno">
        <div class="centro">
            <h2>
                <span>Certificações</span><br />
                Socio-Ambiental<img src="media/img/iconSetaTitulo.gif" alt="" style="margin-top: 9px; margin-right: 6px; margin-left: 6px;" />
                ISCC</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="quemsomos">
        <br />

        <img src="media/img/LOGO_ISCC.png" alt="ISCC" style="float: right; margin-left: 15px; margin-bottom: 10px;" width="213" height="120" />
        <h2 class="tituloCertificaoInterno">Sobre o Selo</h2>
        <p>
            O ISCC é um sistema de certificação líder global que abrange toda a cadeia de suprimentos e todos os tipos de matérias-primas e fontes renováveis. 
            A certificação assegura o cumprimento dos elevados requisitos de sustentabilidade ecológica e social, a rastreabilidade e as emissões de gases efeito de estufa em toda a cadeia de abastecimento. 
            O ISCC pode ser aplicado em vários mercados, incluindo o setor de bioenergia, o mercado de alimentos, ração para animais e o mercado de produtos químicos. Desde o seu início de operação em 2010, 
            mais de 12.500 certificados, em mais de 100 países foram emitidos
        </p>
        <br />

        <h2 class="tituloCertificaoInterno">O que é cobertura pelo ISCC</h2>
        <p>
            ISCC é globalmente aplicável para todos os tipos de culturas agrícolas, seus derivados e renováveis. São abrangidos todos os elementos ao longo da cadeia de abastecimento, 
            desde a agricultura ou o ponto de origem até ao utilizador do produto final. O ISCC garante que: 
        </p>
        <br />
        <p>
            &bull; As emissões de gases de efeito estufa são reduzidas;
            <br />
            &bull; A biomassa não é produzida em terras com alta biodiversidade e alto estoque de carbono;
            <br />
            &bull; As boas práticas agrícolas e a protecção do solo, da água e do ar são aplicadas;
            <br />
            &bull; Os direitos humanos, trabalhistas e fundiários são respeitados;
            <br />
        </p>
        <br />

        <p>
            Requisitos elevados de rastreabilidade asseguram que a biomassa possa ser rastreada ao longo de toda a cadeia de abastecimento. 
            Além disso, o ISCC fornece metodologias para calcular os balanços de massa e verificar as emissões de gases de efeito estufa ao longo da cadeia de suprimentos. 
        </p>

        <h2 class="tituloCertificaoInterno">Quais são os princípios do ISCC</h2>
        <p>
            <b>Princípio 1:</b> Proteção de terras com alto valor de biodiversidade ou alto estoque de carbono.
            Isto inclui florestas primárias e outras terras arborizadas de espécies nativas, prados de alta biodiversidade, turfeiras, zonas húmidas, áreas florestais contínuas, 
            áreas designadas para a protecção de ecossistemas ou espécies raras, ameaçadas ou em perigo, bem como áreas de alto valor de conservação (VHC)

        </p> <br />
        <p>
            <b>Princípio 2:</b> Produção ambientalmente responsável para proteger o solo, a água e o ar.
        </p> <br />
        <p>
            <b>Princípio 3:</b> Condições de trabalho seguras.
        </p> <br />
        <p>
            <b>Princípio 4:</b> Cumprimento dos direitos humanos, trabalhistas e fundiários e relações comunitárias responsáveis.
        </p> <br />
        <p>
            <b>Princípio 5:</b> Cumprimento das leis aplicáveis e dos tratados internacionais relevantes.
        </p> <br />
        <p>
            <b>Princípio 6:</b> Boas práticas de gestão e compromisso com a melhoria contínua.
        </p> <br />
        <br />

        <h2 class="tituloCertificaoInterno">Porquê o ISCC ?</h2>
        <p>
            O ISCC oferece uma solução única para todos os cultivos e mercados. 
            Ele permite que os usuários do sistema acessem diferentes segmentos de mercado com um único certificado. 
            O ISCC é oficialmente reconhecido pela Comissão Europeia ao abrigo da Diretiva Europeia de Energia Renovável (RED). 
            Além disso, o ISCC está em conformidade com o Código de Agricultura Sustentável da Unilever, com a Avaliação de Sustentabilidade da SAI (nível prata ou nível ouro) 
            e os requisitos da FEFAC com relação às diretrizes de abastecimento de soja. O ISCC também foi selecionado como um sistema de escolha por uma variedade de iniciativas 
            de diferentes segmentos de mercado (por exemplo, Aireg para combustíveis de jato, INRO para materiais de base biológica e FONAP para produtos à base de palmeira).
        </p>

        <h2 class="tituloCertificaoInterno">Quem governa o ISCC ?</h2>
        <p>
            O ISCC é uma iniciativa multi-stakeholder governada por uma associação com atualmente mais de 80 membros. 
            O ISCC foi desenvolvido através de um processo aberto de múltiplas partes interessadas envolvendo cerca de 250 
            associações internacionais, corporações, instituições de pesquisa e ONGs da Europa, Américas e Sudeste Asiático, 
            a fim de garantir alta praticidade e rentabilidade
        </p>
        <br />

        <h2 class="tituloCertificaoInterno">Diretrizes Atendidas</h2>
        - Diretrizes do programa ISCC
        <br />

        <h2 class="tituloCertificaoInterno">Segmentos Atendidos</h2>
        - Agricultura<br />
        - Industria de alimento e bebidas<br />

    </div>
    <!--quemsomos-->
</asp:Content>
