﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pt/Interna.Master" AutoEventWireup="true"
    CodeBehind="EventosFeirasDetalhes.aspx.cs" Inherits="HTMSiteAdmin.Web.pt.EventosFeirasDetalhes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderTopoInterno" runat="server">
    <div id="topoInterno" class="topoInterno">
        <div class="centro">
            <h2>
                Eventos e Feiras</h2>
            <a href="/">voltar para a home</a>
        </div>
        <!--centro-->
    </div>
    <!--topoInterno-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderConteudo" runat="server">
    <div id="noticias">
        <div class="itemNoticiasInterno">
            <div class="texto2 texto">
                <div class="dataTipo">
                    <span>
                        <asp:Literal ID="ltrPeriodo" Text="de 01 a 10 de Janeiro de 2012" runat="server" /></span>
                    <h4>
                        <asp:Literal ID="ltrCategoria" Text="Notícia" runat="server" /></h4>
                    <p>
                        publicado em
                        <asp:Literal ID="ltrDataPublicacao" Text="30/01/2012" runat="server" /></p>
                </div>
                <!--dataTipo-->
                <h3>
                    <asp:Literal ID="ltrTitulo" Text="Título da notícia" runat="server" /></h3>
                <p>
                    <asp:Literal ID="ltrConteudo" Text="Conteúdo" runat="server" /></p>
            </div>
            <!--texto-->
        </div>
        <!--itemNoticias-->
    </div>
    <!--noticias-->
</asp:Content>
