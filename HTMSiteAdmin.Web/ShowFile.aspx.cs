﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using HTMSiteAdmin.Business.Sistema;
using HTMSiteAdmin.Data;
using System.IO;
using System.Configuration;

namespace HTMSiteAdmin.Web
{
    public partial class ShowFile : System.Web.UI.Page
    {
        public enum enmShowFileAction
        {
            View = 1,
            Download = 2
        }

        private enmShowFileAction? Action
        {
            get
            {
                try { return (enmShowFileAction)Convert.ToInt32(Request.QueryString["action"].ToString()); 
                    //return enmShowFileAction.Download;
                }
                catch (Exception) { return null; }
            }
        }
        private Guid? FileId
        {
            get
            {
                try { return new Guid(Request.QueryString["fileid"]); }
                catch (Exception) { return null; }
            }
        }
        private int? Width
        {
            get
            {
                try { return Convert.ToInt32(Request.QueryString["width"]); }
                catch (Exception) { return null; }
            }
        }
        private int? Height
        {
            get
            {
                try { return Convert.ToInt32(Request.QueryString["height"]); }
                catch (Exception) { return null; }
            }
        }
        private HTMSiteAdmin.Library.ImageTools.UseMethod? ViewMethod
        {
            get
            {
                try { return (HTMSiteAdmin.Library.ImageTools.UseMethod)Convert.ToInt32(Request.QueryString["ViewMethod"]); }
                catch (Exception) { return null; }
            }
        }
        private HTMSiteAdmin.Library.ImageTools.Dimensions? Dimension
        {
            get
            {
                try { return (HTMSiteAdmin.Library.ImageTools.Dimensions)Convert.ToInt32(Request.QueryString["Dimension"]); }
                catch (Exception) { return null; }
            }
        }
        private int? Size
        {
            get
            {
                try { return Convert.ToInt32(Request.QueryString["Size"]); }
                catch (Exception) { return null; }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string path = Server.MapPath(ConfigurationManager.AppSettings["RELATIVEPATH_ARQUIVODIGITAL"]);

            try
            {
                StringBuilder sbValidacoes = new StringBuilder();

                if (!Action.HasValue)
                    sbValidacoes.AppendLine("Ação não pode ser identificada");


                ARQUIVO_DIGITAL _objArquivoDigital = new ArquivoDigitalBo().Find(lbda => lbda.ID_ARQUIVO == FileId).First();
                DOWNLOAD download = _objArquivoDigital.DOWNLOAD.FirstOrDefault();
                if (download != null)
                {
                    if (!download.ATIVO)
                    {
                        sbValidacoes.AppendLine("Arquivo está inativo.");
                    }
                }

                if (sbValidacoes.Length > 0)
                    throw new Exception(sbValidacoes.ToString());

                byte[] content = File.ReadAllBytes(string.Format("{0}{1}.{2}", path, _objArquivoDigital.ID_ARQUIVO.ToString(), _objArquivoDigital.EXTENSAO));

                switch (Action.Value)
                {
                    case enmShowFileAction.View:
                        if (_objArquivoDigital.EXTENSAO.Replace(".", "").ToLower() == "pdf")
                        {
                            StringBuilder str = new StringBuilder();
                            str.AppendLine("<html>");
                            str.AppendLine("<head>");
                            str.AppendLine("<script type='text/javascript'>");
                            str.AppendLine(@"window.location='" + Page.ResolveClientUrl("~/Media/arquivo_digital/" + _objArquivoDigital.ID_ARQUIVO.ToString() + "." + _objArquivoDigital.EXTENSAO) + "';");
                            str.AppendLine("</script>");
                            str.AppendLine("</head>");
                            str.AppendLine("</html>");

                            string script = @"window.location='" + Page.ResolveClientUrl("~/Media/arquivo_digital/" + _objArquivoDigital.ID_ARQUIVO.ToString() + "." + _objArquivoDigital.EXTENSAO) + "';";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "showfile", script, true);

                            //Response.Clear();
                            //Response.Write(str.ToString());
                            //Response.End();
                            return;
                        }
                        Response.ContentType = _objArquivoDigital.EXTENSAO;
                        if (FileId.HasValue)
                        {
                            if (Width.HasValue
                                && Width.Value > 0
                                && Height.HasValue
                                && Height.Value > 0)
                                Response.BinaryWrite(HTMSiteAdmin.Library.ImageTools.ScaleToFixedWithCrop(content, Width.Value, Height.Value, Library.ImageTools.AnchorPosition.Center));
                            else if (ViewMethod == HTMSiteAdmin.Library.ImageTools.UseMethod.ConstrainProportions)
                            {
                                if (Size.HasValue
                                    && Size.Value > 0
                                    && Dimension.HasValue)
                                    Response.BinaryWrite(HTMSiteAdmin.Library.ImageTools.ConstrainProportions(content, Size.Value, Dimension.Value));
                                else
                                    Response.BinaryWrite(content);
                            }
                            else
                                Response.BinaryWrite(content);

                        }
                        break;
                    case enmShowFileAction.Download:
                        Response.ContentType = _objArquivoDigital.EXTENSAO;
                        Response.Clear();
                        //Response.Buffer = true;
                        string arquivo = HttpUtility.UrlPathEncode(_objArquivoDigital.NOME);
                        Response.AddHeader("content-disposition", "attachment;filename=" + arquivo);
                        //Response.Charset = "";
                        Response.WriteFile(string.Format("{0}{1}.{2}", path, _objArquivoDigital.ID_ARQUIVO.ToString(), _objArquivoDigital.EXTENSAO));
                        break;
                }

            }
            catch (Exception ex) { }
            
        }
    }
}