﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace HTMSiteAdmin.Web
{
    public static class ControlExtension
    {
        public static T FindControl<T>(this Control control, string controlName) where T : Control
        {
            object result = control.FindControl(controlName);
            return (T)result;
        }
    }

}