﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HTMSiteAdmin.Web.m
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;

            string lang = currentCulture.TwoLetterISOLanguageName;

            if (lang.Equals("pt"))
            {
                Response.Redirect("~/pt_mobile/Default.aspx");
            }
            else
            {
                Response.Redirect("~/en_mobile/Default.aspx");
            }
        }
    }
}