﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="QuemSomos.aspx.cs" Inherits="HTMSiteAdmin.Web.en_mobile.QuemSomos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">

    <div class="interna quemsomos">
    
    	
    
    	<h3 class="tituloGeral">Quem Somos</h3>
        
    	<img src="../media_mobile/img/quemsomos/1.jpg" alt="" width="280" height="93" />
    
		<p>IBD prides itself of &nbsp;being pioneer  in Latin America and the only Brazilian organic products certifier that is  accredited under IFOAM (international market), ISO/IEC 17065 (European market,  rule CE 834/2007), Demeter (international market), USDA/NOP (North American  market) and INMETRO / MAPA (Brazilian market), making its certificate accepted  globally.&nbsp;</p>
        
        <img src="../media_mobile/img/quemsomos/2.jpg" alt="" width="280" height="93" />
    
        <p>In addition to organic certification protocols, IBD offers social environmental  certifications: RSPO (Roundtable on Sustainable Palm Oil), UEBT (Union for  Ethical BioTrade), Fair Trade IBD, Rainforest Alliance, UTZ, 4 C ASSOCIATION.</p>
        
        <img src="../media_mobile/img/quemsomos/3.jpg" alt="" width="280" height="93" />
    
		<p>Located in Botucatu/SP (Brazil) and founded in 1983, IBD operates  throughout all the Brazilian states, as well as in diverse countries such as  Argentina, Bolivia, Columbia, Ecuador, Mexico, Paraguay, Uruguay, the United  States, Canada , Belgium, Holland, New Zealand, China, India, and Thailand,  working towards sustainable production standards and stimulating fair trade.</p>
        
        
    

    
    </div><!--interna-->
</asp:Content>
