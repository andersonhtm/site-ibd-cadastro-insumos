﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="SejaIBD.aspx.cs" Inherits="HTMSiteAdmin.Web.en_mobile.SejaIBD" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">
     <div class="interna sejaibd">
    
    	<h3 class="tituloGeral">Become an IBD client</h3>

        <p>We offer personalized service, protocols appropriate to your needs, and a specialized team with vast experience in the certification market.</p>
         <p>Contact our customer service department to obtain more information and request documents for initiating the certification process. We are happy to serve you. </p>
        
        <h4 class="tituloInterno"> Discover the 10 reasons to become an IBD client:</h4>
        
        <ul>
        	<li><span>1</span>Throughout the world and also in Brazil, the name IBD is linked to professionalism, seriousness and dedication. With an IBD certificate, your products will be accepted globally.</li>
            
            <li><span>2</span> In addition to being the largest certifier of organic products in Latin America, IBD has extended its services to protocols related to sustainability, recently obtaining accreditation for RSPO, UEBT and BONSUCRO, demonstrating vanguardism and professionalism. </li>

            <li><span>3</span>
                    The Fair Trade IBD seal for social, environmental and fair trade adequacy of organic
                    products.
            </li>
            <li><span>4</span>
                    Estimates are clear and transparent, permitting complete planning by the client
                    for investment, structure and meeting deadlines.
            </li>
            <li><span>5</span>
                    Nearly 90% of the projects certified by IBD are family farmers. IBD’s structure
                    permits accessibility and differentiated costs, adapted to the economic situation
                    of each producer.
            </li>
            <li><span>6</span>
                    IBD works with a vast network of inspectors distributed regionally, providing for
                    efficiency and cost accessibility.
            </li>
            <li><span>7</span>
                    For those interested in importing organic product into Brazil under the Brazilian
                    Law 10.831, IBD relies on a network of more than 50 inspectors located in more than
                    20 countries worldwide.
            </li>
            <li><span>8</span>
                    Inspectors receive annual internal trainings, including specialized lectures and
                    technical audit content, giving the IBD inspector superior technical qualifications.
            </li>
            <li><span>9</span>
                    IBD was the pioneer for incorporating social issues, based on international labor
                    agreements, into certification, serving as a model for other certifiers internationally.
            </li>
            <li><span>10</span>
                    IBD is a pioneer in guaranteeing the preservation of forests and water resources
                    within certified projects. As a result, tens of thousands of native species have
                    been replanted.
            </li>
        </ul>
    
    </div><!--interna-->
</asp:Content>
