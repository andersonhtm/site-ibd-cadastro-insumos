﻿using HTMSiteAdmin.Business.Eventos;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Web.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HTMSiteAdmin.Web.en_mobile
{
    public partial class EventosDetalhes : System.Web.UI.Page 
    {
        private int? idConteudo
        {
            get
            {
                try { return Convert.ToInt32(Request.QueryString["id_conteudo"]); }
                catch (Exception) { return null; }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (idConteudo.HasValue && idConteudo.Value > 0)
            {
                EVENTO _objEvento = new EventoBo().Find(c => c.ATIVO &&  !string.IsNullOrEmpty(c.DESCRICAO_EN) && c.ID_EVENTO == idConteudo.Value).FirstOrDefault();

                if (_objEvento == null)
                    return;

                ltrPeriodo.Text = Util.GetPeriodoEvento(_objEvento);                

                if (_objEvento.DATA_PUBLICACAO.HasValue)
                    ltrDataPublicacao.Text = _objEvento.DATA_PUBLICACAO.Value.ToShortDateString();
                else
                    ltrDataPublicacao.Text = _objEvento.SESSAO.DATA_INICIO.ToShortDateString();
                
                ltrTitulo.Text = _objEvento.DESCRICAO_EN;
                ltrConteudo.Text = _objEvento.DESCRICAO_DETALHADA_EN;
            }
        }
    }
}