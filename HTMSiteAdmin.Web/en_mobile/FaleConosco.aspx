﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="FaleConosco.aspx.cs" Inherits="HTMSiteAdmin.Web.en_mobile.FaleConosco" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="interna faleconosco">
    
    	<h3 class="tituloGeral">Contact us</h3>
        
		<h4 class="tituloInterno" style="border-top:none; padding-top:0px;">Phone</h4>
        <span class="telefone">+55 (14) 3811 9800</span>
        
        <h4 class="tituloInterno">Fax</h4>        
        <span class="telefone">+55 (14) 3811 9801</span>
        
        <h4 class="tituloInterno">Email</h4>
        <span class="email">ibd@ibd.com.br</span>
        
        <h4 class="tituloInterno">Address</h4>
        <span class="email">
        Rua Amando de Barros, 2275 - Centro<br />
        CEP: 18.602.150  Botucatu - SP
        </span>
        
        
            
        <div class="clear"></div>
        


        
        <fieldset>

             <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="sumario-erros" ForeColor="Red" DisplayMode="BulletList" />

            <asp:Label ID="Label1" runat="server" Text="Assunto" AssociatedControlID="ddlAssunto"></asp:Label>
            <asp:DropDownList runat="server" ID="ddlAssunto">
                <asp:ListItem Text="Get certified with IBD" Selected="True" />
                <asp:ListItem Text="Questions and Help Desk" />
                <asp:ListItem Text="Financial Department" />
            </asp:DropDownList>

            <asp:Label ID="Label2" runat="server" Text="Name" AssociatedControlID="txtNome"></asp:Label>
            <asp:TextBox ID="txtNome" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required name" ControlToValidate="txtNome"></asp:RequiredFieldValidator>            

            <asp:Label ID="Label3" runat="server" Text="E-mail *" AssociatedControlID="txtEmail"></asp:Label>
            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail" ErrorMessage="Required e-mail"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator Display="None" SetFocusOnError="true" ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid e-mail" ControlToValidate="txtEmail" ValidationExpression="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"></asp:RegularExpressionValidator>

            <asp:Label ID="Label4" runat="server" Text="Phone" AssociatedControlID="txtTelefone"></asp:Label>
            <asp:TextBox ID="txtTelefone" runat="server" style="width:110px"></asp:TextBox>            

            <asp:Label ID="Label5" runat="server" Text="Message *" AssociatedControlID="txtMensagem"></asp:Label>
            <asp:TextBox ID="txtMensagem" TextMode="MultiLine" runat="server"></asp:TextBox>            
            <asp:RequiredFieldValidator Display="None" ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required message" ControlToValidate="txtMensagem"></asp:RequiredFieldValidator>
            
            <asp:LinkButton CssClass="enviar" OnClick="btnEnviar_Click" ID="btnEnviar" runat="server">send »</asp:LinkButton>               
                        
        </fieldset>
        
        <h4 class="tituloCertificaoInterno">Complaint</h4>
        
            <ul>
            	<li><span>1</span>If you wish to report a complaint and/or investigations, please use the link: <br />
                    <a href="https://docs.google.com/forms/d/e/1FAIpQLScWixdWY6YcZhDY31nUnWE1qBwa2pC9MIeOPOgsFEN28i5Umg/viewform">docs.google.com/forms/d/e/1FAIpQLScWixdWY6YcZhDY31nUnWE1qBwa2pC9MIeOPOgsFEN28i5Umg/viewform</a></li>
                <li><span>2</span>The protocol number will be informed to the contact indicated.</li>
                <li><span>3</span>All claims or complaints will be processed within 60 days. Due to accreditation processes and actions needed to clarify the process can stretch. The appellant / complainant will be notified of the status of your complaint.</li>
            </ul>
</div><!--interna-->
</asp:Content>
