﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Business.Conteudos;
using HTMSiteAdmin.Data;

namespace HTMSiteAdmin.Web.en_mobile.UserControls
{
    public partial class ucUltimasNoticias : System.Web.UI.UserControl
    {
        IQueryable<CONTEUDO> _objConteudoCollection;

        public bool chamada { get; set; }

        public int nrPagina
        {
            get
            {
                try { return int.Parse(Request.QueryString["pagina"].ToString()); }
                catch (Exception) { return 1; }
            }
        }
        private int pageIndex
        {
            get
            {
                try { return Convert.ToInt32(Request.QueryString["page_index"]); }
                catch (Exception) { return 0; }
            }
        }
        private int totalRegistros { get; set; }
        private List<CONTEUDO> _objResult { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            _objConteudoCollection = new ConteudoBo().Find(c => c.ID_CONTEUDO_TIPO == 1 && !string.IsNullOrEmpty(c.TITULO_EN) && (!c.DATA_PUBLICACAO.HasValue || (c.DATA_PUBLICACAO.HasValue && c.DATA_PUBLICACAO.Value <= DateTime.Now))).OrderByDescending(lbda => lbda.DATA_PUBLICACAO);
            _objResult = _objConteudoCollection.Skip(((nrPagina * 20) - 20)).Take(20).ToList();
            totalRegistros = _objConteudoCollection.Count();

            if (chamada && _objResult.Count() >= 2)
                rptUltimasNoticias.DataSource = _objResult.ToList().GetRange(0, 2);
            else
                rptUltimasNoticias.DataSource = _objResult.ToList();

            rptUltimasNoticias.DataBind();

            if (!chamada)
            {
                Paginacao();
            }
            else
            {
                pnlPaginacao.Visible = false;
            }
        }

        private void Paginacao()
        {
            string formattedUrl = Request.Url.ToString().Replace("?pagina=" + nrPagina, "");
            //Descobrindo o total de páginas
            int totalPaginas = 1;

            if (totalRegistros > 20)
                totalPaginas = (totalRegistros / 20) + 1;

            //Se o total de página for igual a 1, não precisa exibir o paginador
            if (totalPaginas == 1)
                pnlPaginador.Visible = pnlPaginador.Visible = false;

            //Somente se o total de páginas for MAIOR que 1, o paginador será exibido
            else if (totalPaginas > 1)
            {
                pnlPaginador.Visible = pnlPaginador.Visible = true;

                if (nrPagina == 1)
                    ltrAnterior.Text = ltrAnterior.Text = string.Format("<a href=\"{0}?pagina={1}\" class=\"anterior\">previous</a>", formattedUrl, 1);
                else
                    ltrAnterior.Text = ltrAnterior.Text = string.Format("<a href=\"{0}?pagina={1}\" class=\"anterior\">previous</a>", formattedUrl, nrPagina - 1);

                int nrPaginadores = (nrPagina - 5) < 0 ? 1 : (nrPagina - 5);
                ltrPaginas.Text = ltrPaginas.Text = "";

                while (nrPaginadores <= (nrPagina + 5) && nrPaginadores <= totalPaginas)
                {
                    ltrPaginas.Text += string.Format("<a href=\"{0}?pagina={1}\" class=\"bt\">{1}</a>", formattedUrl, nrPaginadores);
                    nrPaginadores++;
                }
                ltrPaginas.Text = ltrPaginas.Text;

                if (totalPaginas == nrPagina)
                    ltrProxima.Text = ltrProxima.Text = string.Format("<a href=\"{0}?pagina={1}\" class=\"proxima\">next</a>", formattedUrl, nrPagina);
                else
                    ltrProxima.Text = ltrProxima.Text = string.Format("<a href=\"{0}?pagina={1}\" class=\"proxima\">next</a>", formattedUrl, nrPagina + 1);
            }

            ltrContagemPaginacao.Text = string.Format("pages {0}/{1} - listing {2}/{3}",
                nrPagina
                , totalPaginas
                , ((nrPagina * 20) - 20) + _objResult.Count
                , totalRegistros);
        }

        protected void rptUltimasNoticias_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer || e.Item.ItemType == ListItemType.Header)
                return;

            string dia, mes;

            if (((CONTEUDO)e.Item.DataItem).DATA_PUBLICACAO.HasValue)
                dia = ((CONTEUDO)e.Item.DataItem).DATA_PUBLICACAO.Value.Day.ToString();
            else
                dia = DateTime.Now.Day.ToString();

            if (((CONTEUDO)e.Item.DataItem).DATA_PUBLICACAO.HasValue)
                mes = ((CONTEUDO)e.Item.DataItem).DATA_PUBLICACAO.Value.ToString("MMMM");
            else
                mes = HTMSiteAdmin.Library.UserInterface.Converters.MesString(DateTime.Now.Month);

            ((Literal)e.Item.FindControl("ltrLinkNoticia")).Text = string.Format("<div class=\"item\"><a href=\"NoticiasDetalhes.aspx?id_conteudo={0}\">{1} <span>{2} {3}</span></a></div>", ((CONTEUDO)e.Item.DataItem).ID_CONTEUDO.ToString(), ((CONTEUDO)e.Item.DataItem).TITULO_EN, dia, mes);
        }
    }
}