﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Data;
using HTMSiteAdmin.Business.Eventos;

namespace HTMSiteAdmin.Web.en_mobile.UserControls
{
    public partial class ucChamadaEventos : System.Web.UI.UserControl
    {
        IQueryable<EVENTO> _objCollection;

        public bool chamada { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            _objCollection = new EventoBo().Find(c => (!c.DATA_PUBLICACAO.HasValue || (c.DATA_PUBLICACAO.HasValue && c.DATA_PUBLICACAO.Value <= DateTime.Now)) && !string.IsNullOrEmpty(c.DESCRICAO_EN) && (c.DATA_INICIO.HasValue && c.DATA_INICIO.Value >= DateTime.Now)).OrderByDescending(lbda => lbda.DATA_PUBLICACAO);
            
            if (chamada && _objCollection.Count() >= 2)
                dtUltimasNoticias.DataSource = _objCollection.ToList().GetRange(0, 2);
            else
                dtUltimasNoticias.DataSource = _objCollection.ToList();

            dtUltimasNoticias.DataBind();
        }

        protected void dtUltimasNoticias_ItemDataBound(object sender, DataListItemEventArgs e)
        {            
            ((Literal)e.Item.FindControl("ltrLinkNoticia")).Text = string.Format("<a href=\"EventosDetalhes.aspx?id_conteudo={0}\">{1} <span>{2}</span></a>", ((EVENTO)e.Item.DataItem).ID_EVENTO.ToString(), ((EVENTO)e.Item.DataItem).DESCRICAO_EN, ((EVENTO)e.Item.DataItem).DATA_INICIO.Value.ToShortDateString());
        }
    }
}