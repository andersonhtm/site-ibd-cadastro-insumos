﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HTMSiteAdmin.Business.Localizacao;
using HTMSiteAdmin.Data;

namespace HTMSiteAdmin.Web.en_mobile.UserControls
{
    public partial class ucClientesCertificados : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (HTMSiteAdminEntities _ctx = new HTMSiteAdminEntities())
                {
                    ddlCertificado.DataSource = _ctx.GetCertificadosComClientes();
                    ddlCertificado.DataBind();

                    ddlClienteCertificadoPais.DataSource = _ctx.GetPaisesComProdutosCertificados();
                    ddlClienteCertificadoPais.DataBind();

                    ddlClienteCertificadoUf.DataSource = _ctx.GetEstadosComProdutosCertificados(ddlClienteCertificadoPais.SelectedValue);
                    ddlClienteCertificadoUf.DataBind();
                }
            }
        }

        protected void ddlClienteCertificadoPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlClienteCertificadoPais.SelectedValue != "0")
            {
                try
                {
                    spanTextoEstado.Visible = false;
                    ddlClienteCertificadoUf.Visible = true;
                    ddlClienteCertificadoUf.DataSource = new HTMSiteAdminEntities().GetEstadosComProdutosCertificados(ddlClienteCertificadoPais.SelectedValue);
                    ddlClienteCertificadoUf.DataBind();
                }
                catch (Exception ex) { throw; }
            }
            else{
                spanTextoEstado.Visible = true;
                ddlClienteCertificadoUf.Visible = false;
                ddlClienteCertificadoUf.Items.Clear();
                ddlClienteCertificadoUf.DataBind();
            }
        }

        protected void ddlClienteCertificadoUf_DataBound(object sender, EventArgs e)
        {
            if (((DropDownList)sender).Items.FindByValue("0") == null)
                ((DropDownList)sender).Items.Insert(0, new ListItem("Todos", "0"));
        }

        protected void ddlClienteCertificadoPais_DataBound(object sender, EventArgs e)
        {
            if (((DropDownList)sender).Items.FindByValue("0") == null)
                ((DropDownList)sender).Items.Insert(0, new ListItem("Todos", "0"));
        }

        protected void btnClienteCertificadoBuscar_Click(object sender, EventArgs e)
        {
            string strLink = string.Format("~/pt/ClientesResultadoPesquisa.aspx?ID_CERTIFICADO={0}&PRODUTO={1}&CLIENTE={2}&PAIS={3}&ESTADO_SIGLA={4}", ddlCertificado.SelectedValue, txtClienteCertificadoProduto.Text, txtClienteCertificadoCliente.Text, ddlClienteCertificadoPais.SelectedValue, ddlClienteCertificadoUf.SelectedValue);
            Response.Redirect(strLink);
        }

        protected void ddlCertificado_DataBound(object sender, EventArgs e)
        {
            if (((DropDownList)sender).Items.FindByValue("0") == null)
                ((DropDownList)sender).Items.Insert(0, new ListItem("Todos", "0"));
        }
    }
}