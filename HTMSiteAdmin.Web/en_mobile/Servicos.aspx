﻿<%@ Page Title="" Language="C#" MasterPageFile="~/en_mobile/InternaMobile.Master" AutoEventWireup="true" CodeBehind="Servicos.aspx.cs" Inherits="HTMSiteAdmin.Web.en_mobile.Servicos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Conteudo" runat="server">
    <div class="interna servicos">

        <h3 class="tituloGeral">Services</h3>

        <img src="../media_mobile/img/servicos/1.jpg" alt="" width="280" height="100" />

        <h4 class="tituloInterno">Certification</h4>

        <p>
            In order to verify certification requirements, IBD relies on a team of specialized inspectors that audit agricultural properties and production processes to verify conformity of cultivation and/or processes with organic and biodynamic production norms. 
            <br />
            <br />
            Certification requires a series of precautions, such as the detoxification of the soil for 1 to 3 years in areas under transition from chemical to organic agriculture, the absence of chemical fertilizers and pesticides, attention to ecological aspects (for example, the conservation of Permanent Preservation Areas through reforestation of riparian forests), preservation of native species and natural springs, respect for Indian reserves and social standards based on international labor agreements, humane treatment of animals, and, for the Fair Trade Ecosocial standards, involvement with social and environmental conservation projects. 
        </p>

        <div class="clear"></div>

        <img src="../media_mobile/img/servicos/2.jpg" alt="" width="280" height="100" />

        <h4 class="tituloInterno">Auditoria</h4>

        <p>
            O IBD Certificações criou o Programa de Aprovação de Insumos no intuito de avaliar a possibilidade de uso dos insumos comerciais disponíveis no mercado de acordo com as principais diretrizes de produção orgânica (Normas EUA, Européia, IFOAM, Japonesa, Canadense e Brasileira). Possui uma diretriz e procedimentos próprios e únicos a nível mundial que garantem segurança, credibilidade e confiabilidade aos insumos aprovados e aos produtores e empresas interessadas no uso.
            <br />
            <br />
            O Programa de Aprovação avalia os insumos de acordo com as normas de produção agrícola, processamento de alimentos e pecuária orgânica, sendo destinado a fabricantes, importadores e distribuidores de insumos localizados no Brasil e no exterior.
        </p>

        <div class="clear"></div>

        <img src="../media_mobile/img/servicos/3.jpg" alt="" width="280" height="100" />

        <h4 class="tituloInterno">Training Services</h4>

        <p>
            IBD organizes trainings and courses for its team of inspectors in addition to courses that are open to professionals and candidates for inspection work.<br />
            <br />
            Contact us for more information about the schedule of upcoming courses or access the course schedule. <a href="Eventos.aspx">[click here]</a>
        </p>

        <h4 class="tituloInterno">Mapping and Geoprocessing</h4>

        <div style="display: flex; flex-direction: column; align-items: center">
            <img src="../Media/img/mapa-geo-detalhes-1.png" alt="" style="margin: 15px auto; text-align: center" />
            <img src="../Media/img/seta.png" alt="" style="transform: rotate(90deg); max-height: 28px" />
            <img src="../Media/img/mapa-geo-detalhes-2.png" alt="" style="margin: 15px auto; text-align: center" />
        </div>

        <p>Geoprocessing is the technology capable of spatializing and cross-referencing geographically information from a variety of sources. Through layers of information that can be studied and analyzed allows a better understanding of how geographical factors interfere in productive and everyday activities.
           IBD has a variety of services and applications in geoprocessing and geographic information system. Access the link below and see the portfolio of products and services IBD offers.</p>

        <a href="http://ibd.com.br/en/ServicosMapeamentoGeoprocessamentoDetalhes.aspx">Access here</a>
    </div>
    <!--interna-->
</asp:Content>
