﻿using HTMSiteAdmin.Business.Conteudos;
using HTMSiteAdmin.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HTMSiteAdmin.Web.en_mobile
{
    public partial class NoticiasDetalhes : System.Web.UI.Page
    {
        private int? idConteudo
        {
            get
            {
                try { return Convert.ToInt32(Request.QueryString["id_conteudo"]); }
                catch (Exception) { return null; }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (idConteudo.HasValue && idConteudo.Value > 0)
            {
                //var container = new HTMSiteAdminEntities();
                var _objConteudo = new ConteudoBo().Find(c => c.ID_CONTEUDO == idConteudo.Value).First();

                if (_objConteudo.ID_CONTEUDO_SITUACAO != 1)
                    return;

                if (_objConteudo.DATA_PUBLICACAO.HasValue)
                {                    
                    ltrDataPublicacao.Text = _objConteudo.DATA_PUBLICACAO.Value.ToShortDateString();
                }
                else
                {                    
                    ltrDataPublicacao.Text = _objConteudo.SESSAO.DATA_INICIO.ToShortDateString();
                }

                //var arquivoDigital = container.ARQUIVO_DIGITAL.FirstOrDefault(a => a.ID_ARQUIVO == _objConteudo.ID_ARQUIVO_DIGITAL);                
                ltrTitulo.Text = _objConteudo.TITULO_EN;
                if (!String.IsNullOrEmpty(_objConteudo.SUB_TITULO_EN))
                    ltrSubTitulo.Text = _objConteudo.SUB_TITULO_EN;

                ltrConteudo.Text = _objConteudo.CONTENT_DATA;
                if (!String.IsNullOrEmpty(_objConteudo.FONTE))
                    ltrFonte.Text = "Source: " + _objConteudo.FONTE;

                //ltrImagem.Text = "<img src=\"" + ResolveUrl(string.Format("~/ShowFile.aspx?action=1&fileid={0}", _objConteudo.ID_ARQUIVO_DIGITAL)) + "\" alt=\"\" width=\"\" height=\"\" />";
            }
        }
    }
}