﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HTMSiteAdmin.Web
{
	public partial class Default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{           

            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;

            string lang = currentCulture.TwoLetterISOLanguageName;

            if (lang.Equals("pt"))
            {
                if (Classes.Util.fBrowserIsMobile())
                    Response.Redirect("~/pt_mobile/Default.aspx");
                else
                    Response.Redirect("~/pt/Default.aspx");
            }
            else
            {
                if (Classes.Util.fBrowserIsMobile())
                    Response.Redirect("~/en_mobile/Default.aspx");
                else
                    Response.Redirect("~/en/Default.aspx");
            }

		}
	}
}