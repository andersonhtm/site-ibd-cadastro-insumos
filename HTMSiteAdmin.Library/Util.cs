﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.IO;
using System.Web;
using System.Text;
using System.Data.SqlClient;

namespace HTMSiteAdmin.Library
{
    public static class Util
    {
        public static string GetTotalMemoryString()
        {
            long MemoriaApp = GC.GetTotalMemory(false);

            decimal tamKb = MemoriaApp / 1024;
            decimal tamMb = tamKb / 1024;

            if (tamMb > 1)
                return tamMb.ToString("0.00 MB");
            else if (tamKb > 1)
                return tamKb.ToString("0 KB");
            else
                return MemoriaApp.ToString("0 bytes");
        }

        public static string GetServerPath(Page _pagina)
        {
            Uri _myUrl = _pagina.Request.Url;

            string retorno = _myUrl.Scheme + "://" + _myUrl.Authority;
            return retorno;
        }

        public static bool SaveFile(string path, string name, string extension, byte[] fileContent)
        {

            using (FileStream file = new FileStream(string.Format("{0}{1}.{2}", path, name, extension), FileMode.Create)) {
                using (BinaryWriter writer = new BinaryWriter(file))
                {
                    writer.Write(fileContent);
                }
            }
            return true;
        }

        public static string GetWebAppRoot()
        {
            string host = (HttpContext.Current.Request.Url.IsDefaultPort) ?
                HttpContext.Current.Request.Url.Host :
                HttpContext.Current.Request.Url.Authority;
            host = String.Format("{0}://{1}", HttpContext.Current.Request.Url.Scheme, host);
            if (HttpContext.Current.Request.ApplicationPath == "/")
                return host;
            else
                return host + HttpContext.Current.Request.ApplicationPath;
        }
    }
}