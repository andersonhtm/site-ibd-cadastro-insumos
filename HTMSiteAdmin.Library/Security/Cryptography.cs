﻿using System;

namespace HTMSiteAdmin.Library.Security
{
    public static class Cryptography
    {
        public static string Encrypt(string pConteudo)
        {
            byte[] bytConteudo = System.Text.ASCIIEncoding.ASCII.GetBytes(pConteudo);
            return Convert.ToBase64String(bytConteudo);
        }

        public static string Decrypt(string pConteudo)
        {
            byte[] bytConteudo = Convert.FromBase64String(pConteudo);
            return System.Text.ASCIIEncoding.ASCII.GetString(bytConteudo);
        }

        public static string EncryptPassword(string input)
        {
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}
