﻿using System;

namespace HTMSiteAdmin.Library.Exceptions
{
    /// <summary>
    /// Excessões tratáveis do sistema
    /// </summary>
    [Serializable]
    public class HTMSiteAdminException : ApplicationException
    {
        public HTMSiteAdminException() { }
        public HTMSiteAdminException(string message) : base(message) { }
        public HTMSiteAdminException(string message, System.Exception inner) : base(message, inner) { }
        protected HTMSiteAdminException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    /// <summary>
    /// Excessões de erros do sistema
    /// </summary>
    [Serializable]
    public class HTMSiteAdminErrorException : ApplicationException
    {
        public HTMSiteAdminErrorException() { }
        public HTMSiteAdminErrorException(string message) : base(message) { }
        public HTMSiteAdminErrorException(string message, System.Exception inner) : base(message, inner) { }
        protected HTMSiteAdminErrorException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
