﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace HTMSiteAdmin.Library
{


    public static class FileTools
    {
        public enum enmFileTypes
        {
            Image = 1,
            DocumentWord = 2,
            DocumentPdf = 3,
            DocumentExcel = 4,
            Generic = 5
        }

        public static enmFileTypes GetFileType(string _Extension)
        {
            if (_Extension.ToUpper().Contains("JPG")
                || _Extension.ToUpper().Contains("GIF")
                || _Extension.ToUpper().Contains("PNG")
                || _Extension.ToUpper().Contains("BMP")
                || _Extension.ToUpper().Contains("TIF"))
                return enmFileTypes.Image;
            else if (_Extension.ToUpper().Contains("DOC")
                || _Extension.ToUpper().Contains("RTF")
                || _Extension.ToUpper().Contains("TXT"))
                return enmFileTypes.DocumentWord;
            else if (_Extension.ToUpper().Contains("XLS")
                || _Extension.ToUpper().Contains("CSV"))
                return enmFileTypes.DocumentExcel;
            else if (_Extension.ToUpper().Contains("PDF"))
                return enmFileTypes.DocumentPdf;
            else
                return enmFileTypes.Generic;
        }
    }

    public static class ImageTools
    {
        #region [ Enumerators ]

        public enum UseMethod
        {
            ConstrainProportions = 1,
            ScaleToFixed = 2,
            ScalePercent = 3,
            ScaleToFixedWithCrop = 4
        }

        public enum Dimensions
        {
            Width = 1,
            Height = 2
        }

        public enum AnchorPosition
        {
            Top,
            Center,
            Bottom,
            Left,
            Right
        }

        #endregion

        #region [ Converters ]

        public static byte[] ImageToByte(Bitmap _objBitmap)
        {
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                //Salvando a imagem dentro do MemoryStream
                _objBitmap.Save(stream, ImageFormat.Png);

                //Lendo MemoryStream
                byte[] retrurnBytes = stream.GetBuffer();
                _objBitmap.Dispose();
                stream.Close();

                return retrurnBytes;
            }
        }

        public static byte[] ImageToByte(Image _objImage)
        {
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                //Salvando a imagem dentro do MemoryStream
                _objImage.Save(stream, ImageFormat.Png);

                //Lendo MemoryStream
                byte[] retrurnBytes = stream.GetBuffer();
                _objImage.Dispose();
                stream.Close();

                return retrurnBytes;
            }
        }

        #endregion

        #region [ Methods ]

        public static byte[] ConstrainProportions(byte[] FileBytes, int Size, Dimensions Dimension)
        {
            //Criando stream usando o buffer
            using (MemoryStream stream = new MemoryStream(FileBytes))
            {
                //Criando imagem a partir do stream
                using (Image imgPhoto = Image.FromStream(stream))
                {
                    //Tamanho original
                    int sourceWidth = imgPhoto.Width;
                    int sourceHeight = imgPhoto.Height;

                    //Posicionamento
                    int sourceX = 0;
                    int sourceY = 0;
                    int destX = 0;
                    int destY = 0;

                    //Calculando porcentagens
                    float nPercent = 0;
                    switch (Dimension)
                    {
                        case Dimensions.Width:
                            nPercent = ((float)Size / (float)sourceWidth);
                            break;
                        default:
                            nPercent = ((float)Size / (float)sourceHeight);
                            break;
                    }

                    //Novos tamanhos
                    int destWidth = (int)(sourceWidth * nPercent);
                    int destHeight = (int)(sourceHeight * nPercent);

                    using (Bitmap bmPhoto = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb))
                    {
                        bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

                        using (Graphics grPhoto = Graphics.FromImage(bmPhoto))
                        {
                            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

                            grPhoto.DrawImage(imgPhoto,
                            new Rectangle(destX, destY, destWidth, destHeight),
                            new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight), GraphicsUnit.Pixel);
                        }
                        return ImageToByte(bmPhoto);
                    }
                }
            }
        }

        public static byte[] ScaleToFixed(byte[] FileBytes, int Width, int Height)
        {
            //Criando stream usando o buffer
            using (MemoryStream stream = new MemoryStream(FileBytes))
            {
                //Criando imagem a partir do stream
                using (Image imgPhoto = Image.FromStream(stream))
                {
                    //Tamanho Original
                    int sourceWidth = imgPhoto.Width;
                    int sourceHeight = imgPhoto.Height;

                    //Posicionamentos
                    int sourceX = 0;
                    int sourceY = 0;
                    int destX = 0;
                    int destY = 0;

                    //Porcentagens
                    float nPercent = 0;
                    float nPercentW = 0;
                    float nPercentH = 0;

                    //Calculando a porcentagem do redimensionamento
                    nPercentW = ((float)Width / (float)sourceWidth);
                    nPercentH = ((float)Height / (float)sourceHeight);
                    if (nPercentH < nPercentW)
                    {
                        nPercent = nPercentH;
                        destX = System.Convert.ToInt16((Width - (sourceWidth * nPercent)) / 2);
                    }
                    else
                    {
                        nPercent = nPercentW;
                        destY = System.Convert.ToInt16((Height - (sourceHeight * nPercent)) / 2);
                    }

                    //Calculando os novos tamanhos da imagem
                    int destWidth = (int)(sourceWidth * nPercent);
                    int destHeight = (int)(sourceHeight * nPercent);

                    //Criando Bitmap auxiliar para construção da imagem redimensionada no tamanho desejado
                    using (Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb))
                    {
                        bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

                        //Criando Graphic para renderizar a imagem
                        using (Graphics grPhoto = Graphics.FromImage(bmPhoto))
                        {
                            grPhoto.Clear(Color.Transparent);
                            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            grPhoto.DrawImage(imgPhoto, new Rectangle(destX, destY, destWidth, destHeight), new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight), GraphicsUnit.Pixel);
                        }

                        return ImageToByte(bmPhoto);
                    }
                }
            }
        }

        public static byte[] ScalePercent(byte[] FileBytes, int Percent)
        {
            //Criando stream usando o buffer
            using (MemoryStream stream = new MemoryStream(FileBytes))
            {
                //Criando imagem a partir do stream
                using (Image imgPhoto = Image.FromStream(stream))
                {
                    //Porcentagem
                    float nPercent = ((float)Percent / 100);

                    //Tamanho original
                    int sourceWidth = imgPhoto.Width;
                    int sourceHeight = imgPhoto.Height;

                    //Posicionamentos
                    int sourceX = 0;
                    int sourceY = 0;
                    int destX = 0;
                    int destY = 0;

                    //Novos tamanhos da imagem
                    int destWidth = (int)(sourceWidth * nPercent);
                    int destHeight = (int)(sourceHeight * nPercent);

                    using (Bitmap bmPhoto = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb))
                    {
                        bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

                        using (Graphics grPhoto = Graphics.FromImage(bmPhoto))
                        {
                            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

                            grPhoto.DrawImage(imgPhoto,
                                new Rectangle(destX, destY, destWidth, destHeight),
                                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                                GraphicsUnit.Pixel);

                        }

                        return ImageToByte(bmPhoto);
                    }
                }
            }
        }

        public static byte[] ScaleToFixedWithCrop(byte[] FileBytes, int Width, int Height, AnchorPosition Anchor)
        {
            //Criando stream usando o buffer
            using (MemoryStream stream = new MemoryStream(FileBytes))
            {
                //Criando imagem a partir do stream
                using (Image imgPhoto = Image.FromStream(stream))
                {
                    //Tamanho orginal
                    int sourceWidth = imgPhoto.Width;
                    int sourceHeight = imgPhoto.Height;

                    //Posicionamento
                    int sourceX = 0;
                    int sourceY = 0;
                    int destX = 0;
                    int destY = 0;

                    //Porcentagens
                    float nPercent = 0;
                    float nPercentW = 0;
                    float nPercentH = 0;

                    //Calculando novas porcentagens a partir do ponto ancora
                    nPercentW = ((float)Width / (float)sourceWidth);
                    nPercentH = ((float)Height / (float)sourceHeight);

                    if (nPercentH < nPercentW)
                    {
                        nPercent = nPercentW;
                        switch (Anchor)
                        {
                            case AnchorPosition.Top:
                                destY = 0;
                                break;
                            case AnchorPosition.Bottom:
                                destY = (int)
                                    (Height - (sourceHeight * nPercent));
                                break;
                            default:
                                destY = (int)
                                    ((Height - (sourceHeight * nPercent)) / 2);
                                break;
                        }
                    }
                    else
                    {
                        nPercent = nPercentH;
                        switch (Anchor)
                        {
                            case AnchorPosition.Left:
                                destX = 0;
                                break;
                            case AnchorPosition.Right:
                                destX = (int)
                                  (Width - (sourceWidth * nPercent));
                                break;
                            default:
                                destX = (int)
                                  ((Width - (sourceWidth * nPercent)) / 2);
                                break;
                        }
                    }

                    //Novos tamanhos da imagem
                    int destWidth = (int)(sourceWidth * nPercent);
                    int destHeight = (int)(sourceHeight * nPercent);

                    //Criando bitmap auxiliar para renderizar imagem
                    using (Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb))
                    {
                        bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

                        using (Graphics grPhoto = Graphics.FromImage(bmPhoto))
                        {
                            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            grPhoto.DrawImage(imgPhoto, new Rectangle(destX, destY, destWidth, destHeight), new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight), GraphicsUnit.Pixel);
                        }

                        return ImageToByte(bmPhoto);
                    }
                }
            }
        }

        #endregion
    }
}
