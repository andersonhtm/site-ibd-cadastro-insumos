﻿using System.Web.UI;

namespace HTMSiteAdmin.Library.UserInterface
{
    public interface IPagina
    {
        void ValidateAcess();

        void MapButtons();

        void UserInterfaceAspectChanged();

        void StartPageControls();

        void RestartForm();

        void ClearMemory();

        void ClearFields();

        void EnableFields();

        void DisableFields();

        void ButtonNew_Click(object sender, ImageClickEventArgs e);

        void ButtonSave_Click(object sender, ImageClickEventArgs e);

        void ButtonSaveNew_Click(object sender, ImageClickEventArgs e);

        void ButtonRefresh_Click(object sender, ImageClickEventArgs e);

        void ButtonCancel_Click(object sender, ImageClickEventArgs e);

        void ButtonDelete_Click(object sender, ImageClickEventArgs e);

        void GetForm();

        void SaveView();

        //void FillNullableObjectProperties();

        void SetForm();
    }
}
