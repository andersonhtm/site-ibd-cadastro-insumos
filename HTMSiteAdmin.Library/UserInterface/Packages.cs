﻿
namespace HTMSiteAdmin.Library.UserInterface
{
    /// <summary>
    /// Define o Aspecto de uma interface
    /// </summary>
    public enum enmUserInterfaceAspect
    {
        Start,
        New,
        Update
    }
}
