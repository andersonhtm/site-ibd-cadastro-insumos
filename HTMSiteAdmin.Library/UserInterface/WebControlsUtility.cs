﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;

namespace HTMSiteAdmin.Library.UserInterface
{
    public sealed class UploadedFile
    {
        public UploadedFile(byte[] FileBytes, string FileName) { this._fileBytes = FileBytes; this._fileName = FileName; }

        private Guid? _fileId;

        public Guid FileId
        {
            get
            {
                if (!_fileId.HasValue)
                    _fileId = Guid.NewGuid();

                return _fileId.Value;
            }
            set { _fileId = value; }
        }

        private byte[] _fileBytes;
        public byte[] FileBytes { get { return _fileBytes; } }

        private string _fileName;
        public string FileName { get { return _fileName; } }

        public string FileExtension { get { return this._fileName.Split('.')[1]; } }
    }

    public static class WebControlsUtility
    {
        /// <summary>
        /// Controla a aparência e exibição dos Expanders da tela
        /// </summary>
        /// <param name="pnlTitle">Panel do Título</param>
        /// <param name="pnlContent">Panel do Conteúdo</param>
        /// <param name="Active">Ativa ou Inativa o Expander sem testar o status atual. Padrão: Nulo</param>
        public static void CollapseControl(Panel pnlTitle, Panel pnlContent, bool? Active = null)
        {
            if (Active.HasValue && Active.Value)
            {
                pnlTitle.CssClass = "CollapseFormTitleActive";
                pnlContent.CssClass = "CollapseFormContentActive";
            }
            else if (Active.HasValue && !Active.Value)
            {
                pnlTitle.CssClass = "CollapseFormTitle";
                pnlContent.CssClass = "CollapseFormContent";
            }
            else
            {
                if (((Panel)pnlContent).CssClass.Equals("CollapseFormContent"))
                {
                    pnlTitle.CssClass = "CollapseFormTitleActive";
                    pnlContent.CssClass = "CollapseFormContentActive";
                }
                else
                {
                    pnlTitle.CssClass = "CollapseFormTitle";
                    pnlContent.CssClass = "CollapseFormContent";
                }
            }
        }

        /// <summary>
        /// Coloca o item "Selecionar" em DropDownList, caso não houver
        /// </summary>
        /// <param name="sender">DropDownList que irá receber o item</param>
        /// <param name="DefaultValue">Valor para o item "Selecionar". Valor padrão: 0.</param>
        public static void DropDownList_DataBound(object sender, string UICulture = "pt", string DefaultValue = "0")
        {
            switch (UICulture)
            {
                case "en":
                    if (((DropDownList)sender).Items.FindByValue(DefaultValue) == null)
                        ((DropDownList)sender).Items.Insert(0, new ListItem("All", DefaultValue));
                    break;
                default:
                    if (((DropDownList)sender).Items.FindByValue(DefaultValue) == null)
                        ((DropDownList)sender).Items.Insert(0, new ListItem("Selecione", DefaultValue));
                    break;
            }
        }

        /// <summary>
        /// Acrescenta item ao DropDownList, caso o item ainda não tenha sido incluído
        /// </summary>
        /// <param name="sender">DropDownList que irá receber o item</param>
        /// <param name="Text">Texto do item</param>
        /// <param name="Value">Valor do item</param>
        public static void DropDownList_AddItem(object sender, string Text, string Value)
        {
            if (((DropDownList)sender).Items.FindByValue(Value) == null)
                ((DropDownList)sender).Items.Insert(((DropDownList)sender).Items.Count, new ListItem(Text, Value));
        }

        /// <summary>
        /// Este metodo trata-se de um comando que por referencia executa um script dentro de um updatepanel
        /// passando a página e a string do script
        /// </summary>
        public static void ExecuteScriptInAjax(Page Pagina, string script)
        {
            ScriptManager.RegisterClientScriptBlock(Pagina, typeof(Page), Guid.NewGuid().ToString(), script.Replace("'", "\'"), true);
        }

        public static UploadedFile AsyncFileUpload_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            return new UploadedFile(((AsyncFileUpload)sender).FileBytes, ((AsyncFileUpload)sender).FileName);
        }
    }
}
