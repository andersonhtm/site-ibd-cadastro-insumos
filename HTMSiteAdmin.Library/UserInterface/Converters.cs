﻿using System;

namespace HTMSiteAdmin.Library.UserInterface
{
    public static class Converters
    {
        public static string SituacaoString(DateTime? DataValidadeInicial, DateTime? DataValidadeFinal)
        {
            try
            {
                if (!DataValidadeInicial.HasValue)
                    return "Data inicial não definida";

                string strSituacao = DataValidadeInicial.HasValue && DataValidadeInicial.Value < DateTime.Now ? "Ativo" : "Inativo";

                if (strSituacao.Equals("Ativo") && DataValidadeFinal.HasValue)
                    strSituacao = DataValidadeFinal.Value >= DateTime.Now ? "Ativo" : "Inativo";

                return strSituacao;
            }
            catch (System.Exception ex) { throw new Exceptions.HTMSiteAdminErrorException("Erro ao criar String Situação. Motivo: " + ex.Message); }
        }

        public static string MesString(int MesNumero)
        {
            switch (MesNumero)
            {
                case 1:
                    return "Janeiro";
                case 2:
                    return "Fevereiro";
                case 3:
                    return "Março";
                case 4:
                    return "Abril";
                case 5:
                    return "Maio";
                case 6:
                    return "Junho";
                case 7:
                    return "Julho";
                case 8:
                    return "Agosto";
                case 9:
                    return "Setembro";
                case 10:
                    return "Outubro";
                case 11:
                    return "Novembro";
                case 12:
                    return "Dezembro";
                default:
                    return "Desconhecido";
            }
        }
    }
}
